package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="master_educationlevel")
@TableGenerator(name ="master_educationlevel", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "master_educationlevel.findById", query = "SELECT r FROM EducationLevelDO r where r.educationLevelId =:id"),
    @NamedQuery(name = "master_educationlevel.findByStatus", query = "SELECT r FROM EducationLevelDO r where r.status =:status"),
    @NamedQuery(name = "master_educationlevel.findByName", query = "SELECT r FROM EducationLevelDO r where r.name=:name"),
    @NamedQuery(name = "master_educationlevel.findByNameAndId", query = "SELECT r FROM EducationLevelDO r where r.name=:name and r.educationLevelId NOT IN :id"),
})
public class EducationLevelDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_BY_ID = "master_educationlevel.findById";
	
	public static final String FIND_BY_STATUS = "master_educationlevel.findByStatus";
	
	public static final String FIND_BY_NAME = "master_educationlevel.findByName";
	
	public static final String FIND_FOR_UPDATE = "master_educationlevel.findByNameAndId";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true) 
	private Long educationLevelId;
	private String name;
	private char status;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getEducationLevelId() {
		return educationLevelId;
	}
	public void setEducationLevelId(Long educationLevelId) {
		this.educationLevelId = educationLevelId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}