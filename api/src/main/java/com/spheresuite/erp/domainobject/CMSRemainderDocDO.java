package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="cmsremainderdoc")
@TableGenerator(name ="cmsremainderdoc", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "cmsremainderdoc.findAll", query = "SELECT r FROM CMSRemainderDocDO r"),
    @NamedQuery(name = "cmsremainderdoc.findByCMSRemainderId", query = "SELECT r FROM CMSRemainderDocDO r where r.cmsremainderId =:id"),
})
public class CMSRemainderDocDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "cmsremainderdoc.findAll";
	
	public static final String FIND_BY_CMSREMAINDER_ID = "cmsremainderdoc.findByCMSRemainderId";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO) 
	private Long id;
	private Long cmsremainderId;
	@Column(columnDefinition="LONGTEXT")
	private String photo;
	private String fileName;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private String updatedBy;
	private Long empId;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getCmsremainderId() {
		return cmsremainderId;
	}
	public void setCmsremainderId(Long cmsremainderId) {
		this.cmsremainderId = cmsremainderId;
	}
	
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Long getEmpId() {
		return empId;
	}
	public void setEmpId(Long empId) {
		this.empId = empId;
	}
}