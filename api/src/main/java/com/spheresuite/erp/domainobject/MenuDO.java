package com.spheresuite.erp.domainobject;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="menu")
@TableGenerator(name ="menu", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "menu.findById", query = "SELECT r FROM MenuDO r where r.menuId =:id"),
    @NamedQuery(name = "menu.findMainMenu", query = "SELECT r FROM MenuDO r where r.supermenu =:supermenu"),
    @NamedQuery(name = "menu.findMenu", query = "SELECT r FROM MenuDO r where r.supermenu IS NULL"),
    @NamedQuery(name = "menu.findPoduct", query = "SELECT r FROM MenuDO r where r.product =:submenu"),
    
   
})
public class MenuDO implements Serializable {
	private static final long serialVersionUID = 1L;

	
	public static final String FIND_BY_ID = "menu.findById";
	
	public static final String FIND_SUPER_MENU = "menu.findMainMenu";
	
	public static final String FIND_MENU = "menu.findMenu";
	
	public static final String FIND_PRODUCT = "menu.findPoduct";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long menuId;
	
	private String name;
	private String displayName;
	private String url;
	private String icon;
	private Long supermenu;
	private Long product;
	private Long orderId;
	private String activeicon;
	
	public Long getMenuId() {
		return menuId;
	}
	public void setMenuId(Long menuId) {
		this.menuId = menuId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public Long getSupermenu() {
		return supermenu;
	}
	public void setSupermenu(Long supermenu) {
		this.supermenu = supermenu;
	}
	public Long getProduct() {
		return product;
	}
	public void setProduct(Long product) {
		this.product = product;
	}
	public Long getOrderId() {
		return orderId;
	}
	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}
	public String getActiveicon() {
		return activeicon;
	}
	public void setActiveicon(String activeicon) {
		this.activeicon = activeicon;
	}
}