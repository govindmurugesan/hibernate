package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="ams_master_os")
@TableGenerator(name ="ams_master_os", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "ams_master_os.findAll", query = "SELECT r FROM AMSOSDO r"),
    @NamedQuery(name = "ams_master_os.findById", query = "SELECT r FROM AMSOSDO r where r.osId =:id"),
    @NamedQuery(name = "ams_master_os.findByStatus", query = "SELECT r FROM AMSOSDO r where r.status =:status"),
    @NamedQuery(name = "ams_master_os.findByName", query = "SELECT r FROM AMSOSDO r where r.status =:status and r.os=:name"),
   @NamedQuery(name = "ams_master_os.findByNameAndId", query = "SELECT r FROM AMSOSDO r where r.status =:status and r.os=:name and r.osId NOT IN :id"),
})
public class AMSOSDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "ams_master_os.findAll";
	public static final String FIND_BY_ID = "ams_master_os.findById";
	public static final String FIND_BY_STATUS = "ams_master_os.findByStatus";
	public static final String FIND_BY_NAME = "ams_master_os.findByName";
	public static final String FIND_FOR_UPDATE = "ams_master_os.findByNameAndId";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO) 
	@Column(nullable = false, unique = true) 
	private Long osId;
	
	private String os;
	private String os_comments;
	private char status;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
	
	public Long getOsId() {
		return osId;
	}
	public void setOsId(Long osId) {
		this.osId = osId;
	}
	public String getOs() {
		return os;
	}
	public void setOs(String os) {
		this.os = os;
	}
	public String getOs_comments() {
		return os_comments;
	}
	public void setOs_comments(String os_comments) {
		this.os_comments = os_comments;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}