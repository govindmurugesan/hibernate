package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="user")
@TableGenerator(name ="user", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "user.findById", query = "SELECT u FROM UserDO u where u.userId =:id and u.isDeleted=0"),
    //@NamedQuery(name = "UserDO.findByUserName", query = "SELECT u FROM UserDO u where u.name =:name  and  u.idDeleted=0"),
    @NamedQuery(name = "user.findByStatus", query = "SELECT u FROM UserDO u where u.status =:status  and  u.isDeleted=0"),
    @NamedQuery(name = "user.findByEmailId", query = "SELECT u FROM UserDO u where u.employee.companyemail =:email  and  u.isDeleted=0 and u.status = 'a'"),
    @NamedQuery(name = "user.findForLogin", query = "SELECT u FROM UserDO u where u.employee.companyemail =:email  and  u.password=:password and u.isDeleted=0 and u.status =:status"),
    @NamedQuery(name = "user.findBytempPassword", query = "SELECT u FROM UserDO u where u.temppassowrd =:temppassword  and  u.isDeleted=0 and u.employee.empId =:empId"),
    @NamedQuery(name = "user.findByEmailForEmp", query = "SELECT u FROM UserDO u where (u.employee.companyemail =:personal or u.employee.companyemail =:secondary) and  u.isDeleted=0"),
    @NamedQuery(name = "user.findByEmpId", query = "SELECT u FROM UserDO u where u.employee.empId =:userid and u.isDeleted=0"),
    @NamedQuery(name = "user.findSuperAdmin", query = "SELECT u FROM UserDO u where u.status =:status  and  u.isDeleted=0 and u.role.roleId =:adminId"),
    @NamedQuery(name = "user.findforUpdate", query = "SELECT r FROM UserDO r where  r.userId NOT IN :id"),
    @NamedQuery(name = "user.findByUnitid", query = "SELECT u FROM UserDO u where u.employee.unitOrBranch.unitOrBranchId =:unit "),
    
    
    
})
public class UserDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_BY_ID = "user.findById";
	
	public static final String FIND_BY_EMAIL = "user.findByEmailId";
	
	public static final String FIND_BY_EMAIL_FOR_EMP = "user.findByEmailForEmp";
	
	//public static final String FIND_BY_NAME = "UserDO.findByName";
	
	public static final String FIND_BY_STATUS = "user.findByStatus";
	
	public static final String FIND_BY_USER_NAME = "user.findByUserName";
	
	public static final String FIND_FOR_LOGIN = "user.findForLogin";
	
	public static final String FIND_BY_UNITID = "user.findByUnitid";
	
	
	public static final String FIND_BY_TEMPPASSWORD = "user.findBytempPassword";
	
	public static final String FIND_BY_EMP_ID = "user.findByEmpId";
	
	public static final String FIND_SUPERADMIN = "user.findSuperAdmin";
	
	public static final String FIND_FOR_UPDATE = "user.findforUpdate";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long userId;
	
	private String temppassowrd;
	private String password;
	private char status;
	private Long isDeleted;

	@ManyToOne
	@JoinColumn(name="empId")
	private EmployeeDO employee;
	
	@ManyToOne
	@JoinColumn(name="roleId")
	private RolesDO role;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public EmployeeDO getEmployee() {
		return employee;
	}
	public void setEmployee(EmployeeDO employee) {
		this.employee = employee;
	}
	public String getTemppassowrd() {
		return temppassowrd;
	}
	public void setTemppassowrd(String temppassowrd) {
		this.temppassowrd = temppassowrd;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public RolesDO getRole() {
		return role;
	}
	public void setRole(RolesDO role) {
		this.role = role;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public Long getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(Long isDeleted) {
		this.isDeleted = isDeleted;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}