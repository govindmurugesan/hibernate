package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="crm_project")
@TableGenerator(name ="crm_project", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "crm_project.findAll", query = "SELECT r FROM ProjectDO r where r.employee.empId IN :id"),
    @NamedQuery(name = "crm_project.findAllData", query = "SELECT r FROM ProjectDO r"),
    @NamedQuery(name = "crm_project.findById", query = "SELECT r FROM ProjectDO r where r.projectId =:id"),
    @NamedQuery(name = "crm_project.findByOppId", query = "SELECT r FROM ProjectDO r where r.opportunity.opportunityId =:id"),
})
public class ProjectDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "crm_project.findAll";
	public static final String FIND_BY_ID = "crm_project.findById";
	public static final String FIND_BY_OPP_ID = "crm_project.findByOppId";
	public static final String FIND_ALL_DATA = "crm_project.findAllData";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long projectId;
	
	private String projectname;
	private String displayname;
	private String startdate;
	private String enddate;
	private String projectduration;
	private Long probability;
	private Long amount;
	private String comment;
	
	@ManyToOne
	@JoinColumn(name="empId")
	private EmployeeDO employee;
	
	@ManyToOne
	@JoinColumn(name="gstTaxSlabId")
	private GSTTaxSlabDO gstTaxSlab;
	
	@ManyToOne
	@JoinColumn(name="opportunityId")
	private OpportunitiesDO opportunity;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getProjectId() {
		return projectId;
	}
	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}
	public String getProjectname() {
		return projectname;
	}
	public void setProjectname(String projectname) {
		this.projectname = projectname;
	}
	public String getDisplayname() {
		return displayname;
	}
	public void setDisplayname(String displayname) {
		this.displayname = displayname;
	}
	public String getStartdate() {
		return startdate;
	}
	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}
	public String getEnddate() {
		return enddate;
	}
	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}
	public String getProjectduration() {
		return projectduration;
	}
	public void setProjectduration(String projectduration) {
		this.projectduration = projectduration;
	}
	public Long getProbability() {
		return probability;
	}
	public void setProbability(Long probability) {
		this.probability = probability;
	}
	public Long getAmount() {
		return amount;
	}
	public void setAmount(Long amount) {
		this.amount = amount;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public EmployeeDO getEmployee() {
		return employee;
	}
	public void setEmployee(EmployeeDO employee) {
		this.employee = employee;
	}
	public GSTTaxSlabDO getGstTaxSlab() {
		return gstTaxSlab;
	}
	public void setGstTaxSlab(GSTTaxSlabDO gstTaxSlab) {
		this.gstTaxSlab = gstTaxSlab;
	}
	public OpportunitiesDO getOpportunity() {
		return opportunity;
	}
	public void setOpportunity(OpportunitiesDO opportunity) {
		this.opportunity = opportunity;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}