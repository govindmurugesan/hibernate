package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="crm_customer")
@TableGenerator(name ="crm_customer", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "crm_customer.findAll", query = "SELECT r FROM CustomerDO r"),
    @NamedQuery(name = "crm_customer.findById", query = "SELECT r FROM CustomerDO r where r.customerId =:id"),
    @NamedQuery(name = "crm_customer.findActive", query = "SELECT r FROM CustomerDO r where r.status =:status"),
})
public class CustomerDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "customer.findAll";
	
	public static final String FIND_BY_ID = "customer.findById";
	
	public static final String FIND_ACTIVE = "customer.findActive";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO) 
	private Long customerId;
	private Long sourceId;
	
	@ManyToOne
	@JoinColumn(name="industryId")
	private IndustryDO industry;
	//private Long industryId;
	private String name;
	private String status;
	private Long mobile;
	private Long pone;
	private String email;
	private String address;
	private String comment;
	private String updatedby;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private String createdby;
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdon;
	private String empId;

	
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public IndustryDO getIndustry() {
		return industry;
	}
	public void setIndustry(IndustryDO industry) {
		this.industry = industry;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getEmpId() {
		return empId;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
	public Long getSourceId() {
		return sourceId;
	}
	public void setSourceId(Long sourceId) {
		this.sourceId = sourceId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Long getMobile() {
		return mobile;
	}
	public void setMobile(Long mobile) {
		this.mobile = mobile;
	}
	public Long getPone() {
		return pone;
	}
	public void setPone(Long pone) {
		this.pone = pone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}