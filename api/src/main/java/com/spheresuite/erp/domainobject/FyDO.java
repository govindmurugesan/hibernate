package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="master_fy")
@TableGenerator(name ="master_fy", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "master_fy.findById", query = "SELECT r FROM FyDO r where r.fyId =:id"),
    @NamedQuery(name = "master_fy.findByStatus", query = "SELECT r FROM FyDO r where r.status =:status"),
    @NamedQuery(name = "master_fy.findByName", query = "SELECT r FROM FyDO r where r.fromyear=:from and r.toyear=:to"),
    @NamedQuery(name = "master_fy.findByNameAndId", query = "SELECT r FROM FyDO r where r.fromyear=:from and r.toyear=:to and r.fyId NOT IN :id"),
})
public class FyDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static final String FIND_BY_ID = "master_fy.findById";
	
	public static final String FIND_BY_STATUS = "master_fy.findByStatus";
	
	public static final String FIND_BY_NAME = "master_fy.findByName";
	
	public static final String FIND_FOR_UPDATE = "master_fy.findByNameAndId";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true) 
	private Long fyId;
	
	@Temporal(TemporalType.DATE)
	private Date fromyear;
	@Temporal(TemporalType.DATE)
	private Date toyear;
	private char status;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getFyId() {
		return fyId;
	}
	public void setFyId(Long fyId) {
		this.fyId = fyId;
	}
	public Date getFromyear() {
		return fromyear;
	}
	public void setFromyear(Date fromyear) {
		this.fromyear = fromyear;
	}
	public Date getToyear() {
		return toyear;
	}
	public void setToyear(Date toyear) {
		this.toyear = toyear;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}