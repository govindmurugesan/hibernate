package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_insurancePaymentDetail")
@TableGenerator(name ="hrms_insurancePaymentDetail", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "hrms_insurancePaymentDetail.findById", query = "SELECT r FROM InsurancePaymentDetailDO r where r.insurancePaymentDetailId =:id"),
    @NamedQuery(name = "hrms_insurancePaymentDetail.findByAdvanceId", query = "SELECT r FROM InsurancePaymentDetailDO r where r.insurancePayment.insurancePaymentId =:id"),
    @NamedQuery(name = "hrms_insurancePaymentDetail.findByEmpIdMonth", query = "SELECT r FROM InsurancePaymentDetailDO r where r.employee.empId =:empId and r.status =:status and r.month =:month"),
})
public class InsurancePaymentDetailDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_BY_ID = "hrms_insurancePaymentDetail.findById";
	
	public static final String FIND_BY_ADVANCE_ID = "hrms_insurancePaymentDetail.findByAdvanceId";
	
	public static final String FIND_BY_EMPID_MONTH = "hrms_insurancePaymentDetail.findByEmpIdMonth";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)  
	private Long insurancePaymentDetailId;
	private String month;
	private String amount;
	private String status;
	
	@ManyToOne
	@JoinColumn(name="empId")
	private EmployeeDO employee;
	
	@ManyToOne
	@JoinColumn(name="insurancePaymentId")
	private InsurancePaymentDO insurancePayment;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
	public Long getInsurancePaymentDetailId() {
		return insurancePaymentDetailId;
	}
	public void setInsurancePaymentDetailId(Long insurancePaymentDetailId) {
		this.insurancePaymentDetailId = insurancePaymentDetailId;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public EmployeeDO getEmployee() {
		return employee;
	}
	public void setEmployee(EmployeeDO employee) {
		this.employee = employee;
	}
	public InsurancePaymentDO getInsurancePayment() {
		return insurancePayment;
	}
	public void setInsurancePayment(InsurancePaymentDO insurancePayment) {
		this.insurancePayment = insurancePayment;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}