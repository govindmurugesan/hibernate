package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_employeetravel")
@TableGenerator(name ="hrms_employeetravel", initialValue =100101, allocationSize =1)
@NamedQueries({
	 @NamedQuery(name = "hrms_employeetravel.findAll", query = "SELECT r FROM EmployeeTravelDO r ORDER BY r.empTravelId desc"),
    @NamedQuery(name = "hrms_employeetravel.findById", query = "SELECT r FROM EmployeeTravelDO r where r.empTravelId =:id"),
    @NamedQuery(name = "hrms_employeetravel.findByReportingId", query = "SELECT r FROM EmployeeTravelDO r where r.reportingTo =:id"),
    @NamedQuery(name = "hrms_employeetravel.findByEmpId", query = "SELECT r FROM EmployeeTravelDO r where r.employee.empId =:id"),
    
    /* @NamedQuery(name = "hrms_employeetravel.findByEmpIdStatus", query = "SELECT r FROM employeeTravelDO r where r.employee.empId =:id and r.status =:status"),*/
  

})
public class EmployeeTravelDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static final String FIND_ALL= "hrms_employeetravel.findAll";
	
	//public static final String FIND_ALL_PERMISSION_FORMONTH= "employeeTravelDO.findAllPermissionForMonth";

	public static final String FIND_BY_ID = "hrms_employeetravel.findById";
	
	public static final String FIND_BY_REPORTINGID = "hrms_employeetravel.findByReportingId";
	
	public static final String FIND_BY_EMPID = "hrms_employeetravel.findByEmpId";
	
	
	/*public static final String FINDBY_EMP_ID_STATUS = "employeeTravelDO.findByEmpIdStatus";*/
	
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long empTravelId;
	
	private String comments;
	private String status;
	private String type;
	private String place;
	private String reportingTo;
	
	
	@Temporal(TemporalType.DATE)
	private Date fromDate;
	@Temporal(TemporalType.DATE)
	private Date toDate;
	
	@ManyToOne
	@JoinColumn(name="empId")
	private EmployeeDO employee;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
    
	public Long getEmpTravelId() {
		return empTravelId;
	}
	public void setEmpTravelId(Long empTravelId) {
		this.empTravelId = empTravelId;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getPlace() {
		return place;
	}
	public void setPlace(String place) {
		this.place = place;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public EmployeeDO getEmployee() {
		return employee;
	}
	public void setEmployee(EmployeeDO employee) {
		this.employee = employee;
	}
	
	public String getReportingTo() {
		return reportingTo;
	}
	public void setReportingTo(String reportingTo) {
		this.reportingTo = reportingTo;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}