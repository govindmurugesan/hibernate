package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="supplieraccess")
@TableGenerator(name ="supplieraccess", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "supplieraccess.findById", query = "SELECT u FROM SupplierAccessDO u where u.supplierUserId =:id and u.isDeleted=0"),
    //@NamedQuery(name = "UserDO.findByUserName", query = "SELECT u FROM UserDO u where u.name =:name  and  u.idDeleted=0"),
    @NamedQuery(name = "supplieraccess.findByStatus", query = "SELECT u FROM SupplierAccessDO u where u.status =:status  and  u.isDeleted=0"),
    @NamedQuery(name = "supplieraccess.findByEmailId", query = "SELECT u FROM SupplierAccessDO u where u.contact.email =:email  and  u.isDeleted=0 and u.status = 'a'"),
    @NamedQuery(name = "supplieraccess.findForLogin", query = "SELECT u FROM SupplierAccessDO u where u.contact.email =:email  and  u.password=:password and u.isDeleted=0 and u.status =:status"),
    @NamedQuery(name = "supplieraccess.findBytempPassword", query = "SELECT u FROM SupplierAccessDO u where u.temppassowrd =:temppassword  and  u.isDeleted=0 and u.contact.supplierContactId =:empId"),
    @NamedQuery(name = "supplieraccess.findByEmailForEmp", query = "SELECT u FROM SupplierAccessDO u where (u.contact.email =:personal or u.contact.email =:secondary) and  u.isDeleted=0"),
    @NamedQuery(name = "supplieraccess.findBySupplierId", query = "SELECT u FROM SupplierAccessDO u where u.contact.supplierContactId =:userid and u.isDeleted=0"),
    @NamedQuery(name = "supplieraccess.findSuperAdmin", query = "SELECT u FROM SupplierAccessDO u where u.status =:status  and  u.isDeleted=0 and u.role.roleId =:adminId"),
    @NamedQuery(name = "supplieraccess.findforUpdate", query = "SELECT r FROM SupplierAccessDO r where r.contact.email=:email and r.supplierUserId NOT IN :id"),
    
})
public class SupplierAccessDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_BY_ID = "supplieraccess.findById";
	
	public static final String FIND_BY_EMAIL = "supplieraccess.findByEmailId";
	
	public static final String FIND_BY_EMAIL_FOR_EMP = "supplieraccess.findByEmailForEmp";
	
	//public static final String FIND_BY_NAME = "UserDO.findByName";
	
	public static final String FIND_BY_STATUS = "supplieraccess.findByStatus";
	
	public static final String FIND_BY_USER_NAME = "supplieraccess.findByUserName";
	
	public static final String FIND_FOR_LOGIN = "supplieraccess.findForLogin";
	
	public static final String FIND_BY_TEMPPASSWORD = "supplieraccess.findBytempPassword";
	
	public static final String FIND_BY_SUPPLIER_ID = "supplieraccess.findBySupplierId";
	
	public static final String FIND_SUPERADMIN = "supplieraccess.findSuperAdmin";
	
	public static final String FIND_FOR_UPDATE = "supplieraccess.findforUpdate";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long supplierUserId;
	
	private String temppassowrd;
	private String password;
	private char status;
	private Long isDeleted;

	@ManyToOne
	@JoinColumn(name="supplierContactId")
	private SuppliersContactDO contact;
	
	@ManyToOne
	@JoinColumn(name="roleId")
	private RolesDO role;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	
	public Long getSupplierUserId() {
		return supplierUserId;
	}
	public void setSupplierUserId(Long supplierUserId) {
		this.supplierUserId = supplierUserId;
	}
	public SuppliersContactDO getContact() {
		return contact;
	}
	public void setContact(SuppliersContactDO contact) {
		this.contact = contact;
	}
	public String getTemppassowrd() {
		return temppassowrd;
	}
	public void setTemppassowrd(String temppassowrd) {
		this.temppassowrd = temppassowrd;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public RolesDO getRole() {
		return role;
	}
	public void setRole(RolesDO role) {
		this.role = role;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public Long getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(Long isDeleted) {
		this.isDeleted = isDeleted;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}