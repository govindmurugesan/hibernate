package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_payrollgroupattendanceAllowance")
@TableGenerator(name ="hrms_payrollgroupattendanceAllowance", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "hrms_payrollgroupattendanceAllowance.findById", query = "SELECT r FROM PayrollGroupAttendanceAllowanceDO r where r.payrollGroupAttendanceAllowanceId =:id"),
    @NamedQuery(name = "hrms_payrollgroupattendanceAllowance.findForAdd", query = "SELECT r FROM PayrollGroupAttendanceAllowanceDO r where r.attendanceAllowance.attendanceAllowanceId=:allowenceId and r.status =:status and r.payrollGroup.payrollGroupId=:payrollGroupId"),
    @NamedQuery(name = "hrms_payrollgroupattendanceAllowance.findByGroupId", query = "SELECT r FROM PayrollGroupAttendanceAllowanceDO r where r.payrollGroup.payrollGroupId =:id and r.status =:status"),
})
public class PayrollGroupAttendanceAllowanceDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_BY_ID = "hrms_payrollgroupattendanceAllowance.findById";
	
	public final String FIND_FOR_ADD = "hrms_payrollgroupattendanceAllowance.findForAdd";
	
	public static final String FIND_BY_GROUP_ID = "hrms_payrollgroupattendanceAllowance.findByGroupId";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long payrollGroupAttendanceAllowanceId;
	
	private char status;
	
	@ManyToOne
	@JoinColumn(name="payrollGroupId")
	private PayrollGroupDO payrollGroup;
	
	@ManyToOne
	@JoinColumn(name="attendanceAllowanceId")
	private AttendanceAllowanceDO attendanceAllowance;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
	public Long getPayrollGroupAttendanceAllowanceId() {
		return payrollGroupAttendanceAllowanceId;
	}
	public void setPayrollGroupAttendanceAllowanceId(
			Long payrollGroupAttendanceAllowanceId) {
		this.payrollGroupAttendanceAllowanceId = payrollGroupAttendanceAllowanceId;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public PayrollGroupDO getPayrollGroup() {
		return payrollGroup;
	}
	public void setPayrollGroup(PayrollGroupDO payrollGroup) {
		this.payrollGroup = payrollGroup;
	}
	public AttendanceAllowanceDO getAttendanceAllowance() {
		return attendanceAllowance;
	}
	public void setAttendanceAllowance(AttendanceAllowanceDO attendanceAllowance) {
		this.attendanceAllowance = attendanceAllowance;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
    
	
}