package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_esigroupearnings")
@TableGenerator(name ="hrms_esigroupearnings", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "hrms_esigroupearnings.findById", query = "SELECT r FROM ESIGroupEarningsDO r where r.esiGroupEarningsId =:id"),
    @NamedQuery(name = "hrms_esigroupearnings.findForAdd", query = "SELECT r FROM ESIGroupEarningsDO r where r.allowanceSettings.allowanceSettingsId=:allowenceId and r.status =:status and r.esiGroup.esiGroupId=:esiGroupId"),
    @NamedQuery(name = "hrms_esigroupearnings.findByGroupId", query = "SELECT r FROM ESIGroupEarningsDO r where r.esiGroup.esiGroupId =:id and r.status =:status"),
})
public class ESIGroupEarningsDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_BY_ID = "hrms_esigroupearnings.findById";
	
	public static final String FIND_FOR_ADD = "hrms_esigroupearnings.findForAdd";
	
	public static final String FIND_BY_GROUP_ID = "hrms_esigroupearnings.findByGroupId";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long esiGroupEarningsId;
	
	private char status;
	
	@ManyToOne
	@JoinColumn(name="esiGroupId")
	private ESIGroupDO esiGroup;
	
	@ManyToOne
	@JoinColumn(name="allowanceSettingsId")
	private AllowanceSettingDO allowanceSettings;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
	public Long getEsiGroupEarningsId() {
		return esiGroupEarningsId;
	}
	public void setEsiGroupEarningsId(Long esiGroupEarningsId) {
		this.esiGroupEarningsId = esiGroupEarningsId;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public ESIGroupDO getEsiGroup() {
		return esiGroup;
	}
	public void setEsiGroup(ESIGroupDO esiGroup) {
		this.esiGroup = esiGroup;
	}
	public AllowanceSettingDO getAllowanceSettings() {
		return allowanceSettings;
	}
	public void setAllowanceSettings(AllowanceSettingDO allowanceSettings) {
		this.allowanceSettings = allowanceSettings;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}