package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_master_itslabEffective")
@TableGenerator(name ="hrms_master_itslabEffective", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "hrms_master_itslabEffective.findAll", query = "SELECT r FROM ItSlabEffectiveDO r"),
    @NamedQuery(name = "hrms_master_itslabEffective.findById", query = "SELECT r FROM ItSlabEffectiveDO r where r.itSlabEffectiveId =:id"),
    @NamedQuery(name = "hrms_master_itslabEffective.findByDate", query = "SELECT r FROM ItSlabEffectiveDO r where r.status =:status and (r.effectivefromDate <=:from and r.effectiveToDate >=:to)"),
    @NamedQuery(name = "hrms_master_itslabEffective.findDuplicatePersist", query = "SELECT r FROM ItSlabEffectiveDO r where r.effectivefromDate =:from and r.effectiveToDate =:to"),
    @NamedQuery(name = "hrms_master_itslabEffective.findDuplicateUpdate", query = "SELECT r FROM ItSlabEffectiveDO r where r.effectivefromDate =:from and r.effectiveToDate =:to and r.itSlabEffectiveId <>:id"),
})
public class ItSlabEffectiveDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "hrms_master_itslabEffective.findAll";
	
	public static final String FIND_BY_ID = "hrms_master_itslabEffective.findById";
	
	public static final String FIND_BY_DATE = "hrms_master_itslabEffective.findByDate";

	public static final String FIND_BY_DUPLICATE_PERSIST = "hrms_master_itslabEffective.findDuplicatePersist";
	
	public static final String FIND_BY_DUPLICATE_UPDATE = "hrms_master_itslabEffective.findDuplicateUpdate";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long itSlabEffectiveId;
	
	
	@Temporal(TemporalType.DATE)
	private Date effectivefromDate;
	@Temporal(TemporalType.DATE)
	private Date effectiveToDate;
	private char status;
	
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
	public Long getItSlabEffectiveId() {
		return itSlabEffectiveId;
	}
	public void setItSlabEffectiveId(Long itSlabEffectiveId) {
		this.itSlabEffectiveId = itSlabEffectiveId;
	}
	public Date getEffectivefromDate() {
		return effectivefromDate;
	}
	public void setEffectivefromDate(Date effectivefromDate) {
		this.effectivefromDate = effectivefromDate;
	}
	public Date getEffectiveToDate() {
		return effectiveToDate;
	}
	public void setEffectiveToDate(Date effectiveToDate) {
		this.effectiveToDate = effectiveToDate;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}