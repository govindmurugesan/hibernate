package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="crm_master_industry")
@TableGenerator(name ="crm_master_industry", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "crm_master_industry.findById", query = "SELECT r FROM IndustryDO r where r.industryId =:id"),
    @NamedQuery(name = "crm_master_industry.findByStatus", query = "SELECT r FROM IndustryDO r where r.status =:status"),
    @NamedQuery(name = "crm_master_industry.findByName", query = "SELECT r FROM IndustryDO r where r.name=:name"),
    @NamedQuery(name = "crm_master_industry.findByNameAndId", query = "SELECT r FROM IndustryDO r where r.name=:name and r.industryId NOT IN :id"),
    @NamedQuery(name = "crm_master_industry.findByActiveName", query = "SELECT r FROM IndustryDO r where r.status =:status and r.name=:name"),
})
public class IndustryDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static final String FIND_BY_ID = "crm_master_industry.findById";
	
	public static final String FIND_BY_STATUS = "crm_master_industry.findByStatus";
	
	public static final String FIND_BY_NAME = "crm_master_industry.findByName";
	
	public static final String FIND_FOR_UPDATE = "crm_master_industry.findByNameAndId";
	
	public static final String FIND_BY_ACTIVE_NAME = "crm_master_industry.findByActiveName";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long industryId;
	
	private String name;
	private char status;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getIndustryId() {
		return industryId;
	}
	public void setIndustryId(Long industryId) {
		this.industryId = industryId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}