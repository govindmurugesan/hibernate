package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="employeeeducation")
@TableGenerator(name ="employeeeducation", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "employeeeducation.findById", query = "SELECT r FROM EmployeeEducationDO r where r.empEducationId =:id"),
    @NamedQuery(name = "employeeeducation.findByEmpId", query = "SELECT r FROM EmployeeEducationDO r where r.employee.empId =:empId order by r.empEducationId desc"),
})
public class EmployeeEducationDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static final String FIND_BY_ID = "employeeeducation.findById";
	
	public static final String FIND_BY_EMPID = "employeeeducation.findByEmpId";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long empEducationId;
	
	private String area;
	private String university;
	private String city;
	private String yearPassed;
	
	@ManyToOne
	@JoinColumn(name="empId")
	private EmployeeDO employee;
	
	@ManyToOne
	@JoinColumn(name="countryId")
	private CountryDO country;
	
	@ManyToOne
	@JoinColumn(name="stateId")
	private StateDO state;
	
	@ManyToOne
	@JoinColumn(name="educationLevelId")
	private EducationLevelDO educationLevel;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getEmpEducationId() {
		return empEducationId;
	}
	public void setEmpEducationId(Long empEducationId) {
		this.empEducationId = empEducationId;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getUniversity() {
		return university;
	}
	public void setUniversity(String university) {
		this.university = university;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getYearPassed() {
		return yearPassed;
	}
	public void setYearPassed(String yearPassed) {
		this.yearPassed = yearPassed;
	}
	public EmployeeDO getEmployee() {
		return employee;
	}
	public void setEmployee(EmployeeDO employee) {
		this.employee = employee;
	}
	public CountryDO getCountry() {
		return country;
	}
	public void setCountry(CountryDO country) {
		this.country = country;
	}
	public StateDO getState() {
		return state;
	}
	public void setState(StateDO state) {
		this.state = state;
	}
	public EducationLevelDO getEducationLevel() {
		return educationLevel;
	}
	public void setEducationLevel(EducationLevelDO educationLevel) {
		this.educationLevel = educationLevel;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}