package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_master_leavegroup")
@TableGenerator(name ="hrms_master_leavegroup", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "hrms_master_leavegroup.findById", query = "SELECT r FROM LeaveGroupDO r where r.leaveGroupId =:id"),
    @NamedQuery(name = "hrms_master_leavegroup.findByName", query = "SELECT r FROM LeaveGroupDO r where r.leaveGroup =:name"),
  //  @NamedQuery(name = "hrms_master_leavegroup.findByType", query = "SELECT r FROM LeaveGroupDO r where r.leaveGroup =:type and r.status =:status"),
   // @NamedQuery(name = "hrms_master_leavegroup.findByTypeForUpdate", query = "SELECT r FROM LeaveGroupDO r where r.type =:type and r.status NOT IN :id and r.status =:status"),
    @NamedQuery(name = "hrms_master_leavegroup.findForupdate", query = "SELECT r FROM LeaveGroupDO r where r.leaveGroup=:name and r.leaveGroupId NOT IN :id"),
    @NamedQuery(name = "hrms_master_leavegroup.findByStatus", query = "SELECT r FROM LeaveGroupDO r where r.status =:status"),
})
public class LeaveGroupDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_BY_ID = "hrms_master_leavegroup.findById";
	
	//public static final String FIND_BY_TYPE = "hrms_master_leavegroup.findByType";
	
	//public static final String FIND_BY_TYPE_FOR_UPDATE = "hrms_master_leavegroup.findByTypeForUpdate";
	
	public static final String FIND_BY_NAME = "hrms_master_leavegroup.findByName";
	
	public static final String FIND_FOR_UPDATE = "hrms_master_leavegroup.findForupdate";
	
	public static final String FIND_BY_STATUS = "hrms_master_leavegroup.findByStatus";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long leaveGroupId;
	
	private String leaveGroup;
	private char status;
	private String Description;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getLeaveGroupId() {
		return leaveGroupId;
	}
	public void setLeaveGroupId(Long leaveGroupId) {
		this.leaveGroupId = leaveGroupId;
	}
	public String getLeaveGroup() {
		return leaveGroup;
	}
	public void setLeaveGroup(String leaveGroup) {
		this.leaveGroup = leaveGroup;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}