package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_master_itsection")
@TableGenerator(name ="hrms_master_itsection", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "hrms_master_itsection.findById", query = "SELECT r FROM ItSectionDO r where r.itSectionId =:id"),
    @NamedQuery(name = "hrms_master_itsection.findByStatus", query = "SELECT r FROM ItSectionDO r where r.status =:status"),
    @NamedQuery(name = "hrms_master_itsection.findByName", query = "SELECT r FROM ItSectionDO r where r.section =:name"),
    @NamedQuery(name = "hrms_master_itsection.findDuplicatePersist", query = "SELECT r FROM ItSectionDO r where r.fromYear =:fromDate and r.toYear =:toDate"
    			+ " and r.section =:section"),
    @NamedQuery(name = "hrms_master_itsection.findDuplicateUpdate", query = "SELECT r FROM ItSectionDO r where r.fromYear =:fromDate and r.toYear =:toDate"
			+ " and r.section =:section and r.itSectionId <>:id"),
   // @NamedQuery(name = "hrms_master_itsection.findByDate", query = "SELECT r FROM ItSectionDO r where r.status =:status and (r.fromYear =:fromDate and r.toYear =:toDate)"),
    @NamedQuery(name = "hrms_master_itsection.findByCurrentDate", query = "SELECT r FROM ItSectionDO r where r.status =:status and (r.fromYear <=:date and r.toYear >=:date)"),
})
public class ItSectionDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_BY_ID = "hrms_master_itsection.findById";
	
	public static final String FIND_BY_DUPLICATE_PERSIST = "hrms_master_itsection.findDuplicatePersist";
	
	public static final String FIND_BY_DUPLICATE_UPDATE = "hrms_master_itsection.findDuplicateUpdate";
	
	public static final String FIND_BY_STATUS = "hrms_master_itsection.findByStatus";
	
	public static final String FIND_BY_NAME = "hrms_master_itsection.findByName";
	
	//public static final String FIND_BY_DATE = "hrms_master_itsection.findByDate";
	
	public static final String FIND_BY_CURRENTDATE = "hrms_master_itsection.findByCurrentDate";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long itSectionId;
	
	@Temporal(TemporalType.DATE)
	private Date fromYear;
	  @Temporal(TemporalType.DATE)
	private Date toYear;
	private String section;
	private Long maxLimit;
	private char status;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getItSectionId() {
		return itSectionId;
	}
	public void setItSectionId(Long itSectionId) {
		this.itSectionId = itSectionId;
	}
	public Date getFromYear() {
		return fromYear;
	}
	public void setFromYear(Date fromYear) {
		this.fromYear = fromYear;
	}
	public Date getToYear() {
		return toYear;
	}
	public void setToYear(Date toYear) {
		this.toYear = toYear;
	}
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	public Long getMaxLimit() {
		return maxLimit;
	}
	public void setMaxLimit(Long maxLimit) {
		this.maxLimit = maxLimit;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}