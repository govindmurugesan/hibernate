package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="suppliercontact")
@TableGenerator(name ="suppliercontact", initialValue =100101, allocationSize =1)
@NamedQueries({
    //@NamedQuery(name = "suppliercontact.findAll", query = "SELECT r FROM ContactDO r where r.empId IN :id and r.isDeleted=:status"),
    @NamedQuery(name = "suppliercontact.findById", query = "SELECT r FROM SuppliersContactDO r where r.supplierContactId =:id"),
    @NamedQuery(name = "suppliercontact.findBySupplierId", query = "SELECT r FROM SuppliersContactDO r where r.suppliers.supplierId =:id"),
    @NamedQuery(name = "suppliercontact.findAllData", query = "SELECT r FROM SuppliersContactDO r"),
   @NamedQuery(name = "suppliercontact.findByEmailId", query = "SELECT r FROM SuppliersContactDO r where r.email =:email"),
})
public class SuppliersContactDO implements Serializable {
	private static final long serialVersionUID = 1L;

	//public static final String FIND_ALL = "suppliercontact.findAll";
	
	public static final String FIND_ALL_DATA = "suppliercontact.findAllData";
	
	public static final String FIND_BY_ID = "suppliercontact.findById";
	
	public static final String FIND_BY_EMAIL_ID = "suppliercontact.findByEmailId";
	
	public static final String FIND_BY_SUPPLIERID = "suppliercontact.findBySupplierId";
	
	@Id
	@Column(nullable = false, unique = true)
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long supplierContactId;
	
	@ManyToOne
	@JoinColumn(name="supplierId")
	private SuppliersDO suppliers;
	
	private String fname;
	private String lname;
	private String mname;
	private String designation;
	private String mobile;
	private String email;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
	private String updatedby;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	
	
	public Long getSupplierContactId() {
		return supplierContactId;
	}
	public void setSupplierContactId(Long supplierContactId) {
		this.supplierContactId = supplierContactId;
	}
	public SuppliersDO getSuppliers() {
		return suppliers;
	}
	public void setSuppliers(SuppliersDO suppliers) {
		this.suppliers = suppliers;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public String getMname() {
		return mname;
	}
	public void setMname(String mname) {
		this.mname = mname;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}