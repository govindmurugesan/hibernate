package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="crm_logacivity")
@TableGenerator(name ="crm_logacivity", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "crm_logacivity.findAll", query = "SELECT r FROM LogActivityDO r where r.type =:type"),
    @NamedQuery(name = "crm_logacivity.findById", query = "SELECT r FROM LogActivityDO r where r.id =:id"),
    @NamedQuery(name = "crm_logacivity.findByLeadId", query = "SELECT r FROM LogActivityDO r where r.lead.leadId =:id"),
})
public class LogActivityDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "crm_logacivity.findAll";
	
	public static final String FIND_BY_ID = "crm_logacivity.findById";
	
	public static final String FIND_BY_LEAD_ID = "crm_logacivity.findByLeadId";
	
	@Id
	@Column(nullable = false, unique = true)
	@GeneratedValue(strategy=GenerationType.AUTO) 
	private Long logActivityId;
	
	@ManyToOne
	@JoinColumn(name="leadId")
	private LeadDO lead;
	//private Long leadId;
	private String type;
	private String status;
	private String logDetails;
	private String logDate;
	private String logTime;
	private String updatedBy;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private String empId;

	public Long getLogActivityId() {
		return logActivityId;
	}

	public void setLogActivityId(Long logActivityId) {
		this.logActivityId = logActivityId;
	}

	public LeadDO getLead() {
		return lead;
	}

	public void setLead(LeadDO lead) {
		this.lead = lead;
	}

	public String getCreatedby() {
		return createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	public Date getCreatedon() {
		return createdon;
	}

	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getLogDate() {
		return logDate;
	}

	public void setLogDate(String logDate) {
		this.logDate = logDate;
	}

	public String getLogTime() {
		return logTime;
	}

	public void setLogTime(String logTime) {
		this.logTime = logTime;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedon() {
		return updatedon;
	}

	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}

	public String getLogDetails() {
		return logDetails;
	}

	public void setLogDetails(String logDetails) {
		this.logDetails = logDetails;
	}

	public String getEmpId() {
		return empId;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
}