package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="cms_category")
@TableGenerator(name ="cms_category", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "cms_category.findAll", query = "SELECT r FROM CMSCategoryDO r"),
    @NamedQuery(name = "cms_category.findById", query = "SELECT r FROM CMSCategoryDO r where r.id =:id"),
    @NamedQuery(name = "cms_category.findByStatus", query = "SELECT r FROM CMSCategoryDO r where r.status =:status"),
    @NamedQuery(name = "cms_category.findByName", query = "SELECT r FROM CMSCategoryDO r where r.status =:status and r.name=:name"),
    @NamedQuery(name = "cms_category.findByNameAndId", query = "SELECT r FROM CMSCategoryDO r where r.status =:status and r.name=:name and r.id NOT IN :id"),
})
public class CMSCategoryDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "cms_category.findAll";
	
	public static final String FIND_BY_ID = "cms_category.findById";
	
	public static final String FIND_BY_STATUS = "cms_category.findByStatus";
	
	public static final String FIND_BY_NAME = "cms_category.findByName";
	public static final String FIND_FOR_UPDATE = "cms_category.findByNameAndId";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO) 
	private Long id;
	private String name;
	private String descripetion;
	private char status;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	
	private String updatedBy;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescripetion() {
		return descripetion;
	}

	public void setDescripetion(String descripetion) {
		this.descripetion = descripetion;
	}

	public char getStatus() {
		return status;
	}

	public void setStatus(char status) {
		this.status = status;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedon() {
		return updatedon;
	}

	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	
}