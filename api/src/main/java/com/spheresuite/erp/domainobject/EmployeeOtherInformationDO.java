package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="otherinformation")
@TableGenerator(name ="otherinformation", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "otherinformation.findById", query = "SELECT r FROM EmployeeOtherInformationDO r where r.empAdditionalInfoId =:id"),
    @NamedQuery(name = "otherinformation.findByEmpId", query = "SELECT r FROM EmployeeOtherInformationDO r where r.employee.empId =:empId order by r.empAdditionalInfoId desc"),
})
public class EmployeeOtherInformationDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_BY_ID = "otherinformation.findById";
	
	public static final String FIND_BY_EMPID = "otherinformation.findByEmpId";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long empAdditionalInfoId;
	
	private String voterId;
	private String rationCard;
	private String maritalStatus;
	private String spouseName;
	private Long childern;
	private String bloodGroup;
	
	@ManyToOne
	@JoinColumn(name="empId")
	private EmployeeDO employee;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getEmpAdditionalInfoId() {
		return empAdditionalInfoId;
	}
	public void setEmpAdditionalInfoId(Long empAdditionalInfoId) {
		this.empAdditionalInfoId = empAdditionalInfoId;
	}
	public String getVoterId() {
		return voterId;
	}
	public void setVoterId(String voterId) {
		this.voterId = voterId;
	}
	public String getRationCard() {
		return rationCard;
	}
	public void setRationCard(String rationCard) {
		this.rationCard = rationCard;
	}
	public String getMaritalStatus() {
		return maritalStatus;
	}
	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}
	public String getSpouseName() {
		return spouseName;
	}
	public void setSpouseName(String spouseName) {
		this.spouseName = spouseName;
	}
	public Long getChildern() {
		return childern;
	}
	public void setChildern(Long childern) {
		this.childern = childern;
	}
	public String getBloodGroup() {
		return bloodGroup;
	}
	public void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}
	public EmployeeDO getEmployee() {
		return employee;
	}
	public void setEmployee(EmployeeDO employee) {
		this.employee = employee;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}