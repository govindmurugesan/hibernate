package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="assignedmenu")
@TableGenerator(name ="assignedmenu", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "assignedmenu.findAll", query = "SELECT r FROM AssignedMenuDO r"),
    @NamedQuery(name = "assignedmenu.findById", query = "SELECT r FROM AssignedMenuDO r where r.assignedmenuId =:id"),
    @NamedQuery(name = "assignedmenu.findByroleId", query = "SELECT r FROM AssignedMenuDO r where r.role.roleId =:roleId"),
    @NamedQuery(name = "assignedmenu.findByProductIdRoleId", query = "SELECT r FROM AssignedMenuDO r where r.role.roleId =:roleId and productId=:id"),
    @NamedQuery(name = "assignedmenu.findBySuperMenuRoleId", query = "SELECT r FROM AssignedMenuDO r where r.role.roleId =:roleId and r.menu.supermenu=:id"),
   // @NamedQuery(name = "assignedmenu.findMenuRoleId", query = "SELECT r FROM AssignedMenuDO r where r.role.roleId =:roleId and productId=:id")
})
public class AssignedMenuDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "assignedmenu.findAll";
	
	public static final String FIND_BY_ID = "assignedmenu.findById";
	
	public static final String FIND_BY_ROLE_ID = "assignedmenu.findByroleId";
	
	public static final String FIND_BY_PRODUCTID_ROLEID = "assignedmenu.findByProductIdRoleId";
	
	public static final String FIND_BY_SUPERMENU_ROLEID = "assignedmenu.findBySuperMenuRoleId";
	
	
	
	//public static final String FIND_MENU_ROLEID = "assignedmenu.findMenuRoleId";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long assignedmenuId;
	
	@ManyToOne
	@JoinColumn(name="roleId")
	private RolesDO role;
	
	@ManyToOne
	@JoinColumn(name="menuId")
	private MenuDO menu;
	
	private Long productId;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getAssignedmenuId() {
		return assignedmenuId;
	}
	public void setAssignedmenuId(Long assignedmenuId) {
		this.assignedmenuId = assignedmenuId;
	}
	public RolesDO getRole() {
		return role;
	}
	public void setRole(RolesDO role) {
		this.role = role;
	}
	public MenuDO getMenu() {
		return menu;
	}
	public void setMenu(MenuDO menu) {
		this.menu = menu;
	}
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}