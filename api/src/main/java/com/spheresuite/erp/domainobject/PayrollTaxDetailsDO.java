package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_payrolltaxdetails")
@TableGenerator(name ="hrms_payrolltaxdetails", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "hrms_payrolltaxdetails.findByEmpPayrollId", query = "SELECT r FROM PayrollTaxDetailsDO r where r.payroll.payrollId =:id"),
    @NamedQuery(name = "hrms_payrolltaxdetails.findByEmpPayrollIds", query = "SELECT r FROM PayrollTaxDetailsDO r where r.payroll.payrollId IN :id"),
})
public class PayrollTaxDetailsDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "hrms_payrolltaxdetails.findAll";
	
	public static final String FIND_BY_ID = "hrms_payrolltaxdetails.findByEmpPayrollId";
	
	public static final String FIND_BY_IDS = "hrms_payrolltaxdetails.findByEmpPayrollIds";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO) 
	@Column(nullable = false, unique = true) 
	private Long payrollTaxId;
	
	private Double monthly;
	private Double ytd;
	private String name;
	
	@ManyToOne
	@JoinColumn(name="payrollId")
	private EmployeePayrollDO payroll;
	
	@ManyToOne
	@JoinColumn(name="payrollGroupId")
	private PayrollGroupDO payrollGroup;
	
	@ManyToOne
	@JoinColumn(name="payrollGroupTaxId")
	private PayrollGroupTaxDO payrollGroupTax;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getPayrollTaxId() {
		return payrollTaxId;
	}
	public void setPayrollTaxId(Long payrollTaxId) {
		this.payrollTaxId = payrollTaxId;
	}
	public Double getMonthly() {
		return monthly;
	}
	public void setMonthly(Double monthly) {
		this.monthly = monthly;
	}
	public Double getYtd() {
		return ytd;
	}
	public void setYtd(Double ytd) {
		this.ytd = ytd;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public EmployeePayrollDO getPayroll() {
		return payroll;
	}
	public void setPayroll(EmployeePayrollDO payroll) {
		this.payroll = payroll;
	}
	public PayrollGroupDO getPayrollGroup() {
		return payrollGroup;
	}
	public void setPayrollGroup(PayrollGroupDO payrollGroup) {
		this.payrollGroup = payrollGroup;
	}
	public PayrollGroupTaxDO getPayrollGroupTax() {
		return payrollGroupTax;
	}
	public void setPayrollGroupTax(PayrollGroupTaxDO payrollGroupTax) {
		this.payrollGroupTax = payrollGroupTax;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}