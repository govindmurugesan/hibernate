package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="pl_ocndetails")
@TableGenerator(name ="pl_ocndetails", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "pl_ocndetails.findById", query = "SELECT r FROM OCNDetailsDO r where r.ocnDetailsId =:id"),
    @NamedQuery(name = "pl_ocndetails.findByStatus", query = "SELECT r FROM OCNDetailsDO r where r.status =:status"),
    @NamedQuery(name = "pl_ocndetails.findByOCN", query = "SELECT r FROM OCNDetailsDO r where r.ocn.ocnId=:ocn"),
    @NamedQuery(name = "pl_ocndetails.findByUniqueOCN", query = "SELECT distinct r.ocn FROM OCNDetailsDO r"),
    @NamedQuery(name = "pl_ocndetails.findByOCNAndId", query = "SELECT r FROM OCNDetailsDO r where r.ocn=:ocn and r.ocnDetailsId NOT IN :id"),
})
public class OCNDetailsDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static final String FIND_BY_ID = "pl_ocndetails.findById";
	
	public static final String FIND_BY_STATUS = "pl_ocndetails.findByStatus";
	
	public static final String FIND_BY_OCN = "pl_ocndetails.findByOCN";
	
	public static final String FIND_BY_UNIQUE_OCN = "pl_ocndetails.findByUniqueOCN";
	
	public static final String FIND_FOR_UPDATE = "pl_ocndetails.findByOCNAndId";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long ocnDetailsId;
	
	@ManyToOne
	@JoinColumn(name="ocnId")
	private OCNDO ocn;
	
	private String article;
	private String size;
	private Long qty;
	private Double amount;
	private String status;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getOcnDetailsId() {
		return ocnDetailsId;
	}
	public void setOcnDetailsId(Long ocnDetailsId) {
		this.ocnDetailsId = ocnDetailsId;
	}
	public OCNDO getOcn() {
		return ocn;
	}
	public void setOcn(OCNDO ocn) {
		this.ocn = ocn;
	}
	public String getArticle() {
		return article;
	}
	public void setArticle(String article) {
		this.article = article;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	
	public Long getQty() {
		return qty;
	}
	public void setQty(Long qty) {
		this.qty = qty;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}