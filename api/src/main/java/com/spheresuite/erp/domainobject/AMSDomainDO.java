package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="ams_master_domain")
@TableGenerator(name ="ams_master_domain", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "ams_master_domain.findAll", query = "SELECT r FROM AMSDomainDO r"),
    @NamedQuery(name = "ams_master_domain.findById", query = "SELECT r FROM AMSDomainDO r where r.domainId =:id"),
    @NamedQuery(name = "ams_master_domain.findByStatus", query = "SELECT r FROM AMSDomainDO r where r.status =:status"),
    @NamedQuery(name = "ams_master_domain.findByName", query = "SELECT r FROM AMSDomainDO r where r.status =:status and r.doaminName=:name"),
   @NamedQuery(name = "ams_master_domain.findByNameAndId", query = "SELECT r FROM AMSDomainDO r where r.status =:status and r.doaminName=:name and r.domainId NOT IN :id"),
})
public class AMSDomainDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "ams_master_domain.findAll";
	public static final String FIND_BY_ID = "ams_master_domain.findById";
	public static final String FIND_BY_STATUS = "ams_master_domain.findByStatus";
	public static final String FIND_BY_NAME = "ams_master_domain.findByName";
	public static final String FIND_FOR_UPDATE = "ams_master_domain.findByNameAndId";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO) 
	@Column(nullable = false, unique = true) 
	private Long domainId;
	
	private String doaminName;
	private String doaminComments;
	private char status;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
	public Long getDomainId() {
		return domainId;
	}
	public void setDomainId(Long domainId) {
		this.domainId = domainId;
	}
	public String getDoaminName() {
		return doaminName;
	}
	public void setDoaminName(String doaminName) {
		this.doaminName = doaminName;
	}
	public String getDoaminComments() {
		return doaminComments;
	}
	public void setDoaminComments(String doaminComments) {
		this.doaminComments = doaminComments;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}