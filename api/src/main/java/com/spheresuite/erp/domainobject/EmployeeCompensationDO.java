package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_employeecompensation")
@TableGenerator(name ="hrms_employeecompensation", initialValue =100101, allocationSize =1)
@NamedQueries({
     @NamedQuery(name = "hrms_employeecompensation.findById", query = "SELECT r FROM EmployeeCompensationDO r where r.employeeCompensationId =:id order by r.employeeCompensationId desc"),
    @NamedQuery(name = "hrms_employeecompensation.findByEmpIdStatus", query = "SELECT r FROM EmployeeCompensationDO r where r.status =:status and r.employee.empId =:empId"),
  //  @NamedQuery(name = "hrms_employeecompensation.findByEmpId", query = "SELECT r FROM EmployeeCompensationDO r where r.empId =:empId"),
    @NamedQuery(name = "hrms_employeecompensation.status", query = "SELECT r FROM EmployeeCompensationDO r where r.status =:status order by r.employee.empId desc"),
    @NamedQuery(name = "hrms_employeecompensation.findForPayroll", query = "SELECT r FROM EmployeeCompensationDO r where r.payrollGroup.payrollGroupId =:id and (r.effectivefrom <=:date and r.effectiveto >=:date or r.effectivefrom <=:date and r.effectiveto IS NULL)"),
    @NamedQuery(name = "hrms_employeecompensation.findByEmpIDAndMonth", query = "SELECT r FROM EmployeeCompensationDO r where r.employee.empId =:id and r.status =:status and (r.effectivefrom <=:date and r.effectiveto >=:date or r.effectivefrom <=:date and r.effectiveto IS NULL)"),
   // @NamedQuery(name = "hrms_employeecompensation.findForPayroll", query = "SELECT r FROM EmployeeCompensationDO r where r.payrollGroup.payrollGroupId =:id and (to_char( r.effectivefrom,'YYYY-MM-DD') <=:date and ToChar(r.enddate,'YYYY-MM-DD') >=:date or ToChar(r.effectivefrom,'YYYY-MM-DD')<=:date and ToChar(r.enddate,'YYYY-MM-DD') IS NULL)"),
})
public class EmployeeCompensationDO implements Serializable {
	private static final long serialVersionUID = 1L;

	
	public static final String FIND_BY_ID = "hrms_employeecompensation.findById";
	
	public static final String FIND_BY_EMPID_STATUS = "hrms_employeecompensation.findByEmpIdStatus";
	
	public static final String FIND_BY_STATUS = "hrms_employeecompensation.status";
	
	public static final String FIND_FOR_PAYROLL = "hrms_employeecompensation.findForPayroll";
	
	public static final String FIND_BY_EMPID_MONTH  = "hrms_employeecompensation.findByEmpIDAndMonth";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO) 
	@Column(nullable = false, unique = true) 
	private Long employeeCompensationId;
	
	
	@Temporal(TemporalType.DATE)
	private Date effectivefrom;
	@Temporal(TemporalType.DATE)
	private Date effectiveto;
	private char status;
	
	@ManyToOne
	@JoinColumn(name="empId")
	private EmployeeDO employee;
	
	@ManyToOne
	@JoinColumn(name="payrollGroupId")
	private PayrollGroupDO payrollGroup;
	
	@ManyToOne
	@JoinColumn(name="empCtcId")
	private EmployeeCtcDO empCtc;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
	public Long getEmployeeCompensationId() {
		return employeeCompensationId;
	}
	public void setEmployeeCompensationId(Long employeeCompensationId) {
		this.employeeCompensationId = employeeCompensationId;
	}
	public Date getEffectivefrom() {
		return effectivefrom;
	}
	public void setEffectivefrom(Date effectivefrom) {
		this.effectivefrom = effectivefrom;
	}
	public Date getEffectiveto() {
		return effectiveto;
	}
	public void setEffectiveto(Date effectiveto) {
		this.effectiveto = effectiveto;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public EmployeeDO getEmployee() {
		return employee;
	}
	public void setEmployee(EmployeeDO employee) {
		this.employee = employee;
	}
	public PayrollGroupDO getPayrollGroup() {
		return payrollGroup;
	}
	public void setPayrollGroup(PayrollGroupDO payrollGroup) {
		this.payrollGroup = payrollGroup;
	}
	public EmployeeCtcDO getEmpCtc() {
		return empCtc;
	}
	public void setEmpCtc(EmployeeCtcDO empCtc) {
		this.empCtc = empCtc;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
    
}