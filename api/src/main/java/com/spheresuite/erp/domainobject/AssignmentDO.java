package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="crm_assignment")
@TableGenerator(name ="crm_assignment", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "crm_assignment.findAll", query = "SELECT r FROM AssignmentDO r"),
    @NamedQuery(name = "crm_assignment.findAllData", query = "SELECT r FROM AssignmentDO r"),
    @NamedQuery(name = "crm_assignment.findByStaus", query = "SELECT r FROM AssignmentDO r where r.status =:status"),
    @NamedQuery(name = "crm_assignment.findById", query = "SELECT r FROM AssignmentDO r where r.assignementId =:id"),
    @NamedQuery(name = "crm_assignment.findByEmId", query = "SELECT r FROM AssignmentDO r where r.empId =:id and r.status =:status"),
    @NamedQuery(name = "crm_assignment.findByOppId", query = "SELECT r FROM AssignmentDO r where r.opportunity.opportunityId =:id"),
})
public class AssignmentDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "crm_assignment.findAll";
	public static final String FIND_BY_ID = "crm_assignment.findById";
	public static final String FIND_BY_EMPID = "crm_assignment.findByEmId";
	public static final String FIND_BY_STATUS = "crm_assignment.findByStaus";
	public static final String FIND_BY_OPP_ID = "crm_assignment.findByOppId";
	public static final String FIND_ALL_DATA = "crm_assignment.findAllData";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long assignementId;
	
	//private Long opportunityId;
	@ManyToOne
	@JoinColumn(name="opportunityId")
	private OpportunitiesDO opportunity;
	
	@ManyToOne
	@JoinColumn(name="projectId")
	private ProjectDO project;
	//private Long projectId;
	
	@ManyToOne
	@JoinColumn(name="leadId")
	private LeadDO customer;
	//private Long customerId;
	private String projectType;
	private String startdate;
	private String enddate;
	private String orderNumber;
	private Long amountType;
	private Long amount;
	@ManyToOne
	@JoinColumn(name="paymentTermId")
	private PaymentTermDO paymentTerms;
	//private Long paymentTerms;
	private String status;
	private String comments;
	private String updatedby;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
	private String empId;
	
	
	public Long getAssignementId() {
		return assignementId;
	}
	public void setAssignementId(Long assignementId) {
		this.assignementId = assignementId;
	}
	public OpportunitiesDO getOpportunity() {
		return opportunity;
	}
	public void setOpportunity(OpportunitiesDO opportunity) {
		this.opportunity = opportunity;
	}
	public ProjectDO getProject() {
		return project;
	}
	public void setProject(ProjectDO project) {
		this.project = project;
	}
	public LeadDO getCustomer() {
		return customer;
	}
	public void setCustomer(LeadDO customer) {
		this.customer = customer;
	}
	public PaymentTermDO getPaymentTerms() {
		return paymentTerms;
	}
	public void setPaymentTerms(PaymentTermDO paymentTerms) {
		this.paymentTerms = paymentTerms;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getProjectType() {
		return projectType;
	}
	public void setProjectType(String projectType) {
		this.projectType = projectType;
	}
	public String getStartdate() {
		return startdate;
	}
	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}
	public String getEnddate() {
		return enddate;
	}
	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public Long getAmountType() {
		return amountType;
	}
	public void setAmountType(Long amountType) {
		this.amountType = amountType;
	}
	public Long getAmount() {
		return amount;
	}
	public void setAmount(Long amount) {
		this.amount = amount;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public String getEmpId() {
		return empId;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
	
	
}