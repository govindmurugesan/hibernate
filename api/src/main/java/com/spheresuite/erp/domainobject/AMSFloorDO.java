package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="ams_master_floor")
@TableGenerator(name ="ams_master_floor", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "ams_master_floor.findAll", query = "SELECT r FROM AMSFloorDO r"),
    @NamedQuery(name = "ams_master_floor.findById", query = "SELECT r FROM AMSFloorDO r where r.floorId =:id"),
    @NamedQuery(name = "ams_master_floor.findByStatus", query = "SELECT r FROM AMSFloorDO r where r.status =:status"),
    @NamedQuery(name = "ams_master_floor.findByName", query = "SELECT r FROM AMSFloorDO r where r.status =:status and r.floorName=:name"),
   @NamedQuery(name = "ams_master_floor.findByNameAndId", query = "SELECT r FROM AMSFloorDO r where r.status =:status and r.floorName=:name and r.floorId NOT IN :id"),
})
public class AMSFloorDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "ams_master_floor.findAll";
	public static final String FIND_BY_ID = "ams_master_floor.findById";
	public static final String FIND_BY_STATUS = "ams_master_floor.findByStatus";
	public static final String FIND_BY_NAME = "ams_master_floor.findByName";
	public static final String FIND_FOR_UPDATE = "ams_master_floor.findByNameAndId";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO) 
	@Column(nullable = false, unique = true)  
	private Long floorId;
	
	private String floorName;
	private String floorComments;
	private char status;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getFloorId() {
		return floorId;
	}
	public void setFloorId(Long floorId) {
		this.floorId = floorId;
	}
	public String getFloorName() {
		return floorName;
	}
	public void setFloorName(String floorName) {
		this.floorName = floorName;
	}
	public String getFloorComments() {
		return floorComments;
	}
	public void setFloorComments(String floorComments) {
		this.floorComments = floorComments;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}