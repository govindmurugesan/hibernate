package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_employeeexpense")
@TableGenerator(name ="hrms_employeeexpense", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "hrms_employeeexpense.findAll", query = "SELECT r FROM EmployeeExpenseDO r"),
    @NamedQuery(name = "hrms_employeeexpense.findById", query = "SELECT r FROM EmployeeExpenseDO r where r.empExpenseId =:id"),
    @NamedQuery(name = "hrms_employeeexpense.findByEmpId", query = "SELECT r FROM EmployeeExpenseDO r where r.employee.empId =:empId order by r.empExpenseId desc"),
    @NamedQuery(name = "hrms_employeeexpense.findByReprtingId", query = "SELECT r FROM EmployeeExpenseDO r where r.employee.reportto =:id"),
    @NamedQuery(name = "hrms_employeeexpense.findByEmpIDAndMonth", query = "SELECT r FROM EmployeeExpenseDO r where r.employee.empId =:id and r.status =:status and ((r.fromDate >=:fromDate and  r.toDate <=:toDate) or ((r.fromDate >=:fromDate and r.toDate IS NULL) and (r.fromDate <=:toDate)))"),
})
public class EmployeeExpenseDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "hrms_employeeexpense.findAll";
	
	public static final String FIND_BY_ID = "hrms_employeeexpense.findById";
	
	public static final String FIND_BY_EMPID = "hrms_employeeexpense.findByEmpId";
	
	public static final String FIND_BY_REPORTINGID = "hrms_employeeexpense.findByReprtingId";
	
	public static final String FIND_BY_EMPID_MONTH = "hrms_employeeexpense.findByEmpIDAndMonth";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO) 
	private Long empExpenseId;
	
	private String description;
	private String status;
	private Long amount;
	
	@Temporal(TemporalType.DATE)
    private Date paidon;
	
	@Temporal(TemporalType.DATE)
    private Date fromDate;
	
	@Temporal(TemporalType.DATE)
    private Date toDate;
	
	@ManyToOne
	@JoinColumn(name="empId")
	private EmployeeDO employee;
	
	@ManyToOne
	@JoinColumn(name="expenseTypeId")
	private ExpenseTypeDO expenseType;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
	public Long getEmpExpenseId() {
		return empExpenseId;
	}
	public void setEmpExpenseId(Long empExpenseId) {
		this.empExpenseId = empExpenseId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Long getAmount() {
		return amount;
	}
	public void setAmount(Long amount) {
		this.amount = amount;
	}
	public Date getPaidon() {
		return paidon;
	}
	public void setPaidon(Date paidon) {
		this.paidon = paidon;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public EmployeeDO getEmployee() {
		return employee;
	}
	public void setEmployee(EmployeeDO employee) {
		this.employee = employee;
	}
	public ExpenseTypeDO getExpenseType() {
		return expenseType;
	}
	public void setExpenseType(ExpenseTypeDO expenseType) {
		this.expenseType = expenseType;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
    
}