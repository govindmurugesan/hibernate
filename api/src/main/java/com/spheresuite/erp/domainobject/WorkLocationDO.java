package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="master_worklocation")
@TableGenerator(name ="master_worklocation", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "master_worklocation.findAll", query = "SELECT r FROM WorkLocationDO r"),
    @NamedQuery(name = "master_worklocation.findById", query = "SELECT r FROM WorkLocationDO r where r.worklocationId =:id"),
    @NamedQuery(name = "master_worklocation.findByStateId", query = "SELECT r FROM WorkLocationDO r where r.state.stateId =:id"),
    @NamedQuery(name = "master_worklocation.findByStatus", query = "SELECT r FROM WorkLocationDO r where r.status =:status"),
    @NamedQuery(name = "master_worklocation.findByName", query = "SELECT r FROM WorkLocationDO r where r.worklocation=:name"),
    @NamedQuery(name = "master_worklocation.findByNameAll", query = "SELECT r FROM WorkLocationDO r where r.worklocation=:name"),
    @NamedQuery(name = "master_worklocation.findByNameAndId", query = "SELECT r FROM WorkLocationDO r where r.worklocation=:name and r.worklocationId NOT IN :id"),
})
public class WorkLocationDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "master_worklocation.findAll";
	
	public static final String FIND_BY_ID = "master_worklocation.findById";
	
	public static final String FIND_BY_STATE_ID = "master_worklocation.findByStateId";
	
	public static final String FIND_BY_STATUS = "master_worklocation.findByStatus";
	
	public static final String FIND_BY_NAME = "master_worklocation.findByName";
	
	public static final String FIND_BY_CITY_ALL = "master_worklocation.findByNameAll";
	
	public static final String FIND_FOR_UPDATE = "master_worklocation.findByNameAndId";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long worklocationId;
	
	private String worklocation;
	private String address;
	private char status;
	
	@ManyToOne
	@JoinColumn(name="countryId")
	private CountryDO country;
	
	@ManyToOne
	@JoinColumn(name="stateId")
	private StateDO state;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getWorklocationId() {
		return worklocationId;
	}
	public void setWorklocationId(Long worklocationId) {
		this.worklocationId = worklocationId;
	}
	public String getWorklocation() {
		return worklocation;
	}
	public void setWorklocation(String worklocation) {
		this.worklocation = worklocation;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public CountryDO getCountry() {
		return country;
	}
	public void setCountry(CountryDO country) {
		this.country = country;
	}
	public StateDO getState() {
		return state;
	}
	public void setState(StateDO state) {
		this.state = state;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}