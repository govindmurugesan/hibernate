package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="ams_master_stock")
@TableGenerator(name ="ams_master_stock", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "ams_master_stock.findAll", query = "SELECT r FROM AMSStockDO r"),
    @NamedQuery(name = "ams_master_stock.findById", query = "SELECT r FROM AMSStockDO r where r.stockId =:id"),
    @NamedQuery(name = "ams_master_stock.findByStatus", query = "SELECT r FROM AMSStockDO r where r.status =:status"),
    @NamedQuery(name = "ams_master_stock.findByName", query = "SELECT r FROM AMSStockDO r where r.status =:status and r.stock_name=:name"),
    @NamedQuery(name = "ams_master_stock.findByNameAndId", query = "SELECT r FROM AMSStockDO r where r.status =:status and r.stock_name=:name and r.stockId NOT IN :id"),
    
})
public class AMSStockDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "ams_master_stock.findAll";
	
	public static final String FIND_BY_ID = "ams_master_stock.findById";
	
	public static final String FIND_BY_STATUS = "ams_master_stock.findByStatus";
	
	public static final String FIND_IS_MANAGER = "ams_master_stock.findisManager";
	
	public static final String FIND_BY_NAME = "ams_master_stock.findByName";
	
	public static final String FIND_FOR_UPDATE = "ams_master_stock.findByNameAndId";
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO) 
	@Column(nullable = false, unique = true) 
	private Long stockId;
	
	private String stock_name;
	private char status;
	private String stock_comments;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
	public Long getStockId() {
		return stockId;
	}
	public void setStockId(Long stockId) {
		this.stockId = stockId;
	}
	public String getStock_name() {
		return stock_name;
	}
	public void setStock_name(String stock_name) {
		this.stock_name = stock_name;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getStock_comments() {
		return stock_comments;
	}
	public void setStock_comments(String stock_comments) {
		this.stock_comments = stock_comments;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}