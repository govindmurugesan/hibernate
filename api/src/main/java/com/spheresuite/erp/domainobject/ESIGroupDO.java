package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_esigroup")
@TableGenerator(name ="hrms_esigroup", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "hrms_esigroup.findById", query = "SELECT r FROM ESIGroupDO r where r.esiGroupId =:id"),
    @NamedQuery(name = "hrms_esigroup.findByStatus", query = "SELECT r FROM ESIGroupDO r where r.status =:status"),
    @NamedQuery(name = "hrms_esigroup.findByName", query = "SELECT r FROM ESIGroupDO r where r.name=:name"),
    @NamedQuery(name = "hrms_esigroup.findByNameAndId", query = "SELECT r FROM ESIGroupDO r where r.name=:name and r.esiGroupId NOT IN :id"),
})
public class ESIGroupDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_BY_ID = "hrms_esigroup.findById";
	
	public static final String FIND_BY_STATUS = "hrms_esigroup.findByStatus";
	
	public static final String FIND_BY_NAME = "hrms_esigroup.findByName";
	
	public static final String FIND_FOR_UPDATE = "hrms_esigroup.findByNameAndId";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long esiGroupId;
	
	private String name;
	private String description;
	private char status;
	private Double amount;
	
	@OneToMany(mappedBy="esiGroup", fetch = FetchType.EAGER)
    private Set<ESIGroupEarningsDO> esiGroupEarnings;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
    
	public Long getEsiGroupId() {
		return esiGroupId;
	}
	public void setEsiGroupId(Long esiGroupId) {
		this.esiGroupId = esiGroupId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Set<ESIGroupEarningsDO> getEsiGroupEarnings() {
		return esiGroupEarnings;
	}
	public void setEsiGroupEarnings(Set<ESIGroupEarningsDO> esiGroupEarnings) {
		this.esiGroupEarnings = esiGroupEarnings;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}