package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="ams_master_cputype")
@TableGenerator(name ="ams_master_cputype", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "ams_master_cputype.findAll", query = "SELECT r FROM AMSCPUTypeDO r"),
    @NamedQuery(name = "ams_master_cputype.findById", query = "SELECT r FROM AMSCPUTypeDO r where r.cpuTypeId =:id"),
    @NamedQuery(name = "ams_master_cputype.findByStatus", query = "SELECT r FROM AMSCPUTypeDO r where r.status =:status"),
    @NamedQuery(name = "ams_master_cputype.findByName", query = "SELECT r FROM AMSCPUTypeDO r where r.status =:status and r.cputype_name=:name"),
    @NamedQuery(name = "ams_master_cputype.findByNameAndId", query = "SELECT r FROM AMSCPUTypeDO r where r.status =:status and r.cputype_name=:name and r.cpuTypeId NOT IN :id"),
    
})
public class AMSCPUTypeDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "ams_master_cputype.findAll";
	
	public static final String FIND_BY_ID = "ams_master_cputype.findById";
	
	public static final String FIND_BY_STATUS = "ams_master_cputype.findByStatus";
	
	public static final String FIND_IS_MANAGER = "ams_master_cputype.findisManager";
	
	public static final String FIND_BY_NAME = "ams_master_cputype.findByName";
	
	public static final String FIND_FOR_UPDATE = "ams_master_cputype.findByNameAndId";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO) 
	@Column(nullable = false, unique = true)
	private Long cpuTypeId;
	
	private String cputype_name;
	private char status;
	private String cputype_comments;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getCpuTypeId() {
		return cpuTypeId;
	}
	public void setCpuTypeId(Long cpuTypeId) {
		this.cpuTypeId = cpuTypeId;
	}
	public String getCputype_name() {
		return cputype_name;
	}
	public void setCputype_name(String cputype_name) {
		this.cputype_name = cputype_name;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getCputype_comments() {
		return cputype_comments;
	}
	public void setCputype_comments(String cputype_comments) {
		this.cputype_comments = cputype_comments;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}