package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_payrollgroupworkingdayAllowance")
@TableGenerator(name ="hrms_payrollgroupworkingdayAllowance", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "hrms_payrollgroupworkingdayAllowance.findById", query = "SELECT r FROM PayrollGroupWorkingdaysAllowanceDO r where r.payrollGroupWorkingDaysId =:id"),
    @NamedQuery(name = "hrms_payrollgroupworkingdayAllowance.findForAdd", query = "SELECT r FROM PayrollGroupWorkingdaysAllowanceDO r where r.workingDayAllowance.workingDayAllowanceId=:allowenceId and r.status =:status and r.payrollGroup.payrollGroupId=:payrollGroupId"),
    @NamedQuery(name = "hrms_payrollgroupworkingdayAllowance.findByGroupId", query = "SELECT r FROM PayrollGroupWorkingdaysAllowanceDO r where r.payrollGroup.payrollGroupId =:id and r.status =:status"),
})
public class PayrollGroupWorkingdaysAllowanceDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_BY_ID = "hrms_payrollgroupworkingdayAllowance.findById";
	
	public static final String FIND_FOR_ADD = "hrms_payrollgroupworkingdayAllowance.findForAdd";
	
	public static final String FIND_BY_GROUP_ID = "hrms_payrollgroupworkingdayAllowance.findByGroupId";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long payrollGroupWorkingDaysId;
	
	private char status;
	
	@ManyToOne
	@JoinColumn(name="payrollGroupId")
	private PayrollGroupDO payrollGroup;
	
	@ManyToOne
	@JoinColumn(name="workingDayAllowanceId")
	private WorkingDayAllowanceDO workingDayAllowance;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
	public Long getPayrollGroupWorkingDaysId() {
		return payrollGroupWorkingDaysId;
	}
	public void setPayrollGroupWorkingDaysId(Long payrollGroupWorkingDaysId) {
		this.payrollGroupWorkingDaysId = payrollGroupWorkingDaysId;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public PayrollGroupDO getPayrollGroup() {
		return payrollGroup;
	}
	public void setPayrollGroup(PayrollGroupDO payrollGroup) {
		this.payrollGroup = payrollGroup;
	}
	public WorkingDayAllowanceDO getWorkingDayAllowance() {
		return workingDayAllowance;
	}
	public void setWorkingDayAllowance(WorkingDayAllowanceDO workingDayAllowance) {
		this.workingDayAllowance = workingDayAllowance;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
    
}