package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="employeedriverlicense")
@TableGenerator(name ="employeedriverlicense", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "employeedriverlicense.findById", query = "SELECT r FROM EmployeeDriverLicenseDO r where r.empDriverLicenseId =:id"),
    @NamedQuery(name = "employeedriverlicense.findByEmpId", query = "SELECT r FROM EmployeeDriverLicenseDO r where r.employee.empId =:empId"),
})
public class EmployeeDriverLicenseDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_BY_ID = "employeedriverlicense.findById";
	public static final String FIND_BY_EMPID = "employeedriverlicense.findByEmpId";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true) 
	private Long empDriverLicenseId;
	
	private String city;
	private String licenseNumber;
	private String name;
	private String dateIssued;
	private String expiryDate;
	private String address1;
	private String address2;
	private Long zip;
	
	@ManyToOne
	@JoinColumn(name="empId")
	private EmployeeDO employee;
	
	@ManyToOne
	@JoinColumn(name="countryId")
	private CountryDO country;
	
	@ManyToOne
	@JoinColumn(name="stateId")
	private StateDO state;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getEmpDriverLicenseId() {
		return empDriverLicenseId;
	}
	public void setEmpDriverLicenseId(Long empDriverLicenseId) {
		this.empDriverLicenseId = empDriverLicenseId;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getLicenseNumber() {
		return licenseNumber;
	}
	public void setLicenseNumber(String licenseNumber) {
		this.licenseNumber = licenseNumber;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDateIssued() {
		return dateIssued;
	}
	public void setDateIssued(String dateIssued) {
		this.dateIssued = dateIssued;
	}
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public Long getZip() {
		return zip;
	}
	public void setZip(Long zip) {
		this.zip = zip;
	}
	public EmployeeDO getEmployee() {
		return employee;
	}
	public void setEmployee(EmployeeDO employee) {
		this.employee = employee;
	}
	public CountryDO getCountry() {
		return country;
	}
	public void setCountry(CountryDO country) {
		this.country = country;
	}
	public StateDO getState() {
		return state;
	}
	public void setState(StateDO state) {
		this.state = state;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}