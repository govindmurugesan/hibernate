package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="cms_department")
@TableGenerator(name ="cms_department", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "cms_department.findAll", query = "SELECT r FROM CMSDepartmentDO r"),
    @NamedQuery(name = "cms_department.findById", query = "SELECT r FROM CMSDepartmentDO r where r.id =:id"),
    @NamedQuery(name = "cms_department.findByStatus", query = "SELECT r FROM CMSDepartmentDO r where r.status =:status"),
    @NamedQuery(name = "cms_department.findByName", query = "SELECT r FROM CMSDepartmentDO r where r.status =:status and r.name=:name"),
    @NamedQuery(name = "cms_department.findByNameAndId", query = "SELECT r FROM CMSDepartmentDO r where r.status =:status and r.name=:name and r.id NOT IN :id"),
    @NamedQuery(name = "cms_department.findByNameAll", query = "SELECT r FROM CMSDepartmentDO r where r.name=:name"),
    
})
public class CMSDepartmentDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "cms_department.findAll";
	
	public static final String FIND_BY_ID = "cms_department.findById";
	
	public static final String FIND_BY_STATUS = "cms_department.findByStatus";
	
	public static final String FIND_IS_MANAGER = "cms_department.findisManager";
	
	public static final String FIND_BY_NAME = "cms_department.findByName";
	
	public static final String FIND_FOR_UPDATE = "cms_department.findByNameAndId";
	
	public static final String FIND_BY_NAME_ALL = "cms_department.findByNameAll";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO) 
	private Long id;
	private String name;
	private char status;
	private String updatedby;
	private String comments;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public char getStatus() {
		return status;
	}

	public void setStatus(char status) {
		this.status = status;
	}

	public String getUpdatedby() {
		return updatedby;
	}

	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}

	public Date getUpdatedon() {
		return updatedon;
	}

	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}
	

}