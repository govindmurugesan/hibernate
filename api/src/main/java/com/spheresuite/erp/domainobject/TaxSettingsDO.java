package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name="hrms_taxsetting")
@TableGenerator(name ="hrms_taxsetting", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "hrms_taxsetting.findAll", query = "SELECT r FROM TaxSettingsDO r"),
    @NamedQuery(name = "hrms_taxsetting.findById", query = "SELECT r FROM TaxSettingsDO r where r.taxSettingId =:id"),
    @NamedQuery(name = "hrms_taxsetting.findActive", query = "SELECT r FROM TaxSettingsDO r where r.status =:status and r.name =:name"),
})
public class TaxSettingsDO implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final String FIND_ALL = "hrms_taxsetting.findAll";
	public static final String FIND_BY_ID = "hrms_taxsetting.findById";
	public static final String FIND_ACTIVE = "hrms_taxsetting.findActive";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO) 
	private Long taxSettingId;
	
	private String name;
	private String section;
	private String updatedBy;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private String createdBy;
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdOn;
	private String preposttax;
	private String type;
	private String fixedamount;
	private String percentageamount;
	private String basicgrosspay;
	private String displayname;
	private String notes;
	@Temporal(TemporalType.DATE)
	private Date startdate;
	@Temporal(TemporalType.DATE)
	private Date enddate;
	private char status;
	@Transient
	private String settingName;
	
	public Long getTaxSettingId() {
		return taxSettingId;
	}
	public void setTaxSettingId(Long taxSettingId) {
		this.taxSettingId = taxSettingId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public String getPreposttax() {
		return preposttax;
	}
	public void setPreposttax(String preposttax) {
		this.preposttax = preposttax;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getFixedamount() {
		return fixedamount;
	}
	public void setFixedamount(String fixedamount) {
		this.fixedamount = fixedamount;
	}
	public String getPercentageamount() {
		return percentageamount;
	}
	public void setPercentageamount(String percentageamount) {
		this.percentageamount = percentageamount;
	}
	public String getBasicgrosspay() {
		return basicgrosspay;
	}
	public void setBasicgrosspay(String basicgrosspay) {
		this.basicgrosspay = basicgrosspay;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public String getDisplayname() {
		return displayname;
	}
	public void setDisplayname(String displayname) {
		this.displayname = displayname;
	}
	public Date getStartdate() {
		return startdate;
	}
	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}
	public Date getEnddate() {
		return enddate;
	}
	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getSettingName() {
		return settingName;
	}
	public void setSettingName(String settingName) {
		this.settingName = settingName;
	}
}