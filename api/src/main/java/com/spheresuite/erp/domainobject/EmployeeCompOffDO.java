package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_employee_compoff")
@TableGenerator(name ="hrms_employee_compoff", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "hrms_employee_compoff.findById", query = "SELECT r FROM EmployeeCompOffDO r where r.empCompOffId =:id"),
    @NamedQuery(name = "hrms_employee_compoff.findByEmpId", query = "SELECT r FROM EmployeeCompOffDO r where r.employee.empId =:id"),
    @NamedQuery(name = "hrms_employee_compoff.findByReprtingId", query = "SELECT r FROM EmployeeCompOffDO r where r.employee.reportto =:id"),
    @NamedQuery(name = "hrms_employee_compoff.findAll", query = "SELECT r FROM EmployeeCompOffDO r ORDER BY r.empCompOffId DESC"),
    @NamedQuery(name = "hrms_employee_compoff.findByEmpIdValidDate", query = "SELECT r FROM EmployeeCompOffDO r where r.status=:status and (r.expiryDate>=:expiryDate or r.expiryDate IS NULL) and r.employee.empId =:id"),
    @NamedQuery(name = "hrms_employee_compoff.findByEmpIdValidDateAndType", query = "SELECT r FROM EmployeeCompOffDO r where r.status=:status and r.compOffType=:type and r.applyDate <=:expiryDate  and(r.expiryDate>=:expiryDate or r.expiryDate IS NULL) and r.employee.empId =:id"),
    @NamedQuery(name = "hrms_employee_compoff.findByEmpIdExpDateAndType", query = "SELECT r FROM EmployeeCompOffDO r where r.status=:status and r.compOffType=:type and r.employee.empId =:id  and (r.expiryDate>=:expiryDate or r.expiryDate IS NULL) ORDER BY expiryDate asc"),
    
})
public class EmployeeCompOffDO implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final String FIND_BY_ID = "hrms_employee_compoff.findById";
	public static final String FIND_BY_EMPID = "hrms_employee_compoff.findByEmpId";
	public static final String FIND_BY_EMPID_VALIDDATE = "hrms_employee_compoff.findByEmpIdValidDate";
	public static final String FIND_BY_EMPID_VALIDDATEANDTYPE = "hrms_employee_compoff.findByEmpIdValidDateAndType";
	public static final String FIND_BY_EMPID_EXPITYDATE_TYPE = "hrms_employee_compoff.findByEmpIdExpDateAndType";
	public static final String FIND_BY_REPORTINGID = "hrms_employee_compoff.findByReprtingId";
	public static final String FIND_ALL = "hrms_employee_compoff.findAll";
	
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long empCompOffId;
	@Temporal(TemporalType.TIMESTAMP)
    private Date applyDate;
	@Temporal(TemporalType.TIMESTAMP)
    private Date expiryDate;
	@ManyToOne
	@JoinColumn(name="empId")
	private EmployeeDO employee;
	private char compOffType;
	private char status;
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    
    private String comments;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
	
    public Long getEmpCompOffId() {
		return empCompOffId;
	}
	public void setEmpCompOffId(Long empCompOffId) {
		this.empCompOffId = empCompOffId;
	}
	public Date getApplyDate() {
		return applyDate;
	}
	public void setApplyDate(Date applyDate) {
		this.applyDate = applyDate;
	}
	public Date getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	public EmployeeDO getEmployee() {
		return employee;
	}
	public void setEmployee(EmployeeDO employee) {
		this.employee = employee;
	}
	public char getCompOffType() {
		return compOffType;
	}
	public void setCompOffType(char compOffType) {
		this.compOffType = compOffType;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
}