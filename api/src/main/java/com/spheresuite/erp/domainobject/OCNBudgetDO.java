package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="pl_ocnbudget")
@TableGenerator(name ="pl_ocnbudget", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "pl_ocnbudget.findById", query = "SELECT r FROM OCNBudgetDO r where r.ocnBudgetId =:id"),
    @NamedQuery(name = "pl_ocnbudget.findByStatus", query = "SELECT r FROM OCNBudgetDO r where r.status =:status"),
    @NamedQuery(name = "pl_ocnbudget.findByOCN", query = "SELECT r FROM OCNBudgetDO r where r.ocnDetails.ocnId=:ocn"),
    @NamedQuery(name = "pl_ocnbudget.findByOCNAndId", query = "SELECT r FROM OCNBudgetDO r where r.ocnDetails.ocnId=:ocn and r.ocnBudgetId NOT IN :id"),
})
public class OCNBudgetDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static final String FIND_BY_ID = "pl_ocnbudget.findById";
	
	public static final String FIND_BY_STATUS = "pl_ocnbudget.findByStatus";
	
	public static final String FIND_BY_OCN = "pl_ocnbudget.findByOCN";
	
	public static final String FIND_FOR_UPDATE = "pl_ocnbudget.findByOCNAndId";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long ocnBudgetId;
	
	@ManyToOne
	@JoinColumn(name="ocnId")
	private OCNDO ocnDetails;
	
	@ManyToOne
	@JoinColumn(name="cpmId")
	private CPMDO cpm;
	
	private Double greigeFabric;
	private Double greigeFabricm;
	private Double dyeingCost;
	private Double finishingCost;
	private Double trims;
	private Double sam;
	private Double rejection;
	private String exportIncentive;
	private String status;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getOcnBudgetId() {
		return ocnBudgetId;
	}
	public void setOcnBudgetId(Long ocnBudgetId) {
		this.ocnBudgetId = ocnBudgetId;
	}
	public OCNDO getOcnDetails() {
		return ocnDetails;
	}
	public void setOcnDetails(OCNDO ocnDetails) {
		this.ocnDetails = ocnDetails;
	}
	public CPMDO getCpm() {
		return cpm;
	}
	public void setCpm(CPMDO cpm) {
		this.cpm = cpm;
	}
	public Double getGreigeFabricm() {
		return greigeFabricm;
	}
	public void setGreigeFabricm(Double greigeFabricm) {
		this.greigeFabricm = greigeFabricm;
	}
	public Double getGreigeFabric() {
		return greigeFabric;
	}
	public void setGreigeFabric(Double greigeFabric) {
		this.greigeFabric = greigeFabric;
	}
	public Double getDyeingCost() {
		return dyeingCost;
	}
	public void setDyeingCost(Double dyeingCost) {
		this.dyeingCost = dyeingCost;
	}
	public Double getFinishingCost() {
		return finishingCost;
	}
	public void setFinishingCost(Double finishingCost) {
		this.finishingCost = finishingCost;
	}
	public Double getTrims() {
		return trims;
	}
	public void setTrims(Double trims) {
		this.trims = trims;
	}
	public Double getSam() {
		return sam;
	}
	public void setSam(Double sam) {
		this.sam = sam;
	}
	public Double getRejection() {
		return rejection;
	}
	public void setRejection(Double rejection) {
		this.rejection = rejection;
	}
	public String getExportIncentive() {
		return exportIncentive;
	}
	public void setExportIncentive(String exportIncentive) {
		this.exportIncentive = exportIncentive;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}