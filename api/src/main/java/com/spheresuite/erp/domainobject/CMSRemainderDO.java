package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="cms_remainder")
@TableGenerator(name ="cms_remainder", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "cms_remainder.findAll", query = "SELECT r FROM CMSRemainderDO r"),
    @NamedQuery(name = "cms_remainder.findById", query = "SELECT r FROM CMSRemainderDO r where r.id =:id"),
    @NamedQuery(name = "cms_remainder.findByTodayDate", query = "SELECT r FROM CMSRemainderDO r where r.validUpto =:CURRENT_DATE"),
})
public class CMSRemainderDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "cms_remainder.findAll";
	
	public static final String FIND_BY_ID = "cms_remainder.findById";
	
	public static final String FIND_BY_TODAY_DATE = "cms_remainder.findByTodayDate";
	
	//public static final String FIND_BY_STATUS = "remainder.findByStatus";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO) 
	private Long id;
	
	private Long departmentId;
	private Long categoryId;
	/*private Long subCategoryId;*/
	private Long frequencyId;
	private String description;
	private String issuedBy;
	
	//private Date validUpto;
	@Temporal(TemporalType.TIMESTAMP)
	private Date validUpto;
	/*private String validUptoTime;*/
	
	
	private Long leadTime;
	
	/*@Temporal(TemporalType.TIMESTAMP)
	private Date nextRenewal;*/
	private String noOfAlerts;
	private String email;
	private Long recurrence;
	private String comments;

	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private String updatedBy;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getDepartmentId() {
		return departmentId;
	}
	public void setDepartmentId(Long departmentId) {
		this.departmentId = departmentId;
	}
	public Long getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}
	
	public Long getFrequencyId() {
		return frequencyId;
	}
	public void setFrequencyId(Long frequencyId) {
		this.frequencyId = frequencyId;
	}
	public String getIssuedBy() {
		return issuedBy;
	}
	public void setIssuedBy(String issuedBy) {
		this.issuedBy = issuedBy;
	}
	public Date getValidUpto() {
		return validUpto;
	}
	public void setValidUpto(Date validUpto) {
		this.validUpto = validUpto;
	}
	
	public Long getLeadTime() {
		return leadTime;
	}
	public void setLeadTime(Long leadTime) {
		this.leadTime = leadTime;
	}
	
	public String getNoOfAlerts() {
		return noOfAlerts;
	}
	public void setNoOfAlerts(String noOfAlerts) {
		this.noOfAlerts = noOfAlerts;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Long getRecurrence() {
		return recurrence;
	}
	public void setRecurrence(Long recurrence) {
		this.recurrence = recurrence;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
	
}