package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_empLeavegroup")
@TableGenerator(name ="hrms_empLeavegroup", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "hrms_empLeavegroup.findById", query = "SELECT r FROM EmployeeLeavegroupDO r where r.empLeavegroupId =:id"),
    @NamedQuery(name = "hrms_empLeavegroup.findByEmpId", query = "SELECT r FROM EmployeeLeavegroupDO r where r.employee.empId =:empId order by r.empLeavegroupId desc"),
})
public class EmployeeLeavegroupDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_BY_ID = "hrms_empLeavegroup.findById";
	
	public static final String FIND_BY_EMPID = "hrms_empLeavegroup.findByEmpId";
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)  
	private Long empLeavegroupId;
	private String status;
	
	@ManyToOne
	@JoinColumn(name="leaveGroupId")
	private LeaveGroupDO leaveGroup;
	
	@ManyToOne
	@JoinColumn(name="empId")
	private EmployeeDO employee;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getEmpLeavegroupId() {
		return empLeavegroupId;
	}
	public void setEmpLeavegroupId(Long empLeavegroupId) {
		this.empLeavegroupId = empLeavegroupId;
	}
	public LeaveGroupDO getLeaveGroup() {
		return leaveGroup;
	}
	public void setLeaveGroup(LeaveGroupDO leaveGroup) {
		this.leaveGroup = leaveGroup;
	}
	public EmployeeDO getEmployee() {
		return employee;
	}
	public void setEmployee(EmployeeDO employee) {
		this.employee = employee;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}