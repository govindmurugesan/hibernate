package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_master_deductionsubtype")
@TableGenerator(name ="hrms_master_deductionsubtype", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "hrms_master_deductionsubtype.findById", query = "SELECT r FROM DeductionSubTypeDO r where r.deductionSubTypeId =:id"),
    @NamedQuery(name = "hrms_master_deductionsubtype.findByStatus", query = "SELECT r FROM DeductionSubTypeDO r where r.status =:status"),
   @NamedQuery(name = "hrms_master_deductionsubtype.findByName", query = "SELECT r FROM DeductionSubTypeDO r where r.name=:name"),
    @NamedQuery(name = "hrms_master_deductionsubtype.findByNameAndId", query = "SELECT r FROM DeductionSubTypeDO r where r.name=:name and r.deductionSubTypeId NOT IN :id"),
    @NamedQuery(name = "hrms_master_deductionsubtype.findByDecuctionTypeId", query = "SELECT r FROM DeductionSubTypeDO r where r.deductionType.deductionTypeId=:id and r.status =:status"),
})
public class DeductionSubTypeDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static final String FIND_BY_ID = "hrms_master_deductionsubtype.findById";
	
	public static final String FIND_BY_STATUS = "hrms_master_deductionsubtype.findByStatus";
	
	public static final String FIND_BY_NAME = "hrms_master_deductionsubtype.findByName";
	
	public static final String FIND_FOR_UPDATE = "hrms_master_deductionsubtype.findByNameAndId";
	
	public static final String FIND_BY_DEDUCTIONTYPEID = "hrms_master_deductionsubtype.findByDecuctionTypeId";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long deductionSubTypeId;
	private String name;
	private String status;
	
	
	@ManyToOne
	@JoinColumn(name="deductionTypeId")
	private DeductionTypeDO deductionType;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	
	public Long getDeductionSubTypeId() {
		return deductionSubTypeId;
	}
	public void setDeductionSubTypeId(Long deductionSubTypeId) {
		this.deductionSubTypeId = deductionSubTypeId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public DeductionTypeDO getDeductionType() {
		return deductionType;
	}
	public void setDeductionType(DeductionTypeDO deductionType) {
		this.deductionType = deductionType;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}