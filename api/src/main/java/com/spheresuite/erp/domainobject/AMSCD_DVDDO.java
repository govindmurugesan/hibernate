package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="ams_master_cd_dvd")
@TableGenerator(name ="ams_master_cd_dvd", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "ams_master_cd_dvd.findAll", query = "SELECT r FROM AMSCD_DVDDO r"),
    @NamedQuery(name = "ams_master_cd_dvd.findById", query = "SELECT r FROM AMSCD_DVDDO r where r.cd_dvdId =:id"),
    @NamedQuery(name = "ams_master_cd_dvd.findByStatus", query = "SELECT r FROM AMSCD_DVDDO r where r.status =:status"),
    @NamedQuery(name = "ams_master_cd_dvd.findByName", query = "SELECT r FROM AMSCD_DVDDO r where r.status =:status and r.cd_dvdname=:name"),
   @NamedQuery(name = "ams_master_cd_dvd.findByNameAndId", query = "SELECT r FROM AMSCD_DVDDO r where r.status =:status and r.cd_dvdname=:name and r.cd_dvdId NOT IN :id"),
})
public class AMSCD_DVDDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "ams_master_cd_dvd.findAll";
	public static final String FIND_BY_ID = "ams_master_cd_dvd.findById";
	public static final String FIND_BY_STATUS = "ams_master_cd_dvd.findByStatus";
	public static final String FIND_BY_NAME = "ams_master_cd_dvd.findByName";
	public static final String FIND_FOR_UPDATE = "ams_master_cd_dvd.findByNameAndId";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long cd_dvdId;
	private String cd_dvdname;
	private String comments;
	private char status;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
	public Long getCd_dvdId() {
		return cd_dvdId;
	}
	public void setCd_dvdId(Long cd_dvdId) {
		this.cd_dvdId = cd_dvdId;
	}
	public String getCd_dvdname() {
		return cd_dvdname;
	}
	public void setCd_dvdname(String cd_dvdname) {
		this.cd_dvdname = cd_dvdname;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}