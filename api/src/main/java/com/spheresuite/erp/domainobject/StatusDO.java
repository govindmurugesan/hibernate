package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="master_status")
@TableGenerator(name ="master_status", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "master_status.findById", query = "SELECT r FROM StatusDO r where r.statusId =:id"),
    @NamedQuery(name = "master_status.findByName", query = "SELECT r FROM StatusDO r where r.name=:name"),
    @NamedQuery(name = "master_status.findByNameAndId", query = "SELECT r FROM StatusDO r where r.name=:name and r.statusId NOT IN :id"),
   /* @NamedQuery(name = "master_status.findByStatus", query = "SELECT r FROM AddressTypeDO r where r.status =:status"),

    */
})
public class StatusDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static final String FIND_BY_ID = "master_status.findById";
	
	public static final String FIND_BY_NAME = "master_status.findByName";
	
	public static final String FIND_FOR_UPDATE = "master_status.findByNameAndId";
	
	/*public static final String FIND_BY_STATUS = "master_addresstype.findByStatus";
	
	
	
	*/
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long statusId;
	private String name;
	private String status;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
	public Long getStatusId() {
		return statusId;
	}
	public void setAdvanceTypeId(Long statusId) {
		this.statusId = statusId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}