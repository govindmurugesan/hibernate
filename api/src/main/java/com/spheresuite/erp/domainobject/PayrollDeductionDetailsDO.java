package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_payrolldeductiondetails")
@TableGenerator(name ="hrms_payrolldeductiondetails", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "hrms_payrolldeductiondetails.findByEmpPayrollId", query = "SELECT r FROM PayrollDeductionDetailsDO r where r.payroll.payrollId =:id"),
    @NamedQuery(name = "hrms_payrolldeductiondetails.findByEmpPayrollIds", query = "SELECT r FROM PayrollDeductionDetailsDO r where r.payroll.payrollId IN :id"),
    @NamedQuery(name = "hrms_payrolldeductiondetails.findAllDeduction", query = "SELECT sum(r.monthly) FROM PayrollDeductionDetailsDO r where r.payroll.payrollId =:id"),
})
public class PayrollDeductionDetailsDO implements Serializable {
	private static final long serialVersionUID = 1L;

	
	public static final String FIND_BY_ID = "hrms_payrolldeductiondetails.findByEmpPayrollId";
	
	public static final String FIND_BY_IDS = "hrms_payrolldeductiondetails.findByEmpPayrollIds";
	
	public static final String FIND_ALL_DEDUCTION = "hrms_payrolldeductiondetails.findAllDeduction";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO) 
	@Column(nullable = false, unique = true) 
	private Long payrollDeductionId;
	
	private Double monthly;
	private Double ytd;
	private String name;
	
	@ManyToOne
	@JoinColumn(name="payrollId")
	private EmployeePayrollDO payroll;
	
	@ManyToOne
	@JoinColumn(name="payrollGroupId")
	private PayrollGroupDO payrollGroup;
	
	@ManyToOne
	@JoinColumn(name="payrollGroupDeductionId")
	private PayrollGroupDeductionDO payrollGroupDeduction;
	
	@ManyToOne
	@JoinColumn(name="dearnessallowanceId")
	private DASettingsDO dearnessAllowance;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getPayrollDeductionId() {
		return payrollDeductionId;
	}
	public void setPayrollDeductionId(Long payrollDeductionId) {
		this.payrollDeductionId = payrollDeductionId;
	}
	public Double getMonthly() {
		return monthly;
	}
	public void setMonthly(Double monthly) {
		this.monthly = monthly;
	}
	public Double getYtd() {
		return ytd;
	}
	public void setYtd(Double ytd) {
		this.ytd = ytd;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public EmployeePayrollDO getPayroll() {
		return payroll;
	}
	public void setPayroll(EmployeePayrollDO payroll) {
		this.payroll = payroll;
	}
	public PayrollGroupDO getPayrollGroup() {
		return payrollGroup;
	}
	public void setPayrollGroup(PayrollGroupDO payrollGroup) {
		this.payrollGroup = payrollGroup;
	}
	public PayrollGroupDeductionDO getPayrollGroupDeduction() {
		return payrollGroupDeduction;
	}
	public void setPayrollGroupDeduction(
			PayrollGroupDeductionDO payrollGroupDeduction) {
		this.payrollGroupDeduction = payrollGroupDeduction;
	}
	public DASettingsDO getDearnessAllowance() {
		return dearnessAllowance;
	}
	public void setDearnessAllowance(DASettingsDO dearnessAllowance) {
		this.dearnessAllowance = dearnessAllowance;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}