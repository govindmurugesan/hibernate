package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_master_shift")
@TableGenerator(name ="hrms_master_shift", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "hrms_master_shift.findById", query = "SELECT r FROM ShiftDO r where r.shiftId =:id"),
    @NamedQuery(name = "hrms_master_shift.findByStatus", query = "SELECT r FROM ShiftDO r where r.status =:status"),
    @NamedQuery(name = "hrms_master_shift.findByName", query = "SELECT r FROM ShiftDO r where r.name=:name"),
    @NamedQuery(name = "hrms_master_shift.findByNameAndId", query = "SELECT r FROM ShiftDO r where r.name=:name and r.shiftId NOT IN :id"),
})
public class ShiftDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static final String FIND_BY_ID = "hrms_master_shift.findById";
	
	public static final String FIND_BY_STATUS = "hrms_master_shift.findByStatus";
	
	public static final String FIND_BY_NAME = "hrms_master_shift.findByName";
	
	public static final String FIND_FOR_UPDATE = "hrms_master_shift.findByNameAndId";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long shiftId;
	private String name;
	private String startTime;
	private String endTime;
	private char status;
	private String isManager;
	private String maxHrExcuse;
	private String noOfExcuse;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
	public Long getShiftId() {
		return shiftId;
	}
	public void setShiftId(Long shiftId) {
		this.shiftId = shiftId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getIsManager() {
		return isManager;
	}
	public void setIsManager(String isManager) {
		this.isManager = isManager;
	}
	public String getMaxHrExcuse() {
		return maxHrExcuse;
	}
	public void setMaxHrExcuse(String maxHrExcuse) {
		this.maxHrExcuse = maxHrExcuse;
	}
	public String getNoOfExcuse() {
		return noOfExcuse;
	}
	public void setNoOfExcuse(String noOfExcuse) {
		this.noOfExcuse = noOfExcuse;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}