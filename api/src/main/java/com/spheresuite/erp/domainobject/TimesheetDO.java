package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_timesheet")
@TableGenerator(name ="hrms_timesheet", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "hrms_timesheet.findById", query = "SELECT r FROM TimesheetDO r where r.timesheetId =:id"),
    @NamedQuery(name = "hrms_timesheet.findForDuplicatePersist", query = "SELECT r FROM TimesheetDO r where r.timesheetType.timesheetTypeId =:id and r.date =:date and r.weeklyTimesheet.date=:weekenddate"),
    @NamedQuery(name = "hrms_timesheet.findByWeekendDate", query = "SELECT r FROM TimesheetDO r where r.weeklyTimesheet.date =:weekenddate and r.empId =:empId"),
    @NamedQuery(name = "hrms_timesheet.findForDuplicateUpdate", query = "SELECT r FROM TimesheetDO r where r.timesheetType.timesheetTypeId =:id and r.date =:date and r.weeklyTimesheet.date=:weekenddate and r.timesheetId <>:Id"),
})
public class TimesheetDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_BY_DATE = "hrms_timesheet.findByWeekendDate";
	public static final String FIND_BY_ID = "hrms_timesheet.findById";
	public static final String FIND_DUPLICATE_PERSIST = "hrms_timesheet.findForDuplicatePersist";
	public static final String FIND_DUPLICATE_UPDATE = "hrms_timesheet.findForDuplicateUpdate";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO) 
	private Long timesheetId;
	
	private Long hours;
	private String date;
	private String description;
	private String empId;
	//private char status;
	
	@ManyToOne
	@JoinColumn(name="timesheetTypeId")
	private TimesheetTypeDO timesheetType;
	
	@ManyToOne
	@JoinColumn(name="weeklyTimesheetId")
	private WeeklyTimesheetDO weeklyTimesheet;

	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getTimesheetId() {
		return timesheetId;
	}
	public void setTimesheetId(Long timesheetId) {
		this.timesheetId = timesheetId;
	}
	public Long getHours() {
		return hours;
	}
	public void setHours(Long hours) {
		this.hours = hours;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getEmpId() {
		return empId;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
	public TimesheetTypeDO getTimesheetType() {
		return timesheetType;
	}
	public void setTimesheetType(TimesheetTypeDO timesheetType) {
		this.timesheetType = timesheetType;
	}
	public WeeklyTimesheetDO getWeeklyTimesheet() {
		return weeklyTimesheet;
	}
	public void setWeeklyTimesheet(WeeklyTimesheetDO weeklyTimesheet) {
		this.weeklyTimesheet = weeklyTimesheet;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}