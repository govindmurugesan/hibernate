package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="master_employeetype")
@TableGenerator(name ="master_employeetype", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "master_employeetype.findById", query = "SELECT r FROM EmployeeTypeDO r where r.empTypeId =:id"),
    @NamedQuery(name = "master_employeetype.findByStatus", query = "SELECT r FROM EmployeeTypeDO r where r.status =:status"),
    @NamedQuery(name = "master_employeetype.findByName", query = "SELECT r FROM EmployeeTypeDO r where r.name=:name"),
    @NamedQuery(name = "master_employeetype.findByNameAndId", query = "SELECT r FROM EmployeeTypeDO r where r.name=:name and r.empTypeId NOT IN :id"),
    @NamedQuery(name = "master_employeetype.findByNameAll", query = "SELECT r FROM EmployeeTypeDO r where r.name=:name"),
})
public class EmployeeTypeDO implements Serializable {
	private static final long serialVersionUID = 1L;

	
	public static final String FIND_BY_ID = "master_employeetype.findById";
	
	public static final String FIND_BY_STATUS = "master_employeetype.findByStatus";
	
	public static final String FIND_BY_NAME = "master_employeetype.findByName";
	
	public static final String FIND_FOR_UPDATE = "master_employeetype.findByNameAndId";
	
	public static final String FIND_BY_NAME_ALL = "master_employeetype.findByNameAll";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true) 
	private Long empTypeId;
	
	private String name;
	private char status;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
	private String updatedby;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	
	public Long getEmpTypeId() {
		return empTypeId;
	}
	public void setEmpTypeId(Long empTypeId) {
		this.empTypeId = empTypeId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}