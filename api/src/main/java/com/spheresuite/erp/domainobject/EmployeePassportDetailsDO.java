package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="employeepassportdetails")
@TableGenerator(name ="employeepassportdetails", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "employeepassportdetails.findById", query = "SELECT r FROM EmployeePassportDetailsDO r where r.empPassportDetailId =:id"),
    @NamedQuery(name = "employeepassportdetails.findByEmpId", query = "SELECT r FROM EmployeePassportDetailsDO r where r.employee.empId =:empId order by r.empPassportDetailId desc"),
})
public class EmployeePassportDetailsDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_BY_ID = "employeepassportdetails.findById";
	
	public static final String FIND_BY_EMPID = "employeepassportdetails.findByEmpId";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO) 
	private Long empPassportDetailId;
	
	private String passportNumber;
	private String placeOfIssue;
	@Temporal(TemporalType.DATE)
    private Date dateOfIssue;
	@Temporal(TemporalType.DATE)
    private Date dateOfExpiry;
	
	@ManyToOne
	@JoinColumn(name="empId")
	private EmployeeDO employee;

	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getEmpPassportDetailId() {
		return empPassportDetailId;
	}
	public void setEmpPassportDetailId(Long empPassportDetailId) {
		this.empPassportDetailId = empPassportDetailId;
	}
	public String getPassportNumber() {
		return passportNumber;
	}
	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}
	public String getPlaceOfIssue() {
		return placeOfIssue;
	}
	public void setPlaceOfIssue(String placeOfIssue) {
		this.placeOfIssue = placeOfIssue;
	}
	public Date getDateOfIssue() {
		return dateOfIssue;
	}
	public void setDateOfIssue(Date dateOfIssue) {
		this.dateOfIssue = dateOfIssue;
	}
	public Date getDateOfExpiry() {
		return dateOfExpiry;
	}
	public void setDateOfExpiry(Date dateOfExpiry) {
		this.dateOfExpiry = dateOfExpiry;
	}
	public EmployeeDO getEmployee() {
		return employee;
	}
	public void setEmployee(EmployeeDO employee) {
		this.employee = employee;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
    
	
}