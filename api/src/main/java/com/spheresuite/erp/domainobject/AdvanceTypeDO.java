package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_master_advancetype")
@TableGenerator(name ="hrms_master_advancetype", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "hrms_master_advancetype.findById", query = "SELECT r FROM AdvanceTypeDO r where r.advanceTypeId =:id"),
    @NamedQuery(name = "hrms_master_advancetype.findByName", query = "SELECT r FROM AdvanceTypeDO r where r.name=:name"),
    @NamedQuery(name = "hrms_master_advancetype.findByNameAndId", query = "SELECT r FROM AdvanceTypeDO r where r.name=:name and r.advanceTypeId NOT IN :id"),
   /* @NamedQuery(name = "hrms_master_advancetype.findByStatus", query = "SELECT r FROM AddressTypeDO r where r.status =:status"),

    */
})
public class AdvanceTypeDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static final String FIND_BY_ID = "hrms_master_advancetype.findById";
	
	public static final String FIND_BY_NAME = "hrms_master_advancetype.findByName";
	
	public static final String FIND_FOR_UPDATE = "hrms_master_advancetype.findByNameAndId";
	
	/*public static final String FIND_BY_STATUS = "master_addresstype.findByStatus";
	
	
	
	*/
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long advanceTypeId;
	private String name;
	private String status;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
	public Long getAdvanceTypeId() {
		return advanceTypeId;
	}
	public void setAdvanceTypeId(Long advanceTypeId) {
		this.advanceTypeId = advanceTypeId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}