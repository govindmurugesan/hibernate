package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="ams_master_office")
@TableGenerator(name ="ams_master_office", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "ams_master_office.findAll", query = "SELECT r FROM AMSOfficeDO r"),
    @NamedQuery(name = "ams_master_office.findById", query = "SELECT r FROM AMSOfficeDO r where r.officeId =:id"),
    @NamedQuery(name = "ams_master_office.findByStatus", query = "SELECT r FROM AMSOfficeDO r where r.status =:status"),
    @NamedQuery(name = "ams_master_office.findByName", query = "SELECT r FROM AMSOfficeDO r where r.status =:status and r.office_name=:name"),
   @NamedQuery(name = "ams_master_office.findByNameAndId", query = "SELECT r FROM AMSOfficeDO r where r.status =:status and r.office_name=:name and r.officeId NOT IN :id"),
})
public class AMSOfficeDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "ams_master_office.findAll";
	public static final String FIND_BY_ID = "ams_master_office.findById";
	public static final String FIND_BY_STATUS = "ams_master_office.findByStatus";
	public static final String FIND_BY_NAME = "ams_master_office.findByName";
	public static final String FIND_FOR_UPDATE = "ams_master_office.findByNameAndId";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO) 
	@Column(nullable = false, unique = true)
	private Long officeId;
	
	private String office_name;
	private String office_comments;
	private char status;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
	public Long getOfficeId() {
		return officeId;
	}
	public void setOfficeId(Long officeId) {
		this.officeId = officeId;
	}
	public String getOffice_name() {
		return office_name;
	}
	public void setOffice_name(String office_name) {
		this.office_name = office_name;
	}
	public String getOffice_comments() {
		return office_comments;
	}
	public void setOffice_comments(String office_comments) {
		this.office_comments = office_comments;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}