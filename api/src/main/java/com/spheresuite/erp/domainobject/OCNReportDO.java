package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="pl_ocnreport")
@TableGenerator(name ="pl_ocnreport", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "pl_ocnreport.findById", query = "SELECT r FROM OCNReportDO r where r.ocnReportId =:id"),
    @NamedQuery(name = "pl_ocnreport.findByOCN", query = "SELECT r FROM OCNReportDO r where r.ocnDetails.ocnId =:ocn"),
    @NamedQuery(name = "pl_ocnreport.findByStatus", query = "SELECT r FROM OCNReportDO r where r.status =:status"),
    @NamedQuery(name = "pl_ocnreport.findAll", query = "SELECT r FROM OCNReportDO r where r.status IS NOT NULL"),
    @NamedQuery(name = "pl_ocnreport.findByStatusNull", query = "SELECT r FROM OCNReportDO r where r.status is NULL"),
})
public class OCNReportDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static final String FIND_BY_ID = "pl_ocnreport.findById";
	
	public static final String FIND_BY_OCN = "pl_ocnreport.findByOCN";
	
	public static final String FIND_BY_STATUS = "pl_ocnreport.findByStatus";
	
	public static final String FIND_ALL = "pl_ocnreport.findAll";
	
	public static final String FIND_BY_STATUS_NULL = "pl_ocnreport.findByStatusNull";
	
	public static final String FIND_FOR_UPDATE = "pl_ocnreport.findByNameAndId";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long ocnReportId;
	@Column(columnDefinition="LONGTEXT")
	private String reason;
	
	@ManyToOne
	@JoinColumn(name="ocnId")
	private OCNDO ocnDetails;
	
	@ManyToOne
	@JoinColumn(name="ocnBudgetId")
	private OCNBudgetDO ocnBudget;
	
	private String customerName;
	private String customerId;
	private Double greigeFabricm;
	private Double greigeFabric;
	private Double dyeingCost;
	private Double finishingCost;
	private Double trims;
	private Double sam;
	private Double rejection;
	private String greigeFabricQty;
	private String dyeingCostQty;
	private String finishingCostQty;
	private String trimsQty;
	private String samQty;
	private String rejectionQty;
	private String greigeFabricAmount;
	private String dyeingAmount;
	private String finishingAmount;
	private String trimsAmount;
	private String samAmount;
	private String rejectionAmount;
	private String amount;
	private String status;
	private String invoiceAmount;
	private String salesPercentage;
	
	@Temporal(TemporalType.DATE)
    private Date shippingDate;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
    
	public Long getOcnReportId() {
		return ocnReportId;
	}
	public void setOcnReportId(Long ocnReportId) {
		this.ocnReportId = ocnReportId;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	
	public OCNDO getOcnDetails() {
		return ocnDetails;
	}
	public void setOcnDetails(OCNDO ocnDetails) {
		this.ocnDetails = ocnDetails;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public Double getGreigeFabricm() {
		return greigeFabricm;
	}
	public void setGreigeFabricm(Double greigeFabricm) {
		this.greigeFabricm = greigeFabricm;
	}
	public OCNBudgetDO getOcnBudget() {
		return ocnBudget;
	}
	public void setOcnBudget(OCNBudgetDO ocnBudget) {
		this.ocnBudget = ocnBudget;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public Double getGreigeFabric() {
		return greigeFabric;
	}
	public void setGreigeFabric(Double greigeFabric) {
		this.greigeFabric = greigeFabric;
	}
	public Double getDyeingCost() {
		return dyeingCost;
	}
	public void setDyeingCost(Double dyeingCost) {
		this.dyeingCost = dyeingCost;
	}
	public Double getFinishingCost() {
		return finishingCost;
	}
	public void setFinishingCost(Double finishingCost) {
		this.finishingCost = finishingCost;
	}
	public Double getTrims() {
		return trims;
	}
	public void setTrims(Double trims) {
		this.trims = trims;
	}
	public Double getSam() {
		return sam;
	}
	public void setSam(Double sam) {
		this.sam = sam;
	}
	public Double getRejection() {
		return rejection;
	}
	public void setRejection(Double rejection) {
		this.rejection = rejection;
	}
	
	public String getGreigeFabricQty() {
		return greigeFabricQty;
	}
	public void setGreigeFabricQty(String greigeFabricQty) {
		this.greigeFabricQty = greigeFabricQty;
	}
	public String getDyeingCostQty() {
		return dyeingCostQty;
	}
	public void setDyeingCostQty(String dyeingCostQty) {
		this.dyeingCostQty = dyeingCostQty;
	}
	public String getFinishingCostQty() {
		return finishingCostQty;
	}
	public void setFinishingCostQty(String finishingCostQty) {
		this.finishingCostQty = finishingCostQty;
	}
	public String getTrimsQty() {
		return trimsQty;
	}
	public void setTrimsQty(String trimsQty) {
		this.trimsQty = trimsQty;
	}
	public String getSamQty() {
		return samQty;
	}
	public void setSamQty(String samQty) {
		this.samQty = samQty;
	}
	public String getRejectionQty() {
		return rejectionQty;
	}
	public void setRejectionQty(String rejectionQty) {
		this.rejectionQty = rejectionQty;
	}
	public String getGreigeFabricAmount() {
		return greigeFabricAmount;
	}
	public void setGreigeFabricAmount(String greigeFabricAmount) {
		this.greigeFabricAmount = greigeFabricAmount;
	}
	public String getDyeingAmount() {
		return dyeingAmount;
	}
	public void setDyeingAmount(String dyeingAmount) {
		this.dyeingAmount = dyeingAmount;
	}
	public String getFinishingAmount() {
		return finishingAmount;
	}
	public void setFinishingAmount(String finishingAmount) {
		this.finishingAmount = finishingAmount;
	}
	public String getTrimsAmount() {
		return trimsAmount;
	}
	public void setTrimsAmount(String trimsAmount) {
		this.trimsAmount = trimsAmount;
	}
	public String getSamAmount() {
		return samAmount;
	}
	public void setSamAmount(String samAmount) {
		this.samAmount = samAmount;
	}
	public String getRejectionAmount() {
		return rejectionAmount;
	}
	public void setRejectionAmount(String rejectionAmount) {
		this.rejectionAmount = rejectionAmount;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getInvoiceAmount() {
		return invoiceAmount;
	}
	public void setInvoiceAmount(String invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}
	public String getSalesPercentage() {
		return salesPercentage;
	}
	public void setSalesPercentage(String salesPercentage) {
		this.salesPercentage = salesPercentage;
	}
	public Date getShippingDate() {
		return shippingDate;
	}
	public void setShippingDate(Date shippingDate) {
		this.shippingDate = shippingDate;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
    
}