package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_employeeshift")
@TableGenerator(name ="hrms_employeeshift", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "hrms_employeeshift.findById", query = "SELECT r FROM EmployeeShiftDO r where r.empShiftId = :id"),
    @NamedQuery(name = "hrms_employeeshift.findByEmpId", query = "SELECT r FROM EmployeeShiftDO r where r.employee.empId = :id"),
    @NamedQuery(name = "hrms_employeeshift.findByDate", query = "SELECT r FROM EmployeeShiftDO r where r.fromDate >= :date and r.toDate <= :date"),
    @NamedQuery(name = "hrms_employeeshift.findByEmpDate", query = "SELECT r FROM EmployeeShiftDO r where r.employee.empId = :id AND r.fromDate >= :date AND r.toDate <= :date"),
    @NamedQuery(name = "hrms_employeeshift.findByDateValidate", query = "SELECT r FROM EmployeeShiftDO r where r.empShiftId = :id AND r.fromDate > adddate(CURRENT_DATE ,-30)"),
})
public class EmployeeShiftDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static final String FIND_BY_ID = "hrms_employeeshift.findById";
	
	public static final String FIND_BY_EMP_ID = "hrms_employeeshift.findByEmpId";
	
	public static final String FIND_BY_DATE = "hrms_employeeshift.findByDate";
	
	public static final String FIND_BY_EMP_DATE = "hrms_employeeshift.findByEmpDate";
	
	public static final String FIND_BY_DATE_VALIDATE = "hrms_employeeshift.findByDateValidate";
	
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long empShiftId;
	
	@Temporal(TemporalType.DATE)
    private Date fromDate;
	
	@Temporal(TemporalType.DATE)
    private Date toDate;
	
	@ManyToOne
	@JoinColumn(name="empId")
	private EmployeeDO employee;
	
	@ManyToOne
	@JoinColumn(name="shiftId")
	private ShiftDO shift;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getEmpShiftId() {
		return empShiftId;
	}
	public void setEmpShiftId(Long empShiftId) {
		this.empShiftId = empShiftId;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public EmployeeDO getEmployee() {
		return employee;
	}
	public void setEmployee(EmployeeDO employee) {
		this.employee = employee;
	}
	public ShiftDO getShift() {
		return shift;
	}
	public void setShift(ShiftDO shift) {
		this.shift = shift;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}