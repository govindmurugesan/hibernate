package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="employeeterminate")
@TableGenerator(name ="employeeterminate", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "employeeterminate.findById", query = "SELECT r FROM EmployeeTerminateDO r where r.empTerminateId =:id"),
    @NamedQuery(name = "employeeterminate.findByEmpId", query = "SELECT r FROM EmployeeTerminateDO r where r.employee.empId =:empId"),
})
public class EmployeeTerminateDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_BY_ID = "employeeterminate.findById";
	
	public static final String FIND_BY_EMPID = "employeeterminate.findByEmpId";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long empTerminateId;
	
	@Temporal(TemporalType.DATE)
	private Date terminateDate;
	@Temporal(TemporalType.DATE)
	private Date resignDate;
	private String comments;
	
	@ManyToOne
	@JoinColumn(name="empId")
	private EmployeeDO employee;
	
	@ManyToOne
	@JoinColumn(name="terminationReasonId")
	private TerminateReasonTypeDO terminateReason;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getEmpTerminateId() {
		return empTerminateId;
	}
	public void setEmpTerminateId(Long empTerminateId) {
		this.empTerminateId = empTerminateId;
	}
	public Date getTerminateDate() {
		return terminateDate;
	}
	public void setTerminateDate(Date terminateDate) {
		this.terminateDate = terminateDate;
	}
	public Date getResignDate() {
		return resignDate;
	}
	public void setResignDate(Date resignDate) {
		this.resignDate = resignDate;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public EmployeeDO getEmployee() {
		return employee;
	}
	public void setEmployee(EmployeeDO employee) {
		this.employee = employee;
	}
	public TerminateReasonTypeDO getTerminateReason() {
		return terminateReason;
	}
	public void setTerminateReason(TerminateReasonTypeDO terminateReason) {
		this.terminateReason = terminateReason;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}