package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_generalEarning")
@TableGenerator(name ="hrms_generalEarning", initialValue =100101, allocationSize =1)
@NamedQueries({
	@NamedQuery(name = "hrms_generalEarning.findAll", query = "SELECT r FROM GeneralEarningDO r ORDER BY r.generalEarningId DESC"),
    @NamedQuery(name = "hrms_generalEarning.findById", query = "SELECT r FROM GeneralEarningDO r where r.generalEarningId =:id"),
    @NamedQuery(name = "hrms_generalEarning.findActiveByEmpId", query = "SELECT r FROM GeneralEarningDO r where r.employee.empId =:empId and r.status =:status"),
    @NamedQuery(name = "hrms_generalEarning.findActiveByEmpIdAndMonth", query = "SELECT r FROM GeneralEarningDO r where r.employee.empId =:empId and r.month =:month and r.status =:status"),
})
public class GeneralEarningDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_BY_ID = "hrms_generalEarning.findById";
	
	public static final String FIND_ALL = "hrms_generalEarning.findAll";
	
	public static final String FIND_BY_EMPID = "hrms_generalEarning.findByEmpId";
	
	public static final String FIND_ACTIVE_BY_EMPID = "hrms_generalEarning.findActiveByEmpId";
	
	public static final String FIND_ACTIVE_BY_EMPID_MONTH = "hrms_generalEarning.findActiveByEmpIdAndMonth";
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)  
	private Long generalEarningId;
	private Long amount;
	private String month;
	private String status;
	
	@ManyToOne
	@JoinColumn(name="empId")
	private EmployeeDO employee;
	
	@ManyToOne
	@JoinColumn(name="earningTypeId")
	private EarningTypeDO earningType;
	
	@ManyToOne
	@JoinColumn(name="unitOrBranchId")
	private UnitOrBranchDO unitOrBranch;
	
	@ManyToOne
	@JoinColumn(name="earningSubTypeId")
	private EarningSubTypeDO earningSubType;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
	public Long getGeneralEarningId() {
		return generalEarningId;
	}
	public void setGeneralEarningId(Long generalEarningId) {
		this.generalEarningId = generalEarningId;
	}
	public Long getAmount() {
		return amount;
	}
	public void setAmount(Long amount) {
		this.amount = amount;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public EmployeeDO getEmployee() {
		return employee;
	}
	public void setEmployee(EmployeeDO employee) {
		this.employee = employee;
	}
	public EarningTypeDO getEarningType() {
		return earningType;
	}
	public void setEarningType(EarningTypeDO earningType) {
		this.earningType = earningType;
	}
	public UnitOrBranchDO getUnitOrBranch() {
		return unitOrBranch;
	}
	public void setUnitOrBranch(UnitOrBranchDO unitOrBranch) {
		this.unitOrBranch = unitOrBranch;
	}
	public EarningSubTypeDO getEarningSubType() {
		return earningSubType;
	}
	public void setEarningSubType(EarningSubTypeDO earningSubType) {
		this.earningSubType = earningSubType;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
    
    
}