package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="greige_quotation")
@TableGenerator(name ="greige_quotation", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "greige_quotation.findById", query = "SELECT r FROM Greige_CreateQuotationDO r where r.quoteId =:id"),
    @NamedQuery(name = "greige_quotation.findQuoteByStatus", query = "SELECT r FROM Greige_CreateQuotationDO r where r.status =:status ORDER BY CASE r.status WHEN 'd' THEN 1 WHEN 'r' THEN 2 WHEN 's' THEN 3 WHEN 'a' THEN 4  END"),
    @NamedQuery(name = "greige_quotation.findApproveQuote", query = "SELECT r FROM Greige_CreateQuotationDO r where r.status =:status or r.status =:approvedStatus ORDER BY CASE r.status WHEN 'd' THEN 1 WHEN 'r' THEN 2 WHEN 's' THEN 3 WHEN 'a' THEN 4  END"),
    @NamedQuery(name = "greige_quotation.findSortId", query = "SELECT r FROM Greige_CreateQuotationDO r where r.sortName.sortId =:id"),
    @NamedQuery(name = "greige_quotation.findSortIds", query = "SELECT r FROM Greige_CreateQuotationDO r where r.sortName.sortId IN (:id)"),
    //@NamedQuery(name = "greige_quotation.findQuotDueDate", query = "SELECT r FROM Greige_CreateQuotationDO r where r.quoteType =:type and r.greigeReqDueDate <:date ORDER BY CASE r.status WHEN 'd' THEN 1 WHEN 'r' THEN 2 WHEN 's' THEN 3 WHEN 'a' THEN 4  END"),
    @NamedQuery(name = "greige_quotation.findQuotDueDate", query = "SELECT r FROM Greige_CreateQuotationDO r where r.quoteType =:type ORDER BY CASE r.status WHEN 'd' THEN 1 WHEN 'r' THEN 2 WHEN 's' THEN 3 WHEN 'a' THEN 4  END"),
    @NamedQuery(name = "greige_quotation.findQuotType", query = "SELECT r FROM Greige_CreateQuotationDO r where r.quoteType =:type ORDER BY CASE r.status WHEN 'd' THEN 1 WHEN 'r' THEN 2 WHEN 's' THEN 3 WHEN 'a' THEN 4  END"),
    @NamedQuery(name = "greige_quotation.findQuotReqCount", query = "SELECT r FROM Greige_CreateQuotationDO r where r.status ='r'"),
    @NamedQuery(name = "greige_quotation.findQuotDraftCount", query = "SELECT r FROM Greige_CreateQuotationDO r where r.status ='d'"),
  //  @NamedQuery(name = "greige_pendingOrders.findByStatus", query = "SELECT r FROM Greige_pendingOrders r where r.status =:status"),
/*    @NamedQuery(name = "ep_master_sort.findByName", query = "SELECT r FROM EarningTypeDO r where r.name=:name"),
    @NamedQuery(name = "ep_master_sort.findByNameAndId", query = "SELECT r FROM EarningTypeDO r where r.name=:name and r.earningTypeId NOT IN :id"),*/
})
public class Greige_CreateQuotationDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_BY_ID = "greige_quotation.findById";
	
	public static final String FIND_QUOTEBYSTATUS = "greige_quotation.findQuoteByStatus";
	
	public static final String FIND_QUOTEBYAPPROVESTATUS = "greige_quotation.findApproveQuote";
	
	public static final String FIND_BY_SORTID = "greige_quotation.findSortId";
	
	public static final String FIND_BY_SORTIDS = "greige_quotation.findSortIds";
	
	public static final String FIND_BY_QUOTDUEDATE = "greige_quotation.findQuotDueDate";
	
	public static final String FIND_BY_QUOTTYPE = "greige_quotation.findQuotType";
	
	
	public static final String FIND_QUOTE_REQ_COUNT = "greige_quotation.findQuotReqCount";
	
	public static final String FIND_QUOTE_DRAFT_COUNT = "greige_quotation.findQuotDraftCount";
	
	
	//public static final String FIND_BY_STATUS = "greige_pendingOrders.findByStatus";
	
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long quoteId;
	
	private String yc;
	private String tc;
	private String gw;
	private String grCons;
	private String quoteType;
	private String requiredMeter;
	private String status;
	private Long targetPrice;
	private String wave;
	private String pendingRequiredMeter;
	@Temporal(TemporalType.TIMESTAMP)
    private Date greigeReqDueDate;
	@Temporal(TemporalType.DATE)
    private Date greigReqDate;
	
	@ManyToOne
	@JoinColumn(name="sortId")
	private EPMasterSortDO sortName;
	
	@ManyToOne
	@JoinColumn(name="quoteDueDateId")
	private EPMasterQuoteDueDateDO quoteDue;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
	public Long getQuoteId() {
		return quoteId;
	}
	public void setQuoteId(Long quoteId) {
		this.quoteId = quoteId;
	}
	public String getYc() {
		return yc;
	}
	public void setYc(String yc) {
		this.yc = yc;
	}
	public String getTc() {
		return tc;
	}
	public void setTc(String tc) {
		this.tc = tc;
	}
	public String getGw() {
		return gw;
	}
	public void setGw(String gw) {
		this.gw = gw;
	}
	public String getGrCons() {
		return grCons;
	}
	public void setGrCons(String grCons) {
		this.grCons = grCons;
	}
	public String getWave() {
		return wave;
	}
	public void setWave(String wave) {
		this.wave = wave;
	}
	public String getRequiredMeter() {
		return requiredMeter;
	}
	public void setRequiredMeter(String requiredMeter) {
		this.requiredMeter = requiredMeter;
	}
	public String getPendingRequiredMeter() {
		return pendingRequiredMeter;
	}
	public void setPendingRequiredMeter(String pendingRequiredMeter) {
		this.pendingRequiredMeter = pendingRequiredMeter;
	}
	public Long getTargetPrice() {
		return targetPrice;
	}
	public void setTargetPrice(Long targetPrice) {
		this.targetPrice = targetPrice;
	}
	public Date getGreigeReqDueDate() {
		return greigeReqDueDate;
	}
	public void setGreigeReqDueDate(Date greigeReqDueDate) {
		this.greigeReqDueDate = greigeReqDueDate;
	}
	public Date getGreigReqDate() {
		return greigReqDate;
	}
	public void setGreigReqDate(Date greigReqDate) {
		this.greigReqDate = greigReqDate;
	}
	public EPMasterSortDO getSortName() {
		return sortName;
	}
	public String getQuoteType() {
		return quoteType;
	}
	public void setQuoteType(String quoteType) {
		this.quoteType = quoteType;
	}
	public void setSortName(EPMasterSortDO sortName) {
		this.sortName = sortName;
	}
	public EPMasterQuoteDueDateDO getQuoteDue() {
		return quoteDue;
	}
	public void setQuoteDue(EPMasterQuoteDueDateDO quoteDue) {
		this.quoteDue = quoteDue;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
    
}