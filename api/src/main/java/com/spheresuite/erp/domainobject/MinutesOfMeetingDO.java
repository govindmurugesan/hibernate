package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="crm_mom")
@TableGenerator(name ="crm_mom", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "crm_mom.findAll", query = "SELECT r FROM MinutesOfMeetingDO r"),
    @NamedQuery(name = "crm_mom.findById", query = "SELECT r FROM MinutesOfMeetingDO r where r.momId =:id"),
    @NamedQuery(name = "crm_mom.findByName", query = "SELECT r FROM MinutesOfMeetingDO r where r.company =:name"),
    @NamedQuery(name = "crm_mom.findByLeadId", query = "SELECT r FROM MinutesOfMeetingDO r where r.lead.leadId =:id"),
    @NamedQuery(name = "crm_mom.findByEvent", query = "SELECT r FROM MinutesOfMeetingDO r where r.events.eventId =:id"),
    @NamedQuery(name = "crm_mom.findByEmpMom", query = "SELECT r FROM MinutesOfMeetingDO r where r.events.eventId =:eventId and r.empId IN :id"),
})
public class MinutesOfMeetingDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "crm_mom.findAll";
	
	public static final String FIND_BY_ID = "crm_mom.findById";
	
	public static final String FIND_BY_NAME = "crm_mom.findByName";
	
	public static final String FIND_BY_EVENT = "crm_mom.findByEvent";
	
	public static final String FIND_BY_LEADID = "crm_mom.findByLeadId";
	
	public static final String FIND_BY_EMP_MOM = "crm_mom.findByEmpMom";
	
	@Id
	@Column(nullable = false, unique = true)
	@GeneratedValue(strategy=GenerationType.AUTO) 
	private Long momId;
	
	//private Long leadtype;
	/*@ManyToOne
	@JoinColumn(name="leadId")
	private LeadDO lead;*/
	
	//private Long salutation;
	@ManyToOne
	@JoinColumn(name="eventId")
	private EventsDO events;
	
	@ManyToOne
	@JoinColumn(name="leadId")
	private LeadDO lead;
	
	private String company;
	private String participants;
	 @Temporal(TemporalType.DATE)
	private Date momDate;
	private String momTime;
	private String event;
	private String team;
	
	private String updatedby;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private String empId;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
	public Long getMomId() {
		return momId;
	}
	public void setMomId(Long momId) {
		this.momId = momId;
	}
	public EventsDO getEvents() {
		return events;
	}
	public void setEvents(EventsDO events) {
		this.events = events;
	}
	public LeadDO getLead() {
		return lead;
	}
	public void setLead(LeadDO lead) {
		this.lead = lead;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public Date getMomDate() {
		return momDate;
	}
	public void setMomDate(Date momDate) {
		this.momDate = momDate;
	}
	public String getMomTime() {
		return momTime;
	}
	public void setMomTime(String momTime) {
		this.momTime = momTime;
	}
	public String getParticipants() {
		return participants;
	}
	public void setParticipants(String participants) {
		this.participants = participants;
	}
	public String getEvent() {
		return event;
	}
	public void setEvent(String event) {
		this.event = event;
	}
	public String getTeam() {
		return team;
	}
	public void setTeam(String team) {
		this.team = team;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public String getEmpId() {
		return empId;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	
    
}