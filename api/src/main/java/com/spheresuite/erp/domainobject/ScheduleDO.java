package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="crm_schedule")
@TableGenerator(name ="crm_schedule", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "crm_schedule.findById", query = "SELECT r FROM ScheduleDO r where r.scheduleId =:id"),
    @NamedQuery(name = "crm_schedule.findByLeadId", query = "SELECT r FROM ScheduleDO r where r.lead.leadId =:id"),
    @NamedQuery(name = "crm_schedule.findUpcommingCalender", query = "SELECT r FROM ScheduleDO r where r.logDate >= now() AND r.empId=:empId order by r.logDate asc"),
    @NamedQuery(name = "crm_schedule.findUpcommingCalenderOfEmp", query = "SELECT r FROM ScheduleDO r where r.empId=:empId"),
})
public class ScheduleDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_BY_ID = "crm_schedule.findById";
	
	public static final String FIND_BY_LEAD_ID = "crm_schedule.findByLeadId";
	
	public static final String FIND_UPCOMMING_CALENDER = "crm_schedule.findUpcommingCalender";
	
	public static final String FIND_UPCOMMING_CALENDER_OF_EMP = "crm_schedule.findUpcommingCalenderOfEmp";
			
	
	@Id
	@Column(nullable = false, unique = true)
	@GeneratedValue(strategy=GenerationType.AUTO) 
	private Long scheduleId;
	
	@ManyToOne
	@JoinColumn(name="leadId")
	private LeadDO lead;
	
	private String description;
	private String subject;
	private String contactList;
	@Temporal(TemporalType.TIMESTAMP)
	private Date logDate;
	private String logTime;
	private String updatedBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    
	private String empId;

	public Long getScheduleId() {
		return scheduleId;
	}

	public void setScheduleId(Long scheduleId) {
		this.scheduleId = scheduleId;
	}

	public LeadDO getLead() {
		return lead;
	}

	public void setLead(LeadDO lead) {
		this.lead = lead;
	}

	public String getCreatedby() {
		return createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	public Date getCreatedon() {
		return createdon;
	}

	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}

	public String getEmpId() {
		return empId;
	}

	public void setEmpId(String empId) {
		this.empId = empId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContactList() {
		return contactList;
	}

	public void setContactList(String contactList) {
		this.contactList = contactList;
	}

	public Date getLogDate() {
		return logDate;
	}

	public void setLogDate(Date logDate) {
		this.logDate = logDate;
	}

	public String getLogTime() {
		return logTime;
	}

	public void setLogTime(String logTime) {
		this.logTime = logTime;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedon() {
		return updatedon;
	}

	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}

}