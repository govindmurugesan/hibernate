package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_leaverequests")
@TableGenerator(name ="hrms_leaverequests", initialValue =100101, allocationSize =1)
@NamedQueries({
	 @NamedQuery(name = "hrms_leaverequests.findAll", query = "SELECT r FROM LeaveRequestsDO r ORDER BY r.leaveRequestId desc"),
    @NamedQuery(name = "hrms_leaverequests.findById", query = "SELECT r FROM LeaveRequestsDO r where r.leaveRequestId =:id"),
    @NamedQuery(name = "hrms_leaverequests.findByEmpId", query = "SELECT r FROM LeaveRequestsDO r where r.employee.empId =:id"),
    @NamedQuery(name = "hrms_leaverequests.findByEmpIdStatus", query = "SELECT r FROM LeaveRequestsDO r where r.employee.empId =:id and r.status =:status"),
   /* @NamedQuery(name = "hrms_leaverequests.findReportByEmpId", query = "SELECT r FROM LeaveRequestsDO r where r.employee.empId =:id and r.leaveType.leaveTypeId =:type"),*/
    @NamedQuery(name = "hrms_leaverequests.findReportByEmpId", query = "SELECT r FROM LeaveRequestsDO r where r.employee.empId =:id and r.leaveManagement.leaveManagementId =:type"),
    @NamedQuery(name = "hrms_leaverequests.findLeaveByReprtingId", query = "SELECT r FROM LeaveRequestsDO r where r.reportingTo =:id"),
/*    @NamedQuery(name = "hrms_leaverequests.findLeaveByEmpId", query = "SELECT r FROM LeaveRequestsDO r where r.employee.empId =:id and r.leavetypefromDate =:leaveTypeFromDate and r.leavetypeToDate =:leaveTypeToDate and r.status =:status and r.leaveType.leaveTypeId =:type "),*/
  
    @NamedQuery(name = "hrms_leaverequests.findLeaveByEmpId", query = "SELECT r FROM LeaveRequestsDO r where r.employee.empId =:id and r.leavetypefromDate =:leaveTypeFromDate and r.leavetypeToDate =:leaveTypeToDate and r.status =:status and r.leaveManagement.leaveManagementId =:type "),
    // @NamedQuery(name = "leaverequests.findActive", query = "SELECT r FROM LeaveRequestsDO r where r.status =:status"),
   // @NamedQuery(name = "hrms_leaverequests.findForYear", query = "SELECT r FROM LeaveRequestsDO r where ((r.fromDate >=:fromDate and r.toDate >=:fromDate) or r.fromDate >=:fromDate and r.toDate IS NULL)) and (r.fromDate <=:toDate and r.toDate <=:toDate)  group by r.employee.empId"),
/*    @NamedQuery(name = "hrms_leaverequests.findForYear", query = "SELECT r FROM LeaveRequestsDO r where r.employee.empId =:id and  r.leaveType.leaveTypeId =:type and ((r.fromDate >=:fromDate and  r.toDate <=:toDate) or ((r.fromDate >=:fromDate and r.toDate IS NULL) and (r.fromDate <=:toDate)))"),*/
    @NamedQuery(name = "hrms_leaverequests.findForYear", query = "SELECT r FROM LeaveRequestsDO r where r.employee.empId =:id and  r.leaveManagement.leaveManagementId =:type and ((r.fromDate >=:fromDate and  r.toDate <=:toDate) or ((r.fromDate >=:fromDate and r.toDate IS NULL) and (r.fromDate <=:toDate)))"),
    
    @NamedQuery(name = "hrms_leaverequests.findPermission", query = "SELECT r FROM LeaveRequestsDO r where r.employee.empId =:id and r.status =:status and  r.type =:type and ((r.permisssionformhours >=:fromDate and  r.permisssiontohours <=:toDate) or ((r.permisssionformhours >=:fromDate and r.permisssiontohours IS NULL) and (r.permisssionformhours <=:toDate)))"),
    @NamedQuery(name = "hrms_leaverequests.findPermissionByEmpId", query = "SELECT r FROM LeaveRequestsDO r where r.employee.empId =:id and r.status =:status and r.type =:type"),
    @NamedQuery(name = "hrms_leaverequests.findByFromDate", query = "SELECT r FROM LeaveRequestsDO r where r.employee.empId =:id and r.status =:status and r.fromDate >=:fromDate)"),
    @NamedQuery(name = "hrms_leaverequests.findByFromToDate", query = "SELECT r FROM LeaveRequestsDO r where r.employee.empId =:id and r.status =:status and (r.fromDate between :fromDate and :toDate or r.toDate between :fromDate and :toDate))"),
    @NamedQuery(name = "hrms_leaverequests.findForPayroll", query = "SELECT r FROM LeaveRequestsDO r where r.employee.empId =:id  and r.status <>'a' and r.status <>'c' and r.status <>'r' and ((r.fromDate >=:fromDate and  r.toDate <=:toDate) or ((r.fromDate >=:fromDate and r.toDate IS NULL) and (r.fromDate <=:toDate)))"),
    @NamedQuery(name = "hrms_leaverequests.findForEmpReport", query = "SELECT r FROM LeaveRequestsDO r where r.employee.empId =:id and r.status =:status and r.leaveGroup.leaveGroupId =:leaveGroup and r.leaveManagement.leaveManagementId =:type and r.leavetypefromDate =:leaveTypeFromDate and r.leavetypeToDate =:leaveTypeToDate and (r.fromDate between :fromDate and :toDate or r.toDate between :fromDate and :toDate))"),
    @NamedQuery(name = "hrms_leaverequests.findAllPermissionForMonth", query = "SELECT r FROM LeaveRequestsDO r where r.employee.empId =:id and r.status =:status and r.type =:type and year(r.fromDate) =:year and month(r.fromDate) =:month"),
    @NamedQuery(name = "hrms_leaverequests.findEmpCompoffValidity", query = "SELECT r FROM LeaveRequestsDO r where r.employee.empId =:id and r.status <>'c' and r.status <>'r' and r.type =:type  and (r.fromDate >=:fromDate and r.fromDate <=:toDate)"),
    @NamedQuery(name = "hrms_leaverequests.findEmpCompoffValidityWithoutExpiry", query = "SELECT r FROM LeaveRequestsDO r where r.employee.empId =:id and r.status <>'c' and r.status <>'r' and r.type =:type and r.fromDate >=:fromDate "),
    @NamedQuery(name = "hrms_leaverequests.findPermissionMonthDetails", query = "SELECT r FROM LeaveRequestsDO r where r.employee.empId =:id and r.status <>'c' and r.status <>'r' and  r.type =:type and ((r.permisssionformhours >=:fromDate and  r.permisssiontohours <=:toDate) or ((r.permisssionformhours >=:fromDate and r.permisssiontohours IS NULL) and (r.permisssionformhours <=:toDate)))"),
    @NamedQuery(name = "hrms_leaverequests.findByFromDateWithType", query = "SELECT r FROM LeaveRequestsDO r where r.employee.empId =:id and r.leaveManagement.leaveManagementId =:type and r.status =:status and r.fromDate >=:fromDate)"),
    @NamedQuery(name = "hrms_leaverequests.findByFromToDateType", query = "SELECT r FROM LeaveRequestsDO r where r.employee.empId =:id and r.leaveManagement.leaveManagementId =:type and r.status =:status and (r.fromDate between :fromDate and :toDate or r.toDate between :fromDate and :toDate))"),
    @NamedQuery(name = "hrms_leaverequests.findByEmpIdStatusType", query = "SELECT r FROM LeaveRequestsDO r where r.employee.empId =:id and r.leaveManagement.leaveManagementId =:type and r.status =:status"),
})
public class LeaveRequestsDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static final String FIND_ALL= "hrms_leaverequests.findAll";
	
	public static final String FIND_ALL_PERMISSION_FORMONTH= "hrms_leaverequests.findAllPermissionForMonth";

	public static final String FIND_BY_ID = "hrms_leaverequests.findById";
	
	public static final String FINDBY_EMP_ID = "hrms_leaverequests.findByEmpId";
	
	public static final String FINDBY_EMP_ID_STATUS = "hrms_leaverequests.findByEmpIdStatus";
	
	public static final String FIND_LEAVE_BY_EMP_ID = "hrms_leaverequests.findLeaveByEmpId";
	
	public static final String FINDBY_REPORTING_ID = "hrms_leaverequests.findLeaveByReprtingId";
	
	public static final String FINDFORYEAR = "hrms_leaverequests.findForYear";
	
	public static final String FINDBYFROMDATE = "hrms_leaverequests.findByFromDate";
	
	public static final String FINDBYFROMTODATE  = "hrms_leaverequests.findByFromToDate";
	
	public static final String FINDREPORTBY_EMPID = "hrms_leaverequests.findReportByEmpId";
	
	public static final String FIND_PERMISSION = "hrms_leaverequests.findPermission";
	
	public static final String FIND_PERMISSION_BYEMPID = "hrms_leaverequests.findPermissionByEmpId";
	
	public static final String FIND_FORPAYROLL = "hrms_leaverequests.findForPayroll";
	
	public static final String FIND_EMPREPORT = "hrms_leaverequests.findForEmpReport";
	
	public static final String FIND_EMPCOMPOFFVALIDITY = "hrms_leaverequests.findEmpCompoffValidity";
	
	public static final String FIND_EMPCOMPOFFVALIDITYWITHOUTEXPIRY = "hrms_leaverequests.findEmpCompoffValidityWithoutExpiry";
	
	public static final String FIND_FINDPERMISSIONMONTHDETAILS = "hrms_leaverequests.findPermissionMonthDetails";
	
	public static final String FIND_FROMDATEWITHTYPE = "hrms_leaverequests.findByFromDateWithType";
	
	public static final String FINDBYFROMTODATETYPE = "hrms_leaverequests.findByFromToDateType";
	
	public static final String FINDBY_EMP_ID_STATUS_TYPE = "hrms_leaverequests.findByEmpIdStatusType";
	
	 
	
	
	
	
	
	
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long leaveRequestId;
	
	@Column(columnDefinition="LONGTEXT")
	private String description;
	private String numberofhours;
	//private String fromDate;
	//private String toDate;
	private char status;
	private String comments;
	private String type;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date permisssionformhours;
	@Temporal(TemporalType.TIMESTAMP)
	private Date permisssiontohours;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date leavetypefromDate;
	@Temporal(TemporalType.TIMESTAMP)
	private Date leavetypeToDate;
	
	
	@Temporal(TemporalType.DATE)
	private Date fromDate;
	@Temporal(TemporalType.DATE)
	private Date toDate;
	
	
	/*private String leavetypefromDate;
	private String leavetypeToDate;*/
	private String reportingTo;
	/*private String leaveType;*/
	
	@ManyToOne
	@JoinColumn(name="leaveManagementId")
	private LeaveManagementDO leaveManagement;
	
	@ManyToOne
	@JoinColumn(name="empId")
	private EmployeeDO employee;
	
	@ManyToOne
	@JoinColumn(name="leaveGroupId")
	private LeaveGroupDO leaveGroup;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getLeaveRequestId() {
		return leaveRequestId;
	}
	public void setLeaveRequestId(Long leaveRequestId) {
		this.leaveRequestId = leaveRequestId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getNumberofhours() {
		return numberofhours;
	}
	public void setNumberofhours(String numberofhours) {
		this.numberofhours = numberofhours;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getReportingTo() {
		return reportingTo;
	}
	public void setReportingTo(String reportingTo) {
		this.reportingTo = reportingTo;
	}
	public EmployeeDO getEmployee() {
		return employee;
	}
	public void setEmployee(EmployeeDO employee) {
		this.employee = employee;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public Date getLeavetypefromDate() {
		return leavetypefromDate;
	}
	public void setLeavetypefromDate(Date leavetypefromDate) {
		this.leavetypefromDate = leavetypefromDate;
	}
	public Date getLeavetypeToDate() {
		return leavetypeToDate;
	}
	public void setLeavetypeToDate(Date leavetypeToDate) {
		this.leavetypeToDate = leavetypeToDate;
	}
	/*public LeaveTypeDO getLeaveType() {
		return leaveType;
	}
	public void setLeaveType(LeaveTypeDO leaveType) {
		this.leaveType = leaveType;
	}*/
	public LeaveManagementDO getLeaveManagement() {
		return leaveManagement;
	}
	public void setLeaveManagement(LeaveManagementDO leaveManagement) {
		this.leaveManagement = leaveManagement;
	}
	
	/*public String getLeaveType() {
		return leaveType;
	}
	public void setLeaveType(String leaveType) {
		this.leaveType = leaveType;
	}*/
	public LeaveGroupDO getLeaveGroup() {
		return leaveGroup;
	}
	public void setLeaveGroup(LeaveGroupDO leaveGroup) {
		this.leaveGroup = leaveGroup;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public Date getPermisssionformhours() {
		return permisssionformhours;
	}
	public void setPermisssionformhours(Date permisssionformhours) {
		this.permisssionformhours = permisssionformhours;
	}
	public Date getPermisssiontohours() {
		return permisssiontohours;
	}
	public void setPermisssiontohours(Date permisssiontohours) {
		this.permisssiontohours = permisssiontohours;
	}
	
	/*public String getPermisssionformhours() {
		return permisssionformhours;
	}
	public void setPermisssionformhours(String permisssionformhours) {
		this.permisssionformhours = permisssionformhours;
	}
	public String getPermisssiontohours() {
		return permisssiontohours;
	}
	public void setPermisssiontohours(String permisssiontohours) {
		this.permisssiontohours = permisssiontohours;
	}*/
}