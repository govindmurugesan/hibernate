 package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="crm_master_salesstage")
@TableGenerator(name ="crm_master_salesstage", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "crm_master_salesstage.findById", query = "SELECT r FROM SalesStageDO r where r.salesStageId =:id"),
    @NamedQuery(name = "crm_master_salesstage.findByStatus", query = "SELECT r FROM SalesStageDO r where r.status =:status"),
    @NamedQuery(name = "crm_master_salesstage.findByName", query = "SELECT r FROM SalesStageDO r where r.name=:name"),
    @NamedQuery(name = "crm_master_salesstage.findByNameAndId", query = "SELECT r FROM SalesStageDO r where r.name=:name and r.salesStageId NOT IN :id"),
})
public class SalesStageDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_BY_ID = "crm_master_salesstage.findById";
	
	public static final String FIND_BY_STATUS = "crm_master_salesstage.findByStatus";
	
	public static final String FIND_BY_NAME = "crm_master_salesstage.findByName";
	
	public static final String FIND_FOR_UPDATE = "crm_master_salesstage.findByNameAndId";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long salesStageId;
	
	private String name;
	private char status;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getSalesStageId() {
		return salesStageId;
	}
	public void setSalesStageId(Long salesStageId) {
		this.salesStageId = salesStageId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}