package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_earnedleaves")
@TableGenerator(name ="hrms_earnedleaves", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "hrms_earnedleaves.findByempId", query = "SELECT r FROM EarnedLeavesDO r where r.employee.empId =:empId"),
   /* @NamedQuery(name = "hrms_earnedleaves.findByStatus", query = "SELECT r FROM AddressTypeDO r where r.status =:status"),
    @NamedQuery(name = "hrms_earnedleaves.findByName", query = "SELECT r FROM AddressTypeDO r where r.name=:name"),
    @NamedQuery(name = "hrms_earnedleaves.findByNameAndId", query = "SELECT r FROM AddressTypeDO r where r.name=:name and r.addressTypeId NOT IN :id"),*/
})
public class EarnedLeavesDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	/*public static final String FIND_BY_ID = "master_addresstype.findById";
	
	public static final String FIND_BY_STATUS = "master_addresstype.findByStatus";
	
	public static final String FIND_BY_NAME = "master_addresstype.findByName";
	
	public static final String FIND_FOR_UPDATE = "master_addresstype.findByNameAndId";*/
	
	
	public static final String FIND_BY_EMPID = "hrms_earnedleaves.findByempId";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long earnedLeaveId;
	
	@ManyToOne
	@JoinColumn(name="empId")
	private EmployeeDO employee;
	
	@Temporal(TemporalType.TIMESTAMP)
    private Date fromDate;
	
	@Temporal(TemporalType.TIMESTAMP)
    private Date toDate;
	
	/*private Long availedLeaves;*/
	private Long availableLeaves;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
	public Long getEarnedLeaveId() {
		return earnedLeaveId;
	}
	public void setEarnedLeaveId(Long earnedLeaveId) {
		this.earnedLeaveId = earnedLeaveId;
	}
	public EmployeeDO getEmployee() {
		return employee;
	}
	public void setEmployee(EmployeeDO employee) {
		this.employee = employee;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	/*public Long getAvailedLeaves() {
		return availedLeaves;
	}
	public void setAvailedLeaves(Long availedLeaves) {
		this.availedLeaves = availedLeaves;
	}*/
	public Long getAvailableLeaves() {
		return availableLeaves;
	}
	public void setAvailableLeaves(Long availableLeaves) {
		this.availableLeaves = availableLeaves;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}