package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="master_country")
@TableGenerator(name ="master_country", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "master_country.findById", query = "SELECT r FROM CountryDO r where r.countryId =:id"),
    @NamedQuery(name = "master_country.findByName", query = "SELECT r FROM CountryDO r where r.countryName=:name"),
    @NamedQuery(name = "master_country.findByStatus", query = "SELECT r FROM CountryDO r where r.status =:status"),
    @NamedQuery(name = "master_country.findByNameAndId", query = "SELECT r FROM CountryDO r where r.countryName=:name and r.countryId NOT IN :id"),
})
public class CountryDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_BY_ID = "master_country.findById";
	
	public static final String FIND_BY_STATUS = "master_country.findByStatus";
	
	public static final String FIND_BY_NAME = "master_country.findByName";
	
	public static final String FIND_FOR_UPDATE = "master_country.findByNameAndId";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
    private Long countryId;
    
    private String countryName;
    private String countryCode;
    private String callingCode;
    private String currencyType;
    private char status;
    
    /*@OneToMany(mappedBy="country", fetch = FetchType.EAGER)
    private Set<StateDO> state;
    
    @OneToMany(mappedBy="country", fetch = FetchType.EAGER)
    private Set<UnitOrBranchDO> unitOrBranch;*/
    
    private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getCountryId() {
		return countryId;
	}
	public void setCountryId(Long countryId) {
		this.countryId = countryId;
	}
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public String getCallingCode() {
		return callingCode;
	}
	public void setCallingCode(String callingCode) {
		this.callingCode = callingCode;
	}
	public String getCurrencyType() {
		return currencyType;
	}
	public void setCurrencyType(String currencyType) {
		this.currencyType = currencyType;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	/*public Set<StateDO> getState() {
		return state;
	}
	public void setState(Set<StateDO> state) {
		this.state = state;
	}
	public Set<UnitOrBranchDO> getUnitOrBranch() {
		return unitOrBranch;
	}
	public void setUnitOrBranch(Set<UnitOrBranchDO> unitOrBranch) {
		this.unitOrBranch = unitOrBranch;
	}*/
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}