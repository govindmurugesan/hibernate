package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_employeectc")
@TableGenerator(name ="hrms_employeectc", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "hrms_employeectc.findById", query = "SELECT r FROM EmployeeCtcDO r where r.empCtcId =:id"),
    @NamedQuery(name = "hrms_employeectc.findByStatusAndEmpId", query = "SELECT r FROM EmployeeCtcDO r where r.status =:status and r.employee.empId =:empId"),
    @NamedQuery(name = "hrms_employeectc.findByEmpIdAndDate", query = "SELECT r FROM EmployeeCtcDO r where r.employee.empId =:empId and (r.startdate <=:date and r.enddate >=:date or r.startdate <=:date and r.enddate IS NULL)"),
    @NamedQuery(name = "hrms_employeectc.findByEmpId", query = "SELECT r FROM EmployeeCtcDO r where r.employee.empId =:empId"),
    @NamedQuery(name = "hrms_employeectc.findByStatus", query = "SELECT r FROM EmployeeCtcDO r where r.status =:status order by r.employee.empId desc"),
    @NamedQuery(name = "hrms_employeectc.findByBatchId", query = "SELECT r.employee.empId FROM EmployeeCtcDO r where r.payrollBatch.payrollBatchId =:payrollbatchId"),
    
    @NamedQuery(name = "hrms_employeectc.findByBatchAndMonth", query = "SELECT r FROM EmployeeCtcDO r where r.payrollBatch.payrollBatchId =:id and ((r.startdate <=:date and r.enddate >=:date) or (r.startdate <=:date and r.enddate IS NULL))"),
    @NamedQuery(name = "hrms_employeectc.findByEmployeectc", query = "SELECT r FROM EmployeeCtcDO r where r.employee.empId =:empId and r.empctc=:empctc and r.status =:status"),	
    @NamedQuery(name = "hrms_employeectc.findWeeklyBatchAndMonth", query = "SELECT r FROM EmployeeCtcDO r where r.payrollBatch.payrollBatchId =:id and r.payrollBatch.payrollType =:type and ((r.startdate <=:date and r.enddate >=:date) or (r.startdate <=:date and r.enddate IS NULL))"),

})
public class EmployeeCtcDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_BY_ID = "hrms_employeectc.findById";
	
	public static final String FIND_BY_STATUS_EMP_ID = "hrms_employeectc.findByStatusAndEmpId";
	
	public static final String FIND_BY_EMP_ID = "hrms_employeectc.findByEmpId";
	
	public static final String FIND_BY_EMPID_DATE = "hrms_employeectc.findByEmpIdAndDate";
	
	public static final String FIND_BY_BATCH_ID = "hrms_employeectc.findByBatchId";
	
	public static final String FIND_BY_STATUS = "hrms_employeectc.findByStatus";
	
	public static final String FIND_BY_BATCH_MONTH  = "hrms_employeectc.findByBatchAndMonth";
	
	public static final String FIND_BY_EMPLOYEE_CTC  =  "hrms_employeectc.findByEmployeectc";
	
	public static final String FIND_WEEKLY_BATCH_MONTH  = "hrms_employeectc.findWeeklyBatchAndMonth";
	
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO) 
	@Column(nullable = false, unique = true)
	private Long empCtcId;
	
	private Long empctc;
	private char status;
	private String ctcType;
	@Temporal(TemporalType.DATE)
	private Date startdate;
	@Temporal(TemporalType.DATE)
	private Date enddate;
	
	@ManyToOne
	@JoinColumn(name="empId")
	private EmployeeDO employee;
	
	@ManyToOne
	@JoinColumn(name="payrollBatchId")
	private PayrollBatchDO payrollBatch;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getEmpCtcId() {
		return empCtcId;
	}
	public void setEmpCtcId(Long empCtcId) {
		this.empCtcId = empCtcId;
	}
	public Long getEmpctc() {
		return empctc;
	}
	public void setEmpctc(Long empctc) {
		this.empctc = empctc;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getCtcType() {
		return ctcType;
	}
	public void setCtcType(String ctcType) {
		this.ctcType = ctcType;
	}
	public Date getStartdate() {
		return startdate;
	}
	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}
	public Date getEnddate() {
		return enddate;
	}
	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}
	public EmployeeDO getEmployee() {
		return employee;
	}
	public void setEmployee(EmployeeDO employee) {
		this.employee = employee;
	}
	public PayrollBatchDO getPayrollBatch() {
		return payrollBatch;
	}
	public void setPayrollBatch(PayrollBatchDO payrollBatch) {
		this.payrollBatch = payrollBatch;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}