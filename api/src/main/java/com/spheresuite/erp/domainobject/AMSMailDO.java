package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="ams_master_mail")
@TableGenerator(name ="ams_master_mail", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "ams_master_mail.findAll", query = "SELECT r FROM AMSMailDO r"),
    @NamedQuery(name = "ams_master_mail.findById", query = "SELECT r FROM AMSMailDO r where r.mailId =:id"),
    @NamedQuery(name = "ams_master_mail.findByStatus", query = "SELECT r FROM AMSMailDO r where r.status =:status"),
    @NamedQuery(name = "ams_master_mail.findByName", query = "SELECT r FROM AMSMailDO r where r.status =:status and r.mail_name=:name"),
   @NamedQuery(name = "ams_master_mail.findByNameAndId", query = "SELECT r FROM AMSMailDO r where r.status =:status and r.mail_name=:name and r.mailId NOT IN :id"),
})
public class AMSMailDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "ams_master_mail.findAll";
	public static final String FIND_BY_ID = "ams_master_mail.findById";
	public static final String FIND_BY_STATUS = "ams_master_mail.findByStatus";
	public static final String FIND_BY_NAME = "ams_master_mail.findByName";
	public static final String FIND_FOR_UPDATE = "ams_master_mail.findByNameAndId";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO) 
	@Column(nullable = false, unique = true) 
	private Long mailId;
	
	private String mail_name;
	private String mail_comments;
	private char status;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
	public Long getMailId() {
		return mailId;
	}
	public void setMailId(Long mailId) {
		this.mailId = mailId;
	}
	public String getMail_name() {
		return mail_name;
	}
	public void setMail_name(String mail_name) {
		this.mail_name = mail_name;
	}
	public String getMail_comments() {
		return mail_comments;
	}
	public void setMail_comments(String mail_comments) {
		this.mail_comments = mail_comments;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}