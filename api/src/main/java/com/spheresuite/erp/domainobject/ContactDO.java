package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="crm_contact")
@TableGenerator(name ="crm_contact", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "crm_contact.findAll", query = "SELECT r FROM ContactDO r where r.empId IN :id and r.isDeleted=:status"),
    @NamedQuery(name = "crm_contact.findById", query = "SELECT r FROM ContactDO r where r.contactId =:id and r.isDeleted=:status"),
    @NamedQuery(name = "crm_contact.findByLeadId", query = "SELECT r FROM ContactDO r where r.lead.leadId =:id and r.isDeleted=:status"),
    @NamedQuery(name = "crm_contact.findAllData", query = "SELECT r FROM ContactDO r  where r.isDeleted=:status"),
    @NamedQuery(name = "crm_contact.findByEmailId", query = "SELECT r FROM ContactDO r where r.primaryemail =:email and r.isDeleted=:status"),
})
public class ContactDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "crm_contact.findAll";
	
	public static final String FIND_ALL_DATA = "crm_contact.findAllData";
	
	public static final String FIND_BY_ID = "crm_contact.findById";
	
	public static final String FIND_BY_EMAIL_ID = "crm_contact.findByEmailId";
	
	public static final String FIND_BY_LEADID = "crm_contact.findByLeadId";
	
	@Id
	@Column(nullable = false, unique = true)
	@GeneratedValue(strategy=GenerationType.AUTO) 
	private Long contactId;
	
	//private Long leadtype;
	@ManyToOne
	@JoinColumn(name="leadId")
	private LeadDO lead;
	
	//private Long salutation;
	@ManyToOne
	@JoinColumn(name="salutationId")
	private SalutationDO salutation;
	
	private String firstname;
	private String lastname;
	private String middlename;
	private String designation;
	
	//private Long contattype;
	@ManyToOne
	@JoinColumn(name="contactTypeId")
	private ContactTypeDO contactType;
	
	private String mobile1;
	private Long mobile2;
	private Long phone1;
	private Long phone2;
	private Long phone3;
	private Long phone4;
	private String address1;
	private String address2;
	private String primaryemail;
	private String secondaryemail;
	private Long fax;
	private String website;
	private String requirements;
	private String notes;
	private String comments;
	private String updatedby;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private String empId;
	private String isDeleted;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
	
    
	public Long getContactId() {
		return contactId;
	}
	public void setContactId(Long contactId) {
		this.contactId = contactId;
	}
	public LeadDO getLead() {
		return lead;
	}
	public void setLead(LeadDO lead) {
		this.lead = lead;
	}
	public SalutationDO getSalutation() {
		return salutation;
	}
	public void setSalutation(SalutationDO salutation) {
		this.salutation = salutation;
	}
	public ContactTypeDO getContactType() {
		return contactType;
	}
	public void setContactType(ContactTypeDO contactType) {
		this.contactType = contactType;
	}
	public String getEmpId() {
		return empId;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getMiddlename() {
		return middlename;
	}
	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getMobile1() {
		return mobile1;
	}
	public void setMobile1(String mobile1) {
		this.mobile1 = mobile1;
	}
	public Long getMobile2() {
		return mobile2;
	}
	public void setMobile2(Long mobile2) {
		this.mobile2 = mobile2;
	}
	public Long getPhone1() {
		return phone1;
	}
	public void setPhone1(Long phone1) {
		this.phone1 = phone1;
	}
	public Long getPhone2() {
		return phone2;
	}
	public void setPhone2(Long phone2) {
		this.phone2 = phone2;
	}
	public Long getPhone3() {
		return phone3;
	}
	public void setPhone3(Long phone3) {
		this.phone3 = phone3;
	}
	public Long getPhone4() {
		return phone4;
	}
	public void setPhone4(Long phone4) {
		this.phone4 = phone4;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getPrimaryemail() {
		return primaryemail;
	}
	public void setPrimaryemail(String primaryemail) {
		this.primaryemail = primaryemail;
	}
	public String getSecondaryemail() {
		return secondaryemail;
	}
	public void setSecondaryemail(String secondaryemail) {
		this.secondaryemail = secondaryemail;
	}
	public Long getFax() {
		return fax;
	}
	public void setFax(Long fax) {
		this.fax = fax;
	}
	public String getWebsite() {
		return website;
	}
	public void setWebsite(String website) {
		this.website = website;
	}
	public String getRequirements() {
		return requirements;
	}
	public void setRequirements(String requirements) {
		this.requirements = requirements;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	
	public String getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}
}