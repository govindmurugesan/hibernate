package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="crm_mom_notes")
@TableGenerator(name ="crm_mom_notes", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "crm_mom_notes.findByMoMId", query = "SELECT r FROM MinutesOfMeetingNotesDO r where r.mom.momId =:momId"),
    @NamedQuery(name = "crm_mom_notes.findByMoMIdPricing", query = "SELECT r FROM MinutesOfMeetingNotesDO r where r.mom.momId =:momId and r.pricing =:pricing"),
    @NamedQuery(name = "crm_mom_notes.findByMoMIdSamples", query = "SELECT r FROM MinutesOfMeetingNotesDO r where r.mom.momId =:momId and r.sample =:sample"),
    @NamedQuery(name = "crm_mom_notes.findByMoMIdFlloweups", query = "SELECT r FROM MinutesOfMeetingNotesDO r where r.mom.momId =:momId and r.followups =:followups"),
    @NamedQuery(name = "crm_mom_notes.findByMoMIdPricingSampleFlloweups", query = "SELECT r FROM MinutesOfMeetingNotesDO r where r.mom.momId =:momId and r.pricing =:pricing and r.sample =:sample and r.followups =:followups"),
    @NamedQuery(name = "crm_mom_notes.findByMoMIdFlloweupsSamples", query = "SELECT r FROM MinutesOfMeetingNotesDO r where r.mom.momId =:momId and r.sample =:sample and r.followups =:followups"),
    @NamedQuery(name = "crm_mom_notes.findByMoMIdFlloweupsPricing", query = "SELECT r FROM MinutesOfMeetingNotesDO r where r.mom.momId =:momId and r.pricing =:pricing and r.followups =:followups "),
    @NamedQuery(name = "crm_mom_notes.findByMoMIdSamplesPricing", query = "SELECT r FROM MinutesOfMeetingNotesDO r where r.mom.momId =:momId and r.pricing =:pricing and r.sample =:sample"),
})
public class MinutesOfMeetingNotesDO implements Serializable {
	private static final long serialVersionUID = 1L;

	
	public static final String FIND_BY_MOM_ID = "crm_mom_notes.findByMoMId";	
	
	public static final String FIND_BY_MOM_ID_PRICING = "crm_mom_notes.findByMoMIdPricing";
	
	public static final String FIND_BY_MOM_ID_SAMPLES = "crm_mom_notes.findByMoMIdSamples";
	
	public static final String FIND_BY_MOM_ID_FLLOWEUPS = "crm_mom_notes.findByMoMIdFlloweups";
	
	public static final String FIND_BY_MOM_ID_FLLOWEUPS_SAMPLES_PRICING = "crm_mom_notes.findByMoMIdPricingSampleFlloweups";
	
	public static final String FIND_BY_MOM_ID_FLLOWEUPS_SAMPLES = "crm_mom_notes.findByMoMIdFlloweupsSamples";
	
	public static final String FIND_BY_MOM_ID_FLLOWEUPS_PRICING = "crm_mom_notes.findByMoMIdFlloweupsPricing";
	
	public static final String FIND_BY_MOM_ID_SAMPLES_PRICING = "crm_mom_notes.findByMoMIdSamplesPricing";
	
	@Id
	@Column(nullable = false, unique = true)
	@GeneratedValue(strategy=GenerationType.AUTO) 
	private Long momNotesId;
	
	@ManyToOne
	@JoinColumn(name="momId")
	private MinutesOfMeetingDO mom;
	//private Long proposalId;
	
	private String description;
	private String sample;
	private String pricing;
	private String followups;
	private String updatedby;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private String empId;
	private Long indexValue;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
	public Long getMomNotesId() {
		return momNotesId;
	}
	public void setMomNotesId(Long momNotesId) {
		this.momNotesId = momNotesId;
	}
	public MinutesOfMeetingDO getMom() {
		return mom;
	}
	public void setMom(MinutesOfMeetingDO mom) {
		this.mom = mom;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getSample() {
		return sample;
	}
	public void setSample(String sample) {
		this.sample = sample;
	}
	public String getPricing() {
		return pricing;
	}
	public void setPricing(String pricing) {
		this.pricing = pricing;
	}
	public String getFollowups() {
		return followups;
	}
	public void setFollowups(String followups) {
		this.followups = followups;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public String getEmpId() {
		return empId;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
	public Long getIndexValue() {
		return indexValue;
	}
	public void setIndexValue(Long indexValue) {
		this.indexValue = indexValue;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
}