package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="greige_quotationDetails")
@TableGenerator(name ="greige_quotationDetails", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "greige_quotationDetails.findById", query = "SELECT r FROM Greige_QuotatioDetailsDO r where r.quoteDetailId =:id"),
    @NamedQuery(name = "greige_createQuotation.findQuotId", query = "SELECT r FROM Greige_QuotatioDetailsDO r where r.quotation.quoteId =:id"),
   
  //  @NamedQuery(name = "greige_createQuotation.findQuoteByStatus", query = "SELECT r FROM Greige_QuotatioDetailsDO r where r.status =:status"),
  //  @NamedQuery(name = "greige_createQuotation.findSortId", query = "SELECT r FROM Greige_QuotatioDetailsDO r where r.sortName.sortId =:id"),
  //  @NamedQuery(name = "greige_pendingOrders.findByStatus", query = "SELECT r FROM Greige_pendingOrders r where r.status =:status"),
/*    @NamedQuery(name = "ep_master_sort.findByName", query = "SELECT r FROM EarningTypeDO r where r.name=:name"),
    @NamedQuery(name = "ep_master_sort.findByNameAndId", query = "SELECT r FROM EarningTypeDO r where r.name=:name and r.earningTypeId NOT IN :id"),*/
})
public class Greige_QuotatioDetailsDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_BY_ID = "greige_quotationDetails.findById";
	
	public static final String FIND_BY_QUOTID = "greige_createQuotation.findQuotId";
	

	
	
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long quoteDetailId;
	
	@ManyToOne
	@JoinColumn(name="quoteId")
	private Greige_CreateQuotationDO quotation;
	
	@ManyToOne
	@JoinColumn(name="pendingOrderId")
	private Greige_PendingOrdersDO ocn;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
	public Long getQuoteDetailId() {
		return quoteDetailId;
	}
	public void setQuoteDetailId(Long quoteDetailId) {
		this.quoteDetailId = quoteDetailId;
	}
	
	public Greige_CreateQuotationDO getQuotation() {
		return quotation;
	}
	public void setQuotation(Greige_CreateQuotationDO quotation) {
		this.quotation = quotation;
	}
	public Greige_PendingOrdersDO getOcn() {
		return ocn;
	}
	public void setOcn(Greige_PendingOrdersDO ocn) {
		this.ocn = ocn;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}