package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="greige_spplier_quote")
@TableGenerator(name ="greige_spplier_quote", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "greige_spplier_quote.findById", query = "SELECT r FROM Greige_Spplier_QuoteDO r where r.supplierQuoteId =:id"),
    @NamedQuery(name = "greige_spplier_quote.findByQuoteIdSupplierId", query = "SELECT r FROM Greige_Spplier_QuoteDO r where r.supplierQuoteId=:id AND r.quotation.quoteId =:quotId AND supplierId =:supplierId ORDER BY CASE r.status WHEN 'r' THEN 1 WHEN 'p' THEN 2 WHEN 'q' THEN 3 WHEN 's' THEN 4 WHEN 'd' THEN 4 WHEN 'a' THEN 5 WHEN 'r' THEN 6  END"),
    @NamedQuery(name = "greige_spplier_quote.findByStatus", query = "SELECT r FROM Greige_Spplier_QuoteDO r where r.status =:status order by updatedon desc"),
    @NamedQuery(name = "greige_spplier_quote.findBySpplierId", query = "SELECT r FROM Greige_Spplier_QuoteDO r where r.supplierId =:id and r.quotation.quoteType =:type ORDER BY CASE r.status WHEN 'r' THEN 1 WHEN 'p' THEN 2 WHEN 'q' THEN 3 WHEN 's' THEN 4 WHEN 'd' THEN 4 WHEN 'a' THEN 5 WHEN 'r' THEN 6  END"),
    @NamedQuery(name = "greige_spplier_quote.findByQuoteId", query = "SELECT r FROM Greige_Spplier_QuoteDO r where r.quotation.quoteId =:id ORDER BY CASE r.status WHEN 'r' THEN 1 WHEN 'p' THEN 2 WHEN 'q' THEN 3 WHEN 's' THEN 4 WHEN 'd' THEN 4 WHEN 'a' THEN 5 WHEN 'r' THEN 6  END"),
    @NamedQuery(name = "greige_spplier_quote.findByQuoteSubmittedId", query = "SELECT r FROM Greige_Spplier_QuoteDO r where r.quotation.quoteId =:id AND (r.status =:status)"),
    @NamedQuery(name = "greige_spplier_quote.findByQuoteApprove", query = "SELECT r FROM Greige_Spplier_QuoteDO r where r.quotation.quoteId =:id AND (r.status ='a' OR r.status ='s')"),
    @NamedQuery(name = "greige_spplier_quote.findByAcceptCost", query = "SELECT r FROM Greige_Spplier_QuoteDO r where r.acceptCost =:acceptCost"),
    @NamedQuery(name = "greige_spplier_quote.findBySortId", query = "SELECT r FROM Greige_Spplier_QuoteDO r where r.quotation.quoteId =:id AND (r.status =:status)"),
    @NamedQuery(name = "greige_spplier_quote.findApproveQuote", query = "SELECT r FROM Greige_Spplier_QuoteDO r where (r.status =:status or r.status =:approvedStatus) and r.acceptCost='a' and r.submitted='true' ORDER BY CASE r.status WHEN 'd' THEN 1 WHEN 'r' THEN 2 WHEN 's' THEN 3 WHEN 'a' THEN 4  END"),
    @NamedQuery(name = "greige_spplier_quote.findReceivedQuote", query = "SELECT r FROM Greige_Spplier_QuoteDO r where r.status =:status ORDER BY CASE r.status WHEN 'd' THEN 1 WHEN 'r' THEN 2 WHEN 's' THEN 3 WHEN 'a' THEN 4  END"),
    @NamedQuery(name = "greige_spplier_quote.findQuoteAlert", query = "SELECT r FROM Greige_Spplier_QuoteDO r where r.acceptCost IS NULL and r.status =:status and DATE(r.updatedon) = CURRENT_DATE and MINUTE(now()) = MINUTE(r.updatedon) and HOUR(now()) = HOUR(r.updatedon)"),
    /*    @NamedQuery(name = "ep_master_sort.findByName", query = "SELECT r FROM EarningTypeDO r where r.name=:name"),
    @NamedQuery(name = "ep_master_sort.findByNameAndId", query = "SELECT r FROM EarningTypeDO r where r.name=:name and r.earningTypeId NOT IN :id"),*/
})
public class Greige_Spplier_QuoteDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_BY_ID = "greige_spplier_quote.findById";
	
	public static final String FIND_BY_QUOTEID_SUPPLIERID = "greige_spplier_quote.findByQuoteIdSupplierId";
	
	public static final String FIND_BY_STATUS = "greige_spplier_quote.findByStatus";
	
	public static final String FIND_BY_SPPLIERID = "greige_spplier_quote.findBySpplierId";
	
	public static final String FIND_BY_QUOTEID = "greige_spplier_quote.findByQuoteId";
	
	public static final String FIND_BY_QUOTE_SUBMITTED = "greige_spplier_quote.findByQuoteSubmittedId";
	
	public static final String FIND_BY_QUOTE_APPROVE = "greige_spplier_quote.findByQuoteApprove";
	
	public static final String FIND_BY_ACCEPT_COST = "greige_spplier_quote.findByAcceptCost";
	
	public static final String FIND_QUOTEBYAPPROVESTATUS = "greige_spplier_quote.findApproveQuote";
	
	public static final String FIND_QUOTEBYRECEIVED = "greige_spplier_quote.findReceivedQuote";
	
	public static final String FIND_QUOTEALERT = "greige_spplier_quote.findQuoteAlert";
	
	//public static final String FIND_BY_STATUS = "greige_pendingOrders.findByStatus";
	
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long supplierQuoteId;
	
	private String supplierId;
	private String dent;
	private String reedcount;
	private String grcons;
	private String gw;
	private String qtym;
	private String exmillrate;
	private Long finalizedrate;
	private Long targetprice;
	private String targetQty;
	private String finalizedQty;
	private String remarks;
	private String status;
	private String submitted;
	private String acceptCost;
	
	@ManyToOne
	@JoinColumn(name="quoteId")
	private Greige_CreateQuotationDO quotation;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
	public Long getSupplierQuoteId() {
		return supplierQuoteId;
	}
	public void setSupplierQuoteId(Long supplierQuoteId) {
		this.supplierQuoteId = supplierQuoteId;
	}
	public String getSupplierId() {
		return supplierId;
	}
	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}
	public String getReedcount() {
		return reedcount;
	}
	public void setReedcount(String reedcount) {
		this.reedcount = reedcount;
	}
	public String getGrcons() {
		return grcons;
	}
	public void setGrcons(String grcons) {
		this.grcons = grcons;
	}
	public String getGw() {
		return gw;
	}
	public void setGw(String gw) {
		this.gw = gw;
	}
	public String getQtym() {
		return qtym;
	}
	public void setQtym(String qtym) {
		this.qtym = qtym;
	}
	public String getExmillrate() {
		return exmillrate;
	}
	public void setExmillrate(String exmillrate) {
		this.exmillrate = exmillrate;
	}
	public Long getFinalizedrate() {
		return finalizedrate;
	}
	public void setFinalizedrate(Long finalizedrate) {
		this.finalizedrate = finalizedrate;
	}
	public String getDent() {
		return dent;
	}
	public void setDent(String dent) {
		this.dent = dent;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public Greige_CreateQuotationDO getQuotation() {
		return quotation;
	}
	public void setQuotation(Greige_CreateQuotationDO quotation) {
		this.quotation = quotation;
	}
	public Long getTargetprice() {
		return targetprice;
	}
	public void setTargetprice(Long targetprice) {
		this.targetprice = targetprice;
	}
	public String getTargetQty() {
		return targetQty;
	}
	public void setTargetQty(String targetQty) {
		this.targetQty = targetQty;
	}
	public String getFinalizedQty() {
		return finalizedQty;
	}
	public void setFinalizedQty(String finalizedQty) {
		this.finalizedQty = finalizedQty;
	}
	public String getSubmitted() {
		return submitted;
	}
	public void setSubmitted(String submitted) {
		this.submitted = submitted;
	}
	public String getAcceptCost() {
		return acceptCost;
	}
	public void setAcceptCost(String acceptCost) {
		this.acceptCost = acceptCost;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}