package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_master_monthlypermission")
@TableGenerator(name ="hrms_master_monthlypermission", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "hrms_master_monthlypermission.findById", query = "SELECT r FROM MonthlyPermissionDO r where r.monthlyPermissionId =:id"),
    @NamedQuery(name = "hrms_master_monthlypermission.findByStatus", query = "SELECT r FROM MonthlyPermissionDO r where r.status =:status"),
})
public class MonthlyPermissionDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_BY_ID = "hrms_master_monthlypermission.findById";
	
	public static final String FIND_BY_STATUS = "hrms_master_monthlypermission.findByStatus";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long monthlyPermissionId;
	private String permissionHoursPerDay;
	private String permissionHoursPerMonth;
	private Double numberOfPermissionPerMonth;
	private char status;
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
	
    public Long getMonthlyPermissionId() {
		return monthlyPermissionId;
	}
	public void setMonthlyPermissionId(Long monthlyPermissionId) {
		this.monthlyPermissionId = monthlyPermissionId;
	}
	
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public String getPermissionHoursPerDay() {
		return permissionHoursPerDay;
	}
	public void setPermissionHoursPerDay(String permissionHoursPerDay) {
		this.permissionHoursPerDay = permissionHoursPerDay;
	}
	public String getPermissionHoursPerMonth() {
		return permissionHoursPerMonth;
	}
	public void setPermissionHoursPerMonth(String permissionHoursPerMonth) {
		this.permissionHoursPerMonth = permissionHoursPerMonth;
	}
	public Double getNumberOfPermissionPerMonth() {
		return numberOfPermissionPerMonth;
	}
	public void setNumberOfPermissionPerMonth(Double numberOfPermissionPerMonth) {
		this.numberOfPermissionPerMonth = numberOfPermissionPerMonth;
	}
}