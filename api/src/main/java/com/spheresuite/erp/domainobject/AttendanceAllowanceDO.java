package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_master_attendanceallowance")
@TableGenerator(name ="hrms_master_attendanceallowance", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "hrms_master_attendanceallowance.findById", query = "SELECT r FROM AttendanceAllowanceDO r where r.attendanceAllowanceId =:id"),
    @NamedQuery(name = "hrms_master_attendanceallowance.findByName", query = "SELECT r FROM AttendanceAllowanceDO r where r.name=:name"),
    @NamedQuery(name = "hrms_master_attendanceallowance.findByNameAndId", query = "SELECT r FROM AttendanceAllowanceDO r where r.name=:name and r.attendanceAllowanceId NOT IN :id"),
   /* @NamedQuery(name = "hrms_master_attendanceallowance.findByStatus", query = "SELECT r FROM AddressTypeDO r where r.status =:status"),

    */
})
public class AttendanceAllowanceDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static final String FIND_BY_ID = "hrms_master_attendanceallowance.findById";
	
	public static final String FIND_BY_NAME = "hrms_master_attendanceallowance.findByName";
	
	public static final String FIND_FOR_UPDATE = "hrms_master_attendanceallowance.findByNameAndId";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long attendanceAllowanceId;
	private String name;
	private Double amount;
	private String status;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getAttendanceAllowanceId() {
		return attendanceAllowanceId;
	}
	public void setAttendanceAllowanceId(Long attendanceAllowanceId) {
		this.attendanceAllowanceId = attendanceAllowanceId;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}