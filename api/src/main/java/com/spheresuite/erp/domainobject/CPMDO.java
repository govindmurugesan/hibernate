package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="pl_master_cpm")
@TableGenerator(name ="pl_master_cpm", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "pl_master_cpm.findById", query = "SELECT r FROM CPMDO r where r.cpmId =:id"),
    @NamedQuery(name = "pl_master_cpm.findByMonth", query = "SELECT r FROM CPMDO r where r.month =:month"),
    @NamedQuery(name = "pl_master_cpm.findByStatus", query = "SELECT r FROM CPMDO r where r.status=:status"),
    @NamedQuery(name = "pl_master_cpm.findAll", query = "SELECT r FROM CPMDO r"),
})
public class CPMDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static final String FIND_BY_ID = "pl_master_cpm.findById";
	
	public static final String FIND_BY_MONTH = "pl_master_cpm.findByMonth";
	
	public static final String FIND_ALL = "pl_master_cpm.findAll";
	
	public static final String FIND_BY_STATUS = "pl_master_cpm.findByStatus";
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO) 
	@Column(nullable = false, unique = true)
	private Long cpmId;
	
	private String month;
	private String amount;
	private String status;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getCpmId() {
		return cpmId;
	}
	public void setCpmId(Long cpmId) {
		this.cpmId = cpmId;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
    
}