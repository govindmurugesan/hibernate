package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name="hrms_attendance")
@TableGenerator(name ="hrms_attendance", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "hrms_attendance.findByEmpId", query = "SELECT r FROM AttendanceDO r where r.employee.empId =:id"),
    @NamedQuery(name = "hrms_attendance.findById", query = "SELECT r FROM AttendanceDO r where r.attendanceId =:id"),
    @NamedQuery(name = "hrms_attendance.specificfield", query = "SELECT  r.attendanceId,r.timeInDate,r.timeOutDate,r.employee.empId,r.employee.firstname,r.employee.middlename,r.employee.lastname,"
    		+ "r.timeIn,r.timeOut, r.attendanceType FROM AttendanceDO r"),
    @NamedQuery(name = "hrms_attendance.findByTeam", query = "SELECT r FROM AttendanceDO r where r.employee.empId IN :id"),
    @NamedQuery(name = "hrms_attendance.findByEmpIdAndTime", query = "SELECT r FROM AttendanceDO r where r.employee.empId =:id and (r.timeIn >=:fromDate and r.timeIn <=:enddate )"),
    @NamedQuery(name = "hrms_attendance.findByEmpIdAndDate", query = "SELECT r FROM AttendanceDO r where r.employee.empId =:id and (r.timeInDate >=:fromDate and r.timeInDate <=:enddate)"),
    /*  @NamedQuery(name = "hrms_master_leavetype.findByName", query = "SELECT r FROM LeaveTypeDO r where r.type =:name"),
    @NamedQuery(name = "hrms_master_leavetype.findByType", query = "SELECT r FROM LeaveTypeDO r where r.type =:type and r.status =:status"),
    @NamedQuery(name = "hrms_master_leavetype.findByTypeForUpdate", query = "SELECT r FROM LeaveTypeDO r where r.type =:type and r.status NOT IN :id and r.status =:status"),
    @NamedQuery(name = "hrms_master_leavetype.findForupdate", query = "SELECT r FROM LeaveTypeDO r where r.type=:name and r.leaveTypeId NOT IN :id"),
    @NamedQuery(name = "hrms_master_leavetype.findByStatus", query = "SELECT r FROM LeaveTypeDO r where r.status =:status"),*/
})
public class AttendanceDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_BY_EMPID = "hrms_attendance.findByEmpId";
	
	public static final String FIND_BY_ID = "hrms_attendance.findById";
	
	public static final String FIND_SPECIFIC_FIELD = "hrms_attendance.specificfield";
	
	public static final String FIND_BY_EMPID_TIME = "hrms_attendance.findByEmpIdAndTime";
	
	public static final String FIND_BY_EMPID_DATE = "hrms_attendance.findByEmpIdAndDate";
	
	public static final String FIND_BY_TEAM = "hrms_attendance.findByTeam";
	
	/*public static final String FIND_BY_TYPE = "hrms_master_leavetype.findByType";
	
	public static final String FIND_BY_TYPE_FOR_UPDATE = "hrms_master_leavetype.findByTypeForUpdate";
	
	public static final String FIND_BY_NAME = "hrms_master_leavetype.findByName";
	
	public static final String FIND_FOR_UPDATE = "hrms_master_leavetype.findForupdate";
	
	public static final String FIND_BY_STATUS = "hrms_master_leavetype.findByStatus";*/
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long attendanceId;
	
	@Temporal(TemporalType.DATE)
	private Date timeInDate;
	
	@Temporal(TemporalType.DATE)
	private Date timeOutDate;
	
	@ManyToOne
	@JoinColumn(name="empId")
	private EmployeeDO employee;
	
	private String timeIn;
	private String timeOut;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
    
    private String attendanceType;
    
    
	public Long getAttendanceId() {
		return attendanceId;
	}
	public void setAttendanceId(Long attendanceId) {
		this.attendanceId = attendanceId;
	}
	public Date getTimeInDate() {
		return timeInDate;
	}
	public void setTimeInDate(Date timeInDate) {
		this.timeInDate = timeInDate;
	}
	public Date getTimeOutDate() {
		return timeOutDate;
	}
	public void setTimeOutDate(Date timeOutDate) {
		this.timeOutDate = timeOutDate;
	}
	public EmployeeDO getEmployee() {
		return employee;
	}
	public void setEmployee(EmployeeDO employee) {
		this.employee = employee;
	}
	public String getTimeIn() {
		return timeIn;
	}
	public void setTimeIn(String timeIn) {
		this.timeIn = timeIn;
	}
	public String getTimeOut() {
		return timeOut;
	}
	public void setTimeOut(String timeOut) {
		this.timeOut = timeOut;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public String getAttendanceType() {
		return attendanceType;
	}
	public void setAttendanceType(String attendanceType) {
		this.attendanceType = attendanceType;
	}
}