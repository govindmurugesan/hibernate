package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_master_earningsubtype")
@TableGenerator(name ="hrms_master_earningsubtype", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "hrms_master_earningsubtype.findById", query = "SELECT r FROM EarningSubTypeDO r where r.earningSubTypeId =:id"),
    @NamedQuery(name = "hrms_master_earningsubtype.findByStatus", query = "SELECT r FROM EarningSubTypeDO r where r.status =:status"),
    @NamedQuery(name = "hrms_master_earningsubtype.findByName", query = "SELECT r FROM EarningSubTypeDO r where r.name=:name"),
    @NamedQuery(name = "hrms_master_earningsubtype.findByNameAndId", query = "SELECT r FROM EarningSubTypeDO r where r.name=:name and r.earningSubTypeId NOT IN :id"),
    @NamedQuery(name = "hrms_master_earningsubtype.findByEarningTypeId", query = "SELECT r FROM EarningSubTypeDO r where r.status =:status and r.earningType.earningTypeId=:id"),
})
public class EarningSubTypeDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_BY_ID = "hrms_master_earningsubtype.findById";
	
	public static final String FIND_BY_STATUS = "hrms_master_earningsubtype.findByStatus";
	
	public static final String FIND_BY_EARNINGTYPE_ID = "hrms_master_earningsubtype.findByEarningTypeId";
	
	public static final String FIND_BY_NAME = "hrms_master_earningsubtype.findByName";
	
	public static final String FIND_FOR_UPDATE = "hrms_master_earningsubtype.findByNameAndId";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long earningSubTypeId;
	private String name;
	private char status;
	
	@ManyToOne
	@JoinColumn(name="earningTypeId")
	private EarningTypeDO earningType;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
	public Long getEarningSubTypeId() {
		return earningSubTypeId;
	}
	public void setEarningSubTypeId(Long earningSubTypeId) {
		this.earningSubTypeId = earningSubTypeId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public EarningTypeDO getEarningType() {
		return earningType;
	}
	public void setEarningType(EarningTypeDO earningType) {
		this.earningType = earningType;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
    
}