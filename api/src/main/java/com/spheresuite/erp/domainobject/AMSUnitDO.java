package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="ams_master_unit")
@TableGenerator(name ="ams_master_unit", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "ams_master_unit.findAll", query = "SELECT r FROM AMSUnitDO r"),
    @NamedQuery(name = "ams_master_unit.findById", query = "SELECT r FROM AMSUnitDO r where r.unitId =:id"),
    @NamedQuery(name = "ams_master_unit.findByStatus", query = "SELECT r FROM AMSUnitDO r where r.status =:status"),
    @NamedQuery(name = "ams_master_unit.findByName", query = "SELECT r FROM AMSUnitDO r where r.status =:status and r.unitName=:name"),
    @NamedQuery(name = "ams_master_unit.findByNameAndId", query = "SELECT r FROM AMSUnitDO r where r.status =:status and r.unitName=:name and r.unitId NOT IN :id"),
    @NamedQuery(name = "ams_master_unit.findByUnitHead", query = "SELECT r FROM AMSUnitDO r where r.unitHead.empId =:id"),
})
public class AMSUnitDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "ams_master_unit.findAll";
	public static final String FIND_BY_ID = "ams_master_unit.findById";
	public static final String FIND_BY_STATUS = "ams_master_unit.findByStatus";
	public static final String FIND_BY_NAME = "ams_master_unit.findByName";
	public static final String FIND_FOR_UPDATE = "ams_master_unit.findByNameAndId";
	public static final String FIND_BY_UNIIHEAD = "ams_master_unit.findByUnitHead";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO) 
	@Column(nullable = false, unique = true) 
	private Long unitId;
	
	private String unitName;
	private String unitComments;
	private char status;
	
	@ManyToOne
	@JoinColumn(name="empId")
	private EmployeeDO unitHead;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
	public Long getUnitId() {
		return unitId;
	}
	public void setUnitId(Long unitId) {
		this.unitId = unitId;
	}
	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	public String getUnitComments() {
		return unitComments;
	}
	public void setUnitComments(String unitComments) {
		this.unitComments = unitComments;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public EmployeeDO getUnitHead() {
		return unitHead;
	}
	public void setUnitHead(EmployeeDO unitHead) {
		this.unitHead = unitHead;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	
}