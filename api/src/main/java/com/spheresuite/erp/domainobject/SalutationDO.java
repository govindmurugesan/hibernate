package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="master_salutation")
@TableGenerator(name ="master_salutation", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "master_salutation.findAll", query = "SELECT r FROM SalutationDO r"),
    @NamedQuery(name = "master_salutation.findById", query = "SELECT r FROM SalutationDO r where r.salutationId =:id"),
    @NamedQuery(name = "master_salutation.findByStatus", query = "SELECT r FROM SalutationDO r where r.status =:status"),
    @NamedQuery(name = "master_salutation.findByName", query = "SELECT r FROM SalutationDO r where r.name=:name"),
    @NamedQuery(name = "master_salutation.findByNameAndId", query = "SELECT r FROM SalutationDO r where r.name=:name and r.id NOT IN :id"),
})
public class SalutationDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "master_salutation.findAll";
	
	public static final String FIND_BY_ID = "master_salutation.findById";
	
	public static final String FIND_BY_STATUS = "master_salutation.findByStatus";
	
	public static final String FIND_BY_NAME = "master_salutation.findByName";
	
	public static final String FIND_FOR_UPDATE = "master_salutation.findByNameAndId";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true) 
	private Long salutationId;
	
	private String name;
	private char status;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
	private String updatedby;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	
	public Long getSalutationId() {
		return salutationId;
	}
	public void setSalutationId(Long salutationId) {
		this.salutationId = salutationId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}

}