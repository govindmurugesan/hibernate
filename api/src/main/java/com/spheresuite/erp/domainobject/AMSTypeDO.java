package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="ams_master_type")
@TableGenerator(name ="ams_master_type", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "ams_master_type.findAll", query = "SELECT r FROM AMSTypeDO r"),
    @NamedQuery(name = "ams_master_type.findById", query = "SELECT r FROM AMSTypeDO r where r.typeId =:id"),
    @NamedQuery(name = "ams_master_type.findByStatus", query = "SELECT r FROM AMSTypeDO r where r.status =:status"),
    @NamedQuery(name = "ams_master_type.findByName", query = "SELECT r FROM AMSTypeDO r where r.status =:status and r.typeName=:name"),
   @NamedQuery(name = "ams_master_type.findByNameAndId", query = "SELECT r FROM AMSTypeDO r where r.status =:status and r.typeName=:name and r.typeId NOT IN :id"),
})
public class AMSTypeDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "ams_master_type.findAll";
	public static final String FIND_BY_ID = "ams_master_type.findById";
	public static final String FIND_BY_STATUS = "ams_master_type.findByStatus";
	public static final String FIND_BY_NAME = "ams_master_type.findByName";
	public static final String FIND_FOR_UPDATE = "ams_master_type.findByNameAndId";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO) 
	@Column(nullable = false, unique = true) 
	private Long typeId;
	private String typeName;
	private String typeComments;
	private char status;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
	public Long getTypeId() {
		return typeId;
	}
	public void setTypeId(Long typeId) {
		this.typeId = typeId;
	}
	public String getTypeName() {
		return typeName;
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	public String getTypeComments() {
		return typeComments;
	}
	public void setTypeComments(String typeComments) {
		this.typeComments = typeComments;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}