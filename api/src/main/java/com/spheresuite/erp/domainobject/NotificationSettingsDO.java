package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_master_notificationsettings")
@TableGenerator(name ="hrms_master_notificationsettings", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "hrms_master_notificationsettings.findById", query = "SELECT r FROM NotificationSettingsDO r where r.notificationSettingId =:id"),
})
public class NotificationSettingsDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "hrms_master_notificationsettings.findAll";
	
	public static final String FIND_BY_ID = "hrms_master_notificationsettings.findById";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true) 
	private Long notificationSettingId;
	private String hrRequest;
	private String payroll;
	private String holidays;
	private String leaves;
	private String hrpolicies;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getNotificationSettingId() {
		return notificationSettingId;
	}
	public void setNotificationSettingId(Long notificationSettingId) {
		this.notificationSettingId = notificationSettingId;
	}
	public String getHrRequest() {
		return hrRequest;
	}
	public void setHrRequest(String hrRequest) {
		this.hrRequest = hrRequest;
	}
	public String getPayroll() {
		return payroll;
	}
	public void setPayroll(String payroll) {
		this.payroll = payroll;
	}
	public String getHolidays() {
		return holidays;
	}
	public void setHolidays(String holidays) {
		this.holidays = holidays;
	}
	public String getLeaves() {
		return leaves;
	}
	public void setLeaves(String leaves) {
		this.leaves = leaves;
	}
	public String getHrpolicies() {
		return hrpolicies;
	}
	public void setHrpolicies(String hrpolicies) {
		this.hrpolicies = hrpolicies;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}