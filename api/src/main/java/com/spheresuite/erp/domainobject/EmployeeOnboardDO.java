package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="employeeonboard")
@TableGenerator(name ="employeeonboard", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "employeeonboard.findById", query = "SELECT r FROM EmployeeOnboardDO r where r.employeeOnboardId =:id"),
    @NamedQuery(name = "employeeonboard.findByEmail", query = "SELECT r FROM EmployeeOnboardDO r where r.personalemail =:personalEmail and r.status<>:status"),
    @NamedQuery(name = "employeeonboard.findByEmailId_Stauts", query = "SELECT r FROM EmployeeOnboardDO r where r.personalemail =:email and (r.status = 'a' or r.status = 'i' or r.status = 'p')"),
    @NamedQuery(name = "employeeonboard.findforUpdate", query = "SELECT r FROM EmployeeOnboardDO r where r.personalemail=:email and r.employeeOnboardId NOT IN :id"),
   // @NamedQuery(name = "employeeonboard.findByEmailId", query = "SELECT r FROM EmployeeOnboardDO r where r.personalemail =:personalEmail"),
})
public class EmployeeOnboardDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_BY_ID = "employeeonboard.findById";
	
	public static final String FIND_BY_EMAIL = "employeeonboard.findByEmail";
	
	public static final String FIND_BY_EMAILID_STATUS = "employeeonboard.findByEmailId_Stauts";
	
	public static final String FIND_FOR_UPDATE = "employeeonboard.findforUpdate";
	
	/*public static final String FIND_BY_EMAILID_WITHOUT_STATUS = "employeeonboard.findByEmailId";*/
	
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long employeeOnboardId;
	
	private String firstname;
	private String lastname;
	private String middlename;
	private String personalcontact;
	private String primarycontact;
	private String personalemail;
	private String aadhar;
	private String panno;
	private String dateofbirth;
	private char gender;
	@Column(columnDefinition="LONGTEXT")
	private String reason;
	private char status;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
	public Long getEmployeeOnboardId() {
		return employeeOnboardId;
	}
	public void setEmployeeOnboardId(Long employeeOnboardId) {
		this.employeeOnboardId = employeeOnboardId;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getMiddlename() {
		return middlename;
	}
	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}
	public String getPersonalcontact() {
		return personalcontact;
	}
	public void setPersonalcontact(String personalcontact) {
		this.personalcontact = personalcontact;
	}
	public String getPrimarycontact() {
		return primarycontact;
	}
	public void setPrimarycontact(String primarycontact) {
		this.primarycontact = primarycontact;
	}
	public String getPersonalemail() {
		return personalemail;
	}
	public void setPersonalemail(String personalemail) {
		this.personalemail = personalemail;
	}
	public String getAadhar() {
		return aadhar;
	}
	public void setAadhar(String aadhar) {
		this.aadhar = aadhar;
	}
	public String getPanno() {
		return panno;
	}
	public void setPanno(String panno) {
		this.panno = panno;
	}
	public String getDateofbirth() {
		return dateofbirth;
	}
	public void setDateofbirth(String dateofbirth) {
		this.dateofbirth = dateofbirth;
	}
	public char getGender() {
		return gender;
	}
	public void setGender(char gender) {
		this.gender = gender;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}