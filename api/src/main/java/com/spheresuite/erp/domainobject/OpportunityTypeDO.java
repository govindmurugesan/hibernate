package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="crm_master_opportunitytype")
@TableGenerator(name ="crm_master_opportunitytype", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "crm_master_opportunitytype.findById", query = "SELECT r FROM OpportunityTypeDO r where r.OpportunityTypeId =:id"),
    @NamedQuery(name = "crm_master_opportunitytype.findByStatus", query = "SELECT r FROM OpportunityTypeDO r where r.status =:status"),
    @NamedQuery(name = "crm_master_opportunitytype.findByName", query = "SELECT r FROM OpportunityTypeDO r where r.name=:name"),
    @NamedQuery(name = "crm_master_opportunitytype.findByNameAndId", query = "SELECT r FROM OpportunityTypeDO r where r.name=:name and r.OpportunityTypeId NOT IN :id"),
})
public class OpportunityTypeDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_BY_ID = "crm_master_opportunitytype.findById";
	
	public static final String FIND_BY_STATUS = "crm_master_opportunitytype.findByStatus";
	
	public static final String FIND_BY_NAME = "crm_master_opportunitytype.findByName";
	
	public static final String FIND_FOR_UPDATE = "crm_master_opportunitytype.findByNameAndId";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true) 
	private Long OpportunityTypeId;
	
	private String name;
	private char status;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
	public Long getOpportunityTypeId() {
		return OpportunityTypeId;
	}
	
	public void setOpportunityTypeId(Long opportunityTypeId) {
		OpportunityTypeId = opportunityTypeId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}