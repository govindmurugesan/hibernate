package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="ams_master_pcunder")
@TableGenerator(name ="ams_master_pcunder", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "ams_master_pcunder.findAll", query = "SELECT r FROM AMSPCUnderDO r"),
    @NamedQuery(name = "ams_master_pcunder.findById", query = "SELECT r FROM AMSPCUnderDO r where r.pcUnderId =:id"),
    @NamedQuery(name = "ams_master_pcunder.findByStatus", query = "SELECT r FROM AMSPCUnderDO r where r.status =:status"),
    @NamedQuery(name = "ams_master_pcunder.findByName", query = "SELECT r FROM AMSPCUnderDO r where r.status =:status and r.pcunder=:name"),
   @NamedQuery(name = "ams_master_pcunder.findByNameAndId", query = "SELECT r FROM AMSPCUnderDO r where r.status =:status and r.pcunder=:name and r.pcUnderId NOT IN :id"),
})
public class AMSPCUnderDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "ams_master_pcunder.findAll";
	public static final String FIND_BY_ID = "ams_master_pcunder.findById";
	public static final String FIND_BY_STATUS = "ams_master_pcunder.findByStatus";
	public static final String FIND_BY_NAME = "ams_master_pcunder.findByName";
	public static final String FIND_FOR_UPDATE = "ams_master_pcunder.findByNameAndId";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO) 
	@Column(nullable = false, unique = true) 
	private Long pcUnderId;
	
	private String pcunder;
	private String comments;
	private char status;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
	public Long getPcUnderId() {
		return pcUnderId;
	}
	public void setPcUnderId(Long pcUnderId) {
		this.pcUnderId = pcUnderId;
	}
	public String getPcunder() {
		return pcunder;
	}
	public void setPcunder(String pcunder) {
		this.pcunder = pcunder;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}