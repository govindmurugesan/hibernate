package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_master_leavegroupsettings")
@TableGenerator(name ="hrms_master_leavegroupsettings", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "hrms_master_leavegroupsettings.findById", query = "SELECT r FROM LeaveGroupSettingsDO r where r.leaveGroupSettingsId =:id"),
    @NamedQuery(name = "hrms_master_leavegroupsettings.findForAdd", query = "SELECT r FROM LeaveGroupSettingsDO r where r.leaveManagement.leaveManagementId=:id and r.status =:status and r.leaveGroup.leaveGroupId=:leaveGroupId"),
    @NamedQuery(name = "hrms_master_leavegroupsettings.findByGroupIdAndStatus", query = "SELECT r FROM LeaveGroupSettingsDO r where r.leaveGroup.leaveGroupId =:id and r.status =:status"),
    @NamedQuery(name = "hrms_master_leavegroupsettings.findByGroupId", query = "SELECT r FROM LeaveGroupSettingsDO r where r.leaveGroup.leaveGroupId =:id and r.status =:status and r.leaveManagement.status=:status"),
    @NamedQuery(name = "hrms_master_leavegroupsettings.findByGroupAndDate", query = "SELECT r FROM LeaveGroupSettingsDO r where r.leaveGroup.leaveGroupId =:id and r.status =:status and r.leaveManagement.status=:status and ((r.leaveManagement.fromDate <=:date and r.leaveManagement.toDate >=:date) or (r.leaveManagement.fromDate <=:date and r.leaveManagement.toDate IS NULL))"),
    @NamedQuery(name = "hrms_master_leavegroupsettings.findByGroupAndLeaveId", query = "SELECT r FROM LeaveGroupSettingsDO r where r.leaveManagement.leaveManagementId=:id  and r.leaveGroup.leaveGroupId=:leaveGroupId"),
    @NamedQuery(name = "hrms_master_leavegroupsettings.findByGroupAndFromToDate", query = "SELECT r FROM LeaveGroupSettingsDO r where r.leaveGroup.leaveGroupId =:id and r.status =:status and r.leaveManagement.status=:status"),
    @NamedQuery(name = "hrms_master_leavegroupsettings.findByGroupPermission", query = "SELECT r FROM LeaveGroupSettingsDO r where r.leaveGroup.leaveGroupId=:leaveGroupId and r.permission=:type"),
    @NamedQuery(name = "hrms_master_leavegroupsettings.findByGroupCompensation", query = "SELECT r FROM LeaveGroupSettingsDO r where r.leaveGroup.leaveGroupId=:leaveGroupId and r.compOff=:type"),
})
public class LeaveGroupSettingsDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_BY_ID = "hrms_master_leavegroupsettings.findById";
	
//	public static final String FIND_BY_STATUS = "hrms_payrollgroupearnings.findByStatus";
	
	public static final String FIND_FOR_ADD = "hrms_master_leavegroupsettings.findForAdd";
	
	public static final String FIND_BY_GROUP_ID_STATUS = "hrms_master_leavegroupsettings.findByGroupIdAndStatus";
	
	public static final String FIND_BY_GROUP_ID = "hrms_master_leavegroupsettings.findByGroupId";
	
	public static final String FIND_BY_GROUP_DATE = "hrms_master_leavegroupsettings.findByGroupAndDate";
	
	public static final String FIND_BY_GROUP_FROMTODATE = "hrms_master_leavegroupsettings.findByGroupAndFromToDate";
	
	public static final String FIND_BY_GROUP_LEAVE_ID = "hrms_master_leavegroupsettings.findByGroupAndLeaveId";
	
	public static final String FIND_BY_GROUP_COMPENSATION = "hrms_master_leavegroupsettings.findByGroupCompensation";
	
	public static final String FIND_BY_GROUP_PERMISSION = "hrms_master_leavegroupsettings.findByGroupPermission";
	
	//public static final String FIND_FOR_UPDATE = "hrms_payrollgroupearnings.findByNameAndId";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long leaveGroupSettingsId;
	
	private char status;
	
	@ManyToOne
	@JoinColumn(name="leaveGroupId")
	private LeaveGroupDO leaveGroup;
	
	@ManyToOne
	@JoinColumn(name="leaveManagementId")
	private LeaveManagementDO leaveManagement;
	
	private String permission;
	private String compOff;
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
	public Long getLeaveGroupSettingsId() {
		return leaveGroupSettingsId;
	}
	public void setLeaveGroupSettingsId(Long leaveGroupSettingsId) {
		this.leaveGroupSettingsId = leaveGroupSettingsId;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public LeaveGroupDO getLeaveGroup() {
		return leaveGroup;
	}
	public void setLeaveGroup(LeaveGroupDO leaveGroup) {
		this.leaveGroup = leaveGroup;
	}
	public LeaveManagementDO getLeaveManagement() {
		return leaveManagement;
	}
	public void setLeaveManagement(LeaveManagementDO leaveManagement) {
		this.leaveManagement = leaveManagement;
	}
	public String getPermission() {
		return permission;
	}
	public void setPermission(String permission) {
		this.permission = permission;
	}
	public String getCompOff() {
		return compOff;
	}
	public void setCompOff(String compOff) {
		this.compOff = compOff;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}