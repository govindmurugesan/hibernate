package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_master_itsavingssetting")
@TableGenerator(name ="hrms_master_itsavingssetting", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "hrms_master_itsavingssetting.findById", query = "SELECT r FROM ItSavingsSettingsDO r where r.itSavingsSettingsId =:id"),
    @NamedQuery(name = "hrms_master_itsavingssetting.findByStatus", query = "SELECT r FROM ItSavingsSettingsDO r where r.status =:status"),
    @NamedQuery(name = "hrms_master_itsavingssetting.findDuplicatePersist", query = "SELECT r FROM ItSavingsSettingsDO r where r.fromYear =:fromDate and r.toYear =:toDate"
    			+ " and r.itSection.itSectionId =:section and r.name =:name"),
    @NamedQuery(name = "hrms_master_itsavingssetting.findDuplicateUpdate", query = "SELECT r FROM ItSavingsSettingsDO r where r.fromYear =:fromDate and r.toYear =:toDate"
			+ " and r.itSection.itSectionId =:section and r.name =:name and r.itSavingsSettingsId <>:id"),
    @NamedQuery(name = "hrms_master_itsavingssetting.findByDate", query = "SELECT r FROM ItSavingsSettingsDO r where r.status =:status and (r.fromYear =:fromDate and r.toYear =:toDate)"),
})
public class ItSavingsSettingsDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_BY_ID = "hrms_master_itsavingssetting.findById";
	
	public static final String FIND_BY_DUPLICATE_PERSIST = "hrms_master_itsavingssetting.findDuplicatePersist";
	
	public static final String FIND_BY_DUPLICATE_UPDATE = "hrms_master_itsavingssetting.findDuplicateUpdate";
	
	public static final String FIND_BY_STATUS = "hrms_master_itsavingssetting.findByStatus";
	
	public static final String FIND_BY_DATE = "hrms_master_itsavingssetting.findByDate";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long itSavingsSettingsId;
	
	private String name;
	@Temporal(TemporalType.DATE)
	private Date fromYear;
	@Temporal(TemporalType.DATE)
	private Date toYear;
	//private Long maxLimit;
	private char status;
	
	@ManyToOne
	@JoinColumn(name="itSectionId")
	private ItSectionDO itSection;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getItSavingsSettingsId() {
		return itSavingsSettingsId;
	}
	public void setItSavingsSettingsId(Long itSavingsSettingsId) {
		this.itSavingsSettingsId = itSavingsSettingsId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getFromYear() {
		return fromYear;
	}
	public void setFromYear(Date fromYear) {
		this.fromYear = fromYear;
	}
	public Date getToYear() {
		return toYear;
	}
	public void setToYear(Date toYear) {
		this.toYear = toYear;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public ItSectionDO getItSection() {
		return itSection;
	}
	public void setItSection(ItSectionDO itSection) {
		this.itSection = itSection;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
    
}