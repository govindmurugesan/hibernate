package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="master_unitOrBranch")
@TableGenerator(name ="master_unitOrBranch", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "master_unitOrBranch.findAll", query = "SELECT r FROM UnitOrBranchDO r"),
    @NamedQuery(name = "master_unitOrBranch.findById", query = "SELECT r FROM UnitOrBranchDO r where r.unitOrBranchId =:id"),
    @NamedQuery(name = "master_unitOrBranch.findByStatus", query = "SELECT r FROM UnitOrBranchDO r where r.status =:status"),
    @NamedQuery(name = "master_unitOrBranch.findByName", query = "SELECT r FROM UnitOrBranchDO r where r.unitOrBranch=:name"),
    @NamedQuery(name = "master_unitOrBranch.findByNameAll", query = "SELECT r FROM UnitOrBranchDO r where r.unitOrBranch=:name"),
    @NamedQuery(name = "master_unitOrBranch.findByNameAndId", query = "SELECT r FROM UnitOrBranchDO r where r.unitOrBranch=:name and r.unitOrBranchId NOT IN :id"),
})
public class UnitOrBranchDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "master_unitOrBranch.findAll";
	
	public static final String FIND_BY_ID = "master_unitOrBranch.findById";
	
	public static final String FIND_BY_STATUS = "master_unitOrBranch.findByStatus";
	
	public static final String FIND_BY_NAME = "master_unitOrBranch.findByName";
	
	public static final String FIND_BY_CITY_ALL = "master_unitOrBranch.findByNameAll";
	
	public static final String FIND_FOR_UPDATE = "master_unitOrBranch.findByNameAndId";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long unitOrBranchId;
	
	private String unitOrBranch;
	private String address;
	private char status;
	
	@ManyToOne
	@JoinColumn(name="countryId")
	private CountryDO country;
	
	@ManyToOne
	@JoinColumn(name="stateId")
	private StateDO state;
	
	@ManyToOne
	@JoinColumn(name="empId")
	private EmployeeDO managerId;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getUnitOrBranchId() {
		return unitOrBranchId;
	}
	public void setUnitOrBranchId(Long unitOrBranchId) {
		this.unitOrBranchId = unitOrBranchId;
	}
	public String getUnitOrBranch() {
		return unitOrBranch;
	}
	public void setUnitOrBranch(String unitOrBranch) {
		this.unitOrBranch = unitOrBranch;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public CountryDO getCountry() {
		return country;
	}
	public void setCountry(CountryDO country) {
		this.country = country;
	}
	public StateDO getState() {
		return state;
	}
	public void setState(StateDO state) {
		this.state = state;
	}
	public EmployeeDO getManagerId() {
		return managerId;
	}
	public void setManagerId(EmployeeDO managerId) {
		this.managerId = managerId;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}