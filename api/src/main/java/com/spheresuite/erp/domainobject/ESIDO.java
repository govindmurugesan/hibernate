package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_master_esi")
@TableGenerator(name ="hrms_master_esi", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "hrms_master_esi.findById", query = "SELECT r FROM ESIDO r where r.esiId =:id"),
    //@NamedQuery(name = "hrms_master_esi.findBySalary", query = "SELECT r FROM ESIDO r where (r.salaryFrom <=:fromAmount and r.salaryTo >=:toAmount)"),
    @NamedQuery(name = "hrms_master_esi.findActive", query = "SELECT r FROM ESIDO r where r.status =:status"),
 //   @NamedQuery(name = "hrms_pf.findByDate", query = "SELECT r FROM PFDO r where (r.fromYear =:fromDate and r.toYear =:toDate)"),
   // 
   // @NamedQuery(name = "hrms_pf.findByCurrentDate", query = "SELECT r FROM EducationCessDO r where r.status =:status and ((r.fromYear <=:date and r.toYear >=:date) or (r.fromYear <=:date and r.toYear IS NULL))"),
   // @NamedQuery(name = "hrms_master_esi.findForUpdate", query = "SELECT r FROM ESIDO r where (r.salaryFrom <=:fromAmount and r.salaryTo >=:toAmount) and r.esiId  <>:id"),
})
public class ESIDO implements Serializable {
	private static final long serialVersionUID = 1L;

	
	public static final String FIND_BY_ID = "hrms_master_esi.findById";
	
	//public static final String FIND_BY_SALARY = "hrms_master_esi.findBySalary";
	
	public static final String FIND_ACTIVE = "hrms_master_esi.findActive";
	
	/*public static final String FIND_BY_DATE = "hrms_pf.findByDate";
	
	public static final String FIND_BY_CURRENTDATE = "hrms_pf.findByCurrentDate";*/
	
//	public static final String FIND_FOR_UPDATE = "hrms_master_esi.findForUpdate";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long esiId;
	
	/*private Long salaryFrom;
	private Long salaryTo;*/
	private Double employeePercentage;
	private Double employerPercentage;
	private char status;
	
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
	
	public Long getEsiId() {
		return esiId;
	}
	public void setEsiId(Long esiId) {
		this.esiId = esiId;
	}
	public Double getEmployeePercentage() {
		return employeePercentage;
	}
	public void setEmployeePercentage(Double employeePercentage) {
		this.employeePercentage = employeePercentage;
	}
	public Double getEmployerPercentage() {
		return employerPercentage;
	}
	public void setEmployerPercentage(Double employerPercentage) {
		this.employerPercentage = employerPercentage;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}