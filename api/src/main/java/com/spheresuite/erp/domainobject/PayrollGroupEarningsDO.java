package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_payrollgroupearnings")
@TableGenerator(name ="hrms_payrollgroupearnings", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "hrms_payrollgroupearnings.findById", query = "SELECT r FROM PayrollGroupEarningsDO r where r.payrollGroupEarningsId =:id"),
    //@NamedQuery(name = "hrms_payrollgroupearnings.findByStatus", query = "SELECT r FROM PayrollGroupDO r where r.status =:status"),
    @NamedQuery(name = "hrms_payrollgroupearnings.findForAdd", query = "SELECT r FROM PayrollGroupEarningsDO r where r.allowanceSettings.allowanceSettingsId=:allowenceId and r.status =:status and r.payrollGroup.payrollGroupId=:payrollGroupId"),
    @NamedQuery(name = "hrms_payrollgroupearnings.findByGroupId", query = "SELECT r FROM PayrollGroupEarningsDO r where r.payrollGroup.payrollGroupId =:id and r.status =:status"),
})
public class PayrollGroupEarningsDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_BY_ID = "hrms_payrollgroupearnings.findById";
	
//	public static final String FIND_BY_STATUS = "hrms_payrollgroupearnings.findByStatus";
	
	public static final String FIND_FOR_ADD = "hrms_payrollgroupearnings.findForAdd";
	
	public static final String FIND_BY_GROUP_ID = "hrms_payrollgroupearnings.findByGroupId";
	
	//public static final String FIND_FOR_UPDATE = "hrms_payrollgroupearnings.findByNameAndId";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long payrollGroupEarningsId;
	
	private char status;
	
	@ManyToOne
	@JoinColumn(name="payrollGroupId")
	private PayrollGroupDO payrollGroup;
	
	@ManyToOne
	@JoinColumn(name="allowanceSettingsId")
	private AllowanceSettingDO allowanceSettings;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getPayrollGroupEarningsId() {
		return payrollGroupEarningsId;
	}
	public void setPayrollGroupEarningsId(Long payrollGroupEarningsId) {
		this.payrollGroupEarningsId = payrollGroupEarningsId;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public PayrollGroupDO getPayrollGroup() {
		return payrollGroup;
	}
	public void setPayrollGroup(PayrollGroupDO payrollGroup) {
		this.payrollGroup = payrollGroup;
	}
	public AllowanceSettingDO getAllowanceSettings() {
		return allowanceSettings;
	}
	public void setAllowanceSettings(AllowanceSettingDO allowanceSettings) {
		this.allowanceSettings = allowanceSettings;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}