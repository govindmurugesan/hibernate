package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_advancePaymentDetail")
@TableGenerator(name ="hrms_advancePaymentDetail", initialValue =100101, allocationSize =1)
@NamedQueries({
	//@NamedQuery(name = "hrms_advancePaymentDetail.findAll", query = "SELECT r FROM AdvancePaymentDetailDO r ORDER BY r.advancePaymentId DESC"),
    @NamedQuery(name = "hrms_advancePaymentDetail.findById", query = "SELECT r FROM AdvancePaymentDetailDO r where r.advancePaymentDetailId =:id"),
    @NamedQuery(name = "hrms_advancePaymentDetail.findByAdvanceId", query = "SELECT r FROM AdvancePaymentDetailDO r where r.advancePayment.advancePaymentId =:id"),
    @NamedQuery(name = "hrms_advancePaymentDetail.findByEmpIdMonth", query = "SELECT r FROM AdvancePaymentDetailDO r where r.employee.empId =:empId and r.status =:status and r.month =:month"),
})
public class AdvancePaymentDetailDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_BY_ID = "hrms_advancePaymentDetail.findById";
	
	public static final String FIND_BY_ADVANCE_ID = "hrms_advancePaymentDetail.findByAdvanceId";
	
	public static final String FIND_BY_EMPID_MONTH = "hrms_advancePaymentDetail.findByEmpIdMonth";
	
	
	
	
	
	/*public static final String FIND_ALL = "hrms_advancePaymentDetail.findAll";
	
	public static final String FIND_BY_EMPID = "hrms_advancePaymentDetail.findByEmpId";
	
	public static final String FIND_ACTIVE_BY_EMPID = "hrms_advancePaymentDetail.findActiveByEmpId";*/
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)  
	private Long advancePaymentDetailId;
	private String month;
	private String amount;
	private String status;
	
	@ManyToOne
	@JoinColumn(name="empId")
	private EmployeeDO employee;
	
	@ManyToOne
	@JoinColumn(name="advancePaymentId")
	private AdvancePaymentDO advancePayment;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
	public Long getAdvancePaymentDetailId() {
		return advancePaymentDetailId;
	}
	public void setAdvancePaymentDetailId(Long advancePaymentDetailId) {
		this.advancePaymentDetailId = advancePaymentDetailId;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public EmployeeDO getEmployee() {
		return employee;
	}
	public void setEmployee(EmployeeDO employee) {
		this.employee = employee;
	}
	public AdvancePaymentDO getAdvancePayment() {
		return advancePayment;
	}
	public void setAdvancePayment(AdvancePaymentDO advancePayment) {
		this.advancePayment = advancePayment;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}

}