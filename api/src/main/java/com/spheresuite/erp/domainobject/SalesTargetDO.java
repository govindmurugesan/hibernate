package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="crm_salestarget")
@TableGenerator(name ="crm_salestarget", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "crm_salestarget.findAll", query = "SELECT r FROM SalesTargetDO r where r.employee.empId IN :id"),
    @NamedQuery(name = "crm_salestarget.findAllData", query = "SELECT r FROM SalesTargetDO r"),
    @NamedQuery(name = "crm_salestarget.findById", query = "SELECT r FROM SalesTargetDO r where r.salesTargetId =:id"),
    @NamedQuery(name = "crm_salestarget.findByEmpId", query = "SELECT r FROM SalesTargetDO r where r.employee.empId =:empId"),
    @NamedQuery(name = "crm_salestarget.findBetweenYear", query = "SELECT r FROM SalesTargetDO r where (r.fromDate >=:fromDate AND r.toDate <=:toDate)"),
    @NamedQuery(name = "crm_salestarget.findBetweenYearForEmp", query = "SELECT r FROM SalesTargetDO r where r.fromDate >=:fromDate AND r.toDate <=:toDate AND r.employee.empId =:empId"),
})
public class SalesTargetDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "crm_salestarget.findAll";
	
	public static final String FIND_BY_ID = "crm_salestarget.findById";
	
	public static final String FIND_BY_EMP_ID = "crm_salestarget.findByEmpId";
	
	public static final String FIND_BY_YEAR = "crm_salestarget.findBetweenYear";
	
	public static final String FIND_BY_YEAR_FOR_EMP = "crm_salestarget.findBetweenYearForEmp";
	
	public static final String FIND_ALL_DATA = "salescrm_salestargettarget.findAllData";

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO) 
	@Column(nullable = false, unique = true)
	private Long salesTargetId;
	
	private Long fromDate;
	private Long toDate;
	
	@ManyToOne
	@JoinColumn(name="empId")
	private EmployeeDO employee;
	
	private Double q1;
	private Double q2;
	private Double q3;
	private Double q4;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getSalesTargetId() {
		return salesTargetId;
	}
	public void setSalesTargetId(Long salesTargetId) {
		this.salesTargetId = salesTargetId;
	}
	public Long getFromDate() {
		return fromDate;
	}
	public void setFromDate(Long fromDate) {
		this.fromDate = fromDate;
	}
	public Long getToDate() {
		return toDate;
	}
	public void setToDate(Long toDate) {
		this.toDate = toDate;
	}
	public EmployeeDO getEmployee() {
		return employee;
	}
	public void setEmployee(EmployeeDO employee) {
		this.employee = employee;
	}
	public Double getQ1() {
		return q1;
	}
	public void setQ1(Double q1) {
		this.q1 = q1;
	}
	public Double getQ2() {
		return q2;
	}
	public void setQ2(Double q2) {
		this.q2 = q2;
	}
	public Double getQ3() {
		return q3;
	}
	public void setQ3(Double q3) {
		this.q3 = q3;
	}
	public Double getQ4() {
		return q4;
	}
	public void setQ4(Double q4) {
		this.q4 = q4;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}