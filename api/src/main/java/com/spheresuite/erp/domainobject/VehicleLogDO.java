package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="gms_vehiclelog")
@TableGenerator(name ="gms_vehiclelog", initialValue =100101, allocationSize =1)
@NamedQueries({
	@NamedQuery(name = "gms_vehiclelog.findById", query = "SELECT r FROM VehicleLogDO r where r.id =:id"),
	@NamedQuery(name = "gms_vehiclelog.findByVehicleNoAndStatus", query = "SELECT r FROM VehicleLogDO r where r.status =:status and r.vehicleNo=:vehicleNo"),
	@NamedQuery(name = "gms_vehiclelog.findByVehicleNo", query = "SELECT r FROM VehicleLogDO r where r.vehicleNo=:vehicleNo"),
})
public class VehicleLogDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static final String FIND_BY_VechicleNoAndStatus = "gms_vehiclelog.findByNameAndId";
	public static final String FIND_BY_ID = "gms_vehiclelog.findById";
	public static final String FIND_BY_VEHICLENOANDSTATUS = "gms_vehiclelog.findByVehicleNoAndStatus";
	public static final String FIND_BY_VEHICLENO = "gms_vehiclelog.findByVehicleNo";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private String vehicleNo;
	private String driverName;
	private String mobile;
	private String unitName;
	private String company;
	private String address;
	private String city;
	private String state;
	private String idProf;
	private String purpose;
	private String poNo;
	/*private String goodsIn;
	private String goodsInComments;
	private String goodsOut;
	private String goodsOutComments;*/
	@Temporal(TemporalType.TIMESTAMP)
	private Date enteredOn;
	@Temporal(TemporalType.TIMESTAMP)
	private Date exitOn;
	private char status;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private String updatedBy;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getVehicleNo() {
		return vehicleNo;
	}
	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}
	public String getDriverName() {
		return driverName;
	}
	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getIdProf() {
		return idProf;
	}
	public void setIdProf(String idProf) {
		this.idProf = idProf;
	}
	public String getPurpose() {
		return purpose;
	}
	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}
	public String getPoNo() {
		return poNo;
	}
	public void setPoNo(String poNo) {
		this.poNo = poNo;
	}
	public Date getEnteredOn() {
		return enteredOn;
	}
	public void setEnteredOn(Date enteredOn) {
		this.enteredOn = enteredOn;
	}
	public Date getExitOn() {
		return exitOn;
	}
	public void setExitOn(Date exitOn) {
		this.exitOn = exitOn;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	
	
	
	

	
}