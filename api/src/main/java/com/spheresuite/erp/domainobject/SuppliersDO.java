package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="supplier")
@TableGenerator(name ="supplier", initialValue =100101, allocationSize =1)
@NamedQueries({
	@NamedQuery(name = "supplier.findAllSupplier", query = "SELECT r FROM SuppliersDO r"),
	@NamedQuery(name = "supplier.findById", query = "SELECT r FROM SuppliersDO r where r.supplierId =:id"),
   /* @NamedQuery(name = "supplier.findAll", query = "SELECT r FROM supplierDO r where r.type=:type and r.empId IN :id and r.isDeleted =:status"),
    @NamedQuery(name = "supplier.findAllDate", query = "SELECT r FROM supplierDO r where r.type=:type  and r.isDeleted =:status"),
    @NamedQuery(name = "supplier.findSum", query = "SELECT count(r) FROM supplierDO r where r.type=:type  and r.isDeleted =:status"),
    @NamedQuery(name = "supplier.find", query = "SELECT r FROM supplierDO r where r.empId IN :id and r.isDeleted =:status"),
    @NamedQuery(name = "supplier.findById", query = "SELECT r FROM supplierDO r where r.supplierId =:id and r.isDeleted =:status"),
    @NamedQuery(name = "supplier.findByDate", query = "SELECT r FROM supplierDO r where r.createdon BETWEEN :fromDate AND :toDate AND r.type =:type and r.isDeleted =:status"),
    @NamedQuery(name = "supplier.findCustomerByDate", query = "SELECT r FROM supplierDO r where r.convertedDate BETWEEN :fromDate AND :toDate AND r.type =:type and r.isDeleted =:status"),
    @NamedQuery(name = "supplier.findByDateByEmp", query = "SELECT r FROM supplierDO r where r.createdon BETWEEN :fromDate AND :toDate AND r.type =:type and r.isDeleted =:status and r.empId =:empId"),
    @NamedQuery(name = "supplier.findByDateByEmpForCus", query = "SELECT r FROM supplierDO r where r.convertedDate BETWEEN :fromDate AND :toDate AND r.type =:type and r.isDeleted =:status and r.empId =:empId"),
    @NamedQuery(name = "supplier.findByByEmp", query = "SELECT r FROM supplierDO r where r.type =:type AND r.empId =:empId and r.isDeleted =:status"),
    @NamedQuery(name = "supplier.findByByPermanentEmp", query = "SELECT r FROM supplierDO r where r.type =:type AND r.empId =:empId and r.isDeleted =:status and (r.transferType <>:transferType or r.transferType IS NULL)"),
    @NamedQuery(name = "supplier.findByTransferType", query = "SELECT r FROM supplierDO r where r.type =:type AND r.transferType =:transferType and r.isDeleted =:status order by r.supplierId desc"),
    @NamedQuery(name = "supplier.findByAllTransferType", query = "SELECT r FROM supplierDO r where r.type =:type AND r.transferType is not null and r.isDeleted =:status order by r.supplierId desc"),
    @NamedQuery(name = "supplier.findByTransferTypeById", query = "SELECT r FROM supplierDO r where r.type =:type AND r.transferType =:transferType and r.isDeleted =:status and r.transferfrom =:empId"),
    @NamedQuery(name = "supplier.findAllLead", query = "SELECT r FROM supplierDO r  where r.isDeleted =:status"),
    @NamedQuery(name = "supplier.findAllCustId", query = "SELECT r.supplierId FROM supplierDO r where r.type=:type  and r.isDeleted =:status"),*/
    
})
public class SuppliersDO implements Serializable {
	private static final long serialVersionUID = 1L;

	//public static final String FIND_ALL = "supplier.findAll";
	
	public static final String FIND_ALL_SUPPLIER = "supplier.findAllSupplier";
	
	/*public static final String FIND_ALL_DATE = "supplier.findAllDate";
	
	public static final String FIND_ALL_CUST_ID = "supplier.findAllCustId";
	
	public static final String FIND_SUM_CUSTOMER="supplier.findSum";
	
	public static final String FIND = "supplier.find";*/
	
	public static final String FIND_BY_ID = "supplier.findById";

	/*public static final String FIND_BY_DATE = "supplier.findByDate";
	
	public static final String FIND_BY_DATE_BY_EMP = "supplier.findByDateByEmp";
	
	public static final String FIND_BY_DATE_BY_EMP_FOR_CUS = "supplier.findByDateByEmpForCus";
	
	public static final String FIND_CUSTOMER_BY_DATE = "supplier.findCustomerByDate";
	
	public static final String FIND_BY_EMP = "supplier.findByByEmp";
	
	public static final String FIND_BY_PERMANANT_EMP = "supplier.findByByPermanentEmp";
	
	public static final String FIND_BY_TRANSFERTYPE = "supplier.findByTransferType";
	
	public static final String FIND_BY_ALL_TRANSFERTYPE = "supplier.findByAllTransferType";
	
	public static final String FIND_BY_TRANSFERTYPE_BY_ID = "supplier.findByTransferTypeById";*/
	
	
	@Id
	@Column(nullable = false, unique = true)
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long supplierId;
	
	private String name;
	private String registeredAddr;
	private String communicationAddr;
	private String entityStatus;
	private String nameOfDirectors;
	private String pan;
	private String gstRegNo;
	private String cin;
	private String cstRegNo;
	private String centralExciseRegNo;
	private String esiRegNo;
	private String pfRegNo;
	private String bankName;
	private String bankBranch;
	private String typeOfAccount;
	private Long accountNo;
	private String ifsCode;
	private String listOfProducts;
	private String specializedIn;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
	private String updatedby;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	
	public Long getSupplierId() {
		return supplierId;
	}
	public void setSupplierId(Long supplierId) {
		this.supplierId = supplierId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getRegisteredAddr() {
		return registeredAddr;
	}
	public void setRegisteredAddr(String registeredAddr) {
		this.registeredAddr = registeredAddr;
	}
	public String getCommunicationAddr() {
		return communicationAddr;
	}
	public void setCommunicationAddr(String communicationAddr) {
		this.communicationAddr = communicationAddr;
	}
	public String getEntityStatus() {
		return entityStatus;
	}
	public void setEntityStatus(String entityStatus) {
		this.entityStatus = entityStatus;
	}
	public String getNameOfDirectors() {
		return nameOfDirectors;
	}
	public void setNameOfDirectors(String nameOfDirectors) {
		this.nameOfDirectors = nameOfDirectors;
	}
	public String getPan() {
		return pan;
	}
	public void setPan(String pan) {
		this.pan = pan;
	}
	public String getGstRegNo() {
		return gstRegNo;
	}
	public void setGstRegNo(String gstRegNo) {
		this.gstRegNo = gstRegNo;
	}
	public String getCin() {
		return cin;
	}
	public void setCin(String cin) {
		this.cin = cin;
	}
	public String getCstRegNo() {
		return cstRegNo;
	}
	public void setCstRegNo(String cstRegNo) {
		this.cstRegNo = cstRegNo;
	}
	public String getCentralExciseRegNo() {
		return centralExciseRegNo;
	}
	public void setCentralExciseRegNo(String centralExciseRegNo) {
		this.centralExciseRegNo = centralExciseRegNo;
	}
	public String getEsiRegNo() {
		return esiRegNo;
	}
	public void setEsiRegNo(String esiRegNo) {
		this.esiRegNo = esiRegNo;
	}
	public String getPfRegNo() {
		return pfRegNo;
	}
	public void setPfRegNo(String pfRegNo) {
		this.pfRegNo = pfRegNo;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getBankBranch() {
		return bankBranch;
	}
	public void setBankBranch(String bankBranch) {
		this.bankBranch = bankBranch;
	}
	public String getTypeOfAccount() {
		return typeOfAccount;
	}
	public void setTypeOfAccount(String typeOfAccount) {
		this.typeOfAccount = typeOfAccount;
	}
	public Long getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(Long accountNo) {
		this.accountNo = accountNo;
	}
	public String getIfsCode() {
		return ifsCode;
	}
	public void setIfsCode(String ifsCode) {
		this.ifsCode = ifsCode;
	}
	public String getListOfProducts() {
		return listOfProducts;
	}
	public void setListOfProducts(String listOfProducts) {
		this.listOfProducts = listOfProducts;
	}
	public String getSpecializedIn() {
		return specializedIn;
	}
	public void setSpecializedIn(String specializedIn) {
		this.specializedIn = specializedIn;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	
	
	
	
}