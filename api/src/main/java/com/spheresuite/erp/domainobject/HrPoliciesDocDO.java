package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_hrpoliciesdoc")
@TableGenerator(name ="hrms_hrpoliciesdoc", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "hrms_hrpoliciesdoc.findAll", query = "SELECT r FROM HrPoliciesDocDO r"),
    @NamedQuery(name = "hrms_hrpoliciesdoc.findByPolicieId", query = "SELECT r FROM HrPoliciesDocDO r where r.hrPoliceId =:id"),
})
public class HrPoliciesDocDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "hrms_hrpoliciesdoc.findAll";
	
	public static final String FIND_BY_POLICIE_ID = "hrms_hrpoliciesdoc.findByPolicieId";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO) 
	private Long hrpoliciesDocId;
	
	private Long hrPoliceId;
	@Column(columnDefinition="LONGTEXT")
	private String photo;
	private String fileName;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
	public Long getHrpoliciesDocId() {
		return hrpoliciesDocId;
	}
	public void setHrpoliciesDocId(Long hrpoliciesDocId) {
		this.hrpoliciesDocId = hrpoliciesDocId;
	}
	public Long getHrPoliceId() {
		return hrPoliceId;
	}
	public void setHrPoliceId(Long hrPoliceId) {
		this.hrPoliceId = hrPoliceId;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}