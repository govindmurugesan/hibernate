package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_employeeexpensedoc")
@TableGenerator(name ="hrms_employeeexpensedoc", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "hrms_employeeexpensedoc.findAll", query = "SELECT r FROM EmployeeExpenseDocDO r"),
    @NamedQuery(name = "hrms_employeeexpensedoc.findByExpenseId", query = "SELECT r FROM EmployeeExpenseDocDO r where r.empExpense.empExpenseId =:id"),
})
public class EmployeeExpenseDocDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "hrms_employeeexpensedoc.findAll";
	
	public static final String FIND_BY_EXPENSE_ID = "hrms_employeeexpensedoc.findByExpenseId";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO) 
	private Long employeeExpensesDocId;
	
	@ManyToOne
	@JoinColumn(name="empExpenseId")
	private EmployeeExpenseDO empExpense;
	@Column(columnDefinition="LONGTEXT")
	private String photo;
	private String fileName;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getEmployeeExpensesDocId() {
		return employeeExpensesDocId;
	}
	public void setEmployeeExpensesDocId(Long employeeExpensesDocId) {
		this.employeeExpensesDocId = employeeExpensesDocId;
	}
	public EmployeeExpenseDO getEmpExpense() {
		return empExpense;
	}
	public void setEmpExpense(EmployeeExpenseDO empExpense) {
		this.empExpense = empExpense;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}