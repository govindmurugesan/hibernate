package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_hrrequests")
@TableGenerator(name ="hrms_hrrequests", initialValue =100101, allocationSize =1)
@NamedQueries({
	 @NamedQuery(name = "hrms_hrrequests.findAll", query = "SELECT r FROM HrRequestsDO r ORDER BY r.hrRequestId DESC"),
    @NamedQuery(name = "hrms_hrrequests.findById", query = "SELECT r FROM HrRequestsDO r where r.hrRequestId =:id"),
    @NamedQuery(name = "hrms_hrrequests.findByEmpId", query = "SELECT r FROM HrRequestsDO r where r.empId =:id"),
    
})
public class HrRequestsDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static final String FIND_ALL = "hrms_hrrequests.findAll";
	public static final String FIND_BY_ID = "hrms_hrrequests.findById";
	public static final String FINDBY_EMP_ID = "hrms_hrrequests.findByEmpId";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO) 
	@Column(nullable = false, unique = true)
	private Long hrRequestId;
	
	@Column(columnDefinition="LONGTEXT")
	private String description;
	private String comments;
	private char status;
	private String empId;
	
	@ManyToOne
	@JoinColumn(name="hrRequestTypeId")
	private HrRequestTypeDO hrRequestType;
	
	@ManyToOne
	@JoinColumn(name="departmentId")
	private DepartmentDO department;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getHrRequestId() {
		return hrRequestId;
	}
	public void setHrRequestId(Long hrRequestId) {
		this.hrRequestId = hrRequestId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getEmpId() {
		return empId;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
	public HrRequestTypeDO getHrRequestType() {
		return hrRequestType;
	}
	public void setHrRequestType(HrRequestTypeDO hrRequestType) {
		this.hrRequestType = hrRequestType;
	}
	public DepartmentDO getDepartment() {
		return department;
	}
	public void setDepartment(DepartmentDO department) {
		this.department = department;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}