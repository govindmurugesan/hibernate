package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_payrollearningsdetails")
@TableGenerator(name ="hrms_payrollearningsdetails", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "hrms_payrollearningsdetails.findByEmpPayrollId", query = "SELECT r FROM PayrollEarningsDetailsDO r where r.payroll.payrollId =:id"),
    @NamedQuery(name = "hrms_payrollearningsdetails.findByEmpPayrollIds", query = "SELECT r FROM PayrollEarningsDetailsDO r where r.payroll.payrollId IN :id"),
})
public class PayrollEarningsDetailsDO implements Serializable {
	private static final long serialVersionUID = 1L;

	
	public static final String FIND_BY_ID = "hrms_payrollearningsdetails.findByEmpPayrollId";
	
	public static final String FIND_BY_IDS = "hrms_payrollearningsdetails.findByEmpPayrollIds";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO) 
	@Column(nullable = false, unique = true) 
	private Long payrollEarningId;
	
	private String name;
	private Double monthly;
	private Double ytd;
	
	@ManyToOne
	@JoinColumn(name="payrollId")
	private EmployeePayrollDO payroll;
	
	@ManyToOne
	@JoinColumn(name="payrollGroupId")
	private PayrollGroupDO payrollGroup;
	
	@ManyToOne
	@JoinColumn(name="payrollGroupEarningsId")
	private PayrollGroupEarningsDO payrollGroupEarning;
	
	@ManyToOne
	@JoinColumn(name="empBonusId")
	private EmployeeBonusDO bonus;
	
	@ManyToOne
	@JoinColumn(name="dearnessallowanceId")
	private DASettingsDO dearnessAllowance;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getPayrollEarningId() {
		return payrollEarningId;
	}
	public void setPayrollEarningId(Long payrollEarningId) {
		this.payrollEarningId = payrollEarningId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getMonthly() {
		return monthly;
	}
	public void setMonthly(Double monthly) {
		this.monthly = monthly;
	}
	public Double getYtd() {
		return ytd;
	}
	public void setYtd(Double ytd) {
		this.ytd = ytd;
	}
	public EmployeePayrollDO getPayroll() {
		return payroll;
	}
	public void setPayroll(EmployeePayrollDO payroll) {
		this.payroll = payroll;
	}
	public PayrollGroupDO getPayrollGroup() {
		return payrollGroup;
	}
	public void setPayrollGroup(PayrollGroupDO payrollGroup) {
		this.payrollGroup = payrollGroup;
	}
	public PayrollGroupEarningsDO getPayrollGroupEarning() {
		return payrollGroupEarning;
	}
	public void setPayrollGroupEarning(PayrollGroupEarningsDO payrollGroupEarning) {
		this.payrollGroupEarning = payrollGroupEarning;
	}
	public EmployeeBonusDO getBonus() {
		return bonus;
	}
	public void setBonus(EmployeeBonusDO bonus) {
		this.bonus = bonus;
	}
	public DASettingsDO getDearnessAllowance() {
		return dearnessAllowance;
	}
	public void setDearnessAllowance(DASettingsDO dearnessAllowance) {
		this.dearnessAllowance = dearnessAllowance;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}