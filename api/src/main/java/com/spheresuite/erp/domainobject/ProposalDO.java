package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="crm_proposal")
@TableGenerator(name ="crm_proposal", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "crm_proposal.findAll", query = "SELECT r FROM ProposalDO r where r.employeeId IN :id"),
    @NamedQuery(name = "crm_proposal.findAllData", query = "SELECT r FROM ProposalDO r"),
    @NamedQuery(name = "crm_proposal.findById", query = "SELECT r FROM ProposalDO r where r.proposalId =:id"),
    @NamedQuery(name = "crm_proposal.findAllByCustomerId", query = "SELECT r FROM ProposalDO r where r.customer.leadId =:id"),
})
public class ProposalDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "crm_proposal.findAll";
	
	public static final String FIND_BY_ID = "crm_proposal.findById";
	
	public static final String FIND_ALL_DATA = "crm_proposal.findAllData";
	
	public static final String FIND_ALL_BY_CUSTOMERID = "crm_proposal.findAllByCustomerId";

	@Id
	@Column(nullable = false, unique = true)
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long proposalId;
	
	@ManyToOne
	@JoinColumn(name="leadId")
	private LeadDO customer;
	//private Long customerId;
	
	@ManyToOne
	@JoinColumn(name="opportunityId")
	private OpportunitiesDO project;
	//private Long projectId;
	
	private Long projectTypeId;
	//private String currencyType;
	@ManyToOne
	@JoinColumn(name="countryId")
	private CountryDO currencyType;
	private Long qunatity;	
	
	//private Long paymentTermId;
	@ManyToOne
	@JoinColumn(name="paymentTermId")
	private PaymentTermDO paymentTerm;
	
	private String invoiceTerm;
	private String rate;
	private String updatedby;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private String employeeId;
	private String description;
	private String supplierName;
	/*private String startDate;
	private String endDate;*/
	private Long duration;
	private String projectType;
	private String proposalName;
	@Temporal(TemporalType.DATE)
	private Date proposalDate;
	private String expenses;
	private String expensesTerm;
	private String workLocation;
	private String workDesc;
	
	@ManyToOne
	@JoinColumn(name="contactId")
	private ContactDO customerContact;
	
	@ManyToOne
	@JoinColumn(name="empId")
	private EmployeeDO supplierContact;

	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
	
	
	
	public Long getProposalId() {
		return proposalId;
	}
	public void setProposalId(Long proposalId) {
		this.proposalId = proposalId;
	}
	public LeadDO getCustomer() {
		return customer;
	}
	public void setCustomer(LeadDO lead) {
		this.customer = lead;
	}
	public OpportunitiesDO getProject() {
		return project;
	}
	public void setProject(OpportunitiesDO project) {
		this.project = project;
	}
	public PaymentTermDO getPaymentTerm() {
		return paymentTerm;
	}
	public void setPaymentTerm(PaymentTermDO paymentTerm) {
		this.paymentTerm = paymentTerm;
	}
	public String getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}
	public Long getProjectTypeId() {
		return projectTypeId;
	}
	public void setProjectTypeId(Long projectTypeId) {
		this.projectTypeId = projectTypeId;
	}
	public CountryDO getCurrencyType() {
		return currencyType;
	}
	public void setCurrencyType(CountryDO currencyType) {
		this.currencyType = currencyType;
	}
	public Long getQunatity() {
		return qunatity;
	}
	public void setQunatity(Long qunatity) {
		this.qunatity = qunatity;
	}
	public String getInvoiceTerm() {
		return invoiceTerm;
	}
	public void setInvoiceTerm(String invoiceTerm) {
		this.invoiceTerm = invoiceTerm;
	}
	public String getRate() {
		return rate;
	}
	public void setRate(String rate) {
		this.rate = rate;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getSupplierName() {
		return supplierName;
	}
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}
	public Long getDuration() {
		return duration;
	}
	public void setDuration(Long duration) {
		this.duration = duration;
	}
	public String getProjectType() {
		return projectType;
	}
	public void setProjectType(String projectType) {
		this.projectType = projectType;
	}
	public String getExpenses() {
		return expenses;
	}
	public void setExpenses(String expenses) {
		this.expenses = expenses;
	}
	public String getExpensesTerm() {
		return expensesTerm;
	}
	public void setExpensesTerm(String expensesTerm) {
		this.expensesTerm = expensesTerm;
	}
	public String getWorkLocation() {
		return workLocation;
	}
	public void setWorkLocation(String workLocation) {
		this.workLocation = workLocation;
	}
	public String getProposalName() {
		return proposalName;
	}
	public void setProposalName(String proposalName) {
		this.proposalName = proposalName;
	}
	public Date getProposalDate() {
		return proposalDate;
	}
	public void setProposalDate(Date proposalDate) {
		this.proposalDate = proposalDate;
	}
	public String getWorkDesc() {
		return workDesc;
	}
	public void setWorkDesc(String workDesc) {
		this.workDesc = workDesc;
	}
	
	public ContactDO getCustomerContact() {
		return customerContact;
	}
	public void setCustomerContact(ContactDO customerContact) {
		this.customerContact = customerContact;
	}
	public EmployeeDO getSupplierContact() {
		return supplierContact;
	}
	public void setSupplierContact(EmployeeDO supplierContact) {
		this.supplierContact = supplierContact;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
}