package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_generaldeduction")
@TableGenerator(name ="hrms_generaldeduction", initialValue =100101, allocationSize =1)
@NamedQueries({
	@NamedQuery(name = "hrms_generaldeduction.findAll", query = "SELECT r FROM GeneralDeductionDO r ORDER BY r.generalDeductionId DESC"),
    @NamedQuery(name = "hrms_generaldeduction.findById", query = "SELECT r FROM GeneralDeductionDO r where r.generalDeductionId =:id"),
    @NamedQuery(name = "hrms_generaldeduction.findActiveByEmpId", query = "SELECT r FROM GeneralDeductionDO r where r.employee.empId =:empId and r.status =:status"),
})
public class GeneralDeductionDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_BY_ID = "hrms_generaldeduction.findById";
	
	public static final String FIND_ALL = "hrms_generaldeduction.findAll";
	
	public static final String FIND_BY_EMPID = "hrms_generaldeduction.findByEmpId";
	
	public static final String FIND_ACTIVE_BY_EMPID = "hrms_generaldeduction.findActiveByEmpId";
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)  
	private Long generalDeductionId;
	private Long amount;
	private String month;
	private String noOfInstallments;
	private String installmentAmount;
	private String status;
	
	@ManyToOne
	@JoinColumn(name="empId")
	private EmployeeDO employee;
	
	@ManyToOne
	@JoinColumn(name="deductionTypeId")
	private DeductionTypeDO deductionType;
	
	@ManyToOne
	@JoinColumn(name="unitOrBranchId")
	private UnitOrBranchDO unitOrBranch;
	
	@ManyToOne
	@JoinColumn(name="deductionSubTypeId")
	private DeductionSubTypeDO deductionSubType;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
	public Long getGeneralDeductionId() {
		return generalDeductionId;
	}
	public void setGeneralDeductionId(Long generalDeductionId) {
		this.generalDeductionId = generalDeductionId;
	}
	public Long getAmount() {
		return amount;
	}
	public void setAmount(Long amount) {
		this.amount = amount;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public UnitOrBranchDO getUnitOrBranch() {
		return unitOrBranch;
	}
	public void setUnitOrBranch(UnitOrBranchDO unitOrBranch) {
		this.unitOrBranch = unitOrBranch;
	}
	public String getNoOfInstallments() {
		return noOfInstallments;
	}
	public void setNoOfInstallments(String noOfInstallments) {
		this.noOfInstallments = noOfInstallments;
	}
	public String getInstallmentAmount() {
		return installmentAmount;
	}
	public void setInstallmentAmount(String installmentAmount) {
		this.installmentAmount = installmentAmount;
	}
	public DeductionTypeDO getDeductionType() {
		return deductionType;
	}
	public void setDeductionType(DeductionTypeDO deductionType) {
		this.deductionType = deductionType;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public EmployeeDO getEmployee() {
		return employee;
	}
	public void setEmployee(EmployeeDO employee) {
		this.employee = employee;
	}
	/*public DeductionSubTypeDO getLoanType() {
		return loanType;
	}
	public void setLoanType(DeductionSubTypeDO loanType) {
		this.loanType = loanType;
	}*/
	
	public String getCreatedby() {
		return createdby;
	}
	public DeductionSubTypeDO getDeductionSubType() {
		return deductionSubType;
	}
	public void setDeductionSubType(DeductionSubTypeDO deductionSubType) {
		this.deductionSubType = deductionSubType;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
    
}