package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="crm_customeraccess")
@TableGenerator(name ="crm_customeraccess", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "crm_customeraccess.findById", query = "SELECT u FROM CustomerAccessDO u where u.customerAccessId =:id and u.isDeleted=0"),
    //@NamedQuery(name = "UserDO.findByUserName", query = "SELECT u FROM UserDO u where u.name =:name  and  u.idDeleted=0"),
    @NamedQuery(name = "crm_customeraccess.findByStatus", query = "SELECT u FROM CustomerAccessDO u where u.status =:status  and  u.isDeleted=0"),
    @NamedQuery(name = "crm_customeraccess.findByEmailId", query = "SELECT u FROM CustomerAccessDO u where u.contact.primaryemail =:email  and  u.isDeleted=0 and u.status = 'a'"),
    @NamedQuery(name = "crm_customeraccess.findForLogin", query = "SELECT u FROM CustomerAccessDO u where u.contact.primaryemail =:email  and  u.password=:password and u.isDeleted=0 and u.status =:status"),
    @NamedQuery(name = "crm_customeraccess.findBytempPassword", query = "SELECT u FROM CustomerAccessDO u where u.temppassowrd =:temppassword  and  u.isDeleted=0 and u.contact.contactId =:empId"),
    @NamedQuery(name = "crm_customeraccess.findByEmailForCustomer", query = "SELECT u FROM CustomerAccessDO u where (u.contact.primaryemail =:personal or u.contact.secondaryemail =:secondary) and  u.isDeleted=0"),
    @NamedQuery(name = "crm_customeraccess.findByCustomerId", query = "SELECT u FROM CustomerAccessDO u where u.contact.contactId =:userid and u.isDeleted=0"),
    @NamedQuery(name = "crm_customeraccess.findSuperAdmin", query = "SELECT u FROM CustomerAccessDO u where u.status =:status  and  u.isDeleted=0 and u.role.roleId =:adminId"),
    @NamedQuery(name = "crm_customeraccess.findforUpdate", query = "SELECT r FROM CustomerAccessDO r where r.contact.primaryemail=:email and r.customerAccessId NOT IN :id"),
    
})
public class CustomerAccessDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_BY_ID = "crm_customeraccess.findById";
	
	public static final String FIND_BY_EMAIL = "crm_customeraccess.findByEmailId";
	
	public static final String FIND_BY_EMAIL_FOR_CUSTOMER = "crm_customeraccess.findByEmailForCustomer";
	
	//public static final String FIND_BY_NAME = "UserDO.findByName";
	
	public static final String FIND_BY_STATUS = "crm_customeraccess.findByStatus";
	
	public static final String FIND_BY_USER_NAME = "crm_customeraccess.findByUserName";
	
	public static final String FIND_FOR_LOGIN = "crm_customeraccess.findForLogin";
	
	public static final String FIND_BY_TEMPPASSWORD = "crm_customeraccess.findBytempPassword";
	
	public static final String FIND_BY_CUSTOMER_ID = "crm_customeraccess.findByCustomerId";
	
	public static final String FIND_SUPERADMIN = "crm_customeraccess.findSuperAdmin";
	
	public static final String FIND_FOR_UPDATE = "crm_customeraccess.findforUpdate";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long customerAccessId;
	
	private String temppassowrd;
	private String password;
	private char status;
	private Long isDeleted;

	@ManyToOne
	@JoinColumn(name="contactId")
	private ContactDO contact;
	
	@ManyToOne
	@JoinColumn(name="roleId")
	private RolesDO role;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	
	public Long getCustomerAccessId() {
		return customerAccessId;
	}
	public void setCustomerAccessId(Long customerAccessId) {
		this.customerAccessId = customerAccessId;
	}
	public ContactDO getContact() {
		return contact;
	}
	public void setContact(ContactDO contact) {
		this.contact = contact;
	}
	public String getTemppassowrd() {
		return temppassowrd;
	}
	public void setTemppassowrd(String temppassowrd) {
		this.temppassowrd = temppassowrd;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public RolesDO getRole() {
		return role;
	}
	public void setRole(RolesDO role) {
		this.role = role;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public Long getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(Long isDeleted) {
		this.isDeleted = isDeleted;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}