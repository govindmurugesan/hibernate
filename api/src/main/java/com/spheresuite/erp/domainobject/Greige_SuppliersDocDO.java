package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="greige_suppliersdoc")
@TableGenerator(name ="greige_suppliersdoc", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "greige_suppliersdoc.findAll", query = "SELECT r FROM Greige_SuppliersDocDO r"),
    @NamedQuery(name = "greige_suppliersdoc.findByQuoteId", query = "SELECT r FROM Greige_SuppliersDocDO r where r.suppliers.supplierQuoteId =:id"),
})
public class Greige_SuppliersDocDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "greige_suppliersdoc.findAll";
	
	public static final String FIND_BY_SUPPLIERS_QUOTE_ID = "greige_suppliersdoc.findByQuoteId";
	
	@Id
	@Column(nullable = false, unique = true)
	@GeneratedValue(strategy=GenerationType.AUTO) 
	private Long suppliersDocId;
	
	@ManyToOne
	@JoinColumn(name="supplierQuoteId")
	private Greige_Spplier_QuoteDO suppliers;
	//private Long opportunity;
	@Column(columnDefinition="LONGTEXT")
	private String photo;
	private String fileName;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private String updatedBy;
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdon;
	private String createdBy;
	private String supplierId;
	
	public Long getSuppliersDocId() {
		return suppliersDocId;
	}
	public void setSuppliersDocId(Long suppliersDocId) {
		this.suppliersDocId = suppliersDocId;
	}
	public Greige_Spplier_QuoteDO getSuppliers() {
		return suppliers;
	}
	public void setSuppliers(Greige_Spplier_QuoteDO suppliers) {
		this.suppliers = suppliers;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getSupplierId() {
		return supplierId;
	}
	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}
}