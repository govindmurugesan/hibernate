package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_employeelop")
@TableGenerator(name ="hrms_employeelop", initialValue =100101, allocationSize =1)
@NamedQueries({
  //  @NamedQuery(name = "employeelop.findAll", query = "SELECT r FROM EmployeeLopDO r"),
    @NamedQuery(name = "hrms_employeelop.findById", query = "SELECT r FROM EmployeeLopDO r where r.lopId =:id"),
    @NamedQuery(name = "hrms_employeelop.findByEmpId", query = "SELECT r FROM EmployeeLopDO r where r.employee.empId =:empId and r.status =:status"),
    @NamedQuery(name = "hrms_employeelop.findByEmpDate", query = "SELECT r FROM EmployeeLopDO r where r.employee.empId =:empId and r.status =:status and r.startdate=:startDate and r.enddate=:enddate"),
    @NamedQuery(name = "hrms_employeelop.findByEmpDateForUpdate", query = "SELECT r FROM EmployeeLopDO r where r.employee.empId =:empId and r.status =:status and r.startdate=:startDate and r.enddate=:enddate and r.lopId <>:id"),
    /*@NamedQuery(name = "employeebonus.findByEmpIdsAndMonth", query = "SELECT r FROM EmployeeBonusDO r where r.empId IN :empId and SUBSTRING(r.startdate,1,7) >=:fromMonth and SUBSTRING(r.startdate,1,7) <=:toMonth"),
    @NamedQuery(name = "employeebonus.findByMonth", query = "SELECT r FROM EmployeeBonusDO r where r.bonusMonth = :monthly and r.status =:status"),*/
    /*SELECT * FROM spheresuite.employeelop where (SUBSTR(startdate,1,7) >= '2017-04' and SUBSTR(startdate,1,7) <='2018-03')*/
    @NamedQuery(name = "hrms_employeelop.findByEmpDateAndStatus", query = "SELECT r FROM EmployeeLopDO r where r.employee.empId =:empId and r.status =:status and ((r.startdate >=:startDate and r.enddate <=:enddate) or ((r.startdate >=:startDate and r.enddate IS NULL) and (r.startdate >=:startDate and r.startdate <=:enddate)))"),
})
public class EmployeeLopDO implements Serializable {
	private static final long serialVersionUID = 1L;

	//public static final String FIND_ALL = "employeelop.findAll";
	
	public static final String FIND_BY_ID = "hrms_employeelop.findById";
	
	public static final String FIND_BY_EMPID = "hrms_employeelop.findByEmpId";
	
	public static final String FIND_BY_EMPIDBYDATE = "hrms_employeelop.findByEmpDate";
	
	public static final String FIND_BY_EMPIDBYDATE_FORUPDATE = "hrms_employeelop.findByEmpDateForUpdate";
	
	public static final String FIND_BY_EMPID_DATE_STATUS = "hrms_employeelop.findByEmpDateAndStatus";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true) 
	private Long lopId;
	private String  type;
	private char status;
	@Temporal(TemporalType.DATE)
	private Date startdate;
	@Temporal(TemporalType.DATE)
	private Date enddate;
	@Temporal(TemporalType.DATE)
	private Date processedOn;
	
	@ManyToOne
	@JoinColumn(name="empId")
	private EmployeeDO employee;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getLopId() {
		return lopId;
	}
	public void setLopId(Long lopId) {
		this.lopId = lopId;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public Date getStartdate() {
		return startdate;
	}
	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Date getEnddate() {
		return enddate;
	}
	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}
	public Date getProcessedOn() {
		return processedOn;
	}
	public void setProcessedOn(Date processedOn) {
		this.processedOn = processedOn;
	}
	public EmployeeDO getEmployee() {
		return employee;
	}
	public void setEmployee(EmployeeDO employee) {
		this.employee = employee;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	
}