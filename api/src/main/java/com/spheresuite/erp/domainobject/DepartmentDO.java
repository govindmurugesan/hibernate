package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="master_department")
@TableGenerator(name ="master_department", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "master_department.findById", query = "SELECT r FROM DepartmentDO r where r.departmentId =:id"),
    @NamedQuery(name = "master_department.findByStatus", query = "SELECT r FROM DepartmentDO r where r.status =:status"),
    @NamedQuery(name = "master_department.findisManager", query = "SELECT r.departmentId FROM DepartmentDO r where r.isManager =:isManager and r.status =:status"),
    @NamedQuery(name = "master_department.findByName", query = "SELECT r FROM DepartmentDO r where r.name=:name"),
    @NamedQuery(name = "master_department.findByNameAndId", query = "SELECT r FROM DepartmentDO r where r.name=:name and r.departmentId NOT IN :id"),
    @NamedQuery(name = "master_department.findByNameAll", query = "SELECT r FROM DepartmentDO r where r.name=:name"),
    
})
public class DepartmentDO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public static final String FIND_BY_ID = "master_department.findById";
	
	public static final String FIND_BY_STATUS = "master_department.findByStatus";
	
	public static final String FIND_IS_MANAGER = "master_department.findisManager";
	
	public static final String FIND_BY_NAME = "master_department.findByName";
	
	public static final String FIND_FOR_UPDATE = "master_department.findByNameAndId";
	
	public static final String FIND_BY_NAME_ALL = "master_department.findByNameAll";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long departmentId;
	
	private String name;
	private char status;
	private String isManager;
	private String fromTime;
	private String toTime;
	private String maxHrExcuse;
	private String noOfExcuse;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
	private String updatedby;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	
	public Long getDepartmentId() {
		return departmentId;
	}
	public void setDepartmentId(Long departmentId) {
		this.departmentId = departmentId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getIsManager() {
		return isManager;
	}
	public void setIsManager(String isManager) {
		this.isManager = isManager;
	}
	public String getFromTime() {
		return fromTime;
	}
	public void setFromTime(String fromTime) {
		this.fromTime = fromTime;
	}
	public String getToTime() {
		return toTime;
	}
	public void setToTime(String toTime) {
		this.toTime = toTime;
	}
	public String getMaxHrExcuse() {
		return maxHrExcuse;
	}
	public void setMaxHrExcuse(String maxHrExcuse) {
		this.maxHrExcuse = maxHrExcuse;
	}
	public String getNoOfExcuse() {
		return noOfExcuse;
	}
	public void setNoOfExcuse(String noOfExcuse) {
		this.noOfExcuse = noOfExcuse;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}