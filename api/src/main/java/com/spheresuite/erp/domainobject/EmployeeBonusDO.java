package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_employeebonus")
@TableGenerator(name ="hrms_employeebonus", initialValue =100101, allocationSize =1)
@NamedQueries({
	@NamedQuery(name = "hrms_employeebonus.findAll", query = "SELECT r FROM EmployeeBonusDO r ORDER BY r.empBonusId DESC"),
    @NamedQuery(name = "hrms_employeebonus.findById", query = "SELECT r FROM EmployeeBonusDO r where r.empBonusId =:id"),
    @NamedQuery(name = "hrms_employeebonus.findByEmpId", query = "SELECT r FROM EmployeeBonusDO r where r.employee.empId =:empId"),
    @NamedQuery(name = "hrms_employeebonus.findByEmpIdAndMonth", query = "SELECT r FROM EmployeeBonusDO r where r.employee.empId =:empId and r.bonusMonth = :monthly and r.status =:status"),
    @NamedQuery(name = "hrms_employeebonus.findByEmpIdAndMonthStatus", query = "SELECT r FROM EmployeeBonusDO r where r.employee.empId =:empId and r.bonusMonth = :monthly"),
    @NamedQuery(name = "hrms_employeebonus.findByEmpIdBetweenMonth", query = "SELECT sum(r.amount) FROM EmployeeBonusDO r where r.employee.empId =:empId and (r.bonusMonth >= :fromMonth and r.bonusMonth <= :toMonth) and r.status =:status"),
    @NamedQuery(name = "hrms_employeebonus.findByEmpIdsAndMonth", query = "SELECT r FROM EmployeeBonusDO r where r.employee.empId IN :empId and r.bonusMonth = :monthly and r.status =:status"),
    @NamedQuery(name = "hrms_employeebonus.findByMonth", query = "SELECT r FROM EmployeeBonusDO r where r.bonusMonth = :monthly and r.status =:status"),
    
    @NamedQuery(name = "hrms_employeebonus.findByEmpIdsBetweenMonth", query = "SELECT r FROM EmployeeBonusDO r where r.employee.empId IN :empId and (r.bonusMonth >= :fromMonth and r.bonusMonth <= :toMonth) and r.status =:status"),
    @NamedQuery(name = "hrms_employeebonus.findBetweenMonth", query = "SELECT r FROM EmployeeBonusDO r where (r.bonusMonth >= :fromMonth and r.bonusMonth <= :toMonth) and r.status =:status "),
})
public class EmployeeBonusDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "hrms_employeebonus.findAll";
	
	public static final String FIND_BY_ID = "hrms_employeebonus.findById";
	
	public static final String FIND_BY_EMPID = "hrms_employeebonus.findByEmpId";
	
	public static final String FIND_BY_EMPID_DATE = "hrms_employeebonus.findByEmpIdAndMonth";
	
	public static final String FIND_BY_EMPID_DATE_WITHOUT_STATUS = "hrms_employeebonus.findByEmpIdAndMonthStatus";
	
	public static final String FIND_BY_EMPID_BETWEEN_DATE = "hrms_employeebonus.findByEmpIdBetweenMonth";
	
	public static final String FIND_BY_EMPID_MONTH = "hrms_employeebonus.findByEmpIdsAndMonth";
	
	public static final String FIND_BY_MONTH = "hrms_employeebonus.findByMonth";
	
	public static final String FIND_BY_EMPID_BETWEENMONTH = "hrms_employeebonus.findByEmpIdsBetweenMonth";
	
	public static final String FIND_BETWEENMONTH = "hrms_employeebonus.findBetweenMonth";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO) 
	@Column(nullable = false, unique = true) 
	private Long empBonusId;
	
	private Long amount;
	private String bonusMonth;
	private String comment;
	private char status;
	@Temporal(TemporalType.TIMESTAMP)
    private Date paidOn;
	
	@ManyToOne
	@JoinColumn(name="empId")
	private EmployeeDO employee;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getEmpBonusId() {
		return empBonusId;
	}
	public void setEmpBonusId(Long empBonusId) {
		this.empBonusId = empBonusId;
	}
	public Long getAmount() {
		return amount;
	}
	public void setAmount(Long amount) {
		this.amount = amount;
	}
	public String getBonusMonth() {
		return bonusMonth;
	}
	public void setBonusMonth(String bonusMonth) {
		this.bonusMonth = bonusMonth;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public Date getPaidOn() {
		return paidOn;
	}
	public void setPaidOn(Date paidOn) {
		this.paidOn = paidOn;
	}
	public EmployeeDO getEmployee() {
		return employee;
	}
	public void setEmployee(EmployeeDO employee) {
		this.employee = employee;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
    
}