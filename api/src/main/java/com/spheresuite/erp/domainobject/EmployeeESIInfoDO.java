package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_empESIInfo")
@TableGenerator(name ="hrms_empESIInfo", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "hrms_empESIInfo.findById", query = "SELECT r FROM EmployeeESIInfoDO r where r.empESIInfoId =:id"),
    @NamedQuery(name = "hrms_empESIInfo.findByEmpId", query = "SELECT r FROM EmployeeESIInfoDO r where r.employee.empId =:empId order by r.empESIInfoId desc"),
    @NamedQuery(name = "hrms_empESIInfo.findByEmpIdAndDate", query = "SELECT r FROM EmployeeESIInfoDO r where r.employee.empId =:empId and ((r.fromDate <=:fromDate and r.toDate >=:toDate) or ((r.fromDate <=:fromDate and r.toDate IS NULL) and (r.fromDate <=:toDate)))"),
   // @NamedQuery(name = "hrms_empPFInfo.findByAccountId", query = "SELECT r FROM EmployeePFInfoDO r where r.PFAccNo =:pfAccountNo and r.ESIAccNo =:esiAccountNo"),
})
public class EmployeeESIInfoDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_BY_ID = "hrms_empESIInfo.findById";
	
	public static final String FIND_BY_EMPID = "hrms_empESIInfo.findByEmpId";
	
	public static final String FIND_BY_EMPID_DATE = "hrms_empESIInfo.findByEmpIdAndDate";
	
	//public static final String FIND_BY_ACCOUNTID = "hrms_empPFInfo.findByAccountId";
	
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)  
	private Long empESIInfoId;
	
	private String ESIAccNo;
	private String type;
	
	private Long amount;
	
	@ManyToOne
	@JoinColumn(name="esiGroupId")
	private ESIGroupDO esiGroup;
	
	@ManyToOne
	@JoinColumn(name="empId")
	private EmployeeDO employee;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date fromDate;
	 
	@Temporal(TemporalType.TIMESTAMP)
	private Date toDate;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
	public Long getEmpESIInfoId() {
		return empESIInfoId;
	}
	public void setEmpESIInfoId(Long empESIInfoId) {
		this.empESIInfoId = empESIInfoId;
	}
	public String getESIAccNo() {
		return ESIAccNo;
	}
	public void setESIAccNo(String eSIAccNo) {
		ESIAccNo = eSIAccNo;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Long getAmount() {
		return amount;
	}
	public void setAmount(Long amount) {
		this.amount = amount;
	}
	public ESIGroupDO getEsiGroup() {
		return esiGroup;
	}
	public void setEsiGroup(ESIGroupDO esiGroup) {
		this.esiGroup = esiGroup;
	}
	public EmployeeDO getEmployee() {
		return employee;
	}
	public void setEmployee(EmployeeDO employee) {
		this.employee = employee;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}