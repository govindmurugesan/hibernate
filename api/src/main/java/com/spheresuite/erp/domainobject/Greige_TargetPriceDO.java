package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="greige_targetprice")
@TableGenerator(name ="greige_targetprice", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "greige_targetprice.findById", query = "SELECT r FROM Greige_TargetPriceDO r where r.targetPriceId =:id"),
    @NamedQuery(name = "greige_targetprice.findAll", query = "SELECT r FROM Greige_TargetPriceDO r order by r.createdon desc"),
    @NamedQuery(name = "greige_targetprice.findBySortId", query = "SELECT r FROM Greige_TargetPriceDO r where r.sortName.sortId =:id"),
    @NamedQuery(name = "greige_targetprice.findBySupplierId", query = "SELECT r FROM Greige_TargetPriceDO r where r.supplierId =:id"),
    @NamedQuery(name = "greige_targetprice.findByQuoteId", query = "SELECT r FROM Greige_TargetPriceDO r where r.quoteId =:id"),
    @NamedQuery(name = "greige_targetprice.findBySupplierSortQuoteId", query = "SELECT r FROM Greige_TargetPriceDO r where r.sortName.sortId =:sortId AND r.supplierId =:supplierId AND r.quoteId =:quotId"),
    //  @NamedQuery(name = "greige_targetprice.findByStatus", query = "SELECT r FROM greige_targetprice r where r.status =:status"),
/*    @NamedQuery(name = "ep_master_sort.findByName", query = "SELECT r FROM EarningTypeDO r where r.name=:name"),
    @NamedQuery(name = "ep_master_sort.findByNameAndId", query = "SELECT r FROM EarningTypeDO r where r.name=:name and r.earningTypeId NOT IN :id"),*/
})
public class Greige_TargetPriceDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_BY_ID = "greige_targetprice.findById";
	
	public static final String FIND_ALL = "greige_targetprice.findAll";
	
	public static final String FIND_BY_SORTID= "greige_targetprice.findBySortId";
	
	public static final String FIND_BY_SUPPLIER_ID= "greige_targetprice.findBySupplierId";
	
	public static final String FIND_BY_QUOTE_ID= "greige_targetprice.findByQuoteId";
	
	public static final String FIND_BY_SUPPLIER_SORT_QUOTE_ID= "greige_targetprice.findBySupplierSortQuoteId";
	
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long targetPriceId;
	
	@ManyToOne
	@JoinColumn(name="sortId")
	private EPMasterSortDO sortName;
	private String quoteId;
	private String supplierId;
	//private String supplierId;
	private String targetPrice;
	private String targetQty;
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	
	public Long getTargetPriceId() {
		return targetPriceId;
	}
	public void setTargetPriceId(Long targetPriceId) {
		this.targetPriceId = targetPriceId;
	}
	public EPMasterSortDO getSortName() {
		return sortName;
	}
	public void setSortName(EPMasterSortDO sortName) {
		this.sortName = sortName;
	}
	public String getQuoteId() {
		return quoteId;
	}
	public void setQuoteId(String quoteId) {
		this.quoteId = quoteId;
	}
	public String getSupplierId() {
		return supplierId;
	}
	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}
	public String getTargetPrice() {
		return targetPrice;
	}
	public void setTargetPrice(String targetPrice) {
		this.targetPrice = targetPrice;
	}
	public String getTargetQty() {
		return targetQty;
	}
	public void setTargetQty(String targetQty) {
		this.targetQty = targetQty;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}