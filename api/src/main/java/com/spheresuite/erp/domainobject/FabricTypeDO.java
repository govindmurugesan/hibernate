package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="pl_master_fabrictype")
@TableGenerator(name ="pl_master_fabrictype", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "pl_master_fabrictype.findById", query = "SELECT r FROM FabricTypeDO r where r.fabricTypeId =:id"),
    @NamedQuery(name = "pl_master_fabrictype.findByStatus", query = "SELECT r FROM FabricTypeDO r where r.status =:status"),
    @NamedQuery(name = "pl_master_fabrictype.findByName", query = "SELECT r FROM FabricTypeDO r where r.name=:name"),
    @NamedQuery(name = "pl_master_fabrictype.findByNameAndId", query = "SELECT r FROM FabricTypeDO r where r.name=:name and r.fabricTypeId NOT IN :id"),
})
public class FabricTypeDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static final String FIND_BY_ID = "pl_master_fabrictype.findById";
	
	public static final String FIND_BY_STATUS = "pl_master_fabrictype.findByStatus";
	
	public static final String FIND_BY_NAME = "pl_master_fabrictype.findByName";
	
	public static final String FIND_FOR_UPDATE = "pl_master_fabrictype.findByNameAndId";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long fabricTypeId;
	private String name;
	private char status;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getFabricTypeId() {
		return fabricTypeId;
	}
	public void setFabricTypeId(Long fabricTypeId) {
		this.fabricTypeId = fabricTypeId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}