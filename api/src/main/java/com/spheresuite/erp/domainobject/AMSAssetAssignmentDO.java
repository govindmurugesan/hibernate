package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="ams_assignedasset")
@TableGenerator(name ="ams_assignedasset", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "ams_assignedasset.findById", query = "SELECT r FROM AMSAssetAssignmentDO r where r.assetAssignmentId =:id"),
    @NamedQuery(name = "ams_assignedasset.findForAdd", query = "SELECT r FROM AMSAssetAssignmentDO r where r.asset.assetId =:id and r.status =:status"),
    @NamedQuery(name = "ams_assignedasset.findByUnit", query = "SELECT r FROM AMSAssetAssignmentDO r where r.unit.unitId =:id and r.status =:status"),
    @NamedQuery(name = "ams_assignedasset.findByEmpId", query = "SELECT r FROM AMSAssetAssignmentDO r where r.employee.empId =:id"),
})
public class AMSAssetAssignmentDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_BY_ID = "ams_assignedasset.findById";
	
	public static final String FIND_FOR_ADD = "ams_assignedasset.findForAdd";
	
	public static final String FIND_BY_UNIT = "ams_assignedasset.findByUnit";
	
	public static final String FIND_BY_EMPID = "ams_assignedasset.findByEmpId";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO) 
	@Column(nullable = false, unique = true) 
	private Long assetAssignmentId;
	
	@Temporal(TemporalType.DATE)
	private Date assignedDate;
	@Temporal(TemporalType.DATE)
	private Date releasedDate;
	private char status;
	
	@ManyToOne
	@JoinColumn(name="empId")
	private EmployeeDO employee;
	
	@ManyToOne
	@JoinColumn(name="assetId")
	private AMSAssetDO asset;
	
	@ManyToOne
	@JoinColumn(name="unitId")
	private AMSUnitDO unit;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
	
	public Long getAssetAssignmentId() {
		return assetAssignmentId;
	}
	public void setAssetAssignmentId(Long assetAssignmentId) {
		this.assetAssignmentId = assetAssignmentId;
	}
	public Date getAssignedDate() {
		return assignedDate;
	}
	public void setAssignedDate(Date assignedDate) {
		this.assignedDate = assignedDate;
	}
	public Date getReleasedDate() {
		return releasedDate;
	}
	public void setReleasedDate(Date releasedDate) {
		this.releasedDate = releasedDate;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public EmployeeDO getEmployee() {
		return employee;
	}
	public void setEmployee(EmployeeDO employee) {
		this.employee = employee;
	}
	public AMSAssetDO getAsset() {
		return asset;
	}
	public void setAsset(AMSAssetDO asset) {
		this.asset = asset;
	}
	public AMSUnitDO getUnit() {
		return unit;
	}
	public void setUnit(AMSUnitDO unit) {
		this.unit = unit;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}