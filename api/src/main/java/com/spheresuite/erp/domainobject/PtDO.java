package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_master_pt")
@TableGenerator(name ="hrms_master_pt", initialValue =100101, allocationSize =1)
@NamedQueries({
	 @NamedQuery(name = "hrms_master_pt.findAll", query = "SELECT r FROM PtDO r"),
    @NamedQuery(name = "hrms_master_pt.findById", query = "SELECT r FROM PtDO r where r.ptId =:id"),
    @NamedQuery(name = "hrms_master_pt.findByStatus", query = "SELECT r FROM PtDO r where r.status =:status"),
    @NamedQuery(name = "hrms_master_pt.findByState", query = "SELECT r FROM PtDO r where r.status =:status and r.state.stateId =:id"),
    @NamedQuery(name = "hrms_master_pt.findDuplicatePersist", query = "SELECT r FROM PtDO r where r.state.stateId =:stateId and r.country.countryId =:countryId and "
    		+ "((r.fromamount <=:fromAmount and r.toamount >=:toAmount))"),
    @NamedQuery(name = "hrms_master_pt.findDuplicateUpdate", query = "SELECT r FROM PtDO r where r.state.stateId =:stateId and r.country.countryId =:countryId and "
    		+ "r.fromamount =:fromAmount and r.toamount =:toAmount and r.ptamount =:ptAmount and r.ptId  <>:id"),
    		
   @NamedQuery(name = "hrms_master_pt.findForPayroll", query = "SELECT r FROM PtDO r where r.state.stateId =:stateId and r.status =:status and (r.fromamount <=:amount and r.toamount >=:amount)"),
})
public class PtDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static final String FIND_ALL = "hrms_master_pt.findAll";

	public static final String FIND_BY_ID = "hrms_master_pt.findById";
	
	public static final String FIND_BY_STATUS = "hrms_master_pt.findByStatus";
	
	public static final String FIND_BY_STATE = "hrms_master_pt.findByState";
	
	public static final String FIND_BY_DUPLICATE_PERSIST = "hrms_master_pt.findDuplicatePersist";
	
	public static final String FIND_BY_DUPLICATE_UPDATE = "hrms_master_pt.findDuplicateUpdate";
	
	public static final String FIND_FOR_PAYROLL = "hrms_master_pt.findForPayroll";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true) 
	private Long ptId;
	
	private Long fromamount;
	private Long toamount;
	private Long ptamount;
	private char status;
	
	@ManyToOne
	@JoinColumn(name="stateId")
	private StateDO state;
	@ManyToOne
	@JoinColumn(name="countryId")
	private CountryDO country;
	
	@ManyToOne
	@JoinColumn(name="worklocationId")
	private WorkLocationDO worklocation;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getPtId() {
		return ptId;
	}
	public void setPtId(Long ptId) {
		this.ptId = ptId;
	}
	public Long getFromamount() {
		return fromamount;
	}
	public void setFromamount(Long fromamount) {
		this.fromamount = fromamount;
	}
	public Long getToamount() {
		return toamount;
	}
	public void setToamount(Long toamount) {
		this.toamount = toamount;
	}
	public Long getPtamount() {
		return ptamount;
	}
	public void setPtamount(Long ptamount) {
		this.ptamount = ptamount;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public StateDO getState() {
		return state;
	}
	public void setState(StateDO state) {
		this.state = state;
	}
	public CountryDO getCountry() {
		return country;
	}
	public void setCountry(CountryDO country) {
		this.country = country;
	}
	public WorkLocationDO getWorklocation() {
		return worklocation;
	}
	public void setWorklocation(WorkLocationDO worklocation) {
		this.worklocation = worklocation;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}