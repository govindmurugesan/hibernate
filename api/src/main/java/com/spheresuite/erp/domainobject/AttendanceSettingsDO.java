package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="master_attendancesettings")
@TableGenerator(name ="master_attendancesettings", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "master_attendancesettings.findById", query = "SELECT r FROM AttendanceSettingsDO r where r.attendanceSettingId =:id"),
    @NamedQuery(name = "master_attendancesettings.findByStatus", query = "SELECT r FROM AttendanceSettingsDO r where r.status =:status"),
    @NamedQuery(name = "master_attendancesettings.findByDatabaseType", query = "SELECT r FROM AttendanceSettingsDO r where r.databaseType =:databaseType and r.status =:status"),
    @NamedQuery(name = "master_attendancesettings.findByName", query = "SELECT r FROM AttendanceSettingsDO r where r.databaseType=:databaseType"),
    @NamedQuery(name = "master_attendancesettings.findByNameAndId", query = "SELECT r FROM AttendanceSettingsDO r where r.databaseType=:databaseType and r.attendanceSettingId NOT IN :id"),
})
public class AttendanceSettingsDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "master_attendancesettings.findAll";
	
	public static final String FIND_BY_ID = "master_attendancesettings.findById";
	
	public static final String FIND_BY_STATUS = "master_attendancesettings.findByStatus";
	
	public static final String FIND_BY_DATABASETYPE = "master_attendancesettings.findByDatabaseType";
	
	public static final String FIND_BY_NAME = "master_attendancesettings.findByName";
	
	public static final String FIND_FOR_UPDATE = "master_attendancesettings.findByNameAndId";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long attendanceSettingId;
	
	private String databaseType;
	private String databaseName;
	private String vendor;
	private String host;
	private String port;
	private String status;
	private String username;
	private String password;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getAttendanceSettingId() {
		return attendanceSettingId;
	}
	public void setAttendanceSettingId(Long attendanceSettingId) {
		this.attendanceSettingId = attendanceSettingId;
	}
	public String getDatabaseType() {
		return databaseType;
	}
	public void setDatabaseType(String databaseType) {
		this.databaseType = databaseType;
	}
	public String getDatabaseName() {
		return databaseName;
	}
	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}
	public String getVendor() {
		return vendor;
	}
	public void setVendor(String vendor) {
		this.vendor = vendor;
	}
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}