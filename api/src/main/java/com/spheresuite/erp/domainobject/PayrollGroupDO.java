package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_payrollgroup")
@TableGenerator(name ="hrms_payrollgroup", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "hrms_payrollgroup.findById", query = "SELECT r FROM PayrollGroupDO r where r.payrollGroupId =:id"),
    @NamedQuery(name = "hrms_payrollgroup.findByStatus", query = "SELECT r FROM PayrollGroupDO r where r.status =:status"),
    @NamedQuery(name = "hrms_payrollgroup.findByName", query = "SELECT r FROM PayrollGroupDO r where r.name=:name"),
    @NamedQuery(name = "hrms_payrollgroup.findByNameAndId", query = "SELECT r FROM PayrollGroupDO r where r.name=:name and r.payrollGroupId NOT IN :id"),
})
public class PayrollGroupDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_BY_ID = "hrms_payrollgroup.findById";
	
	public static final String FIND_BY_STATUS = "hrms_payrollgroup.findByStatus";
	
	public static final String FIND_BY_NAME = "hrms_payrollgroup.findByName";
	
	public static final String FIND_FOR_UPDATE = "hrms_payrollgroup.findByNameAndId";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long payrollGroupId;
	
	private String name;
	private String description;
	private char status;
	
	@ManyToOne
	@JoinColumn(name="unitOrBranchId")
	private UnitOrBranchDO unitOrBranch;
	
	@OneToMany(mappedBy="payrollGroup", fetch = FetchType.EAGER)
    private Set<PayrollGroupEarningsDO> payrollGroupEarning;
	
	@OneToMany(mappedBy="payrollGroup", fetch = FetchType.EAGER)
    private Set<PayrollGroupDeductionDO> payrollGroupDeduction;
	
	@OneToMany(mappedBy="payrollGroup", fetch = FetchType.EAGER)
    private Set<PayrollGroupTaxDO> payrollGroupTax;
	
	@OneToMany(mappedBy="payrollGroup", fetch = FetchType.EAGER)
    private Set<PayrollGroupWorkingdaysAllowanceDO> payrollGroupWorkingdays;
	
	
	@OneToMany(mappedBy="payrollGroup", fetch = FetchType.EAGER)
    private Set<PayrollGroupAttendanceAllowanceDO > payrollGroupAttendanceAllowance;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getPayrollGroupId() {
		return payrollGroupId;
	}
	public void setPayrollGroupId(Long payrollGroupId) {
		this.payrollGroupId = payrollGroupId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public UnitOrBranchDO getUnitOrBranch() {
		return unitOrBranch;
	}
	public void setUnitOrBranch(UnitOrBranchDO unitOrBranch) {
		this.unitOrBranch = unitOrBranch;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public Set<PayrollGroupEarningsDO> getPayrollGroupEarning() {
		return payrollGroupEarning;
	}
	public void setPayrollGroupEarning(
			Set<PayrollGroupEarningsDO> payrollGroupEarning) {
		this.payrollGroupEarning = payrollGroupEarning;
	}
	public Set<PayrollGroupDeductionDO> getPayrollGroupDeduction() {
		return payrollGroupDeduction;
	}
	public void setPayrollGroupDeduction(
			Set<PayrollGroupDeductionDO> payrollGroupDeduction) {
		this.payrollGroupDeduction = payrollGroupDeduction;
	}
	public Set<PayrollGroupWorkingdaysAllowanceDO> getPayrollGroupWorkingdays() {
		return payrollGroupWorkingdays;
	}
	public void setPayrollGroupWorkingdays(
			Set<PayrollGroupWorkingdaysAllowanceDO> payrollGroupWorkingdays) {
		this.payrollGroupWorkingdays = payrollGroupWorkingdays;
	}
	public Set<PayrollGroupTaxDO> getPayrollGroupTax() {
		return payrollGroupTax;
	}
	public void setPayrollGroupTax(Set<PayrollGroupTaxDO> payrollGroupTax) {
		this.payrollGroupTax = payrollGroupTax;
	}
	public Set<PayrollGroupAttendanceAllowanceDO> getPayrollGroupAttendanceAllowance() {
		return payrollGroupAttendanceAllowance;
	}
	public void setPayrollGroupAttendanceAllowance(
			Set<PayrollGroupAttendanceAllowanceDO> payrollGroupAttendanceAllowance) {
		this.payrollGroupAttendanceAllowance = payrollGroupAttendanceAllowance;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}