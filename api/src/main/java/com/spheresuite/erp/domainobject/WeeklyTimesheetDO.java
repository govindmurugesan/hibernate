package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_weeklytimesheet")
@TableGenerator(name ="hrms_weeklytimesheet", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "hrms_weeklytimesheet.findAllByDept", query = "SELECT r FROM WeeklyTimesheetDO r where r.empId IN :id"),
    @NamedQuery(name = "hrms_weeklytimesheet.findById", query = "SELECT r FROM WeeklyTimesheetDO r where r.id =:id"),
    @NamedQuery(name = "hrms_weeklytimesheet.findByDate", query = "SELECT r FROM WeeklyTimesheetDO r where r.date =:weekenddate and r.empId =:empId"),
    @NamedQuery(name = "hrms_weeklytimesheet.findByEmpId", query = "SELECT r FROM WeeklyTimesheetDO r where r.empId =:empId"),
    @NamedQuery(name = "hrms_weeklytimesheet.findSubmitted", query = "SELECT r FROM WeeklyTimesheetDO r where r.status =:status"),
})
public class WeeklyTimesheetDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_BY_ID = "hrms_weeklytimesheet.findById";
	
	public static final String FIND_BY_DATE = "hrms_weeklytimesheet.findByDate";
	
	public static final String FIND_BY_EMPID = "hrms_weeklytimesheet.findByEmpId";
	
	public static final String FIND_SUBMITTED = "hrms_weeklytimesheet.findSubmitted";
	
	public static final String FIND_ALL_BY_DEPT = "hrms_weeklytimesheet.findAllByDept";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO) 
	@Column(nullable = false, unique = true)
	private Long weeklyTimesheetId;
	
	@Column(columnDefinition="LONGTEXT")
	private Long hours;
	private String date;
	private String empId;
	private String comments;
	private char status;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
	public Long getWeeklyTimesheetId() {
		return weeklyTimesheetId;
	}
	public void setWeeklyTimesheetId(Long weeklyTimesheetId) {
		this.weeklyTimesheetId = weeklyTimesheetId;
	}
	public Long getHours() {
		return hours;
	}
	public void setHours(Long hours) {
		this.hours = hours;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getEmpId() {
		return empId;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}