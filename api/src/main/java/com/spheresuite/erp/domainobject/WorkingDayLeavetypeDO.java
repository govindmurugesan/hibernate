package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_workingdayleavetype")
@TableGenerator(name ="hrms_workingdayleavetype", initialValue =100101, allocationSize =1)
@NamedQueries({
	 @NamedQuery(name = "hrms_workingdayleavetype.findAll", query = "SELECT r FROM WorkingDayLeavetypeDO r"),
	 @NamedQuery(name = "hrms_workingdayleavetype.findById", query = "SELECT r FROM WorkingDayLeavetypeDO r where r.workingDayLeavetypeId =:id"),
	 @NamedQuery(name = "hrms_workingdayleavetype.findByStatus", query = "SELECT r FROM WorkingDayLeavetypeDO r where r.status =:status"),
	 @NamedQuery(name = "hrms_workingdayleavetype.findByWorkingId", query = "SELECT r FROM WorkingDayLeavetypeDO r where r.workingDayAllowance.workingDayAllowanceId =:id"),
	 
	 
  /*  @NamedQuery(name = "hrms_workingDayAllowance.findById", query = "SELECT r FROM AddressTypeDO r where r.addressTypeId =:id"),
    */
  /*  @NamedQuery(name = "hrms_workingDayAllowance.findByName", query = "SELECT r FROM AddressTypeDO r where r.name=:name"),
    @NamedQuery(name = "hrms_workingDayAllowance.findByNameAndId", query = "SELECT r FROM AddressTypeDO r where r.name=:name and r.addressTypeId NOT IN :id"),*/
})
public class WorkingDayLeavetypeDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static final String FIND_ALL = "hrms_workingdayleavetype.findAll";
	
	public static final String FIND_BY_ID = "hrms_workingdayleavetype.findById";
	
	public static final String FIND_BY_STATUS = "hrms_workingdayleavetype.findByStatus";
	
	public static final String FIND_BY_WORKING_ID = "hrms_workingdayleavetype.findByWorkingId";
	
	
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long workingDayLeavetypeId;
	private String status;
	
	@ManyToOne
	@JoinColumn(name="leaveManagementId")
	private LeaveManagementDO leaveManagement;
	
	@ManyToOne
	@JoinColumn(name="workingDayAllowanceId")
	private WorkingDayAllowanceDO workingDayAllowance;
	
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getWorkingDayLeavetypeId() {
		return workingDayLeavetypeId;
	}
	public void setWorkingDayLeavetypeId(Long workingDayLeavetypeId) {
		this.workingDayLeavetypeId = workingDayLeavetypeId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public LeaveManagementDO getLeaveManagement() {
		return leaveManagement;
	}
	public void setLeaveManagement(LeaveManagementDO leaveManagement) {
		this.leaveManagement = leaveManagement;
	}
	public WorkingDayAllowanceDO getWorkingDayAllowance() {
		return workingDayAllowance;
	}
	public void setWorkingDayAllowance(WorkingDayAllowanceDO workingDayAllowance) {
		this.workingDayAllowance = workingDayAllowance;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}