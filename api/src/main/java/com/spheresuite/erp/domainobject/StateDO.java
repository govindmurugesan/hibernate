package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name="master_state")
@TableGenerator(name ="master_state", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "master_state.findAll", query = "SELECT r FROM StateDO r"),
    @NamedQuery(name = "master_state.findById", query = "SELECT r FROM StateDO r where r.stateId =:id"),
    @NamedQuery(name = "master_state.findByStatus", query = "SELECT r FROM StateDO r where r.status =:status"),
    @NamedQuery(name = "master_state.findByCountryId", query = "SELECT r FROM StateDO r where r.country.countryId =:countryId"),
    @NamedQuery(name = "master_state.findByName", query = "SELECT r FROM StateDO r where r.stateName=:name"),
    @NamedQuery(name = "master_state.findByNameAndId", query = "SELECT r FROM StateDO r where r.stateName=:name and r.stateId NOT IN :id"),
})
public class StateDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "master_state.findAll";
	
	public static final String FIND_BY_ID = "master_state.findById";
	
	public static final String FIND_BY_STATUS = "master_state.findByStatus";
	
	public static final String FIND_BY_COUNTRY_ID="master_state.findByCountryId";
	
	public static final String FIND_BY_NAME = "master_state.findByName";
	
	public static final String FIND_FOR_UPDATE = "master_state.findByNameAndId";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO) 
	@Column(nullable = false, unique = true)
	private Long stateId;

	private String stateName;
	private char status;
	
	@OneToMany(mappedBy="state", fetch = FetchType.EAGER)
    private Set<UnitOrBranchDO> unitOrBranch;
	
	@ManyToOne
	@JoinColumn(name="countryId")
	private CountryDO country;
	
    private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getStateId() {
		return stateId;
	}
	public void setStateId(Long stateId) {
		this.stateId = stateId;
	}
	public String getStateName() {
		return stateName;
	}
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public Set<UnitOrBranchDO> getUnitOrBranch() {
		return unitOrBranch;
	}
	public void setUnitOrBranch(Set<UnitOrBranchDO> unitOrBranch) {
		this.unitOrBranch = unitOrBranch;
	}
	public CountryDO getCountry() {
		return country;
	}
	public void setCountry(CountryDO country) {
		this.country = country;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}