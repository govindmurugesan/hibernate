package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="employeecertification")
@TableGenerator(name ="employeecertification", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "employeecertification.findById", query = "SELECT r FROM EmployeeCertificationDO r where r.empCertificationId =:id"),
    @NamedQuery(name = "employeecertification.findByEmpId", query = "SELECT r FROM EmployeeCertificationDO r where r.employee.empId =:empId order by r.empCertificationId desc"),
})
public class EmployeeCertificationDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_BY_ID = "employeecertification.findById";
	
	public static final String FIND_BY_EMPID = "employeecertification.findByEmpId";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO) 
	private Long empCertificationId;
	private String month;
	private String title;
	private String comment;
	
	@ManyToOne
	@JoinColumn(name="empId")
	private EmployeeDO employee;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getEmpCertificationId() {
		return empCertificationId;
	}
	public void setEmpCertificationId(Long empCertificationId) {
		this.empCertificationId = empCertificationId;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public EmployeeDO getEmployee() {
		return employee;
	}
	public void setEmployee(EmployeeDO employee) {
		this.employee = employee;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}