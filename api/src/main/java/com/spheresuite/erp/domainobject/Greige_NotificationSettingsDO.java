package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="greige_master_notificationsettings")
@TableGenerator(name ="greige_master_notificationsettings", initialValue =100101, allocationSize =1)
@NamedQueries({
	@NamedQuery(name = "greige_master_notificationsettings.findAll", query = "SELECT r FROM Greige_NotificationSettingsDO r"),
    @NamedQuery(name = "greige_master_notificationsettings.findById", query = "SELECT r FROM Greige_NotificationSettingsDO r where r.greigeNotificationSettingId =:id"),
})
public class Greige_NotificationSettingsDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "greige_master_notificationsettings.findAll";
	
	public static final String FIND_BY_ID = "greige_master_notificationsettings.findById";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true) 
	private Long greigeNotificationSettingId;
	
	private String approver;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getGreigeNotificationSettingId() {
		return greigeNotificationSettingId;
	}
	public void setGreigeNotificationSettingId(Long greigeNotificationSettingId) {
		this.greigeNotificationSettingId = greigeNotificationSettingId;
	}
	public String getApprover() {
		return approver;
	}
	public void setApprover(String approver) {
		this.approver = approver;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}