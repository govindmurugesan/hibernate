package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="crm_salespolicies")
@TableGenerator(name ="crm_salespolicies", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "crm_salespolicies.findAll", query = "SELECT r FROM SalesPoliciesDO r where r.employee.empId IN :id"),
    @NamedQuery(name = "crm_salespolicies.findAllData", query = "SELECT r FROM SalesPoliciesDO r"),
    @NamedQuery(name = "crm_salespolicies.findById", query = "SELECT r FROM SalesPoliciesDO r where r.salesPoliciesId =:id"),
})
public class SalesPoliciesDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "crm_salespolicies.findAll";
	
	public static final String FIND_BY_ID = "crm_salespolicies.findById";
	
	public static final String FIND_ALL_DATA="crm_salespolicies.findAllData";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO) 
	@Column(nullable = false, unique = true)
	private Long salesPoliciesId;
	
	@Column(columnDefinition="LONGTEXT")
	private String name;
	private char status;
	
	@ManyToOne
	@JoinColumn(name="empId")
	private EmployeeDO employee;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
    
	public Long getSalesPoliciesId() {
		return salesPoliciesId;
	}
	public void setSalesPoliciesId(Long salesPoliciesId) {
		this.salesPoliciesId = salesPoliciesId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public EmployeeDO getEmployee() {
		return employee;
	}
	public void setEmployee(EmployeeDO employee) {
		this.employee = employee;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}