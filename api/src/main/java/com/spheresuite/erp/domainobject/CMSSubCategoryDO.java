package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="cms_subcategory")
@TableGenerator(name ="cms_subcategory", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "cms_subcategory.findAll", query = "SELECT r FROM CMSSubCategoryDO r"),
    @NamedQuery(name = "cms_subcategory.findById", query = "SELECT r FROM CMSSubCategoryDO r where r.id =:id"),
    @NamedQuery(name = "cms_subcategory.findByStatus", query = "SELECT r FROM CMSSubCategoryDO r where r.status =:status"),
    @NamedQuery(name = "cms_subcategory.findByName", query = "SELECT r FROM CMSSubCategoryDO r where r.status =:status and r.name=:name"),
    @NamedQuery(name = "cms_subcategory.findByNameAndId", query = "SELECT r FROM CMSSubCategoryDO r where r.status =:status and r.name=:name and r.id NOT IN :id"),
    @NamedQuery(name = "cms_subcategory.findByNameAll", query = "SELECT r FROM CMSSubCategoryDO r where r.name=:name"),
})
public class CMSSubCategoryDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "cms_subcategory.findAll";
	
	public static final String FIND_BY_ID = "cms_subcategory.findById";
	
	public static final String FIND_BY_STATUS = "cms_subcategory.findByStatus";
	
	public static final String FIND_BY_NAME = "cms_subcategory.findByName";
	public static final String FIND_FOR_UPDATE = "cms_subcategory.findByNameAndId";
	
	public static final String FIND_BY_NAME_ALL = "cms_subcategory.findByNameAll";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO) 
	private Long id;
	private Long categoryId;
	private String name;
	private String commets;
	private char status;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	
	private String updatedBy;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCommets() {
		return commets;
	}

	public void setCommets(String commets) {
		this.commets = commets;
	}

	public char getStatus() {
		return status;
	}

	public void setStatus(char status) {
		this.status = status;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedon() {
		return updatedon;
	}

	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	
}