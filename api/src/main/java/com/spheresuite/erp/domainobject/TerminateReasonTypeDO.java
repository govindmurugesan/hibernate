package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="master_terminatereason")
@TableGenerator(name ="master_terminatereason", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "master_terminatereason.findById", query = "SELECT r FROM TerminateReasonTypeDO r where r.terminationReasonId =:id"),
    @NamedQuery(name = "master_terminatereason.findByStatus", query = "SELECT r FROM TerminateReasonTypeDO r where r.status =:status"),
    @NamedQuery(name = "master_terminatereason.findByName", query = "SELECT r FROM TerminateReasonTypeDO r where r.name=:name"),
    @NamedQuery(name = "master_terminatereason.findByNameAndId", query = "SELECT r FROM TerminateReasonTypeDO r where r.name=:name and r.terminationReasonId NOT IN :id"),
})
public class TerminateReasonTypeDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_BY_ID = "master_terminatereason.findById";
	
	public static final String FIND_BY_STATUS = "master_terminatereason.findByStatus";
	
	public static final String FIND_BY_NAME = "master_terminatereason.findByName";
	
	public static final String FIND_FOR_UPDATE = "master_terminatereason.findByNameAndId";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long terminationReasonId;
	private String name;
	private char status;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getTerminationReasonId() {
		return terminationReasonId;
	}
	public void setTerminationReasonId(Long terminationReasonId) {
		this.terminationReasonId = terminationReasonId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}