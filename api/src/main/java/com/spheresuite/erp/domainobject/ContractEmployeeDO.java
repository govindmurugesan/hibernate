package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="wf_contract_employee")
@TableGenerator(name ="wf_contract_employee", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "wf_contract_employee.findById", query = "SELECT r FROM ContractEmployeeDO r where r.contractLabourId =:id"),
    /*@NamedQuery(name = "hrms_contractLabours.findByStatus", query = "SELECT r FROM AddressTypeDO r where r.status =:status"),
    @NamedQuery(name = "hrms_contractLabours.findByName", query = "SELECT r FROM AddressTypeDO r where r.name=:name"),
    @NamedQuery(name = "hrms_contractLabours.findByNameAndId", query = "SELECT r FROM AddressTypeDO r where r.name=:name and r.addressTypeId NOT IN :id"),*/
})
public class ContractEmployeeDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static final String FIND_BY_ID = "wf_contract_employee.findById";
	
	/*public static final String FIND_BY_STATUS = "hrms_contractLabours.findByStatus";
	
	public static final String FIND_BY_NAME = "hrms_contractLabours.findByName";
	
	public static final String FIND_FOR_UPDATE = "hrms_contractLabours.findByNameAndId";*/
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long contractLabourId;
	
	@ManyToOne
	@JoinColumn(name="supplierId")
	private SuppliersDO supplier;
	
	/*@ManyToOne
	@JoinColumn(name="empId")
	private EmployeeDO employee;*/
	
	 private String empId;
	
	private String name;
	
	@ManyToOne
	@JoinColumn(name="departmentId")
	private DepartmentDO department;
	
	@ManyToOne
	@JoinColumn(name="designationId")
	private DesignationDO designation;
	
	 @Temporal(TemporalType.TIMESTAMP)
	 private Date doj;
	 
	 @Temporal(TemporalType.TIMESTAMP)
	 private Date dob;
	 
	 private String fatherName;
	 private String spouseName;
	 private String gender;
	 private String maritalStatus;
	 private String cat;
	 private String qualification;
	 private String bloodGroup;
	 
	 private Long wages;
	 private Long contactNo;
	 private Long emergencyContactNo;
	 private Long previousExperience;
	 private Long bankAcNo;
	 private String bankName;
	 private String ifsc;
	 private String panNo;
	 private Long aadharNo;
	 private String presentAddress;
	 private String permanentAddress;
	 @Column(columnDefinition="LONGTEXT")
	private String employeePhoto;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
	public Long getContractLabourId() {
		return contractLabourId;
	}
	public void setContractLabourId(Long contractLabourId) {
		this.contractLabourId = contractLabourId;
	}
	public SuppliersDO getSupplier() {
		return supplier;
	}
	public void setSupplier(SuppliersDO supplier) {
		this.supplier = supplier;
	}
	public String getEmpId() {
		return empId;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public DepartmentDO getDepartment() {
		return department;
	}
	public void setDepartment(DepartmentDO department) {
		this.department = department;
	}
	public DesignationDO getDesignation() {
		return designation;
	}
	public void setDesignation(DesignationDO designation) {
		this.designation = designation;
	}
	public Date getDoj() {
		return doj;
	}
	public void setDoj(Date doj) {
		this.doj = doj;
	}
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	public String getFatherName() {
		return fatherName;
	}
	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}
	public String getSpouseName() {
		return spouseName;
	}
	public void setSpouseName(String spouseName) {
		this.spouseName = spouseName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getMaritalStatus() {
		return maritalStatus;
	}
	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}
	public String getCat() {
		return cat;
	}
	public void setCat(String cat) {
		this.cat = cat;
	}
	public String getQualification() {
		return qualification;
	}
	public void setQualification(String qualification) {
		this.qualification = qualification;
	}
	public String getBloodGroup() {
		return bloodGroup;
	}
	public void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}
	public Long getWages() {
		return wages;
	}
	public void setWages(Long wages) {
		this.wages = wages;
	}
	public Long getContactNo() {
		return contactNo;
	}
	public void setContactNo(Long contactNo) {
		this.contactNo = contactNo;
	}
	public Long getEmergencyContactNo() {
		return emergencyContactNo;
	}
	public void setEmergencyContactNo(Long emergencyContactNo) {
		this.emergencyContactNo = emergencyContactNo;
	}
	public Long getPreviousExperience() {
		return previousExperience;
	}
	public void setPreviousExperience(Long previousExperience) {
		this.previousExperience = previousExperience;
	}
	public Long getBankAcNo() {
		return bankAcNo;
	}
	public void setBankAcNo(Long bankAcNo) {
		this.bankAcNo = bankAcNo;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getIfsc() {
		return ifsc;
	}
	public void setIfsc(String ifsc) {
		this.ifsc = ifsc;
	}
	public String getPanNo() {
		return panNo;
	}
	public void setPanNo(String panNo) {
		this.panNo = panNo;
	}
	public Long getAadharNo() {
		return aadharNo;
	}
	public void setAadharNo(Long aadharNo) {
		this.aadharNo = aadharNo;
	}
	public String getPresentAddress() {
		return presentAddress;
	}
	public void setPresentAddress(String presentAddress) {
		this.presentAddress = presentAddress;
	}
	public String getPermanentAddress() {
		return permanentAddress;
	}
	public void setPermanentAddress(String permanentAddress) {
		this.permanentAddress = permanentAddress;
	}
	public String getEmployeePhoto() {
		return employeePhoto;
	}
	public void setEmployeePhoto(String employeePhoto) {
		this.employeePhoto = employeePhoto;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
    
}