package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_holidays")
@TableGenerator(name ="hrms_holidays", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "hrms_holidays.findById", query = "SELECT r FROM HolidaysDO r where r.holidayId =:id"),
    @NamedQuery(name = "hrms_holidays.findByHoliday", query = "SELECT r FROM HolidaysDO r where r.year =:year and r.leavedate =:date"),
    @NamedQuery(name = "hrms_holidays.findByHolidayUpdate", query = "SELECT r FROM HolidaysDO r where r.year =:year and r.leavedate =:date and r.holidayId NOT IN :id"),
    @NamedQuery(name = "hrms_holidays.findByDate", query = "SELECT r FROM HolidaysDO r where r.year =:year and r.status =:status and r.leavedate LIKE CONCAT(:date,'%')"),
    @NamedQuery(name = "hrms_holidays.findByDateWorkLocation", query = "SELECT r FROM HolidaysDO r where r.year =:year and r.worklocation.worklocationId =:id and r.status =:status and r.leavedate LIKE CONCAT(:date,'%')"),
    
})
public class HolidaysDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static final String FIND_BY_ID = "hrms_holidays.findById";
	
	public static final String FIND_BY_HOLIDAY = "hrms_holidays.findByHoliday";
	
	public static final String FIND_BY_HOLIDAY_UPDATE = "hrms_holidays.findByHolidayUpdate";
	
	public static final String FIND_BY_DATE = "hrms_holidays.findByDate";
	
	public static final String FIND_BY_DATE_WORKLOCATION = "hrms_holidays.findByDateWorkLocation";
	
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO) 
	@Column(nullable = false, unique = true)
	private Long holidayId;
	
	@Column(columnDefinition="LONGTEXT")
	private String leavedate;
	private String year;
	private String description;
	private String comment;
	private char status;
	
	@ManyToOne
	@JoinColumn(name="worklocationId")
	private WorkLocationDO worklocation;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getHolidayId() {
		return holidayId;
	}
	public void setHolidayId(Long holidayId) {
		this.holidayId = holidayId;
	}
	public String getLeavedate() {
		return leavedate;
	}
	public void setLeavedate(String leavedate) {
		this.leavedate = leavedate;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public WorkLocationDO getWorklocation() {
		return worklocation;
	}
	public void setWorklocation(WorkLocationDO worklocation) {
		this.worklocation = worklocation;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}