package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="master_designation")
@TableGenerator(name ="master_designation", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "master_designation.findById", query = "SELECT r FROM DesignationDO r where r.designationId =:id"),
    @NamedQuery(name = "master_designation.findByStatus", query = "SELECT r FROM DesignationDO r where r.status =:status"),
    @NamedQuery(name = "master_designation.findByName", query = "SELECT r FROM DesignationDO r where r.name=:name"),
    @NamedQuery(name = "master_designation.findByNameAndId", query = "SELECT r FROM DesignationDO r where r.name=:name and r.designationId NOT IN :id"),
})
public class DesignationDO implements Serializable {
	private static final long serialVersionUID = 1L;

	
	public static final String FIND_BY_ID = "master_designation.findById";
	
	public static final String FIND_BY_STATUS = "master_designation.findByStatus";
	
	public static final String FIND_BY_NAME = "master_designation.findByName";
	
	public static final String FIND_FOR_UPDATE = "master_designation.findByNameAndId";
	
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true) 
	private Long designationId;
	
	private String name;
	private char status;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
	private String updatedby;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	public Long getDesignationId() {
		return designationId;
	}
	public void setDesignationId(Long designationId) {
		this.designationId = designationId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}