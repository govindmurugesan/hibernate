package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="roles")
@TableGenerator(name ="roles", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "roles.findAll", query = "SELECT r FROM RolesDO r"),
    @NamedQuery(name = "roles.findById", query = "SELECT r FROM RolesDO r where r.roleId =:id"),
    @NamedQuery(name = "roles.findByName", query = "SELECT r FROM RolesDO r where r.name =:name"),
    @NamedQuery(name = "roles.findForUpdate", query = "SELECT r FROM RolesDO r where r.name=:name and r.roleId NOT IN :id"),
})
public class RolesDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "roles.findAll";
	
	public static final String FIND_BY_ID = "roles.findById";
	
	public static final String FIND_BY_NAME = "roles.findByName";
	
	public static final String FIND_FOR_UPDATE = "roles.findForUpdate";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long roleId;
	
	private String name;
	private String description;
	private char status;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getRoleId() {
		return roleId;
	}
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}