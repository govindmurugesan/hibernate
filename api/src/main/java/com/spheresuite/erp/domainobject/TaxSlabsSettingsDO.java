package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_master_taxslabssetting")
@TableGenerator(name ="hrms_master_taxslabssetting", initialValue =100101, allocationSize =1)
@NamedQueries({
	@NamedQuery(name = "hrms_master_taxslabssetting.findAll", query = "SELECT r FROM TaxSlabsSettingsDO r"),
    @NamedQuery(name = "hrms_master_taxslabssetting.findById", query = "SELECT r FROM TaxSlabsSettingsDO r where r.taxSlabId =:id"),
    @NamedQuery(name = "hrms_master_taxslabssetting.findActive", query = "SELECT r FROM TaxSlabsSettingsDO r where r.status =:status"),
    @NamedQuery(name = "hrms_master_taxslabssetting.findForUpdate", query = "SELECT r FROM TaxSlabsSettingsDO r where r.empAgeFrom =:fromAge and r.empAgeTo =:toAge and (r.startdate <=:fromDate and r.enddate >=:toDate) and r.taxSlabId  <>:id"),
    @NamedQuery(name = "hrms_master_taxslabssetting.findForAdd", query = "SELECT r FROM TaxSlabsSettingsDO r where r.empAgeFrom =:fromAge and r.empAgeTo =:toAge and (r.startdate <=:fromDate and r.enddate >=:toDate)"),
    @NamedQuery(name = "hrms_master_taxslabssetting.findByAgeAndDate", query = "SELECT r FROM TaxSlabsSettingsDO r where r.status =:status and (r.empAgeFrom <=:age and r.empAgeTo >=:age) and ((r.startdate <=:date and r.enddate >=:date) or (r.startdate <=:date and r.enddate IS NULL)) order by amountfrom + 0"),
    //  @NamedQuery(name = "hrms_taxslabssetting.findByTaxPayee", query = "SELECT r FROM TaxSlabsSettingsDO r where r.status =:status and r.startdate =:startDate and r.enddate =:enddate"),
})
public class TaxSlabsSettingsDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static final String FIND_ALL= "hrms_master_taxslabssetting.findAll";
	
	public static final String FIND_BY_ID = "hrms_master_taxslabssetting.findById";
	
	public static final String FIND_ACTIVE = "hrms_master_taxslabssetting.findActive";
	
	public static final String FIND_FOR_UPDATE = "hrms_master_taxslabssetting.findForUpdate";
	
	public static final String FIND_FOR_ADD = "hrms_master_taxslabssetting.findForAdd";
	
	public static final String FIND_BY_AGE_DATE = "hrms_master_taxslabssetting.findByAgeAndDate";
	//public static final String FIND_BY_TAXPAYEEID = "hrms_taxslabssetting.findByTaxPayee";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO) 
	private Long taxSlabId;
	
	private Long amountfrom;
	private Long amountto;
	private Double percentageamount;
	
	private Long empAgeFrom;
	private Long empAgeTo;
	
	 @Temporal(TemporalType.DATE)
	private Date startdate;
	 @Temporal(TemporalType.DATE)
	private Date enddate;
	
	private char status;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getTaxSlabId() {
		return taxSlabId;
	}
	public void setTaxSlabId(Long taxSlabId) {
		this.taxSlabId = taxSlabId;
	}
	public Double getPercentageamount() {
		return percentageamount;
	}
	public void setPercentageamount(Double percentageamount) {
		this.percentageamount = percentageamount;
	}
	public Long getAmountfrom() {
		return amountfrom;
	}
	public void setAmountfrom(Long amountfrom) {
		this.amountfrom = amountfrom;
	}
	public Long getAmountto() {
		return amountto;
	}
	public void setAmountto(Long amountto) {
		this.amountto = amountto;
	}
	public Long getEmpAgeFrom() {
		return empAgeFrom;
	}
	public void setEmpAgeFrom(Long empAgeFrom) {
		this.empAgeFrom = empAgeFrom;
	}
	public Long getEmpAgeTo() {
		return empAgeTo;
	}
	public void setEmpAgeTo(Long empAgeTo) {
		this.empAgeTo = empAgeTo;
	}
	public Date getStartdate() {
		return startdate;
	}
	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}
	public Date getEnddate() {
		return enddate;
	}
	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}