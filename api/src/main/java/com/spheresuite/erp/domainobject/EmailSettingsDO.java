package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="master_emailsettings")
@TableGenerator(name ="master_emailsettings", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "master_emailsettings.findById", query = "SELECT r FROM EmailSettingsDO r where r.emailSettingId =:id"),
    @NamedQuery(name = "master_emailsettings.findByStatus", query = "SELECT r FROM EmailSettingsDO r where r.status =:status"),
    @NamedQuery(name = "master_emailsettings.findByMailType", query = "SELECT r FROM EmailSettingsDO r where r.mailtype =:mailType and r.status =:status"),
    @NamedQuery(name = "master_emailsettings.findByName", query = "SELECT r FROM EmailSettingsDO r where r.mailtype=:mailType"),
    @NamedQuery(name = "master_emailsettings.findByNameAndId", query = "SELECT r FROM EmailSettingsDO r where r.mailtype=:mailType and r.emailSettingId NOT IN :id"),
})
public class EmailSettingsDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "master_emailsettings.findAll";
	
	public static final String FIND_BY_ID = "master_emailsettings.findById";
	
	public static final String FIND_BY_STATUS = "master_emailsettings.findByStatus";
	
	public static final String FIND_BY_MAILTYPE = "master_emailsettings.findByMailType";
	
	public static final String FIND_BY_NAME = "master_emailsettings.findByName";
	
	public static final String FIND_FOR_UPDATE = "master_emailsettings.findByNameAndId";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long emailSettingId;
	
	private String mailtype;
	private String imaphost;
	private String imapport;
	private String smtphost;
	private String smtpport;
	private char status;
	private String sendemail;
	private String sendpassword;
	private Integer authType;
	private Integer tlsEnable;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getEmailSettingId() {
		return emailSettingId;
	}
	public void setEmailSettingId(Long emailSettingId) {
		this.emailSettingId = emailSettingId;
	}
	public String getMailtype() {
		return mailtype;
	}
	public void setMailtype(String mailtype) {
		this.mailtype = mailtype;
	}
	public String getImaphost() {
		return imaphost;
	}
	public void setImaphost(String imaphost) {
		this.imaphost = imaphost;
	}
	public String getImapport() {
		return imapport;
	}
	public void setImapport(String imapport) {
		this.imapport = imapport;
	}
	public String getSmtphost() {
		return smtphost;
	}
	public void setSmtphost(String smtphost) {
		this.smtphost = smtphost;
	}
	public String getSmtpport() {
		return smtpport;
	}
	public void setSmtpport(String smtpport) {
		this.smtpport = smtpport;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getSendemail() {
		return sendemail;
	}
	public void setSendemail(String sendemail) {
		this.sendemail = sendemail;
	}
	public String getSendpassword() {
		return sendpassword;
	}
	public void setSendpassword(String sendpassword) {
		this.sendpassword = sendpassword;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public Integer getAuthType() {
		return authType;
	}
	public void setAuthType(Integer authType) {
		this.authType = authType;
	}
	public Integer getTlsEnable() {
		return tlsEnable;
	}
	public void setTlsEnable(Integer tlsEnable) {
		this.tlsEnable = tlsEnable;
	}
}