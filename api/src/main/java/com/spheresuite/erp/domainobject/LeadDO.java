package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="crm_lead")
@TableGenerator(name ="crm_lead", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "crm_lead.findAll", query = "SELECT r FROM LeadDO r where r.type=:type and r.empId IN :id and r.isDeleted =:status"),
    @NamedQuery(name = "crm_lead.findAllDate", query = "SELECT r FROM LeadDO r where r.type=:type  and r.isDeleted =:status"),
    @NamedQuery(name = "crm_lead.findSum", query = "SELECT count(r) FROM LeadDO r where r.type=:type  and r.isDeleted =:status"),
    @NamedQuery(name = "crm_lead.find", query = "SELECT r FROM LeadDO r where r.empId IN :id and r.isDeleted =:status"),
    @NamedQuery(name = "crm_lead.findById", query = "SELECT r FROM LeadDO r where r.leadId =:id and r.isDeleted =:status"),
    @NamedQuery(name = "crm_lead.findByDate", query = "SELECT r FROM LeadDO r where r.createdon BETWEEN :fromDate AND :toDate AND r.type =:type and r.isDeleted =:status"),
    @NamedQuery(name = "crm_lead.findCustomerByDate", query = "SELECT r FROM LeadDO r where r.convertedDate BETWEEN :fromDate AND :toDate AND r.type =:type and r.isDeleted =:status"),
    @NamedQuery(name = "crm_lead.findByDateByEmp", query = "SELECT r FROM LeadDO r where r.createdon BETWEEN :fromDate AND :toDate AND r.type =:type and r.isDeleted =:status and r.empId =:empId"),
    @NamedQuery(name = "crm_lead.findByDateByEmpForCus", query = "SELECT r FROM LeadDO r where r.convertedDate BETWEEN :fromDate AND :toDate AND r.type =:type and r.isDeleted =:status and r.empId =:empId"),
    @NamedQuery(name = "crm_lead.findByByEmp", query = "SELECT r FROM LeadDO r where r.type =:type AND r.empId =:empId and r.isDeleted =:status"),
    @NamedQuery(name = "crm_lead.findByLeadName", query = "SELECT r FROM LeadDO r where r.name =:name and r.isDeleted =:status"),
    @NamedQuery(name = "crm_lead.findByByPermanentEmp", query = "SELECT r FROM LeadDO r where r.type =:type AND r.empId =:empId and r.isDeleted =:status and (r.transferType <>:transferType or r.transferType IS NULL)"),
    @NamedQuery(name = "crm_lead.findByTransferType", query = "SELECT r FROM LeadDO r where r.type =:type AND r.transferType =:transferType and r.isDeleted =:status order by r.leadId desc"),
    @NamedQuery(name = "crm_lead.findByAllTransferType", query = "SELECT r FROM LeadDO r where r.type =:type AND r.transferType is not null and r.isDeleted =:status order by r.leadId desc"),
    @NamedQuery(name = "crm_lead.findByTransferTypeById", query = "SELECT r FROM LeadDO r where r.type =:type AND r.transferType =:transferType and r.isDeleted =:status and r.transferfrom =:empId"),
    @NamedQuery(name = "crm_lead.findAllLead", query = "SELECT r FROM LeadDO r  where r.isDeleted =:status"),
    @NamedQuery(name = "crm_lead.findAllCustId", query = "SELECT r.leadId FROM LeadDO r where r.type=:type  and r.isDeleted =:status"),
    @NamedQuery(name = "crm_lead.findByCurrentFollowupLeads", query = "SELECT r FROM LeadDO r where DATE(r.followupdate) <= CURRENT_DATE and r.isDeleted =:status and r.empId =:empId"),
    @NamedQuery(name = "crm_lead.findByDateFollowupLeads", query = "SELECT r FROM LeadDO r where DATE(r.followupdate) =:date and r.isDeleted =:status and r.empId =:empId"),
    @NamedQuery(name = "crm_lead.findAllLatestLead", query = "SELECT r FROM LeadDO r where r.type =:type AND r.empId =:empId and r.isDeleted =:status ORDER BY r.updatedon DESC"),
    
})
public class LeadDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "crm_lead.findAll";
	
	public static final String FIND_ALL_LATEST_LEAD = "crm_lead.findAllLatestLead";
	
	public static final String FIND_ALL_LEAD = "crm_lead.findAllLead";
	
	public static final String FIND_ALL_DATE = "crm_lead.findAllDate";
	
	public static final String FIND_ALL_CUST_ID = "crm_lead.findAllCustId";
	
	public static final String FIND_SUM_CUSTOMER="crm_lead.findSum";
	
	public static final String FIND = "crm_lead.find";
	
	public static final String FIND_BY_ID = "crm_lead.findById";

	public static final String FIND_BY_DATE = "crm_lead.findByDate";
	
	public static final String FIND_BY_DATE_BY_EMP = "crm_lead.findByDateByEmp";
	
	public static final String FIND_BY_DATE_BY_EMP_FOR_CUS = "crm_lead.findByDateByEmpForCus";
	
	public static final String FIND_CUSTOMER_BY_DATE = "crm_lead.findCustomerByDate";
	
	public static final String FIND_BY_EMP = "crm_lead.findByByEmp";
	
	public static final String FIND_BY_LEAD_NAME = "crm_lead.findByLeadName";
	
	public static final String FIND_BY_PERMANANT_EMP = "crm_lead.findByByPermanentEmp";
	
	public static final String FIND_BY_TRANSFERTYPE = "crm_lead.findByTransferType";
	
	public static final String FIND_BY_ALL_TRANSFERTYPE = "crm_lead.findByAllTransferType";
	
	public static final String FIND_BY_TRANSFERTYPE_BY_ID = "crm_lead.findByTransferTypeById";
	
	public static final String FIND_BY_CURRENT_FOLLOWUP_LEADS = "crm_lead.findByCurrentFollowupLeads";
	
	public static final String FIND_BY_DATE_FOLLOWUP_LEADS = "crm_lead.findByDateFollowupLeads";
	
	
	@Id
	@Column(nullable = false, unique = true)
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long leadId;
	
	private String name;
	
	@ManyToOne
	@JoinColumn(name="leadStatusId")
	private LeadStatusDO status;
	//private Long status;
	private String source;
	
	@ManyToOne
	@JoinColumn(name="industryId")
	private IndustryDO industry;
	//private Long industryID;
	
	private String mobile;
	private String phone;
	private String email;
	private String address;
	private String comment;
	private String updatedby;
	
	@ManyToOne
	@JoinColumn(name="leadTypeId")
	private LeadTypeDO leadType;
	//private Long leadType;
	
	private String type;
	private String convertCustomerComments;
	@Temporal(TemporalType.TIMESTAMP)
	private Date convertedDate;
	private String createdby;
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdon;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private String empId;
	private String transferfrom;
	@Temporal(TemporalType.DATE)
	private Date transferFromDate;
	@Temporal(TemporalType.DATE)
	private Date transferToDate;
	private String transferType;
	@Column(columnDefinition="LONGTEXT")
	private String photo;
	private String isDeleted;
	private String city;
	
	@ManyToOne
	@JoinColumn(name="stateId")
	private StateDO state;
	//private Long state;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date followupdate;
	@Temporal(TemporalType.DATE)
	private Date leaddate;
	
	public Long getLeadId() {
		return leadId;
	}
	public void setId(Long leadId) {
		this.leadId = leadId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
		
	public String getEmpId() {
		return empId;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getConvertCustomerComments() {
		return convertCustomerComments;
	}
	public void setConvertCustomerComments(String convertCustomerComments) {
		this.convertCustomerComments = convertCustomerComments;
	}
	public Date getConvertedDate() {
		return convertedDate;
	}
	public void setConvertedDate(Date convertedDate) {
		this.convertedDate = convertedDate;
	}
	
	public String getTransferfrom() {
		return transferfrom;
	}
	public void setTransferfrom(String transferFrom2) {
		this.transferfrom = transferFrom2;
	}
	public Date getTransferFromDate() {
		return transferFromDate;
	}
	public void setTransferFromDate(Date transferFromDate) {
		this.transferFromDate = transferFromDate;
	}
	public Date getTransferToDate() {
		return transferToDate;
	}
	public void setTransferToDate(Date transferToDate) {
		this.transferToDate = transferToDate;
	}
	public String getTransferType() {
		return transferType;
	}
	public void setTransferType(String transferType) {
		this.transferType = transferType;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public String getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	
	public Date getFollowupdate() {
		return followupdate;
	}
	public void setFollowupdate(Date followupdate) {
		this.followupdate = followupdate;
	}
	public Date getLeaddate() {
		return leaddate;
	}
	public void setLeaddate(Date leaddate) {
		this.leaddate = leaddate;
	}
	public LeadStatusDO getStatus() {
		return status;
	}
	public void setStatus(LeadStatusDO status) {
		this.status = status;
	}
	public IndustryDO getIndustry() {
		return industry;
	}
	public void setIndustry(IndustryDO industry) {
		this.industry = industry;
	}
	public LeadTypeDO getLeadType() {
		return leadType;
	}
	public void setLeadType(LeadTypeDO leadType) {
		this.leadType = leadType;
	}
	public StateDO getState() {
		return state;
	}
	public void setState(StateDO state) {
		this.state = state;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
}