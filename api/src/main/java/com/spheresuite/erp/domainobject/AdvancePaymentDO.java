package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_advancePayment")
@TableGenerator(name ="hrms_advancePayment", initialValue =100101, allocationSize =1)
@NamedQueries({
	@NamedQuery(name = "hrms_advancePayment.findAll", query = "SELECT r FROM AdvancePaymentDO r ORDER BY r.advancePaymentId DESC"),
    @NamedQuery(name = "hrms_advancePayment.findById", query = "SELECT r FROM AdvancePaymentDO r where r.advancePaymentId =:id"),
    @NamedQuery(name = "hrms_advancePayment.findActiveByEmpId", query = "SELECT r FROM AdvancePaymentDO r where r.employee.empId =:empId and r.status =:status"),
})
public class AdvancePaymentDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_BY_ID = "hrms_advancePayment.findById";
	
	public static final String FIND_ALL = "hrms_advancePayment.findAll";
	
	public static final String FIND_BY_EMPID = "hrms_advancePayment.findByEmpId";
	
	public static final String FIND_ACTIVE_BY_EMPID = "hrms_advancePayment.findActiveByEmpId";
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)  
	private Long advancePaymentId;
	private Long amount;
	private String month;
	private String noOfInstallments;
	private String installmentAmount;
	private String status;
	
	@ManyToOne
	@JoinColumn(name="empId")
	private EmployeeDO employee;
	
	@ManyToOne
	@JoinColumn(name="advanceTypeId")
	private AdvanceTypeDO advanceType;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
	public Long getAdvancePaymentId() {
		return advancePaymentId;
	}
	public void setAdvancePaymentId(Long advancePaymentId) {
		this.advancePaymentId = advancePaymentId;
	}
	public Long getAmount() {
		return amount;
	}
	public void setAmount(Long amount) {
		this.amount = amount;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public String getNoOfInstallments() {
		return noOfInstallments;
	}
	public void setNoOfInstallments(String noOfInstallments) {
		this.noOfInstallments = noOfInstallments;
	}
	public String getInstallmentAmount() {
		return installmentAmount;
	}
	public void setInstallmentAmount(String installmentAmount) {
		this.installmentAmount = installmentAmount;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public EmployeeDO getEmployee() {
		return employee;
	}
	public void setEmployee(EmployeeDO employee) {
		this.employee = employee;
	}
	public AdvanceTypeDO getAdvanceType() {
		return advanceType;
	}
	public void setAdvanceType(AdvanceTypeDO advanceType) {
		this.advanceType = advanceType;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}