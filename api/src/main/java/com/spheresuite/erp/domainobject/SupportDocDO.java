package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="supportdoc")
@TableGenerator(name ="supportdoc", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "supportdoc.findAll", query = "SELECT r FROM SupportDocDO r"),
    @NamedQuery(name = "supportdoc.findByOppId", query = "SELECT r FROM SupportDocDO r where r.support.supportId =:id"),
})
public class SupportDocDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "supportdoc.findAll";
	
	public static final String FIND_BY_SUPPORT_ID = "supportdoc.findBySupportId";
	
	@Id
	@Column(nullable = false, unique = true)
	@GeneratedValue(strategy=GenerationType.AUTO) 
	private Long supportDocId;
	
	@ManyToOne
	@JoinColumn(name="supportIdId")
	private SupportDO support;
	//private Long opportunity;
	@Column(columnDefinition="LONGTEXT")
	private String photo;
	private String fileName;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private String updatedBy;
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdon;
	private String createdBy;
	private String empId;
	
	
	public Long getSupportDocId() {
		return supportDocId;
	}
	public void setSupportDocId(Long supportDocId) {
		this.supportDocId = supportDocId;
	}
	public SupportDO getSupport() {
		return support;
	}
	public void setSupport(SupportDO support) {
		this.support = support;
	}
	public String getEmpId() {
		return empId;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
}