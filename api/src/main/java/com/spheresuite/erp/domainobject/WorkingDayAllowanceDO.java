package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_workingDayAllowance")
@TableGenerator(name ="hrms_workingDayAllowance", initialValue =100101, allocationSize =1)
@NamedQueries({
	 @NamedQuery(name = "hrms_workingDayAllowance.findAll", query = "SELECT r FROM WorkingDayAllowanceDO r"),
	 @NamedQuery(name = "hrms_workingDayAllowance.findById", query = "SELECT r FROM WorkingDayAllowanceDO r where r.workingDayAllowanceId =:id"),
	 @NamedQuery(name = "hrms_workingDayAllowance.findByStatus", query = "SELECT r FROM WorkingDayAllowanceDO r where r.status =:status"),
  /*  @NamedQuery(name = "hrms_workingDayAllowance.findById", query = "SELECT r FROM AddressTypeDO r where r.addressTypeId =:id"),
    */
  /*  @NamedQuery(name = "hrms_workingDayAllowance.findByName", query = "SELECT r FROM AddressTypeDO r where r.name=:name"),
    @NamedQuery(name = "hrms_workingDayAllowance.findByNameAndId", query = "SELECT r FROM AddressTypeDO r where r.name=:name and r.addressTypeId NOT IN :id"),*/
})
public class WorkingDayAllowanceDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static final String FIND_ALL = "hrms_workingDayAllowance.findAll";
	
	public static final String FIND_BY_ID = "hrms_workingDayAllowance.findById";
	
	public static final String FIND_BY_STATUS = "hrms_workingDayAllowance.findByStatus";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long workingDayAllowanceId;
	private String name;
	private char status;
	private Double cost;
	/*private String type;*/
	/*private String costEffective;
	private String calculationType;*/
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getWorkingDayAllowanceId() {
		return workingDayAllowanceId;
	}
	public void setWorkingDayAllowanceId(Long workingDayAllowanceId) {
		this.workingDayAllowanceId = workingDayAllowanceId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getCost() {
		return cost;
	}
	public void setCost(Double cost) {
		this.cost = cost;
	}
	/*public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}*/
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	/*public String getCostEffective() {
		return costEffective;
	}
	public void setCostEffective(String costEffective) {
		this.costEffective = costEffective;
	}
	public String getCalculationType() {
		return calculationType;
	}
	public void setCalculationType(String calculationType) {
		this.calculationType = calculationType;
	}*/
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}