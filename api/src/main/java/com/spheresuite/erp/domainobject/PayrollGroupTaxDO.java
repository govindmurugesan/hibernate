package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_payrollgrouptax")
@TableGenerator(name ="hrms_payrollgrouptax", initialValue =100101, allocationSize =1)
@NamedQueries({
     @NamedQuery(name = "hrms_payrollgrouptax.findById", query = "SELECT r FROM PayrollGroupTaxDO r where r.payrollGroupTaxId =:id"),
   // @NamedQuery(name = "hrms_payrollgrouptax.findByStatus", query = "SELECT r FROM PayrollGroupDO r where r.status =:status"),
   // @NamedQuery(name = "hrms_payrollgrouptax.findByName", query = "SELECT r FROM PayrollGroupDO r where r.name=:name"),
   // @NamedQuery(name = "hrms_payrollgrouptax.findByNameAndId", query = "SELECT r FROM PayrollGroupDO r where r.name=:name and r.payrollGroupId NOT IN :id"),
	@NamedQuery(name = "hrms_payrollgrouptax.findForAdd", query = "SELECT r FROM PayrollGroupTaxDO r where r.taxSettings.taxSettingId=:taxId and r.status =:status and r.payrollGroup.payrollGroupId=:payrollGroupId"),
	  @NamedQuery(name = "hrms_payrollgrouptax.findByGroupId", query = "SELECT r FROM PayrollGroupTaxDO r where r.payrollGroup.payrollGroupId =:id and r.status =:status"),
     @NamedQuery(name = "hrms_payrollgrouptax.findByTds", query = "SELECT r FROM PayrollGroupTaxDO r where r.tds =:apply and r.payrollGroup.payrollGroupId =:id"),
     @NamedQuery(name = "hrms_payrollgrouptax.findByPt", query = "SELECT r FROM PayrollGroupTaxDO r where r.pt =:apply and r.payrollGroup.payrollGroupId =:id"),
     @NamedQuery(name = "hrms_payrollgrouptax.findByEducationCess", query = "SELECT r FROM PayrollGroupTaxDO r where r.educationCess =:apply and r.payrollGroup.payrollGroupId =:id"),
})
public class PayrollGroupTaxDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_BY_ID = "hrms_payrollgrouptax.findById";
	
	//public static final String FIND_BY_STATUS = "hrms_payrollgrouptax.findByStatus";
	
	//public static final String FIND_BY_NAME = "hrms_payrollgrouptax.findByName";
	
	//public static final String FIND_FOR_UPDATE = "hrms_payrollgrouptax.findByNameAndId";
	public static final String FIND_FOR_ADD = "hrms_payrollgrouptax.findForAdd";
	
	public static final String FIND_BY_GROUP_ID = "hrms_payrollgrouptax.findByGroupId";
	
	public static final String FIND_BY_TDS = "hrms_payrollgrouptax.findByTds";
	
	public static final String FIND_BY_PT = "hrms_payrollgrouptax.findByPt";
	
	public static final String FIND_BY_EDUCATIONCESS = "hrms_payrollgrouptax.findByEducationCess";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long payrollGroupTaxId;
	
	private char status;
	
	private String tds;
	
	private String pt;
	
	private String educationCess;
	
	@ManyToOne
	@JoinColumn(name="payrollGroupId")
	private PayrollGroupDO payrollGroup;
	
	@ManyToOne
	@JoinColumn(name="taxSettingId")
	private TaxSettingsDO taxSettings;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
    
	public Long getPayrollGroupTaxId() {
		return payrollGroupTaxId;
	}
	public void setPayrollGroupTaxId(Long payrollGroupTaxId) {
		this.payrollGroupTaxId = payrollGroupTaxId;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public PayrollGroupDO getPayrollGroup() {
		return payrollGroup;
	}
	public void setPayrollGroup(PayrollGroupDO payrollGroup) {
		this.payrollGroup = payrollGroup;
	}
	public TaxSettingsDO getTaxSettings() {
		return taxSettings;
	}
	public void setTaxSettings(TaxSettingsDO taxSettings) {
		this.taxSettings = taxSettings;
	}
	public String getTds() {
		return tds;
	}
	public void setTds(String tds) {
		this.tds = tds;
	}
	public String getPt() {
		return pt;
	}
	public void setPt(String pt) {
		this.pt = pt;
	}
	public String getEducationCess() {
		return educationCess;
	}
	public void setEducationCess(String educationCess) {
		this.educationCess = educationCess;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}