package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="cms_frequency")
@TableGenerator(name ="cms_frequency", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "cms_frequency.findAll", query = "SELECT r FROM CMSFrequencyDO r"),
    @NamedQuery(name = "cms_frequency.findById", query = "SELECT r FROM CMSFrequencyDO r where r.id =:id"),
    @NamedQuery(name = "cms_frequency.findByStatus", query = "SELECT r FROM CMSFrequencyDO r where r.status =:status"),
    @NamedQuery(name = "cms_frequency.findByName", query = "SELECT r FROM CMSFrequencyDO r where r.status =:status and r.name=:name"),
    @NamedQuery(name = "cms_frequency.findByNameAndId", query = "SELECT r FROM CMSFrequencyDO r where r.status =:status and r.name=:name and r.id NOT IN :id"),
    @NamedQuery(name = "cms_frequency.findByNameAll", query = "SELECT r FROM CMSFrequencyDO r where r.name=:name"),
})
public class CMSFrequencyDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "cms_frequency.findAll";
	public static final String FIND_BY_ID = "cms_frequency.findById";
	public static final String FIND_BY_STATUS = "cms_frequency.findByStatus";
	public static final String FIND_BY_NAME = "cms_frequency.findByName";
	public static final String FIND_FOR_UPDATE = "cms_frequency.findByNameAndId";
	public static final String FIND_BY_NAME_ALL = "cms_frequency.findByNameAll";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO) 
	private Long id;
	private String name;
	private String comments;
	private char status;
	
	private String updatedby;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public char getStatus() {
		return status;
	}

	public void setStatus(char status) {
		this.status = status;
	}

	public String getUpdatedby() {
		return updatedby;
	}

	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}

	public Date getUpdatedon() {
		return updatedon;
	}

	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	
}