package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_master_payrollbatch")
@TableGenerator(name ="hrms_master_payrollbatch", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "hrms_master_payrollbatch.findById", query = "SELECT r FROM PayrollBatchDO r where r.payrollBatchId =:id"),
    @NamedQuery(name = "hrms_master_payrollbatch.findByUnitId", query = "SELECT r FROM PayrollBatchDO r where r.unitOrBranch.unitOrBranchId =:id and r.status =:status"),
    @NamedQuery(name = "hrms_master_payrollbatch.findByStatus", query = "SELECT r FROM PayrollBatchDO r where r.status =:status"),
    @NamedQuery(name = "hrms_master_payrollbatch.findByName", query = "SELECT r FROM PayrollBatchDO r where r.name=:name"),
    @NamedQuery(name = "hrms_master_payrollbatch.findByNameAndId", query = "SELECT r FROM PayrollBatchDO r where r.name=:name and r.payrollBatchId NOT IN :id"),
    @NamedQuery(name = "hrms_master_payrollbatch.findByPayrollType", query = "SELECT r FROM PayrollBatchDO r where r.payrollType=:type"),
})
public class PayrollBatchDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static final String FIND_BY_ID = "hrms_master_payrollbatch.findById";
	
	public static final String FIND_BY_UNIT_ID = "hrms_master_payrollbatch.findByUnitId";
	
	public static final String FIND_BY_STATUS = "hrms_master_payrollbatch.findByStatus";
	
	public static final String FIND_BY_NAME = "hrms_master_payrollbatch.findByName";
	
	public static final String FIND_FOR_UPDATE = "hrms_master_payrollbatch.findByNameAndId";
	
	public static final String FIND_BY_PAROLLTYPE = "hrms_master_payrollbatch.findByPayrollType";
	
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long payrollBatchId;
	
	private String name;
	private String displayname;
	private char status;
	private String payrollType;
	
	private String otCheck;
	private String minHrOT;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
    @ManyToOne
	@JoinColumn(name="unitOrBranchId")
	private UnitOrBranchDO unitOrBranch;
    
	public Long getPayrollBatchId() {
		return payrollBatchId;
	}
	public void setPayrollBatchId(Long payrollBatchId) {
		this.payrollBatchId = payrollBatchId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDisplayname() {
		return displayname;
	}
	public void setDisplayname(String displayname) {
		this.displayname = displayname;
	}
	public String getPayrollType() {
		return payrollType;
	}
	public void setPayrollType(String payrollType) {
		this.payrollType = payrollType;
	}
	public String getOtCheck() {
		return otCheck;
	}
	public void setOtCheck(String otCheck) {
		this.otCheck = otCheck;
	}
	public String getMinHrOT() {
		return minHrOT;
	}
	public void setMinHrOT(String minHrOT) {
		this.minHrOT = minHrOT;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	
	public UnitOrBranchDO getUnitOrBranch() {
		return unitOrBranch;
	}
	public void setUnitOrBranch(UnitOrBranchDO unitOrBranch) {
		this.unitOrBranch = unitOrBranch;
	}
}