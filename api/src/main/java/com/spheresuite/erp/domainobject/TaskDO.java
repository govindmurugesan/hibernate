package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="crm_task")
@TableGenerator(name ="crm_task", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "crm_task.findAll", query = "SELECT r FROM TaskDO r"),
    @NamedQuery(name = "crm_task.findById", query = "SELECT r FROM TaskDO r where r.taskId =:id"),
    @NamedQuery(name = "crm_task.findByLeadId", query = "SELECT r FROM TaskDO r where r.lead.leadId =:id"),
    @NamedQuery(name = "crm_task.findPendingTask", query = "SELECT r FROM TaskDO r where r.status =:status order by r.taskDate asc"),
})
public class TaskDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "crm_task.findAll";
	
	public static final String FIND_BY_ID = "crm_task.findById";
	
	public static final String FIND_BY_LEAD_ID = "crm_task.findByLeadId";
	
	public static final String FIND_PENDING_TASK = "crm_task.findPendingTask";
	
	@Id
	@Column(nullable = false, unique = true)
	@GeneratedValue(strategy=GenerationType.AUTO) 
	private Long taskId;
	
	@ManyToOne
	@JoinColumn(name="leadId")
	private LeadDO lead;
	
	private String taskDetails;
	@Temporal(TemporalType.TIMESTAMP)
	private Date taskDate;
	private String taskTime;
	private char status;
	private String updatedBy;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    
	private String empId;


	public Long getTaskId() {
		return taskId;
	}

	public void setTaskIid(Long taskId) {
		this.taskId = taskId;
	}

	public LeadDO getLead() {
		return lead;
	}

	public void setLead(LeadDO lead) {
		this.lead = lead;
	}

	public String getCreatedby() {
		return createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	public Date getCreatedon() {
		return createdon;
	}

	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}

	public String getEmpId() {
		return empId;
	}

	public void setEmpId(String empId) {
		this.empId = empId;
	}

	public String getTaskDetails() {
		return taskDetails;
	}

	public void setTaskDetails(String taskDetails) {
		this.taskDetails = taskDetails;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedon() {
		return updatedon;
	}

	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public String getTaskTime() {
		return taskTime;
	}

	public void setTaskTime(String taskTime) {
		this.taskTime = taskTime;
	}

	public Date getTaskDate() {
		return taskDate;
	}

	public void setTaskDate(Date taskDate) {
		this.taskDate = taskDate;
	}
	public char getStatus() {
		return status;
	}

	public void setStatus(char status) {
		this.status = status;
	}

}