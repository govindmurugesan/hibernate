package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_empPFInfo")
@TableGenerator(name ="hrms_empPFInfo", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "hrms_empPFInfo.findById", query = "SELECT r FROM EmployeePFInfoDO r where r.empPFInfoId =:id"),
    @NamedQuery(name = "hrms_empPFInfo.findByEmpId", query = "SELECT r FROM EmployeePFInfoDO r where r.employee.empId =:empId order by r.empPFInfoId desc"),
    @NamedQuery(name = "hrms_empPFInfo.findByEmpIdAndDate", query = "SELECT r FROM EmployeePFInfoDO r where r.employee.empId =:empId and ((r.fromDate <=:fromDate and r.toDate >=:toDate) or ((r.fromDate <=:fromDate and r.toDate IS NULL) and (r.fromDate <=:toDate)))"),
})
public class EmployeePFInfoDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_BY_ID = "hrms_empPFInfo.findById";
	
	public static final String FIND_BY_EMPID = "hrms_empPFInfo.findByEmpId";
	
	public static final String FIND_BY_EMPID_DATE = "hrms_empPFInfo.findByEmpIdAndDate";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)  
	private Long empPFInfoId;
	
	private String PFAccNo;
	private String type;
	private String uan;
	
	private Long amount;
	
	@ManyToOne
	@JoinColumn(name="pfGroupId")
	private PFGroupDO pfGroup;
	
	/*@ManyToOne
	@JoinColumn(name="esiGroupId")
	private ESIGroupDO esiGroup;*/
	
	@ManyToOne
	@JoinColumn(name="empId")
	private EmployeeDO employee;
	
	 @Temporal(TemporalType.TIMESTAMP)
	 private Date fromDate;
	 
	 @Temporal(TemporalType.TIMESTAMP)
	 private Date toDate;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getEmpPFInfoId() {
		return empPFInfoId;
	}
	public void setEmpPFInfoId(Long empPFInfoId) {
		this.empPFInfoId = empPFInfoId;
	}
	public String getPFAccNo() {
		return PFAccNo;
	}
	public void setPFAccNo(String pFAccNo) {
		PFAccNo = pFAccNo;
	}
	public PFGroupDO getPfGroup() {
		return pfGroup;
	}
	public void setPfGroup(PFGroupDO pfGroup) {
		this.pfGroup = pfGroup;
	}
	public EmployeeDO getEmployee() {
		return employee;
	}
	public void setEmployee(EmployeeDO employee) {
		this.employee = employee;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getUan() {
		return uan;
	}
	public void setUan(String uan) {
		this.uan = uan;
	}
	public Long getAmount() {
		return amount;
	}
	public void setAmount(Long amount) {
		this.amount = amount;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
    
    
	
}