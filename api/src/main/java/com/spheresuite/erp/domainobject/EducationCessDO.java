package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_eductionCess")
@TableGenerator(name ="hrms_eductionCess", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "hrms_eductionCess.findById", query = "SELECT r FROM EducationCessDO r where r.educationCessId =:id"),
    @NamedQuery(name = "hrms_eductionCess.findByDate", query = "SELECT r FROM EducationCessDO r where (r.fromYear =:fromDate and r.toYear =:toDate)"),
    @NamedQuery(name = "hrms_eductionCess.findByFy", query = "SELECT r FROM EducationCessDO r where (r.fromYear <=:fromDate and r.toYear >=:toDate)"),
    @NamedQuery(name = "hrms_eductionCess.findByCurrentDate", query = "SELECT r FROM EducationCessDO r where r.status =:status and ((r.fromYear <=:date and r.toYear >=:date) or (r.fromYear <=:date and r.toYear IS NULL))"),
    @NamedQuery(name = "hrms_eductionCess.findForUpdate", query = "SELECT r FROM EducationCessDO r where (r.fromYear <=:fromDate and r.toYear >=:toDate) and r.educationCessId  <>:id"),
})
public class EducationCessDO implements Serializable {
	private static final long serialVersionUID = 1L;

	
	public static final String FIND_BY_ID = "hrms_eductionCess.findById";
	
	public static final String FIND_BY_FY = "hrms_eductionCess.findByFy";
	
	public static final String FIND_BY_DATE = "hrms_eductionCess.findByDate";
	
	public static final String FIND_BY_CURRENTDATE = "hrms_eductionCess.findByCurrentDate";
	
	public static final String FIND_FOR_UPDATE = "hrms_eductionCess.findForUpdate";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long educationCessId;
	
	
	@Temporal(TemporalType.DATE)
	private Date fromYear;
	@Temporal(TemporalType.DATE)
	private Date toYear;
	private Double percentage;
	private char status;
	
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getEducationCessId() {
		return educationCessId;
	}
	public void setEducationCessId(Long educationCessId) {
		this.educationCessId = educationCessId;
	}
	public Date getFromYear() {
		return fromYear;
	}
	public void setFromYear(Date fromYear) {
		this.fromYear = fromYear;
	}
	public Date getToYear() {
		return toYear;
	}
	public void setToYear(Date toYear) {
		this.toYear = toYear;
	}
	public Double getPercentage() {
		return percentage;
	}
	public void setPercentage(Double percentage) {
		this.percentage = percentage;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}