package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_allowancesetting")
@TableGenerator(name ="hrms_allowancesetting", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "hrms_allowancesetting.findById", query = "SELECT r FROM AllowanceSettingDO r where r.allowanceSettingsId =:id"),
    @NamedQuery(name = "hrms_allowancesetting.findActive", query = "SELECT r FROM AllowanceSettingDO r where r.status =:status and r.name =:name"),
  //  @NamedQuery(name = "hrms_allowancesetting.findBetweenDate", query = "SELECT r FROM AllowanceSettingDO r where r.status =:status and r.name =:name"),
    
})
public class AllowanceSettingDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static final String FIND_BY_ID = "hrms_allowancesetting.findById";
	
	public static final String FIND_ACTIVE = "hrms_allowancesetting.findActive";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO) 
	private Long allowanceSettingsId;
	
	private String type;
	private String name;
	private String displayname;
	@Temporal(TemporalType.DATE)
	private Date startdate;
	@Temporal(TemporalType.DATE)
	private Date enddate;
	private String section;
	private String taxable;
	private String fixedamount;
	private String percentageamount;
	private String basicgrosspay;
	private String notes;
	private char status;
	
	@ManyToOne
	@JoinColumn(name="earningTypeId")
	private EarningTypeDO earningType;
	//private Long allowancetypeid;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getAllowanceSettingsId() {
		return allowanceSettingsId;
	}
	public void setAllowanceSettingsId(Long allowanceSettingsId) {
		this.allowanceSettingsId = allowanceSettingsId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	public String getTaxable() {
		return taxable;
	}
	public void setTaxable(String taxable) {
		this.taxable = taxable;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public String getDisplayname() {
		return displayname;
	}
	public void setDisplayname(String displayname) {
		this.displayname = displayname;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getFixedamount() {
		return fixedamount;
	}
	public void setFixedamount(String fixedamount) {
		this.fixedamount = fixedamount;
	}
	public String getPercentageamount() {
		return percentageamount;
	}
	public void setPercentageamount(String percentageamount) {
		this.percentageamount = percentageamount;
	}
	public String getBasicgrosspay() {
		return basicgrosspay;
	}
	public void setBasicgrosspay(String basicgrosspay) {
		this.basicgrosspay = basicgrosspay;
	}
	public Date getStartdate() {
		return startdate;
	}
	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}
	public Date getEnddate() {
		return enddate;
	}
	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public EarningTypeDO getEarningType() {
		return earningType;
	}
	public void setEarningType(EarningTypeDO earningType) {
		this.earningType = earningType;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	/*@Transient
	private String allowanceSettingName;*/
}