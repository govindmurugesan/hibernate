package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="payrolllopdetails")
@TableGenerator(name ="payrolllopdetails", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "payrolllopdetails.findAll", query = "SELECT r FROM PayrollLopDetailsDO r"),
    @NamedQuery(name = "payrolllopdetails.findByEmpId", query = "SELECT r FROM PayrollLopDetailsDO r where r.empId =:id and r.payrollMonth =:monthly"),
    @NamedQuery(name = "payrolllopdetails.findByBetweenDateByEpmId", query = "SELECT r FROM PayrollLopDetailsDO r where r.empId =:id and (r.payrollMonth <= :fromDate and r.payrollMonth >= :toDate)"),
})
public class PayrollLopDetailsDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "payrolllopdetails.findAll";
	
	public static final String FIND_BY_EMPID = "payrolllopdetails.findByEmpId";
	
	public static final String FIND_BY_EMPID_BETWEENDATES = "payrolllopdetails.findByBetweenDateByEpmId";
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO) 
	private Long id;
	private Long empId;
	private String payrollMonth;
	private Double monthly;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private String updatedBy;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getEmpId() {
		return empId;
	}
	public void setEmpId(Long empId) {
		this.empId = empId;
	}
	public String getPayrollMonth() {
		return payrollMonth;
	}
	public void setPayrollMonth(String payrollMonth) {
		this.payrollMonth = payrollMonth;
	}
	public Double getMonthly() {
		return monthly;
	}
	public void setMonthly(Double monthly) {
		this.monthly = monthly;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
}