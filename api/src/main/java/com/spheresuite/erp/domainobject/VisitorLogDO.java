package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="gms_visitorlog")
@TableGenerator(name ="gms_visitorlog", initialValue =100101, allocationSize =1)
@NamedQueries({
	@NamedQuery(name = "gms_visitorlog.findByMobileNoAndStatus", query = "SELECT r FROM VisitorLogDO r where r.status =:status and r.mobile=:mobile"),
	@NamedQuery(name = "gms_visitorlog.findByMobileNo", query = "SELECT r FROM VisitorLogDO r where r.mobile=:mobile"),
	@NamedQuery(name = "gms_visitorlog.findById", query = "SELECT r FROM VisitorLogDO r where r.id =:id"),
})
public class VisitorLogDO implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final String FIND_BY_MOBILENOANDSTATUS = "gms_visitorlog.findByMobileNoAndStatus";
	public static final String FIND_BY_MOBILENO = "gms_visitorlog.findByMobileNo";
	public static final String FIND_BY_ID = "gms_visitorlog.findById";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private String name;
	private String mobile;
	private String vehicleNo;
	private String company;
	private String address;
	private String city;
	private String state;
	private String idProof;
	private String purpose;
	private String personToMeet;
	@Temporal(TemporalType.TIMESTAMP)
	private Date enteredOn;
	@Temporal(TemporalType.TIMESTAMP)
	private Date exitOn;
	private char status;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private String updatedBy;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getVehicleNo() {
		return vehicleNo;
	}
	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getIdProof() {
		return idProof;
	}
	public void setIdProof(String idProof) {
		this.idProof = idProof;
	}
	public String getPurpose() {
		return purpose;
	}
	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}
	public String getPersonToMeet() {
		return personToMeet;
	}
	public void setPersonToMeet(String personToMeet) {
		this.personToMeet = personToMeet;
	}
	public Date getEnteredOn() {
		return enteredOn;
	}
	public void setEnteredOn(Date enteredOn) {
		this.enteredOn = enteredOn;
	}
	public Date getExitOn() {
		return exitOn;
	}
	public void setExitOn(Date exitOn) {
		this.exitOn = exitOn;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	
	

	
}