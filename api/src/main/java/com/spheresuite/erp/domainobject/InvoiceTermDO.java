package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="crm_invoiceterm")
@TableGenerator(name ="crm_invoiceterm", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "crm_invoiceterm.findByProposalId", query = "SELECT r FROM InvoiceTermDO r where r.proposal.proposalId =:proposalId"),
})
public class InvoiceTermDO implements Serializable {
	private static final long serialVersionUID = 1L;

	
	public static final String FIND_BY_PROPOSAL_ID = "crm_invoiceterm.findByProposalId";
	
	
	@Id
	@Column(nullable = false, unique = true)
	@GeneratedValue(strategy=GenerationType.AUTO) 
	private Long invoiceTermId;
	
	@ManyToOne
	@JoinColumn(name="proposalId")
	private ProposalDO proposal;
	//private Long proposalId;
	
	private String descripetion;
	private String projectPercentage;
	private String updatedby;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private String empId;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;

	public Long getInvoiceTermId() {
		return invoiceTermId;
	}
	public void setInvoiceTermId(Long invoiceTermId) {
		this.invoiceTermId = invoiceTermId;
	}
	public ProposalDO getProposal() {
		return proposal;
	}
	public void setProposal(ProposalDO proposal) {
		this.proposal = proposal;
	}
	public String getEmpId() {
		return empId;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getDescripetion() {
		return descripetion;
	}
	public void setDescripetion(String descripetion) {
		this.descripetion = descripetion;
	}
	public String getProjectPercentage() {
		return projectPercentage;
	}
	public void setProjectPercentage(String projectPercentage) {
		this.projectPercentage = projectPercentage;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}