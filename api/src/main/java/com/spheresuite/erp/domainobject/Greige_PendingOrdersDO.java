package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="greige_pendingOrders")
@TableGenerator(name ="greige_pendingOrders", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "greige_pendingOrders.findById", query = "SELECT r FROM Greige_PendingOrdersDO r where r.pendingOrderId =:id"),
    @NamedQuery(name = "greige_pendingOrders.findAll", query = "SELECT r FROM Greige_PendingOrdersDO r order by r.greigReqDate asc"),
    @NamedQuery(name = "greige_pendingOrders.findStatus", query = "SELECT r FROM Greige_PendingOrdersDO r where r.status is null order by r.greigReqDate asc"),
    @NamedQuery(name = "greige_pendingOrders.findBySortId", query = "SELECT r FROM Greige_PendingOrdersDO r where r.sortName.sortId =:id"),
    @NamedQuery(name = "greige_pendingOrders.findByGreigeReqDate", query = "SELECT r FROM Greige_PendingOrdersDO r where r.greigReqDate =:date "),
    @NamedQuery(name = "greige_pendingOrders.findByGrgReqDate", query = "SELECT r FROM Greige_PendingOrdersDO r where r.greigReqDate =:date AND  r.sortName.sortId =:id"),
    @NamedQuery(name = "greige_pendingOrders.findByOcnCount", query = "SELECT count(r) FROM Greige_PendingOrdersDO r")
    //  @NamedQuery(name = "greige_pendingOrders.findByStatus", query = "SELECT r FROM Greige_pendingOrders r where r.status =:status"),and MONTH(r.greigReqDate)=MONTH(CURRENT_DATE) and YEAR(r.greigReqDate)=YEAR(CURRENT_DATE)
/*    @NamedQuery(name = "ep_master_sort.findByName", query = "SELECT r FROM EarningTypeDO r where r.name=:name"),
    @NamedQuery(name = "ep_master_sort.findByNameAndId", query = "SELECT r FROM EarningTypeDO r where r.name=:name and r.earningTypeId NOT IN :id"),*/
})
public class Greige_PendingOrdersDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_BY_ID = "greige_pendingOrders.findById";
	
	public static final String FIND_ALL = "greige_pendingOrders.findAll";
	
	public static final String FIND_STATUS = "greige_pendingOrders.findStatus";
	
	public static final String FIND_COUNT = "greige_pendingOrders.findByOcnCount";
	
	public static final String FIND_BY_SORTID= "greige_pendingOrders.findBySortId";
	
	public static final String FIND_BY_GREIGE_REQ_DATE= "greige_pendingOrders.findByGreigeReqDate";
	
	public static final String FIND_BY_GREIG_REQ_DATE= "greige_pendingOrders.findByGrgReqDate";
	
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long pendingOrderId;
	
	private String ocn;
	private String planNo;
	private String greigReq;
	private String status;
	/*private String grCons ;*/
/*	private Long pendigDays ;*/
	@Temporal(TemporalType.DATE)
    private Date shippingDate;
	@Temporal(TemporalType.DATE)
    private Date greigReqDate;
	
	@ManyToOne
	@JoinColumn(name="sortId")
	private EPMasterSortDO sortName;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getPendingOrderId() {
		return pendingOrderId;
	}
	public void setPendingOrderId(Long pendingOrderId) {
		this.pendingOrderId = pendingOrderId;
	}
	public String getOcn() {
		return ocn;
	}
	public void setOcn(String ocn) {
		this.ocn = ocn;
	}
	public String getPlanNo() {
		return planNo;
	}
	public void setPlanNo(String planNo) {
		this.planNo = planNo;
	}
	public String getGreigReq() {
		return greigReq;
	}
	public void setGreigReq(String greigReq) {
		this.greigReq = greigReq;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	/*public String getGrCons() {
		return grCons;
	}
	public void setGrCons(String grCons) {
		this.grCons = grCons;
	}*/
	public Date getShippingDate() {
		return shippingDate;
	}
	public void setShippingDate(Date shippingDate) {
		this.shippingDate = shippingDate;
	}
	public Date getGreigReqDate() {
		return greigReqDate;
	}
	public void setGreigReqDate(Date greigReqDate) {
		this.greigReqDate = greigReqDate;
	}
	public EPMasterSortDO getSortName() {
		return sortName;
	}
	public void setSortName(EPMasterSortDO sortName) {
		this.sortName = sortName;
	}
	/*public Long getPendigDays() {
		return pendigDays;
	}
	public void setPendigDays(Long pendigDays) {
		this.pendigDays = pendigDays;
	}*/
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}