package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_generalDeductionDetail")
@TableGenerator(name ="hrms_generalDeductionDetail", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "hrms_generalDeductionDetail.findById", query = "SELECT r FROM GeneralDeductionDetailDO r where r.generalDeductionDetailId =:id"),
    @NamedQuery(name = "hrms_generalDeductionDetail.findByLoanId", query = "SELECT r FROM GeneralDeductionDetailDO r where r.loanPayment.generalDeductionId =:id"),
    @NamedQuery(name = "hrms_generalDeductionDetail.findByEmpIdMonth", query = "SELECT r FROM GeneralDeductionDetailDO r where r.employee.empId =:empId and r.status =:status and r.month =:month"),
})
public class GeneralDeductionDetailDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_BY_ID = "hrms_generalDeductionDetail.findById";
	
	public static final String FIND_BY_LOAN_ID = "hrms_generalDeductionDetail.findByLoanId";
	
	public static final String FIND_BY_EMPID_MONTH = "hrms_generalDeductionDetail.findByEmpIdMonth";
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)  
	private Long generalDeductionDetailId;
	private String month;
	private String amount;
	private String status;
	
	@ManyToOne
	@JoinColumn(name="empId")
	private EmployeeDO employee;
	
	@ManyToOne
	@JoinColumn(name="generalDeductionId")
	private GeneralDeductionDO loanPayment;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
	public Long getGeneralDeductionDetailId() {
		return generalDeductionDetailId;
	}
	public void setGeneralDeductionDetailId(Long generalDeductionDetailId) {
		this.generalDeductionDetailId = generalDeductionDetailId;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public EmployeeDO getEmployee() {
		return employee;
	}
	public void setEmployee(EmployeeDO employee) {
		this.employee = employee;
	}
	public GeneralDeductionDO getLoanPayment() {
		return loanPayment;
	}
	public void setLoanPayment(GeneralDeductionDO loanPayment) {
		this.loanPayment = loanPayment;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}