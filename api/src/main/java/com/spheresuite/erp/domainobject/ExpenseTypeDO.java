package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_master_expensetype")
@TableGenerator(name ="hrms_master_expensetype", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "hrms_master_expensetype.findById", query = "SELECT r FROM ExpenseTypeDO r where r.expenseTypeId =:id"),
    @NamedQuery(name = "hrms_master_expensetype.findByStatus", query = "SELECT r FROM ExpenseTypeDO r where r.status =:status"),
    @NamedQuery(name = "hrms_master_expensetype.findByName", query = "SELECT r FROM ExpenseTypeDO r where r.name=:name"),
    @NamedQuery(name = "hrms_master_expensetype.findByNameAndId", query = "SELECT r FROM ExpenseTypeDO r where r.name=:name and r.expenseTypeId NOT IN :id"),
})
public class ExpenseTypeDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static final String FIND_BY_ID = "hrms_master_expensetype.findById";
	
	public static final String FIND_BY_STATUS = "hrms_master_expensetype.findByStatus";
	
	public static final String FIND_BY_NAME = "hrms_master_expensetype.findByName";
	
	public static final String FIND_FOR_UPDATE = "hrms_master_expensetype.findByNameAndId";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long expenseTypeId;
	private String name;
	private String displayName;
	private String status;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
	public Long getExpenseTypeId() {
		return expenseTypeId;
	}
	public void setExpenseTypeId(Long expenseTypeId) {
		this.expenseTypeId = expenseTypeId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
    
}