package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="employeecontactinfo")
@TableGenerator(name ="employeecontactinfo", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "employeecontactinfo.findById", query = "SELECT r FROM EmployeeContactInfoDO r where r.empContactInfoId =:id"),
    @NamedQuery(name = "employeecontactinfo.findByEmpId", query = "SELECT r FROM EmployeeContactInfoDO r where r.employee.empId =:empId order by r.empContactInfoId desc"),
})
public class EmployeeContactInfoDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_BY_ID = "employeecontactinfo.findById";
	
	public static final String FIND_BY_EMPID = "employeecontactinfo.findByEmpId";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO) 
	private Long empContactInfoId;
	
	private Long homePhone;
	private Long cellPhone;
	private Long workPhone;
	private String personalEmail;
	private String companyEmail;
	private String otherEmail;
	
	@ManyToOne
	@JoinColumn(name="empId")
	private EmployeeDO employee;

	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getEmpContactInfoId() {
		return empContactInfoId;
	}
	public void setEmpContactInfoId(Long empContactInfoId) {
		this.empContactInfoId = empContactInfoId;
	}
	public Long getHomePhone() {
		return homePhone;
	}
	public void setHomePhone(Long homePhone) {
		this.homePhone = homePhone;
	}
	public Long getCellPhone() {
		return cellPhone;
	}
	public void setCellPhone(Long cellPhone) {
		this.cellPhone = cellPhone;
	}
	public Long getWorkPhone() {
		return workPhone;
	}
	public void setWorkPhone(Long workPhone) {
		this.workPhone = workPhone;
	}
	public String getPersonalEmail() {
		return personalEmail;
	}
	public void setPersonalEmail(String personalEmail) {
		this.personalEmail = personalEmail;
	}
	public String getCompanyEmail() {
		return companyEmail;
	}
	public void setCompanyEmail(String companyEmail) {
		this.companyEmail = companyEmail;
	}
	public String getOtherEmail() {
		return otherEmail;
	}
	public void setOtherEmail(String otherEmail) {
		this.otherEmail = otherEmail;
	}
	public EmployeeDO getEmployee() {
		return employee;
	}
	public void setEmployee(EmployeeDO employee) {
		this.employee = employee;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}