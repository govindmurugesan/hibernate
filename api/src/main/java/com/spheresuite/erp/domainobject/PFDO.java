package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_master_pf")
@TableGenerator(name ="hrms_master_pf", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "hrms_master_pf.findById", query = "SELECT r FROM PFDO r where r.pfId =:id"),
   // @NamedQuery(name = "hrms_master_pf.findBySalary", query = "SELECT r FROM PFDO r where (r.salaryFrom <=:fromAmount and r.salaryTo >=:toAmount)"),
    @NamedQuery(name = "hrms_master_pf.findByStatus", query = "SELECT r FROM PFDO r where status=:status)"),
 //   @NamedQuery(name = "hrms_pf.findByDate", query = "SELECT r FROM PFDO r where (r.fromYear =:fromDate and r.toYear =:toDate)"),
   // 
   // @NamedQuery(name = "hrms_pf.findByCurrentDate", query = "SELECT r FROM EducationCessDO r where r.status =:status and ((r.fromYear <=:date and r.toYear >=:date) or (r.fromYear <=:date and r.toYear IS NULL))"),
   // @NamedQuery(name = "hrms_master_pf.findForUpdate", query = "SELECT r FROM PFDO r where (r.salaryFrom <=:fromAmount and r.salaryTo >=:toAmount) and r.pfId  <>:id"),
})
public class PFDO implements Serializable {
	private static final long serialVersionUID = 1L;

	
	public static final String FIND_BY_ID = "hrms_master_pf.findById";
	
//	public static final String FIND_BY_SALARY = "hrms_master_pf.findBySalary";
	
	public static final String FIND_BY_STATUS = "hrms_master_pf.findByStatus";
	
	
	/*public static final String FIND_BY_DATE = "hrms_pf.findByDate";
	
	public static final String FIND_BY_CURRENTDATE = "hrms_pf.findByCurrentDate";*/
	
//	public static final String FIND_FOR_UPDATE = "hrms_master_pf.findForUpdate";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long pfId;
	
	
	private Double employeePercentage;
	private Double employerPercentage;
	private char status;
	
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getPfId() {
		return pfId;
	}
	public void setPfId(Long pfId) {
		this.pfId = pfId;
	}
	public Double getEmployeePercentage() {
		return employeePercentage;
	}
	public void setEmployeePercentage(Double employeePercentage) {
		this.employeePercentage = employeePercentage;
	}
	public Double getEmployerPercentage() {
		return employerPercentage;
	}
	public void setEmployerPercentage(Double employerPercentage) {
		this.employerPercentage = employerPercentage;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}