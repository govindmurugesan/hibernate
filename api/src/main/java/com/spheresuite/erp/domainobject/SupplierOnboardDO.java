package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="supplieronboard")
@TableGenerator(name ="supplieronboard", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "supplieronboard.findById", query = "SELECT r FROM SupplierOnboardDO r where r.supplierOnboardId =:id"),
    @NamedQuery(name = "supplieronboard.findByEmail", query = "SELECT r FROM SupplierOnboardDO r where r.email =:email and r.status<>:status"),
    @NamedQuery(name = "supplieronboard.findByEmailId_Stauts", query = "SELECT r FROM SupplierOnboardDO r where r.email =:email and (r.status = 'a' or r.status = 'i' or r.status = 'p')"),
    @NamedQuery(name = "supplieronboard.findforUpdate", query = "SELECT r FROM SupplierOnboardDO r where r.email=:email and r.supplierOnboardId NOT IN :id"),
})
public class SupplierOnboardDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_BY_ID = "supplieronboard.findById";
	
	public static final String FIND_BY_EMAIL = "supplieronboard.findByEmail";
	
	public static final String FIND_BY_EMAILID_STATUS = "supplieronboard.findByEmailId_Stauts";
	
	public static final String FIND_FOR_UPDATE = "supplieronboard.findforUpdate";
	
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long supplierOnboardId;
	
	private String name;
	private String registeredAddr;
	private String communicationAddr;
	private String entityStatus;
	private String nameOfDirectors;
	private String pan;
	private String gstRegNo;
	private String cin;
	private String cstRegNo;
	private String centralExciseRegNo;
	private String esiRegNo;
	private String pfRegNo;
	private String bankName;
	private String bankBranch;
	private String typeOfAccount;
	private Long accountNo;
	private String ifsCode;
	private String listOfProducts;
	private String specializedIn;
	
	private String supplierContactId;
	private String fname;
	private String lname;
	private String mname;
	private String designation;
	private String mobile;
	private String email;
	
	private String reason;
	
	private char status;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
	private String updatedby;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	
	
	public Long getSupplierOnboardId() {
		return supplierOnboardId;
	}
	public void setSupplierOnboardId(Long supplierOnboardId) {
		this.supplierOnboardId = supplierOnboardId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getRegisteredAddr() {
		return registeredAddr;
	}
	public void setRegisteredAddr(String registeredAddr) {
		this.registeredAddr = registeredAddr;
	}
	public String getCommunicationAddr() {
		return communicationAddr;
	}
	public void setCommunicationAddr(String communicationAddr) {
		this.communicationAddr = communicationAddr;
	}
	public String getEntityStatus() {
		return entityStatus;
	}
	public void setEntityStatus(String entityStatus) {
		this.entityStatus = entityStatus;
	}
	public String getNameOfDirectors() {
		return nameOfDirectors;
	}
	public void setNameOfDirectors(String nameOfDirectors) {
		this.nameOfDirectors = nameOfDirectors;
	}
	public String getPan() {
		return pan;
	}
	public void setPan(String pan) {
		this.pan = pan;
	}
	public String getGstRegNo() {
		return gstRegNo;
	}
	public void setGstRegNo(String gstRegNo) {
		this.gstRegNo = gstRegNo;
	}
	public String getCin() {
		return cin;
	}
	public void setCin(String cin) {
		this.cin = cin;
	}
	public String getCstRegNo() {
		return cstRegNo;
	}
	public void setCstRegNo(String cstRegNo) {
		this.cstRegNo = cstRegNo;
	}
	public String getCentralExciseRegNo() {
		return centralExciseRegNo;
	}
	public void setCentralExciseRegNo(String centralExciseRegNo) {
		this.centralExciseRegNo = centralExciseRegNo;
	}
	public String getEsiRegNo() {
		return esiRegNo;
	}
	public void setEsiRegNo(String esiRegNo) {
		this.esiRegNo = esiRegNo;
	}
	public String getPfRegNo() {
		return pfRegNo;
	}
	public void setPfRegNo(String pfRegNo) {
		this.pfRegNo = pfRegNo;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getBankBranch() {
		return bankBranch;
	}
	public void setBankBranch(String bankBranch) {
		this.bankBranch = bankBranch;
	}
	public String getTypeOfAccount() {
		return typeOfAccount;
	}
	public void setTypeOfAccount(String typeOfAccount) {
		this.typeOfAccount = typeOfAccount;
	}
	public Long getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(Long accountNo) {
		this.accountNo = accountNo;
	}
	public String getIfsCode() {
		return ifsCode;
	}
	public void setIfsCode(String ifsCode) {
		this.ifsCode = ifsCode;
	}
	public String getListOfProducts() {
		return listOfProducts;
	}
	public void setListOfProducts(String listOfProducts) {
		this.listOfProducts = listOfProducts;
	}
	public String getSpecializedIn() {
		return specializedIn;
	}
	public void setSpecializedIn(String specializedIn) {
		this.specializedIn = specializedIn;
	}
	public String getSupplierContactId() {
		return supplierContactId;
	}
	public void setSupplierContactId(String supplierContactId) {
		this.supplierContactId = supplierContactId;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public String getMname() {
		return mname;
	}
	public void setMname(String mname) {
		this.mname = mname;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	
}