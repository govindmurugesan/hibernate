package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="support")
@TableGenerator(name ="support", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "support.findAll", query = "SELECT r FROM SupportDO r where r.empId IN :id"),
    @NamedQuery(name = "support.findById", query = "SELECT r FROM SupportDO r where r.supportId =:id"),
    @NamedQuery(name = "support.findBySupportId", query = "SELECT r FROM SupportDO r where r.issueType.issueTypeId =:id"),
    @NamedQuery(name = "support.findAllData", query = "SELECT r FROM SupportDO r "),
    @NamedQuery(name = "support.findByEmailId", query = "SELECT r FROM SupportDO r where r.email =:email"),
})
public class SupportDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "support.findAll";
	
	public static final String FIND_ALL_DATA = "support.findAllData";
	
	public static final String FIND_BY_ID = "support.findById";
	
	public static final String FIND_BY_EMAIL_ID = "support.findByEmailId";
	
	public static final String FIND_BY_SUPPORT_ID = "support.findBySupportId";
	
	@Id
	@Column(nullable = false, unique = true)
	@GeneratedValue(strategy=GenerationType.AUTO) 
	private Long supportId;
	
	@ManyToOne
	@JoinColumn(name="issueTypeId")
	private IssueTypeDO issueType;
	
	@ManyToOne
	@JoinColumn(name="priorityLevelId")
	private PriorityLevelDO priorityLevel;
	
	@ManyToOne
	@JoinColumn(name="statusId")
	private StatusDO statusDetails;
	
	private String empId;
	private String empName;
	private String mobile;
	private String phone;
	private String email;
	private String organization;
	@Column(columnDefinition="LONGTEXT")
	private String issueSummary;
	@Column(columnDefinition="LONGTEXT")
	private String issueDetails;
	
	private String updatedby;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
	public Long getSupportId() {
		return supportId;
	}
	public void setSupportId(Long supportId) {
		this.supportId = supportId;
	}
	public IssueTypeDO getIssueType() {
		return issueType;
	}
	public void setIssueType(IssueTypeDO issueType) {
		this.issueType = issueType;
	}
	public PriorityLevelDO getPriorityLevel() {
		return priorityLevel;
	}
	public void setPriorityLevel(PriorityLevelDO priorityLevel) {
		this.priorityLevel = priorityLevel;
	}
	public StatusDO getStatusDetails() {
		return statusDetails;
	}
	public void setStatusDetails(StatusDO statusDetails) {
		this.statusDetails = statusDetails;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getOrganization() {
		return organization;
	}
	public void setOrganization(String organization) {
		this.organization = organization;
	}
	public String getIssueSummary() {
		return issueSummary;
	}
	public void setIssueSummary(String issueSummary) {
		this.issueSummary = issueSummary;
	}
	public String getIssueDetails() {
		return issueDetails;
	}
	public void setIssueDetails(String issueDetails) {
		this.issueDetails = issueDetails;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public String getEmpId() {
		return empId;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	
    
}