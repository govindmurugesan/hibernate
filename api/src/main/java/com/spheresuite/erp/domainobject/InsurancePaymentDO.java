package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_insurancePayment")
@TableGenerator(name ="hrms_insurancePayment", initialValue =100101, allocationSize =1)
@NamedQueries({
	@NamedQuery(name = "hrms_insurancePayment.findAll", query = "SELECT r FROM InsurancePaymentDO r ORDER BY r.insurancePaymentId DESC"),
    @NamedQuery(name = "hrms_insurancePayment.findById", query = "SELECT r FROM InsurancePaymentDO r where r.insurancePaymentId =:id"),
    @NamedQuery(name = "hrms_insurancePayment.findActiveByEmpId", query = "SELECT r FROM InsurancePaymentDO r where r.employee.empId =:empId and r.status =:status"),
})
public class InsurancePaymentDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_BY_ID = "hrms_insurancePayment.findById";
	
	public static final String FIND_ALL = "hrms_insurancePayment.findAll";
	
	public static final String FIND_BY_EMPID = "hrms_insurancePayment.findByEmpId";
	
	public static final String FIND_ACTIVE_BY_EMPID = "hrms_insurancePayment.findActiveByEmpId";
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)  
	private Long insurancePaymentId;
	private Long amount;
	private String month;
	private String noOfInstallments;
	private String installmentAmount;
	private String status;
	
	@ManyToOne
	@JoinColumn(name="empId")
	private EmployeeDO employee;
	
	@ManyToOne
	@JoinColumn(name="insuranceTypeId")
	private InsuranceTypeDO insuranceType;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
	public Long getInsurancePaymentId() {
		return insurancePaymentId;
	}
	public void setInsurancePaymentId(Long insurancePaymentId) {
		this.insurancePaymentId = insurancePaymentId;
	}
	public Long getAmount() {
		return amount;
	}
	public void setAmount(Long amount) {
		this.amount = amount;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public String getNoOfInstallments() {
		return noOfInstallments;
	}
	public void setNoOfInstallments(String noOfInstallments) {
		this.noOfInstallments = noOfInstallments;
	}
	public String getInstallmentAmount() {
		return installmentAmount;
	}
	public void setInstallmentAmount(String installmentAmount) {
		this.installmentAmount = installmentAmount;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public EmployeeDO getEmployee() {
		return employee;
	}
	public void setEmployee(EmployeeDO employee) {
		this.employee = employee;
	}
	public InsuranceTypeDO getInsuranceType() {
		return insuranceType;
	}
	public void setInsuranceType(InsuranceTypeDO insuranceType) {
		this.insuranceType = insuranceType;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}