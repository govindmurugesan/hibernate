package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_master_hrrequesttype")
@TableGenerator(name ="hrms_master_hrrequesttype", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "hrms_master_hrrequesttype.findById", query = "SELECT r FROM HrRequestTypeDO r where r.hrRequestTypeId =:id"),
    @NamedQuery(name = "hrms_master_hrrequesttype.findByName", query = "SELECT r FROM HrRequestTypeDO r where r.requestType=:name"),
    @NamedQuery(name = "hrms_master_hrrequesttype.findByNameAndId", query = "SELECT r FROM HrRequestTypeDO r where r.requestType=:name and r.hrRequestTypeId NOT IN :id"),
})
public class HrRequestTypeDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_BY_ID = "hrms_master_hrrequesttype.findById";
	
	public static final String FIND_BY_NAME = "hrms_master_hrrequesttype.findByName";
	
	public static final String FIND_FOR_UPDATE = "hrms_master_hrrequesttype.findByNameAndId";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long hrRequestTypeId;
	
	private String requestType;
	private char status;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getHrRequestTypeId() {
		return hrRequestTypeId;
	}
	public void setHrRequestTypeId(Long hrRequestTypeId) {
		this.hrRequestTypeId = hrRequestTypeId;
	}
	public String getRequestType() {
		return requestType;
	}
	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}