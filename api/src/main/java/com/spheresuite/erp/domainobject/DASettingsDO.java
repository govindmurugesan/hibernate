package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_dearnessallowance")
@TableGenerator(name ="hrms_dearnessallowance", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "hrms_dearnessallowance.findById", query = "SELECT r FROM DASettingsDO r where r.dearnessallowanceId =:id"),
    @NamedQuery(name = "hrms_dearnessallowance.findByMonthYear", query = "SELECT r FROM DASettingsDO r where r.month=:monthly and r.year=:year"),
    @NamedQuery(name = "hrms_dearnessallowance.findForUpdate", query = "SELECT r FROM DASettingsDO r where r.month=:monthly and r.year=:year and r.dearnessallowanceId NOT IN :id"),
    @NamedQuery(name = "hrms_dearnessallowance.findByMonthYearUnit", query = "SELECT r FROM DASettingsDO r where r.month=:monthly and r.year=:year and r.UnitOrBranch.unitOrBranchId=:unit"),
    
    
    /*  @NamedQuery(name = "hrms_dearnessallowance.findByStatus", query = "SELECT r FROM DASettingsDO r where r.status =:status"),
    @NamedQuery(name = "hrms_dearnessallowance.findByName", query = "SELECT r FROM DASettingsDO r where r.name=:name"),
    @NamedQuery(name = "hrms_dearnessallowance.findByNameAndId", query = "SELECT r FROM DASettingsDO r where r.name=:name and r.designationId NOT IN :id"),*/
})
public class DASettingsDO implements Serializable {
	private static final long serialVersionUID = 1L;

	
	public static final String FIND_BY_ID = "hrms_dearnessallowance.findById";
	
	public static final String FIND_BY_MONTH_YEAR = "hrms_dearnessallowance.findByMonthYear";
	
	public static final String FIND_FOR_UPDATE  = "hrms_dearnessallowance.findForUpdate";
	
	public static final String FIND_BY_MONTH_YEAR_UNIT = "hrms_dearnessallowance.findByMonthYearUnit";
	
	
	
	/*public static final String FIND_BY_STATUS = "hrms_dearnessallowance.findByStatus";
	
	public static final String FIND_BY_NAME = "hrms_dearnessallowance.findByName";
	
	public static final String FIND_FOR_UPDATE = "hrms_dearnessallowance.findByNameAndId";*/
	
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true) 
	private Long dearnessallowanceId;
	
	private Long year;
	private String month;
	private Long DApoints;
	private Float DArate;
	private Long DAdeductionpoints;
	private Long addDA;
	private Long subDA;
	
	@ManyToOne
	@JoinColumn(name="unitOrBranchId")
	private UnitOrBranchDO UnitOrBranch;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
	private String updatedby;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	public Long getDearnessallowanceId() {
		return dearnessallowanceId;
	}
	public void setDearnessallowanceId(Long dearnessallowanceId) {
		this.dearnessallowanceId = dearnessallowanceId;
	}
	public Long getYear() {
		return year;
	}
	public void setYear(Long year) {
		this.year = year;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public Long getDApoints() {
		return DApoints;
	}
	public void setDApoints(Long dApoints) {
		DApoints = dApoints;
	}
	public Float getDArate() {
		return DArate;
	}
	public void setDArate(Float dArate) {
		DArate = dArate;
	}
	public Long getDAdeductionpoints() {
		return DAdeductionpoints;
	}
	public void setDAdeductionpoints(Long dAdeductionpoints) {
		DAdeductionpoints = dAdeductionpoints;
	}
	public UnitOrBranchDO getUnitOrBranch() {
		return UnitOrBranch;
	}
	public void setUnitOrBranch(UnitOrBranchDO unitOrBranch) {
		UnitOrBranch = unitOrBranch;
	}
	public Long getAddDA() {
		return addDA;
	}
	public void setAddDA(Long addDA) {
		this.addDA = addDA;
	}
	public Long getSubDA() {
		return subDA;
	}
	public void setSubDA(Long subDA) {
		this.subDA = subDA;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}