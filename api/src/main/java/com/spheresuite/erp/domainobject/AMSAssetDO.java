package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="ams_asset")
@TableGenerator(name ="ams_asset", initialValue =100101, allocationSize =1)
@NamedQueries({
	@NamedQuery(name = "ams_asset.findAll", query = "SELECT r FROM AMSAssetDO r ORDER BY r.assetId DESC"),
    @NamedQuery(name = "ams_asset.findById", query = "SELECT r FROM AMSAssetDO r where r.assetId =:id"),
    @NamedQuery(name = "ams_asset.findByUnit", query = "SELECT r FROM AMSAssetDO r where r.unit.unitId =:id and r.status =:status"),
   // @NamedQuery(name = "ams_asset.findByStatus", query = "SELECT r FROM AMSAssetDO r where r.status =:status"),
   /* @NamedQuery(name = "ams_asset.findByName", query = "SELECT r FROM AMSAssetDO r where r.status =:status and r.cpuspeed_name=:name"),
    @NamedQuery(name = "ams_asset.findByNameAndId", query = "SELECT r FROM AMSAssetDO r where r.status =:status and r.cpuspeed_name=:name and r.id NOT IN :id"),*/
    
})
public class AMSAssetDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "ams_asset.findAll";
	
	public static final String FIND_BY_ID = "ams_asset.findById";
	
	//public static final String FIND_BY_STATUS = "ams_asset.findByStatus";
	
	public static final String FIND_BY_UNIT = "ams_asset.findByUnit";
	
/*	public static final String FIND_BY_NAME = "ams_asset.findByName";
	
	public static final String FIND_FOR_UPDATE = "ams_asset.findByNameAndId";
	*/
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO) 
	@Column(nullable = false, unique = true) 
	private Long assetId;
	
	private String asset_number;
	private String asset_name;
	@Temporal(TemporalType.DATE)
	private Date purchaseDate;
	@Temporal(TemporalType.DATE)
	private Date expiry;
	private String monitor;
	private String fdd;
	private String internet;
	private String usb;
	private String ipAddress;
	private char status;
	
	@ManyToOne
	@JoinColumn(name="osId")
	private AMSOSDO OSModel;
	
	@ManyToOne
	@JoinColumn(name="mailId")
	private AMSMailDO mail;
	
	@ManyToOne
	@JoinColumn(name="officeId")
	private AMSOfficeDO office;
	
	@ManyToOne
	@JoinColumn(name="cd_dvdId")
	private AMSCD_DVDDO cdd_dvd;
	
	@ManyToOne
	@JoinColumn(name="hddId")
	private AMSHddDO hdd;
	
	@ManyToOne
	@JoinColumn(name="ramId")
	private AMSRamDO ram;
	
	@ManyToOne
	@JoinColumn(name="cpuSpeedId")
	private AMSCPUSpeedDO cpuSpeed;
	
	@ManyToOne
	@JoinColumn(name="cpuTypeId")
	private AMSCPUTypeDO cpuType;
	
	@ManyToOne
	@JoinColumn(name="makeId")
	private AMSMakeDO make;
	

	@ManyToOne
	@JoinColumn(name="typeId")
	private AMSTypeDO type;
	
	@ManyToOne
	@JoinColumn(name="pcUnderId")
	private AMSPCUnderDO pcUnder;
	
	@ManyToOne
	@JoinColumn(name="domainId")
	private AMSDomainDO domain;
	
	@ManyToOne
	@JoinColumn(name="floorId")
	private AMSFloorDO floor;
	
	@ManyToOne
	@JoinColumn(name="unitId")
	private AMSUnitDO unit;
	
	@ManyToOne
	@JoinColumn(name="departmentId")
	private AMSDepartmentDO department;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getAssetId() {
		return assetId;
	}
	public void setAssetId(Long assetId) {
		this.assetId = assetId;
	}
	public String getAsset_number() {
		return asset_number;
	}
	public void setAsset_number(String asset_number) {
		this.asset_number = asset_number;
	}
	public String getAsset_name() {
		return asset_name;
	}
	public void setAsset_name(String asset_name) {
		this.asset_name = asset_name;
	}
	public Date getPurchaseDate() {
		return purchaseDate;
	}
	public void setPurchaseDate(Date purchaseDate) {
		this.purchaseDate = purchaseDate;
	}
	public Date getExpiry() {
		return expiry;
	}
	public void setExpiry(Date expiry) {
		this.expiry = expiry;
	}
	public String getMonitor() {
		return monitor;
	}
	public void setMonitor(String monitor) {
		this.monitor = monitor;
	}
	public String getFdd() {
		return fdd;
	}
	public void setFdd(String fdd) {
		this.fdd = fdd;
	}
	public String getInternet() {
		return internet;
	}
	public void setInternet(String internet) {
		this.internet = internet;
	}
	public String getUsb() {
		return usb;
	}
	public void setUsb(String usb) {
		this.usb = usb;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public AMSOSDO getOSModel() {
		return OSModel;
	}
	public void setOSModel(AMSOSDO oSModel) {
		OSModel = oSModel;
	}
	public AMSMailDO getMail() {
		return mail;
	}
	public void setMail(AMSMailDO mail) {
		this.mail = mail;
	}
	public AMSOfficeDO getOffice() {
		return office;
	}
	public void setOffice(AMSOfficeDO office) {
		this.office = office;
	}
	public AMSCD_DVDDO getCdd_dvd() {
		return cdd_dvd;
	}
	public void setCdd_dvd(AMSCD_DVDDO cdd_dvd) {
		this.cdd_dvd = cdd_dvd;
	}
	public AMSHddDO getHdd() {
		return hdd;
	}
	public void setHdd(AMSHddDO hdd) {
		this.hdd = hdd;
	}
	public AMSRamDO getRam() {
		return ram;
	}
	public void setRam(AMSRamDO ram) {
		this.ram = ram;
	}
	public AMSCPUSpeedDO getCpuSpeed() {
		return cpuSpeed;
	}
	public void setCpuSpeed(AMSCPUSpeedDO cpuSpeed) {
		this.cpuSpeed = cpuSpeed;
	}
	public AMSCPUTypeDO getCpuType() {
		return cpuType;
	}
	public void setCpuType(AMSCPUTypeDO cpuType) {
		this.cpuType = cpuType;
	}
	public AMSMakeDO getMake() {
		return make;
	}
	public void setMake(AMSMakeDO make) {
		this.make = make;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public AMSTypeDO getType() {
		return type;
	}
	public void setType(AMSTypeDO type) {
		this.type = type;
	}
	public AMSPCUnderDO getPcUnder() {
		return pcUnder;
	}
	public void setPcUnder(AMSPCUnderDO pcUnder) {
		this.pcUnder = pcUnder;
	}
	public AMSDomainDO getDomain() {
		return domain;
	}
	public void setDomain(AMSDomainDO domain) {
		this.domain = domain;
	}
	public AMSFloorDO getFloor() {
		return floor;
	}
	public void setFloor(AMSFloorDO floor) {
		this.floor = floor;
	}
	public AMSUnitDO getUnit() {
		return unit;
	}
	public void setUnit(AMSUnitDO unit) {
		this.unit = unit;
	}
	public AMSDepartmentDO getDepartment() {
		return department;
	}
	public void setDepartment(AMSDepartmentDO department) {
		this.department = department;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}