package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="gms_groupUnitlog")
@TableGenerator(name ="gms_groupUnitlog", initialValue =100101, allocationSize =1)
@NamedQueries({
	/*@NamedQuery(name = "gms_visitorlog.findByMobileNoAndStatus", query = "SELECT r FROM VisitorLogDO r where r.status =:status and r.mobile=:mobile"),
	@NamedQuery(name = "gms_visitorlog.findByMobileNo", query = "SELECT r FROM VisitorLogDO r where r.mobile=:mobile"),*/
	@NamedQuery(name = "gms_groupUnitlog.findById", query = "SELECT r FROM GroupUnitLogDO r where r.id =:id"),
})
public class GroupUnitLogDO implements Serializable {
	private static final long serialVersionUID = 1L;
	/*public static final String FIND_BY_MOBILENOANDSTATUS = "gms_visitorlog.findByMobileNoAndStatus";
	public static final String FIND_BY_MOBILENO = "gms_visitorlog.findByMobileNo";*/
	public static final String FIND_BY_ID = "gms_groupUnitlog.findById";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private String name;
	private String mobile;
	private String vehicleNo;
	private String unit;
	private String dept;
	private String purpose;
	private String personToMeet;
	@Temporal(TemporalType.TIMESTAMP)
	private Date enteredOn;
	@Temporal(TemporalType.TIMESTAMP)
	private Date exitOn;
	private char status;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private String updatedBy;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getVehicleNo() {
		return vehicleNo;
	}
	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getDept() {
		return dept;
	}
	public void setDept(String dept) {
		this.dept = dept;
	}
	public String getPurpose() {
		return purpose;
	}
	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}
	public String getPersonToMeet() {
		return personToMeet;
	}
	public void setPersonToMeet(String personToMeet) {
		this.personToMeet = personToMeet;
	}
	public Date getEnteredOn() {
		return enteredOn;
	}
	public void setEnteredOn(Date enteredOn) {
		this.enteredOn = enteredOn;
	}
	public Date getExitOn() {
		return exitOn;
	}
	public void setExitOn(Date exitOn) {
		this.exitOn = exitOn;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	
}