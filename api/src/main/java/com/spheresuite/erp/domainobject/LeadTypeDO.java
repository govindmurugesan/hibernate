package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="crm_master_leadtype")
@TableGenerator(name ="crm_master_leadtype", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "crm_master_leadtype.findById", query = "SELECT r FROM LeadTypeDO r where r.leadTypeId =:id"),
    @NamedQuery(name = "crm_master_leadtype.findByStatus", query = "SELECT r FROM LeadTypeDO r where r.status =:status"),
    @NamedQuery(name = "crm_master_leadtype.findByName", query = "SELECT r FROM LeadTypeDO r where r.name=:name"),
    @NamedQuery(name = "crm_master_leadtype.findByNameAndId", query = "SELECT r FROM LeadTypeDO r where r.name=:name and r.leadTypeId NOT IN :id"),
    @NamedQuery(name = "crm_master_leadtype.findByActiveName", query = "SELECT r FROM LeadTypeDO r where r.status =:status and r.name=:name"),
})
public class LeadTypeDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static final String FIND_BY_ID = "crm_master_leadtype.findById";
	
	public static final String FIND_BY_STATUS = "crm_master_leadtype.findByStatus";
	public static final String FIND_BY_NAME = "crm_master_leadtype.findByName";
	
	public static final String FIND_FOR_UPDATE = "crm_master_leadtype.findByNameAndId";
	
	public static final String FIND_BY_ACTIVE_NAME = "crm_master_leadtype.findByActiveName";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long leadTypeId;
	
	private String name;
	private char status;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getLeadTypeId() {
		return leadTypeId;
	}
	public void setLeadTypeId(Long leadTypeId) {
		this.leadTypeId = leadTypeId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}