package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="ams_master_make")
@TableGenerator(name ="ams_master_make", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "ams_master_make.findAll", query = "SELECT r FROM AMSMakeDO r"),
    @NamedQuery(name = "ams_master_make.findById", query = "SELECT r FROM AMSMakeDO r where r.makeId =:id"),
    @NamedQuery(name = "ams_master_make.findByStatus", query = "SELECT r FROM AMSMakeDO r where r.status =:status"),
    @NamedQuery(name = "ams_master_make.findByName", query = "SELECT r FROM AMSMakeDO r where r.status =:status and r.make_name=:name"),
   @NamedQuery(name = "ams_master_make.findByNameAndId", query = "SELECT r FROM AMSMakeDO r where r.status =:status and r.make_name=:name and r.makeId NOT IN :id"),
})
public class AMSMakeDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "ams_master_make.findAll";
	public static final String FIND_BY_ID = "ams_master_make.findById";
	public static final String FIND_BY_STATUS = "ams_master_make.findByStatus";
	public static final String FIND_BY_NAME = "ams_master_make.findByName";
	public static final String FIND_FOR_UPDATE = "ams_master_make.findByNameAndId";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO) 
	private Long makeId;
	
	private String make_name;
	private String make_comments;
	private char status;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
	public Long getMakeId() {
		return makeId;
	}
	public void setMakeId(Long makeId) {
		this.makeId = makeId;
	}
	public String getMake_name() {
		return make_name;
	}
	public void setMake_name(String make_name) {
		this.make_name = make_name;
	}
	public String getMake_comments() {
		return make_comments;
	}
	public void setMake_comments(String make_comments) {
		this.make_comments = make_comments;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}