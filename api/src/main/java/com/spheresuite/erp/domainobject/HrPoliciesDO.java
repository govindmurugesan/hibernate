package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_hrpolicies")
@TableGenerator(name ="hrms_hrpolicies", initialValue =100101, allocationSize =1)
@NamedQueries({
	 @NamedQuery(name = "hrms_hrpolicies.findAll", query = "SELECT r FROM HrPoliciesDO r ORDER BY r.hrPoliceId DESC"),
    @NamedQuery(name = "hrms_hrpolicies.findById", query = "SELECT r FROM HrPoliciesDO r where r.hrPoliceId =:id"),
    @NamedQuery(name = "hrms_hrpolicies.findByRoleId", query = "SELECT r FROM HrPoliciesDO r where (r.role.roleId =:id or r.role.roleId IS NULL) ORDER BY r.hrPoliceId DESC"),
})
public class HrPoliciesDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL= "hrms_hrpolicies.findAll";
	
	public static final String FIND_BY_ID = "hrms_hrpolicies.findById";
	
	public static final String FIND_BY_ROLEID = "hrms_hrpolicies.findByRoleId";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long hrPoliceId;
	
	@Column(columnDefinition="LONGTEXT")
	private String hrPolice;
	private char status;
	private String comment;
	
	@ManyToOne
	@JoinColumn(name="roleId")
	private RolesDO role;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getHrPoliceId() {
		return hrPoliceId;
	}
	public void setHrPoliceId(Long hrPoliceId) {
		this.hrPoliceId = hrPoliceId;
	}
	public String getHrPolice() {
		return hrPolice;
	}
	public void setHrPolice(String hrPolice) {
		this.hrPolice = hrPolice;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public RolesDO getRole() {
		return role;
	}
	public void setRole(RolesDO role) {
		this.role = role;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}