package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_employeepayroll")
@TableGenerator(name ="hrms_employeepayroll", initialValue =100101, allocationSize =1)
@NamedQueries({
	  @NamedQuery(name = "hrms_employeepayroll.retriveByMonthAndEmp", query = "SELECT r FROM EmployeePayrollDO r where r.payrollMonth =:monthly and r.employee.empId =:empId"),
	  @NamedQuery(name = "hrms_employeepayroll.retriveByWeekAndEmp", query = "SELECT r FROM EmployeePayrollDO r where r.payrollWeekFrom =:fromDate and r.payrollWeekTo =:toDate and r.employee.empId =:empId"),
	  @NamedQuery(name = "hrms_employeepayroll.findByEmpIdBetweenDateSum", query = "SELECT sum(r.netmonth), sum(r.earningmonth), sum(r.deductionmonth), sum(r.taxMonth) FROM EmployeePayrollDO r where r.employee.empId =:empId and (r.payrollMonth >= :fromMonth or r.payrollMonth <= :toMonth) and r.status =:status"),
	  @NamedQuery(name = "hrms_employeepayroll.findByEmpIdBetweenDate", query = "SELECT r FROM EmployeePayrollDO r where r.employee.empId =:empId and (r.payrollMonth >= :fromMonth or r.payrollMonth <= :toMonth) and r.status =:status"),
  
	  @NamedQuery(name = "hrms_employeepayroll.findByEmpIdDateSum", query = "SELECT sum(r.netmonth), sum(r.earningmonth), sum(r.deductionmonth), sum(r.taxMonth) FROM EmployeePayrollDO r where r.employee.empId =:empId and r.payrollMonth =:fromMonth and r.status =:status"),
	  
	  @NamedQuery(name = "hrms_employeepayroll.findByMonthAndBatch", query = "SELECT r FROM EmployeePayrollDO r where r.payrollMonth =:monthly and r.payrollBatch.payrollBatchId =:payrollBatchId"),
	  @NamedQuery(name = "hrms_employeepayroll.findForProcess", query = "SELECT r FROM EmployeePayrollDO r where r.payrollMonth =:monthly and r.payrollBatch.payrollBatchId =:payrollBatchId and r.status =:status"),
	  @NamedQuery(name = "hrms_employeepayroll.findForWeeklyProcess", query = "SELECT r FROM EmployeePayrollDO r where  r.payrollWeekFrom =:fromDate and r.payrollWeekTo =:toDate and r.payrollBatch.payrollBatchId =:payrollBatchId and r.status =:status"),
	  
	  @NamedQuery(name = "hrms_employeepayroll.findForDailyProcess", query = "SELECT r FROM EmployeePayrollDO r where  r.payrollDate =:date and r.payrollBatch.payrollBatchId =:payrollBatchId and r.status =:status"),
	  @NamedQuery(name = "hrms_employeepayroll.findById", query = "SELECT r FROM EmployeePayrollDO r where r.payrollId =:id"),
	  
	  @NamedQuery(name = "hrms_employeepayroll.retriveForPayrollSummary", query = "SELECT count(r),r.payrollMonth,sum(r.netmonth),sum(r.earningmonth),sum(r.deductionmonth),sum(r.grossPay),sum(r.taxMonth) FROM EmployeePayrollDO r group by r.payrollMonth"),
	  
	  @NamedQuery(name = "hrms_employeepayroll.retriveForPayrollSummaryByBatchId", query = "SELECT count(r),r.payrollMonth,r.payrollBatch.payrollBatchId,sum(r.netmonth),sum(r.earningmonth),sum(r.deductionmonth),sum(r.grossPay),r.status, r.updatedon, r.updatedby,sum(r.taxMonth) FROM EmployeePayrollDO r where r.payrollMonth=:monthly group by r.payrollMonth,r.payrollBatch.payrollBatchId"),
	  
	  @NamedQuery(name = "hrms_employeepayroll.retriveForWeeklyPayrollSummaryByBatchId", query = "SELECT count(r),r.payrollMonth,r.payrollBatch.payrollBatchId,sum(r.netmonth),sum(r.earningmonth),sum(r.deductionmonth),sum(r.grossPay),r.status, r.updatedon, r.updatedby,r.payrollWeekFrom, r.payrollWeekTo, sum(r.taxMonth) FROM EmployeePayrollDO r where r.payrollMonth=:monthly group by r.payrollWeekFrom"),
	  
	  @NamedQuery(name = "hrms_employeepayroll.findAllEmp", query = "SELECT r FROM EmployeePayrollDO r where r.payrollMonth =:payrollMonth and r.payrollBatch.payrollBatchId =:payrollBatchId and r.status=:status"),
	  
	  @NamedQuery(name = "hrms_employeepayroll.findByEmpIdAndStatus", query = "SELECT r FROM EmployeePayrollDO r where r.employee.empId =:empId and r.status =:status"),
	  
	  @NamedQuery(name = "hrms_employeepayroll.findDailyPayroll", query = "SELECT r FROM EmployeePayrollDO r where r.employee.empId =:empId and r.payrollBatch.payrollBatchId =:payrollBatchId and r.payrollDate =:date "),
	  
	  @NamedQuery(name = "hrms_employeepayroll.findByPayrollMonth", query = "SELECT r FROM EmployeePayrollDO r where r.payrollMonth =:payrollMonth and r.status=:status"),
	  
	  @NamedQuery(name = "hrms_employeepayroll.findByPayrollMonthEmpId", query = "SELECT r FROM EmployeePayrollDO r where r.employee.empId =:empId and r.payrollMonth =:payrollMonth and r.status=:status"),
	  
	  
	  //  @NamedQuery(name = "hrms_employeepayroll.retriveForPayrollSummaryByBatchId", query = "SELECT count(r),r.payrollMonth,r.payrollBatch.payrollBatchId,sum(r.netmonth),sum(r.earningmonth),sum(r.deductionmonth),sum(r.grossPay),r.status, r.updatedon, r.updatedBy,sum(r.taxMonth) FROM EmployeePayrollDO r where r.payrollMonth=:monthly group by r.payrollMonth,r.payrollBatch.payrollBatchId,r.status, r.updatedon, r.updatedBy"),
	  
	  
	  /* @NamedQuery(name = "employeepayroll.findAll", query = "SELECT r FROM EmployeePayrollDO r where r.status =:status"),
    @NamedQuery(name = "employeepayroll.findById", query = "SELECT r FROM EmployeePayrollDO r where r.id =:id"),
    @NamedQuery(name = "employeepayroll.findByStatus", query = "SELECT r FROM EmployeePayrollDO r where r.status =:status and r.empId =:empId"),
    @NamedQuery(name = "employeepayroll.findByEmpId", query = "SELECT r FROM EmployeePayrollDO r where r.empId =:empId and r.status =:status"),
    @NamedQuery(name = "employeepayroll.findByMonth", query = "SELECT r FROM EmployeePayrollDO r where r.payrollMonth =:monthly and r.status =:status and r.payrollbatchId =:payrollBatchId"),
  
   // @NamedQuery(name = "employeepayroll.retriveForPayrollSummary", query = "SELECT count(r),r.payrollMonth,sum(r.netmonth),sum(r.earningmonth),sum(r.deductionmonth),sum(r.grossPay),r.status, r.updatedon, r.updatedBy,sum(r.taxMonth) FROM EmployeePayrollDO r group by r.payrollMonth, r.status, r.updatedon, r.updatedBy"),
    @NamedQuery(name = "employeepayroll.retriveForPayrollSummary", query = "SELECT count(r),r.payrollMonth,sum(r.netmonth),sum(r.earningmonth),sum(r.deductionmonth),sum(r.grossPay),sum(r.taxMonth) FROM EmployeePayrollDO r group by r.payrollMonth"),
    @NamedQuery(name = "employeepayroll.retriveForPayrollSummaryByBatchId", query = "SELECT count(r),r.payrollMonth,r.payrollbatchId,sum(r.netmonth),sum(r.earningmonth),sum(r.deductionmonth),sum(r.grossPay),r.status, r.updatedon, r.updatedBy,sum(r.taxMonth) FROM EmployeePayrollDO r where r.payrollMonth=:monthly group by r.payrollMonth,r.payrollbatchId,r.status, r.updatedon, r.updatedBy"),
    @NamedQuery(name = "employeepayroll.findAllEmp", query = "SELECT r FROM EmployeePayrollDO r where r.payrollMonth =:payrollMonth and r.payrollbatchId =:payrollBatchId and r.status=:status"),
  
     @NamedQuery(name = "employeepayroll.retriveByMonthForReport", query = "SELECT r FROM EmployeePayrollDO r where r.payrollMonth =:monthly"),
    @NamedQuery(name = "employeepayroll.retriveByMonthForReportBatch", query = "SELECT r FROM EmployeePayrollDO r where r.payrollMonth =:monthly and r.payrollbatchId =:payrollBatchId"),
    @NamedQuery(name = "employeepayroll.findByEmpIdBetweenDateForReportBatch", query = "SELECT r FROM EmployeePayrollDO r where (r.payrollMonth >= :fromMonth or r.payrollMonth <= :toMonth) and r.payrollbatchId =:payrollBatchId"),
    @NamedQuery(name = "employeepayroll.findByEmpIdBetweenDateForReport", query = "SELECT r FROM EmployeePayrollDO r where (r.payrollMonth >= :fromMonth or r.payrollMonth <= :toMonth)"),
*/
})

public class EmployeePayrollDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static final String FIND_BY_MONTH_EMP = "hrms_employeepayroll.retriveByMonthAndEmp";
	
	public static final String FIND_BY_WEEK_EMP = "hrms_employeepayroll.retriveByWeekAndEmp";
	
	public static final String FIND_BY_EMP_BEWTEENDATE_SUM = "hrms_employeepayroll.findByEmpIdBetweenDateSum";
	
	public static final String FIND_BY_EMP_BEWTEENDATE = "hrms_employeepayroll.findByEmpIdBetweenDate";
	
	public static final String FIND_BY_EMP_DATE_SUM = "hrms_employeepayroll.findByEmpIdDateSum";
	
	public static final String FIND_BY_MONTH_BATCh = "hrms_employeepayroll.findByMonthAndBatch";
	
	public static final String FIND_FOR_PROCESS  = "hrms_employeepayroll.findForProcess";
	
	public static final String FIND_WEEKLY_FOR_PROCESS = "hrms_employeepayroll.findForWeeklyProcess";
	
	public static final String FIND_DAILY_FOR_PROCESS = "hrms_employeepayroll.findForDailyProcess";
	
	
	
	public static final String FIND_BY_ID = "hrms_employeepayroll.findById";
	
	public static final String FIND_FOR_SUMMARY = "hrms_employeepayroll.retriveForPayrollSummary";
	
	public static final String FIND_FOR_SUMMARY_BY_BATCHID = "hrms_employeepayroll.retriveForPayrollSummaryByBatchId";
	
	public static final String FIND_ALL_EMP = "hrms_employeepayroll.findAllEmp";
	
	public static final String FIND_BY_EMP_STATUS = "hrms_employeepayroll.findByEmpIdAndStatus";
	
	public static final String FIND_BY_WEEKLY_PAYROLLSUMMARY_BATCHID = "hrms_employeepayroll.retriveForWeeklyPayrollSummaryByBatchId";
	
	public static final String FIND_DAILY_PAYROLL = "hrms_employeepayroll.findDailyPayroll";
	
	public static final String FIND_BY_PAYROLLMONTH = "hrms_employeepayroll.findByPayrollMonth";
	
	public static final String FIND_BY_PAYROLLMONTH_EMPID = "hrms_employeepayroll.findByPayrollMonthEmpId";
	
	
	
	
	
	/*private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "employeepayroll.findAll";
	

	
	public static final String FIND_BY_STATUS = "employeepayroll.findByStatus";
	
	public static final String FIND_BY_EMP_ID = "employeepayroll.findByEmpId";
	
	
	

	

	
	public static final String FIND_BY_MONTH = "employeepayroll.findByMonth";
	

	
	public static final String FIND_FOR_SUMMARY = "employeepayroll.retriveForPayrollSummary";
	
	public static final String FIND_ALL_EMP = "employeepayroll.findAllEmp";
	
	public static final String FIND_FOR_REPORT="employeepayroll.retriveByMonthForReport";
	
	public static final String FIND_FOR_REPORT_BATCH="employeepayroll.retriveByMonthForReportBatch";
	
	public static final String FIND_FOR_REPORT_BEWTEENDATE_BATCH="employeepayroll.findByEmpIdBetweenDateForReportBatch";
	
	public static final String FIND_FOR_REPORT_BEWTEENDATE="employeepayroll.findByEmpIdBetweenDateForReport";
	
	public static final String FIND_FOR_SUMMARY_BY_BATCHID = "employeepayroll.retriveForPayrollSummaryByBatchId";*/
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO) 
	@Column(nullable = false, unique = true) 
	private Long payrollId;
	
	private String payrollMonth;
	private Double netmonth;
	private Double earningmonth;
	private Double deductionmonth;
	private char status;
	private Double grossPay;
	private Double taxMonth;
	private String payrollWeekFrom;
	private String payrollWeekTo;
	private String payrollDate;
	

	@ManyToOne
	@JoinColumn(name="empId")
	private EmployeeDO employee;
	
	@ManyToOne
	@JoinColumn(name="payrollBatchId")
	private PayrollBatchDO payrollBatch;
	
	@ManyToOne
	@JoinColumn(name="payrollGroupId")
	private PayrollGroupDO payrollGroup;
	
	@ManyToOne
	@JoinColumn(name="employeeCompensationId")
	private EmployeeCompensationDO employeeCompensation;
	
	
	
	@OneToMany(mappedBy="payroll", fetch = FetchType.EAGER)
    private Set<PayrollEarningsDetailsDO> payrollEarning;
	
	@OneToMany(mappedBy="payroll", fetch = FetchType.EAGER)
    private Set<PayrollDeductionDetailsDO> payrollDeduction;
	
	@OneToMany(mappedBy="payroll", fetch = FetchType.EAGER)
    private Set<PayrollTaxDetailsDO> payrollTax;
	
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getPayrollId() {
		return payrollId;
	}
	public void setPayrollId(Long payrollId) {
		this.payrollId = payrollId;
	}
	public String getPayrollMonth() {
		return payrollMonth;
	}
	public void setPayrollMonth(String payrollMonth) {
		this.payrollMonth = payrollMonth;
	}
	public Double getNetmonth() {
		return netmonth;
	}
	public void setNetmonth(Double netmonth) {
		this.netmonth = netmonth;
	}
	public Double getEarningmonth() {
		return earningmonth;
	}
	public void setEarningmonth(Double earningmonth) {
		this.earningmonth = earningmonth;
	}
	public Double getDeductionmonth() {
		return deductionmonth;
	}
	public void setDeductionmonth(Double deductionmonth) {
		this.deductionmonth = deductionmonth;
	}
	public String getPayrollWeekFrom() {
		return payrollWeekFrom;
	}
	public void setPayrollWeekFrom(String payrollWeekFrom) {
		this.payrollWeekFrom = payrollWeekFrom;
	}
	public String getPayrollWeekTo() {
		return payrollWeekTo;
	}
	public void setPayrollWeekTo(String payrollWeekTo) {
		this.payrollWeekTo = payrollWeekTo;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getPayrollDate() {
		return payrollDate;
	}
	public void setPayrollDate(String payrollDate) {
		this.payrollDate = payrollDate;
	}
	public Double getGrossPay() {
		return grossPay;
	}
	public void setGrossPay(Double grossPay) {
		this.grossPay = grossPay;
	}
	public Double getTaxMonth() {
		return taxMonth;
	}
	public void setTaxMonth(Double taxMonth) {
		this.taxMonth = taxMonth;
	}
	public EmployeeDO getEmployee() {
		return employee;
	}
	public void setEmployee(EmployeeDO employee) {
		this.employee = employee;
	}
	public PayrollBatchDO getPayrollBatch() {
		return payrollBatch;
	}
	public void setPayrollBatch(PayrollBatchDO payrollBatch) {
		this.payrollBatch = payrollBatch;
	}
	public PayrollGroupDO getPayrollGroup() {
		return payrollGroup;
	}
	public void setPayrollGroup(PayrollGroupDO payrollGroup) {
		this.payrollGroup = payrollGroup;
	}
	public EmployeeCompensationDO getEmployeeCompensation() {
		return employeeCompensation;
	}
	public void setEmployeeCompensation(EmployeeCompensationDO employeeCompensation) {
		this.employeeCompensation = employeeCompensation;
	}
	public Set<PayrollEarningsDetailsDO> getPayrollEarning() {
		return payrollEarning;
	}
	public void setPayrollEarning(Set<PayrollEarningsDetailsDO> payrollEarning) {
		this.payrollEarning = payrollEarning;
	}
	public Set<PayrollDeductionDetailsDO> getPayrollDeduction() {
		return payrollDeduction;
	}
	public void setPayrollDeduction(Set<PayrollDeductionDetailsDO> payrollDeduction) {
		this.payrollDeduction = payrollDeduction;
	}
	public Set<PayrollTaxDetailsDO> getPayrollTax() {
		return payrollTax;
	}
	public void setPayrollTax(Set<PayrollTaxDetailsDO> payrollTax) {
		this.payrollTax = payrollTax;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}