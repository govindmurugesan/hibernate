package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="employee")
@TableGenerator(name ="employee", initialValue =100101, allocationSize =1)
@NamedQueries({
	@NamedQuery(name = "employee.findAll", query = "SELECT r FROM EmployeeDO r ORDER BY r.empId DESC"),
	@NamedQuery(name = "employee.findLastId", query = "SELECT r FROM EmployeeDO r ORDER BY r.createdon DESC"),
	@NamedQuery(name = "employee.findEmpNameId", query = "SELECT r.empId, r.firstname,r.middlename,r.lastname FROM EmployeeDO r where r.status =:status"),
	@NamedQuery(name = "employee.findAllSpecificField", query = "SELECT r.empId,r.firstname,r.lastname,r.middlename,r.jobdesc,r.companyemail"
			+ ",r.primarycontact,r.employeeType.name,r.status FROM EmployeeDO r ORDER BY r.empId DESC"),
    @NamedQuery(name = "employee.findActive", query = "SELECT r FROM EmployeeDO r where r.status=:status order by r.id desc"),
    //@NamedQuery(name = "employee.findIsActive", query = "SELECT r FROM EmployeeDO r where r.status=:status and r.empId =:id"),
   // @NamedQuery(name = "employee.findById", query = "SELECT r FROM EmployeeDO r where r.empId =:id"),
    @NamedQuery(name = "employee.findEmpId", query = "SELECT r FROM EmployeeDO r where r.empId =:id"),
    @NamedQuery(name = "employee.findEmpIdOnlyNames", query = "SELECT r.firstname,r.middlename,r.lastname FROM EmployeeDO r where r.empId =:id"),
    @NamedQuery(name = "employee.findByEmail", query = "SELECT r FROM EmployeeDO r where r.companyemail =:email or r.personalemail =:email"),
    //@NamedQuery(name = "employee.getCount", query = "SELECT COUNT(r) FROM EmployeeDO r"),
   // @NamedQuery(name = "employee.checkDuplicateInsert", query = "SELECT r FROM EmployeeDO r where r.companyemail =:email or r.empId =:empId and r.status=:status"),
    @NamedQuery(name = "employee.checkDuplicateUpdate", query = "SELECT r FROM EmployeeDO r where r.companyemail =:email and r.empId NOT IN :id"),
    //@NamedQuery(name = "employee.checkDuplicateUpdateNoEmpId", query = "SELECT r FROM EmployeeDO r where r.companyemail =:email and r.empId NOT IN :id and r.status=:status"),
    //@NamedQuery(name = "employee.checkDuplicateInsertNoEmpId", query = "SELECT r FROM EmployeeDO r where r.companyemail =:email and r.status=:status"),
    //@NamedQuery(name = "employee.findByDept", query = "SELECT r.empId FROM EmployeeDO r where r.department.departmentId IN :id and r.status=:status"),
    @NamedQuery(name = "employee.findEmployeeNoCtc", query = "SELECT r FROM EmployeeDO r where r.status=:status and r.empId NOT IN :id"),
   // @NamedQuery(name = "employee.findempId", query = "SELECT r FROM EmployeeDO r where r.empId =:id"),
    @NamedQuery(name = "employee.findByDept", query = "SELECT r.empId FROM EmployeeDO r where r.department.departmentId IN :id and r.status=:status"),
	@NamedQuery(name = "employee.findByEmail_EmpId", query = "SELECT r FROM EmployeeDO r where r.empId =:id"),
	@NamedQuery(name = "employee.findByReportTo", query = "SELECT r FROM EmployeeDO r where r.reportto =:reportTo and r.status =:status"),
	@NamedQuery(name = "employee.findByEmpFullName", query = "SELECT r FROM EmployeeDO r where r.firstname =:firstName and r.lastname =:lastName and  r.middlename =:middleName"),
	@NamedQuery(name = "employee.findByUnit", query = "SELECT r.empId,r.firstname,r.middlename,r.lastname FROM EmployeeDO r where r.unitOrBranch.unitOrBranchId =:id and r.status =:status")
})
public class EmployeeDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static final String FIND_ACTIVE = "employee.findActive";
	public static final String FIND_BY_UNIT = "employee.findByUnit";
	public static final String FIND_ONLY_EMPNAME_ID ="employee.findEmpNameId";
	
	public static final String FIND_ALL = "employee.findAll";
	public static final String FIND_LAST_ID = "employee.findLastId";
	public static final String FIND_ALL_SPECIFICFIELD = "employee.findAllSpecificField";
	
	
	public static final String FIND_IS_ACTIVE = "employee.findIsActive";
	
	public static final String FIND_BY_DEPT = "employee.findByDept";
	
	//public static final String FIND_BY_ID = "employee.findById";

	public static final String FIND_BY_EMAIL = "employee.findByEmail";
	
	//public static final String FIND_EMP_ID = "employee.findEmpId";
	
//	public static final String FIND_DUPLICATE_INSERT = "employee.checkDuplicateInsert";
	
	public static final String FIND_DUPLICATE_UPDATE = "employee.checkDuplicateUpdate";
	
	public static final String FIND_DUPLICATE_UPDATE_NOEMP = "employee.checkDuplicateUpdateNoEmpId";
	
	public static final String FIND_DUPLICATE_NOEMP = "employee.checkDuplicateInsertNoEmpId";
	
	public static final String FIND_EMP_NOCTC = "employee.findEmployeeNoCtc";
	
	public static final String FIND_EMPID = "employee.findEmpId";
	
	public static final String FIND_EMPID_ONLYNAMES = "employee.findEmpIdOnlyNames";
	
	public static final String FIND_BY_EMAIL_EMPID = "employee.findByEmail_EmpId";
	
	public static final String FIND_EMPFUllNAME = "employee.findByEmpFullName";
	
	public static final String FIND_BY_REPORT_TO = "employee.findByReportTo";
	
    @Id
	@Column(nullable = false, unique = true)
	private String empId;
	private String firstname;
	private String lastname;
	private String middlename;
	private String personalcontact;
	private String primarycontact;
	private String companyemail;
	private String personalemail;
	private String aadhar;
	private String panno;
	private String dateofbirth;
	private String reportto;
	//private String title;
	private String jobdesc;
	private String startdate;
	private char gender;
	@Column(columnDefinition="LONGTEXT")
	private String photo;
	private String emailId;
	private String emailPassword;
	private String emailType;
	private char status;
	
	@ManyToOne
	@JoinColumn(name="unitOrBranchId")
	private UnitOrBranchDO unitOrBranch;
	
	@ManyToOne
	@JoinColumn(name="worklocationId")
	private WorkLocationDO worklocation;
	
	@ManyToOne
	@JoinColumn(name="empTypeId")
	private EmployeeTypeDO employeeType;
	
	@ManyToOne
	@JoinColumn(name="departmentId")
	private DepartmentDO department;
	
	@ManyToOne
	@JoinColumn(name="designationId")
	private DesignationDO designation;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public String getEmpId() {
		return empId;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getMiddlename() {
		return middlename;
	}
	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}
	public String getPersonalcontact() {
		return personalcontact;
	}
	public void setPersonalcontact(String personalcontact) {
		this.personalcontact = personalcontact;
	}
	public String getPrimarycontact() {
		return primarycontact;
	}
	public void setPrimarycontact(String primarycontact) {
		this.primarycontact = primarycontact;
	}
	public String getCompanyemail() {
		return companyemail;
	}
	public void setCompanyemail(String companyemail) {
		this.companyemail = companyemail;
	}
	public String getPersonalemail() {
		return personalemail;
	}
	public void setPersonalemail(String personalemail) {
		this.personalemail = personalemail;
	}
	public String getAadhar() {
		return aadhar;
	}
	public void setAadhar(String aadhar) {
		this.aadhar = aadhar;
	}
	public String getPanno() {
		return panno;
	}
	public void setPanno(String panno) {
		this.panno = panno;
	}
	public String getDateofbirth() {
		return dateofbirth;
	}
	public void setDateofbirth(String dateofbirth) {
		this.dateofbirth = dateofbirth;
	}
	public String getReportto() {
		return reportto;
	}
	public void setReportto(String reportto) {
		this.reportto = reportto;
	}
	public DesignationDO getDesignation() {
		return designation;
	}
	public void setDesignation(DesignationDO designation) {
		this.designation = designation;
	}
	public String getJobdesc() {
		return jobdesc;
	}
	public void setJobdesc(String jobdesc) {
		this.jobdesc = jobdesc;
	}
	public String getStartdate() {
		return startdate;
	}
	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}
	public char getGender() {
		return gender;
	}
	public void setGender(char gender) {
		this.gender = gender;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getEmailPassword() {
		return emailPassword;
	}
	public void setEmailPassword(String emailPassword) {
		this.emailPassword = emailPassword;
	}
	public String getEmailType() {
		return emailType;
	}
	public void setEmailType(String emailType) {
		this.emailType = emailType;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public UnitOrBranchDO getUnitOrBranch() {
		return unitOrBranch;
	}
	public void setUnitOrBranch(UnitOrBranchDO unitOrBranch) {
		this.unitOrBranch = unitOrBranch;
	}
	public EmployeeTypeDO getEmployeeType() {
		return employeeType;
	}
	public void setEmployeeType(EmployeeTypeDO employeeType) {
		this.employeeType = employeeType;
	}
	public DepartmentDO getDepartment() {
		return department;
	}
	public void setDepartment(DepartmentDO department) {
		this.department = department;
	}
	public WorkLocationDO getWorklocation() {
		return worklocation;
	}
	public void setWorklocation(WorkLocationDO worklocation) {
		this.worklocation = worklocation;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}