package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name="hrms_deductionsetting")
@TableGenerator(name ="hrms_deductionsetting", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "hrms_deductionsetting.findAll", query = "SELECT r FROM DeductionSettingDO r"),
    @NamedQuery(name = "hrms_deductionsetting.findById", query = "SELECT r FROM DeductionSettingDO r where r.deductionsettingId =:id"),
    @NamedQuery(name = "hrms_deductionsetting.findActive", query = "SELECT r FROM DeductionSettingDO r where r.status =:status and r.name =:name"),
})
public class DeductionSettingDO implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final String FIND_ALL = "hrms_deductionsetting.findAll";
	public static final String FIND_BY_ID = "hrms_deductionsetting.findById";
	public static final String FIND_ACTIVE = "hrms_deductionsetting.findActive";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO) 
	private Long deductionsettingId;
	
	private String name;
	private String section;
	private String preposttax;
	private String type;
	private String fixedamount;
	private String percentageamount;
	private String basicgrosspay;
	private String displayname;
	private String notes;
	@Temporal(TemporalType.DATE)
	private Date startdate;
	@Temporal(TemporalType.DATE)
	private Date enddate;
	private char status;
	@Transient
	private String settingName;
	
	@ManyToOne
	@JoinColumn(name="deductionTypeId")
	private DeductionTypeDO deductionType;
	
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getDeductionsettingId() {
		return deductionsettingId;
	}
	public void setDeductionsettingId(Long deductionsettingId) {
		this.deductionsettingId = deductionsettingId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	public String getPreposttax() {
		return preposttax;
	}
	public void setPreposttax(String preposttax) {
		this.preposttax = preposttax;
	}
	public String getFixedamount() {
		return fixedamount;
	}
	public void setFixedamount(String fixedamount) {
		this.fixedamount = fixedamount;
	}
	public String getPercentageamount() {
		return percentageamount;
	}
	public void setPercentageamount(String percentageamount) {
		this.percentageamount = percentageamount;
	}
	public String getBasicgrosspay() {
		return basicgrosspay;
	}
	public void setBasicgrosspay(String basicgrosspay) {
		this.basicgrosspay = basicgrosspay;
	}
	public String getDisplayname() {
		return displayname;
	}
	public void setDisplayname(String displayname) {
		this.displayname = displayname;
	}
	public String getNotes() {
		return notes;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public Date getStartdate() {
		return startdate;
	}
	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}
	public Date getEnddate() {
		return enddate;
	}
	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getSettingName() {
		return settingName;
	}
	public void setSettingName(String settingName) {
		this.settingName = settingName;
	}
	public DeductionTypeDO getDeductionType() {
		return deductionType;
	}
	public void setDeductionType(DeductionTypeDO deductionType) {
		this.deductionType = deductionType;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	
}