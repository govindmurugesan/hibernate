package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_companyCalender")
@TableGenerator(name ="hrms_companyCalender", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "hrms_companyCalender.findById", query = "SELECT r FROM CompanyCalenderDO r where r.companyCalenderId =:id"),
   // @NamedQuery(name = "hrms_companyCalender.deleteByMonth", query = "SELECT r FROM CompanyCalenderDO r where r.month =:monthly"),
    @NamedQuery(name = "hrms_companyCalender.findByMonth", query = "SELECT r FROM CompanyCalenderDO r where r.month =:monthly and r.payrollBatch.payrollBatchId =:id"),
   // @NamedQuery(name = "hrms_companyCalender.findByStatus", query = "SELECT r FROM companyCalenderDO r where r.status =:status"),
   // @NamedQuery(name = "hrms_companyCalender.findByName", query = "SELECT r FROM companyCalenderDO r where r.name=:name"),
   // @NamedQuery(name = "hrms_companyCalender.findByNameAndId", query = "SELECT r FROM companyCalenderDO r where r.name=:name and r.designationId NOT IN :id"),
})
public class CompanyCalenderDO implements Serializable {
	private static final long serialVersionUID = 1L;

	
	public static final String FIND_BY_ID = "hrms_companyCalender.findById";
	
	//public static final String DELETBYMONTH = "hrms_companyCalender.deleteByMonth";
	
	public static final String FINDBYMONTH = "hrms_companyCalender.findByMonth";
	
//	public static final String FIND_BY_STATUS = "hrms_companyCalender.findByStatus";
//	
	//public static final String FIND_BY_NAME = "hrms_companyCalender.findByName";
	
	//public static final String FIND_FOR_UPDATE = "hrms_companyCalender.findByNameAndId";
	
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true) 
	private Long companyCalenderId;
	
	private String month;
	private String minimunWorkingDays;
	private String noOfSalaryDays;
	@Temporal(TemporalType.DATE)
	private Date date;
	
	@ManyToOne
	@JoinColumn(name="payrollBatchId")
	private PayrollBatchDO payrollBatch;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
	private String updatedby;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	
	@ManyToOne
	@JoinColumn(name="unitOrBranchId")
	private UnitOrBranchDO unitOrBranch;
	
	public Long getCompanyCalenderId() {
		return companyCalenderId;
	}
	public void setCompanyCalenderId(Long companyCalenderId) {
		this.companyCalenderId = companyCalenderId;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getMinimunWorkingDays() {
		return minimunWorkingDays;
	}
	public void setMinimunWorkingDays(String minimunWorkingDays) {
		this.minimunWorkingDays = minimunWorkingDays;
	}
	public String getNoOfSalaryDays() {
		return noOfSalaryDays;
	}
	public void setNoOfSalaryDays(String noOfSalaryDays) {
		this.noOfSalaryDays = noOfSalaryDays;
	}
	public PayrollBatchDO getPayrollBatch() {
		return payrollBatch;
	}
	public void setPayrollBatch(PayrollBatchDO payrollBatch) {
		this.payrollBatch = payrollBatch;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	
	public UnitOrBranchDO getUnitOrBranch() {
		return unitOrBranch;
	}
	public void setUnitOrBranch(UnitOrBranchDO unitOrBranch) {
		this.unitOrBranch = unitOrBranch;
	}
}