package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_payrollgroupDeduction")
@TableGenerator(name ="hrms_payrollgroupDeduction", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "hrms_payrollgroupDeduction.findById", query = "SELECT r FROM PayrollGroupDeductionDO r where r.payrollGroupDeductionId =:id"),
   // @NamedQuery(name = "hrms_payrollgroupDeduction.findByStatus", query = "SELECT r FROM PayrollGroupDO r where r.status =:status"),
   // @NamedQuery(name = "hrms_payrollgroupDeduction.findByName", query = "SELECT r FROM PayrollGroupDO r where r.name=:name"),
   // @NamedQuery(name = "hrms_payrollgroupDeduction.findByNameAndId", query = "SELECT r FROM PayrollGroupDO r where r.name=:name and r.payrollGroupId NOT IN :id"),
	  @NamedQuery(name = "hrms_payrollgroupDeduction.findForAdd", query = "SELECT r FROM PayrollGroupDeductionDO r where r.deductionSettings.deductionsettingId=:deductionId and r.status =:status and r.payrollGroup.payrollGroupId=:payrollGroupId"),
	  @NamedQuery(name = "hrms_payrollgroupDeduction.findByGroupId", query = "SELECT r FROM PayrollGroupDeductionDO r where r.payrollGroup.payrollGroupId =:id and r.status =:status"),
})
public class PayrollGroupDeductionDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_BY_ID = "hrms_payrollgroupDeduction.findById";
	
//	public static final String FIND_BY_STATUS = "hrms_payrollgroupDeduction.findByStatus";
	
	//public static final String FIND_BY_NAME = "hrms_payrollgroupDeduction.findByName";
	
//	public static final String FIND_FOR_UPDATE = "hrms_payrollgroupDeduction.findByNameAndId";
	public static final String FIND_FOR_ADD = "hrms_payrollgroupDeduction.findForAdd";
	
	public static final String FIND_BY_GROUP_ID = "hrms_payrollgroupDeduction.findByGroupId";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long payrollGroupDeductionId;
	
	private char status;
	
	@ManyToOne
	@JoinColumn(name="payrollGroupId")
	private PayrollGroupDO payrollGroup;
	
	@ManyToOne
	@JoinColumn(name="deductionsettingId")
	private DeductionSettingDO deductionSettings;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getPayrollGroupDeductionId() {
		return payrollGroupDeductionId;
	}
	public void setPayrollGroupDeductionId(Long payrollGroupDeductionId) {
		this.payrollGroupDeductionId = payrollGroupDeductionId;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public PayrollGroupDO getPayrollGroup() {
		return payrollGroup;
	}
	public void setPayrollGroup(PayrollGroupDO payrollGroup) {
		this.payrollGroup = payrollGroup;
	}
	public DeductionSettingDO getDeductionSettings() {
		return deductionSettings;
	}
	public void setDeductionSettings(DeductionSettingDO deductionSettings) {
		this.deductionSettings = deductionSettings;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}