package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="ams_master_cpuspeed")
@TableGenerator(name ="ams_master_cpuspeed", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "ams_master_cpuspeed.findAll", query = "SELECT r FROM AMSCPUSpeedDO r"),
    @NamedQuery(name = "ams_master_cpuspeed.findById", query = "SELECT r FROM AMSCPUSpeedDO r where r.cpuSpeedId =:id"),
    @NamedQuery(name = "ams_master_cpuspeed.findByStatus", query = "SELECT r FROM AMSCPUSpeedDO r where r.status =:status"),
    @NamedQuery(name = "ams_master_cpuspeed.findByName", query = "SELECT r FROM AMSCPUSpeedDO r where r.status =:status and r.cpuspeed_name=:name"),
    @NamedQuery(name = "ams_master_cpuspeed.findByNameAndId", query = "SELECT r FROM AMSCPUSpeedDO r where r.status =:status and r.cpuspeed_name=:name and r.cpuSpeedId NOT IN :id"),
    
})
public class AMSCPUSpeedDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "ams_master_cpuspeed.findAll";
	
	public static final String FIND_BY_ID = "ams_master_cpuspeed.findById";
	
	public static final String FIND_BY_STATUS = "ams_master_cpuspeed.findByStatus";
	
	public static final String FIND_IS_MANAGER = "ams_master_cpuspeed.findisManager";
	
	public static final String FIND_BY_NAME = "ams_master_cpuspeed.findByName";
	
	public static final String FIND_FOR_UPDATE = "ams_master_cpuspeed.findByNameAndId";
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO) 
	@Column(nullable = false, unique = true) 
	private Long cpuSpeedId;
	
	private String cpuspeed_name;
	private char status;
	private String cpuspeed_comments;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
	public Long getCpuSpeedId() {
		return cpuSpeedId;
	}
	public void setCpuSpeedId(Long cpuSpeedId) {
		this.cpuSpeedId = cpuSpeedId;
	}
	public String getCpuspeed_name() {
		return cpuspeed_name;
	}
	public void setCpuspeed_name(String cpuspeed_name) {
		this.cpuspeed_name = cpuspeed_name;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getCpuspeed_comments() {
		return cpuspeed_comments;
	}
	public void setCpuspeed_comments(String cpuspeed_comments) {
		this.cpuspeed_comments = cpuspeed_comments;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}