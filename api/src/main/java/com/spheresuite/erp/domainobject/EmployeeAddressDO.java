package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="employeeaddress")
@TableGenerator(name ="employeeaddress", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "employeeaddress.findAll", query = "SELECT r FROM EmployeeAddressDO r"),
    @NamedQuery(name = "employeeaddress.findById", query = "SELECT r FROM EmployeeAddressDO r where r.empAddressInfoId =:id"),
    @NamedQuery(name = "employeeaddress.findByEmpId", query = "SELECT r FROM EmployeeAddressDO r where r.employee.empId =:empId order by r.empAddressInfoId desc"),
})
public class EmployeeAddressDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "employeeaddress.findAll";
	
	public static final String FIND_BY_ID = "employeeaddress.findById";
	
	public static final String FIND_BY_EMPID = "employeeaddress.findByEmpId";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO) 
	private Long empAddressInfoId;
	
	private String address1;
	private String address2;
	private String city;
	private String notes;
	private Long zip;
	
	@ManyToOne
	@JoinColumn(name="empId")
	private EmployeeDO employee;
	
	@ManyToOne
	@JoinColumn(name="addressTypeId")
	private AddressTypeDO addressType;
	
	@ManyToOne
	@JoinColumn(name="countryId")
	private CountryDO country;
	
	@ManyToOne
	@JoinColumn(name="stateId")
	private StateDO state;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getEmpAddressInfoId() {
		return empAddressInfoId;
	}
	public void setEmpAddressInfoId(Long empAddressInfoId) {
		this.empAddressInfoId = empAddressInfoId;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public Long getZip() {
		return zip;
	}
	public void setZip(Long zip) {
		this.zip = zip;
	}
	public EmployeeDO getEmployee() {
		return employee;
	}
	public void setEmployee(EmployeeDO employee) {
		this.employee = employee;
	}
	public AddressTypeDO getAddressType() {
		return addressType;
	}
	public void setAddressType(AddressTypeDO addressType) {
		this.addressType = addressType;
	}
	public CountryDO getCountry() {
		return country;
	}
	public void setCountry(CountryDO country) {
		this.country = country;
	}
	public StateDO getState() {
		return state;
	}
	public void setState(StateDO state) {
		this.state = state;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}