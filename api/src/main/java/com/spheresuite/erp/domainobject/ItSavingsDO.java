package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_itsavings")
@TableGenerator(name ="hrms_itsavings", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "hrms_itsavings.findAll", query = "SELECT r FROM ItSavingsDO r"),
    @NamedQuery(name = "hrms_itsavings.findById", query = "SELECT r FROM ItSavingsDO r where r.itSavingsId =:id"),
    @NamedQuery(name = "hrms_itsavings.findByEmpId", query = "SELECT r FROM ItSavingsDO r where r.employee.empId =:empId"),
    @NamedQuery(name = "hrms_itsavings.findByDate", query = "SELECT r FROM ItSavingsDO r where (r.fromYear =:fromDate and r.toYear =:toDate)"),
    @NamedQuery(name = "hrms_itsavings.findByEmpIdSectionIdAndDate", query = "SELECT r FROM ItSavingsDO r where r.employee.empId =:empId and itSavingsSettings.itSection.itSectionId =:sectionId and (r.fromYear <=:date and r.toYear >=:date)"),
})
public class ItSavingsDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "hrms_itsavings.findAll";
	
	public static final String FIND_BY_ID = "hrms_itsavings.findById";
	
	public static final String FIND_BY_EMPID = "hrms_itsavings.findByEmpId";
	
	public static final String FIND_BY_DATE = "hrms_itsavings.findByDate";
	
	public static final String FIND_BY_EMPID_DATE_SECTIONID = "hrms_itsavings.findByEmpIdSectionIdAndDate";
	
	
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long itSavingsId;
	
	
	@Temporal(TemporalType.DATE)
	private Date fromYear;
	@Temporal(TemporalType.DATE)
	private Date toYear;
	private String comments;
	private Double amount;
	private char status;
	
	@ManyToOne
	@JoinColumn(name="empId")
	private EmployeeDO employee;
	
	@ManyToOne
	@JoinColumn(name="itSavingsSettingsId")
	private ItSavingsSettingsDO itSavingsSettings;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getItSavingsId() {
		return itSavingsId;
	}
	public void setItSavingsId(Long itSavingsId) {
		this.itSavingsId = itSavingsId;
	}
	public Date getFromYear() {
		return fromYear;
	}
	public void setFromYear(Date fromYear) {
		this.fromYear = fromYear;
	}
	public Date getToYear() {
		return toYear;
	}
	public void setToYear(Date toYear) {
		this.toYear = toYear;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public EmployeeDO getEmployee() {
		return employee;
	}
	public void setEmployee(EmployeeDO employee) {
		this.employee = employee;
	}
	public ItSavingsSettingsDO getItSavingsSettings() {
		return itSavingsSettings;
	}
	public void setItSavingsSettings(ItSavingsSettingsDO itSavingsSettings) {
		this.itSavingsSettings = itSavingsSettings;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}