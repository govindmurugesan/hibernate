package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_master_leavemanagement")
@TableGenerator(name ="hrms_master_leavemanagement", initialValue =100101, allocationSize =1)
@NamedQueries({
   // @NamedQuery(name = "master_leavemanagement.findAllByDept ", query = "SELECT r FROM LeaveManagementDO r where r.empId IN :id"),
    @NamedQuery(name = "hrms_master_leavemanagement.findById", query = "SELECT r FROM LeaveManagementDO r where r.leaveManagementId =:id"),
   // @NamedQuery(name = "master_leavemanagement.findByEmpId", query = "SELECT r FROM LeaveManagementDO r where r.empId =:id"),
    @NamedQuery(name = "hrms_master_leavemanagement.findCountByType", query = "SELECT sum(r.numberOfDays)  FROM LeaveManagementDO r where r.leaveType =:type"),
    @NamedQuery(name = "hrms_master_leavemanagement.findActiveById", query = "SELECT r FROM LeaveManagementDO r where r.leaveManagementId =:id and r.status =:status"),
    @NamedQuery(name = "hrms_master_leavemanagement.findActiveByType", query = "SELECT r FROM LeaveManagementDO r where r.leaveType =:type and r.status =:status"),
    @NamedQuery(name = "hrms_master_leavemanagement.findActive", query = "SELECT r FROM LeaveManagementDO r where r.status =:status"),
    @NamedQuery(name = "hrms_master_leavemanagement.findForAdd", query = "SELECT r FROM LeaveManagementDO r where  r.leaveType =:type and ((r.fromDate <= :fromDate and r.toDate >=:toDate) or(r.fromDate >=:fromDate and r.toDate >=:toDate))"),
    @NamedQuery(name = "hrms_master_leavemanagement.findForUpdate", query = "SELECT r FROM LeaveManagementDO r where  r.leaveType =:type and ((r.fromDate <= :fromDate and r.toDate >=:toDate) or (r.fromDate >=:fromDate and r.toDate >=:toDate)) and r.leaveManagementId NOT IN :id"),
    @NamedQuery(name = "hrms_master_leavemanagement.findCountForYear", query = "SELECT sum(r.numberOfDays) FROM LeaveManagementDO r where  r.leaveType =:type and ((r.fromDate >= :fromDate and r.toDate <= :toDate) or (r.fromDate <= :fromDate and r.toDate >= :toDate))"),
   // @NamedQuery(name = "hrms_master_leavemanagement.findByTypeName", query = "SELECT r FROM LeaveManagementDO r where r.leaveType.type =:type"),
                                           
})
public class LeaveManagementDO implements Serializable {
	private static final long serialVersionUID = 1L;

	//public static final String FIND_ALL_BY_DEPT = "master_leavemanagement.findAllByDept";
	
	public static final String FIND_BY_ID = "hrms_master_leavemanagement.findById";
	
	//public static final String FINDBY_EMP_ID = "master_leavemanagement.findByEmpId";
	public static final String FIND_ACTIVE_BY_ID = "hrms_master_leavemanagement.findActiveById";
	
	public static final String FIND_COUNT_BY_TYPE = "hrms_master_leavemanagement.findCountByType";
	
	public static final String FIND_ACTIVE = "hrms_master_leavemanagement.findActive";
	
	public static final String FIND_FOR_ADD = "hrms_master_leavemanagement.findForAdd";
	
	public static final String FIND_FOR_UPDATE = "hrms_master_leavemanagement.findForUpdate";
	
	public static final String FIND_COUNT_FORYEAR = "hrms_master_leavemanagement.findCountForYear";
	
//	public static final String FIND_BY_TYPENAME = "hrms_master_leavemanagement.findByTypeName";
	
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long leaveManagementId;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date fromDate;
	@Temporal(TemporalType.TIMESTAMP)
	private Date toDate;
	private Double numberOfDays;
	private Double daysAllowed;
	private char status;
	private String carryForward;
	private String compoff;
	private String permission;
	private String saturday;
	private String sunday;
	private String leaveType;
	private String earnedLeave;
	private Double multiplyby;
	private Double dividedBy;
	
/*	@ManyToOne
	@JoinColumn(name="leaveTypeId")
	private LeaveTypeDO leaveType;*/
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getLeaveManagementId() {
		return leaveManagementId;
	}
	public void setLeaveManagementId(Long leaveManagementId) {
		this.leaveManagementId = leaveManagementId;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public Double getNumberOfDays() {
		return numberOfDays;
	}
	public void setNumberOfDays(Double numberOfDays) {
		this.numberOfDays = numberOfDays;
	}
	public Double getDaysAllowed() {
		return daysAllowed;
	}
	public void setDaysAllowed(Double daysAllowed) {
		this.daysAllowed = daysAllowed;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getLeaveType() {
		return leaveType;
	}
	public void setLeaveType(String leaveType) {
		this.leaveType = leaveType;
	}
	
	public String getEarnedLeave() {
		return earnedLeave;
	}
	public void setEarnedLeave(String earnedLeave) {
		this.earnedLeave = earnedLeave;
	}
	public Double getMultiplyby() {
		return multiplyby;
	}
	public void setMultiplyby(Double multiplyby) {
		this.multiplyby = multiplyby;
	}
	public Double getDividedBy() {
		return dividedBy;
	}
	public void setDividedBy(Double dividedBy) {
		this.dividedBy = dividedBy;
	}
	/*public LeaveTypeDO getLeaveType() {
		return leaveType;
	}
	public void setLeaveType(LeaveTypeDO leaveType) {
		this.leaveType = leaveType;
	}*/
	public String getCarryForward() {
		return carryForward;
	}
	public void setCarryForward(String carryForward) {
		this.carryForward = carryForward;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public String getCompoff() {
		return compoff;
	}
	public void setCompoff(String compoff) {
		this.compoff = compoff;
	}
	public String getPermission() {
		return permission;
	}
	public void setPermission(String permission) {
		this.permission = permission;
	}
	public String getSaturday() {
		return saturday;
	}
	public void setSaturday(String saturday) {
		this.saturday = saturday;
	}
	public String getSunday() {
		return sunday;
	}
	public void setSunday(String sunday) {
		this.sunday = sunday;
	}
}