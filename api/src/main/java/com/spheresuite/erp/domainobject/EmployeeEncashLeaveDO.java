package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_empencashleave")
@TableGenerator(name ="hrms_empencashleave", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "hrms_empencashleave.findByStatus", query = "SELECT r FROM EmployeeEncashLeaveDO r where r.status=:status"),
    @NamedQuery(name = "hrms_empencashleave.findByEmpStatus", query = "SELECT r FROM EmployeeEncashLeaveDO r where r.status=:status and r.employee.empId=:empId"),
    @NamedQuery(name = "hrms_empencashleave.findLeaveByEmpId", query = "SELECT r.numberofenchashleave FROM EmployeeEncashLeaveDO r where r.employee.empId =:id and r.fromDate =:fromDate and r.toDate =:toDate and r.status =:status"),
})
public class EmployeeEncashLeaveDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static final String FIND_BY_STATUS = "hrms_empencashleave.findByStatus";
	public static final String FIND_BY_EMP_STATUS = "hrms_empencashleave.findByEmpStatus";
	public static final String FIND_LEAVE_BY_EMP_ID = "hrms_empencashleave.findLeaveByEmpId";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long encashleaveid;
	@Temporal(TemporalType.TIMESTAMP)
	private Date fromDate;
	@Temporal(TemporalType.TIMESTAMP)
	private Date toDate;
	private Long numberofenchashleave;
	private char status;
	@ManyToOne
	@JoinColumn(name="empId")
	private EmployeeDO employee;
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
	
    public Long getEncashleaveid() {
		return encashleaveid;
	}
	public void setEncashleaveid(Long encashleaveid) {
		this.encashleaveid = encashleaveid;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public Long getNumberofenchashleave() {
		return numberofenchashleave;
	}
	public void setNumberofenchashleave(Long numberofenchashleave) {
		this.numberofenchashleave = numberofenchashleave;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public EmployeeDO getEmployee() {
		return employee;
	}
	public void setEmployee(EmployeeDO employee) {
		this.employee = employee;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}