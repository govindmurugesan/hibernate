package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="pl_ocncosting")
@TableGenerator(name ="pl_ocncosting", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "pl_ocncosting.findById", query = "SELECT r FROM OCNCostingDO r where r.ocnCostingId =:id"),
    @NamedQuery(name = "pl_ocncosting.findAll", query = "SELECT r FROM OCNCostingDO r"),
    @NamedQuery(name = "pl_ocncosting.findByOcnDetailsId", query = "SELECT r FROM OCNCostingDO r where r.ocnDetails.ocnDetailsId =:id"),
    @NamedQuery(name = "pl_ocncosting.findByOcnId", query = "SELECT r.ocn, ROUND(SUM(r.reqm), 2), ROUND(SUM(r.budgetCost), 2), sum(r.amount), sum(r.qty), (sum(r.amount)/sum(r.qty)) FROM OCNCostingDO r where r.ocn.ocnId =:id"),
})
public class OCNCostingDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static final String FIND_BY_ID = "pl_ocncosting.findById";
	
	public static final String FIND_ALL = "pl_ocncosting.findAll";
	
	public static final String FIND_BY_OCN_DETAILS_ID = "pl_ocncosting.findByOcnDetailsId";
	
	public static final String FIND_BY_OCN_ID = "pl_ocncosting.findByOcnId";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long ocnCostingId;
	
	@ManyToOne
	@JoinColumn(name="fabricTypeId")
	private FabricTypeDO fabricType;
	
	@ManyToOne
	@JoinColumn(name="ocnDetailsId")
	private OCNDetailsDO ocnDetails;
	
	@ManyToOne
	@JoinColumn(name="ocnId")
	private OCNDO ocn;
	
	private Double articleCon;
	private Double valueLoss;
	private Double grCon;
	private Double cuttingLoss;
	private Double muLoss;
	private Double budgetCost;
	private Double reqm;
	private Double qty;
	private Double amount;
	private Long indexValue;
	
	private char status;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getOcnCostingId() {
		return ocnCostingId;
	}
	public void setOcnCostingId(Long ocnCostingId) {
		this.ocnCostingId = ocnCostingId;
	}
	public FabricTypeDO getFabricType() {
		return fabricType;
	}
	public void setFabricType(FabricTypeDO fabricType) {
		this.fabricType = fabricType;
	}
	public OCNDetailsDO getOcnDetails() {
		return ocnDetails;
	}
	public void setOcnDetails(OCNDetailsDO ocnDetails) {
		this.ocnDetails = ocnDetails;
	}
	public OCNDO getOcn() {
		return ocn;
	}
	public void setOcn(OCNDO ocn) {
		this.ocn = ocn;
	}
	public Double getArticleCon() {
		return articleCon;
	}
	public void setArticleCon(Double articleCon) {
		this.articleCon = articleCon;
	}
	public Double getValueLoss() {
		return valueLoss;
	}
	public void setValueLoss(Double valueLoss) {
		this.valueLoss = valueLoss;
	}
	public Double getGrCon() {
		return grCon;
	}
	public void setGrCon(Double grCon) {
		this.grCon = grCon;
	}
	public Double getCuttingLoss() {
		return cuttingLoss;
	}
	public void setCuttingLoss(Double cuttingLoss) {
		this.cuttingLoss = cuttingLoss;
	}
	public Double getMuLoss() {
		return muLoss;
	}
	public void setMuLoss(Double muLoss) {
		this.muLoss = muLoss;
	}
	public Double getBudgetCost() {
		return budgetCost;
	}
	public void setBudgetCost(Double budgetCost) {
		this.budgetCost = budgetCost;
	}
	public Double getReqm() {
		return reqm;
	}
	public void setReqm(Double reqm) {
		this.reqm = reqm;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Long getIndexValue() {
		return indexValue;
	}
	public void setIndexValue(Long indexValue) {
		this.indexValue = indexValue;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
    
}