package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="employeeskills")
@TableGenerator(name ="employeeskills", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "employeeskills.findById", query = "SELECT r FROM EmployeeSkillsDO r where r.empSkillId =:id"),
    @NamedQuery(name = "employeeskills.findByEmpId", query = "SELECT r FROM EmployeeSkillsDO r where r.employee.empId =:empId"),
})
public class EmployeeSkillsDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_BY_ID = "employeeskills.findById";
	
	public static final String FIND_BY_EMPID = "employeeskills.findByEmpId";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long empSkillId;
	
	private String name;
	
	@ManyToOne
	@JoinColumn(name="empId")
	private EmployeeDO employee;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getEmpSkillId() {
		return empSkillId;
	}
	public void setEmpSkillId(Long empSkillId) {
		this.empSkillId = empSkillId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public EmployeeDO getEmployee() {
		return employee;
	}
	public void setEmployee(EmployeeDO employee) {
		this.employee = employee;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}