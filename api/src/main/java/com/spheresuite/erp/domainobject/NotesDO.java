package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="crm_notes")
@TableGenerator(name ="crm_notes", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "crm_notes.findAll", query = "SELECT r FROM NotesDO r"),
    @NamedQuery(name = "crm_notes.findById", query = "SELECT r FROM NotesDO r where r.notesId =:id"),
    @NamedQuery(name = "crm_notes.findByLeadId", query = "SELECT r FROM NotesDO r where r.lead.leadId =:id ORDER BY r.id DESC"),
    @NamedQuery(name = "crm_notes.findByLeadRecentNote", query = "SELECT r FROM NotesDO r where r.lead.leadId =:id ORDER BY r.id DESC "),
    @NamedQuery(name = "crm_notes.findByLeadIdAndNote", query = "SELECT r FROM NotesDO r where r.lead.leadId =:id and note=:note"),
})
public class NotesDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "crm_notes.findAll";
	
	public static final String FIND_BY_ID = "crm_notes.findById";
	
	public static final String FIND_BY_LEAD_ID = "crm_notes.findByLeadId";
	
	public static final String FIND_BY_LEAD_RECENT_NOTE = "crm_notes.findByLeadRecentNote";
	
	public static final String FIND_BY_LEAD_AND_NOTE = "crm_notes.findByLeadIdAndNote";
	
	@Id
	@Column(nullable = false, unique = true)
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long notesId;
	
	@ManyToOne
	@JoinColumn(name="leadId")
	private LeadDO lead;
	//private Long leadId;
	@Column(columnDefinition="LONGTEXT")
	private String note;
	private String updatedBy;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;

	private String empId;
	
	public Long getNotesId() {
		return notesId;
	}

	public void setNotesId(Long notesId) {
		this.notesId = notesId;
	}

	public LeadDO getLead() {
		return lead;
	}

	public void setLead(LeadDO lead) {
		this.lead = lead;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getCreatedby() {
		return createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	public Date getCreatedon() {
		return createdon;
	}

	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}

	public Date getUpdatedon() {
		return updatedon;
	}

	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}

	public String getEmpId() {
		return empId;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
}