package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_master_timesheettype")
@TableGenerator(name ="hrms_master_timesheettype", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "hrms_master_timesheettype.findById", query = "SELECT r FROM TimesheetTypeDO r where r.timesheetTypeId =:id"),
    @NamedQuery(name = "hrms_master_timesheettype.findByName", query = "SELECT r FROM TimesheetTypeDO r where r.name=:name"),
    @NamedQuery(name = "hrms_master_timesheettype.findByNameAndId", query = "SELECT r FROM TimesheetTypeDO r where r.name=:name and r.timesheetTypeId NOT IN :id"),
})
public class TimesheetTypeDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_BY_ID = "hrms_master_timesheettype.findById";
	
	public static final String FIND_BY_STATUS = "hrms_master_timesheettype.findByStatus";
	
	public static final String FIND_BY_NAME = "hrms_master_timesheettype.findByName";
	
	public static final String FIND_FOR_UPDATE = "hrms_master_timesheettype.findByNameAndId";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long timesheetTypeId;
	private String name;
	private char status;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getTimesheetTypeId() {
		return timesheetTypeId;
	}
	public void setTimesheetTypeId(Long timesheetTypeId) {
		this.timesheetTypeId = timesheetTypeId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}