package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrms_master_deductiontype")
@TableGenerator(name ="hrms_master_deductiontype", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "hrms_master_deductiontype.findAll", query = "SELECT r FROM DeductionTypeDO r"),
    @NamedQuery(name = "hrms_master_deductiontype.findById", query = "SELECT r FROM DeductionTypeDO r where r.deductionTypeId =:id"),
    @NamedQuery(name = "hrms_master_deductiontype.findByStatus", query = "SELECT r FROM DeductionTypeDO r where r.status =:status"),
    @NamedQuery(name = "hrms_master_deductiontype.findByName", query = "SELECT r FROM DeductionTypeDO r where r.name=:name"),
    @NamedQuery(name = "hrms_master_deductiontype.findByNameAndId", query = "SELECT r FROM DeductionTypeDO r where r.name=:name and r.deductionTypeId NOT IN :id"),
})
public class DeductionTypeDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static final String FIND_ALL = "hrms_master_deductiontype.findAll";
	
	public static final String FIND_BY_ID = "hrms_master_deductiontype.findById";
	
	public static final String FIND_BY_STATUS = "hrms_master_deductiontype.findByStatus";
	
	public static final String FIND_BY_NAME = "hrms_master_deductiontype.findByName";
	
	public static final String FIND_FOR_UPDATE = "hrms_master_deductiontype.findByNameAndId";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long deductionTypeId;
	private String name;
	private String displayname;
	private char status;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getDeductionTypeId() {
		return deductionTypeId;
	}
	public void setDeductionTypeId(Long deductionTypeId) {
		this.deductionTypeId = deductionTypeId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDisplayname() {
		return displayname;
	}
	public void setDisplayname(String displayname) {
		this.displayname = displayname;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}