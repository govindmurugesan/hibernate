package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="crm_master_gsttaxslab")
@TableGenerator(name ="crm_master_gsttaxslab", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "crm_master_gsttaxslab.findById", query = "SELECT r FROM GSTTaxSlabDO r where r.gstTaxSlabId =:id"),
    @NamedQuery(name = "crm_master_gsttaxslab.findByStatus", query = "SELECT r FROM GSTTaxSlabDO r where r.status =:status"),
    @NamedQuery(name = "crm_master_gsttaxslab.findByName", query = "SELECT r FROM GSTTaxSlabDO r where r.percentage=:name"),
    @NamedQuery(name = "crm_master_gsttaxslab.findByNameAndId", query = "SELECT r FROM GSTTaxSlabDO r where r.percentage=:name and r.gstTaxSlabId NOT IN :id"),
})
public class GSTTaxSlabDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_BY_ID = "crm_master_gsttaxslab.findById";
	
	public static final String FIND_BY_STATUS = "crm_master_gsttaxslab.findByStatus";
	public static final String FIND_BY_NAME = "crm_master_gsttaxslab.findByName";
	public static final String FIND_FOR_UPDATE = "crm_master_gsttaxslab.findByNameAndId";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false, unique = true)
	private Long gstTaxSlabId;
	
	private String percentage;
	private char status;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
    
	public Long getGstTaxSlabId() {
		return gstTaxSlabId;
	}
	public void setGstTaxSlabId(Long gstTaxSlabId) {
		this.gstTaxSlabId = gstTaxSlabId;
	}
	public String getPercentage() {
		return percentage;
	}
	public void setPercentage(String percentage) {
		this.percentage = percentage;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
}