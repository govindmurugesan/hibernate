package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="crm_opportunities")
@TableGenerator(name ="crm_opportunities", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "crm_opportunities.findAll", query = "SELECT r FROM OpportunitiesDO r where r.empId IN :id"),
    @NamedQuery(name = "crm_opportunities.findAllData", query = "SELECT r FROM OpportunitiesDO r"),
    @NamedQuery(name = "crm_opportunities.findById", query = "SELECT r FROM OpportunitiesDO r where r.opportunityId =:id"),
    @NamedQuery(name = "crm_opportunities.findBySalesStageId", query = "SELECT sum(r.amount) FROM OpportunitiesDO r where r.salesStage.salesStageId =:id"),
    @NamedQuery(name = "crm_opportunities.findByCustomer", query = "SELECT r FROM OpportunitiesDO r where r.lead.leadId =:id"),
    @NamedQuery(name = "crm_opportunities.findByContact", query = "SELECT r FROM OpportunitiesDO r where r.contact.contactId =:id"),
    @NamedQuery(name = "crm_opportunities.findSumCustomer", query = "SELECT count(distinct r.lead.leadId) FROM OpportunitiesDO r where r.lead.leadId IN :id"),
})
public class OpportunitiesDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "crm_opportunities.findAll";
	
	public static final String FIND_BY_ID = "crm_opportunities.findById";
	
	public static final String FIND_BY_SALES_STAGE_ID = "crm_opportunities.findBySalesStageId";
	
	public static final String FIND_BY_CUSTOMER = "crm_opportunities.findByCustomer";
	
	public static final String FIND_ALL_DATA = "crm_opportunities.findAllData";
	
	public static final String FIND_BY_CONTACT = "crm_opportunities.findByContact";
	
	public static final String FIND_SUM_CUSTOMER ="crm_opportunities.findSumCustomer";
	
	@Id
	@Column(nullable = false, unique = true)
	@GeneratedValue(strategy=GenerationType.AUTO) 
	private Long opportunityId;
	
	@ManyToOne
	@JoinColumn(name="leadId")
	private LeadDO lead;
	//private Long customerId;
	
	private String projectname;
	private String displayname;
	private String projectcode;
	@Temporal(TemporalType.DATE)
	private Date startdate;
	
	@Temporal(TemporalType.DATE)
	private Date enddate;
	private String projectduration;
	@ManyToOne
	@JoinColumn(name="countryId")
	private CountryDO currencyType;
	private String comment;
	private String updatedby;
	
	@ManyToOne
	@JoinColumn(name="OpportunityTypeId")
	private OpportunityTypeDO projectType;
	//private Long projectType;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private String empId;
	
	@ManyToOne
	@JoinColumn(name="contactId")
	private ContactDO contact;
	//private Long contactId;
	private Long probability;
	private Long amount;
	
	@ManyToOne
	@JoinColumn(name="salesStageId")
	private SalesStageDO salesStage;
	//private Long salesStage;
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
	
	public String getProjectname() {
		return projectname;
	}
	public Long getOpportunityId() {
		return opportunityId;
	}
	public void setOpportunityId(Long opportunityId) {
		this.opportunityId = opportunityId;
	}
	public LeadDO getLead() {
		return lead;
	}
	public void setLead(LeadDO lead) {
		this.lead = lead;
	}
	public OpportunityTypeDO getProjectType() {
		return projectType;
	}
	public void setProjectType(OpportunityTypeDO projectType) {
		this.projectType = projectType;
	}
	public String getEmpId() {
		return empId;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
	public ContactDO getContact() {
		return contact;
	}
	public void setContact(ContactDO contact) {
		this.contact = contact;
	}
	public SalesStageDO getSalesStage() {
		return salesStage;
	}
	public void setSalesStage(SalesStageDO salesStage) {
		this.salesStage = salesStage;
	}
	public void setProjectname(String projectname) {
		this.projectname = projectname;
	}
	public String getDisplayname() {
		return displayname;
	}
	public void setDisplayname(String displayname) {
		this.displayname = displayname;
	}
	public String getProjectcode() {
		return projectcode;
	}
	public void setProjectcode(String projectcode) {
		this.projectcode = projectcode;
	}
	
	public Date getStartdate() {
		return startdate;
	}
	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}
	public Date getEnddate() {
		return enddate;
	}
	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}
	public String getProjectduration() {
		return projectduration;
	}
	public void setProjectduration(String projectduration) {
		this.projectduration = projectduration;
	}
	public CountryDO getCurrencyType() {
		return currencyType;
	}
	public void setCurrencyType(CountryDO currencyType) {
		this.currencyType = currencyType;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}

	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public Long getProbability() {
		return probability;
	}
	public void setProbability(Long probability) {
		this.probability = probability;
	}
	public Long getAmount() {
		return amount;
	}
	public void setAmount(Long amount) {
		this.amount = amount;
	}
}