package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="ams_master_installedPrograms")
@TableGenerator(name ="ams_master_installedPrograms", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "ams_master_installedPrograms.findAll", query = "SELECT r FROM AMSInstalledProgramsDO r"),
    @NamedQuery(name = "ams_master_installedPrograms.findById", query = "SELECT r FROM AMSInstalledProgramsDO r where r.installedProgramId =:id"),
    @NamedQuery(name = "ams_master_installedPrograms.findByStatus", query = "SELECT r FROM AMSInstalledProgramsDO r where r.status =:status"),
    @NamedQuery(name = "ams_master_installedPrograms.findByName", query = "SELECT r FROM AMSInstalledProgramsDO r where r.status =:status and r.program_name=:name"),
    @NamedQuery(name = "ams_master_installedPrograms.findByNameAndId", query = "SELECT r FROM AMSInstalledProgramsDO r where r.status =:status and r.program_name=:name and r.installedProgramId NOT IN :id"),
    
})
public class AMSInstalledProgramsDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "ams_master_installedPrograms.findAll";
	
	public static final String FIND_BY_ID = "ams_master_installedPrograms.findById";
	
	public static final String FIND_BY_STATUS = "ams_master_installedPrograms.findByStatus";
	
	public static final String FIND_IS_MANAGER = "ams_master_installedPrograms.findisManager";
	
	public static final String FIND_BY_NAME = "ams_master_installedPrograms.findByName";
	
	public static final String FIND_FOR_UPDATE = "ams_master_installedPrograms.findByNameAndId";
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO) 
	@Column(nullable = false, unique = true)  
	private Long installedProgramId;
	
	private String program_name;
	private char status;
	private String program_comments;
	
	private String createdby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    private String updatedby;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedon;
	public Long getInstalledProgramId() {
		return installedProgramId;
	}
	public void setInstalledProgramId(Long installedProgramId) {
		this.installedProgramId = installedProgramId;
	}
	public String getProgram_name() {
		return program_name;
	}
	public void setProgram_name(String program_name) {
		this.program_name = program_name;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getProgram_comments() {
		return program_comments;
	}
	public void setProgram_comments(String program_comments) {
		this.program_comments = program_comments;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
    
}