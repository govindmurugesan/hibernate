package com.spheresuite.erp.gms.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.VehicleLogDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class VehicleLogDAO {
	static Logger logger = Logger.getLogger(VehicleLogDAO.class.getName());
	
	@Autowired
	private GenericDAOImpl<VehicleLogDO> dao;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(VehicleLogDO vehicleLogDO){
		try {
				dao.persist(vehicleLogDO);
				return true;
					
		} catch (Exception eException) {
			return true;
		} finally {
		}
	}
	
	
	public List<VehicleLogDO> retrieve(){
		List<VehicleLogDO> vehicleLogDOList = null;
		try {
			vehicleLogDOList = dao.retrieve(VehicleLogDO.class);
		} catch (Exception eException) {
		} finally {
			
		}
		return vehicleLogDOList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<VehicleLogDO> retrieveByVehicleNoAndStatus(String vehicleNo, char status){
		List<VehicleLogDO> vehicleLogList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(VehicleLogDO.FIND_BY_VEHICLENOANDSTATUS)
					.setParameter(CommonConstants.VEHICLENO, vehicleNo)
					.setParameter(CommonConstants.STATUS, status)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return vehicleLogList;
	}
	
	@SuppressWarnings("unchecked")
	public List<VehicleLogDO> retrieveByVehicleNo(String vehicleNo){
		List<VehicleLogDO> vehicleLogList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(VehicleLogDO.FIND_BY_VEHICLENO)
					.setParameter(CommonConstants.VEHICLENO, vehicleNo)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return vehicleLogList;
	}
	
	@SuppressWarnings("unchecked")
	public List<VehicleLogDO> retrieveById(Long id){
		 List<VehicleLogDO> vechicleLogList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(VehicleLogDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return vechicleLogList;
	}
	
	@SuppressWarnings("unchecked")
	public boolean update(VehicleLogDO vehicleLogDO) {
		try {
			dao.merge(vehicleLogDO);
			return true;
			/*List<VehicleLogDO> addressTypeDOList = null;
			addressTypeDOList = this.sessionFactory.getCurrentSession().getNamedQuery(VehicleLogDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, addressTypeDO.getName())
					.setParameter(CommonConstants.ID, addressTypeDO.getId())
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
			if(addressTypeDOList != null && addressTypeDOList.size() > 0){
				return false;
			}else{
				dao.merge(addressTypeDO);
				return true;
			}*/
		} catch (Exception eException) {
			return true;
		} finally {
		}
	}
	/*public List<AddressTypeDO> retrieveActive(){
		List<AddressTypeDO> addressTypeList = null;
		try {
			addressTypeList =  dao.retrieveActive(CommonConstants.ACTIVE, "addresstype.findByStatus", AddressTypeDO.class);
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return addressTypeList;
	}
	
	@SuppressWarnings("unchecked")
	public List<AddressTypeDO> retrieveById(Long id){
		 List<AddressTypeDO> addressTypeList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(AddressTypeDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return addressTypeList;
	}
	
	@SuppressWarnings("unchecked")
	public boolean update(AddressTypeDO addressTypeDO) {
		try {
			List<AddressTypeDO> addressTypeDOList = null;
			addressTypeDOList = this.sessionFactory.getCurrentSession().getNamedQuery(AddressTypeDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, addressTypeDO.getName())
					.setParameter(CommonConstants.ID, addressTypeDO.getId())
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
			if(addressTypeDOList != null && addressTypeDOList.size() > 0){
				return false;
			}else{
				dao.merge(addressTypeDO);
				return true;
			}
		} catch (Exception eException) {
			return true;
		} finally {
		}
	}*/

}
