package com.spheresuite.erp.gms.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.domainobject.VisitorLogDO;
import com.spheresuite.erp.gms.dao.VisitorLogDAO;

@Service
@Transactional
public class VisitorLogService{
	
	@Autowired
	private VisitorLogDAO visitorLogDAO;

	@Transactional
	public boolean persist(VisitorLogDO visitorLogDetails) {
		return visitorLogDAO.persist(visitorLogDetails);
	}
	
	@Transactional
	public List<VisitorLogDO> retrieve() {
		return visitorLogDAO.retrieve();
	}
	
	@Transactional
	public List<VisitorLogDO> retrieveByMobileNoAndStatus(String mobileNo, char status){
		return visitorLogDAO.retrieveByMobileNoAndStatus(mobileNo, status);
	}
	
	@Transactional
	public List<VisitorLogDO> retrieveByMobileNo(String vehicleNo){
		return visitorLogDAO.retrieveByMobileNo(vehicleNo);
	}
	
	@Transactional
	public boolean update(VisitorLogDO vehicleLogDeatil) {
		return visitorLogDAO.update(vehicleLogDeatil);
	}
	
	@Transactional
	public List<VisitorLogDO> retrieveById(Long id){
		return visitorLogDAO.retrieveById(id);
	}
	


}
