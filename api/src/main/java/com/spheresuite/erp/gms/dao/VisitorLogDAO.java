package com.spheresuite.erp.gms.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.VisitorLogDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class VisitorLogDAO {
	static Logger logger = Logger.getLogger(VisitorLogDAO.class.getName());
	
	@Autowired
	private GenericDAOImpl<VisitorLogDO> dao;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(VisitorLogDO visitorLogDO){
		try {
				dao.persist(visitorLogDO);
				return true;
					
		} catch (Exception eException) {
			return true;
		} finally {
		}
	}
	
	
	public List<VisitorLogDO> retrieve(){
		List<VisitorLogDO> visitorLogDOList = null;
		try {
			visitorLogDOList = dao.retrieve(VisitorLogDO.class);
		} catch (Exception eException) {
		} finally {
			
		}
		return visitorLogDOList;
	}
	
	@SuppressWarnings("unchecked")
	public List<VisitorLogDO> retrieveByMobileNoAndStatus(String mobileNo, char status){
		List<VisitorLogDO> visitorLogList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(VisitorLogDO.FIND_BY_MOBILENOANDSTATUS)
					.setParameter(CommonConstants.MOBILE, mobileNo)
					.setParameter(CommonConstants.STATUS, status)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return visitorLogList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<VisitorLogDO> retrieveByMobileNo(String mobile){
		List<VisitorLogDO> visitorLogList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(VisitorLogDO.FIND_BY_MOBILENO)
					.setParameter(CommonConstants.MOBILE, mobile)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return visitorLogList;
	}
	
	@SuppressWarnings("unchecked")
	public List<VisitorLogDO> retrieveById(Long id){
		 List<VisitorLogDO> visitorLogList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(VisitorLogDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return visitorLogList;
	}
	
	@SuppressWarnings("unchecked")
	public boolean update(VisitorLogDO visitorLogDO) {
		try {
			dao.merge(visitorLogDO);
			return true;
			
		} catch (Exception eException) {
			return true;
		} finally {
		}
	}
	
	/*
	
	
	
	
	
	*/
}
