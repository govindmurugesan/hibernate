package com.spheresuite.erp.gms.rs;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.GroupUnitLogDO;
import com.spheresuite.erp.gms.service.GroupUnitLogService;
import com.spheresuite.erp.gms.web.util.GroupUnitLogUtil;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/groupUnitlog")
public class GroupUnitLogRS {

	String validation = null;
	static Logger logger = Logger.getLogger(GroupUnitLogRS.class.getName());
	
	@Autowired
	private GroupUnitLogService grpUnitLogService;
	
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				GroupUnitLogDO groupUnitLogDO = new GroupUnitLogDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
				if (inputJSON != null){
					
					if(inputJSON.get(CommonConstants.VEHICLENO) != null && !inputJSON.get(CommonConstants.VEHICLENO).toString().isEmpty()){
						groupUnitLogDO.setVehicleNo(!inputJSON.get(CommonConstants.VEHICLENO).toString().isEmpty() ? inputJSON.get(CommonConstants.VEHICLENO).toString().replaceAll("\\s+","").toUpperCase() : null);
					}
					if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
						groupUnitLogDO.setName(!inputJSON.get(CommonConstants.NAME).toString().isEmpty() ? inputJSON.get(CommonConstants.NAME).toString() : null);
					}
					if(inputJSON.get(CommonConstants.MOBILE) != null && !inputJSON.get(CommonConstants.MOBILE).toString().isEmpty()){
						groupUnitLogDO.setMobile(!inputJSON.get(CommonConstants.MOBILE).toString().isEmpty() ? inputJSON.get(CommonConstants.MOBILE).toString() : null);
					}
					
					if(inputJSON.get(CommonConstants.UNIT) != null && !inputJSON.get(CommonConstants.UNIT).toString().isEmpty()){
						groupUnitLogDO.setUnit(!inputJSON.get(CommonConstants.UNIT).toString().isEmpty() ? inputJSON.get(CommonConstants.UNIT).toString() : null);
					}
					if(inputJSON.get(CommonConstants.DEPT) != null && !inputJSON.get(CommonConstants.DEPT).toString().isEmpty()){
						groupUnitLogDO.setDept(!inputJSON.get(CommonConstants.DEPT).toString().isEmpty() ? inputJSON.get(CommonConstants.DEPT).toString() : null);
					}
					if(inputJSON.get(CommonConstants.PURPOSE) != null && !inputJSON.get(CommonConstants.PURPOSE).toString().isEmpty()){
						groupUnitLogDO.setPurpose(!inputJSON.get(CommonConstants.PURPOSE).toString().isEmpty() ? inputJSON.get(CommonConstants.PURPOSE).toString() : null);
					}
					
					if(inputJSON.get(CommonConstants.PERSONTOMEET) != null && !inputJSON.get(CommonConstants.PERSONTOMEET).toString().isEmpty()){
						groupUnitLogDO.setPersonToMeet(!inputJSON.get(CommonConstants.PERSONTOMEET).toString().isEmpty() ? inputJSON.get(CommonConstants.PERSONTOMEET).toString() : null);
					}
					
					/*GroupUnitLogDO.setName(!inputJSON.get(CommonConstants.NAME).toString().isEmpty() ? inputJSON.get(CommonConstants.NAME).toString() : null);
					GroupUnitLogDO.setMobile(!inputJSON.get(CommonConstants.MOBILE).toString().isEmpty() ? inputJSON.get(CommonConstants.MOBILE).toString() : null);
					if(inputJSON.get(CommonConstants.PURPOSE) != null && !inputJSON.get(CommonConstants.PURPOSE).toString().isEmpty()){
						GroupUnitLogDO.setPurpose(!inputJSON.get(CommonConstants.PURPOSE).toString().isEmpty() ? inputJSON.get(CommonConstants.PURPOSE).toString() : null);
					}
					if(inputJSON.get(CommonConstants.COMMENT) != null && !inputJSON.get(CommonConstants.COMMENT).toString().isEmpty()){
						GroupUnitLogDO.setComments(!inputJSON.get(CommonConstants.COMMENT).toString().isEmpty() ? inputJSON.get(CommonConstants.COMMENT).toString() : null);
					}*/
					
					
					groupUnitLogDO.setStatus((!inputJSON.get(CommonConstants.STATUS).toString().isEmpty() ? inputJSON.get(CommonConstants.STATUS).toString().charAt(0) : null));
					if(inputJSON.get(CommonConstants.STATUS).toString().charAt(0) == 'e'){
						groupUnitLogDO.setEnteredOn(new Date());
					}else{
						groupUnitLogDO.setExitOn(new Date());
					}
					groupUnitLogDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			groupUnitLogDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 	}
				if(!grpUnitLogService.persist(groupUnitLogDO)){
					return CommonWebUtil.buildErrorResponse("Something went wrong").toString();
				}
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Visitor In created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<GroupUnitLogDO> grpUnitLogList = grpUnitLogService.retrieve();
				Collections.reverse(grpUnitLogList);
				respJSON = GroupUnitLogUtil.getGroupUnitList(grpUnitLogList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	/*
	@RequestMapping(value = "/retrieveByMobileNoAndStatus/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByMobileNoAndStatus(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				List<GroupUnitLogDO> visitorLogList = visitorLogService.retrieveByMobileNoAndStatus(inputJSON.get(CommonConstants.MOBILE).toString(), inputJSON.get(CommonConstants.STATUS).toString().charAt(0));
				respJSON = VisitorLogUtil.getVisitorInList(visitorLogList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	
	@RequestMapping(value = "/retrieveByMobileNo/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByVehicle(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				List<GroupUnitLogDO> visitorLogList = visitorLogService.retrieveByMobileNo(inputJSON.get(CommonConstants.MOBILE).toString());
				if(visitorLogList != null && visitorLogList.size() >0){
					
				}
				respJSON = VisitorLogUtil.getVisitorInList(visitorLogList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	*/
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				GroupUnitLogDO groupUnitLogDO = new GroupUnitLogDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<GroupUnitLogDO> grpUnitLogList = grpUnitLogService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		
			 		groupUnitLogDO = grpUnitLogList.get(0);
			 		

					if(inputJSON.get(CommonConstants.VEHICLENO) != null && !inputJSON.get(CommonConstants.VEHICLENO).toString().isEmpty()){
						groupUnitLogDO.setVehicleNo(!inputJSON.get(CommonConstants.VEHICLENO).toString().isEmpty() ? inputJSON.get(CommonConstants.VEHICLENO).toString().replaceAll("\\s+","").toUpperCase() : null);
					}
					if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
						groupUnitLogDO.setName(!inputJSON.get(CommonConstants.NAME).toString().isEmpty() ? inputJSON.get(CommonConstants.NAME).toString() : null);
					}
					if(inputJSON.get(CommonConstants.MOBILE) != null && !inputJSON.get(CommonConstants.MOBILE).toString().isEmpty()){
						groupUnitLogDO.setMobile(!inputJSON.get(CommonConstants.MOBILE).toString().isEmpty() ? inputJSON.get(CommonConstants.MOBILE).toString() : null);
					}
					
					if(inputJSON.get(CommonConstants.UNIT) != null && !inputJSON.get(CommonConstants.UNIT).toString().isEmpty()){
						groupUnitLogDO.setUnit(!inputJSON.get(CommonConstants.UNIT).toString().isEmpty() ? inputJSON.get(CommonConstants.UNIT).toString() : null);
					}
					if(inputJSON.get(CommonConstants.DEPT) != null && !inputJSON.get(CommonConstants.DEPT).toString().isEmpty()){
						groupUnitLogDO.setDept(!inputJSON.get(CommonConstants.DEPT).toString().isEmpty() ? inputJSON.get(CommonConstants.DEPT).toString() : null);
					}
					if(inputJSON.get(CommonConstants.PURPOSE) != null && !inputJSON.get(CommonConstants.PURPOSE).toString().isEmpty()){
						groupUnitLogDO.setPurpose(!inputJSON.get(CommonConstants.PURPOSE).toString().isEmpty() ? inputJSON.get(CommonConstants.PURPOSE).toString() : null);
					}
					
					if(inputJSON.get(CommonConstants.PERSONTOMEET) != null && !inputJSON.get(CommonConstants.PERSONTOMEET).toString().isEmpty()){
						groupUnitLogDO.setPersonToMeet(!inputJSON.get(CommonConstants.PERSONTOMEET).toString().isEmpty() ? inputJSON.get(CommonConstants.PERSONTOMEET).toString() : null);
					}
					
					
			 		
					if(inputJSON.get(CommonConstants.STATUS).toString().charAt(0) == 'e'){
						groupUnitLogDO.setEnteredOn(new Date());
					}else{
						groupUnitLogDO.setExitOn(new Date());
					}
					groupUnitLogDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			groupUnitLogDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		groupUnitLogDO.setStatus((char) (inputJSON != null ? inputJSON.get(CommonConstants.STATUS).toString().charAt(0) : ""));
			 		//addressTypeService.update(allowanceTypeDO);
			 		if(!grpUnitLogService.update(groupUnitLogDO)){
						return CommonWebUtil.buildErrorResponse("Details Not Updated").toString();
					}
			 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Details Updated Sucessfully");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	
}
