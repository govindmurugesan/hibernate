package com.spheresuite.erp.gms.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.domainobject.GroupUnitLogDO;
import com.spheresuite.erp.gms.dao.GroupUnitLogDAO;

@Service
@Transactional
public class GroupUnitLogService{
	
	@Autowired
	private GroupUnitLogDAO groupUnitLogDAO;

	@Transactional
	public boolean persist(GroupUnitLogDO gpUnitLogDetails) {
		return groupUnitLogDAO.persist(gpUnitLogDetails);
	}
	
	@Transactional
	public List<GroupUnitLogDO> retrieve() {
		return groupUnitLogDAO.retrieve();
	}
	
	/*@Transactional
	public List<GroupUnitLogDO> retrieveByMobileNoAndStatus(String mobileNo, char status){
		return groupUnitLogDAO.retrieveByMobileNoAndStatus(mobileNo, status);
	}
	
	@Transactional
	public List<GroupUnitLogDO> retrieveByMobileNo(String vehicleNo){
		return groupUnitLogDAO.retrieveByMobileNo(vehicleNo);
	}*/
	
	@Transactional
	public boolean update(GroupUnitLogDO vehicleLogDeatil) {
		return groupUnitLogDAO.update(vehicleLogDeatil);
	}
	
	@Transactional
	public List<GroupUnitLogDO> retrieveById(Long id){
		return groupUnitLogDAO.retrieveById(id);
	}
	


}
