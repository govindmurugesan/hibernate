package com.spheresuite.erp.gms.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.GroupUnitLogDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class GroupUnitLogDAO {
	static Logger logger = Logger.getLogger(GroupUnitLogDAO.class.getName());
	
	@Autowired
	private GenericDAOImpl<GroupUnitLogDO> dao;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(GroupUnitLogDO groupUnitLogDO){
		try {
				dao.persist(groupUnitLogDO);
				return true;
					
		} catch (Exception eException) {
			return true;
		} finally {
		}
	}
	
	
	public List<GroupUnitLogDO> retrieve(){
		List<GroupUnitLogDO> groupUnitLogDOList = null;
		try {
			groupUnitLogDOList = dao.retrieve(GroupUnitLogDO.class);
		} catch (Exception eException) {
		} finally {
			
		}
		return groupUnitLogDOList;
	}
	
	/*@SuppressWarnings("unchecked")
	public List<GroupUnitLogDO> retrieveByMobileNoAndStatus(String mobileNo, char status){
		List<GroupUnitLogDO> visitorLogList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(GroupUnitLogDO.FIND_BY_MOBILENOANDSTATUS)
					.setParameter(CommonConstants.MOBILE, mobileNo)
					.setParameter(CommonConstants.STATUS, status)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return visitorLogList;
	}*/
	
	
	/*@SuppressWarnings("unchecked")
	public List<GroupUnitLogDO> retrieveByMobileNo(String mobile){
		List<GroupUnitLogDO> visitorLogList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(GroupUnitLogDO.FIND_BY_MOBILENO)
					.setParameter(CommonConstants.MOBILE, mobile)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return visitorLogList;
	}*/
	
	@SuppressWarnings("unchecked")
	public List<GroupUnitLogDO> retrieveById(Long id){
		 List<GroupUnitLogDO> groupUnitLogDOList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(GroupUnitLogDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return groupUnitLogDOList;
	}
	
	@SuppressWarnings("unchecked")
	public boolean update(GroupUnitLogDO groupUnitLogDO) {
		try {
			dao.merge(groupUnitLogDO);
			return true;
			
		} catch (Exception eException) {
			return true;
		} finally {
		}
	}
	
	/*
	
	
	
	
	
	*/
}
