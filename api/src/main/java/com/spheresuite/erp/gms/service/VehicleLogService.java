package com.spheresuite.erp.gms.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.domainobject.VehicleLogDO;
import com.spheresuite.erp.gms.dao.VehicleLogDAO;

@Service
@Transactional
public class VehicleLogService{
	
	@Autowired
	private VehicleLogDAO vehicleLogDAO;

	@Transactional
	public boolean persist(VehicleLogDO vehicleLogDetails) {
		return vehicleLogDAO.persist(vehicleLogDetails);
	}
	
	@Transactional
	public List<VehicleLogDO> retrieve() {
		return vehicleLogDAO.retrieve();
	}
	
	@Transactional
	public List<VehicleLogDO> retrieveByVehicleNoAndStatus(String vehicleNo, char status){
		return vehicleLogDAO.retrieveByVehicleNoAndStatus(vehicleNo, status);
	}
	
	@Transactional
	public List<VehicleLogDO> retrieveByVehicleNo(String vehicleNo){
		return vehicleLogDAO.retrieveByVehicleNo(vehicleNo);
	}
	
	@Transactional
	public boolean update(VehicleLogDO vehicleLogDeatil) {
		return vehicleLogDAO.update(vehicleLogDeatil);
	}
	
	@Transactional
	public List<VehicleLogDO> retrieveById(Long id){
		return vehicleLogDAO.retrieveById(id);
	}
	
	/*@Transactional
	public boolean update(FloorDO employee) {
		return floorDAO.update(employee);
	}
	
	@Transactional
	public List<FloorDO> retrieveActive(){
		return floorDAO.retrieveActive();
	}
	
	@Transactional
	public List<FloorDO> retrieveById(Long id){
		return floorDAO.retrieveById(id);
	}
	*/
	
	
	/*@Autowired
	private EmployeeDAO employeeDAO;

	@Override
	@Transactional
	public void addEmployee(Employee employee) {
		employeeDAO.addEmployee(employee);
	}

	@Override
	@Transactional
	public List<Employee> getAllEmployees() {
		return employeeDAO.getAllEmployees();
	}

	@Override
	@Transactional
	public void deleteEmployee(Integer employeeId) {
		employeeDAO.deleteEmployee(employeeId);
	}

	public Employee getEmployee(int empid) {
		return employeeDAO.getEmployee(empid);
	}

	public Employee updateEmployee(Employee employee) {
		// TODO Auto-generated method stub
		return employeeDAO.updateEmployee(employee);
	}

	public void setEmployeeDAO(EmployeeDAO employeeDAO) {
		this.employeeDAO = employeeDAO;
	}*/


}
