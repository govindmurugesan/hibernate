package com.spheresuite.erp.gms.rs;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.VehicleLogDO;
import com.spheresuite.erp.gms.service.VehicleLogService;
import com.spheresuite.erp.gms.web.util.VehicleLogUtil;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/vechiclelog")
public class VehicleLogRS {

	String validation = null;
	static Logger logger = Logger.getLogger(VehicleLogRS.class.getName());
	
	@Autowired
	private VehicleLogService vehicleLogService;
	
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				VehicleLogDO vehicleLogDO = new VehicleLogDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
				if (inputJSON != null){
					
					
					if(inputJSON.get(CommonConstants.VEHICLENO) != null && !inputJSON.get(CommonConstants.VEHICLENO).toString().isEmpty()){
						vehicleLogDO.setVehicleNo(!inputJSON.get(CommonConstants.VEHICLENO).toString().isEmpty() ? inputJSON.get(CommonConstants.VEHICLENO).toString().replaceAll("\\s+","").toUpperCase() : null);
					}
					if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
						vehicleLogDO.setDriverName(!inputJSON.get(CommonConstants.NAME).toString().isEmpty() ? inputJSON.get(CommonConstants.NAME).toString() : null);
					}
					if(inputJSON.get(CommonConstants.MOBILE) != null && !inputJSON.get(CommonConstants.MOBILE).toString().isEmpty()){
						vehicleLogDO.setMobile(!inputJSON.get(CommonConstants.MOBILE).toString().isEmpty() ? inputJSON.get(CommonConstants.MOBILE).toString() : null);
					}
					if(inputJSON.get(CommonConstants.UNITNAME) != null && !inputJSON.get(CommonConstants.UNITNAME).toString().isEmpty()){
						vehicleLogDO.setUnitName(!inputJSON.get(CommonConstants.UNITNAME).toString().isEmpty() ? inputJSON.get(CommonConstants.UNITNAME).toString() : null);
					}
					
					if(inputJSON.get(CommonConstants.COMPANY) != null && !inputJSON.get(CommonConstants.COMPANY).toString().isEmpty()){
						vehicleLogDO.setCompany(!inputJSON.get(CommonConstants.COMPANY).toString().isEmpty() ? inputJSON.get(CommonConstants.COMPANY).toString() : null);
					}
					
					if(inputJSON.get(CommonConstants.ADDRESS) != null && !inputJSON.get(CommonConstants.ADDRESS).toString().isEmpty()){
						vehicleLogDO.setAddress(!inputJSON.get(CommonConstants.ADDRESS).toString().isEmpty() ? inputJSON.get(CommonConstants.ADDRESS).toString() : null);
					}
					if(inputJSON.get(CommonConstants.CITY) != null && !inputJSON.get(CommonConstants.CITY).toString().isEmpty()){
						vehicleLogDO.setCity(!inputJSON.get(CommonConstants.CITY).toString().isEmpty() ? inputJSON.get(CommonConstants.CITY).toString() : null);
					}
					if(inputJSON.get(CommonConstants.STATE) != null && !inputJSON.get(CommonConstants.STATE).toString().isEmpty()){
						vehicleLogDO.setState(!inputJSON.get(CommonConstants.STATE).toString().isEmpty() ? inputJSON.get(CommonConstants.STATE).toString() : null);
					}
					if(inputJSON.get(CommonConstants.IDPROOF) != null && !inputJSON.get(CommonConstants.IDPROOF).toString().isEmpty()){
						vehicleLogDO.setIdProf(!inputJSON.get(CommonConstants.IDPROOF).toString().isEmpty() ? inputJSON.get(CommonConstants.IDPROOF).toString() : null);
					}
					
					if(inputJSON.get(CommonConstants.PURPOSE) != null && !inputJSON.get(CommonConstants.PURPOSE).toString().isEmpty()){
						vehicleLogDO.setPurpose(!inputJSON.get(CommonConstants.PURPOSE).toString().isEmpty() ? inputJSON.get(CommonConstants.PURPOSE).toString() : null);
					}
					
					if(inputJSON.get(CommonConstants.PONO) != null && !inputJSON.get(CommonConstants.PONO).toString().isEmpty()){
						vehicleLogDO.setPoNo(!inputJSON.get(CommonConstants.PONO).toString().isEmpty() ? inputJSON.get(CommonConstants.PONO).toString() : null);
					}
					
					
					
					/*vehicleLogDO.setVehicleNo(!inputJSON.get(CommonConstants.VEHICLENO).toString().isEmpty() ? inputJSON.get(CommonConstants.VEHICLENO).toString().replaceAll("\\s+","").toLowerCase() : null);
					vehicleLogDO.setDriverName(!inputJSON.get(CommonConstants.NAME).toString().isEmpty() ? inputJSON.get(CommonConstants.NAME).toString() : null);
					vehicleLogDO.setMobile(!inputJSON.get(CommonConstants.MOBILE).toString().isEmpty() ? inputJSON.get(CommonConstants.MOBILE).toString() : null);
					vehicleLogDO.setUnitName(!inputJSON.get(CommonConstants.UNITNAME).toString().isEmpty() ? inputJSON.get(CommonConstants.UNITNAME).toString() : null);*/
					/*if(inputJSON.get(CommonConstants.GOODSIN) != null && !inputJSON.get(CommonConstants.GOODSIN).toString().isEmpty()){
						vehicleLogDO.setGoodsIn(!inputJSON.get(CommonConstants.GOODSIN).toString().isEmpty() ? inputJSON.get(CommonConstants.GOODSIN).toString() : null);
					}
					if(inputJSON.get(CommonConstants.GOODSINCOMMENT) != null && !inputJSON.get(CommonConstants.GOODSINCOMMENT).toString().isEmpty()){
						vehicleLogDO.setGoodsInComments(!inputJSON.get(CommonConstants.GOODSINCOMMENT).toString().isEmpty() ? inputJSON.get(CommonConstants.GOODSINCOMMENT).toString() : null);
					}
					if(inputJSON.get(CommonConstants.GOODSOUT) != null && !inputJSON.get(CommonConstants.GOODSOUT).toString().isEmpty()){
						vehicleLogDO.setGoodsOut(!inputJSON.get(CommonConstants.GOODSOUT).toString().isEmpty() ? inputJSON.get(CommonConstants.GOODSOUT).toString() : null);
			 		}
					if(inputJSON.get(CommonConstants.GOODSOUTCOMMENT) != null && !inputJSON.get(CommonConstants.GOODSOUTCOMMENT).toString().isEmpty()){
						vehicleLogDO.setGoodsOutComments(!inputJSON.get(CommonConstants.GOODSOUTCOMMENT).toString().isEmpty() ? inputJSON.get(CommonConstants.GOODSOUTCOMMENT).toString() : null);
			 		}*/
					vehicleLogDO.setStatus((!inputJSON.get(CommonConstants.STATUS).toString().isEmpty() ? inputJSON.get(CommonConstants.STATUS).toString().charAt(0) : null));
					if(inputJSON.get(CommonConstants.STATUS).toString().charAt(0) == 'e'){
						vehicleLogDO.setEnteredOn(new Date());
					}else{
						vehicleLogDO.setExitOn(new Date());
					}
					vehicleLogDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			vehicleLogDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 	}
				//addressTypeService.persist(allowanceTypeDO);
				if(!vehicleLogService.persist(vehicleLogDO)){
					return CommonWebUtil.buildErrorResponse("Vehicle Details Not Added").toString();
				}
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Vehicle Details Updated");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<VehicleLogDO> vehicleLogList = vehicleLogService.retrieve();
				Collections.reverse(vehicleLogList);
				respJSON = VehicleLogUtil.getGoodsInList(vehicleLogList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	@RequestMapping(value = "/retrieveByVehicleNoAndStatus/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByVehicleNoAndStatus(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				List<VehicleLogDO> vehicleLogList = vehicleLogService.retrieveByVehicleNoAndStatus(inputJSON.get(CommonConstants.VEHICLENO).toString().replaceAll("\\s+","").toLowerCase(), inputJSON.get(CommonConstants.STATUS).toString().charAt(0));
				respJSON = VehicleLogUtil.getGoodsInList(vehicleLogList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveByVehicleNo/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByVehicle(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				List<VehicleLogDO> vehicleLogList = vehicleLogService.retrieveByVehicleNo(inputJSON.get(CommonConstants.VEHICLENO).toString().replaceAll("\\s+","").toLowerCase());
				respJSON = VehicleLogUtil.getGoodsInList(vehicleLogList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				VehicleLogDO vehicleLogDO = new VehicleLogDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<VehicleLogDO> vechileLogList = vehicleLogService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		vehicleLogDO = vechileLogList.get(0);
			 		
			 		
			 		
			 		if(inputJSON.get(CommonConstants.VEHICLENO) != null && !inputJSON.get(CommonConstants.VEHICLENO).toString().isEmpty()){
						vehicleLogDO.setVehicleNo(!inputJSON.get(CommonConstants.VEHICLENO).toString().isEmpty() ? inputJSON.get(CommonConstants.VEHICLENO).toString().replaceAll("\\s+","").toUpperCase() : null);
					}
					if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
						vehicleLogDO.setDriverName(!inputJSON.get(CommonConstants.NAME).toString().isEmpty() ? inputJSON.get(CommonConstants.NAME).toString() : null);
					}
					if(inputJSON.get(CommonConstants.MOBILE) != null && !inputJSON.get(CommonConstants.MOBILE).toString().isEmpty()){
						vehicleLogDO.setMobile(!inputJSON.get(CommonConstants.MOBILE).toString().isEmpty() ? inputJSON.get(CommonConstants.MOBILE).toString() : null);
					}
					if(inputJSON.get(CommonConstants.UNITNAME) != null && !inputJSON.get(CommonConstants.UNITNAME).toString().isEmpty()){
						vehicleLogDO.setUnitName(!inputJSON.get(CommonConstants.UNITNAME).toString().isEmpty() ? inputJSON.get(CommonConstants.UNITNAME).toString() : null);
					}
					
					if(inputJSON.get(CommonConstants.COMPANY) != null && !inputJSON.get(CommonConstants.COMPANY).toString().isEmpty()){
						vehicleLogDO.setCompany(!inputJSON.get(CommonConstants.COMPANY).toString().isEmpty() ? inputJSON.get(CommonConstants.COMPANY).toString() : null);
					}
					
					if(inputJSON.get(CommonConstants.ADDRESS) != null && !inputJSON.get(CommonConstants.ADDRESS).toString().isEmpty()){
						vehicleLogDO.setAddress(!inputJSON.get(CommonConstants.ADDRESS).toString().isEmpty() ? inputJSON.get(CommonConstants.ADDRESS).toString() : null);
					}
					if(inputJSON.get(CommonConstants.CITY) != null && !inputJSON.get(CommonConstants.CITY).toString().isEmpty()){
						vehicleLogDO.setCity(!inputJSON.get(CommonConstants.CITY).toString().isEmpty() ? inputJSON.get(CommonConstants.CITY).toString() : null);
					}
					if(inputJSON.get(CommonConstants.STATE) != null && !inputJSON.get(CommonConstants.STATE).toString().isEmpty()){
						vehicleLogDO.setState(!inputJSON.get(CommonConstants.STATE).toString().isEmpty() ? inputJSON.get(CommonConstants.STATE).toString() : null);
					}
					if(inputJSON.get(CommonConstants.IDPROOF) != null && !inputJSON.get(CommonConstants.IDPROOF).toString().isEmpty()){
						vehicleLogDO.setIdProf(!inputJSON.get(CommonConstants.IDPROOF).toString().isEmpty() ? inputJSON.get(CommonConstants.IDPROOF).toString() : null);
					}
					
					if(inputJSON.get(CommonConstants.PURPOSE) != null && !inputJSON.get(CommonConstants.PURPOSE).toString().isEmpty()){
						vehicleLogDO.setPurpose(!inputJSON.get(CommonConstants.PURPOSE).toString().isEmpty() ? inputJSON.get(CommonConstants.PURPOSE).toString() : null);
					}
					
					if(inputJSON.get(CommonConstants.PONO) != null && !inputJSON.get(CommonConstants.PONO).toString().isEmpty()){
						vehicleLogDO.setPoNo(!inputJSON.get(CommonConstants.PONO).toString().isEmpty() ? inputJSON.get(CommonConstants.PONO).toString() : null);
					}
					
					
			 		
			 		
			 		
			 		/*vehicleLogDO.setDriverName(!inputJSON.get(CommonConstants.NAME).toString().isEmpty() ? inputJSON.get(CommonConstants.NAME).toString() : null);
			 		vehicleLogDO.setVehicleNo(!inputJSON.get(CommonConstants.VEHICLENO).toString().isEmpty() ? inputJSON.get(CommonConstants.VEHICLENO).toString() : null);
					vehicleLogDO.setMobile(!inputJSON.get(CommonConstants.MOBILE).toString().isEmpty() ? inputJSON.get(CommonConstants.MOBILE).toString() : null);
					vehicleLogDO.setUnitName(!inputJSON.get(CommonConstants.UNITNAME).toString().isEmpty() ? inputJSON.get(CommonConstants.UNITNAME).toString() : null);
					if(inputJSON.get(CommonConstants.GOODSIN) != null && !inputJSON.get(CommonConstants.GOODSIN).toString().isEmpty()){
						vehicleLogDO.setGoodsIn(!inputJSON.get(CommonConstants.GOODSIN).toString().isEmpty() ? inputJSON.get(CommonConstants.GOODSIN).toString() : null);
					}
					if(inputJSON.get(CommonConstants.GOODSINCOMMENT) != null && !inputJSON.get(CommonConstants.GOODSINCOMMENT).toString().isEmpty()){
						vehicleLogDO.setGoodsInComments(!inputJSON.get(CommonConstants.GOODSINCOMMENT).toString().isEmpty() ? inputJSON.get(CommonConstants.GOODSINCOMMENT).toString() : null);
					}
					if(inputJSON.get(CommonConstants.GOODSOUT) != null && !inputJSON.get(CommonConstants.GOODSOUT).toString().isEmpty()){
						vehicleLogDO.setGoodsOut(!inputJSON.get(CommonConstants.GOODSOUT).toString().isEmpty() ? inputJSON.get(CommonConstants.GOODSOUT).toString() : null);
			 		}
					if(inputJSON.get(CommonConstants.GOODSOUTCOMMENT) != null && !inputJSON.get(CommonConstants.GOODSOUTCOMMENT).toString().isEmpty()){
						vehicleLogDO.setGoodsOutComments(!inputJSON.get(CommonConstants.GOODSOUTCOMMENT).toString().isEmpty() ? inputJSON.get(CommonConstants.GOODSOUTCOMMENT).toString() : null);
			 		}*/
					
					
					
					
					
					
					vehicleLogDO.setStatus((!inputJSON.get(CommonConstants.STATUS).toString().isEmpty() ? inputJSON.get(CommonConstants.STATUS).toString().charAt(0) : null));
					
					if(inputJSON.get(CommonConstants.STATUS).toString().charAt(0) == 'e'){
						vehicleLogDO.setEnteredOn(new Date());
					}else{
						vehicleLogDO.setExitOn(new Date());
					}
			 		vehicleLogDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			vehicleLogDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		vehicleLogDO.setStatus((char) (inputJSON != null ? inputJSON.get(CommonConstants.STATUS).toString().charAt(0) : ""));
			 		//addressTypeService.update(allowanceTypeDO);
			 		if(!vehicleLogService.update(vehicleLogDO)){
						return CommonWebUtil.buildErrorResponse("Details Not Updated").toString();
					}
			 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Details Updated Sucessfully");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
}
