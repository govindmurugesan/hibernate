package com.spheresuite.erp.gms.web.util;

import java.util.List;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.VehicleLogDO;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;

@Component
public class VehicleLogUtil {
	
	private VehicleLogUtil() {}
	
	@Autowired
	private EmployeeService temployeeService;
	
	private static EmployeeService employeeService;
	
	@PostConstruct
	public void init() {
		employeeService = temployeeService;
	}
	
	public static JSONObject getGoodsInList(List<VehicleLogDO> goodsInList){
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (VehicleLogDO goodsInDO : goodsInList) {
				resultJSONArray.put(getGoodsInObject(goodsInDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getGoodsInObject(VehicleLogDO goodsInDO)throws JSONException{
		
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(goodsInDO.getId()) != null ? String.valueOf(goodsInDO.getId()) : "");
		result.put(CommonConstants.VEHICLENO, String.valueOf(goodsInDO.getVehicleNo()) != null ? String.valueOf(goodsInDO.getVehicleNo()) : "");
		result.put(CommonConstants.NAME, String.valueOf(goodsInDO.getDriverName()) != null ? String.valueOf(goodsInDO.getDriverName()) : "" );
		result.put(CommonConstants.MOBILE, String.valueOf(goodsInDO.getMobile()) != null ? String.valueOf(goodsInDO.getMobile()) : "");
		result.put(CommonConstants.UNITNAME, String.valueOf(goodsInDO.getUnitName()) != null ? String.valueOf(goodsInDO.getUnitName()) : "");
		
		result.put(CommonConstants.COMPANY, String.valueOf(goodsInDO.getCompany()) != null ? String.valueOf(goodsInDO.getCompany()) : "");
		result.put(CommonConstants.ADDRESS, String.valueOf(goodsInDO.getAddress()) != null ? String.valueOf(goodsInDO.getAddress()) : "");
		result.put(CommonConstants.CITY, String.valueOf(goodsInDO.getCity()) != null ? String.valueOf(goodsInDO.getCity()) : "");
		result.put(CommonConstants.STATE, String.valueOf(goodsInDO.getState()) != null ? String.valueOf(goodsInDO.getState()) : "");
		result.put(CommonConstants.IDPROOF, String.valueOf(goodsInDO.getIdProf()) != null ? String.valueOf(goodsInDO.getIdProf()) : "");
		result.put(CommonConstants.PURPOSE, String.valueOf(goodsInDO.getPurpose()) != null ? String.valueOf(goodsInDO.getPurpose()) : "");
		result.put(CommonConstants.PONO, String.valueOf(goodsInDO.getPoNo()) != null ? String.valueOf(goodsInDO.getPoNo()) : "");
		
		
		/*result.put(CommonConstants.GOODSIN, String.valueOf(goodsInDO.getGoodsIn()) != null ? String.valueOf(goodsInDO.getGoodsIn()) : "");
		result.put(CommonConstants.GOODSINCOMMENT, String.valueOf(goodsInDO.getGoodsInComments()) != null ? String.valueOf(goodsInDO.getGoodsInComments()) : "");
		result.put(CommonConstants.GOODSOUT, String.valueOf(goodsInDO.getGoodsOut()) != null ? String.valueOf(goodsInDO.getGoodsOut()) : "");
		result.put(CommonConstants.GOODSOUTCOMMENT, String.valueOf(goodsInDO.getGoodsOutComments()) != null ? String.valueOf(goodsInDO.getGoodsOutComments()) : "");*/
		
		
		
		
		result.put(CommonConstants.EXITON, String.valueOf(goodsInDO.getExitOn()) != null ? String.valueOf(CommonUtil.convertDateToStringWithtime(goodsInDO.getExitOn())) : "");
		result.put(CommonConstants.ENTEREDON, String.valueOf(goodsInDO.getEnteredOn()) != null ? String.valueOf(CommonUtil.convertDateToStringWithtime(goodsInDO.getEnteredOn())) : "");
		
		if(goodsInDO.getUpdatedBy() != null){
			String empName = CommonUtil.getEmpObject(goodsInDO.getUpdatedBy());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		
		
		/*if(goodsInDO.getUpdatedBy() != null){
			
			List<EmployeeDO> empList = employeeService.retriveByEmpId(Long.parseLong(goodsInDO.getUpdatedBy()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}*/
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(goodsInDO.getUpdatedon())));
		return result;
	}
}
