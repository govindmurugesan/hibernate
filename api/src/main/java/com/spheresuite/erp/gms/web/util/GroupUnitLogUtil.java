package com.spheresuite.erp.gms.web.util;

import java.util.List;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.GroupUnitLogDO;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;

@Component
public class GroupUnitLogUtil {
	
	private GroupUnitLogUtil() {}
	
	@Autowired
	private EmployeeService temployeeService;
	
	private static EmployeeService employeeService;
	
	@PostConstruct
	public void init() {
		employeeService = temployeeService;
	}
	
	public static JSONObject getGroupUnitList(List<GroupUnitLogDO> grpUnitLogList){
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (GroupUnitLogDO grpUnitlogDO : grpUnitLogList) {
				resultJSONArray.put(getVisitiorLogInObject(grpUnitlogDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getVisitiorLogInObject(GroupUnitLogDO grpUnitlogDO)throws JSONException{
		
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(grpUnitlogDO.getId()) != null ? String.valueOf(grpUnitlogDO.getId()) : "");
		result.put(CommonConstants.NAME, String.valueOf(grpUnitlogDO.getName()) != null ? String.valueOf(grpUnitlogDO.getName()) : "" );
		result.put(CommonConstants.MOBILE, String.valueOf(grpUnitlogDO.getMobile()) != null ? String.valueOf(grpUnitlogDO.getMobile()) : "");
		result.put(CommonConstants.UNIT, String.valueOf(grpUnitlogDO.getUnit()) != null ? String.valueOf(grpUnitlogDO.getUnit()) : "");
		result.put(CommonConstants.PURPOSE, String.valueOf(grpUnitlogDO.getPurpose()) != null ? String.valueOf(grpUnitlogDO.getPurpose()) : "");
		result.put(CommonConstants.DEPT, String.valueOf(grpUnitlogDO.getDept()) != null ? String.valueOf(grpUnitlogDO.getDept()) : "");
		result.put(CommonConstants.PERSONTOMEET, String.valueOf(grpUnitlogDO.getPersonToMeet()) != null ? String.valueOf(grpUnitlogDO.getPersonToMeet()) : "");
		if(grpUnitlogDO.getEnteredOn() != null){
			result.put(CommonConstants.ENTEREDON, String.valueOf(CommonUtil.convertDateToStringWithtime(grpUnitlogDO.getEnteredOn())));
		}else{
			result.put(CommonConstants.ENTEREDON, "");
		}
		
		
		if(grpUnitlogDO.getExitOn() != null){
			result.put(CommonConstants.EXITON, String.valueOf(CommonUtil.convertDateToStringWithtime(grpUnitlogDO.getExitOn())));
		}else{
			result.put(CommonConstants.EXITON, "");
		}
		
		//result.put(CommonConstants.ENTEREDON, String.valueOf(visitorLogInDO.getEnteredOn()) != null ? String.valueOf(CommonUtil.convertDateToStringWithtime(visitorLogInDO.getEnteredOn())) : "");
	//	result.put(CommonConstants.EXITON, String.valueOf(grpUnitlogDO.getExitOn()) != null ? String.valueOf(CommonUtil.convertDateToStringWithtime(grpUnitlogDO.getExitOn())) : "");
		
		if(grpUnitlogDO.getUpdatedBy() != null){
			String empName = CommonUtil.getEmpObject(grpUnitlogDO.getUpdatedBy());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		
		/*if(grpUnitlogDO.getUpdatedBy() != null){
			
			List<EmployeeDO> empList = employeeService.retriveByEmpId(Long.parseLong(grpUnitlogDO.getUpdatedBy()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}*/
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(grpUnitlogDO.getUpdatedon())));
		return result;
	}
}
