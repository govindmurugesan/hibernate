package com.spheresuite.erp.gms.web.util;

import java.util.List;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.VisitorLogDO;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;

@Component
public class VisitorLogUtil {
	
	private VisitorLogUtil() {}
	
	@Autowired
	private EmployeeService temployeeService;
	
	private static EmployeeService employeeService;
	
	@PostConstruct
	public void init() {
		employeeService = temployeeService;
	}
	
	public static JSONObject getVisitorInList(List<VisitorLogDO> visitorInList){
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (VisitorLogDO visitorlogInDO : visitorInList) {
				resultJSONArray.put(getVisitiorLogInObject(visitorlogInDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getVisitiorLogInObject(VisitorLogDO visitorLogInDO)throws JSONException{
		
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(visitorLogInDO.getId()) != null ? String.valueOf(visitorLogInDO.getId()) : "");
		result.put(CommonConstants.NAME, String.valueOf(visitorLogInDO.getName()) != null ? String.valueOf(visitorLogInDO.getName()) : "" );
		result.put(CommonConstants.MOBILE, String.valueOf(visitorLogInDO.getMobile()) != null ? String.valueOf(visitorLogInDO.getMobile()) : "");
		//result.put(CommonConstants.UNITNAME, String.valueOf(visitorLogInDO.getUnitName()) != null ? String.valueOf(visitorLogInDO.getUnitName()) : "");
	//	result.put(CommonConstants.PURPOSE, String.valueOf(visitorLogInDO.getPurpose()) != null ? String.valueOf(visitorLogInDO.getPurpose()) : "");
		//result.put(CommonConstants.COMMENT, String.valueOf(visitorLogInDO.getComments()) != null ? String.valueOf(visitorLogInDO.getComments()) : "");
		
		
		result.put(CommonConstants.COMPANY, String.valueOf(visitorLogInDO.getCompany()) != null ? String.valueOf(visitorLogInDO.getCompany()) : "");
		result.put(CommonConstants.ADDRESS, String.valueOf(visitorLogInDO.getAddress()) != null ? String.valueOf(visitorLogInDO.getAddress()) : "");
		result.put(CommonConstants.CITY, String.valueOf(visitorLogInDO.getCity()) != null ? String.valueOf(visitorLogInDO.getCity()) : "");
		result.put(CommonConstants.STATE, String.valueOf(visitorLogInDO.getState()) != null ? String.valueOf(visitorLogInDO.getState()) : "");
		result.put(CommonConstants.IDPROOF, String.valueOf(visitorLogInDO.getIdProof()) != null ? String.valueOf(visitorLogInDO.getIdProof()) : "");
		result.put(CommonConstants.PURPOSE, String.valueOf(visitorLogInDO.getPurpose()) != null ? String.valueOf(visitorLogInDO.getPurpose()) : "");
		result.put(CommonConstants.PERSONTOMEET, String.valueOf(visitorLogInDO.getPersonToMeet()) != null ? String.valueOf(visitorLogInDO.getPersonToMeet()) : "");
		
		
		if(visitorLogInDO.getEnteredOn() != null){
			result.put(CommonConstants.ENTEREDON, String.valueOf(CommonUtil.convertDateToStringWithtime(visitorLogInDO.getEnteredOn())));
		}else{
			result.put(CommonConstants.ENTEREDON, "");
		}
		
		
		if(visitorLogInDO.getExitOn() != null){
			result.put(CommonConstants.EXITON, String.valueOf(CommonUtil.convertDateToStringWithtime(visitorLogInDO.getExitOn())));
		}else{
			result.put(CommonConstants.EXITON, "");
		}
		
		if(visitorLogInDO.getUpdatedBy() != null){
			String empName = CommonUtil.getEmpObject(visitorLogInDO.getUpdatedBy());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		
		/*if(visitorLogInDO.getUpdatedBy() != null){
			
			List<EmployeeDO> empList = employeeService.retriveByEmpId(Long.parseLong(visitorLogInDO.getUpdatedBy()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}*/
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(visitorLogInDO.getUpdatedon())));
		return result;
	}
}
