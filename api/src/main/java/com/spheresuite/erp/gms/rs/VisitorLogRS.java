package com.spheresuite.erp.gms.rs;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.VisitorLogDO;
import com.spheresuite.erp.gms.service.VisitorLogService;
import com.spheresuite.erp.gms.web.util.VisitorLogUtil;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/visitorlog")
public class VisitorLogRS {

	String validation = null;
	static Logger logger = Logger.getLogger(VisitorLogRS.class.getName());
	
	@Autowired
	private VisitorLogService visitorLogService;
	
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				VisitorLogDO visitorLogDO = new VisitorLogDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
				if (inputJSON != null){
					
					if(inputJSON.get(CommonConstants.VEHICLENO) != null && !inputJSON.get(CommonConstants.VEHICLENO).toString().isEmpty()){
						visitorLogDO.setVehicleNo(!inputJSON.get(CommonConstants.VEHICLENO).toString().isEmpty() ? inputJSON.get(CommonConstants.VEHICLENO).toString().replaceAll("\\s+","").toUpperCase() : null);
					}
					if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
						visitorLogDO.setName(!inputJSON.get(CommonConstants.NAME).toString().isEmpty() ? inputJSON.get(CommonConstants.NAME).toString() : null);
					}
					if(inputJSON.get(CommonConstants.MOBILE) != null && !inputJSON.get(CommonConstants.MOBILE).toString().isEmpty()){
						visitorLogDO.setMobile(!inputJSON.get(CommonConstants.MOBILE).toString().isEmpty() ? inputJSON.get(CommonConstants.MOBILE).toString() : null);
					}
					
					
					if(inputJSON.get(CommonConstants.COMPANY) != null && !inputJSON.get(CommonConstants.COMPANY).toString().isEmpty()){
						visitorLogDO.setCompany(!inputJSON.get(CommonConstants.COMPANY).toString().isEmpty() ? inputJSON.get(CommonConstants.COMPANY).toString() : null);
					}
					
					if(inputJSON.get(CommonConstants.ADDRESS) != null && !inputJSON.get(CommonConstants.ADDRESS).toString().isEmpty()){
						visitorLogDO.setAddress(!inputJSON.get(CommonConstants.ADDRESS).toString().isEmpty() ? inputJSON.get(CommonConstants.ADDRESS).toString() : null);
					}
					if(inputJSON.get(CommonConstants.CITY) != null && !inputJSON.get(CommonConstants.CITY).toString().isEmpty()){
						visitorLogDO.setCity(!inputJSON.get(CommonConstants.CITY).toString().isEmpty() ? inputJSON.get(CommonConstants.CITY).toString() : null);
					}
					if(inputJSON.get(CommonConstants.STATE) != null && !inputJSON.get(CommonConstants.STATE).toString().isEmpty()){
						visitorLogDO.setState(!inputJSON.get(CommonConstants.STATE).toString().isEmpty() ? inputJSON.get(CommonConstants.STATE).toString() : null);
					}
					if(inputJSON.get(CommonConstants.IDPROOF) != null && !inputJSON.get(CommonConstants.IDPROOF).toString().isEmpty()){
						visitorLogDO.setIdProof(!inputJSON.get(CommonConstants.IDPROOF).toString().isEmpty() ? inputJSON.get(CommonConstants.IDPROOF).toString() : null);
					}
					
					if(inputJSON.get(CommonConstants.PURPOSE) != null && !inputJSON.get(CommonConstants.PURPOSE).toString().isEmpty()){
						visitorLogDO.setPurpose(!inputJSON.get(CommonConstants.PURPOSE).toString().isEmpty() ? inputJSON.get(CommonConstants.PURPOSE).toString() : null);
					}
					
					if(inputJSON.get(CommonConstants.PERSONTOMEET) != null && !inputJSON.get(CommonConstants.PERSONTOMEET).toString().isEmpty()){
						visitorLogDO.setPersonToMeet(!inputJSON.get(CommonConstants.PERSONTOMEET).toString().isEmpty() ? inputJSON.get(CommonConstants.PERSONTOMEET).toString() : null);
					}
					
					/*visitorLogDO.setName(!inputJSON.get(CommonConstants.NAME).toString().isEmpty() ? inputJSON.get(CommonConstants.NAME).toString() : null);
					visitorLogDO.setMobile(!inputJSON.get(CommonConstants.MOBILE).toString().isEmpty() ? inputJSON.get(CommonConstants.MOBILE).toString() : null);
					if(inputJSON.get(CommonConstants.PURPOSE) != null && !inputJSON.get(CommonConstants.PURPOSE).toString().isEmpty()){
						visitorLogDO.setPurpose(!inputJSON.get(CommonConstants.PURPOSE).toString().isEmpty() ? inputJSON.get(CommonConstants.PURPOSE).toString() : null);
					}
					if(inputJSON.get(CommonConstants.COMMENT) != null && !inputJSON.get(CommonConstants.COMMENT).toString().isEmpty()){
						visitorLogDO.setComments(!inputJSON.get(CommonConstants.COMMENT).toString().isEmpty() ? inputJSON.get(CommonConstants.COMMENT).toString() : null);
					}*/
					
					
					visitorLogDO.setStatus((!inputJSON.get(CommonConstants.STATUS).toString().isEmpty() ? inputJSON.get(CommonConstants.STATUS).toString().charAt(0) : null));
					if(inputJSON.get(CommonConstants.STATUS).toString().charAt(0) == 'e'){
						visitorLogDO.setEnteredOn(new Date());
					}else{
						visitorLogDO.setExitOn(new Date());
					}
					visitorLogDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			visitorLogDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 	}
				if(!visitorLogService.persist(visitorLogDO)){
					return CommonWebUtil.buildErrorResponse("Something went wrong").toString();
				}
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Visitor In created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<VisitorLogDO> visitorLogList = visitorLogService.retrieve();
				Collections.reverse(visitorLogList);
				respJSON = VisitorLogUtil.getVisitorInList(visitorLogList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveByMobileNoAndStatus/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByMobileNoAndStatus(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				List<VisitorLogDO> visitorLogList = visitorLogService.retrieveByMobileNoAndStatus(inputJSON.get(CommonConstants.MOBILE).toString(), inputJSON.get(CommonConstants.STATUS).toString().charAt(0));
				respJSON = VisitorLogUtil.getVisitorInList(visitorLogList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	
	@RequestMapping(value = "/retrieveByMobileNo/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByVehicle(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				List<VisitorLogDO> visitorLogList = visitorLogService.retrieveByMobileNo(inputJSON.get(CommonConstants.MOBILE).toString());
				/*if(visitorLogList != null && visitorLogList.size() >0){
					
				}*/
				respJSON = VisitorLogUtil.getVisitorInList(visitorLogList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				VisitorLogDO visitorLogDO = new VisitorLogDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<VisitorLogDO> visitorLogList = visitorLogService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		visitorLogDO = visitorLogList.get(0);
			 		
			 		if(inputJSON.get(CommonConstants.VEHICLENO) != null && !inputJSON.get(CommonConstants.VEHICLENO).toString().isEmpty()){
						visitorLogDO.setVehicleNo(!inputJSON.get(CommonConstants.VEHICLENO).toString().isEmpty() ? inputJSON.get(CommonConstants.VEHICLENO).toString().replaceAll("\\s+","").toUpperCase() : null);
					}
					if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
						visitorLogDO.setName(!inputJSON.get(CommonConstants.NAME).toString().isEmpty() ? inputJSON.get(CommonConstants.NAME).toString() : null);
					}
					if(inputJSON.get(CommonConstants.MOBILE) != null && !inputJSON.get(CommonConstants.MOBILE).toString().isEmpty()){
						visitorLogDO.setMobile(!inputJSON.get(CommonConstants.MOBILE).toString().isEmpty() ? inputJSON.get(CommonConstants.MOBILE).toString() : null);
					}
					
					
					if(inputJSON.get(CommonConstants.COMPANY) != null && !inputJSON.get(CommonConstants.COMPANY).toString().isEmpty()){
						visitorLogDO.setCompany(!inputJSON.get(CommonConstants.COMPANY).toString().isEmpty() ? inputJSON.get(CommonConstants.COMPANY).toString() : null);
					}
					
					if(inputJSON.get(CommonConstants.ADDRESS) != null && !inputJSON.get(CommonConstants.ADDRESS).toString().isEmpty()){
						visitorLogDO.setAddress(!inputJSON.get(CommonConstants.ADDRESS).toString().isEmpty() ? inputJSON.get(CommonConstants.ADDRESS).toString() : null);
					}
					if(inputJSON.get(CommonConstants.CITY) != null && !inputJSON.get(CommonConstants.CITY).toString().isEmpty()){
						visitorLogDO.setCity(!inputJSON.get(CommonConstants.CITY).toString().isEmpty() ? inputJSON.get(CommonConstants.CITY).toString() : null);
					}
					if(inputJSON.get(CommonConstants.STATE) != null && !inputJSON.get(CommonConstants.STATE).toString().isEmpty()){
						visitorLogDO.setState(!inputJSON.get(CommonConstants.STATE).toString().isEmpty() ? inputJSON.get(CommonConstants.STATE).toString() : null);
					}
					if(inputJSON.get(CommonConstants.IDPROOF) != null && !inputJSON.get(CommonConstants.IDPROOF).toString().isEmpty()){
						visitorLogDO.setIdProof(!inputJSON.get(CommonConstants.IDPROOF).toString().isEmpty() ? inputJSON.get(CommonConstants.IDPROOF).toString() : null);
					}
					
					if(inputJSON.get(CommonConstants.PURPOSE) != null && !inputJSON.get(CommonConstants.PURPOSE).toString().isEmpty()){
						visitorLogDO.setPurpose(!inputJSON.get(CommonConstants.PURPOSE).toString().isEmpty() ? inputJSON.get(CommonConstants.PURPOSE).toString() : null);
					}
					
					if(inputJSON.get(CommonConstants.PERSONTOMEET) != null && !inputJSON.get(CommonConstants.PERSONTOMEET).toString().isEmpty()){
						visitorLogDO.setPersonToMeet(!inputJSON.get(CommonConstants.PERSONTOMEET).toString().isEmpty() ? inputJSON.get(CommonConstants.PERSONTOMEET).toString() : null);
					}
					
					
			 		/*visitorLogDO.setName(!inputJSON.get(CommonConstants.NAME).toString().isEmpty() ? inputJSON.get(CommonConstants.NAME).toString() : null);
			 		visitorLogDO.setMobile(!inputJSON.get(CommonConstants.MOBILE).toString().isEmpty() ? inputJSON.get(CommonConstants.MOBILE).toString() : null);
			 		visitorLogDO.setUnitName(!inputJSON.get(CommonConstants.UNITNAME).toString().isEmpty() ? inputJSON.get(CommonConstants.UNITNAME).toString() : null);
					if(inputJSON.get(CommonConstants.PURPOSE) != null && !inputJSON.get(CommonConstants.PURPOSE).toString().isEmpty()){
						visitorLogDO.setPurpose(!inputJSON.get(CommonConstants.PURPOSE).toString().isEmpty() ? inputJSON.get(CommonConstants.PURPOSE).toString() : null);
					}
					if(inputJSON.get(CommonConstants.COMMENT) != null && !inputJSON.get(CommonConstants.COMMENT).toString().isEmpty()){
						visitorLogDO.setComments(!inputJSON.get(CommonConstants.COMMENT).toString().isEmpty() ? inputJSON.get(CommonConstants.COMMENT).toString() : null);
					}
					*/
					//visitorLogDO.setStatus((!inputJSON.get(CommonConstants.STATUS).toString().isEmpty() ? inputJSON.get(CommonConstants.STATUS).toString().charAt(0) : null));
					
					if(inputJSON.get(CommonConstants.STATUS).toString().charAt(0) == 'e'){
						visitorLogDO.setEnteredOn(new Date());
					}else{
						visitorLogDO.setExitOn(new Date());
					}
					visitorLogDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			visitorLogDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		visitorLogDO.setStatus((char) (inputJSON != null ? inputJSON.get(CommonConstants.STATUS).toString().charAt(0) : ""));
			 		//addressTypeService.update(allowanceTypeDO);
			 		if(!visitorLogService.update(visitorLogDO)){
						return CommonWebUtil.buildErrorResponse("Details Not Updated").toString();
					}
			 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Details Updated Sucessfully");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	
}
