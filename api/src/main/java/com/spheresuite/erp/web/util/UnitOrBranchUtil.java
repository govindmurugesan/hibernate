package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.UnitOrBranchDO;
import com.spheresuite.erp.util.CommonConstants;

@Component
public class UnitOrBranchUtil {
	
	private UnitOrBranchUtil() {}
	
	public static JSONObject getUnitOrBranchList(List<UnitOrBranchDO> unitOrBranchList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (UnitOrBranchDO unitOrBranch : unitOrBranchList) {
				resultJSONArray.put(getUnitOrBranchDetailObject(unitOrBranch));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getUnitOrBranchDetailObject(UnitOrBranchDO unitOrBranch)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(unitOrBranch.getUnitOrBranchId()));
		result.put(CommonConstants.CITY, String.valueOf(unitOrBranch.getUnitOrBranch()));
		result.put(CommonConstants.COUNTRY_ID, String.valueOf(unitOrBranch.getCountry().getCountryId()));
		result.put(CommonConstants.STATE_ID, String.valueOf(unitOrBranch.getState().getStateId()));
		result.put(CommonConstants.STATUS, String.valueOf(unitOrBranch.getStatus()));
		if(unitOrBranch.getAddress() != null)result.put(CommonConstants.ADDRESS, String.valueOf(unitOrBranch.getAddress()));
		
		result.put(CommonConstants.COUNTRY_NAME, String.valueOf(unitOrBranch.getCountry().getCountryName()));
		result.put(CommonConstants.STATE_NAME, String.valueOf(unitOrBranch.getState().getStateName()));
		if(unitOrBranch.getManagerId() != null){
			String mangerName = CommonUtil.getEmpObject(unitOrBranch.getManagerId().getEmpId());
			result.put(CommonConstants.REPORTTO_NAME, mangerName); 
			result.put(CommonConstants.MANAGERID, unitOrBranch.getManagerId().getEmpId());
		}else{
			result.put(CommonConstants.REPORTTO_NAME, ""); 
			result.put(CommonConstants.MANAGERID, "");
		}
		
		if(unitOrBranch.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(unitOrBranch.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(unitOrBranch.getUpdatedon())));
		return result;
	}
}
