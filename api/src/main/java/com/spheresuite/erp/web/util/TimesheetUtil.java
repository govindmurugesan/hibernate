package com.spheresuite.erp.web.util;

import java.util.List;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.crm.service.ProjectService;
import com.spheresuite.erp.domainobject.ProjectDO;
import com.spheresuite.erp.domainobject.TimesheetDO;
import com.spheresuite.erp.util.CommonConstants;

@Component
public class TimesheetUtil {
	
	private TimesheetUtil() {}
	
	@Autowired
	private ProjectService projectServiceTemp;

	
	private static ProjectService projectService;
	
	@PostConstruct
	public void init() {
		projectService = this.projectServiceTemp;
	}
	
	public static JSONObject getTimesheetList(List<TimesheetDO> timesheetList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (TimesheetDO timesheet : timesheetList) {
				resultJSONArray.put(getTimesheetDetailObject(timesheet));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getTimesheetListWithStatus(List<TimesheetDO> timesheetList, char status) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (TimesheetDO timesheet : timesheetList) {
				resultJSONArray.put(getTimesheetDetailObjectWithStatus(timesheet, status));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getTimesheetDetailObject(TimesheetDO timesheet)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(timesheet.getTimesheetId()));
		result.put(CommonConstants.DATE, String.valueOf(timesheet.getDate()));
		result.put(CommonConstants.HOURS, String.valueOf(timesheet.getHours()));
		
		if(timesheet.getDescription() != null){
			result.put(CommonConstants.DESCRIPTION, String.valueOf(timesheet.getDescription()));
		}else{
			result.put(CommonConstants.DESCRIPTION, "");
		}
		
		result.put(CommonConstants.TYPE, String.valueOf(timesheet.getTimesheetType()));
		/*List<TimesheetTypeDO> timesheetTypeList = timesheetTypeService.retrieveById(timesheet.getTimesheetType());*/
		List<ProjectDO> projectList = projectService.retrieveById(timesheet.getTimesheetType().getTimesheetTypeId());
 		result.put(CommonConstants.TYPENAME, String.valueOf(projectList.get(0).getProjectname()));
 		/*result.put(CommonConstants.TYPENAME, String.valueOf(timesheetTypeList.get(0).getName()));*/
		result.put(CommonConstants.WEEKENDDATE, String.valueOf(timesheet.getWeeklyTimesheet().getDate()));
		//result.put(CommonConstants.STATUS, String.valueOf(timesheet.getStatus()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(timesheet.getUpdatedon())));
		if(timesheet.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(timesheet.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		return result;
	}
	
	public static JSONObject getTimesheetDetailObjectWithStatus(TimesheetDO timesheet, char status)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(timesheet.getTimesheetId()));
		result.put(CommonConstants.DATE, String.valueOf(timesheet.getDate()));
		result.put(CommonConstants.HOURS, String.valueOf(timesheet.getHours()));
		
		if(timesheet.getDescription() != null){
			result.put(CommonConstants.DESCRIPTION, String.valueOf(timesheet.getDescription()));
		}else{
			result.put(CommonConstants.DESCRIPTION, "");
		}
		
		result.put(CommonConstants.TYPE, String.valueOf(timesheet.getTimesheetType()));
		List<ProjectDO> projectList = projectService.retrieveById(timesheet.getTimesheetType().getTimesheetTypeId());
 		result.put(CommonConstants.TYPENAME, String.valueOf(projectList.get(0).getProjectname()));
		result.put(CommonConstants.WEEKENDDATE, String.valueOf(timesheet.getWeeklyTimesheet().getDate()));
		result.put(CommonConstants.STATUS, String.valueOf(status));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(timesheet.getUpdatedon())));
		if(timesheet.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(timesheet.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		return result;
	}
}
