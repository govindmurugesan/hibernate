package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.IssueTypeDO;
import com.spheresuite.erp.util.CommonConstants;

@Component
public class IssueTypeUtil {
	
	private IssueTypeUtil() {}
	
	public static JSONObject getIssueTypeList(List<IssueTypeDO> issueTypeList){
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (IssueTypeDO issueType : issueTypeList) {
				resultJSONArray.put(getIssueDetailObject(issueType));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getIssueDetailObject(IssueTypeDO issueType)throws JSONException{
		
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(issueType.getIssueTypeId()));
		result.put(CommonConstants.NAME, String.valueOf(issueType.getName()));
		result.put(CommonConstants.STATUS, String.valueOf(issueType.getStatus()));
		if(issueType.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(issueType.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(issueType.getUpdatedon())));
		return result;
	}
}
