package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.PayrollGroupAttendanceAllowanceDO;
import com.spheresuite.erp.domainobject.PayrollGroupDO;
import com.spheresuite.erp.domainobject.PayrollGroupDeductionDO;
import com.spheresuite.erp.domainobject.PayrollGroupEarningsDO;
import com.spheresuite.erp.domainobject.PayrollGroupTaxDO;
import com.spheresuite.erp.domainobject.PayrollGroupWorkingdaysAllowanceDO;
import com.spheresuite.erp.util.CommonConstants;

@Component
public class PayrollGroupUtil {
	
	private PayrollGroupUtil() {}
	
	
	
	public static JSONObject getPayrollGroupListDetails(List<PayrollGroupDO> payrollGroupList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (PayrollGroupDO payrollGroup : payrollGroupList) {
				resultJSONArray.put(getPayrollGroupWithAllDetailObject(payrollGroup));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getPayrollGroupList(List<PayrollGroupDO> payrollGroupList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (PayrollGroupDO payrollGroup : payrollGroupList) {
				resultJSONArray.put(getPayrollGroupDetailObject(payrollGroup));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getPayrollGroupDetailObject(PayrollGroupDO payrollGroup)throws JSONException{
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(payrollGroup.getPayrollGroupId()));
		result.put(CommonConstants.NAME, String.valueOf(payrollGroup.getName()));
		/*if(payrollGroup.getPayrollGroupEarning() != null){
			if(payrollGroup.getPayrollGroupEarning().size() >0){
				for(PayrollGroupEarningsDO grp1 : payrollGroup.getPayrollGroupEarning()){
					System.out.println("grp1"+grp1.getAllowanceSettings().getName());
				}
			}
		}*/
		if(payrollGroup.getDescription() != null){
			result.put(CommonConstants.DESC, String.valueOf(payrollGroup.getDescription()));
		}else{
			result.put(CommonConstants.DESC, "");
		}
		if(payrollGroup.getUnitOrBranch() != null){
			result.put(CommonConstants.UNIT, String.valueOf(payrollGroup.getUnitOrBranch().getUnitOrBranchId()));
			result.put(CommonConstants.UNITNAME, String.valueOf(payrollGroup.getUnitOrBranch().getUnitOrBranch()));
		}else{
			result.put(CommonConstants.UNIT, "");
		}
		result.put(CommonConstants.STATUS, String.valueOf(payrollGroup.getStatus()));
		if(payrollGroup.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(payrollGroup.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(payrollGroup.getUpdatedon())));
		return result;
	}
	
	
	public static JSONObject getPayrollGroupWithAllDetailObject(PayrollGroupDO payrollGroup)throws JSONException{
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(payrollGroup.getPayrollGroupId()));
		result.put(CommonConstants.NAME, String.valueOf(payrollGroup.getName()));
		JSONArray payrollGroupEarningDetailsJSONArray= new JSONArray();
		if(payrollGroup.getPayrollGroupEarning() != null){
			if(payrollGroup.getPayrollGroupEarning().size() >0){
				for(PayrollGroupEarningsDO earningGrp : payrollGroup.getPayrollGroupEarning()){
					JSONObject payrollGroupEarningDetails = new JSONObject();
					payrollGroupEarningDetails.put(CommonConstants.ID, String.valueOf(earningGrp.getPayrollGroupEarningsId()));
					payrollGroupEarningDetails.put(CommonConstants.ALLOWENCE_ID, String.valueOf(earningGrp.getAllowanceSettings().getAllowanceSettingsId()));
					payrollGroupEarningDetails.put(CommonConstants.NAME, String.valueOf(earningGrp.getAllowanceSettings().getDisplayname()));
					payrollGroupEarningDetails.put(CommonConstants.STATUS, String.valueOf(earningGrp.getStatus()));
					payrollGroupEarningDetailsJSONArray.put(payrollGroupEarningDetails);
				}
			}
		}
		JSONArray payrollGroupDeductionDetailsJSONArray= new JSONArray();
		if(payrollGroup.getPayrollGroupDeduction() != null){
			if(payrollGroup.getPayrollGroupDeduction().size() >0){
				for(PayrollGroupDeductionDO deductionGrp : payrollGroup.getPayrollGroupDeduction()){
					JSONObject payrollGroupDeductionDetails = new JSONObject();
					payrollGroupDeductionDetails.put(CommonConstants.ID, String.valueOf(deductionGrp.getPayrollGroupDeductionId()));
					payrollGroupDeductionDetails.put(CommonConstants.DEDUCTIONID, String.valueOf(deductionGrp.getDeductionSettings().getDeductionsettingId()));
					payrollGroupDeductionDetails.put(CommonConstants.NAME, String.valueOf(deductionGrp.getDeductionSettings().getDisplayname()));
					payrollGroupDeductionDetails.put(CommonConstants.STATUS, String.valueOf(deductionGrp.getStatus()));
					payrollGroupDeductionDetailsJSONArray.put(payrollGroupDeductionDetails);
				}
			}
		}
		
		
		JSONArray payrollGroupAttendanceDetailsJSONArray= new JSONArray();
		if(payrollGroup.getPayrollGroupAttendanceAllowance() != null){
			if(payrollGroup.getPayrollGroupAttendanceAllowance().size() >0){
				for(PayrollGroupAttendanceAllowanceDO attendacneallwanceGrp : payrollGroup.getPayrollGroupAttendanceAllowance()){
					JSONObject payrollGroupAttendanceAllowanceDetails = new JSONObject();
					payrollGroupAttendanceAllowanceDetails.put(CommonConstants.ID, String.valueOf(attendacneallwanceGrp.getPayrollGroupAttendanceAllowanceId()));
					payrollGroupAttendanceAllowanceDetails.put(CommonConstants.ATTENDANCEALLOWANCEID, String.valueOf(attendacneallwanceGrp.getAttendanceAllowance().getAttendanceAllowanceId()));
					payrollGroupAttendanceAllowanceDetails.put(CommonConstants.NAME, String.valueOf(attendacneallwanceGrp.getAttendanceAllowance().getName()));
					payrollGroupAttendanceAllowanceDetails.put(CommonConstants.STATUS, String.valueOf(attendacneallwanceGrp.getStatus()));
					payrollGroupAttendanceDetailsJSONArray.put(payrollGroupAttendanceAllowanceDetails);
				}
			}
		}
		
		
		JSONArray payrollGroupWorkingDetailsJSONArray= new JSONArray();
		if(payrollGroup.getPayrollGroupWorkingdays() != null){
			if(payrollGroup.getPayrollGroupWorkingdays().size() >0){
				for(PayrollGroupWorkingdaysAllowanceDO payrollGroupWorkingday : payrollGroup.getPayrollGroupWorkingdays()){
					JSONObject payrollGroupWorkingdayDetails = new JSONObject();
					payrollGroupWorkingdayDetails.put(CommonConstants.ID, String.valueOf(payrollGroupWorkingday.getPayrollGroupWorkingDaysId()));
					payrollGroupWorkingdayDetails.put(CommonConstants.WORKALLOWANCEID, String.valueOf(payrollGroupWorkingday.getWorkingDayAllowance().getWorkingDayAllowanceId()));
					payrollGroupWorkingdayDetails.put(CommonConstants.NAME, String.valueOf(payrollGroupWorkingday.getWorkingDayAllowance().getName()));
					payrollGroupWorkingdayDetails.put(CommonConstants.STATUS, String.valueOf(payrollGroupWorkingday.getStatus()));
					payrollGroupWorkingDetailsJSONArray.put(payrollGroupWorkingdayDetails);
				}
			}
		}
		JSONArray payrollGroupTaxDetailsJSONArray= new JSONArray();
		if(payrollGroup.getPayrollGroupTax() != null){
			if(payrollGroup.getPayrollGroupTax().size() >0){
				for(PayrollGroupTaxDO taxGrp : payrollGroup.getPayrollGroupTax()){
					JSONObject payrollGroupTaxDetails = new JSONObject();
					if(taxGrp.getPt() != null && taxGrp.getPt().toString().equalsIgnoreCase(CommonConstants.APPLY)){
						if(taxGrp.getStatus() == CommonConstants.ACTIVE){
							result.put(CommonConstants.PT_ID,  String.valueOf("true"));
						}else{
							result.put(CommonConstants.PT_ID,  String.valueOf("false"));
						}
					}else if(taxGrp.getTds() != null && taxGrp.getTds().toString().equalsIgnoreCase(CommonConstants.APPLY)){
						if(taxGrp.getStatus() == CommonConstants.ACTIVE){
							result.put(CommonConstants.TDSID,  String.valueOf("true"));
						}else{
							result.put(CommonConstants.TDSID,  String.valueOf("false"));
						}
					}else if(taxGrp.getEducationCess() != null && taxGrp.getEducationCess().toString().equalsIgnoreCase(CommonConstants.APPLY)){
						if(taxGrp.getStatus() == CommonConstants.ACTIVE){
							result.put(CommonConstants.EDUCATIONCESS,  String.valueOf("true"));
						}else{
							result.put(CommonConstants.EDUCATIONCESS,  String.valueOf("false"));
						}
					}else{
						payrollGroupTaxDetails.put(CommonConstants.ID, String.valueOf(taxGrp.getPayrollGroupTaxId()));
						payrollGroupTaxDetails.put(CommonConstants.TAXID, String.valueOf(taxGrp.getTaxSettings().getTaxSettingId()));
						payrollGroupTaxDetails.put(CommonConstants.NAME, String.valueOf(taxGrp.getTaxSettings().getDisplayname()));
						payrollGroupTaxDetails.put(CommonConstants.STATUS, String.valueOf(taxGrp.getStatus()));
						payrollGroupTaxDetailsJSONArray.put(payrollGroupTaxDetails);
					}
					
				}
			}
		}
		
		if(payrollGroup.getDescription() != null){
			result.put(CommonConstants.DISPLAYNAME, String.valueOf(payrollGroup.getDescription()));
		}else{
			result.put(CommonConstants.DISPLAYNAME, "");
		}
		result.put(CommonConstants.STATUS, String.valueOf(payrollGroup.getStatus()));
		result.put(CommonConstants.EARNINGID, payrollGroupEarningDetailsJSONArray);
		result.put(CommonConstants.DEDUCTIONID, payrollGroupDeductionDetailsJSONArray);
		result.put(CommonConstants.TAXID, payrollGroupTaxDetailsJSONArray);
		result.put(CommonConstants.WORKALLOWANCELIST, payrollGroupWorkingDetailsJSONArray);
		result.put(CommonConstants.ATTENDANCEALLOWANCELIST, payrollGroupAttendanceDetailsJSONArray);
		
		
		if(payrollGroup.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(payrollGroup.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(payrollGroup.getUpdatedon())));
		return result;
	}
}
