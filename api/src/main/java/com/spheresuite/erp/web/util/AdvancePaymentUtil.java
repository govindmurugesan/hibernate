package com.spheresuite.erp.web.util;

import java.util.List;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.AdvancePaymentDO;
import com.spheresuite.erp.domainobject.AdvancePaymentDetailDO;
import com.spheresuite.erp.service.AdvancePaymentDetailService;
import com.spheresuite.erp.util.CommonConstants;
@Component
public class AdvancePaymentUtil {
	
	private AdvancePaymentUtil() {}
	
	@Autowired
	private AdvancePaymentDetailService tadvancePaymentDetailService;
	private static AdvancePaymentDetailService advancePaymentDetailService;
	
	@PostConstruct
	public void init() {
		advancePaymentDetailService = tadvancePaymentDetailService;
	}
	

	public static JSONObject getAdvancePaymentList(List<AdvancePaymentDO> advancePaymentList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (AdvancePaymentDO advancePayment : advancePaymentList) {
				resultJSONArray.put(getAdvancePaymentDetailObject(advancePayment));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getAdvancePaymentDetailObject(AdvancePaymentDO advancePayment)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(advancePayment.getAdvancePaymentId()));
		if(advancePayment.getEmployee().getEmpId() != null ){
			result.put(CommonConstants.EMPID, String.valueOf(advancePayment.getEmployee().getEmpId()));
			if(advancePayment.getEmployee().getMiddlename() != null) result.put(CommonConstants.EMPNAME, String.valueOf(advancePayment.getEmployee().getFirstname() + " " +advancePayment.getEmployee().getMiddlename() + " " + advancePayment.getEmployee().getLastname()));
			else if(advancePayment.getEmployee().getMiddlename() != null && advancePayment.getEmployee().getLastname() == null ) result.put(CommonConstants.EMPNAME, String.valueOf(advancePayment.getEmployee().getFirstname() + " " +advancePayment.getEmployee().getMiddlename()));
			else if(advancePayment.getEmployee().getMiddlename() == null && advancePayment.getEmployee().getLastname() == null ) result.put(CommonConstants.EMPNAME,String.valueOf(advancePayment.getEmployee().getFirstname()));
			else result.put(CommonConstants.EMPNAME, String.valueOf(advancePayment.getEmployee().getFirstname() + " " + advancePayment.getEmployee().getLastname()));
		}else{
			result.put(CommonConstants.EMPID, "");
			result.put(CommonConstants.EMPNAME, ""); 
		}
		result.put(CommonConstants.AMOUNT, String.valueOf(advancePayment.getAmount()));
		if(advancePayment.getMonth() != null){
			result.put(CommonConstants.MONTHLY, String.valueOf(advancePayment.getMonth()));
		}else{
			result.put(CommonConstants.MONTHLY, "");
		}
		if(advancePayment.getAdvanceType() != null){
			result.put(CommonConstants.TYPE_ID, String.valueOf(advancePayment.getAdvanceType().getAdvanceTypeId()));
			result.put(CommonConstants.TYPE, String.valueOf(advancePayment.getAdvanceType().getName()));
		}else{
			result.put(CommonConstants.TYPE_ID, "");
			result.put(CommonConstants.TYPE, "");
		}
		
		if(advancePayment.getInstallmentAmount() != null){
			result.put(CommonConstants.INSTALLMENTAOUNT, String.valueOf(advancePayment.getInstallmentAmount()));
		}else{
			result.put(CommonConstants.INSTALLMENTAOUNT, "");
		}
		if(advancePayment.getNoOfInstallments() != null){
			result.put(CommonConstants.NOOFINSTALLMENT, String.valueOf(advancePayment.getNoOfInstallments()));
		}else{
			result.put(CommonConstants.NOOFINSTALLMENT, "");
		}
		result.put(CommonConstants.STATUS, String.valueOf(advancePayment.getStatus()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(advancePayment.getUpdatedon())));
		if(advancePayment.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(advancePayment.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		
		Double pendingAmount = 0d;
		List<AdvancePaymentDetailDO> paymentDetailList = advancePaymentDetailService.getByAdvanceId(advancePayment.getAdvancePaymentId());
		if(paymentDetailList != null && paymentDetailList.size() > 0){
			JSONArray resultJSONArray = new JSONArray();
			for (AdvancePaymentDetailDO paymentDetail : paymentDetailList) {
				if(paymentDetail != null && paymentDetail.getAmount() != null &&  paymentDetail.getStatus().equalsIgnoreCase(CommonConstants.ACTIVESTRING)){
					pendingAmount += Double.parseDouble(paymentDetail.getAmount().toString());
				}
				resultJSONArray.put(getPaymentDetailObject(paymentDetail));
			}
			result.put(CommonConstants.ADVANCEDETAIL, resultJSONArray);
			result.put(CommonConstants.PENDINGAMOUNT, pendingAmount);
		}
		
		return result;
	}
	
	
	public static JSONObject getPaymentDetailObject(AdvancePaymentDetailDO paymentDetail)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(paymentDetail.getAdvancePaymentDetailId()));
		result.put(CommonConstants.AMOUNT, String.valueOf(paymentDetail.getAmount()));
		if(paymentDetail.getMonth() != null){
			result.put(CommonConstants.MONTH, String.valueOf(paymentDetail.getMonth()));
		}else{
			result.put(CommonConstants.MONTH, "");
		}
		result.put(CommonConstants.STATUS, String.valueOf(paymentDetail.getStatus()));
		return result;
	}
}
