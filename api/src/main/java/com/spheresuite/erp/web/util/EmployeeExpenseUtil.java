package com.spheresuite.erp.web.util;

import java.util.List;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.EmployeeExpenseDO;
import com.spheresuite.erp.domainobject.EmployeeExpenseDocDO;
import com.spheresuite.erp.service.EmployeeExpenseDocService;
import com.spheresuite.erp.util.CommonConstants;
@Component
public class EmployeeExpenseUtil {
	
	@Autowired
	private EmployeeExpenseDocService tempExpenseDocService;
	
	private static EmployeeExpenseDocService empExpenseDocService;
	
	@PostConstruct
	public void init() {
		empExpenseDocService = tempExpenseDocService;
	}
	
	private EmployeeExpenseUtil() {}

	public static JSONObject getEmployeeExpenseList(List<EmployeeExpenseDO> employeeExpenseList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeeExpenseDO employeeExpense : employeeExpenseList) {
				resultJSONArray.put(getEmployeeExpenseDetailObject(employeeExpense));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getEmployeeExpenseDetailObject(EmployeeExpenseDO employeeExpense)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(employeeExpense.getEmpExpenseId()));
		if(employeeExpense.getDescription() != null){
			result.put(CommonConstants.DESCRIPTION, String.valueOf(employeeExpense.getDescription()));
		}else{
			result.put(CommonConstants.DESCRIPTION, String.valueOf(""));
		}
		if(employeeExpense.getStatus() != null){
			result.put(CommonConstants.STATUS, String.valueOf(employeeExpense.getStatus()));
		}else{
			result.put(CommonConstants.STATUS, String.valueOf(""));
		}
		if(employeeExpense.getAmount() != null){
			result.put(CommonConstants.AMOUNT, String.valueOf(employeeExpense.getAmount()));
		}else{
			result.put(CommonConstants.AMOUNT, String.valueOf(""));
		}
		if(employeeExpense.getFromDate() != null){
			result.put(CommonConstants.FROMDATE, String.valueOf(employeeExpense.getFromDate()));
			result.put(CommonConstants.FROMDATEDISPLAY, String.valueOf(CommonUtil.convertDateToStringApp(employeeExpense.getFromDate())));
		}else{
			result.put(CommonConstants.FROMDATE, String.valueOf(""));
			result.put(CommonConstants.FROMDATEDISPLAY, String.valueOf(""));
		}
		if(employeeExpense.getExpenseType() != null){
			result.put(CommonConstants.TYPE, String.valueOf(employeeExpense.getExpenseType().getExpenseTypeId()));
			result.put(CommonConstants.TYPENAME, String.valueOf(employeeExpense.getExpenseType().getName()));
		}else{
			result.put(CommonConstants.TYPE, "");
			result.put(CommonConstants.TYPENAME, "");
		}
		
		if(employeeExpense.getEmployee() != null){
			String empName = CommonUtil.getEmpObject(employeeExpense.getEmployee().getEmpId());
			result.put(CommonConstants.EMP_NAME, empName); 
			result.put(CommonConstants.EMPID, employeeExpense.getEmployee().getEmpId()); 
		}else{
			result.put(CommonConstants.EMP_NAME, ""); 
			result.put(CommonConstants.EMPID, ""); 
		}
		
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(employeeExpense.getUpdatedon())));
		
		if(employeeExpense.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(employeeExpense.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		if(employeeExpense.getToDate() != null){
			result.put(CommonConstants.TODATE, String.valueOf(employeeExpense.getToDate()));
			result.put(CommonConstants.TODATEDISPLAY, String.valueOf(CommonUtil.convertDateToStringApp(employeeExpense.getToDate())));
		}else{
			result.put(CommonConstants.TODATE, "");
			result.put(CommonConstants.TODATEDISPLAY, String.valueOf(""));
		}
		if(employeeExpense.getPaidon() != null){
			result.put(CommonConstants.PAIDON, String.valueOf(employeeExpense.getPaidon()));
			result.put(CommonConstants.PAIDDATE, String.valueOf(CommonUtil.convertDateToStringApp(employeeExpense.getPaidon())));
		}else{
			result.put(CommonConstants.PAIDON, "");
			result.put(CommonConstants.PAIDDATE, String.valueOf(""));
		}
		result.put(CommonConstants.CREATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(employeeExpense.getCreatedon())));
		if(employeeExpense.getCreatedby() != null){
			String empName = CommonUtil.getEmpObject(employeeExpense.getCreatedby());
			result.put(CommonConstants.CREATED_BY, empName); 
		}else{
			result.put(CommonConstants.CREATED_BY, ""); 
		}
		
		JSONArray resultJSONArray = new JSONArray();
		List<EmployeeExpenseDocDO> empExpenseDocDOList = empExpenseDocService.retrieveByExpenseId(employeeExpense.getEmpExpenseId());
		if(empExpenseDocDOList != null && empExpenseDocDOList.size()  > 0){
			for (EmployeeExpenseDocDO doc : empExpenseDocDOList) {
				resultJSONArray.put(getDocDetailObject(doc));
			}
		}
		result.put(CommonConstants.FILE, resultJSONArray);
		return result;
	}
	
	public static JSONObject getDocDetailObject(EmployeeExpenseDocDO doc)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.FILE, String.valueOf(doc.getPhoto()));
		if(doc.getFileName() != null){
			result.put(CommonConstants.NAME, String.valueOf(doc.getFileName()));
		}else{
			result.put(CommonConstants.NAME, "");	
		}
		
		return result;
	}
}
