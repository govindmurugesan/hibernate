package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.EmployeeTypeDO;
import com.spheresuite.erp.util.CommonConstants;
@Component
public class EmployeeTypeUtil {
	
	private EmployeeTypeUtil() {}
	
	public static JSONObject getEmployeeTypeList(List<EmployeeTypeDO> employeeTypeList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeeTypeDO employeeType : employeeTypeList) {
				resultJSONArray.put(getEmployeeTypeDetailObject(employeeType));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getEmployeeTypeDetailObject(EmployeeTypeDO employeeType)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(employeeType.getEmpTypeId()));
		result.put(CommonConstants.NAME, String.valueOf(employeeType.getName()));
		result.put(CommonConstants.STATUS, String.valueOf(employeeType.getStatus()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(employeeType.getUpdatedon())));
		if(employeeType.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(employeeType.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		return result;
	}
}
