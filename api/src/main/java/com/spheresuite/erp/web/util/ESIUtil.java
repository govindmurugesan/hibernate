package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.ESIDO;
import com.spheresuite.erp.util.CommonConstants;
@Component
public class ESIUtil {
	
	private ESIUtil() {}
	
	public static JSONObject getESIList(List<ESIDO> esiList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (ESIDO esi : esiList) {
				resultJSONArray.put(getESIDetailObject(esi));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getESIDetailObject(ESIDO esi)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(esi.getEsiId()));
		if(esi.getEmployeePercentage() != null){
			result.put(CommonConstants.PERCENTAGE, String.valueOf(esi.getEmployeePercentage()));
		}else{
			result.put(CommonConstants.PERCENTAGE, String.valueOf(""));
		}
		
		if(esi.getEmployerPercentage() != null){
			result.put(CommonConstants.EMPLOYERPERCENTAGE, String.valueOf(esi.getEmployerPercentage()));
		}else{
			result.put(CommonConstants.EMPLOYERPERCENTAGE, String.valueOf(""));
		}
		result.put(CommonConstants.STATUS, String.valueOf(esi.getStatus()));
		if(esi.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(esi.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		if(esi.getUpdatedon() != null)result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(esi.getUpdatedon())));

		return result;
	}
}
