package com.spheresuite.erp.web.util;

import java.util.List;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.EmployeeBonusDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.util.CommonConstants;
@Component
public class EmployeeBonusUtil {
	
	private EmployeeBonusUtil() {}

	@Autowired
	private EmployeeService temployeeService;
	
	private static EmployeeService employeeService;
	
	@PostConstruct
	public void init() {
		employeeService = temployeeService;
	}
	
	public static JSONObject getEmployeeBonusList(List<EmployeeBonusDO> employeeBonusList, String frequency, String period) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			if(frequency != null){
				resultJSON.put(CommonConstants.FREQUENCY, String.valueOf(frequency));
			}else{
				resultJSON.put(CommonConstants.FREQUENCY, String.valueOf(""));
			}
			
			if(period != null){
				resultJSON.put(CommonConstants.PERIOD, String.valueOf(period));
			}else{
				resultJSON.put(CommonConstants.PERIOD, String.valueOf(""));
			}
			
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeeBonusDO employeeBonusDO : employeeBonusList) {
				resultJSONArray.put(getEmployeeBonusDetailObject(employeeBonusDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject MultipleMonthBonuReport(List<EmployeeBonusDO> bonusList, String frequency, String period) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			if(frequency != null){
				resultJSON.put(CommonConstants.FREQUENCY, String.valueOf(frequency));
			}else{
				resultJSON.put(CommonConstants.FREQUENCY, String.valueOf(""));
			}
			
			if(period != null){
				resultJSON.put(CommonConstants.PERIOD, String.valueOf(period));
			}else{
				resultJSON.put(CommonConstants.PERIOD, String.valueOf(""));
			}
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeeBonusDO bonus : bonusList) {
				 /*Map<Long,Long> map=new HashMap<Long,Long>();
				 if(!map.containsKey(bonus.getEmpId())){*/
					 resultJSONArray.put(getEmployeeBonusDetailObject(bonus));
				 /*}*/
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getMultipleMonthBonusDetailObject(List<EmployeeBonusDO> bonusList, Long empId)throws JSONException {
		JSONObject result = new JSONObject();
		if(empId != null){
			List<EmployeeDO> empList = employeeService.retrieveEmpId(empId.toString());
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null)  result.put(CommonConstants.EMPNAME,  String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else if(empList.get(0).getMiddlename() != null && empList.get(0).getLastname() == null )  result.put(CommonConstants.EMPNAME, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename()));
				else if(empList.get(0).getMiddlename() == null && empList.get(0).getLastname() == null )  result.put(CommonConstants.EMPNAME,  String.valueOf(empList.get(0).getFirstname()));
				else  result.put(CommonConstants.EMPNAME, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			
			}else{
				result.put(CommonConstants.EMPNAME, ""); 
			}
		}else{
			result.put(CommonConstants.EMPNAME, ""); 
		}
		Double amount = 0D;
		for(EmployeeBonusDO bonus : bonusList){
			if(bonus.getEmployee().getEmpId().equals(empId)){
				amount = amount+bonus.getAmount();
			}
		}
		result.put(CommonConstants.AMOUNT,String.valueOf(amount)); 
		return result;
	}

	public static JSONObject getEmployeeBonusDetailObject(EmployeeBonusDO employeeBonusDO)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(employeeBonusDO.getEmpBonusId()));
		if(employeeBonusDO.getEmployee().getEmpId() != null ){
			result.put(CommonConstants.EMPID, String.valueOf(employeeBonusDO.getEmployee().getEmpId()));
			if(employeeBonusDO.getEmployee().getMiddlename() != null) result.put(CommonConstants.EMPNAME, String.valueOf(employeeBonusDO.getEmployee().getFirstname() + " " +employeeBonusDO.getEmployee().getMiddlename() + " " + employeeBonusDO.getEmployee().getLastname()));
			else if(employeeBonusDO.getEmployee().getMiddlename() != null && employeeBonusDO.getEmployee().getLastname() == null ) result.put(CommonConstants.EMPNAME, String.valueOf(employeeBonusDO.getEmployee().getFirstname() + " " +employeeBonusDO.getEmployee().getMiddlename()));
			else if(employeeBonusDO.getEmployee().getMiddlename() == null && employeeBonusDO.getEmployee().getLastname() == null ) result.put(CommonConstants.EMPNAME,String.valueOf(employeeBonusDO.getEmployee().getFirstname()));
			else result.put(CommonConstants.EMPNAME, String.valueOf(employeeBonusDO.getEmployee().getFirstname() + " " + employeeBonusDO.getEmployee().getLastname()));
		}else{
			result.put(CommonConstants.EMPID, "");
			result.put(CommonConstants.EMPNAME, ""); 
		}
		result.put(CommonConstants.AMOUNT, String.valueOf(employeeBonusDO.getAmount()));
		result.put(CommonConstants.BONUSMONTH, String.valueOf(employeeBonusDO.getBonusMonth()));
		if(employeeBonusDO.getPaidOn() != null){
			result.put(CommonConstants.PAIDON, String.valueOf(employeeBonusDO.getPaidOn()));
		}else{
			result.put(CommonConstants.PAIDON, "");
		}
		result.put(CommonConstants.COMMENT, String.valueOf(employeeBonusDO.getComment() != null ? employeeBonusDO.getComment() : ""));
		result.put(CommonConstants.STATUS, String.valueOf(employeeBonusDO.getStatus()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(employeeBonusDO.getUpdatedon())));
		if(employeeBonusDO.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(employeeBonusDO.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		return result;
	}
}
