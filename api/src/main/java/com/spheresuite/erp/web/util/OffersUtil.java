package com.spheresuite.erp.web.util;

import java.util.List;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.crm.service.LeadService;
import com.spheresuite.erp.crm.service.OpportunitiesService;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.LeadDO;
import com.spheresuite.erp.domainobject.OffersDO;
import com.spheresuite.erp.domainobject.OpportunitiesDO;
import com.spheresuite.erp.domainobject.RequirementsDO;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.RequirementsService;
import com.spheresuite.erp.util.CommonConstants;
@Component
public class OffersUtil {
	
	private OffersUtil() {}
	
	@Autowired
	private LeadService tleadService;
	private static LeadService leadService;

	@Autowired
	private OpportunitiesService topportunitiesService;
	private static OpportunitiesService opportunitiesService;
	
	@Autowired
	private RequirementsService trequirementsService;
	private static RequirementsService requirementsService;
	
	@Autowired
	private EmployeeService temployeeService;
	private static EmployeeService employeeService;
	
	@PostConstruct
	public void init() {
		employeeService = temployeeService;
		leadService = tleadService;
		opportunitiesService = topportunitiesService;
		requirementsService = trequirementsService;
	}
	
	public static JSONObject getOffersList(List<OffersDO> offersList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (OffersDO offersDO : offersList) {
				resultJSONArray.put(getOffersDetailObject(offersDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getOffersDetailObject(OffersDO offersDO)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(offersDO.getId() != null ? offersDO.getId() : ""));
		result.put(CommonConstants.PROJECTID, String.valueOf(offersDO.getProjectid() != null ? offersDO.getProjectid() : ""));
		result.put(CommonConstants.CUSTOMERID, String.valueOf(offersDO.getCustomerid() != null ? offersDO.getCustomerid() : ""));
		result.put(CommonConstants.OFFERDATE, String.valueOf(offersDO.getOfferDate() != null ? offersDO.getOfferDate() : ""));
		result.put(CommonConstants.NAME, String.valueOf(offersDO.getName() != null ? offersDO.getName() : ""));
		result.put(CommonConstants.DESIGNATION, String.valueOf(offersDO.getDesg() != null ? offersDO.getDesg() : ""));
		result.put(CommonConstants.JOININGDATE, String.valueOf(offersDO.getJoiningDate() != null ? offersDO.getJoiningDate() : ""));
		result.put(CommonConstants.COST, String.valueOf(offersDO.getCtc() != null ? offersDO.getCtc() : ""));
		result.put(CommonConstants.COMMENT, String.valueOf(offersDO.getComments() != null ? offersDO.getComments() : ""));
		result.put(CommonConstants.PHONE, String.valueOf(offersDO.getPhone() != null ? offersDO.getPhone() : ""));
		result.put(CommonConstants.ADDRESS, String.valueOf(offersDO.getAddresss() != null ? offersDO.getAddresss() : ""));
		result.put(CommonConstants.STATUS, String.valueOf(offersDO.getStatus()));
		result.put(CommonConstants.APPROVER_ID, String.valueOf(offersDO.getApproverid() != null ? offersDO.getApproverid() : ""));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(offersDO.getUpdatedon())));
	/*	if(offersDO.getUpdatedBy() != null){
			List<EmployeeDO> empList = employeeService.retriveByEmpId(Long.parseLong(offersDO.getUpdatedBy()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}*/
		
		if(offersDO.getUpdatedBy() != null){
			String empName = CommonUtil.getEmpObject(offersDO.getUpdatedBy());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		
		if(offersDO.getProjectid() != null){
			List<OpportunitiesDO> opportunitiesList = opportunitiesService.retrieveById(offersDO.getProjectid());
			if(opportunitiesList != null && opportunitiesList.size() > 0){
				result.put(CommonConstants.PROJECTNAME, opportunitiesList.get(0).getProjectname());
			}else{
				result.put(CommonConstants.PROJECTNAME, "");
			}
		}else{
			result.put(CommonConstants.PROJECTNAME, "");
		}
		
		if(offersDO.getRequirementid() != null){
			List<RequirementsDO> requirementList = requirementsService.retrieveById(offersDO.getRequirementid());
			result.put(CommonConstants.REQUIREMENT_ID, String.valueOf(offersDO.getRequirementid()));
			if(requirementList != null && requirementList.size() > 0){
				result.put(CommonConstants.REQUIREMENT, requirementList.get(0).getName());
			}else{
				result.put(CommonConstants.REQUIREMENT, "");
			}
		}else{
			result.put(CommonConstants.REQUIREMENT, "");
			result.put(CommonConstants.REQUIREMENT_ID, "");
		}
		
		if(offersDO.getCustomerid() != null){
			List<LeadDO> leadList = leadService.retrieveById(offersDO.getCustomerid());
			if(leadList != null && leadList.size() > 0){
				result.put(CommonConstants.CUSTOMERNAME, leadList.get(0).getName());	
			}else{
				result.put(CommonConstants.CUSTOMERNAME, "");	
			}
		}else{
			result.put(CommonConstants.CUSTOMERNAME, "");
		}
		
		if(offersDO.getApproverid() != null){
			List<EmployeeDO> empList = employeeService.retrieveEmpId(offersDO.getApproverid().toString());
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.APPROVER, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.APPROVER, ""); 
			}
		}else{
			result.put(CommonConstants.APPROVER, ""); 
		}
		
		return result;
	}
}
