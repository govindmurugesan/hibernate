package com.spheresuite.erp.web.util;

import java.util.List;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.AdvancePaymentDetailDO;
import com.spheresuite.erp.domainobject.InsurancePaymentDO;
import com.spheresuite.erp.domainobject.InsurancePaymentDetailDO;
import com.spheresuite.erp.service.InsurancePaymentDetailService;
import com.spheresuite.erp.util.CommonConstants;
@Component
public class InsurancePaymentUtil {
	
	private InsurancePaymentUtil() {}
	
	@Autowired
	private InsurancePaymentDetailService tinsurancePaymentDetailService;
	private static InsurancePaymentDetailService insurancePaymentDetailService;
	
	@PostConstruct
	public void init() {
		insurancePaymentDetailService = tinsurancePaymentDetailService;
	}
	

	public static JSONObject getInsurancePaymentList(List<InsurancePaymentDO> insurancePaymentList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (InsurancePaymentDO insurancePayment : insurancePaymentList) {
				resultJSONArray.put(getInsurancePaymentDetailObject(insurancePayment));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getInsurancePaymentDetailObject(InsurancePaymentDO insurancePayment)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(insurancePayment.getInsurancePaymentId()));
		if(insurancePayment.getEmployee().getEmpId() != null ){
			result.put(CommonConstants.EMPID, String.valueOf(insurancePayment.getEmployee().getEmpId()));
			if(insurancePayment.getEmployee().getMiddlename() != null) result.put(CommonConstants.EMPNAME, String.valueOf(insurancePayment.getEmployee().getFirstname() + " " +insurancePayment.getEmployee().getMiddlename() + " " + insurancePayment.getEmployee().getLastname()));
			else if(insurancePayment.getEmployee().getMiddlename() != null && insurancePayment.getEmployee().getLastname() == null ) result.put(CommonConstants.EMPNAME, String.valueOf(insurancePayment.getEmployee().getFirstname() + " " +insurancePayment.getEmployee().getMiddlename()));
			else if(insurancePayment.getEmployee().getMiddlename() == null && insurancePayment.getEmployee().getLastname() == null ) result.put(CommonConstants.EMPNAME,String.valueOf(insurancePayment.getEmployee().getFirstname()));
			else result.put(CommonConstants.EMPNAME, String.valueOf(insurancePayment.getEmployee().getFirstname() + " " + insurancePayment.getEmployee().getLastname()));
		}else{
			result.put(CommonConstants.EMPID, "");
			result.put(CommonConstants.EMPNAME, ""); 
		}
		result.put(CommonConstants.AMOUNT, String.valueOf(insurancePayment.getAmount()));
		if(insurancePayment.getMonth() != null){
			result.put(CommonConstants.MONTHLY, String.valueOf(insurancePayment.getMonth()));
		}else{
			result.put(CommonConstants.MONTHLY, "");
		}
		if(insurancePayment.getInsuranceType() != null){
			result.put(CommonConstants.TYPE_ID, String.valueOf(insurancePayment.getInsuranceType().getInsuranceTypeId()));
			result.put(CommonConstants.TYPE, String.valueOf(insurancePayment.getInsuranceType().getName()));
		}else{
			result.put(CommonConstants.TYPE_ID, "");
			result.put(CommonConstants.TYPE, "");
		}
		
		if(insurancePayment.getInstallmentAmount() != null){
			result.put(CommonConstants.INSTALLMENTAOUNT, String.valueOf(insurancePayment.getInstallmentAmount()));
		}else{
			result.put(CommonConstants.INSTALLMENTAOUNT, "");
		}
		if(insurancePayment.getNoOfInstallments() != null){
			result.put(CommonConstants.NOOFINSTALLMENT, String.valueOf(insurancePayment.getNoOfInstallments()));
		}else{
			result.put(CommonConstants.NOOFINSTALLMENT, "");
		}
		result.put(CommonConstants.STATUS, String.valueOf(insurancePayment.getStatus()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(insurancePayment.getUpdatedon())));
		if(insurancePayment.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(insurancePayment.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		
		Double pendingAmount = 0d;
		List<InsurancePaymentDetailDO> paymentDetailList = insurancePaymentDetailService.getByAdvanceId(insurancePayment.getInsurancePaymentId());
		if(paymentDetailList != null && paymentDetailList.size() > 0){
			JSONArray resultJSONArray = new JSONArray();
			for (InsurancePaymentDetailDO paymentDetail : paymentDetailList) {
				if(paymentDetail != null && paymentDetail.getAmount() != null &&  paymentDetail.getStatus().equalsIgnoreCase(CommonConstants.ACTIVESTRING)){
					pendingAmount += Double.parseDouble(paymentDetail.getAmount().toString());
				}
				resultJSONArray.put(getPaymentDetailObject(paymentDetail));
			}
			result.put(CommonConstants.INSURANCEDETAIL, resultJSONArray);
			result.put(CommonConstants.PENDINGAMOUNT, pendingAmount);
		}
		
		return result;
	}
	
	
	public static JSONObject getPaymentDetailObject(InsurancePaymentDetailDO paymentDetail)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(paymentDetail.getInsurancePaymentDetailId()));
		result.put(CommonConstants.AMOUNT, String.valueOf(paymentDetail.getAmount()));
		if(paymentDetail.getMonth() != null){
			result.put(CommonConstants.MONTH, String.valueOf(paymentDetail.getMonth()));
		}else{
			result.put(CommonConstants.MONTH, "");
		}
		result.put(CommonConstants.STATUS, String.valueOf(paymentDetail.getStatus()));
		return result;
	}
}
