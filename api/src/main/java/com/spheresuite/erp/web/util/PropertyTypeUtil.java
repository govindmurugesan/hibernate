package com.spheresuite.erp.web.util;

import java.util.List;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.PropertyTypeDO;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.util.CommonConstants;
@Component
public class PropertyTypeUtil {
	
	private PropertyTypeUtil() {}
	
	@Autowired
	private EmployeeService employeeServiceTemp;
	
	private static EmployeeService employeeService;
	
	@PostConstruct
	public void init() {
		employeeService = this.employeeServiceTemp;
	}
	
	public static JSONObject getPropertyTypeList(List<PropertyTypeDO> propertyTypeList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (PropertyTypeDO propertyTypeDO : propertyTypeList) {
				resultJSONArray.put(getPropertyTypeDetailObject(propertyTypeDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getPropertyTypeDetailObject(PropertyTypeDO propertyTypeDO)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(propertyTypeDO.getId()));
		result.put(CommonConstants.NAME, String.valueOf(propertyTypeDO.getName()));
		result.put(CommonConstants.STATUS, String.valueOf(propertyTypeDO.getStatus()));
		/*if(propertyTypeDO.getUpdatedBy() != null){
			
			List<EmployeeDO> empList = employeeService.retriveByEmpId(Long.parseLong(propertyTypeDO.getUpdatedBy()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}*/
		if(propertyTypeDO.getUpdatedBy() != null){
			String empName = CommonUtil.getEmpObject(propertyTypeDO.getUpdatedBy());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(propertyTypeDO.getUpdatedon())));
		return result;
	}
}
