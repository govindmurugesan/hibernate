package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.EmployeeDriverLicenseDO;
import com.spheresuite.erp.util.CommonConstants;
@Component
public class EmployeeDriverLicenseUtil {
	
	private EmployeeDriverLicenseUtil() {}
	
	public static JSONObject getEmployeeLicenseList(List<EmployeeDriverLicenseDO> employeeDriverLicenseList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeeDriverLicenseDO employeeDriverLicenseDO : employeeDriverLicenseList) {
				resultJSONArray.put(getEmployeeLicenseDetailObject(employeeDriverLicenseDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getEmployeeLicenseDetailObject(EmployeeDriverLicenseDO employeeDriverLicenseDO)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(employeeDriverLicenseDO.getEmpDriverLicenseId()));
		if(employeeDriverLicenseDO.getCountry() != null){
			result.put(CommonConstants.COUNTRY_ID, String.valueOf(employeeDriverLicenseDO.getCountry().getCountryId()));
			result.put(CommonConstants.COUNTRY_NAME, String.valueOf(employeeDriverLicenseDO.getCountry().getCountryName()));
		}else{
			result.put(CommonConstants.COUNTRY_ID, " ");
			result.put(CommonConstants.COUNTRY_NAME, "");
		}
		if(employeeDriverLicenseDO.getState() != null){
			result.put(CommonConstants.STATE_ID, String.valueOf(employeeDriverLicenseDO.getState().getStateId()));
			result.put(CommonConstants.STATE_NAME, String.valueOf(employeeDriverLicenseDO.getState().getStateName()));
		}else{
			result.put(CommonConstants.STATE_ID, " ");
			result.put(CommonConstants.STATE_NAME, "");
		}
		result.put(CommonConstants.CITY, String.valueOf(employeeDriverLicenseDO.getCity()));
		result.put(CommonConstants.LICENSENUMBER, String.valueOf(employeeDriverLicenseDO.getLicenseNumber()));
		result.put(CommonConstants.ADDRESS1, String.valueOf(employeeDriverLicenseDO.getAddress1()));
		result.put(CommonConstants.ADDRESS2, String.valueOf(employeeDriverLicenseDO.getAddress2()!=null?employeeDriverLicenseDO.getAddress2():""));
		result.put(CommonConstants.NAME, String.valueOf(employeeDriverLicenseDO.getName()));
		result.put(CommonConstants.DATEISSUEDNAME, String.valueOf(CommonUtil.convertDateToStringWithdatetime(CommonUtil.convertStringToSqlDate(employeeDriverLicenseDO.getDateIssued().substring(0,10)))));
		result.put(CommonConstants.DATEISSUED, String.valueOf(employeeDriverLicenseDO.getDateIssued()));
		result.put(CommonConstants.EXPIRYDATE, String.valueOf(employeeDriverLicenseDO.getExpiryDate()));
		result.put(CommonConstants.EXPIRYDATENAME,  String.valueOf(CommonUtil.convertDateToStringWithdatetime(CommonUtil.convertStringToSqlDate(employeeDriverLicenseDO.getExpiryDate().substring(0,10)))));
		result.put(CommonConstants.EMPID, String.valueOf(employeeDriverLicenseDO.getEmployee().getEmailId()));
		if(employeeDriverLicenseDO.getZip() != null){
			result.put(CommonConstants.ZIP, String.valueOf(employeeDriverLicenseDO.getZip()));
		}else{
			result.put(CommonConstants.ZIP, "");
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(employeeDriverLicenseDO.getUpdatedon())));
		if(employeeDriverLicenseDO.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(employeeDriverLicenseDO.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		return result;
	}
}
