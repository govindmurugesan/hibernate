package com.spheresuite.erp.web.util;

import java.util.List;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.MenuDO;
import com.spheresuite.erp.service.MenuService;
import com.spheresuite.erp.util.CommonConstants;
@Component
public class MenuUtil {
	
	private MenuUtil() {}
	
	@Autowired
	private MenuService tmenuService;
	private static MenuService menuService;
	@PostConstruct
	public void init() {
		menuService = tmenuService;
	}
	
	public static JSONObject getMenuList1(List<MenuDO> menuList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (MenuDO menu : menuList) {
				resultJSONArray.put(getMenuDetailObject1(menu));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getMenuListforRole(List<MenuDO> menuList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (MenuDO menu : menuList) {
				resultJSONArray.put(getMenuListforRoleObject(menu));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	
	public static JSONObject getMenuListforRoleObject(MenuDO menu)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(menu.getMenuId()));
		result.put(CommonConstants.NAME, String.valueOf(menu.getName()));
		if(menu.getUrl() != null){
			result.put(CommonConstants.URL, String.valueOf(menu.getUrl()));
		}
		if(menu.getIcon() != null){
			result.put(CommonConstants.ICON, String.valueOf(menu.getIcon()));
		}
		
		if(menu.getActiveicon() != null){
			result.put(CommonConstants.ACTIVEICON, String.valueOf(menu.getActiveicon()));
		}else{
			result.put(CommonConstants.ACTIVEICON, "");
		}
		if(menu.getSupermenu() != null){
			result.put(CommonConstants.SUPERMENU, String.valueOf(menu.getSupermenu()));
		}else{
			result.put(CommonConstants.SUPERMENU, "");
		}
		if(menu.getProduct() != null){
			result.put(CommonConstants.PRODUCT_ID, String.valueOf(menu.getProduct()));
		}else{
			result.put(CommonConstants.PRODUCT_ID, "");
		}
		JSONArray resultJSONArraySubMenu = new JSONArray();
	//	if(menu.getSupermenu() == null || menu.getSupermenu() == Long.parseLong("1")){
			List<MenuDO> submenuList = menuService.retrieveByProductId(menu.getMenuId());
			if(submenuList != null && submenuList.size() > 0){
				for(MenuDO subMenu : submenuList){
					if(subMenu.getSupermenu() == null || subMenu.getSupermenu() == Long.parseLong("1")){
						resultJSONArraySubMenu.put(getSubMenuListforRoleObject(subMenu));
					}
				}
			}
	//	}
		result.put(CommonConstants.SUB_MENU,resultJSONArraySubMenu);
		return result;
	}

	
	public static JSONObject getSubMenuListforRoleObject(MenuDO subMenu)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(subMenu.getMenuId()));
		result.put(CommonConstants.NAME, String.valueOf(subMenu.getName()));
		if(subMenu.getUrl() != null){
			result.put(CommonConstants.URL, String.valueOf(subMenu.getUrl()));
		}
		if(subMenu.getIcon() != null){
			result.put(CommonConstants.ICON, String.valueOf(subMenu.getIcon()));
		}
		
		if(subMenu.getActiveicon() != null){
			result.put(CommonConstants.ACTIVEICON, String.valueOf(subMenu.getActiveicon()));
		}else{
			result.put(CommonConstants.ACTIVEICON, "");
		}
		
		if(subMenu.getProduct() != null){
			result.put(CommonConstants.PRODUCT_ID, String.valueOf(subMenu.getProduct()));
		}else{
			result.put(CommonConstants.PRODUCT_ID, "");
		}
		
		JSONArray resultJSONArraySubMenu = new JSONArray();
		if(subMenu.getSupermenu() == null || subMenu.getSupermenu() == Long.parseLong("1")){
			List<MenuDO> submenuList = menuService.retrieveSuperMenu(subMenu.getMenuId());
			if(submenuList != null && submenuList.size() > 0){
				for(MenuDO subMenu1 : submenuList){
					resultJSONArraySubMenu.put(getSubMenuDetailObject(subMenu1));
				}
			}
		}
		result.put(CommonConstants.SUB_SUB_MENU,resultJSONArraySubMenu);
		return result;
	}
	
	public static JSONObject getMenuDetailObject1(MenuDO menu)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(menu.getMenuId()));
		result.put(CommonConstants.NAME, String.valueOf(menu.getName()));
		if(menu.getUrl() != null){
			result.put(CommonConstants.URL, String.valueOf(menu.getUrl()));
		}
		if(menu.getIcon() != null){
			result.put(CommonConstants.ICON, String.valueOf(menu.getIcon()));
		}
		if(menu.getProduct() != null){
			result.put(CommonConstants.PRODUCT_ID, String.valueOf(menu.getProduct()));
		}else{
			result.put(CommonConstants.PRODUCT_ID, "");
		}
		if(menu.getActiveicon() != null){
			result.put(CommonConstants.ACTIVEICON, String.valueOf(menu.getActiveicon()));
		}else{
			result.put(CommonConstants.ACTIVEICON, "");
		}
		JSONArray resultJSONArraySubMenu = new JSONArray();
		List<MenuDO> submenuList = menuService.retrieveByProductId(menu.getMenuId());
		if(submenuList != null && submenuList.size() > 0){
			for(MenuDO subMenu : submenuList){
				resultJSONArraySubMenu.put(getSubMenuDetailObject1(subMenu));
			}
		}
		result.put(CommonConstants.SUB_MENU,resultJSONArraySubMenu);
		return result;
	}
	
	public static JSONObject getSubMenuDetailObject1(MenuDO subMenu)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(subMenu.getMenuId()));
		result.put(CommonConstants.NAME, String.valueOf(subMenu.getName()));
		if(subMenu.getUrl() != null){
			result.put(CommonConstants.URL, String.valueOf(subMenu.getUrl()));
		}
		if(subMenu.getIcon() != null){
			result.put(CommonConstants.ICON, String.valueOf(subMenu.getIcon()));
		}
		
		if(subMenu.getActiveicon() != null){
			result.put(CommonConstants.ACTIVEICON, String.valueOf(subMenu.getActiveicon()));
		}else{
			result.put(CommonConstants.ACTIVEICON, "");
		}
		
		if(subMenu.getProduct() != null){
			result.put(CommonConstants.PRODUCT_ID, String.valueOf(subMenu.getProduct()));
		}else{
			result.put(CommonConstants.PRODUCT_ID, "");
		}
		
		/*JSONArray resultJSONArraySubMenu = new JSONArray();
		List<MenuDO> submenuList = menuService.retrieveByProductId(subMenu.getMenuId());
		if(submenuList != null && submenuList.size() > 0){
			for(MenuDO subMenu1 : submenuList){
				resultJSONArraySubMenu.put(getSubMenuDetailObject(subMenu1));
			}
		}
		result.put(CommonConstants.SUB_SUB_MENU,resultJSONArraySubMenu);*/
		return result;
	}
	
	
	public static JSONObject getMenuList(List<MenuDO> menuList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (MenuDO menu : menuList) {
				if(menu.getSupermenu() == null || menu.getSupermenu() == Long.parseLong("1")){
					resultJSONArray.put(getMenu(menu));
				}
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getMenu(MenuDO menu)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(menu.getMenuId()));
		result.put(CommonConstants.NAME, String.valueOf(menu.getName()));
		result.put(CommonConstants.DISPLAYNAME, String.valueOf(menu.getDisplayName()));
		if(menu.getUrl() != null){
			result.put(CommonConstants.URL, String.valueOf(menu.getUrl()));
		}
		if(menu.getIcon() != null){
			result.put(CommonConstants.ICON, String.valueOf(menu.getIcon()));
		}
		if(menu.getSupermenu() != null){
			result.put(CommonConstants.SUPERMENU, String.valueOf(menu.getSupermenu()));
		}else{
			result.put(CommonConstants.SUPERMENU, "");
		}
		if(menu.getActiveicon() != null){
			result.put(CommonConstants.ACTIVEICON, String.valueOf(menu.getActiveicon()));
		}else{
			result.put(CommonConstants.ACTIVEICON, "");
		}
		if(menu.getOrderId() != null){
			result.put(CommonConstants.ORDERNUMBER, String.valueOf(menu.getOrderId()));
		}else{
			result.put(CommonConstants.ORDERNUMBER, "");
		}
		if(menu.getProduct() != null){
			result.put(CommonConstants.PRODUCT_ID, String.valueOf(menu.getProduct()));
		}else{
			result.put(CommonConstants.PRODUCT_ID, "");
		}
		JSONArray resultJSONArraySubMenu = new JSONArray();
		if(menu.getSupermenu() != null ){
			List<MenuDO> submenuList = menuService.retrieveSuperMenu(menu.getMenuId());
			if(submenuList != null && submenuList.size() > 0){
				for(MenuDO subMenu : submenuList){
					resultJSONArraySubMenu.put(getSubMenuDetailObject(subMenu));
				}
			}
		}
		result.put(CommonConstants.SUB_MENU,resultJSONArraySubMenu);
		return result;
	}
	
	public static JSONObject getSubMenuDetailObject(MenuDO subMenu)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(subMenu.getMenuId()));
		result.put(CommonConstants.NAME, String.valueOf(subMenu.getName()));
		if(subMenu.getUrl() != null){
			result.put(CommonConstants.URL, String.valueOf(subMenu.getUrl()));
		}
		if(subMenu.getIcon() != null){
			result.put(CommonConstants.ICON, String.valueOf(subMenu.getIcon()));
		}
		if(subMenu.getSupermenu() != null){
			result.put(CommonConstants.SUPERMENU, String.valueOf(subMenu.getSupermenu()));
		}else{
			result.put(CommonConstants.SUPERMENU, "");
		}
		if(subMenu.getActiveicon() != null){
			result.put(CommonConstants.ACTIVEICON, String.valueOf(subMenu.getActiveicon()));
		}else{
			result.put(CommonConstants.ACTIVEICON, "");
		}
		if(subMenu.getOrderId() != null){
			result.put(CommonConstants.ORDERNUMBER, String.valueOf(subMenu.getOrderId()));
		}else{
			result.put(CommonConstants.ORDERNUMBER, "");
		}
		if(subMenu.getProduct() != null){
			result.put(CommonConstants.PRODUCT_ID, String.valueOf(subMenu.getProduct()));
		}else{
			result.put(CommonConstants.PRODUCT_ID, "");
		}
		return result;
	}
}
