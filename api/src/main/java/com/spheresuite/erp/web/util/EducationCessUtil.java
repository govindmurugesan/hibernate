package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.EducationCessDO;
import com.spheresuite.erp.util.CommonConstants;
@Component
public class EducationCessUtil {
	
	private EducationCessUtil() {}
	
	public static JSONObject getEducationCessList(List<EducationCessDO> educationCessList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EducationCessDO educationCes : educationCessList) {
				resultJSONArray.put(getEducationCessDetailObject(educationCes));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getEducationCessDetailObject(EducationCessDO educationCes)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(educationCes.getEducationCessId()));
		result.put(CommonConstants.FROMDATE, String.valueOf(educationCes.getFromYear()));
		
		if(educationCes.getToYear() != null){
			result.put(CommonConstants.TODATE, String.valueOf(educationCes.getToYear()));
		}else{
			result.put(CommonConstants.TODATE, String.valueOf(""));
		}
		result.put(CommonConstants.PERCENTAGE, String.valueOf(educationCes.getPercentage()));
		result.put(CommonConstants.STATUS, String.valueOf(educationCes.getStatus()));
		if(educationCes.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(educationCes.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(educationCes.getUpdatedon())));

		return result;
	}
}
