package com.spheresuite.erp.web.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.AllowanceSettingDO;
import com.spheresuite.erp.domainobject.AttendanceAllowanceDO;
import com.spheresuite.erp.domainobject.DeductionSettingDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.EmployeePayrollDO;
import com.spheresuite.erp.domainobject.GeneralDeductionDetailDO;
import com.spheresuite.erp.domainobject.GeneralEarningDO;
import com.spheresuite.erp.domainobject.PayrollBatchDO;
import com.spheresuite.erp.domainobject.PayrollDeductionDetailsDO;
import com.spheresuite.erp.domainobject.PayrollEarningsDetailsDO;
import com.spheresuite.erp.domainobject.PayrollTaxDetailsDO;
import com.spheresuite.erp.domainobject.TaxSettingsDO;
import com.spheresuite.erp.domainobject.WorkingDayAllowanceDO;
import com.spheresuite.erp.service.AllowanceSettingsService;
import com.spheresuite.erp.service.DeductionSettingsService;
import com.spheresuite.erp.service.EmployeeBonusService;
import com.spheresuite.erp.service.EmployeeCtcService;
import com.spheresuite.erp.service.EmployeeLopService;
import com.spheresuite.erp.service.EmployeePayrollService;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.GeneralDeductionDetailService;
import com.spheresuite.erp.service.GeneralEarningService;
import com.spheresuite.erp.service.PayrollBatchService;
import com.spheresuite.erp.service.PayrollDeductionDetailsService;
import com.spheresuite.erp.service.PayrollEarningsDetailsService;
import com.spheresuite.erp.service.PayrollLopDetailsService;
import com.spheresuite.erp.service.PayrollTaxDetailsService;
import com.spheresuite.erp.service.TaxSettingsService;
import com.spheresuite.erp.util.CommonConstants;
@Component
public class EmployeePayrollUtil {
	
	private EmployeePayrollUtil() {}
	
	@Autowired
	private EmployeeService temployeeService;
	private static EmployeeService employeeService;
	
	@Autowired
	private PayrollLopDetailsService tpayrollLopDetailsService;
	private static PayrollLopDetailsService payrollLopDetailsService;

	@Autowired
	private EmployeePayrollService temployeePayrollService;
	private static EmployeePayrollService employeePayrollService;
	
	@Autowired
	private EmployeeBonusService temployeeBonusService;
	private static EmployeeBonusService employeeBonusService;
	
	@Autowired
	private  EmployeeLopService temployeeLopService;
	private static EmployeeLopService employeeLopService;
	
	@Autowired
	private  AllowanceSettingsService tallowanceSettingsService;
	private static AllowanceSettingsService allowanceSettingsService;
	
	@Autowired
	private  DeductionSettingsService tdeductionSettingsService;
	private static DeductionSettingsService deductionSettingsService;
	
	@Autowired
	private  TaxSettingsService ttaxSettingsService;
	private static TaxSettingsService taxSettingsService;
	
	@Autowired
	private PayrollBatchService tpayrollBatchService;
	private static PayrollBatchService payrollBatchService;
	
	@Autowired
	private EmployeeCtcService temployeeCtcService;
	private static EmployeeCtcService employeeCtcService;
	
	@Autowired
	private PayrollEarningsDetailsService tpayrollEarningsDetailsService;
	private static PayrollEarningsDetailsService payrollEarningsDetailsService;
	
	@Autowired
	private PayrollTaxDetailsService tpayrollTaxDetailsService;
	private static PayrollTaxDetailsService payrollTaxDetailsService;
	
	@Autowired
	private PayrollDeductionDetailsService tpayrollDeductionDetailsService;
	private static PayrollDeductionDetailsService payrollDeductionDetailsService;
	
	@Autowired
	private GeneralDeductionDetailService tgeneralDeductionDetailsService;
	private static GeneralDeductionDetailService generalDeductionDetailsService;
	
	@Autowired
	private GeneralEarningService tgeneralEarningService;
	private static GeneralEarningService generalEarningService;
	
	@PostConstruct
	public void init() {
		payrollLopDetailsService=tpayrollLopDetailsService;
		employeeCtcService = temployeeCtcService;
		employeePayrollService = temployeePayrollService;
		employeeBonusService = temployeeBonusService;
		employeeLopService = temployeeLopService;
		taxSettingsService = ttaxSettingsService;
		deductionSettingsService = tdeductionSettingsService;
		allowanceSettingsService = tallowanceSettingsService;
		employeeService = temployeeService;
		payrollBatchService =tpayrollBatchService;
		payrollEarningsDetailsService=tpayrollEarningsDetailsService;
		payrollTaxDetailsService=tpayrollTaxDetailsService;
		payrollDeductionDetailsService=tpayrollDeductionDetailsService;
		generalDeductionDetailsService = tgeneralDeductionDetailsService;
		generalEarningService = tgeneralEarningService;
				
	}
	
	public static JSONObject getPayrollDetail(List<EmployeePayrollDO> employeePayrollList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeePayrollDO empCompensation : employeePayrollList) {
					resultJSONArray.put(getPayrollDetail(empCompensation));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getPayrollDetail(EmployeePayrollDO payroll)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(payroll.getPayrollId()));
		result.put(CommonConstants.EMPID, String.valueOf(payroll.getEmployee().getEmpId()));
		if(payroll.getEmployee().getMiddlename() != null) result.put(CommonConstants.EMPNAME, String.valueOf(payroll.getEmployee().getFirstname() + " " +payroll.getEmployee().getMiddlename() + " " + payroll.getEmployee().getLastname()));
		else if(payroll.getEmployee().getMiddlename() != null && payroll.getEmployee().getLastname() == null ) result.put(CommonConstants.EMPNAME, String.valueOf(payroll.getEmployee().getFirstname() + " " +payroll.getEmployee().getMiddlename()));
		else if(payroll.getEmployee().getMiddlename() == null && payroll.getEmployee().getLastname() == null ) result.put(CommonConstants.EMPNAME,String.valueOf(payroll.getEmployee().getFirstname()));
		else result.put(CommonConstants.EMPNAME, String.valueOf(payroll.getEmployee().getFirstname() + " " + payroll.getEmployee().getLastname()));
		result.put(CommonConstants.BASICGROSS_PAY, String.valueOf(Math.round(payroll.getGrossPay())));
		if(payroll.getPayrollWeekTo() != null){
			result.put(CommonConstants.PAYROLLWEEKTO, String.valueOf(payroll.getPayrollWeekTo()));
		}else{
			result.put(CommonConstants.PAYROLLWEEKTO, String.valueOf(""));
		}
		if(payroll.getPayrollWeekFrom() != null){
			result.put(CommonConstants.PAYROLLWEEKFROM, String.valueOf(payroll.getPayrollWeekFrom()));
		}else{
			result.put(CommonConstants.PAYROLLWEEKFROM, String.valueOf(""));
		}
		if(payroll.getPayrollDate() != null){
			result.put(CommonConstants.PAYROLLDATE, String.valueOf(payroll.getPayrollDate()));
		}else{
			result.put(CommonConstants.PAYROLLDATE, String.valueOf(""));
		}
		result.put(CommonConstants.EARNINGMONTHLY, String.valueOf(Math.round(payroll.getEarningmonth())));
		result.put(CommonConstants.DEDUCTIONMONTHLY, String.valueOf(Math.round(payroll.getDeductionmonth())));
		result.put(CommonConstants.TAXMONTHLY, String.valueOf(Math.round(payroll.getTaxMonth())));
		result.put(CommonConstants.NETMONTHLY, String.valueOf(Math.round(payroll.getNetmonth())));
		result.put(CommonConstants.STATUS, String.valueOf(payroll.getStatus()));
		result.put(CommonConstants.PAYROLLBATCHID, String.valueOf(payroll.getPayrollBatch().getPayrollBatchId()));
		result.put(CommonConstants.PAYROLLBATCHNAME, String.valueOf(payroll.getPayrollBatch().getName()));
		result.put(CommonConstants.PAYROLLMONTH, String.valueOf(payroll.getPayrollMonth()));
		return result;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public static JSONObject getempPayrollList(List<EmployeePayrollDO> employeePayrollList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeePayrollDO empCompensation : employeePayrollList) {
				//List<EmployeeDO> employeeList = employeeService.isEmpActive(empCompensation.getEmpId());
				//if(employeeList.get(0).getStatus() == 'a'){
					resultJSONArray.put(getPayrollDetailObject(empCompensation));
				//}
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject ObjectArray(List<Object[]> employeePayrollList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (Object[] empCompensation : employeePayrollList) {
					resultJSONArray.put(getObjectDetailObject(empCompensation));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject ObjectArrayByBatchId(List<Object[]> employeePayrollList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (Object[] empCompensation : employeePayrollList) {
					resultJSONArray.put(getObjectDetailObjectByBatchId(empCompensation));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject ObjectArrayByBatchId1(List<Object[]> employeePayrollList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (Object[] empCompensation : employeePayrollList) {
					resultJSONArray.put(getObjectDetailObjectByBatchId1(empCompensation));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject SingleMonthDeductionReport(List<EmployeePayrollDO> payrollList, String frequency, String period) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			if(frequency != null){
				resultJSON.put(CommonConstants.FREQUENCY, String.valueOf(frequency));
			}else{
				resultJSON.put(CommonConstants.FREQUENCY, String.valueOf(""));
			}
			
			if(period != null){
				resultJSON.put(CommonConstants.PERIOD, String.valueOf(period));
			}else{
				resultJSON.put(CommonConstants.PERIOD, String.valueOf(""));
			}
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeePayrollDO payroll : payrollList) {
					resultJSONArray.put(getSingleMonthDetailObject(payroll));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject YtdReport(List<EmployeePayrollDO> payrollList, String fromMonth, String frequency) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			if(frequency != null){
				resultJSON.put(CommonConstants.FREQUENCY, String.valueOf(frequency));
			}else{
				resultJSON.put(CommonConstants.FREQUENCY, String.valueOf(""));
			}
			
			if(fromMonth != null){
				resultJSON.put(CommonConstants.PERIOD, String.valueOf(fromMonth));
			}else{
				resultJSON.put(CommonConstants.PERIOD, String.valueOf(""));
			}
			JSONArray resultJSONArray = new JSONArray();
			Map<Long, Long> empids = new HashMap<Long,Long>();
			for (EmployeePayrollDO payroll : payrollList) {
				if(!empids.containsKey(payroll.getEmployee().getEmpId())){
				/*	empids.put(payroll.getEmployee().getEmpId(), payroll.getEmployee().getEmpId());*/
					resultJSONArray.put(getSingleMonthYtd(payroll, fromMonth));
				}
					
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject YtdForMultipleMonth(List<EmployeePayrollDO> payrollList, String fromMonth, String toMonth, String frequency) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			if(frequency != null){
				resultJSON.put(CommonConstants.FREQUENCY, String.valueOf(frequency));
			}else{
				resultJSON.put(CommonConstants.FREQUENCY, String.valueOf(""));
			}
			
			resultJSON.put(CommonConstants.PERIOD, String.valueOf(fromMonth+" - "+toMonth));
			JSONArray resultJSONArray = new JSONArray();
			Map<Long, Long> empids = new HashMap<Long,Long>();
			for (EmployeePayrollDO payroll : payrollList) {
				if(!empids.containsKey(payroll.getEmployee().getEmpId())){
					//empids.put(payroll.getEmployee().getEmpId(), payroll.getEmployee().getEmpId());
					resultJSONArray.put(getMultipleMonthYtd(payroll, fromMonth, toMonth));
				}
					
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getMultipleMonthYtd(EmployeePayrollDO payroll, String fromMonth, String toMonth)throws JSONException {
		JSONObject result = new JSONObject();
		Long bonusMonth = 0L;
		/*List<EmployeeBonusDO> employeeBonusList = employeeBonusService.retrieveByEmpIdAndMonth(payroll.getEmpId(), fromMonth);
		if(employeeBonusList != null && employeeBonusList.size() > 0){
			bonusMonth = employeeBonusList.get(0).getAmount();
		}*/
		
		if(payroll.getEmployee().getEmpId() != null){
			result.put(CommonConstants.EMPID, String.valueOf(payroll.getEmployee().getEmpId()));
			List<EmployeeDO> empList = employeeService.retrieveEmpId(payroll.getEmployee().getEmpId().toString() );
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.EMPNAME, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.EMPNAME, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.EMPNAME, ""); 
			}
		}else{
			result.put(CommonConstants.EMPNAME, ""); 
			result.put(CommonConstants.EMPID, String.valueOf(""));
		}
		
		Object[] earningYear = employeePayrollService.retrieveByEmpIdBetweenDateSum(payroll.getEmployee().getEmpId().toString(), fromMonth, toMonth);
		if(earningYear != null){
			double eraningYear = 0D;
			double deductionYear = 0D;
			double taxYear = 0D;
			
			if(earningYear[1] != null){
				eraningYear = Double.parseDouble(earningYear[1].toString());
				result.put(CommonConstants.EARNINGYEAR,String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(eraningYear))));
			}else{
				eraningYear = bonusMonth;
				result.put(CommonConstants.EARNINGYEAR,String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(eraningYear))));
			}
			
			if(earningYear[2] != null){
				deductionYear = Double.parseDouble(earningYear[2].toString());
				result.put(CommonConstants.DEDUCTIONYEAR,String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(deductionYear))));
			}else{
				deductionYear = 0D;
				result.put(CommonConstants.DEDUCTIONYEAR,String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(deductionYear))));
			}
			
			if(earningYear[3] != null){
				taxYear = Double.parseDouble(earningYear[3].toString());
				result.put(CommonConstants.TAXYEAR,String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(Double.parseDouble(earningYear[3].toString())))));
			}else{
				taxYear = 0D;
				result.put(CommonConstants.TAXYEAR,String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(taxYear))));
			}
			result.put(CommonConstants.NETYEAR,String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(eraningYear - (deductionYear+taxYear)))));
			
		}else{
			result.put(CommonConstants.EARNINGYEAR,String.valueOf(""));
			result.put(CommonConstants.DEDUCTIONYEAR,String.valueOf(""));
			result.put(CommonConstants.TAXYEAR,String.valueOf(""));
			result.put(CommonConstants.NETYEAR,String.valueOf(""));
		}
		
		return result;
	}
	
	public static JSONObject getSingleMonthYtd(EmployeePayrollDO payroll, String fromMonth)throws JSONException {
		JSONObject result = new JSONObject();
		Long bonusMonth = 0L;
		/*List<EmployeeBonusDO> employeeBonusList = employeeBonusService.retrieveByEmpIdAndMonth(payroll.getEmpId(), fromMonth);
		if(employeeBonusList != null && employeeBonusList.size() > 0){
			bonusMonth = employeeBonusList.get(0).getAmount();
		}*/
		
		if(payroll.getEmployee().getEmpId() != null){
			result.put(CommonConstants.EMPID, String.valueOf(payroll.getEmployee().getEmpId()));
			List<EmployeeDO> empList = employeeService.retrieveEmpId(payroll.getEmployee().getEmpId().toString() );
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.EMPNAME, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.EMPNAME, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.EMPNAME, ""); 
			}
		}else{
			result.put(CommonConstants.EMPNAME, ""); 
			result.put(CommonConstants.EMPID, String.valueOf(""));
		}
		
		Object[] earningYear = employeePayrollService.retrieveByEmpIdForMonthSum(payroll.getEmployee().getEmpId(), fromMonth);
		if(earningYear != null){
			double eraningYear = 0D;
			double deductionYear = 0D;
			double taxYear = 0D;
			
			if(earningYear[1] != null){
				eraningYear = Double.parseDouble(earningYear[1].toString());
				result.put(CommonConstants.EARNINGYEAR,String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(eraningYear))));
			}else{
				eraningYear = bonusMonth;
				result.put(CommonConstants.EARNINGYEAR,String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(eraningYear))));
			}
			
			if(earningYear[2] != null){
				deductionYear = Double.parseDouble(earningYear[2].toString());
				result.put(CommonConstants.DEDUCTIONYEAR,String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(deductionYear))));
			}else{
				deductionYear = 0D;
				result.put(CommonConstants.DEDUCTIONYEAR,String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(deductionYear))));
			}
			
			if(earningYear[3] != null){
				taxYear = Double.parseDouble(earningYear[3].toString());
				result.put(CommonConstants.TAXYEAR,String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(Double.parseDouble(earningYear[3].toString())))));
			}else{
				taxYear = 0D;
				result.put(CommonConstants.TAXYEAR,String.valueOf(taxYear));
			}
			result.put(CommonConstants.NETYEAR,String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(eraningYear - (deductionYear+taxYear)))));
			
		}else{
			result.put(CommonConstants.EARNINGYEAR,String.valueOf(""));
			result.put(CommonConstants.DEDUCTIONYEAR,String.valueOf(""));
			result.put(CommonConstants.TAXYEAR,String.valueOf(""));
			result.put(CommonConstants.NETYEAR,String.valueOf(""));
		}
		
		return result;
	}
	
	public static JSONObject MultipleMonthDeductionReport(List<EmployeePayrollDO> payrollList, String fromMonth, String toMonth, String frequency) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			if(frequency != null){
				resultJSON.put(CommonConstants.FREQUENCY, String.valueOf(frequency));
			}else{
				resultJSON.put(CommonConstants.FREQUENCY, String.valueOf(""));
			}
			
			resultJSON.put(CommonConstants.PERIOD, String.valueOf(fromMonth +" - "+toMonth));
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeePayrollDO empdeduction : payrollList) {
				 Map<Long,Long> map=new HashMap<Long,Long>();
				 if(!map.containsKey(empdeduction.getEmployee().getEmpId())){
					 List<EmployeePayrollDO> compensationList = employeePayrollService.retrieveByEmpIdBetweenDate(empdeduction.getEmployee().getEmpId(), fromMonth, toMonth);
					 resultJSONArray.put(getMultipleMonthDetailObject(compensationList, fromMonth, toMonth));
				 }
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getMultipleMonthDetailObject(List<EmployeePayrollDO> empCompensation, String fromMonth, String toMonth)throws JSONException {
		JSONObject result = new JSONObject();
		Map<Long,Long> deduction = new HashMap<Long,Long>();
		if(empCompensation.get(0).getEmployee().getEmpId() != null){
			List<EmployeeDO> empList = employeeService.retrieveEmpId(empCompensation.get(0).getEmployee().getEmpId().toString() );
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.EMPNAME, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.EMPNAME, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.EMPNAME, ""); 
			}
		}else{
			result.put(CommonConstants.EMPNAME, ""); 
		}
		List<Long> payrollIds = new ArrayList<Long>();
		if(empCompensation != null && empCompensation.size() > 0){
			for (EmployeePayrollDO employeePayrollDO : empCompensation) {
				 payrollIds.add(employeePayrollDO.getPayrollId());
			}
		}
		 JSONArray resultJSONArray = new JSONArray();
		 List<PayrollDeductionDetailsDO> payrollDeductionList = payrollDeductionDetailsService.retrieveByEmpCompensationIds(payrollIds);
		 if(payrollDeductionList != null && payrollDeductionList.size() > 0){
			 for (PayrollDeductionDetailsDO payrollDeductionDetailsDO : payrollDeductionList) {
				 Double amount = 0D;
				 Long deductionId = 0L;
				 
				
				/* if(!deduction.containsKey(payrollDeductionDetailsDO.getDeductionId())){
					 deduction.put(payrollDeductionDetailsDO.getDeductionId(),payrollDeductionDetailsDO.getDeductionId());
					 deductionId = payrollDeductionDetailsDO.getDeductionId();
					 for (PayrollDeductionDetailsDO payrollDeductionDetails : payrollDeductionList) {
						 if(payrollDeductionDetails.getDeductionId().equals(deductionId)){
							 amount = amount + payrollDeductionDetails.getMonthly();
						 }
					 }
					 resultJSONArray.put(getDeuctionDetatilsWithAmoount(payrollDeductionDetailsDO, amount));
				 }*/
			}
			 JSONArray resultJSONArrayTemp = new JSONArray();
			 List<DeductionSettingDO> deductionList = deductionSettingsService.retrieve();
			 if(deductionList != null && deductionList.size() > 0){
					for (DeductionSettingDO deductionobj : deductionList) {
						JSONObject deductionResult = new JSONObject();
						if(resultJSONArray.length() <= 0){
							deductionResult.put(CommonConstants.AMOUNT, String.valueOf("0.00"));
							deductionResult.put(CommonConstants.DEDUCTIONID, String.valueOf(deductionobj.getDeductionsettingId()));
							deductionResult.put(CommonConstants.DEDUCTIONNAME, String.valueOf(deductionobj.getName()));
						}else{
							String amount="";
							int count = 0;
							for(int i= 0; i<resultJSONArray.length();i++){
								amount="";
								count = 0;
								JSONObject inputJSON1 = (JSONObject) resultJSONArray.get(i);
								if(deductionobj.getDeductionsettingId().equals(Long.parseLong(inputJSON1.get(CommonConstants.DEDUCTIONID).toString()))){
									count++;
									amount = inputJSON1.get(CommonConstants.AMOUNT).toString();
								}
							}
							if(count > 0){
								deductionResult.put(CommonConstants.AMOUNT, String.valueOf(amount));
								deductionResult.put(CommonConstants.DEDUCTIONID, String.valueOf(deductionobj.getDeductionsettingId()));
								deductionResult.put(CommonConstants.DEDUCTIONNAME, String.valueOf(deductionobj.getName()));
							}else{
								deductionResult.put(CommonConstants.AMOUNT, String.valueOf("0.00"));
								deductionResult.put(CommonConstants.DEDUCTIONID, String.valueOf(deductionobj.getDeductionsettingId()));
								deductionResult.put(CommonConstants.DEDUCTIONNAME, String.valueOf(deductionobj.getName()));
							}
						}
						resultJSONArrayTemp.put(deductionResult);
					}
					result.put(CommonConstants.DEDUCTIONLIST, resultJSONArrayTemp);
				}else{
					result.put(CommonConstants.DEDUCTIONLIST, resultJSONArrayTemp);
				}
			 
		 }else{
			 result.put(CommonConstants.DEDUCTIONLIST, "");
		 }
		return result;
	}
	
	public static JSONObject getSingleMonthDetailObject(EmployeePayrollDO empCompensation)throws JSONException {
		JSONObject result = new JSONObject();
		if(empCompensation != null){
			if(empCompensation.getEmployee().getEmpId() != null){
				List<EmployeeDO> empList = employeeService.retrieveEmpId(empCompensation.getEmployee().getEmpId().toString());
				if(empList != null && empList.size() > 0){
					if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.EMPNAME, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
					else result.put(CommonConstants.EMPNAME, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
				}else{
					result.put(CommonConstants.EMPNAME, ""); 
				}
			}else{
				result.put(CommonConstants.EMPNAME, ""); 
			}
			List<DeductionSettingDO> deductionList = deductionSettingsService.retrieve();
		//	List<PayrollDeductionDetailsDO> payrollDeductionList = payrollDeductionDetailsService.retrieveByEmpCompensationId(empCompensation.getId());
			JSONArray resultJSONArray = new JSONArray();
			JSONArray resultJSONArrayTemp = new JSONArray();
			/*if(payrollDeductionList != null && payrollDeductionList.size()  > 0){
				for (PayrollDeductionDetailsDO payrollDeductionDetailsDO : payrollDeductionList) {
					resultJSONArray.put(getDeuctionDetatils(payrollDeductionDetailsDO));
				}
				result.put(CommonConstants.DEDUCTIONLIST, resultJSONArrayTemp);
				
			}else{
				result.put(CommonConstants.DEDUCTIONLIST, resultJSONArrayTemp);
			}*/
			if(deductionList != null && deductionList.size() > 0){
				for (DeductionSettingDO deduction : deductionList) {
					JSONObject deductionResult = new JSONObject();
					if(resultJSONArray.length() <= 0){
						deductionResult.put(CommonConstants.AMOUNT, String.valueOf("0.00"));
						deductionResult.put(CommonConstants.DEDUCTIONID, String.valueOf(deduction.getDeductionsettingId()));
						deductionResult.put(CommonConstants.DEDUCTIONNAME, String.valueOf(deduction.getName()));
					}else{
						String amount="";
						int count = 0;
						for(int i= 0; i<resultJSONArray.length();i++){
							JSONObject inputJSON1 = (JSONObject) resultJSONArray.get(i);
							if(deduction.getDeductionsettingId().equals(Long.parseLong(inputJSON1.get(CommonConstants.DEDUCTIONID).toString()))){
								count++;
								amount = inputJSON1.get(CommonConstants.AMOUNT).toString();
							}
						}
						if(count > 0){
							deductionResult.put(CommonConstants.AMOUNT, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(amount))));
							deductionResult.put(CommonConstants.DEDUCTIONID, String.valueOf(deduction.getDeductionsettingId()));
							deductionResult.put(CommonConstants.DEDUCTIONNAME, String.valueOf(deduction.getName()));
						}else{
							deductionResult.put(CommonConstants.AMOUNT, String.valueOf("0.00"));
							deductionResult.put(CommonConstants.DEDUCTIONID, String.valueOf(deduction.getDeductionsettingId()));
							deductionResult.put(CommonConstants.DEDUCTIONNAME, String.valueOf(deduction.getName()));
						}
					}
					resultJSONArrayTemp.put(deductionResult);
				}
				result.put(CommonConstants.DEDUCTIONLIST, resultJSONArrayTemp);
			}else{
				result.put(CommonConstants.DEDUCTIONLIST, resultJSONArrayTemp);
			}
		}else{
			result.put(CommonConstants.DEDUCTIONLIST, "");
		}
		
		return result;
	}
	public static JSONObject getDeuctionDetatils(PayrollDeductionDetailsDO payrollDeduction)throws JSONException {
	//	List<DeductionSettingDO> deductionList = deductionSettingsService.retrieveById(payrollDeduction.getDeductionId());
		JSONObject result = new JSONObject();
		/*if(deductionList != null && deductionList.size() > 0 ){
			result.put(CommonConstants.AMOUNT, String.valueOf(payrollDeduction.getMonthly()));
			result.put(CommonConstants.DEDUCTIONID, String.valueOf(deductionList.get(0).getDeductionsettingId()));
			result.put(CommonConstants.DEDUCTIONNAME, String.valueOf(deductionList.get(0).getName()));
			
		}else{
			result.put(CommonConstants.DEDUCTIONID, String.valueOf(""));
			result.put(CommonConstants.DEDUCTIONNAME, String.valueOf(""));
			result.put(CommonConstants.AMOUNT, String.valueOf(""));
		}*/
		return result;
	}
	
	public static JSONObject getDeuctionDetatilsWithAmoount(PayrollDeductionDetailsDO payrollDeduction, Double amount)throws JSONException {
	//	List<DeductionSettingDO> deductionList = deductionSettingsService.retrieveById(payrollDeduction.getDeductionId());
		JSONObject result = new JSONObject();
		/*if(deductionList != null && deductionList.size() > 0 ){
			result.put(CommonConstants.AMOUNT, String.valueOf(amount));
			result.put(CommonConstants.DEDUCTIONID, String.valueOf(deductionList.get(0).getDeductionsettingId()));
			result.put(CommonConstants.DEDUCTIONNAME, String.valueOf(deductionList.get(0).getName()));
			
		}else{
			result.put(CommonConstants.DEDUCTIONID, String.valueOf(""));
			result.put(CommonConstants.DEDUCTIONNAME, String.valueOf(""));
			result.put(CommonConstants.AMOUNT, String.valueOf(""));
		}*/
		return result;
	}
	

	public static JSONObject getPayrollDetailObject(EmployeePayrollDO payrollDeatils)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(payrollDeatils.getPayrollId()));
		result.put(CommonConstants.EMPID, String.valueOf(payrollDeatils.getEmployee().getEmpId()));
		if(payrollDeatils.getEmployee().getMiddlename() != null) result.put(CommonConstants.EMPNAME, String.valueOf(payrollDeatils.getEmployee().getFirstname() + " " +payrollDeatils.getEmployee().getMiddlename() + " " + payrollDeatils.getEmployee().getLastname()));
		else if(payrollDeatils.getEmployee().getMiddlename() != null && payrollDeatils.getEmployee().getLastname() == null ) result.put(CommonConstants.EMPNAME, String.valueOf(payrollDeatils.getEmployee().getFirstname() + " " +payrollDeatils.getEmployee().getMiddlename()));
		else if(payrollDeatils.getEmployee().getMiddlename() == null && payrollDeatils.getEmployee().getLastname() == null ) result.put(CommonConstants.EMPNAME, String.valueOf(payrollDeatils.getEmployee().getFirstname()));
		else result.put(CommonConstants.EMPNAME, String.valueOf(payrollDeatils.getEmployee().getFirstname() + " " + payrollDeatils.getEmployee().getLastname()));
		result.put(CommonConstants.PAYROLLMONTH,String.valueOf(payrollDeatils.getPayrollMonth() != null ? payrollDeatils.getPayrollMonth() : ""));
		result.put(CommonConstants.STATUS, String.valueOf(payrollDeatils.getStatus()));
		result.put(CommonConstants.NETMONTHLY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(payrollDeatils.getNetmonth()))));
		result.put(CommonConstants.EARNINGMONTHLY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(payrollDeatils.getEarningmonth()))));
		result.put(CommonConstants.DEDUCTIONMONTHLY, String.valueOf(payrollDeatils.getDeductionmonth() != null ? CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(payrollDeatils.getDeductionmonth())) : 0));
		result.put(CommonConstants.BASICGROSS_PAY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(payrollDeatils.getGrossPay()))));
		result.put(CommonConstants.TAXMONTHLY, String.valueOf(payrollDeatils.getTaxMonth() != null ? CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(payrollDeatils.getTaxMonth())) : "0"));
		result.put(CommonConstants.PAYROLLGROUPID, String.valueOf(payrollDeatils.getPayrollGroup() != null ? payrollDeatils.getPayrollGroup().getPayrollGroupId() : ""));
		//result.put(CommonConstants.PAYROLLGROUPNAME, String.valueOf(payrollDeatils.getPayrollGroup() != null ? payrollDeatils.getPayrollGroup().getPayrollGr : ""));
		
		
		if(payrollDeatils.getPayrollWeekFrom() != null){
			result.put(CommonConstants.PAYROLLWEEKFROM, payrollDeatils.getPayrollWeekFrom()); 
		}else{
			result.put(CommonConstants.PAYROLLWEEKFROM, ""); 
		}
		
		if(payrollDeatils.getPayrollWeekTo() != null){
			result.put(CommonConstants.PAYROLLWEEKTO, payrollDeatils.getPayrollWeekTo()); 
		}else{
			result.put(CommonConstants.PAYROLLWEEKTO, ""); 
		}
		
		if(payrollDeatils.getPayrollDate() != null){
			result.put(CommonConstants.PAYROLLDATE, String.valueOf(payrollDeatils.getPayrollDate()));
		}else{
			result.put(CommonConstants.PAYROLLDATE, String.valueOf(""));
		}
		
		if(payrollDeatils.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(payrollDeatils.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(payrollDeatils.getUpdatedon())));
		
		JSONArray earningResultJSONArray = new JSONArray();
		
		if(payrollDeatils.getPayrollEarning() !=  null && payrollDeatils.getPayrollEarning().size() > 0){
			for (PayrollEarningsDetailsDO empEarnings : payrollDeatils.getPayrollEarning()) {
				JSONObject earningResult = new JSONObject();
				earningResult.put(CommonConstants.MONTHLY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(empEarnings.getMonthly()))));
				earningResult.put(CommonConstants.YTD, String.valueOf(empEarnings.getYtd()));
				earningResult.put(CommonConstants.NAME, String.valueOf(empEarnings.getName()));
				earningResult.put(CommonConstants.EARNINGID, String.valueOf(empEarnings.getPayrollEarningId()));
				earningResultJSONArray.put(earningResult);
			}
			result.put(CommonConstants.EARNINGLIST, earningResultJSONArray);
		}else{
			result.put(CommonConstants.EARNINGLIST,"");
		}
		
		JSONArray deductionResultJSONArray = new JSONArray();
				
		if(payrollDeatils.getPayrollDeduction() !=  null && payrollDeatils.getPayrollDeduction().size() > 0){
			for (PayrollDeductionDetailsDO empDeduction : payrollDeatils.getPayrollDeduction()) {
				JSONObject deductionResult = new JSONObject();
				deductionResult.put(CommonConstants.MONTHLY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(empDeduction.getMonthly()))));
				deductionResult.put(CommonConstants.YTD, String.valueOf(empDeduction.getYtd()));
				deductionResult.put(CommonConstants.NAME, String.valueOf(empDeduction.getName()));
				deductionResult.put(CommonConstants.DEDUCTIONID, String.valueOf(empDeduction.getPayrollDeductionId()));
				deductionResultJSONArray.put(deductionResult);
			}
			result.put(CommonConstants.DEDUCTIONLIST, deductionResultJSONArray);
		}else{
			result.put(CommonConstants.DEDUCTIONLIST,"");
		}
		
		JSONArray taxResultJSONArray = new JSONArray();
		
		if(payrollDeatils.getPayrollTax() !=  null && payrollDeatils.getPayrollTax().size() > 0){
			for (PayrollTaxDetailsDO empTax : payrollDeatils.getPayrollTax()) {
				JSONObject taxResult = new JSONObject();
				if(empTax.getMonthly() != null)		taxResult.put(CommonConstants.MONTHLY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(empTax.getMonthly()))));
				else 		taxResult.put(CommonConstants.MONTHLY, String.valueOf(""));
		
				taxResult.put(CommonConstants.YTD, String.valueOf(empTax.getYtd()));
				if(empTax.getName() != null)taxResult.put(CommonConstants.NAME, String.valueOf(empTax.getName()));
				else taxResult.put(CommonConstants.NAME, String.valueOf(""));
				taxResult.put(CommonConstants.TAXID, String.valueOf(empTax.getPayrollTaxId()));
				taxResultJSONArray.put(taxResult);
			}
			result.put(CommonConstants.TAXLIST, taxResultJSONArray);
		}else{
			result.put(CommonConstants.TAXLIST,"");
		}
		
		
		
		/*List<EmployeeLopDO> employeeLopList = employeeLopService.retrieveByEmpId(empCompensation.getEmpId());
		int noOfDaysLop = 0;
		Double lossOfPayAmount = 0D;
		if(empCompensation.getPayrollMonth() != null){
			if(employeeLopList != null && employeeLopList.size() > 0){
				Calendar start = Calendar.getInstance();
				start.setTime(employeeLopList.get(0).getStartdate());
				Calendar end = Calendar.getInstance();
				end.setTime(employeeLopList.get(0).getEnddate());
				
				for (Date date = start.getTime(); start.before(end) || start.equals(end); start.add(Calendar.DATE, 1), date = start.getTime()) {
					Calendar weekend = Calendar.getInstance();
					weekend.setTime(date);
					if (weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && 
						weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){
						if(empCompensation.getPayrollMonth().equalsIgnoreCase(CommonUtil.convertDateToStringWithOutDate(weekend.getTime()))){
							noOfDaysLop++;
						}
					}
				}
			}
		}
		if(empCompensation.getPayrollMonth() != null){
			if(empCompensation.getGrossPay() != null){
				//result.put(CommonConstants.EMPCTC, String.valueOf(empCompensation.getEmpctc()));
				Double ctc = empCompensation.getGrossPay();
				Date s = CommonUtil.convertDateToStringWithOutDate(empCompensation.getPayrollMonth());
		        Calendar c=Calendar.getInstance();
		        c.setTime(s);
		        int days = c.getActualMaximum(Calendar.DAY_OF_MONTH);
				Long perDay = (long) (ctc/days);
				if(noOfDaysLop > 0){
					lossOfPayAmount = (double) (noOfDaysLop * perDay);
				}
			}else{
				result.put(CommonConstants.EMPCTC, "");
			}
		}*/
		/*List<EmployeeBonusDO> employeeBonusList = employeeBonusService.retrieveByEmpIdAndMonth(empCompensation.getEmployee().getEmpId().toString(), empCompensation.getPayrollMonth());
		if(employeeBonusList != null && employeeBonusList.size() > 0){
			result.put(CommonConstants.BONUSMONTH,String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(employeeBonusList.get(0).getAmount()))));
		}else{
			result.put(CommonConstants.BONUSMONTH,String.valueOf(""));
		}*/
	//	List<PayrollLopDetailsDO> payrollLopDetails = payrollLopDetailsService.retrieveByEmpId(empCompensation.getEmployee().getEmpId(), empCompensation.getPayrollMonth());
		/*if(payrollLopDetails != null && payrollLopDetails.size() > 0){
			result.put(CommonConstants.LOSSOFPAYMONTHLY,  String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(payrollLopDetails.get(0).getMonthly()))));
		}else{
			result.put(CommonConstants.LOSSOFPAYMONTHLY,  String.valueOf("0.00"));
		}
		*/
		
		/*Long bonusYear = 0L;
		String fromMnth = "";
		String toMnth = "";
		String todayMnth = CommonUtil.convertDateToStringWithOutDate(new Date());
		String currentMonth [] = todayMnth.split(" ");
		if(currentMonth[0].equalsIgnoreCase("Jan") || currentMonth[0].equalsIgnoreCase("Feb") || currentMonth[0].equalsIgnoreCase("Mar")){
			String mnthYear [] = todayMnth.split(" ");
			int year = Integer.parseInt(mnthYear[1]) - 1;
			String mnth= "Apr";
			fromMnth = mnth + " "+year;
			toMnth = todayMnth;
		}else{
			fromMnth = todayMnth;
			String mnthYear [] = todayMnth.split(" ");
			int year = Integer.parseInt(mnthYear[1]) - 1;
			String mnth= "Apr";
			toMnth = mnth + " "+year;
		}
		*/
		/*bonusYear = employeeBonusService.retrieveByEmpIdBetweenMonth(empCompensation.getEmployee().getEmpId().toString(), fromMnth, toMnth);
		if(bonusYear == null){
			bonusYear = 0L;
		}
		result.put(CommonConstants.BONUSYEAR,String.valueOf(bonusYear));
		List<EmployeeLopDO> lopList = employeeLopService.retrieveByEmpId(empCompensation.getEmployee().getEmpId().toString());
		Double lossOfPayAmountForEmp = 0D;
		if(lopList != null && lopList.size() > 0){
			String todayMnth1 = CommonUtil.convertDateToStringWithOutDate(new Date());
			String fromMnth1 = "";
			String toMnth1 = "";
			String currentMonth1 [] = todayMnth1.split(" ");
			if(currentMonth1[0].equalsIgnoreCase("Jan") || currentMonth1[0].equalsIgnoreCase("Feb") || currentMonth1[0].equalsIgnoreCase("Mar")){
				String mnthYear [] = todayMnth1.split(" ");
				int year = Integer.parseInt(mnthYear[1]) - 1;
				String mnth= "04";
				fromMnth1 = "30-"+mnth+"-"+year;
				toMnth1 = todayMnth1;
			}else{
				toMnth1 = todayMnth1;
				String mnthYear [] = todayMnth1.split(" ");
				int year = Integer.parseInt(mnthYear[1]) - 1;
				String mnth= "04";
				fromMnth1 = "30-"+mnth+"-"+year;
			}
			for(EmployeeLopDO eLopList : lopList){
				int noOfDaysLopForEmp = 0;
				
				Date fromDate = CommonUtil.convertStringToDate(fromMnth1);
				Date toDate = CommonUtil.convertDateToStringWithOutDate(toMnth1);
				if(eLopList.getStartdate() != null && eLopList.getEnddate() != null){
					Date lopFromDate = CommonUtil.convertDateToStringWithOutDate(CommonUtil.convertDateToStringWithOutDate(eLopList.getStartdate()));
					Date lopToDate = CommonUtil.convertDateToStringWithOutDate(CommonUtil.convertDateToStringWithOutDate(eLopList.getEnddate()));
					
					if((lopFromDate.after(fromDate) || lopFromDate.equals(fromDate))
							&& lopFromDate.before(toDate) || lopFromDate.equals(toDate)){
						if((lopToDate.after(fromDate) || lopToDate.equals(fromDate))
								&& lopToDate.before(toDate) || lopToDate.equals(toDate)){
							
							Calendar start = Calendar.getInstance();
							start.setTime(eLopList.getStartdate());
							Calendar end = Calendar.getInstance();
							end.setTime(eLopList.getEnddate());
							
							for (Date date = start.getTime(); start.before(end) || start.equals(end); start.add(Calendar.DATE, 1), date = start.getTime()) {
								Calendar weekend = Calendar.getInstance();
								weekend.setTime(date);
								// me
								if (weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && 
									weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){
									//if(month.equalsIgnoreCase(CommonUtil.convertDateToStringWithOutDate(weekend.getTime()))){
									List<EmployeeCtcDO> empCtcList = employeeCtcService.retrieveActiveWithDate(empCompensation.getEmpId().toString(), CommonUtil.convertDateToStringWithOutDate(empCompensation.getPayrollMonth()));
									
									if(empCtcList != null && empCtcList.size() > 0){
										Long ctc = empCtcList.get(0).getEmpctc()/12;
								        int days = weekend.getActualMaximum(Calendar.DAY_OF_MONTH);
										Long perDay = ctc/days;
										if(noOfDaysLopForEmp > 0){
											//lossOfPayAmountForEmp += (double) (noOfDaysLop * perDay);
										}
									}
									
									//}
								}
							}
						}
					}
				}
				result.put(CommonConstants.LOSSOFPAYYEAR,  String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(lossOfPayAmountForEmp))));
			}
		}else{
			result.put(CommonConstants.LOSSOFPAYYEAR, String.valueOf(""));
		}*/
	/*	Object[] earningYear = employeePayrollService.retrieveByEmpIdBetweenDateSum(empCompensation.getEmployee().getEmpId().toString(), fromMnth, toMnth);
		if(earningYear != null){
			double eraningYear = 0D;
			double deductionYear = 0D;
			double taxYear = 0D;
			
			if(earningYear[1] != null){
				eraningYear = empCompensation.getEarningmonth()+Double.parseDouble(earningYear[1].toString()) +  bonusYear;
				result.put(CommonConstants.EARNINGYEAR,String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(empCompensation.getEarningmonth()+Double.parseDouble(earningYear[1].toString()) +  bonusYear))));
			}else{
				eraningYear = empCompensation.getEarningmonth()+ bonusYear;
				result.put(CommonConstants.EARNINGYEAR,String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(empCompensation.getEarningmonth() +  bonusYear))));
			}
			
			if(earningYear[2] != null){
				deductionYear = empCompensation.getDeductionmonth()+Double.parseDouble(earningYear[2].toString()) +  lossOfPayAmountForEmp;
				result.put(CommonConstants.DEDUCTIONYEAR,String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(empCompensation.getDeductionmonth()+Double.parseDouble(earningYear[2].toString()) +  lossOfPayAmountForEmp))));
			}else{
				deductionYear = empCompensation.getDeductionmonth()+ lossOfPayAmountForEmp;
				result.put(CommonConstants.DEDUCTIONYEAR,String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(empCompensation.getDeductionmonth() + lossOfPayAmountForEmp))));
			}
			
			if(earningYear[3] != null){
				taxYear = empCompensation.getTaxMonth()+Double.parseDouble(earningYear[3].toString());
				result.put(CommonConstants.TAXYEAR,String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(empCompensation.getTaxMonth()+Double.parseDouble(earningYear[3].toString())))));
			}else{
				taxYear = empCompensation.getTaxMonth();
				result.put(CommonConstants.TAXYEAR,String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(empCompensation.getTaxMonth()))));
			}
			result.put(CommonConstants.NETYEAR,String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(eraningYear - (deductionYear+taxYear)))));
			
		}else{
			result.put(CommonConstants.EARNINGYEAR,String.valueOf(""));
			result.put(CommonConstants.DEDUCTIONYEAR,String.valueOf(""));
			result.put(CommonConstants.TAXYEAR,String.valueOf(""));
			result.put(CommonConstants.NETYEAR,String.valueOf(""));
		}
		*/
	/*	List<PayrollEarningsDetailsDO> earningsList = payrollEarningsDetailsService.retrieveByEmpCompensationId(empCompensation.getPayrollId());
		List<PayrollTaxDetailsDO> taxList = payrollTaxDetailsService.retrieveByEmpCompensationId(empCompensation.getPayrollId());
		List<PayrollDeductionDetailsDO> deductionList = payrollDeductionDetailsService.retrieveByEmpCompensationId(empCompensation.getPayrollId());
		
		JSONArray resultJSONArray = new JSONArray();
		
		if(earningsList != null && earningsList.size() > 0){
			for (PayrollEarningsDetailsDO empEarnings : earningsList) {
				resultJSONArray.put(getEmployeeEarningsDetailObject(empEarnings, empCompensation.getEmployee().getEmpId()));
			}
			result.put(CommonConstants.EARNINGLIST, resultJSONArray);
		}else{
			result.put(CommonConstants.EARNINGLIST,"");
		}*/
		//JSONArray resultJSONArray1 = new JSONArray();
		
		/*if(deductionList != null && deductionList.size() > 0){
			for (PayrollDeductionDetailsDO empDeduction : deductionList) {
				resultJSONArray1.put(getEmployeeDeductionDetailObject(empDeduction, empCompensation.getEmployee().getEmpId()));
			}
			result.put(CommonConstants.DEDUCTIONLIST, resultJSONArray1);
		}else{
			result.put(CommonConstants.DEDUCTIONLIST,"");
		}
		
		JSONArray resultJSONArray2 = new JSONArray();
		
		if(taxList != null && taxList.size() > 0){
			for (PayrollTaxDetailsDO empDeduction : taxList) {
				//resultJSONArray2.put(getEmployeeTaxDetailObject(empDeduction, empCompensation.getEmployee().getEmpId()));
			}
			result.put(CommonConstants.TAXLIST, resultJSONArray2);
		}else{
			result.put(CommonConstants.TAXLIST,"");
		}*/
		
		/*if(empCompensation.getPayrollbatchId() != null){
			result.put(CommonConstants.PAYROLLBATCHID, String.valueOf(empCompensation.getPayrollbatchId()));
			List<PayrollBatchDO> payrollBatchList = payrollBatchService.retrieveById(empCompensation.getPayrollbatchId());
			if(payrollBatchList != null && payrollBatchList.size() > 0){
				result.put(CommonConstants.PAYROLLBATCHNAME, String.valueOf(payrollBatchList.get(0).getName()));
			}else{
				result.put(CommonConstants.PAYROLLBATCHNAME, "");
			}
		}else{
			result.put(CommonConstants.PAYROLLBATCHID, "");
			result.put(CommonConstants.PAYROLLBATCHNAME, "");
		}*/
		
		
		
		/*if(empCompensation.getUpdatedBy() != null){
			List<EmployeeDO> empList = employeeService.retrieveEmpId(Long.parseLong(empCompensation.getUpdatedBy()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}*/
		
		/*if(empCompensation.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(empCompensation.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(empCompensation.getUpdatedon())));*/
		return result;
	}
	
	public static JSONObject getObjectDetailObjectByBatchId(Object[] empCompensation)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.COUNT, String.valueOf(empCompensation[0]));
		result.put(CommonConstants.PAYROLLMONTH, String.valueOf(empCompensation[1]));
		result.put(CommonConstants.PAYROLLBATCHID, String.valueOf(empCompensation[2]));
		if(empCompensation[2] != null){
			result.put(CommonConstants.PAYROLLBATCHID, String.valueOf(empCompensation[2]));
			List<PayrollBatchDO> payrollBatchList = payrollBatchService.retrieveById(Long.parseLong(empCompensation[2].toString()));
			if(payrollBatchList != null && payrollBatchList.size() > 0){
				result.put(CommonConstants.PAYROLLBATCHNAME, String.valueOf(payrollBatchList.get(0).getName()));
			}else{
				result.put(CommonConstants.PAYROLLBATCHNAME, "");
			}
		}else{
			result.put(CommonConstants.PAYROLLBATCHID, "");
			result.put(CommonConstants.PAYROLLBATCHNAME, "");
		}
		
		result.put(CommonConstants.NETYEAR, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(empCompensation[3].toString()))));
		result.put(CommonConstants.EARNINGYEAR, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(empCompensation[4].toString()))));
		result.put(CommonConstants.DEDUCTIONYEAR, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(empCompensation[5].toString()))));
		result.put(CommonConstants.BASICGROSS_PAY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(empCompensation[6].toString()))));
		result.put(CommonConstants.STATUS,String.valueOf(empCompensation[7]));
		result.put(CommonConstants.UPDATED_ON, String.valueOf(CommonUtil.convertDateToStringWithtime((Date) empCompensation[8])));
		if(empCompensation[9] != null){
			String empName = CommonUtil.getEmpObject(empCompensation[9].toString());
			result.put(CommonConstants.UPDATED_BY, empName); 
			/*List<EmployeeDO> empList = employeeService.retrieveEmpId(empCompensation[9].toString());
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}*/
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		/*if(empCompensation[10].toString() != null){
			result.put(CommonConstants.PAYROLLWEEKFROM, String.valueOf(empCompensation[10].toString()));
		}
		if(empCompensation[11].toString() != null){
			result.put(CommonConstants.PAYROLLWEEKTO, String.valueOf(empCompensation[11].toString()));
		}*/
		result.put(CommonConstants.TAXYEAR, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(empCompensation[10].toString()))));
		/*;
		result.put(CommonConstants.PAYROLLWEEKTO, String.valueOf(empCompensation[12].toString()));*/
		return result;
	}
	
	
	public static JSONObject getObjectDetailObjectByBatchId1(Object[] empCompensation)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.COUNT, String.valueOf(empCompensation[0]));
		result.put(CommonConstants.PAYROLLMONTH, String.valueOf(empCompensation[1]));
		result.put(CommonConstants.PAYROLLBATCHID, String.valueOf(empCompensation[2]));
		if(empCompensation[2] != null){
			result.put(CommonConstants.PAYROLLBATCHID, String.valueOf(empCompensation[2]));
			List<PayrollBatchDO> payrollBatchList = payrollBatchService.retrieveById(Long.parseLong(empCompensation[2].toString()));
			if(payrollBatchList != null && payrollBatchList.size() > 0){
				result.put(CommonConstants.PAYROLLBATCHNAME, String.valueOf(payrollBatchList.get(0).getName()));
			}else{
				result.put(CommonConstants.PAYROLLBATCHNAME, "");
			}
		}else{
			result.put(CommonConstants.PAYROLLBATCHID, "");
			result.put(CommonConstants.PAYROLLBATCHNAME, "");
		}
		
		result.put(CommonConstants.NETYEAR, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(empCompensation[3].toString()))));
		result.put(CommonConstants.EARNINGYEAR, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(empCompensation[4].toString()))));
		result.put(CommonConstants.DEDUCTIONYEAR, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(empCompensation[5].toString()))));
		result.put(CommonConstants.BASICGROSS_PAY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(empCompensation[6].toString()))));
		result.put(CommonConstants.STATUS,String.valueOf(empCompensation[7]));
		result.put(CommonConstants.UPDATED_ON, String.valueOf(CommonUtil.convertDateToStringWithtime((Date) empCompensation[8])));
		if(empCompensation[9] != null){
			String empName = CommonUtil.getEmpObject(empCompensation[9].toString());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		if(empCompensation[10].toString() != null){
			result.put(CommonConstants.PAYROLLWEEKFROM, String.valueOf(empCompensation[10].toString()));
		}
		if(empCompensation[11].toString() != null){
			result.put(CommonConstants.PAYROLLWEEKTO, String.valueOf(empCompensation[11].toString()));
		}
		
		result.put(CommonConstants.TAXYEAR, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(empCompensation[12].toString()))));
		/*;
		result.put(CommonConstants.PAYROLLWEEKTO, String.valueOf(empCompensation[12].toString()));*/
		return result;
	}
	
	public static JSONObject getObjectDetailObject(Object[] empCompensation)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.COUNT, String.valueOf(empCompensation[0]));
		result.put(CommonConstants.PAYROLLMONTH, String.valueOf(empCompensation[1]));
		result.put(CommonConstants.NETYEAR, String.valueOf(Math.round(Double.valueOf(empCompensation[2].toString()))));
		result.put(CommonConstants.EARNINGYEAR, String.valueOf(Math.round(Double.valueOf(empCompensation[3].toString()))));
		result.put(CommonConstants.DEDUCTIONYEAR, String.valueOf(Math.round(Double.valueOf(empCompensation[4].toString()))));
		result.put(CommonConstants.BASICGROSS_PAY, String.valueOf(Math.round(Double.valueOf(empCompensation[5].toString()))));
		
		//result.put(CommonConstants.STATUS,String.valueOf(empCompensation[6]));
		
		/*result.put(CommonConstants.UPDATED_ON, String.valueOf(CommonUtil.convertDateToStringWithdatetime((Date) empCompensation[7])));
		if(empCompensation[8] != null){
			List<EmployeeDO> empList = employeeService.retriveByEmpId(empCompensation[8].toString());
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}*/
		result.put(CommonConstants.TAXYEAR, String.valueOf(Math.round(Double.valueOf(empCompensation[6].toString()))));
		//result.put(CommonConstants.PAYROLLTYPE, String.valueOf(empCompensation[9] != null ? empCompensation[9] : ""));
		return result;
	}
	
	
	public static JSONObject getEmployeeEarningsDetailObject(PayrollEarningsDetailsDO empCompensation, Long empId)throws JSONException {
		JSONObject result = new JSONObject();
	//	result.put(CommonConstants.EARNINGID, String.valueOf(empCompensation.getEarningsId()));
		//List<AllowanceSettingDO> allowanceList = allowanceSettingsService.retrieveById(empCompensation.getEarningsId());
		/*if(allowanceList != null && allowanceList.size() > 0){
			result.put(CommonConstants.EARNINGNAME, String.valueOf(allowanceList.get(0).getDisplayname()));
		}else{
			result.put(CommonConstants.EARNINGNAME, "");
		}*/
		result.put(CommonConstants.MONTHLY, String.valueOf(empCompensation.getMonthly() != null ? Math.round(Double.valueOf(empCompensation.getMonthly())):"0"));
		result.put(CommonConstants.MONTHLY, String.valueOf(Math.round(Double.valueOf(empCompensation.getMonthly()))));
		String fromMnth = "";
		String toMnth = "";
		String todayMnth = CommonUtil.convertDateToStringWithOutDate(new Date());
		String currentMonth [] = todayMnth.split(" ");
		if(currentMonth[0].equalsIgnoreCase("Jan") || currentMonth[0].equalsIgnoreCase("Feb") || currentMonth[0].equalsIgnoreCase("Mar")){
			String mnthYear [] = todayMnth.split(" ");
			int year = Integer.parseInt(mnthYear[1]) - 1;
			String mnth= "Apr";
			fromMnth = mnth + " "+year;
			toMnth = todayMnth;
		}else{
			fromMnth = todayMnth;
			String mnthYear [] = todayMnth.split(" ");
			int year = Integer.parseInt(mnthYear[1]) - 1;
			String mnth= "Apr";
			toMnth = mnth + " "+year;
		}
	/*	List<EmployeePayrollDO> earningYear = employeePayrollService.retrieveByEmpIdBetweenDate(empId, fromMnth, toMnth);
		
		if(earningYear != null && earningYear.size() > 0){
			List<Long> ids = new ArrayList<Long>();
			for (EmployeePayrollDO employeePayrollDO : earningYear) {
				ids.add(employeePayrollDO.getId());
			}
			Double ytd = 0D;
			List<PayrollEarningsDetailsDO> payrollEarnings = payrollEarningsDetailsService.retrieveByEmpCompensationIds(ids);
			if(payrollEarnings != null && payrollEarnings.size() > 0){
				for (PayrollEarningsDetailsDO payrollEarningsDetailsDO : payrollEarnings) {
					if(payrollEarningsDetailsDO.getEarningsId().equals(empCompensation.getEarningsId())){
						ytd = ytd+payrollEarningsDetailsDO.getMonthly();
					}
				}
			}
			
			result.put(CommonConstants.YTD, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(ytd + empCompensation.getMonthly()))));
		}else{
			result.put(CommonConstants.YTD, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(empCompensation.getMonthly()))));
			
		}*/
		return result;
	}
	
	public static JSONObject getEmployeeDeductionDetailObject(PayrollDeductionDetailsDO empCompensation, Long empId)throws JSONException {
		JSONObject result = new JSONObject();
		//result.put(CommonConstants.DEDUCTIONID, String.valueOf(empCompensation.getDeductionId()));
		/*List<DeductionSettingDO> deductionList = deductionSettingsService.retrieveById(empCompensation.getDeductionId());
		if(deductionList != null && deductionList.size() > 0){
			result.put(CommonConstants.DEDUCTIONNAME, String.valueOf(deductionList.get(0).getDisplayname()));
		}else{
			result.put(CommonConstants.DEDUCTIONNAME, "");
		}*/
		result.put(CommonConstants.MONTHLY, String.valueOf(empCompensation.getMonthly()!= null ?Math.round(Double.valueOf(empCompensation.getMonthly())) :"0"));
		if(empId != null){
			result.put(CommonConstants.MONTHLY, String.valueOf(Math.round(Double.valueOf(empCompensation.getMonthly()))));
			String fromMnth = "";
			String toMnth = "";
			String todayMnth = CommonUtil.convertDateToStringWithOutDate(new Date());
			String currentMonth [] = todayMnth.split(" ");
			if(currentMonth[0].equalsIgnoreCase("Jan") || currentMonth[0].equalsIgnoreCase("Feb") || currentMonth[0].equalsIgnoreCase("Mar")){
				String mnthYear [] = todayMnth.split(" ");
				int year = Integer.parseInt(mnthYear[1]) - 1;
				String mnth= "Apr";
				fromMnth = mnth + " "+year;
				toMnth = todayMnth;
			}else{
				fromMnth = todayMnth;
				String mnthYear [] = todayMnth.split(" ");
				int year = Integer.parseInt(mnthYear[1]) - 1;
				String mnth= "Apr";
				toMnth = mnth + " "+year;
			}
		//	List<EmployeePayrollDO> earningYear = employeePayrollService.retrieveByEmpIdBetweenDate(empId, fromMnth, toMnth);
			
		/*	if(earningYear != null  && earningYear.size() > 0){
				List<Long> ids = new ArrayList<Long>();
				for (EmployeePayrollDO employeePayrollDO : earningYear) {
					ids.add(employeePayrollDO.getId());
				}
				Double ytd = 0D;
				List<PayrollDeductionDetailsDO> payrollDeductionList = payrollDeductionDetailsService.retrieveByEmpCompensationIds(ids);
				if(payrollDeductionList != null && payrollDeductionList.size() > 0){
					for (PayrollDeductionDetailsDO payrollDeductionDetailsDO : payrollDeductionList) {
						if(payrollDeductionDetailsDO.getDeductionId().equals(empCompensation.getDeductionId())){
							ytd = ytd+payrollDeductionDetailsDO.getMonthly();
						}
					}
				}
				
				result.put(CommonConstants.YTD, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(ytd + empCompensation.getMonthly()))));
			}else{
				result.put(CommonConstants.YTD, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(empCompensation.getMonthly()))));
				
			}*/
		}else{
			result.put(CommonConstants.YTD, String.valueOf(Math.round(Double.valueOf(empCompensation.getYtd()))));
		}
		return result;
	}
	
	public static JSONObject getEmployeeTaxDetailObject(PayrollTaxDetailsDO empCompensation, Long empId)throws JSONException {
		JSONObject result = new JSONObject();
	//	result.put(CommonConstants.TAXID, String.valueOf(empCompensation.getTaxId()));
		/*List<TaxSettingsDO> deductionList = taxSettingsService.retrieveById(empCompensation.getTaxId());
		if(deductionList != null && deductionList.size() > 0){
			result.put(CommonConstants.TAXNAME, String.valueOf(deductionList.get(0).getDisplayname()));
		}else{
			result.put(CommonConstants.TAXNAME, "");
		}*/
		result.put(CommonConstants.MONTHLY, String.valueOf(empCompensation.getMonthly() != null ? Math.round(Double.valueOf(empCompensation.getMonthly())):"0"));
		if(empId != null){
			result.put(CommonConstants.MONTHLY, String.valueOf(Math.round(Double.valueOf(empCompensation.getMonthly()))));
			String fromMnth = "";
			String toMnth = "";
			String todayMnth = CommonUtil.convertDateToStringWithOutDate(new Date());
			String currentMonth [] = todayMnth.split(" ");
			if(currentMonth[0].equalsIgnoreCase("Jan") || currentMonth[0].equalsIgnoreCase("Feb") || currentMonth[0].equalsIgnoreCase("Mar")){
				String mnthYear [] = todayMnth.split(" ");
				int year = Integer.parseInt(mnthYear[1]) - 1;
				String mnth= "Apr";
				fromMnth = mnth + " "+year;
				toMnth = todayMnth;
			}else{
				fromMnth = todayMnth;
				String mnthYear [] = todayMnth.split(" ");
				int year = Integer.parseInt(mnthYear[1]) - 1;
				String mnth= "Apr";
				toMnth = mnth + " "+year;
			}
		//	List<EmployeePayrollDO> earningYear = employeePayrollService.retrieveByEmpIdBetweenDate(empId, fromMnth, toMnth);
			
			/*if(earningYear != null && earningYear.size() > 0){
				List<Long> ids = new ArrayList<Long>();
				for (EmployeePayrollDO employeePayrollDO : earningYear) {
					ids.add(employeePayrollDO.getId());
				}
				Double ytd = 0D;
				List<PayrollTaxDetailsDO> paytaxList = payrollTaxDetailsService.retrieveByEmpCompensationIds(ids);
				if(paytaxList != null && paytaxList.size() > 0){
					for (PayrollTaxDetailsDO payrollTaxDetailsDO : paytaxList) {
						if(payrollTaxDetailsDO.getTaxId().equals(empCompensation.getTaxId())){
							ytd = ytd+payrollTaxDetailsDO.getMonthly();
						}
					}
				}
				result.put(CommonConstants.YTD, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(ytd + empCompensation.getMonthly()))));
			}else{
				result.put(CommonConstants.YTD, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(empCompensation.getMonthly()))));
				
			}*/
		}else{
			//result.put(CommonConstants.YTD, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(empCompensation.getYtd()))));
		}
		return result;
	}
	
	public static JSONObject SingleMonthTaxReport(List<EmployeePayrollDO> payrollList, String frequency, String period) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			if(frequency != null){
				resultJSON.put(CommonConstants.FREQUENCY, String.valueOf(frequency));
			}else{
				resultJSON.put(CommonConstants.FREQUENCY, String.valueOf(""));
			}
			
			if(period != null){
				resultJSON.put(CommonConstants.PERIOD, String.valueOf(period));
			}else{
				resultJSON.put(CommonConstants.PERIOD, String.valueOf(""));
			}
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeePayrollDO payroll : payrollList) {
					resultJSONArray.put(getSingleMonthTaxDetailObject(payroll));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getSingleMonthTaxDetailObject(EmployeePayrollDO empCompensation)throws JSONException {
		JSONObject result = new JSONObject();
		if(empCompensation != null){
			if(empCompensation.getEmployee().getEmpId() != null){
				List<EmployeeDO> empList = employeeService.retrieveEmpId(empCompensation.getEmployee().getEmpId().toString());
				if(empList != null && empList.size() > 0){
					if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.EMPNAME, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
					else result.put(CommonConstants.EMPNAME, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
				}else{
					result.put(CommonConstants.EMPNAME, ""); 
				}
			}else{
				result.put(CommonConstants.EMPNAME, ""); 
			}
			List<TaxSettingsDO> taxList = taxSettingsService.retrieve();
			/*List<PayrollTaxDetailsDO> payrollTaxList = payrollTaxDetailsService.retrieveByEmpCompensationId(empCompensation.getId());
			JSONArray resultJSONArray = new JSONArray();
			if(payrollTaxList != null && payrollTaxList.size()  > 0){
				for (PayrollTaxDetailsDO payrollTaxDetailsDO : payrollTaxList) {
					resultJSONArray.put(getTaxDetatils(payrollTaxDetailsDO));
				}
				result.put(CommonConstants.TAXLIST, resultJSONArray);
				
			}else{
				result.put(CommonConstants.TAXLIST, resultJSONArray);
			}*/
			JSONArray resultJSONArrayTemp = new JSONArray();
			/*if(taxList != null && taxList.size() > 0){
				for (TaxSettingsDO tax : taxList) {
					JSONObject taxResult = new JSONObject();
					if(resultJSONArray.length() <= 0){
						taxResult.put(CommonConstants.AMOUNT, String.valueOf("0.00"));
						taxResult.put(CommonConstants.TAXID, String.valueOf(tax.getTaxSettingId()));
						taxResult.put(CommonConstants.TAXNAME, String.valueOf(tax.getName()));
					}else{
						String amount="";
						int count = 0;
						for(int i= 0; i<resultJSONArray.length();i++){
							JSONObject inputJSON1 = (JSONObject) resultJSONArray.get(i);
							if(tax.getTaxSettingId().equals(Long.parseLong(inputJSON1.get(CommonConstants.TAXID).toString()))){
								count++;
								amount = inputJSON1.get(CommonConstants.AMOUNT).toString();
							}
						}
						if(count > 0){
							taxResult.put(CommonConstants.AMOUNT, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(amount))));
							taxResult.put(CommonConstants.TAXID, String.valueOf(tax.getTaxSettingId()));
							taxResult.put(CommonConstants.TAXNAME, String.valueOf(tax.getName()));
						}else{
							taxResult.put(CommonConstants.AMOUNT, String.valueOf("0.00"));
							taxResult.put(CommonConstants.TAXID, String.valueOf(tax.getTaxSettingId()));
							taxResult.put(CommonConstants.TAXNAME, String.valueOf(tax.getName()));
						}
					}
					resultJSONArrayTemp.put(taxResult);
				}
				result.put(CommonConstants.TAXLIST, resultJSONArrayTemp);
			}else{
				result.put(CommonConstants.TAXLIST, resultJSONArrayTemp);
			}*/
			
		}else{
			result.put(CommonConstants.TAXLIST, "");
		}
		
		return result;
	}
	public static JSONObject getTaxDetatils(PayrollTaxDetailsDO payrollTaxDetailsDO)throws JSONException {
		//List<TaxSettingsDO> taxList = taxSettingsService.retrieveById(payrollTaxDetailsDO.getTaxId());
		JSONObject result = new JSONObject();
		/*if(taxList != null && taxList.size() > 0 ){
			result.put(CommonConstants.AMOUNT, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(payrollTaxDetailsDO.getMonthly()))));
			result.put(CommonConstants.TAXID, String.valueOf(taxList.get(0).getTaxSettingId()));
			result.put(CommonConstants.TAXNAME, String.valueOf(taxList.get(0).getName()));
			
		}else{
			result.put(CommonConstants.TAXID, String.valueOf(""));
			result.put(CommonConstants.TAXNAME, String.valueOf(""));
			result.put(CommonConstants.AMOUNT, String.valueOf(""));
		}*/
		return result;
	}
	
	public static JSONObject MultipleMonthTaxReport(List<EmployeePayrollDO> payrollList, String fromMonth, String toMonth, String frequency) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			if(frequency != null){
				resultJSON.put(CommonConstants.FREQUENCY, String.valueOf(frequency));
			}else{
				resultJSON.put(CommonConstants.FREQUENCY, String.valueOf(""));
			}
			
			resultJSON.put(CommonConstants.PERIOD, String.valueOf(fromMonth+" - "+toMonth));
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeePayrollDO payroll : payrollList) {
				 Map<Long,Long> map=new HashMap<Long,Long>();
				/* if(!map.containsKey(payroll.getEmpId())){
					 List<EmployeePayrollDO> compensationList = employeePayrollService.retrieveByEmpIdBetweenDate(payroll.getEmpId(), fromMonth, toMonth);
					 resultJSONArray.put(getMultipleMonthTaxDetailObject(compensationList, fromMonth, toMonth));
				 }*/
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getMultipleMonthTaxDetailObject(List<EmployeePayrollDO> empCompensation, String fromMonth, String toMonth)throws JSONException {
		JSONObject result = new JSONObject();
		Map<Long,Long> tax = new HashMap<Long,Long>();
		/*if(empCompensation.get(0).getEmpId() != null){
			List<EmployeeDO> empList = employeeService.retrieveEmpId(empCompensation.get(0).getEmpId().toString() );
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.EMPNAME, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.EMPNAME, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.EMPNAME, ""); 
			}
		}else{
			result.put(CommonConstants.EMPNAME, ""); 
		}*/
		List<Long> payrollIds = new ArrayList<Long>();
		if(empCompensation != null && empCompensation.size() > 0){
			for (EmployeePayrollDO employeePayrollDO : empCompensation) {
				// payrollIds.add(employeePayrollDO.getId());
			}
		}
		List<TaxSettingsDO> taxList = taxSettingsService.retrieve();
		 List<PayrollTaxDetailsDO> payrollTaxList = payrollTaxDetailsService.retrieveByEmpCompensationIds(payrollIds);
		 JSONArray resultJSONArray = new JSONArray();
		 if(payrollTaxList != null && payrollTaxList.size() > 0){
			 for (PayrollTaxDetailsDO payrollTaxDetailsDO : payrollTaxList) {
				 Double amount = 0D;
				 Long deductionId = 0L;
				 /*if(!tax.containsKey(payrollTaxDetailsDO.getTaxId())){
					 tax.put(payrollTaxDetailsDO.getTaxId(),payrollTaxDetailsDO.getTaxId());
					 deductionId = payrollTaxDetailsDO.getTaxId();
					 for (PayrollTaxDetailsDO payrollDeductionDetails : payrollTaxList) {
						 if(payrollDeductionDetails.getTaxId().equals(deductionId)){
							 amount = amount + payrollDeductionDetails.getMonthly();
						 }
					 }
					 resultJSONArray.put(getTaxDetatilsWithAmoount(payrollTaxDetailsDO, amount));
					 result.put(CommonConstants.TAXLIST, resultJSONArray);
				 }*/
			}
			 JSONArray resultJSONArrayTemp = new JSONArray();
			 
				if(taxList != null && taxList.size() > 0){
					for (TaxSettingsDO taxobj : taxList) {
						JSONObject taxResult = new JSONObject();
						if(resultJSONArray.length() <= 0){
							taxResult.put(CommonConstants.AMOUNT, String.valueOf("0"));
							taxResult.put(CommonConstants.TAXID, String.valueOf(taxobj.getTaxSettingId()));
							taxResult.put(CommonConstants.TAXNAME, String.valueOf(taxobj.getName()));
						}else{
							String amount="";
							int count = 0;
							for(int i= 0; i<resultJSONArray.length();i++){
								JSONObject inputJSON1 = (JSONObject) resultJSONArray.get(i);
								if(taxobj.getTaxSettingId().equals(Long.parseLong(inputJSON1.get(CommonConstants.TAXID).toString()))){
									count++;
									amount = inputJSON1.get(CommonConstants.AMOUNT).toString();
								}
							}
							if(count > 0){
								taxResult.put(CommonConstants.AMOUNT, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(amount))));
								taxResult.put(CommonConstants.TAXID, String.valueOf(taxobj.getTaxSettingId()));
								taxResult.put(CommonConstants.TAXNAME, String.valueOf(taxobj.getName()));
							}else{
								taxResult.put(CommonConstants.AMOUNT, String.valueOf("0.00"));
								taxResult.put(CommonConstants.TAXID, String.valueOf(taxobj.getTaxSettingId()));
								taxResult.put(CommonConstants.TAXNAME, String.valueOf(taxobj.getName()));
							}
						}
						resultJSONArrayTemp.put(taxResult);
					}
					result.put(CommonConstants.TAXLIST, resultJSONArrayTemp);
				}else{
					result.put(CommonConstants.TAXLIST, resultJSONArrayTemp);
				}
		 }else{
			 result.put(CommonConstants.TAXLIST, "");
		 }
		return result;
	}
	
	public static JSONObject getTaxDetatilsWithAmoount(PayrollTaxDetailsDO payrollTax, Double amount)throws JSONException {
		//List<DeductionSettingDO> taxList = deductionSettingsService.retrieveById(payrollTax.getTaxId());
		JSONObject result = new JSONObject();
		/*if(taxList != null && taxList.size() > 0 ){
			result.put(CommonConstants.AMOUNT, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(amount))));
			result.put(CommonConstants.TAXID, String.valueOf(taxList.get(0).getDeductionsettingId()));
			result.put(CommonConstants.TAXNAME, String.valueOf(taxList.get(0).getName()));
			
		}else{
			result.put(CommonConstants.TAXID, String.valueOf(""));
			result.put(CommonConstants.TAXNAME, String.valueOf(""));
			result.put(CommonConstants.AMOUNT, String.valueOf(""));
		}*/
		return result;
	}
	public static JSONObject getPayrollHeadingList(List<AllowanceSettingDO> allowanceList,List<DeductionSettingDO> deductionList,List<TaxSettingsDO> taxList, List<WorkingDayAllowanceDO> workingDayList, List<AttendanceAllowanceDO> attendeanceList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			
			resultJSONArray.put(getEarningHeadList(allowanceList,workingDayList,attendeanceList));
			resultJSONArray.put(getDeductionHeadList(deductionList));
			resultJSONArray.put(getTaxHeadList(taxList));
			
			
			
			/*for (AllowanceSettingDO allowance : allowanceList) {
				resultJSONArray.put(allowance.getDisplayname());
			}*/
			/*resultJSONArray.put("Other Earnings");
			resultJSONArray.put("Bonus");*/
		
			/*for (WorkingDayAllowanceDO workingDay : workingDayList) {
				resultJSONArray.put(workingDay.getName());
			}*/
			/*resultJSONArray.put("Provident Fund");
			resultJSONArray.put("State Insurance");
			resultJSONArray.put("Loss Of Pay");
			resultJSONArray.put("General Deduction");
			for (DeductionSettingDO deduction : deductionList) {
				resultJSONArray.put(deduction.getDisplayname());
			}*/
			/*resultJSONArray.put("TDS");
			resultJSONArray.put("Professional Tax");
			resultJSONArray.put("Education Cess");
			for (TaxSettingsDO tax : taxList) {
				resultJSONArray.put(tax.getDisplayname());
			}*/
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONArray getDeductionHeadList(List<DeductionSettingDO> deductionList)throws JSONException {
		JSONArray result = new JSONArray();
		result.put("Provident Fund");
		//result.put("State Insurance");
		result.put("Employees' State Insurance");
		result.put("Loss Of Pay");
		//result.put("General Deduction");
		for (DeductionSettingDO deduction : deductionList) {
			result.put(deduction.getDisplayname());
			
		}
		
		List<GeneralDeductionDetailDO> generalDetailList = generalDeductionDetailsService.retrieve();  
		if(generalDetailList != null && generalDetailList.size() > 0){
			for(GeneralDeductionDetailDO generalDetail : generalDetailList){
				result.put(generalDetail.getLoanPayment().getDeductionSubType().getName());
			}
		}
		
		return result;
	}
	
	public static JSONArray getEarningHeadList(List<AllowanceSettingDO> allowanceList, List<WorkingDayAllowanceDO> workingDayList, List<AttendanceAllowanceDO> attendeanceList)throws JSONException {
		JSONArray result = new JSONArray();
		for (AllowanceSettingDO allowance : allowanceList) {
			result.put(allowance.getDisplayname());
		}
		for (WorkingDayAllowanceDO workingDay : workingDayList) {
			result.put(workingDay.getName());
		}
		
		for (AttendanceAllowanceDO attendeance : attendeanceList) {
			result.put(attendeance.getName());
		}
		List<GeneralEarningDO> generalEarningList = generalEarningService.retrieve();
		if(generalEarningList != null && generalEarningList.size() > 0){
			for(GeneralEarningDO generalEarning: generalEarningList){
				result.put(generalEarning.getEarningSubType().getName());
			}
		}
		
		result.put("Other Earnings");
		result.put("Bonus");
		result.put("Claims");
		return result;
	}
	public static JSONArray getTaxHeadList(List<TaxSettingsDO> taxList)throws JSONException {
		JSONArray result = new JSONArray();
		result.put("TDS");
		result.put("Professional Tax");
		result.put("Education Cess");
		for (TaxSettingsDO tax : taxList) {
			result.put(tax.getDisplayname());
		}
		return result;
	}
	
	public static JSONObject getPayrollMonthDetails(List<EmployeePayrollDO> employeePayrollList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeePayrollDO empCompensation : employeePayrollList) {
					resultJSONArray.put(getPayrollMonthlyDetail(empCompensation));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	
	public static JSONObject getPayrollMonthlyDetail(EmployeePayrollDO payrollDetails)throws JSONException {
		JSONObject result = new JSONObject();
		
		if(payrollDetails.getEmployee() != null){
			result.put(CommonConstants.EMPID,payrollDetails.getEmployee().getEmpId());
			String empName = CommonUtil.getEmpObject(payrollDetails.getEmployee().getEmpId());
			result.put(CommonConstants.EMPNAME,empName);
		}
		result.put(CommonConstants.MONTH,payrollDetails.getPayrollMonth());
		if(payrollDetails.getPayrollTax() != null){
			for (PayrollTaxDetailsDO tax : payrollDetails.getPayrollTax()) {
				
				if(tax.getName() != null && tax.getName().equalsIgnoreCase("TDS")){
					result.put(CommonConstants.TDSAMOUNT,tax.getMonthly());
				}
				if(tax.getName() != null &&  tax.getName().equalsIgnoreCase("Professional Tax")){
					result.put(CommonConstants.PTAMOUNT,tax.getMonthly());
				}
				
			}
		}
		
		if(payrollDetails.getPayrollDeduction() != null){
			for (PayrollDeductionDetailsDO deduction : payrollDetails.getPayrollDeduction()) {
				if(deduction.getName() != null &&  deduction.getName().equalsIgnoreCase("Provident Fund")){
					result.put(CommonConstants.PFAMOUNT,deduction.getMonthly());
				}
				if(deduction.getName() != null &&  deduction.getName().equalsIgnoreCase("Employees' State Insurance")){
					result.put(CommonConstants.ESIAMOUNT,deduction.getMonthly());
				}
				
			}
		}
		
		
		
		
		
		return result;
	}
	
}
