package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.AdvanceTypeDO;
import com.spheresuite.erp.util.CommonConstants;

@Component
public class AdvanceTypeUtil {
	
	private AdvanceTypeUtil() {}
	
	public static JSONObject getAdvanceTypeList(List<AdvanceTypeDO> advanceTypeList){
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (AdvanceTypeDO advanceType : advanceTypeList) {
				resultJSONArray.put(getAdvanceDetailObject(advanceType));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getAdvanceDetailObject(AdvanceTypeDO advanceType)throws JSONException{
		
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(advanceType.getAdvanceTypeId()));
		result.put(CommonConstants.NAME, String.valueOf(advanceType.getName()));
		result.put(CommonConstants.STATUS, String.valueOf(advanceType.getStatus()));
		if(advanceType.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(advanceType.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(advanceType.getUpdatedon())));
		return result;
	}
}
