package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.PayrollGroupDeductionDO;
import com.spheresuite.erp.domainobject.PayrollGroupEarningsDO;
import com.spheresuite.erp.domainobject.PayrollGroupTaxDO;
import com.spheresuite.erp.util.CommonConstants;

@Component
public class PayrollGroupSettingsUtil {
	
	private PayrollGroupSettingsUtil() {}
	
	public static JSONObject getPayrollGroupSettingsList(List<PayrollGroupEarningsDO> payrollGroupEarningList, List<PayrollGroupDeductionDO> payrollGroupDeductionList, List<PayrollGroupTaxDO> payrollGroupTaxList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONObject result = new JSONObject();
			
			JSONArray payrollGroupEarningDetailsJSONArray= new JSONArray();
			for (PayrollGroupEarningsDO payrollGroupEarning : payrollGroupEarningList) {
				JSONObject payrollGroupEarningDetails = new JSONObject();
				payrollGroupEarningDetails.put(CommonConstants.ID, String.valueOf(payrollGroupEarning.getPayrollGroupEarningsId()));
				payrollGroupEarningDetails.put(CommonConstants.ALLOWENCE_ID, String.valueOf(payrollGroupEarning.getAllowanceSettings().getAllowanceSettingsId()));
				payrollGroupEarningDetailsJSONArray.put(payrollGroupEarningDetails);
			}
			
			JSONArray payrollGroupDeductionDetailsJSONArray= new JSONArray();
			for (PayrollGroupDeductionDO payrollGroupDeduction : payrollGroupDeductionList) {
				JSONObject payrollGroupDeductionDetails = new JSONObject();
				payrollGroupDeductionDetails.put(CommonConstants.ID, String.valueOf(payrollGroupDeduction.getPayrollGroupDeductionId()));
				payrollGroupDeductionDetails.put(CommonConstants.DEDUCTIONID, String.valueOf(payrollGroupDeduction.getDeductionSettings().getDeductionsettingId()));
				payrollGroupDeductionDetailsJSONArray.put(payrollGroupDeductionDetails);
			}
			
			JSONArray payrollGroupTaxDetailsJSONArray= new JSONArray();
			for (PayrollGroupTaxDO payrollGrouptax : payrollGroupTaxList) {
				JSONObject payrollGroupTaxDetails = new JSONObject();
				payrollGroupTaxDetails.put(CommonConstants.ID, String.valueOf(payrollGrouptax.getPayrollGroupTaxId()));
				payrollGroupTaxDetails.put(CommonConstants.TAXID, String.valueOf(payrollGrouptax.getTaxSettings().getTaxSettingId()));
				payrollGroupTaxDetailsJSONArray.put(payrollGroupTaxDetails);
			}
			
			result.put(CommonConstants.EARNINGID, payrollGroupEarningDetailsJSONArray);
			result.put(CommonConstants.DEDUCTIONID, payrollGroupDeductionDetailsJSONArray);
			result.put(CommonConstants.TAXID, payrollGroupTaxDetailsJSONArray);
			resultJSON.put(CommonConstants.RESULTS, result);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	/*public static JSONObject getPayrollGroupDetailObject(PayrollGroupDO payrollGroup)throws JSONException{
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(payrollGroup.getPayrollGroupId()));
		result.put(CommonConstants.NAME, String.valueOf(payrollGroup.getName()));
		if(payrollGroup.getDescription() != null){
			result.put(CommonConstants.DISPLAYNAME, String.valueOf(payrollGroup.getDescription()));
		}else{
			result.put(CommonConstants.DISPLAYNAME, "");
		}
		result.put(CommonConstants.STATUS, String.valueOf(payrollGroup.getStatus()));
		if(payrollGroup.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(payrollGroup.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(payrollGroup.getUpdatedon())));
		return result;
	}*/
}
