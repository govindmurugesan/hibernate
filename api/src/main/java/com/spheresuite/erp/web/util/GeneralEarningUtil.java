package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.GeneralEarningDO;
import com.spheresuite.erp.util.CommonConstants;
@Component
public class GeneralEarningUtil {
	
	private GeneralEarningUtil() {}
	

	public static JSONObject getAdvancePaymentList(List<GeneralEarningDO> loanPaymentList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (GeneralEarningDO loanPayment : loanPaymentList) {
				resultJSONArray.put(getAdvancePaymentDetailObject(loanPayment));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getAdvancePaymentDetailObject(GeneralEarningDO loanPayment)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(loanPayment.getGeneralEarningId()));
		if(loanPayment.getEmployee().getEmpId() != null ){
			result.put(CommonConstants.EMPID, String.valueOf(loanPayment.getEmployee().getEmpId()));
			if(loanPayment.getEmployee().getMiddlename() != null) result.put(CommonConstants.EMPNAME, String.valueOf(loanPayment.getEmployee().getFirstname() + " " +loanPayment.getEmployee().getMiddlename() + " " + loanPayment.getEmployee().getLastname()));
			else if(loanPayment.getEmployee().getMiddlename() != null && loanPayment.getEmployee().getLastname() == null ) result.put(CommonConstants.EMPNAME, String.valueOf(loanPayment.getEmployee().getFirstname() + " " +loanPayment.getEmployee().getMiddlename()));
			else if(loanPayment.getEmployee().getMiddlename() == null && loanPayment.getEmployee().getLastname() == null ) result.put(CommonConstants.EMPNAME,String.valueOf(loanPayment.getEmployee().getFirstname()));
			else result.put(CommonConstants.EMPNAME, String.valueOf(loanPayment.getEmployee().getFirstname() + " " + loanPayment.getEmployee().getLastname()));
		}else{
			result.put(CommonConstants.EMPID, "");
			result.put(CommonConstants.EMPNAME, ""); 
		}
		result.put(CommonConstants.AMOUNT, String.valueOf(loanPayment.getAmount()));
		if(loanPayment.getMonth() != null){
			result.put(CommonConstants.MONTHLY, String.valueOf(loanPayment.getMonth()));
		}else{
			result.put(CommonConstants.MONTHLY, "");
		}
		if(loanPayment.getEarningSubType() != null){
			result.put(CommonConstants.TYPE_ID, String.valueOf(loanPayment.getEarningSubType().getEarningSubTypeId()));
			result.put(CommonConstants.TYPE, String.valueOf(loanPayment.getEarningSubType().getName()));
		}else{
			result.put(CommonConstants.TYPE_ID, "");
			result.put(CommonConstants.TYPE, "");
		}
		
		if(loanPayment.getEarningType() != null){
			result.put(CommonConstants.EARNINGTYPEID, String.valueOf(loanPayment.getEarningType().getEarningTypeId()));
			result.put(CommonConstants.EARNINGNAME, String.valueOf(loanPayment.getEarningType().getDisplayname()));
		}else{
			result.put(CommonConstants.EARNINGTYPEID, "");
			result.put(CommonConstants.EARNINGNAME, "");
		}
		
		if(loanPayment.getUnitOrBranch() != null){
			result.put(CommonConstants.UNIT, String.valueOf(loanPayment.getUnitOrBranch().getUnitOrBranchId()));
			result.put(CommonConstants.UNITNAME, String.valueOf(loanPayment.getUnitOrBranch().getUnitOrBranch()));
		}else{
			result.put(CommonConstants.UNIT, "");
			result.put(CommonConstants.UNITNAME, "");
		}
		
		result.put(CommonConstants.STATUS, String.valueOf(loanPayment.getStatus()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(loanPayment.getUpdatedon())));
		if(loanPayment.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(loanPayment.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		
		return result;
	}
	
}
