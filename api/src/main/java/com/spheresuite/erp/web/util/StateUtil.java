package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.StateDO;
import com.spheresuite.erp.util.CommonConstants;

@Component
public class StateUtil {
	
	private StateUtil() {}
	
	public static JSONObject getStateList(List<StateDO> stateList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (StateDO state : stateList) {
				resultJSONArray.put(getStateDetailObject(state));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getStateDetailObject(StateDO state)throws JSONException {
		JSONObject result = new JSONObject();
		//System.out.println(country.getCountryId().getName());
		result.put(CommonConstants.ID, String.valueOf(state.getStateId()));
		result.put(CommonConstants.NAME, String.valueOf(state.getStateName()));
		result.put(CommonConstants.COUNTRY_ID, String.valueOf(state.getCountry().getCountryId()));
		result.put(CommonConstants.COUNTRY_NAME, String.valueOf(state.getCountry().getCountryName()));
		result.put(CommonConstants.STATUS, String.valueOf(state.getStatus()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(state.getUpdatedon())));
		if(state.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(state.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		return result;
	}
}
