package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.ExpenseTypeDO;
import com.spheresuite.erp.util.CommonConstants;

@Component
public class ExpenseTypeUtil {
	
	private ExpenseTypeUtil() {}
	
	public static JSONObject getExpenseTypeList(List<ExpenseTypeDO> expenseTypeList){
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (ExpenseTypeDO expenseTypeDO : expenseTypeList) {
				resultJSONArray.put(getExpenseDetailObject(expenseTypeDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getExpenseDetailObject(ExpenseTypeDO expenseTypeDO)throws JSONException{
		
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(expenseTypeDO.getExpenseTypeId()));
		result.put(CommonConstants.NAME, String.valueOf(expenseTypeDO.getName()));
		result.put(CommonConstants.STATUS, String.valueOf(expenseTypeDO.getStatus()));
		if(expenseTypeDO.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(expenseTypeDO.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		if(expenseTypeDO.getDisplayName() != null){
			result.put(CommonConstants.DISPLAYNAME, expenseTypeDO.getDisplayName()); 
		}else{
			result.put(CommonConstants.DISPLAYNAME, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(expenseTypeDO.getUpdatedon())));
		return result;
	}
}
