package com.spheresuite.erp.web.util;

import java.io.UnsupportedEncodingException;
import java.util.Base64;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.EmailSettingsDO;
import com.spheresuite.erp.util.CommonConstants;

@Component
public class EmailSettingsUtil {
	
	private EmailSettingsUtil() {}
	
	public static JSONObject getEmailSettingsList(List<EmailSettingsDO> emailSettingsDOList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmailSettingsDO emailSettingsDO : emailSettingsDOList) {
				resultJSONArray.put(getEmailSettingsObject(emailSettingsDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getEmailSettingsObject(EmailSettingsDO emailSettingsDO)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(emailSettingsDO.getEmailSettingId()));
		result.put(CommonConstants.MAIL_TYPE, String.valueOf(emailSettingsDO.getMailtype()));
		result.put(CommonConstants.IMAP_HOST, String.valueOf(emailSettingsDO.getImaphost()));
		result.put(CommonConstants.PORT, String.valueOf(emailSettingsDO.getImapport()));
		result.put(CommonConstants.SMTP_HOST, String.valueOf(emailSettingsDO.getSmtphost()));
		result.put(CommonConstants.SMTP_PORT, String.valueOf(emailSettingsDO.getSmtpport()));
		result.put(CommonConstants.EMAIL, String.valueOf(emailSettingsDO.getSendemail()));
		byte[] decodedBytes = Base64.getDecoder().decode(emailSettingsDO.getSendpassword());
		String stringDecode = null;
		try {
			stringDecode = new String(decodedBytes, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String pwd = stringDecode.toString();
		result.put(CommonConstants.PASSWORD, String.valueOf(pwd));
		result.put(CommonConstants.AUTHENTICATE, String.valueOf(emailSettingsDO.getAuthType()));
		result.put(CommonConstants.TLS_ENABLED, String.valueOf(emailSettingsDO.getTlsEnable()));
		result.put(CommonConstants.STATUS, String.valueOf(emailSettingsDO.getStatus()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(emailSettingsDO.getUpdatedon())));
		if(emailSettingsDO.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(emailSettingsDO.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		return result;
	}
}
