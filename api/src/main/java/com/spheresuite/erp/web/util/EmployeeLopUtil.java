
package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.EmployeeLopDO;
import com.spheresuite.erp.util.CommonConstants;
@Component
public class EmployeeLopUtil {
	
	private EmployeeLopUtil() {}

	public static JSONObject getEmployeeLopList(List<EmployeeLopDO> employeeLopList, String frequency, String period) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			if(frequency != null){
				resultJSON.put(CommonConstants.FREQUENCY, String.valueOf(frequency));
			}else{
				resultJSON.put(CommonConstants.FREQUENCY, String.valueOf(""));
			}
			
			if(period != null){
				resultJSON.put(CommonConstants.PERIOD, String.valueOf(period));
			}else{
				resultJSON.put(CommonConstants.PERIOD, String.valueOf(""));
			}
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeeLopDO employeeLopDO : employeeLopList) {
				resultJSONArray.put(getEmployeeLopDetailObject(employeeLopDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getEmployeeLopDetailObject(EmployeeLopDO employeeLopDO)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(employeeLopDO.getLopId()));
		
		if(employeeLopDO.getEnddate() != null){
			result.put(CommonConstants.ENDDATE,String.valueOf(CommonUtil.convertDateToStringWithOutComma(employeeLopDO.getEnddate())));
		}else{
			result.put(CommonConstants.ENDDATE,String.valueOf(""));
		}
		result.put(CommonConstants.STARTDATE,String.valueOf(employeeLopDO.getStartdate() != null ? CommonUtil.convertDateToStringWithOutComma(employeeLopDO.getStartdate()) : ""));

		result.put(CommonConstants.STATUS, String.valueOf(employeeLopDO.getStatus()));
	
		if(employeeLopDO.getType() != null){
			result.put(CommonConstants.LOPTYPE,String.valueOf(employeeLopDO.getType()));
		}else{
			result.put(CommonConstants.LOPTYPE,String.valueOf(""));
		}
		if(employeeLopDO.getEmployee().getEmpId() != null){
			result.put(CommonConstants.EMPID, employeeLopDO.getEmployee().getEmpId());
			String empName = CommonUtil.getEmpObject(employeeLopDO.getEmployee().getEmpId());
			result.put(CommonConstants.EMPNAME, empName); 
		}else{
			result.put(CommonConstants.EMPNAME, ""); 
			result.put(CommonConstants.EMPID, "");
		}
		return result;
	}
}
