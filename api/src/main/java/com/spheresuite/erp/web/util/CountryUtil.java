package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.CountryDO;
import com.spheresuite.erp.util.CommonConstants;
@Component
public class CountryUtil {
	
	private CountryUtil() {}
	
	public static JSONObject getCountryList(List<CountryDO> countryList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (CountryDO country : countryList) {
				resultJSONArray.put(getCountryDetailObject(country));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getCountryDetailObject(CountryDO country)throws JSONException {
		JSONObject result = new JSONObject();
	//	System.out.println("cc"+country.getState().iterator().next());
		result.put(CommonConstants.ID, String.valueOf(country.getCountryId()));
		result.put(CommonConstants.NAME, String.valueOf(country.getCountryName()));
		result.put(CommonConstants.CODE, String.valueOf(country.getCountryCode()));
		result.put(CommonConstants.STATUS, String.valueOf(country.getStatus()));
		result.put(CommonConstants.CALLING_CODE, String.valueOf(country.getCallingCode()));
		result.put(CommonConstants.CURRENCY_TYPE, String.valueOf(country.getCurrencyType()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(country.getUpdatedon())));
		if(country.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(country.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		return result;
	}
}
