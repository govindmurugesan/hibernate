package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.EarningSubTypeDO;
import com.spheresuite.erp.util.CommonConstants;

@Component
public class EarningSubTypeUtil {
	
	private EarningSubTypeUtil() {}
	
	public static JSONObject getEarningTypeList(List<EarningSubTypeDO> earningTypeList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EarningSubTypeDO earningType : earningTypeList) {
				resultJSONArray.put(getEarningTypeDetailObject(earningType));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getEarningTypeDetailObject(EarningSubTypeDO earningType)throws JSONException{
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(earningType.getEarningSubTypeId()));
		result.put(CommonConstants.NAME, String.valueOf(earningType.getName()));
		if(earningType.getEarningType() != null){
			result.put(CommonConstants.EARNINGTYPEID, String.valueOf(earningType.getEarningType().getEarningTypeId()));
			result.put(CommonConstants.EARNINGNAME, String.valueOf(earningType.getEarningType().getDisplayname()));
		}else{
			result.put(CommonConstants.EARNINGTYPEID, "");
			result.put(CommonConstants.EARNINGNAME, "");
		}
		result.put(CommonConstants.STATUS, String.valueOf(earningType.getStatus()));
		if(earningType.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(earningType.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(earningType.getUpdatedon())));
		return result;
	}
}
