package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.EmployeeTravelDO;
import com.spheresuite.erp.util.CommonConstants;

@Component
public class EmployeeTravelUtil {
	
	private EmployeeTravelUtil() {}
	
	public static JSONObject getempTravelList(List<EmployeeTravelDO> empTravelList){
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeeTravelDO empTravelDetail : empTravelList) {
				resultJSONArray.put(getTravelDetailObject(empTravelDetail));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getTravelDetailObject(EmployeeTravelDO empTravelDetail)throws JSONException{
		
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(empTravelDetail.getEmpTravelId()));
		if(empTravelDetail.getEmployee() != null){
			String empName = CommonUtil.getEmpObject(empTravelDetail.getEmployee().getEmpId());
			result.put(CommonConstants.EMPNAME, empName); 
			result.put(CommonConstants.EMPID, empTravelDetail.getEmployee().getEmpId()); 
			
		}else{
			result.put(CommonConstants.EMPNAME, ""); 
			result.put(CommonConstants.EMPID, ""); 
		}
		
		if(empTravelDetail.getReportingTo() != null){
			String empName = CommonUtil.getEmpObject(empTravelDetail.getReportingTo());
			result.put(CommonConstants.REPORTTO_NAME, empName); 
			result.put(CommonConstants.REPORTTO, empTravelDetail.getReportingTo()); 
			
		}else{
			result.put(CommonConstants.REPORTTO_NAME, ""); 
			result.put(CommonConstants.REPORTTO, ""); 
		}
		
		
		if(empTravelDetail.getFromDate() != null) result.put(CommonConstants.FROMDATE, String.valueOf(empTravelDetail.getFromDate()));
		else result.put(CommonConstants.FROMDATE, String.valueOf(""));
		
		if(empTravelDetail.getToDate() != null) result.put(CommonConstants.TODATE, String.valueOf(empTravelDetail.getToDate()));
		else result.put(CommonConstants.TODATE, String.valueOf(""));
		
		if(empTravelDetail.getComments() != null) result.put(CommonConstants.COMMENT, String.valueOf(empTravelDetail.getComments()));
		else result.put(CommonConstants.COMMENT, String.valueOf(""));
		
		
		if(empTravelDetail.getPlace() != null) result.put(CommonConstants.ADDRESS, String.valueOf(empTravelDetail.getPlace()));
		else result.put(CommonConstants.ADDRESS, String.valueOf(""));
		
		if(empTravelDetail.getType() != null) result.put(CommonConstants.TYPE, String.valueOf(empTravelDetail.getType()));
		else result.put(CommonConstants.TYPE, String.valueOf(""));
		
		
		if(empTravelDetail.getStatus() != null) result.put(CommonConstants.STATUS, String.valueOf(empTravelDetail.getStatus()));
		else result.put(CommonConstants.STATUS, String.valueOf(""));
		
		if(empTravelDetail.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(empTravelDetail.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(empTravelDetail.getUpdatedon())));
		return result;
	}
}
