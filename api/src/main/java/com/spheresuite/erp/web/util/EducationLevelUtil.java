package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.EducationLevelDO;
import com.spheresuite.erp.util.CommonConstants;
@Component
public class EducationLevelUtil {
	
	private EducationLevelUtil() {}
	
	public static JSONObject getEducationLevelTypeList(List<EducationLevelDO> educationLevelList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EducationLevelDO educationLevel : educationLevelList) {
				resultJSONArray.put(getEducationLevelDetailObject(educationLevel));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getEducationLevelDetailObject(EducationLevelDO educationLevel)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(educationLevel.getEducationLevelId()));
		result.put(CommonConstants.NAME, String.valueOf(educationLevel.getName()));
		result.put(CommonConstants.STATUS, String.valueOf(educationLevel.getStatus()));
		if(educationLevel.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(educationLevel.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(educationLevel.getUpdatedon())));
		return result;
	}
}
