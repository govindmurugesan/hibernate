package com.spheresuite.erp.web.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.EarnedLeavesDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.EmployeeLeavegroupDO;
import com.spheresuite.erp.domainobject.LeaveGroupDO;
import com.spheresuite.erp.domainobject.LeaveGroupSettingsDO;
import com.spheresuite.erp.domainobject.LeaveRequestsDO;
import com.spheresuite.erp.domainobject.UserDO;
import com.spheresuite.erp.service.EarnedLeavesService;
import com.spheresuite.erp.service.EmployeeLeavegroupService;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.LeaveGroupSettingsService;
import com.spheresuite.erp.service.LeaveRequestsService;
import com.spheresuite.erp.util.CommonConstants;
@Component
public class LeaveRequestsUtil {
	
	private LeaveRequestsUtil() {}
	
	@Autowired
	private EmployeeService temployeeService;
	private static EmployeeService employeeService;
	
	@Autowired
	private LeaveRequestsService tleaveRequestsService;
	private static LeaveRequestsService leaveRequestsService;
	
	@Autowired
	private LeaveGroupSettingsService tleaveGroupSettingsService;
	private static LeaveGroupSettingsService leaveGroupSettingsService;
	
	@Autowired
	private EmployeeLeavegroupService temployeeleaveGroupService;
	private static EmployeeLeavegroupService employeeleaveGroupService;
	
	@Autowired
	private EarnedLeavesService tearnedLeavesService;
	private static EarnedLeavesService earnedLeavesService;
	
	
	
	@PostConstruct
	public void init() {
		employeeService = temployeeService;
		leaveRequestsService = tleaveRequestsService;
		leaveGroupSettingsService = tleaveGroupSettingsService;
		employeeleaveGroupService  = temployeeleaveGroupService;
		earnedLeavesService = tearnedLeavesService;
	}
	
	public static JSONObject getLeaveRequestsList(List<LeaveRequestsDO> leaveRequestsList) throws ParseException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (LeaveRequestsDO leaveRequest : leaveRequestsList) {
				resultJSONArray.put(getLeaveRequestsDetailObject(leaveRequest));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	
	public static String getLeaveRequestsListTime(List<LeaveRequestsDO> leaveRequestsList) throws ParseException {
		try {
			 ArrayList<String> timestampsList = new ArrayList<String>();
			for (LeaveRequestsDO leaveRequestsDO : leaveRequestsList) {
				timestampsList.add(leaveRequestsDO.getNumberofhours()+":00");
			}
			return CommonUtil.totalHours(timestampsList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	
	// old
	/*public static JSONObject getLeaveRequestsDetailForFilter(List<UserDO> userList, Date startDate, Date endDate, List<LeaveTypeDO> leveTypeList) throws ParseException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for(LeaveTypeDO leaveType : leveTypeList){
				for (UserDO user : userList) {
					resultJSONArray.put(getLeaveRequestsDetailObjectForFilter(user,startDate,endDate, leaveType));
				}
			}
			for (UserDO user : userList) {
				resultJSONArray.put(getLeavePermissionDetailObjectForFilter(user,startDate,endDate));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}*/
	
	
	public static JSONObject getLeaveRequestsDetailForFilter(List<UserDO> userList, Date startDate, Date endDate, Long unitId,Long type) throws ParseException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
				for (UserDO user : userList) {
					if(unitId != null){
						if(user.getEmployee() != null && user.getEmployee().getEmpId() != null && user.getEmployee().getUnitOrBranch() != null && user.getEmployee().getUnitOrBranch().getUnitOrBranchId().compareTo(unitId) == 0){
							List<EmployeeLeavegroupDO> leaveGroupList = employeeleaveGroupService.retrieveByEmpId(user.getEmployee().getEmpId().toString());
			 				if(leaveGroupList != null && leaveGroupList.size() > 0 && leaveGroupList.get(0).getLeaveGroup() != null){
			 					resultJSONArray.put(getLeaveRequestsDetailObjectForFilter(user, startDate,endDate, leaveGroupList.get(0).getLeaveGroup(),type));
			 				}
						}
					}else{
						if(user.getEmployee() != null && user.getEmployee().getEmpId() != null){
							List<EmployeeLeavegroupDO> leaveGroupList = employeeleaveGroupService.retrieveByEmpId(user.getEmployee().getEmpId().toString());
			 				if(leaveGroupList != null && leaveGroupList.size() > 0 && leaveGroupList.get(0).getLeaveGroup() != null){
			 					resultJSONArray.put(getLeaveRequestsDetailObjectForFilter(user, startDate,endDate, leaveGroupList.get(0).getLeaveGroup(),type));
			 				}
						}
					}
					
					
				}
			for (UserDO user : userList) {
				resultJSONArray.put(getLeavePermissionDetailObjectForFilter(user,startDate,endDate));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	//old
	/*public static JSONObject getEmpLeaveRequestsDetailForFilter(String empId, Date startDate, Date endDate, List<LeaveTypeDO> leveTypeList) throws ParseException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for(LeaveTypeDO leaveType : leveTypeList){
					resultJSONArray.put(getEmpLeaveRequestsDetailObjectForFilter(empId,startDate,endDate, leaveType));
			}
			resultJSONArray.put(getLeavePermissionDetailObjectForEmpIdFilter(empId,startDate,endDate));
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}*/
	
	
	public static JSONObject getEmpLeaveRequestsDetailForFilter(String empId, Date startDate, Date endDate, Long type) throws ParseException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			
			List<EmployeeDO> empList = employeeService.retrieveEmpId(empId);
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getEmpId() != null){
					List<EmployeeLeavegroupDO> leaveGroupList = employeeleaveGroupService.retrieveByEmpId(empList.get(0).getEmpId().toString());
	 				if(leaveGroupList != null && leaveGroupList.size() > 0 && leaveGroupList.get(0).getLeaveGroup() != null){
	 					resultJSONArray.put(getEmpLeaveRequestsDetailObjectForFilter(empId,startDate,endDate,leaveGroupList.get(0).getLeaveGroup(),type));
	 				}
					
					
					/*List<LeaveGroupSettingsDO> leaveGroupList = leaveGroupSettingsService.retrieveByGroupID(empList.get(0).getLeaveGroup().getLeaveGroupId());
					if(leaveGroupList != null && leaveGroupList.size() > 0){
						for(LeaveGroupSettingsDO leaveGrp : leaveGroupList){
							resultJSONArray.put(getEmpLeaveRequestsDetailObjectForFilter(empId,startDate,endDate, leaveGrp));
						}
					}*/
				}
				
			}
			resultJSONArray.put(getLeavePermissionDetailObjectForEmpIdFilter(empId,startDate,endDate));
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getLeaveRequestsDetailObject(LeaveRequestsDO leaveRequest)throws JSONException, ParseException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(leaveRequest.getLeaveRequestId()));
		if(leaveRequest.getDescription() != null){
			result.put(CommonConstants.DESC, String.valueOf(leaveRequest.getDescription()));
		}else{
			result.put(CommonConstants.DESC, String.valueOf(""));
		}
		
		/*if(leaveRequest.getLeaveType() != null){
			result.put(CommonConstants.TYPE, String.valueOf(leaveRequest.getLeaveType().getLeaveTypeId()));
			result.put(CommonConstants.TYPENAME, String.valueOf(leaveRequest.getLeaveType().getType()));
		}else{
			result.put(CommonConstants.TYPE, "");
			result.put(CommonConstants.TYPENAME, "");
		}*/
		if(leaveRequest.getLeaveManagement() != null){
			result.put(CommonConstants.TYPE, String.valueOf(leaveRequest.getLeaveManagement().getLeaveManagementId()));
			result.put(CommonConstants.TYPENAME, String.valueOf(leaveRequest.getLeaveManagement().getLeaveType()));
		}else{
			result.put(CommonConstants.TYPE, "");
			result.put(CommonConstants.TYPENAME, "");
		
		}
		
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(leaveRequest.getUpdatedon())));
		result.put(CommonConstants.CREATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(leaveRequest.getCreatedon())));
		result.put(CommonConstants.CREATEDONDISPLAY, String.valueOf(CommonUtil.convertStringToDateMMM(leaveRequest.getCreatedon())));
		result.put(CommonConstants.STATUS, String.valueOf(leaveRequest.getStatus()));
		result.put(CommonConstants.LEAVEDAY, String.valueOf(leaveRequest.getType()));
		if(leaveRequest.getComments() != null ){
			result.put(CommonConstants.COMMENT, String.valueOf(leaveRequest.getComments()));
		}else{
			result.put(CommonConstants.COMMENT, "");
		}
		if(leaveRequest.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(leaveRequest.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		
		if(leaveRequest.getReportingTo() != null){
			String empName = CommonUtil.getEmpObject(leaveRequest.getEmployee().getEmpId());
			result.put(CommonConstants.REPORTTO, empName); 
		}else{
			result.put(CommonConstants.REPORTTO, ""); 
		}
		
		
		if(leaveRequest.getEmployee() != null && leaveRequest.getEmployee().getUnitOrBranch() != null && leaveRequest.getEmployee().getUnitOrBranch().getManagerId() != null ){
			String empName = CommonUtil.getEmpObject(leaveRequest.getEmployee().getUnitOrBranch().getManagerId().getEmpId());
			result.put(CommonConstants.MANAGERID, leaveRequest.getEmployee().getUnitOrBranch().getManagerId().getEmpId()); 
			result.put(CommonConstants.MANAGERNAME, empName); 
		}else{
			result.put(CommonConstants.MANAGERID, ""); 
			result.put(CommonConstants.MANAGERNAME, ""); 
		}
		
		if(leaveRequest.getEmployee() != null){
			String empName = CommonUtil.getEmpObject(leaveRequest.getEmployee().getEmpId());
			result.put(CommonConstants.EMPID, leaveRequest.getEmployee().getEmpId()); 
			result.put(CommonConstants.CREATED_BY, empName); 
		}else{
			result.put(CommonConstants.CREATED_BY, "");
			result.put(CommonConstants.EMPID, ""); 
		}
		if(leaveRequest.getFromDate()!= null){
			SimpleDateFormat todayDate = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date();
		
			SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date dateBeforeString = leaveRequest.getFromDate();
			if(myFormat.format(dateBeforeString).compareTo(todayDate.format(date)) >= 0){
				result.put(CommonConstants.CANCELREQUEST, "true");
			}else{
				result.put(CommonConstants.CANCELREQUEST, "false");
			}
			result.put(CommonConstants.FROMDATE, String.valueOf(leaveRequest.getFromDate()));
			result.put(CommonConstants.FROMDATEDISPLAY, String.valueOf(CommonUtil.convertDateToStringApp(leaveRequest.getFromDate())));
		}else{
			result.put(CommonConstants.FROMDATE, "");
			result.put(CommonConstants.FROMDATEDISPLAY, "");
		}
		if(leaveRequest.getToDate() != null){
			result.put(CommonConstants.TODATE, String.valueOf(leaveRequest.getToDate()));
			result.put(CommonConstants.TODATEDISPLAY, String.valueOf(CommonUtil.convertDateToStringApp(leaveRequest.getToDate())));
		}else{
			result.put(CommonConstants.TODATE, "");
			result.put(CommonConstants.TODATEDISPLAY, "");
		}
		if(leaveRequest.getNumberofhours() != null){
			result.put(CommonConstants.HOURS, String.valueOf(leaveRequest.getNumberofhours()));
		}else{
			result.put(CommonConstants.HOURS, "");
		}
		
		if(leaveRequest.getPermisssionformhours() != null){
			result.put(CommonConstants.PERMISSIONFROM, String.valueOf(leaveRequest.getPermisssionformhours()));
		}else{
			result.put(CommonConstants.PERMISSIONFROM, "");
		}
		
		if(leaveRequest.getPermisssiontohours() != null){
			result.put(CommonConstants.PERMISSIONTO, String.valueOf(leaveRequest.getPermisssiontohours()));
		}else{
			result.put(CommonConstants.PERMISSIONTO, "");
		}
		return result;
	}
	
	public static JSONObject getEmpLeaveRequestsList(List<LeaveRequestsDO> leaveRequestsList)throws ParseException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			JSONObject result = new JSONObject();
			double leaveCount = 0;
			for (LeaveRequestsDO leaveRequest : leaveRequestsList) {
				/*if(String.valueOf(leaveRequest.getLeaveDay()).equals("Single")){
					leaveCount = leaveCount+1;
				}*/
				//if(String.valueOf(leaveRequest.getLeaveDay()).equals("Multiple")){
				//	SimpleDateFormat myFormat = new SimpleDateFormat("dd MM yyyy");
					 /*String dateBeforeString = leaveRequest.getFromDate();
					 String dateAfterString = leaveRequest.getToDate();
					 Date dateBefore = myFormat.parse(dateBeforeString);
				     Date dateAfter = myFormat.parse(dateAfterString);
				     long difference = dateAfter.getTime() - dateBefore.getTime();
				     int daysBetween = (int) (difference / (1000*60*60*24));*/
					
					Date dateBeforeString = leaveRequest.getFromDate();
					 if(leaveRequest.getToDate() != null){
						 Calendar end = Calendar.getInstance();
						 Date dateAfterString = leaveRequest.getToDate();
						 // Date dateAfter = myFormat.parse(dateAfterString);
						  end.setTime(dateAfterString);
						//  Date dateBefore = myFormat.parse(dateBeforeString);
						  Calendar start = Calendar.getInstance();
						  start.setTime(dateBeforeString);
						 for (Date date = start.getTime(); start.before(end) || start.equals(end); start.add(Calendar.DATE, 1), date = start.getTime()) {
							Calendar weekend = Calendar.getInstance();
							weekend.setTime(date);
							if (/*weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && */
								weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){
							     /*long difference = dateAfter.getTime() - dateBefore.getTime();
							     int daysBetween = (int) (difference / (1000*60*60*24));*/
							    // leaveCount = leaveCount+1;
								 String leaveType =  String.valueOf(leaveRequest.getType());
								 if( leaveType  != null && leaveType.equalsIgnoreCase("h") ){
									 leaveCount =  (leaveCount+ 0.5);
								 }else{
									 if( leaveType  != null && leaveType.equalsIgnoreCase("f") ){
										 leaveCount = leaveCount+1;
									 }
								 }
								
							}
						}
					 }else{
						// Date dateBefore = myFormat.parse(dateBeforeString);
						  Calendar start = Calendar.getInstance();
						  start.setTime(dateBeforeString);
						  Date date = start.getTime();
						// for (Date date = start.getTime(); start.before(end) || start.equals(end); start.add(Calendar.DATE, 1), date = start.getTime()) {
							 Calendar weekend = Calendar.getInstance();
							 weekend.setTime(date);
								if (/*weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY &&*/ 
									weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){
								     /*long difference = dateAfter.getTime() - dateBefore.getTime();
								     int daysBetween = (int) (difference / (1000*60*60*24));*/
								   String leaveType =  String.valueOf(leaveRequest.getType());
									 if( leaveType  != null && leaveType.equalsIgnoreCase("h") ){
										 leaveCount =  (leaveCount+ 0.5);
									 }else{
										 if( leaveType  != null && leaveType.equalsIgnoreCase("f") ){
											 leaveCount = leaveCount+1;
										 }
									 }
									
									
								}
						//}
					 }
				     
				//}
			}
			result.put(CommonConstants.COUNT, leaveCount);
			resultJSONArray.put(result);
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	/*public static JSONObject getLeaveStatus(List<UserDO> activeUserList,List<LeaveManagementDO> leaveLists)throws ParseException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for(LeaveManagementDO leaveDetail : leaveLists){
				for (UserDO user : activeUserList) {
					resultJSONArray.put(getUserDetailObject(user,leaveDetail));
				}
			}
			for (UserDO user : activeUserList) {
				resultJSONArray.put(getLeavePermissionDetailObjectForFilter(user,null,null));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}*/
	
	
	public static JSONObject getLeaveStatus(List<UserDO> activeUserList)throws ParseException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
				for (UserDO user : activeUserList) {
					/*if(user.getEmployee() != null && user.getEmployee().getLeaveGroup() != null){
						System.out.println("oo"+user.getEmployee().getLeaveGroup().getLeaveGroupId());
						List<LeaveGroupSettingsDO> leaveGroupList = leaveGroupSettingsService.retrieveByGroupID(user.getEmployee().getLeaveGroup().getLeaveGroupId());
						if(leaveGroupList != null && leaveGroupList.size() > 0){
							for (LeaveGroupSettingsDO leaveType : leaveGroupList) {
								resultJSONArray.put(getUserDetailObject(user, leaveType));
							}
						}
					}*/
					if(user.getEmployee() != null && user.getEmployee().getEmpId() != null){
						List<EmployeeLeavegroupDO> leaveGroupList = employeeleaveGroupService.retrieveByEmpId(user.getEmployee().getEmpId().toString());
		 				if(leaveGroupList != null && leaveGroupList.size() > 0 && leaveGroupList.get(0).getLeaveGroup() != null){
		 					resultJSONArray.put(getUserDetailObject(user,leaveGroupList.get(0).getLeaveGroup()));
		 				}
						
						
					}
				}
			for (UserDO user : activeUserList) {
				resultJSONArray.put(getLeavePermissionDetailObjectForFilter(user,null,null));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	
	
	
	public static JSONObject getUserDetailObject(UserDO user, LeaveGroupDO leavegrp)throws JSONException, ParseException {
		JSONObject result = new JSONObject();
		
		double noOfDays = 0.0;
		List<LeaveGroupSettingsDO> leaveGroupList = leaveGroupSettingsService.retrieveByGroupID(leavegrp.getLeaveGroupId());
		if(leaveGroupList != null && leaveGroupList.size() > 0){
			double availableLeaves = 0.0;
			for (LeaveGroupSettingsDO leaveType : leaveGroupList) {
				if(leaveType.getLeaveManagement() !=null && leaveType.getLeaveManagement().getNumberOfDays() != null){
					noOfDays += leaveType.getLeaveManagement().getNumberOfDays();
				}
				if(leaveType.getLeaveManagement() !=null && leaveType.getLeaveManagement().getEarnedLeave() != null && leaveType.getLeaveManagement().getEarnedLeave().equalsIgnoreCase("e")){
					List<EarnedLeavesDO> employeeDetailList = earnedLeavesService.retrieveByEmpId(user.getEmployee().getEmpId());
					if(employeeDetailList != null && employeeDetailList.size() > 0){
						availableLeaves = employeeDetailList.get(0).getAvailableLeaves() != null ? employeeDetailList.get(0).getAvailableLeaves() : 0;
						//availedLeaves = employeeDetailList.get(0).getAvailedLeaves()!=null?employeeDetailList.get(0).getAvailedLeaves():0;
						noOfDays += availableLeaves;
					}
					
				}
			}
		}
		
		
		if(user.getEmployee() != null){
			result.put(CommonConstants.EMPID, String.valueOf(user.getEmployee().getEmpId()));
			
			if(user.getEmployee().getMiddlename() != null) result.put(CommonConstants.NAME, String.valueOf(user.getEmployee().getFirstname() + " " +user.getEmployee().getMiddlename() + " " + user.getEmployee().getLastname()));
			else if(user.getEmployee().getMiddlename() != null && user.getEmployee().getLastname() == null )result.put(CommonConstants.NAME, String.valueOf(user.getEmployee().getFirstname() + " " +user.getEmployee().getMiddlename()));
			else if(user.getEmployee().getMiddlename() == null && user.getEmployee().getLastname() == null )result.put(CommonConstants.NAME, String.valueOf(user.getEmployee().getFirstname()));
			else result.put(CommonConstants.NAME, String.valueOf(user.getEmployee().getFirstname() + " " + user.getEmployee().getLastname()));
		
			if(user.getEmployee().getDesignation() != null){
				result.put(CommonConstants.DESIGNATION, String.valueOf(user.getEmployee().getDesignation().getDesignationId()));
			}else{
				result.put(CommonConstants.DESIGNATION,"");
			}
			
			double leaveCount = 0;
			double halfCount =  0.5;
			//List<LeaveRequestsDO> leaveRequestsList = leaveRequestsService.retriveByEmpIdStatus(user.getEmployee().getEmpId());
			Calendar cal = Calendar.getInstance();
			int year = Calendar.getInstance().get(Calendar.YEAR);
			cal.set(Calendar.YEAR, year);
			cal.set(Calendar.DAY_OF_YEAR, 1);    
			Date startDate = cal.getTime();
			cal.set(Calendar.YEAR, year);
			cal.set(Calendar.MONTH, 11); 
			cal.set(Calendar.DAY_OF_MONTH, 31); 

			Date endDate = cal.getTime();
			List <LeaveRequestsDO> leaveRequestsList = leaveRequestsService.retrieveLeavesByFromToDate(user.getEmployee().getEmpId(), startDate, endDate);
			
			
			JSONArray resultJSONArray = new JSONArray();
			if(leaveRequestsList != null && leaveRequestsList.size() > 0){
				for (LeaveRequestsDO leaveRequest : leaveRequestsList) {
					if(!leaveRequest.getType().equalsIgnoreCase("p")  && leaveRequest.getLeaveManagement() != null){
						JSONObject leaveDetails = new JSONObject();
						leaveDetails.put(CommonConstants.FROM, String.valueOf(leaveRequest.getFromDate()));
						leaveDetails.put(CommonConstants.FROMDATEDISPLAY, String.valueOf(CommonUtil.convertDateToStringApp(leaveRequest.getFromDate())));
						leaveDetails.put(CommonConstants.TODATE, String.valueOf(leaveRequest.getToDate()));
						leaveDetails.put(CommonConstants.STATUS, String.valueOf(leaveRequest.getStatus()));
						leaveDetails.put(CommonConstants.DESCRIPTION, leaveRequest.getDescription()!=null?String.valueOf(leaveRequest.getDescription()):"");
						
						if(leaveRequest.getComments() != null){
							leaveDetails.put(CommonConstants.COMMENT, String.valueOf(leaveRequest.getComments()));
						}else{
							leaveDetails.put(CommonConstants.COMMENT, String.valueOf(""));
						}
						/*leaveDetails.put(CommonConstants.TYPENAME, leaveRequest.getLeaveType().getType());*/
						leaveDetails.put(CommonConstants.TYPENAME, leaveRequest.getLeaveManagement().getLeaveType());
						leaveDetails.put(CommonConstants.TYPE, String.valueOf(leaveRequest.getType()));
						leaveDetails.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(leaveRequest.getUpdatedon())));
						
						if(leaveRequest.getToDate() != null){
							leaveDetails.put(CommonConstants.TODATE, String.valueOf(leaveRequest.getToDate()));
							leaveDetails.put(CommonConstants.TODATEDISPLAY, String.valueOf(CommonUtil.convertDateToStringApp(leaveRequest.getToDate())));
						}else{
							leaveDetails.put(CommonConstants.TODATE, "");
							leaveDetails.put(CommonConstants.TODATEDISPLAY, "");
						}
						if(leaveRequest.getLeaveGroup() != null){
							leaveDetails.put(CommonConstants.LEAVEGROUP_ID, String.valueOf(leaveRequest.getLeaveGroup().getLeaveGroupId()));
							leaveDetails.put(CommonConstants.LEAVEGROUP, String.valueOf(leaveRequest.getLeaveGroup().getLeaveGroup()));
						}else{
							leaveDetails.put(CommonConstants.LEAVEGROUP_ID, "");
							leaveDetails.put(CommonConstants.LEAVEGROUP, "");
						}
						double totalDays = 0.0;
						double numberOfLeaveTaken = 0.0;
						double availableLeaves = 0.0;
						double availedLeaves = 0.0;
						List<LeaveGroupSettingsDO> groupList = leaveGroupSettingsService.retrieveByGroupID(leavegrp.getLeaveGroupId());
						if(groupList != null && groupList.size() > 0){
							for (LeaveGroupSettingsDO leavegrpType : leaveGroupList) {
								if(leavegrpType.getLeaveManagement() !=null && leavegrpType.getLeaveManagement().getNumberOfDays() != null){
									totalDays += leavegrpType.getLeaveManagement().getNumberOfDays();
									/*if(leavegrpType.getLeaveManagement().getEarnedLeave() != null && leavegrpType.getLeaveManagement().getEarnedLeave().equalsIgnoreCase("e")){
										List<EarnedLeavesDO> employeeDetailList = earnedLeavesService.retrieveByEmpId(user.getEmployee().getEmpId());
										if(employeeDetailList != null && employeeDetailList.size() > 0){
											availableLeaves = employeeDetailList.get(0).getAvailableLeaves() != null ? employeeDetailList.get(0).getAvailableLeaves() : 0;
											//availedLeaves = employeeDetailList.get(0).getAvailedLeaves()!=null?employeeDetailList.get(0).getAvailedLeaves():0;
											totalDays += availableLeaves;
										}
										
									}*/
								}
								if(leavegrpType.getLeaveManagement() !=null && leavegrpType.getLeaveManagement().getEarnedLeave() != null && leavegrpType.getLeaveManagement().getEarnedLeave().equalsIgnoreCase("e")){
									List<EarnedLeavesDO> employeeDetailList = earnedLeavesService.retrieveByEmpId(user.getEmployee().getEmpId());
									if(employeeDetailList != null && employeeDetailList.size() > 0){
										availableLeaves = employeeDetailList.get(0).getAvailableLeaves() != null ? employeeDetailList.get(0).getAvailableLeaves() : 0;
										//availedLeaves = employeeDetailList.get(0).getAvailedLeaves()!=null?employeeDetailList.get(0).getAvailedLeaves():0;
										totalDays += availableLeaves;
									}
									
								}
								//totalDays += leavegrpType.getLeaveManagement().getNumberOfDays();
							}
						}
						leaveDetails.put(CommonConstants.TOTAl, totalDays);
						Date dateBeforeString = leaveRequest.getFromDate();
						 if(leaveRequest.getToDate() != null){
							 Calendar end = Calendar.getInstance();
							 Date dateAfterString = leaveRequest.getToDate();
							  end.setTime(dateAfterString);
							  Calendar start = Calendar.getInstance();
							  start.setTime(dateBeforeString);
							 for (Date date = start.getTime(); start.before(end) || start.equals(end); start.add(Calendar.DATE, 1), date = start.getTime()) {
								Calendar weekend = Calendar.getInstance();
								weekend.setTime(date);
								if (weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){
									String type =  String.valueOf(leaveRequest.getType());
									 if( type  != null && type.equalsIgnoreCase("h") ){
										 leaveCount =   (leaveCount+ halfCount);
										 numberOfLeaveTaken =  (numberOfLeaveTaken + halfCount);
									 }else{
										 if( type  != null && type.equalsIgnoreCase("f") ){
											 leaveCount = leaveCount+1;
											 numberOfLeaveTaken = numberOfLeaveTaken+1;
										 }
									 }
								}
							}
						 }else{
							  Calendar start = Calendar.getInstance();
							  start.setTime(dateBeforeString);
							  Date date = start.getTime();
								 Calendar weekend = Calendar.getInstance();
								 weekend.setTime(date);
									if (weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){
										String type =  String.valueOf(leaveRequest.getType());
										 if( type  != null && type.equalsIgnoreCase("h") ){
											 leaveCount =  (leaveCount+ halfCount);
											 numberOfLeaveTaken =  (numberOfLeaveTaken + halfCount);
										 }else{
											 if( type  != null && type.equalsIgnoreCase("f") ){
												 leaveCount = leaveCount+1;
												 numberOfLeaveTaken = numberOfLeaveTaken+1;
											 }
										 }
									}
						 }
						 
						leaveDetails.put(CommonConstants.NUMBEROFLEAVETAKEN, numberOfLeaveTaken);
						resultJSONArray.put(leaveDetails);
					}
				}
			}
				result.put(CommonConstants.LEAVES, resultJSONArray);
				result.put(CommonConstants.USEDLEAVES, leaveCount);
				result.put(CommonConstants.DAYS, noOfDays);
					/*result.put(CommonConstants.TYPE, leaveType.getLeaveManagement().getLeaveType().getLeaveTypeId());
					result.put(CommonConstants.TYPENAME, leaveType.getLeaveManagement().getLeaveType().getType());
					result.put(CommonConstants.DAYS, leaveType.getLeaveManagement().getNumberOfDays());*/
			
		
		}
		return result;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	// old
	/* public static JSONObject getUserDetailObject(UserDO user, LeaveManagementDO leaveDetail)throws JSONException, ParseException {
		JSONObject result = new JSONObject();
		List<EmployeeDO> empList = employeeService.retrieveEmpId(user.getEmployee().getEmpId().toString());
		if(empList != null && empList.size() > 0){
			if(empList.get(0).getMiddlename() != null){
				result.put(CommonConstants.NAME, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
			}else if(empList.get(0).getMiddlename() != null && empList.get(0).getLastname() == null )result.put(CommonConstants.NAME, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename()));
			else if(empList.get(0).getMiddlename() == null && empList.get(0).getLastname() == null )result.put(CommonConstants.NAME, String.valueOf(empList.get(0).getFirstname()));
			else{
				result.put(CommonConstants.NAME, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}
		}
		if(empList.get(0).getDesignation() != null){
			result.put(CommonConstants.DESIGNATION, String.valueOf(empList.get(0).getDesignation().getDesignationId()));
		}else{
			result.put(CommonConstants.DESIGNATION,"");
		}
		result.put(CommonConstants.EMPID, String.valueOf(empList.get(0).getEmpId()));
		List<LeaveRequestsDO> leaveRequestsList = leaveRequestsService.retrieveAvailableLeavesByEmpId(user.getEmployee().getEmpId(),leaveDetail.getFromDate(),leaveDetail.getToDate(),leaveDetail.getLeaveType().getLeaveTypeId());
		
		
		double leaveCount = 0;
		double halfCount =  0.5;
		
		JSONArray resultJSONArray = new JSONArray();
		for (LeaveRequestsDO leaveRequest : leaveRequestsList) {
			
			JSONObject leaveDetails = new JSONObject();
			leaveDetails.put(CommonConstants.FROM, String.valueOf(leaveRequest.getFromDate()));
			leaveDetails.put(CommonConstants.FROMDATEDISPLAY, String.valueOf(CommonUtil.convertDateToStringWithdatetime(leaveRequest.getFromDate())));
			leaveDetails.put(CommonConstants.TODATE, String.valueOf(leaveRequest.getToDate()));
			leaveDetails.put(CommonConstants.STATUS, String.valueOf(leaveRequest.getStatus()));
			leaveDetails.put(CommonConstants.DESCRIPTION, String.valueOf(leaveRequest.getDescription()));
			
			if(leaveRequest.getComments() != null){
				leaveDetails.put(CommonConstants.COMMENT, String.valueOf(leaveRequest.getComments()));
			}else{
				leaveDetails.put(CommonConstants.COMMENT, String.valueOf(""));
			}
			leaveDetails.put(CommonConstants.TYPE, String.valueOf(leaveRequest.getType()));
			leaveDetails.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(leaveRequest.getUpdatedon())));
			
			if(leaveRequest.getToDate() != null){
				leaveDetails.put(CommonConstants.TODATE, String.valueOf(leaveRequest.getToDate()));
				leaveDetails.put(CommonConstants.TODATEDISPLAY, String.valueOf(CommonUtil.convertDateToStringWithdatetime(leaveRequest.getToDate())));
			}else{
				leaveDetails.put(CommonConstants.TODATE, "");
				leaveDetails.put(CommonConstants.TODATEDISPLAY, "");
			}
			
			
			
			resultJSONArray.put(leaveDetails);
			if(String.valueOf(leaveRequest.getLeaveDay()).equals("Single")){
				leaveCount = leaveCount+1;
			}
			//if(String.valueOf(leaveRequest.getLeaveDay()).equals("Multiple")){
			//	SimpleDateFormat myFormat = new SimpleDateFormat("dd MM yyyy");
				 String dateBeforeString = leaveRequest.getFromDate();
				 String dateAfterString = leaveRequest.getToDate();
				 Date dateBefore = myFormat.parse(dateBeforeString);
			     Date dateAfter = myFormat.parse(dateAfterString);
			     long difference = dateAfter.getTime() - dateBefore.getTime();
			     int daysBetween = (int) (difference / (1000*60*60*24));
				
				Date dateBeforeString = leaveRequest.getFromDate();
				 if(leaveRequest.getToDate() != null){
					 Calendar end = Calendar.getInstance();
					 Date dateAfterString = leaveRequest.getToDate();
					  //Date dateAfter = myFormat.parse(dateAfterString);
					  end.setTime(dateAfterString);
					 // Date dateBefore = myFormat.parse(dateBeforeString);
					  Calendar start = Calendar.getInstance();
					  start.setTime(dateBeforeString);
					 for (Date date = start.getTime(); start.before(end) || start.equals(end); start.add(Calendar.DATE, 1), date = start.getTime()) {
						Calendar weekend = Calendar.getInstance();
						weekend.setTime(date);
						if (weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && 
							weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){
						     long difference = dateAfter.getTime() - dateBefore.getTime();
						     int daysBetween = (int) (difference / (1000*60*60*24));
						     //leaveCount = leaveCount+1;
							String leaveType =  String.valueOf(leaveRequest.getType());
							 if( leaveType  != null && leaveType.equalsIgnoreCase("h") ){
								 leaveCount =   (leaveCount+ halfCount);
							 }else{
								 if( leaveType  != null && leaveType.equalsIgnoreCase("f") ){
									 leaveCount = leaveCount+1;
								 }
							 }
						}
					}
				 }else{
					// Date dateBefore = myFormat.parse(dateBeforeString);
					  Calendar start = Calendar.getInstance();
					  start.setTime(dateBeforeString);
					  Date date = start.getTime();
					// for (Date date = start.getTime(); start.before(end) || start.equals(end); start.add(Calendar.DATE, 1), date = start.getTime()) {
						 Calendar weekend = Calendar.getInstance();
						 weekend.setTime(date);
							if (weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && 
								weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){
							     long difference = dateAfter.getTime() - dateBefore.getTime();
							     int daysBetween = (int) (difference / (1000*60*60*24));
							    // leaveCount = leaveCount+1;
								String leaveType =  String.valueOf(leaveRequest.getType());
								 if( leaveType  != null && leaveType.equalsIgnoreCase("h") ){
									 leaveCount =  (leaveCount+ halfCount);
								 }else{
									 if( leaveType  != null && leaveType.equalsIgnoreCase("f") ){
										 leaveCount = leaveCount+1;
									 }
								 }
							}
					//}
				 }
			     
			//}
		}
		for (LeaveRequestsDO leaveRequest : leaveRequestsList) {
			if(String.valueOf(leaveRequest.getLeaveDay()).equals("Single")){
				leaveCount = leaveCount+1;
			}
			//if(String.valueOf(leaveRequest.getLeaveDay()).equals("Multiple")){
				SimpleDateFormat myFormat = new SimpleDateFormat("dd MM yyyy");
				// old
				 String dateBeforeString = leaveRequest.getFromDate();
				 String dateAfterString = leaveRequest.getToDate();
				 Date dateBefore = myFormat.parse(dateBeforeString);
			     Date dateAfter = myFormat.parse(dateAfterString);
			     long difference = dateAfter.getTime() - dateBefore.getTime();
			     int daysBetween = (int) (difference / (1000*60*60*24));
			     leaveCount = leaveCount + daysBetween + 1;
				// old end
				String dateBeforeString = leaveRequest.getFromDate();
				 String dateAfterString = leaveRequest.getToDate();
				 Date dateBefore = myFormat.parse(dateBeforeString);
			     Date dateAfter = myFormat.parse(dateAfterString);
				 Calendar start = Calendar.getInstance();
				 start.setTime(dateBefore);
				 Calendar end = Calendar.getInstance();
				 end.setTime(dateAfter);
				
				 for (Date date = start.getTime(); start.before(end) || start.equals(end); start.add(Calendar.DATE, 1), date = start.getTime()) {
					Calendar weekend = Calendar.getInstance();
					weekend.setTime(date);
					if (weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && 
						weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){
					     long difference = dateAfter.getTime() - dateBefore.getTime();
					     int daysBetween = (int) (difference / (1000*60*60*24));
					     leaveCount = leaveCount+1;
					}
				}
			//}
		}
		result.put(CommonConstants.LEAVES, resultJSONArray);
		result.put(CommonConstants.USEDLEAVES, leaveCount);
		result.put(CommonConstants.TYPE, leaveDetail.getLeaveType().getLeaveTypeId());
		result.put(CommonConstants.TYPENAME, leaveDetail.getLeaveType().getType());
		result.put(CommonConstants.DAYS, leaveDetail.getNumberOfDays());
		return result;
	}
	*/
	
	
	
	
	
	
	
	
	public static JSONObject getLeaveRequestsDetailObjectForFilter(UserDO user, Date startDate, Date endDate, LeaveGroupDO leavegrp, Long type)throws JSONException, ParseException {
		JSONObject result = new JSONObject();
	
		if(user != null && user.getEmployee() != null){
			if(user.getEmployee().getMiddlename() != null) result.put(CommonConstants.NAME, String.valueOf(user.getEmployee().getFirstname() + " " +user.getEmployee().getMiddlename() + " " + user.getEmployee().getLastname()));
			else if(user.getEmployee().getMiddlename() != null && user.getEmployee().getLastname() == null )result.put(CommonConstants.NAME, String.valueOf(user.getEmployee().getFirstname() + " " +user.getEmployee().getMiddlename()));
			else if(user.getEmployee().getMiddlename() == null && user.getEmployee().getLastname() == null )result.put(CommonConstants.NAME, String.valueOf(user.getEmployee().getFirstname()));
			else result.put(CommonConstants.NAME, String.valueOf(user.getEmployee().getFirstname() + " " + user.getEmployee().getLastname()));
			
			result.put(CommonConstants.EMPID, String.valueOf(user.getEmployee().getEmpId()));
			List<LeaveRequestsDO> leaveRequestsList = null;
			if(startDate != null && endDate == null ){
				if(type != null){
					leaveRequestsList = leaveRequestsService.retrieveLeavesByFromDateWithType(user.getEmployee().getEmpId(),startDate,type);
				}else{
					leaveRequestsList = leaveRequestsService.retrieveLeavesByFromDate(user.getEmployee().getEmpId(),startDate);
				}
				 
			}
			if(startDate != null && endDate != null ){
				// leaveRequestsList = leaveRequestsService.retrieveLeavesByFromToDate(user.getEmployee().getEmpId(),startDate,endDate);
				if(type != null){
					leaveRequestsList = leaveRequestsService.retrieveLeavesByFromToDateType(user.getEmployee().getEmpId(),startDate,endDate,type);
				}else{
					leaveRequestsList = leaveRequestsService.retrieveLeavesByFromToDate(user.getEmployee().getEmpId(),startDate,endDate);
				}
			}
			
			if(startDate == null && endDate == null ){
				if(type != null){
					leaveRequestsList = leaveRequestsService.retriveByEmpIdStatusType(user.getEmployee().getEmpId(),type);
				}else{
					leaveRequestsList = leaveRequestsService.retriveByEmpId(user.getEmployee().getEmpId());
				}
			}
					
			double leaveCount = 0.0;
			double halfCount =  0.5;
			double numberOfLeaveTaken =  0.0;
			
			JSONArray resultJSONArray = new JSONArray();
			if(leaveRequestsList != null && leaveRequestsList.size() > 0){
				
				for (LeaveRequestsDO leaveRequest : leaveRequestsList) {
					if(!leaveRequest.getType().equalsIgnoreCase("p")  && leaveRequest.getLeaveManagement() != null){
						JSONObject leaveDetails = new JSONObject();
						leaveDetails.put(CommonConstants.FROM, String.valueOf(leaveRequest.getFromDate()));
						leaveDetails.put(CommonConstants.FROMDATEDISPLAY, String.valueOf(CommonUtil.convertDateToStringApp(leaveRequest.getFromDate())));
						leaveDetails.put(CommonConstants.TODATE, String.valueOf(leaveRequest.getToDate()));
						leaveDetails.put(CommonConstants.STATUS, String.valueOf(leaveRequest.getStatus()));
						leaveDetails.put(CommonConstants.DESCRIPTION, leaveRequest.getDescription()!=null?String.valueOf(leaveRequest.getDescription()):"");
						leaveDetails.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(leaveRequest.getUpdatedon())));
						if(leaveRequest.getComments() != null){
							leaveDetails.put(CommonConstants.COMMENT, String.valueOf(leaveRequest.getComments()));
						}else{
							leaveDetails.put(CommonConstants.COMMENT, String.valueOf(""));
						}
						if(leaveRequest.getLeaveGroup() != null){
							leaveDetails.put(CommonConstants.LEAVEGROUP_ID, String.valueOf(leaveRequest.getLeaveGroup().getLeaveGroupId()));
							leaveDetails.put(CommonConstants.LEAVEGROUP, String.valueOf(leaveRequest.getLeaveGroup().getLeaveGroup()));
						}else{
							leaveDetails.put(CommonConstants.LEAVEGROUP_ID, "");
							leaveDetails.put(CommonConstants.LEAVEGROUP, "");
						}
						Double totalDays = 0.0;
						numberOfLeaveTaken = 0;
						List<LeaveGroupSettingsDO> groupList = leaveGroupSettingsService.retrieveByGroupID(leavegrp.getLeaveGroupId());
						if(groupList != null && groupList.size() > 0){
							for (LeaveGroupSettingsDO leavegrpType : groupList) {
								if(leavegrpType.getLeaveManagement() != null &&leavegrpType.getLeaveManagement().getEarnedLeave() != null && leavegrpType.getLeaveManagement().getEarnedLeave().equalsIgnoreCase("e")){
									//
								}else{
									totalDays += leavegrpType.getLeaveManagement().getNumberOfDays();
								}
								
							}
						}
						leaveDetails.put(CommonConstants.TOTAl, totalDays);
						/*leaveDetails.put(CommonConstants.TYPENAME, String.valueOf(leaveRequest.getLeaveType().getType()));*/
						leaveDetails.put(CommonConstants.TYPENAME, String.valueOf(leaveRequest.getLeaveManagement().getLeaveType()));
						leaveDetails.put(CommonConstants.TYPE, String.valueOf(leaveRequest.getType()));
						if(leaveRequest.getToDate() != null){
							leaveDetails.put(CommonConstants.TODATE, String.valueOf(leaveRequest.getToDate()));
							leaveDetails.put(CommonConstants.TODATEDISPLAY, String.valueOf(CommonUtil.convertDateToStringApp(leaveRequest.getToDate())));
						}else{
							leaveDetails.put(CommonConstants.TODATE, "");
							leaveDetails.put(CommonConstants.TODATEDISPLAY, "");
						}
						Date dateBeforeString = leaveRequest.getFromDate();
						 if(leaveRequest.getToDate() != null){
							 Calendar end = Calendar.getInstance();
							 Date dateAfterString = leaveRequest.getToDate();
							  end.setTime(dateAfterString);
							  Calendar start = Calendar.getInstance();
							  start.setTime(dateBeforeString);
							 for (Date date = start.getTime(); start.before(end) || start.equals(end); start.add(Calendar.DATE, 1), date = start.getTime()) {
								Calendar weekend = Calendar.getInstance();
								weekend.setTime(date);
								/*if (weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){
									String leaveType =  String.valueOf(leaveRequest.getType());
									 if( leaveType  != null && leaveType.equalsIgnoreCase("h") ){
										 leaveCount =   (leaveCount+ halfCount);
										 numberOfLeaveTaken = numberOfLeaveTaken + halfCount;
									 }else{
										 if( leaveType  != null && leaveType.equalsIgnoreCase("f") ){
											 leaveCount = leaveCount+1;
											 numberOfLeaveTaken = numberOfLeaveTaken+1;
											 
										 }
									 }
								}*/
								if(leaveRequest.getLeaveManagement().getSaturday() != null && leaveRequest.getLeaveManagement().getSaturday().equalsIgnoreCase("y") && 
										leaveRequest.getLeaveManagement().getSunday() != null && leaveRequest.getLeaveManagement().getSunday().equalsIgnoreCase("y")){
									String leaveType =  String.valueOf(leaveRequest.getType());
									if( leaveType  != null && leaveType.equalsIgnoreCase("h") ){
										 leaveCount =   (leaveCount+ halfCount);
										 numberOfLeaveTaken = numberOfLeaveTaken + halfCount;
									 }else{
										 if( leaveType  != null && leaveType.equalsIgnoreCase("f") ){
											 leaveCount = leaveCount+1;
											 numberOfLeaveTaken = numberOfLeaveTaken+1;
											 
										 }
									 }
								}else if(leaveRequest.getLeaveManagement().getSaturday() != null && leaveRequest.getLeaveManagement().getSaturday().equalsIgnoreCase("n") &&
										leaveRequest.getLeaveManagement().getSunday() != null && leaveRequest.getLeaveManagement().getSunday().equalsIgnoreCase("n")){
									if (weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && 
										weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){
										String leaveType =  String.valueOf(leaveRequest.getType());
										if( leaveType  != null && leaveType.equalsIgnoreCase("h") ){
											 leaveCount =   (leaveCount+ halfCount);
											 numberOfLeaveTaken = numberOfLeaveTaken + halfCount;
										 }else{
											 if( leaveType  != null && leaveType.equalsIgnoreCase("f") ){
												 leaveCount = leaveCount+1;
												 numberOfLeaveTaken = numberOfLeaveTaken+1;
												 
											 }
										 }
									}
								}else if(leaveRequest.getLeaveManagement().getSaturday() != null && leaveRequest.getLeaveManagement().getSaturday().equalsIgnoreCase("y") && 
										leaveRequest.getLeaveManagement().getSunday() != null && leaveRequest.getLeaveManagement().getSunday().equalsIgnoreCase("n")){
									if (weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){
										String leaveType =  String.valueOf(leaveRequest.getType());
										if( leaveType  != null && leaveType.equalsIgnoreCase("h") ){
											 leaveCount =   (leaveCount+ halfCount);
											 numberOfLeaveTaken = numberOfLeaveTaken + halfCount;
										 }else{
											 if( leaveType  != null && leaveType.equalsIgnoreCase("f") ){
												 leaveCount = leaveCount+1;
												 numberOfLeaveTaken = numberOfLeaveTaken+1;
												 
											 }
										 }
									}
								}else if(leaveRequest.getLeaveManagement().getSaturday() != null && leaveRequest.getLeaveManagement().getSaturday().equalsIgnoreCase("n") &&
										leaveRequest.getLeaveManagement().getSunday() != null && leaveRequest.getLeaveManagement().getSunday().equalsIgnoreCase("y")){
									if (/*weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY &&*/ 
										weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY){
										String leaveType =  String.valueOf(leaveRequest.getType());
										if( leaveType  != null && leaveType.equalsIgnoreCase("h") ){
											 leaveCount =   (leaveCount+ halfCount);
											 numberOfLeaveTaken = numberOfLeaveTaken + halfCount;
										 }else{
											 if( leaveType  != null && leaveType.equalsIgnoreCase("f") ){
												 leaveCount = leaveCount+1;
												 numberOfLeaveTaken = numberOfLeaveTaken+1;
												 
											 }
										 }
									}
								}
							}
						 }else{
							  Calendar start = Calendar.getInstance();
							  start.setTime(dateBeforeString);
							  Date date = start.getTime();
								 Calendar weekend = Calendar.getInstance();
								 weekend.setTime(date);
									/*if (weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){
										String leaveType =  String.valueOf(leaveRequest.getType());
										 if( leaveType  != null && leaveType.equalsIgnoreCase("h") ){
											 leaveCount =  (leaveCount+ halfCount);
											 numberOfLeaveTaken = numberOfLeaveTaken+ halfCount;
										 }else{
											 if( leaveType  != null && leaveType.equalsIgnoreCase("f") ){
												 leaveCount = leaveCount+1;
												 numberOfLeaveTaken = numberOfLeaveTaken+1;
											 }
										 }
									}*/
								 if(leaveRequest.getLeaveManagement().getSaturday() != null && leaveRequest.getLeaveManagement().getSaturday().equalsIgnoreCase("y") && 
											leaveRequest.getLeaveManagement().getSunday() != null && leaveRequest.getLeaveManagement().getSunday().equalsIgnoreCase("y")){
										String leaveType =  String.valueOf(leaveRequest.getType());
										if( leaveType  != null && leaveType.equalsIgnoreCase("h") ){
											 leaveCount =   (leaveCount+ halfCount);
											 numberOfLeaveTaken = numberOfLeaveTaken + halfCount;
										 }else{
											 if( leaveType  != null && leaveType.equalsIgnoreCase("f") ){
												 leaveCount = leaveCount+1;
												 numberOfLeaveTaken = numberOfLeaveTaken+1;
												 
											 }
										 }
									}else if(leaveRequest.getLeaveManagement().getSaturday() != null && leaveRequest.getLeaveManagement().getSaturday().equalsIgnoreCase("n") &&
											leaveRequest.getLeaveManagement().getSunday() != null && leaveRequest.getLeaveManagement().getSunday().equalsIgnoreCase("n")){
										if (weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && 
											weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){
											String leaveType =  String.valueOf(leaveRequest.getType());
											if( leaveType  != null && leaveType.equalsIgnoreCase("h") ){
												 leaveCount =   (leaveCount+ halfCount);
												 numberOfLeaveTaken = numberOfLeaveTaken + halfCount;
											 }else{
												 if( leaveType  != null && leaveType.equalsIgnoreCase("f") ){
													 leaveCount = leaveCount+1;
													 numberOfLeaveTaken = numberOfLeaveTaken+1;
													 
												 }
											 }
										}
									}else if(leaveRequest.getLeaveManagement().getSaturday() != null && leaveRequest.getLeaveManagement().getSaturday().equalsIgnoreCase("y") && 
											leaveRequest.getLeaveManagement().getSunday() != null && leaveRequest.getLeaveManagement().getSunday().equalsIgnoreCase("n")){
										if (weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){
											String leaveType =  String.valueOf(leaveRequest.getType());
											if( leaveType  != null && leaveType.equalsIgnoreCase("h") ){
												 leaveCount =   (leaveCount+ halfCount);
												 numberOfLeaveTaken = numberOfLeaveTaken + halfCount;
											 }else{
												 if( leaveType  != null && leaveType.equalsIgnoreCase("f") ){
													 leaveCount = leaveCount+1;
													 numberOfLeaveTaken = numberOfLeaveTaken+1;
													 
												 }
											 }
										}
									}else if(leaveRequest.getLeaveManagement().getSaturday() != null && leaveRequest.getLeaveManagement().getSaturday().equalsIgnoreCase("n") &&
											leaveRequest.getLeaveManagement().getSunday() != null && leaveRequest.getLeaveManagement().getSunday().equalsIgnoreCase("y")){
										if (/*weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY &&*/ 
											weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY){
											String leaveType =  String.valueOf(leaveRequest.getType());
											if( leaveType  != null && leaveType.equalsIgnoreCase("h") ){
												 leaveCount =   (leaveCount+ halfCount);
												 numberOfLeaveTaken = numberOfLeaveTaken + halfCount;
											 }else{
												 if( leaveType  != null && leaveType.equalsIgnoreCase("f") ){
													 leaveCount = leaveCount+1;
													 numberOfLeaveTaken = numberOfLeaveTaken+1;
													 
												 }
											 }
										}
									}
						 }
						 leaveDetails.put(CommonConstants.NUMBEROFLEAVETAKEN, numberOfLeaveTaken);
							resultJSONArray.put(leaveDetails);
					}
				}
			}
				result.put(CommonConstants.USEDLEAVES, leaveCount);
				//result.put(CommonConstants.TYPE, String.valueOf(leaveGrptype.getLeaveManagement().getLeaveType().getLeaveTypeId()));
				//result.put(CommonConstants.TYPENAME, String.valueOf(leaveGrptype.getLeaveManagement().getLeaveType().getType()));
				result.put(CommonConstants.LEAVES, resultJSONArray);
				long noOfDays = 0;
				List<LeaveGroupSettingsDO> leaveGroupList = leaveGroupSettingsService.retrieveByGroupID(leavegrp.getLeaveGroupId());
				if(leaveGroupList != null && leaveGroupList.size() > 0){
					for (LeaveGroupSettingsDO leaveType : leaveGroupList) {
						if(leaveType.getLeaveManagement() != null && leaveType.getLeaveManagement().getNumberOfDays() != null){
							noOfDays += leaveType.getLeaveManagement().getNumberOfDays();
						}
					}
				}
				result.put(CommonConstants.DAYS, noOfDays);
				
				/*List<LeaveManagementDO> leavemangeList = leaveManageService.retrieveCountByYear(startDate,endDate,leaveGrptype.getLeaveManagement().getLeaveType().getLeaveTypeId());
				int days = 0;
				if(leavemangeList != null && leavemangeList.size() > 0  && leavemangeList.get(0) != null){
					
					result.put(CommonConstants.DAYS, leavemangeList.get(0));
				}else{
					result.put(CommonConstants.DAYS, days);
				}*/
			
		}
		return result;
	}
	
	//old
	/*
	public static JSONObject getLeaveRequestsDetailObjectForFilter(UserDO user, Date startDate, Date endDate, LeaveTypeDO leavetype)throws JSONException, ParseException {
		JSONObject result = new JSONObject();
	
		if(user != null && user.getEmployee() != null){
			if(user.getEmployee().getMiddlename() != null) result.put(CommonConstants.NAME, String.valueOf(user.getEmployee().getFirstname() + " " +user.getEmployee().getMiddlename() + " " + user.getEmployee().getLastname()));
			else if(user.getEmployee().getMiddlename() != null && user.getEmployee().getLastname() == null )result.put(CommonConstants.NAME, String.valueOf(user.getEmployee().getFirstname() + " " +user.getEmployee().getMiddlename()));
			else if(user.getEmployee().getMiddlename() == null && user.getEmployee().getLastname() == null )result.put(CommonConstants.NAME, String.valueOf(user.getEmployee().getFirstname()));
			else result.put(CommonConstants.NAME, String.valueOf(user.getEmployee().getFirstname() + " " + user.getEmployee().getLastname()));
			
			result.put(CommonConstants.EMPID, String.valueOf(user.getEmployee().getEmpId()));
			if(leavetype != null){
					List<LeaveRequestsDO> leaveRequestsList = leaveRequestsService.retrieveLeavesByYear(user.getEmployee().getEmpId(),startDate,endDate,leavetype.getLeaveTypeId());
					double leaveCount = 0;
					double halfCount =  0.5;
					
					JSONArray resultJSONArray = new JSONArray();
					for (LeaveRequestsDO leaveRequest : leaveRequestsList) {
						JSONObject leaveDetails = new JSONObject();
						leaveDetails.put(CommonConstants.FROM, String.valueOf(leaveRequest.getFromDate()));
						leaveDetails.put(CommonConstants.FROMDATEDISPLAY, String.valueOf(CommonUtil.convertDateToStringWithdatetime(leaveRequest.getFromDate())));
						leaveDetails.put(CommonConstants.TODATE, String.valueOf(leaveRequest.getToDate()));
						leaveDetails.put(CommonConstants.STATUS, String.valueOf(leaveRequest.getStatus()));
						leaveDetails.put(CommonConstants.DESCRIPTION, String.valueOf(leaveRequest.getDescription()));
						leaveDetails.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(leaveRequest.getUpdatedon())));
						if(leaveRequest.getComments() != null){
							leaveDetails.put(CommonConstants.COMMENT, String.valueOf(leaveRequest.getComments()));
						}else{
							leaveDetails.put(CommonConstants.COMMENT, String.valueOf(""));
						}
						
						leaveDetails.put(CommonConstants.TYPE, String.valueOf(leaveRequest.getType()));
						if(leaveRequest.getToDate() != null){
							leaveDetails.put(CommonConstants.TODATE, String.valueOf(leaveRequest.getToDate()));
							leaveDetails.put(CommonConstants.TODATEDISPLAY, String.valueOf(CommonUtil.convertDateToStringWithdatetime(leaveRequest.getToDate())));
						}else{
							leaveDetails.put(CommonConstants.TODATE, "");
							leaveDetails.put(CommonConstants.TODATEDISPLAY, "");
						}
						Date dateBeforeString = leaveRequest.getFromDate();
						 if(leaveRequest.getToDate() != null){
							 Calendar end = Calendar.getInstance();
							 Date dateAfterString = leaveRequest.getToDate();
							  end.setTime(dateAfterString);
							  Calendar start = Calendar.getInstance();
							  start.setTime(dateBeforeString);
							 for (Date date = start.getTime(); start.before(end) || start.equals(end); start.add(Calendar.DATE, 1), date = start.getTime()) {
								Calendar weekend = Calendar.getInstance();
								weekend.setTime(date);
								if (weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && 
									weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){
									String leaveType =  String.valueOf(leaveRequest.getType());
									 if( leaveType  != null && leaveType.equalsIgnoreCase("h") ){
										 leaveCount =   (leaveCount+ halfCount);
									 }else{
										 if( leaveType  != null && leaveType.equalsIgnoreCase("f") ){
											 leaveCount = leaveCount+1;
										 }
									 }
								}
							}
						 }else{
							  Calendar start = Calendar.getInstance();
							  start.setTime(dateBeforeString);
							  Date date = start.getTime();
								 Calendar weekend = Calendar.getInstance();
								 weekend.setTime(date);
									if (weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && 
										weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){
										String leaveType =  String.valueOf(leaveRequest.getType());
										 if( leaveType  != null && leaveType.equalsIgnoreCase("h") ){
											 leaveCount =  (leaveCount+ halfCount);
										 }else{
											 if( leaveType  != null && leaveType.equalsIgnoreCase("f") ){
												 leaveCount = leaveCount+1;
											 }
										 }
									}
						 }
							resultJSONArray.put(leaveDetails);
				}
					result.put(CommonConstants.USEDLEAVES, leaveCount);
					result.put(CommonConstants.TYPE, String.valueOf(leavetype.getLeaveTypeId()));
					result.put(CommonConstants.TYPENAME, String.valueOf(leavetype.getType()));
					result.put(CommonConstants.LEAVES, resultJSONArray);
					List<LeaveManagementDO> leavemangeList = leaveManageService.retrieveCountByYear(startDate,endDate,leavetype.getLeaveTypeId());
					int days = 0;
					if(leavemangeList != null && leavemangeList.size() > 0  && leavemangeList.get(0) != null){
						
						result.put(CommonConstants.DAYS, leavemangeList.get(0));
					}else{
						result.put(CommonConstants.DAYS, days);
					}
				}
			}
		return result;
	}*/
	
	public static JSONObject getEmpLeaveRequestsDetailObjectForFilter(String  empId, Date startDate, Date endDate, LeaveGroupDO leavegrp, Long type)throws JSONException, ParseException {
		JSONObject result = new JSONObject();
	
		if(empId != null){
			List<EmployeeDO> empList = employeeService.retrieveEmpId(empId);
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null){
					result.put(CommonConstants.NAME, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				}else if(empList.get(0).getMiddlename() != null && empList.get(0).getLastname() == null )result.put(CommonConstants.NAME, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename()));
				else if(empList.get(0).getMiddlename() == null && empList.get(0).getLastname() == null )result.put(CommonConstants.NAME, String.valueOf(empList.get(0).getFirstname()));
				else{
					result.put(CommonConstants.NAME, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
				}
			}
			result.put(CommonConstants.EMPID, String.valueOf(empId));
			//if(leaveGroupList != null){
				//for(LeaveTypeDO leavetype : leaveTypeList){
				List<LeaveRequestsDO> leaveRequestsList  = null ;
				if(startDate != null && endDate == null){
					
						if(type != null){
							leaveRequestsList = leaveRequestsService.retrieveLeavesByFromDateWithType(empId,startDate,type);
						}else{
							leaveRequestsList = leaveRequestsService.retrieveLeavesByFromDate(empId,startDate);
						}
					//leaveRequestsList = leaveRequestsService.retrieveLeavesByFromDate(empId,startDate);
				}else if(startDate != null && endDate != null){
					if(type != null){
						leaveRequestsList = leaveRequestsService.retrieveLeavesByFromToDateType(empId,startDate,endDate,type);
					}else{
						leaveRequestsList = leaveRequestsService.retrieveLeavesByFromToDate(empId,startDate,endDate);
					}
					 //leaveRequestsList = leaveRequestsService.retrieveLeavesByFromToDate(empId,startDate,endDate);
				}else{
					if(type != null){
						leaveRequestsList = leaveRequestsService.retriveByEmpIdStatusType(empId,type);
					}else{
						leaveRequestsList = leaveRequestsService.retriveByEmpIdStatus(empId);
					}
					//leaveRequestsList = leaveRequestsService.retriveByEmpIdStatus(empId);
				}
				double leaveCount = 0.0;
				double halfCount =  0.5;
				
				JSONArray resultJSONArray = new JSONArray();
				if(leaveRequestsList != null && leaveRequestsList.size() > 0){
					for (LeaveRequestsDO leaveRequest : leaveRequestsList) {
						if(!leaveRequest.getType().equalsIgnoreCase("p") && leaveRequest.getLeaveManagement() != null){
							JSONObject leaveDetails = new JSONObject();
							leaveDetails.put(CommonConstants.FROM, String.valueOf(leaveRequest.getFromDate()));
							leaveDetails.put(CommonConstants.FROMDATEDISPLAY, String.valueOf(CommonUtil.convertDateToStringApp(leaveRequest.getFromDate())));
							leaveDetails.put(CommonConstants.TODATE, String.valueOf(leaveRequest.getToDate()));
							if(leaveRequest.getToDate() != null){
								leaveDetails.put(CommonConstants.TODATEDISPLAY, String.valueOf(CommonUtil.convertDateToStringApp(leaveRequest.getToDate())));
							}
							leaveDetails.put(CommonConstants.STATUS, String.valueOf(leaveRequest.getStatus()));
							leaveDetails.put(CommonConstants.DESCRIPTION, leaveRequest.getDescription()!=null?String.valueOf(leaveRequest.getDescription()):"");
							if(leaveRequest.getComments() != null){
								leaveDetails.put(CommonConstants.COMMENT, String.valueOf(leaveRequest.getComments()));
							}else{
								leaveDetails.put(CommonConstants.COMMENT, String.valueOf(""));
							}
							
							if(leaveRequest.getLeaveGroup() != null){
								leaveDetails.put(CommonConstants.LEAVEGROUP_ID, String.valueOf(leaveRequest.getLeaveGroup().getLeaveGroupId()));
								leaveDetails.put(CommonConstants.LEAVEGROUP, String.valueOf(leaveRequest.getLeaveGroup().getLeaveGroup()));
							}else{
								leaveDetails.put(CommonConstants.LEAVEGROUP_ID, "");
								leaveDetails.put(CommonConstants.LEAVEGROUP, "");
							}
							double totalDays = 0.0;
							double numberOfLeaveTaken = 0.0;
							List<LeaveGroupSettingsDO> groupList = leaveGroupSettingsService.retrieveByGroupID(leavegrp.getLeaveGroupId());
							if(groupList != null && groupList.size() > 0){
								for (LeaveGroupSettingsDO leavegrpType : groupList) {
									if(leavegrpType.getLeaveManagement() != null && leavegrpType.getLeaveManagement().getNumberOfDays() != null){
										totalDays += leavegrpType.getLeaveManagement().getNumberOfDays();
									}
								}
							}
							leaveDetails.put(CommonConstants.TOTAl, totalDays);
							leaveDetails.put(CommonConstants.TYPE, String.valueOf(leaveRequest.getType()));
							leaveDetails.put(CommonConstants.TYPENAME, String.valueOf(leaveRequest.getLeaveManagement().getLeaveType()));
							/*leaveDetails.put(CommonConstants.TYPENAME, String.valueOf(leaveRequest.getLeaveType().getType()));*/
							Date dateBeforeString = leaveRequest.getFromDate();
							 if(leaveRequest.getToDate() != null){
								 Calendar end = Calendar.getInstance();
								 Date dateAfterString = leaveRequest.getToDate();
								  end.setTime(dateAfterString);
								  Calendar start = Calendar.getInstance();
								  start.setTime(dateBeforeString);
								 for (Date date = start.getTime(); start.before(end) || start.equals(end); start.add(Calendar.DATE, 1), date = start.getTime()) {
									Calendar weekend = Calendar.getInstance();
									weekend.setTime(date);
									if(leaveRequest.getLeaveManagement().getSaturday() != null && leaveRequest.getLeaveManagement().getSaturday().equalsIgnoreCase("y") && 
											leaveRequest.getLeaveManagement().getSunday() != null && leaveRequest.getLeaveManagement().getSunday().equalsIgnoreCase("y")){
										String leaveType =  String.valueOf(leaveRequest.getType());
										 if( leaveType  != null && leaveType.equalsIgnoreCase("h") ){
											 leaveCount =   (leaveCount+ halfCount);
											 numberOfLeaveTaken = numberOfLeaveTaken +halfCount;
										 }else{
											 if( leaveType  != null && leaveType.equalsIgnoreCase("f") ){
												 leaveCount = leaveCount+1;
												 numberOfLeaveTaken = numberOfLeaveTaken+1;
											 }
										 }
									}else if(leaveRequest.getLeaveManagement().getSaturday() != null && leaveRequest.getLeaveManagement().getSaturday().equalsIgnoreCase("n") &&
											leaveRequest.getLeaveManagement().getSunday() != null && leaveRequest.getLeaveManagement().getSunday().equalsIgnoreCase("n")){
										if (weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && 
											weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){
											String leaveType =  String.valueOf(leaveRequest.getType());
											 if( leaveType  != null && leaveType.equalsIgnoreCase("h") ){
												 leaveCount =   (leaveCount+ halfCount);
												 numberOfLeaveTaken = numberOfLeaveTaken +halfCount;
											 }else{
												 if( leaveType  != null && leaveType.equalsIgnoreCase("f") ){
													 leaveCount = leaveCount+1;
													 numberOfLeaveTaken = numberOfLeaveTaken+1;
												 }
											 }
										}
									}else if(leaveRequest.getLeaveManagement().getSaturday() != null && leaveRequest.getLeaveManagement().getSaturday().equalsIgnoreCase("y") && 
											leaveRequest.getLeaveManagement().getSunday() != null && leaveRequest.getLeaveManagement().getSunday().equalsIgnoreCase("n")){
										if (weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){
											String leaveType =  String.valueOf(leaveRequest.getType());
											 if( leaveType  != null && leaveType.equalsIgnoreCase("h") ){
												 leaveCount =   (leaveCount+ halfCount);
												 numberOfLeaveTaken = numberOfLeaveTaken +halfCount;
											 }else{
												 if( leaveType  != null && leaveType.equalsIgnoreCase("f") ){
													 leaveCount = leaveCount+1;
													 numberOfLeaveTaken = numberOfLeaveTaken+1;
												 }
											 }
										}
									}else if(leaveRequest.getLeaveManagement().getSaturday() != null && leaveRequest.getLeaveManagement().getSaturday().equalsIgnoreCase("n") &&
											leaveRequest.getLeaveManagement().getSunday() != null && leaveRequest.getLeaveManagement().getSunday().equalsIgnoreCase("y")){
										if (/*weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY &&*/ 
											weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY){
											String leaveType =  String.valueOf(leaveRequest.getType());
											 if( leaveType  != null && leaveType.equalsIgnoreCase("h") ){
												 leaveCount =   (leaveCount+ halfCount);
												 numberOfLeaveTaken = numberOfLeaveTaken +halfCount;
											 }else{
												 if( leaveType  != null && leaveType.equalsIgnoreCase("f") ){
													 leaveCount = leaveCount+1;
													 numberOfLeaveTaken = numberOfLeaveTaken+1;
												 }
											 }
										}
									}
									
								}
							 }else{
								  Calendar start = Calendar.getInstance();
								  start.setTime(dateBeforeString);
								  Date date = start.getTime();
									 Calendar weekend = Calendar.getInstance();
									 weekend.setTime(date);
										/*if ( 
											weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){
											String leaveType =  String.valueOf(leaveRequest.getType());
											 if( leaveType  != null && leaveType.equalsIgnoreCase("h") ){
												 leaveCount =  (leaveCount+ halfCount);
												 numberOfLeaveTaken = numberOfLeaveTaken+halfCount;
											 }else{
												 if( leaveType  != null && leaveType.equalsIgnoreCase("f") ){
													 leaveCount = leaveCount+1;
													 numberOfLeaveTaken = numberOfLeaveTaken+1;
												 }
											 }
										}*/if(leaveRequest.getLeaveManagement().getSaturday() != null && leaveRequest.getLeaveManagement().getSaturday().equalsIgnoreCase("y") && 
												leaveRequest.getLeaveManagement().getSunday() != null && leaveRequest.getLeaveManagement().getSunday().equalsIgnoreCase("y")){
											String leaveType =  String.valueOf(leaveRequest.getType());
											 if( leaveType  != null && leaveType.equalsIgnoreCase("h") ){
												 leaveCount =   (leaveCount+ halfCount);
												 numberOfLeaveTaken = numberOfLeaveTaken +halfCount;
											 }else{
												 if( leaveType  != null && leaveType.equalsIgnoreCase("f") ){
													 leaveCount = leaveCount+1;
													 numberOfLeaveTaken = numberOfLeaveTaken+1;
												 }
											 }
										}else if(leaveRequest.getLeaveManagement().getSaturday() != null && leaveRequest.getLeaveManagement().getSaturday().equalsIgnoreCase("n") &&
												leaveRequest.getLeaveManagement().getSunday() != null && leaveRequest.getLeaveManagement().getSunday().equalsIgnoreCase("n")){
											if (weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && 
												weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){
												String leaveType =  String.valueOf(leaveRequest.getType());
												 if( leaveType  != null && leaveType.equalsIgnoreCase("h") ){
													 leaveCount =   (leaveCount+ halfCount);
													 numberOfLeaveTaken = numberOfLeaveTaken +halfCount;
												 }else{
													 if( leaveType  != null && leaveType.equalsIgnoreCase("f") ){
														 leaveCount = leaveCount+1;
														 numberOfLeaveTaken = numberOfLeaveTaken+1;
													 }
												 }
											}
										}else if(leaveRequest.getLeaveManagement().getSaturday() != null && leaveRequest.getLeaveManagement().getSaturday().equalsIgnoreCase("y") && 
												leaveRequest.getLeaveManagement().getSunday() != null && leaveRequest.getLeaveManagement().getSunday().equalsIgnoreCase("n")){
											if (weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){
												String leaveType =  String.valueOf(leaveRequest.getType());
												 if( leaveType  != null && leaveType.equalsIgnoreCase("h") ){
													 leaveCount =   (leaveCount+ halfCount);
													 numberOfLeaveTaken = numberOfLeaveTaken +halfCount;
												 }else{
													 if( leaveType  != null && leaveType.equalsIgnoreCase("f") ){
														 leaveCount = leaveCount+1;
														 numberOfLeaveTaken = numberOfLeaveTaken+1;
													 }
												 }
											}
										}else if(leaveRequest.getLeaveManagement().getSaturday() != null && leaveRequest.getLeaveManagement().getSaturday().equalsIgnoreCase("n") &&
												leaveRequest.getLeaveManagement().getSunday() != null && leaveRequest.getLeaveManagement().getSunday().equalsIgnoreCase("y")){
											if (/*weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY &&*/ 
												weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY){
												String leaveType =  String.valueOf(leaveRequest.getType());
												 if( leaveType  != null && leaveType.equalsIgnoreCase("h") ){
													 leaveCount =   (leaveCount+ halfCount);
													 numberOfLeaveTaken = numberOfLeaveTaken +halfCount;
												 }else{
													 if( leaveType  != null && leaveType.equalsIgnoreCase("f") ){
														 leaveCount = leaveCount+1;
														 numberOfLeaveTaken = numberOfLeaveTaken+1;
													 }
												 }
											}
										}
							 }
							 leaveDetails.put(CommonConstants.NUMBEROFLEAVETAKEN, numberOfLeaveTaken);
								resultJSONArray.put(leaveDetails);
						}
					}
				}
					result.put(CommonConstants.USEDLEAVES, leaveCount);
					/*result.put(CommonConstants.TYPE, String.valueOf(leaveGroupList.getLeaveManagement().getLeaveType().getLeaveTypeId()));
					result.put(CommonConstants.TYPENAME, String.valueOf(leaveGroupList.getLeaveManagement().getLeaveType().getType()));*/
					result.put(CommonConstants.LEAVES, resultJSONArray);
					
					long noOfDays = 0;
					List<LeaveGroupSettingsDO> leaveGroupList = leaveGroupSettingsService.retrieveByGroupID(leavegrp.getLeaveGroupId());
					if(leaveGroupList != null && leaveGroupList.size() > 0){
						for (LeaveGroupSettingsDO leaveType : leaveGroupList) {
							if(leaveType.getLeaveManagement() != null && leaveType.getLeaveManagement().getNumberOfDays() != null){
								noOfDays += leaveType.getLeaveManagement().getNumberOfDays();
							}
						}
					}
					result.put(CommonConstants.DAYS, noOfDays);
				}
			//}
		//}
		
		
		return result;
	}
	
	
	public static JSONObject getLeavePermissionDetailObjectForFilter(UserDO user, Date startDate, Date endDate)throws JSONException, ParseException {
		JSONObject result = new JSONObject();
	
		if(user != null && user.getEmployee() != null){
			if(user.getEmployee().getMiddlename() != null) result.put(CommonConstants.NAME, String.valueOf(user.getEmployee().getFirstname() + " " +user.getEmployee().getMiddlename() + " " + user.getEmployee().getLastname()));
			else if(user.getEmployee().getMiddlename() != null && user.getEmployee().getLastname() == null )result.put(CommonConstants.NAME, String.valueOf(user.getEmployee().getFirstname() + " " +user.getEmployee().getMiddlename()));
			else if(user.getEmployee().getMiddlename() == null && user.getEmployee().getLastname() == null )result.put(CommonConstants.NAME, String.valueOf(user.getEmployee().getFirstname()));
			else result.put(CommonConstants.NAME, String.valueOf(user.getEmployee().getFirstname() + " " + user.getEmployee().getLastname()));
			
			result.put(CommonConstants.EMPID, String.valueOf(user.getEmployee().getEmpId()));
			List<LeaveRequestsDO> leaveRequestsList  = null ;
			if(startDate == null && endDate == null){
				leaveRequestsList = leaveRequestsService.retrievePermissionByEmpID(user.getEmployee().getEmpId(),"p");
			}else{
				 leaveRequestsList = leaveRequestsService.retrievePermissionByYear(user.getEmployee().getEmpId(),startDate,endDate,"p");
			}
		
			
			JSONArray resultJSONArray = new JSONArray();
			int leaveCount = 0;
			if(leaveRequestsList != null && leaveRequestsList.size() > 0){
				for (LeaveRequestsDO leaveRequest : leaveRequestsList) {
					JSONObject leaveDetails = new JSONObject();
					leaveDetails.put(CommonConstants.FROM, String.valueOf(leaveRequest.getFromDate()));
					leaveDetails.put(CommonConstants.FROMDATEDISPLAY, String.valueOf(CommonUtil.convertDateToStringApp(leaveRequest.getFromDate())));
					leaveDetails.put(CommonConstants.TODATE, String.valueOf(leaveRequest.getToDate()));
					leaveDetails.put(CommonConstants.HOURS, String.valueOf(leaveRequest.getNumberofhours()));
					
					if(leaveRequest.getPermisssionformhours() != null){
						result.put(CommonConstants.PERMISSIONFROM, String.valueOf(leaveRequest.getPermisssionformhours()));
						result.put(CommonConstants.PERMISSIONFROMDISPLAY, String.valueOf(CommonUtil.convertDateToStringApp(leaveRequest.getPermisssionformhours())));
					}else{
						result.put(CommonConstants.PERMISSIONFROM, "");
						result.put(CommonConstants.PERMISSIONFROMDISPLAY, String.valueOf(""));
					}
					
					if(leaveRequest.getPermisssiontohours() != null){
						result.put(CommonConstants.PERMISSIONTO, String.valueOf(leaveRequest.getPermisssiontohours()));
						result.put(CommonConstants.PERMISSIONTODISPLAY, String.valueOf(CommonUtil.convertDateToStringApp(leaveRequest.getPermisssiontohours())));
					}else{
						result.put(CommonConstants.PERMISSIONTO, "");
						result.put(CommonConstants.PERMISSIONTODISPLAY, String.valueOf(""));
					}
					
					leaveDetails.put(CommonConstants.STATUS, String.valueOf(leaveRequest.getStatus()));
					leaveDetails.put(CommonConstants.DESCRIPTION, leaveRequest.getDescription()!=null?String.valueOf(leaveRequest.getDescription()):"");
					leaveDetails.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(leaveRequest.getUpdatedon())));
					if(leaveRequest.getComments() != null){
						leaveDetails.put(CommonConstants.COMMENT, String.valueOf(leaveRequest.getComments()));
					}else{
						leaveDetails.put(CommonConstants.COMMENT, String.valueOf(""));
					}
					if(leaveRequest.getLeaveGroup() != null){
						leaveDetails.put(CommonConstants.LEAVEGROUP_ID, String.valueOf(leaveRequest.getLeaveGroup().getLeaveGroupId()));
						leaveDetails.put(CommonConstants.LEAVEGROUP, String.valueOf(leaveRequest.getLeaveGroup().getLeaveGroup()));
					}else{
						leaveDetails.put(CommonConstants.LEAVEGROUP_ID, "");
						leaveDetails.put(CommonConstants.LEAVEGROUP, "");
					}
					leaveDetails.put(CommonConstants.TYPENAME, String.valueOf("Permission"));
					leaveDetails.put(CommonConstants.TYPE, String.valueOf(leaveRequest.getType()));
					if(leaveRequest.getToDate() != null){
						leaveDetails.put(CommonConstants.TODATE, String.valueOf(leaveRequest.getToDate()));
						leaveDetails.put(CommonConstants.TODATEDISPLAY, String.valueOf(CommonUtil.convertDateToStringApp(leaveRequest.getToDate())));
					}else{
						leaveDetails.put(CommonConstants.TODATE, "");
						leaveDetails.put(CommonConstants.TODATEDISPLAY, "");
					}
					
					
					if(leaveRequest.getPermisssiontohours() != null){
						leaveDetails.put(CommonConstants.PERMISSIONTO, String.valueOf(leaveRequest.getPermisssiontohours()));
						leaveDetails.put(CommonConstants.PERMISSIONTODISPLAY, String.valueOf(CommonUtil.convertDateToStringApp(leaveRequest.getPermisssiontohours())));
					}else{
						leaveDetails.put(CommonConstants.PERMISSIONTO, "");
						leaveDetails.put(CommonConstants.PERMISSIONTODISPLAY, String.valueOf(""));
					}
					
					if(leaveRequest.getPermisssionformhours() != null){
						leaveDetails.put(CommonConstants.PERMISSIONFROM, String.valueOf(leaveRequest.getPermisssionformhours()));
						leaveDetails.put(CommonConstants.PERMISSIONFROMDISPLAY, String.valueOf(CommonUtil.convertDateToStringApp(leaveRequest.getPermisssionformhours())));
					}else{
						leaveDetails.put(CommonConstants.PERMISSIONFROM, "");
						leaveDetails.put(CommonConstants.PERMISSIONFROMDISPLAY, String.valueOf(""));
					}
					
					leaveCount++;
					resultJSONArray.put(leaveDetails);
				}
			}
			
			result.put(CommonConstants.USEDLEAVES, leaveCount);
			result.put(CommonConstants.TYPENAME, String.valueOf("Permission"));
			result.put(CommonConstants.LEAVES, resultJSONArray);
			
			}
		//}
		
		
		return result;
	}
	
	
	public static JSONObject getLeavePermissionDetailObjectForEmpIdFilter(String empId, Date startDate, Date endDate)throws JSONException, ParseException {
		JSONObject result = new JSONObject();
	
		if(empId != null){
			
			List<EmployeeDO> empList = employeeService.retrieveEmpId(empId);
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.NAME, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else if(empList.get(0).getMiddlename() != null && empList.get(0).getLastname() == null )result.put(CommonConstants.NAME, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename()));
				else if(empList.get(0).getMiddlename() == null && empList.get(0).getLastname() == null )result.put(CommonConstants.NAME, String.valueOf(empList.get(0).getFirstname()));
				else result.put(CommonConstants.NAME, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}
			
			
			result.put(CommonConstants.EMPID, String.valueOf(empId));
			List<LeaveRequestsDO> leaveRequestsList  = null ;
			String reqType = "p";
			if(startDate == null && endDate == null){
				
				leaveRequestsList = leaveRequestsService.retrievePermissionByEmpID(empId,reqType);
			}else{
				 leaveRequestsList = leaveRequestsService.retrievePermissionByYear(empId,startDate,endDate,reqType);
			}
			JSONArray resultJSONArray = new JSONArray();
			int leaveCount = 0;
			if(leaveRequestsList != null && leaveRequestsList.size() > 0){
				for (LeaveRequestsDO leaveRequest : leaveRequestsList) {
					JSONObject leaveDetails = new JSONObject();
					leaveDetails.put(CommonConstants.FROM, String.valueOf(leaveRequest.getFromDate()));
					leaveDetails.put(CommonConstants.FROMDATEDISPLAY, String.valueOf(CommonUtil.convertDateToStringApp(leaveRequest.getFromDate())));
					leaveDetails.put(CommonConstants.TODATE, String.valueOf(leaveRequest.getToDate()));
					leaveDetails.put(CommonConstants.STATUS, String.valueOf(leaveRequest.getStatus()));
					leaveDetails.put(CommonConstants.TYPENAME, String.valueOf("Permission"));
					leaveDetails.put(CommonConstants.DESCRIPTION, leaveRequest.getDescription()!=null?String.valueOf(leaveRequest.getDescription()):"");
					leaveDetails.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(leaveRequest.getUpdatedon())));
					if(leaveRequest.getComments() != null){
						leaveDetails.put(CommonConstants.COMMENT, String.valueOf(leaveRequest.getComments()));
					}else{
						leaveDetails.put(CommonConstants.COMMENT, String.valueOf(""));
					}
					if(leaveRequest.getLeaveGroup() != null){
						leaveDetails.put(CommonConstants.LEAVEGROUP_ID, String.valueOf(leaveRequest.getLeaveGroup().getLeaveGroupId()));
						leaveDetails.put(CommonConstants.LEAVEGROUP, String.valueOf(leaveRequest.getLeaveGroup().getLeaveGroup()));
					}else{
						leaveDetails.put(CommonConstants.LEAVEGROUP_ID, "");
						leaveDetails.put(CommonConstants.LEAVEGROUP, "");
					}
					leaveDetails.put(CommonConstants.TYPE, String.valueOf(leaveRequest.getType()));
					if(leaveRequest.getToDate() != null){
						leaveDetails.put(CommonConstants.TODATE, String.valueOf(leaveRequest.getToDate()));
						leaveDetails.put(CommonConstants.TODATEDISPLAY, String.valueOf(CommonUtil.convertDateToStringApp(leaveRequest.getToDate())));
					}else{
						leaveDetails.put(CommonConstants.TODATE, "");
						leaveDetails.put(CommonConstants.TODATEDISPLAY, "");
					}
					if(leaveRequest.getPermisssionformhours() != null){
						leaveDetails.put(CommonConstants.PERMISSIONFROM, String.valueOf(leaveRequest.getPermisssionformhours()));
						leaveDetails.put(CommonConstants.PERMISSIONFROMDISPLAY, String.valueOf(CommonUtil.convertDateToStringApp(leaveRequest.getPermisssionformhours())));
					}else{
						leaveDetails.put(CommonConstants.PERMISSIONFROM, "");
						leaveDetails.put(CommonConstants.PERMISSIONFROMDISPLAY, "");
					}
					
					if(leaveRequest.getPermisssiontohours() != null){
						leaveDetails.put(CommonConstants.PERMISSIONTO, String.valueOf(leaveRequest.getPermisssiontohours()));
						leaveDetails.put(CommonConstants.PERMISSIONTODISPLAY, String.valueOf(CommonUtil.convertDateToStringApp(leaveRequest.getPermisssiontohours())));
					}else{
						leaveDetails.put(CommonConstants.PERMISSIONTO, "");
						leaveDetails.put(CommonConstants.PERMISSIONTODISPLAY, "");
					}
					
					//leaveCount++;
					resultJSONArray.put(leaveDetails);
				}
			}
			
			result.put(CommonConstants.USEDLEAVES, leaveCount);
			result.put(CommonConstants.TYPENAME, String.valueOf("Permission"));
			result.put(CommonConstants.LEAVES, resultJSONArray);
			
			}
		//}
		
		
		return result;
	}
	
	
	public static JSONObject getLeaveReport(List<LeaveGroupSettingsDO> leaveGrpSettingList, String empId, Date start, Date end, Date fromDate, Date toDate)throws ParseException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for(LeaveGroupSettingsDO leaveGrpSetting : leaveGrpSettingList){
				//boolean notInRangs = leaveManagementService.checkLeaveInDateRange(leaveGrpSetting.getLeaveManagement().getLeaveManagementId())
					resultJSONArray.put(getLeaveReportDetails(leaveGrpSetting, empId, start,end,fromDate, toDate));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getLeaveReportDetails(LeaveGroupSettingsDO leaveGrpSetting, String empId, Date startDate, Date endDate,Date fromDate, Date toDate)throws JSONException, ParseException {
		JSONObject result = new JSONObject();
		if(leaveGrpSetting.getLeaveManagement() != null){
			
	        
			//if((leaveGrpSetting.getLeaveManagement().fromDate < start && leaveGrpSetting.getLeaveManagement().toDate >= end) )
			
			if(leaveGrpSetting.getLeaveManagement().getEarnedLeave() != null && leaveGrpSetting.getLeaveManagement().getEarnedLeave().equalsIgnoreCase("e")){
				List<EarnedLeavesDO> employeeDetailList = earnedLeavesService.retrieveByEmpId(empId);
				if(employeeDetailList != null && employeeDetailList.size() > 0){
					result.put(CommonConstants.DAYS_ALLOWED, String.valueOf(employeeDetailList.get(0).getAvailableLeaves()));
				}else{
					result.put(CommonConstants.DAYS_ALLOWED, String.valueOf("0"));
				}
				
			}else{
				if(leaveGrpSetting.getLeaveManagement().getNumberOfDays() != null){
					result.put(CommonConstants.DAYS_ALLOWED, String.valueOf(leaveGrpSetting.getLeaveManagement().getNumberOfDays()));
				}else{
					result.put(CommonConstants.DAYS_ALLOWED, String.valueOf("0"));
				}
			}
			
			
			List <LeaveRequestsDO> takenLeaveCount = null;
			if(fromDate != null && toDate != null){
				 takenLeaveCount = leaveRequestsService.retrivebyEmpLeaveperiod(empId, fromDate, toDate, leaveGrpSetting.getLeaveManagement().getFromDate(),leaveGrpSetting.getLeaveManagement().getToDate(), leaveGrpSetting.getLeaveGroup().getLeaveGroupId(),leaveGrpSetting.getLeaveManagement().getLeaveManagementId());
			}else{
				takenLeaveCount = leaveRequestsService.retrivebyEmpLeaveperiod(empId, startDate, endDate, leaveGrpSetting.getLeaveManagement().getFromDate(),leaveGrpSetting.getLeaveManagement().getToDate(), leaveGrpSetting.getLeaveGroup().getLeaveGroupId(),leaveGrpSetting.getLeaveManagement().getLeaveManagementId());
			}
			
			
			if(takenLeaveCount != null && takenLeaveCount.size()>0){
				double leaveCount = 0.0;
				for (LeaveRequestsDO leaveRequest : takenLeaveCount) {
					double halfCount = 0.5;
					
					Date dateBeforeString = leaveRequest.getFromDate();
					 if(leaveRequest.getToDate() != null){
						 Calendar end = Calendar.getInstance();
						 Date dateAfterString = leaveRequest.getToDate();
						  end.setTime(dateAfterString);
						  Calendar start = Calendar.getInstance();
						  start.setTime(dateBeforeString);
						 for (Date date = start.getTime(); start.before(end) || start.equals(end); start.add(Calendar.DATE, 1), date = start.getTime()) {
							Calendar weekend = Calendar.getInstance();
							weekend.setTime(date);
							/*if (weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){
								String leaveType =  String.valueOf(leaveRequest.getType());
								 if( leaveType  != null && leaveType.equalsIgnoreCase("h") ){
									 leaveCount =   (leaveCount+ halfCount);
								 }else{
									 if( leaveType  != null && leaveType.equalsIgnoreCase("f") ){
										 leaveCount = leaveCount+1;
									 }
								 }
							}*/
							if(leaveRequest.getLeaveManagement().getSaturday() != null && leaveRequest.getLeaveManagement().getSaturday().equalsIgnoreCase("y") && 
									leaveRequest.getLeaveManagement().getSunday() != null && leaveRequest.getLeaveManagement().getSunday().equalsIgnoreCase("y")){
								String leaveType =  String.valueOf(leaveRequest.getType());
								 if( leaveType  != null && leaveType.equalsIgnoreCase("h") ){
									 leaveCount =   (leaveCount+ halfCount);
								 }else{
									 if( leaveType  != null && leaveType.equalsIgnoreCase("f") ){
										 leaveCount = leaveCount+1;
									 }
								 }
							}else if(leaveRequest.getLeaveManagement().getSaturday() != null && leaveRequest.getLeaveManagement().getSaturday().equalsIgnoreCase("n") &&
									leaveRequest.getLeaveManagement().getSunday() != null && leaveRequest.getLeaveManagement().getSunday().equalsIgnoreCase("n")){
								if (weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && 
									weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){
									String leaveType =  String.valueOf(leaveRequest.getType());
									 if( leaveType  != null && leaveType.equalsIgnoreCase("h") ){
										 leaveCount =   (leaveCount+ halfCount);
									 }else{
										 if( leaveType  != null && leaveType.equalsIgnoreCase("f") ){
											 leaveCount = leaveCount+1;
										 }
									 }
								}
							}else if(leaveRequest.getLeaveManagement().getSaturday() != null && leaveRequest.getLeaveManagement().getSaturday().equalsIgnoreCase("y") && 
									leaveRequest.getLeaveManagement().getSunday() != null && leaveRequest.getLeaveManagement().getSunday().equalsIgnoreCase("n")){
								if (weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){
									String leaveType =  String.valueOf(leaveRequest.getType());
									 if( leaveType  != null && leaveType.equalsIgnoreCase("h") ){
										 leaveCount =   (leaveCount+ halfCount);
									 }else{
										 if( leaveType  != null && leaveType.equalsIgnoreCase("f") ){
											 leaveCount = leaveCount+1;
										 }
									 }
								}
							}else if(leaveRequest.getLeaveManagement().getSaturday() != null && leaveRequest.getLeaveManagement().getSaturday().equalsIgnoreCase("n") &&
									leaveRequest.getLeaveManagement().getSunday() != null && leaveRequest.getLeaveManagement().getSunday().equalsIgnoreCase("y")){
								if (/*weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY &&*/ 
									weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY){
									String leaveType =  String.valueOf(leaveRequest.getType());
									 if( leaveType  != null && leaveType.equalsIgnoreCase("h") ){
										 leaveCount =   (leaveCount+ halfCount);
									 }else{
										 if( leaveType  != null && leaveType.equalsIgnoreCase("f") ){
											 leaveCount = leaveCount+1;
										 }
									 }
								}
							}
						}
					 }else{
						  Calendar start = Calendar.getInstance();
						  start.setTime(dateBeforeString);
						  Date date = start.getTime();
							 Calendar weekend = Calendar.getInstance();
							 weekend.setTime(date);
								/*if (weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){
									String leaveType =  String.valueOf(leaveRequest.getType());
									 if( leaveType  != null && leaveType.equalsIgnoreCase("h") ){
										 leaveCount =  (leaveCount+ halfCount);
									 }else{
										 if( leaveType  != null && leaveType.equalsIgnoreCase("f") ){
											 leaveCount = leaveCount+1;
										 }
									 }
								}*/
							 if(leaveRequest.getLeaveManagement().getSaturday() != null && leaveRequest.getLeaveManagement().getSaturday().equalsIgnoreCase("y") && 
										leaveRequest.getLeaveManagement().getSunday() != null && leaveRequest.getLeaveManagement().getSunday().equalsIgnoreCase("y")){
									String leaveType =  String.valueOf(leaveRequest.getType());
									 if( leaveType  != null && leaveType.equalsIgnoreCase("h") ){
										 leaveCount =   (leaveCount+ halfCount);
									 }else{
										 if( leaveType  != null && leaveType.equalsIgnoreCase("f") ){
											 leaveCount = leaveCount+1;
										 }
									 }
								}else if(leaveRequest.getLeaveManagement().getSaturday() != null && leaveRequest.getLeaveManagement().getSaturday().equalsIgnoreCase("n") &&
										leaveRequest.getLeaveManagement().getSunday() != null && leaveRequest.getLeaveManagement().getSunday().equalsIgnoreCase("n")){
									if (weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && 
										weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){
										String leaveType =  String.valueOf(leaveRequest.getType());
										 if( leaveType  != null && leaveType.equalsIgnoreCase("h") ){
											 leaveCount =   (leaveCount+ halfCount);
										 }else{
											 if( leaveType  != null && leaveType.equalsIgnoreCase("f") ){
												 leaveCount = leaveCount+1;
											 }
										 }
									}
								}else if(leaveRequest.getLeaveManagement().getSaturday() != null && leaveRequest.getLeaveManagement().getSaturday().equalsIgnoreCase("y") && 
										leaveRequest.getLeaveManagement().getSunday() != null && leaveRequest.getLeaveManagement().getSunday().equalsIgnoreCase("n")){
									if (weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){
										String leaveType =  String.valueOf(leaveRequest.getType());
										 if( leaveType  != null && leaveType.equalsIgnoreCase("h") ){
											 leaveCount =   (leaveCount+ halfCount);
										 }else{
											 if( leaveType  != null && leaveType.equalsIgnoreCase("f") ){
												 leaveCount = leaveCount+1;
											 }
										 }
									}
								}else if(leaveRequest.getLeaveManagement().getSaturday() != null && leaveRequest.getLeaveManagement().getSaturday().equalsIgnoreCase("n") &&
										leaveRequest.getLeaveManagement().getSunday() != null && leaveRequest.getLeaveManagement().getSunday().equalsIgnoreCase("y")){
									if (/*weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY &&*/ 
										weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY){
										String leaveType =  String.valueOf(leaveRequest.getType());
										 if( leaveType  != null && leaveType.equalsIgnoreCase("h") ){
											 leaveCount =   (leaveCount+ halfCount);
										 }else{
											 if( leaveType  != null && leaveType.equalsIgnoreCase("f") ){
												 leaveCount = leaveCount+1;
											 }
										 }
									}
								}
					 }
				}
				result.put(CommonConstants.DAYS, String.valueOf(leaveCount));
			}else{
				result.put(CommonConstants.DAYS, String.valueOf(0));
			}
			result.put(CommonConstants.TYPENAME, String.valueOf(leaveGrpSetting.getLeaveManagement().getLeaveType()));
		}
		
		return result;
	}
	
}
