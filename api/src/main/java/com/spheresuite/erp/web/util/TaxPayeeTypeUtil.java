package com.spheresuite.erp.web.util;

import java.util.List;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.TaxPayeeTypeDO;
import com.spheresuite.erp.domainobject.TaxSlabsSettingsDO;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.TaxPayeeTypeService;
import com.spheresuite.erp.service.TaxSlabsSettingsService;
import com.spheresuite.erp.util.CommonConstants;

@Component
public class TaxPayeeTypeUtil {
	
	private TaxPayeeTypeUtil() {}
	
	@Autowired
	private EmployeeService employeeServiceTemp;
	
	@Autowired
	private TaxPayeeTypeService taxPayeeTypeServiceTemp;
	
	@Autowired
	private TaxSlabsSettingsService taxSlabsSettingsServiceTemp;
	
	private static EmployeeService employeeService;
	
	private static TaxPayeeTypeService taxPayeeTypeService;
	
	private static TaxSlabsSettingsService taxSlabsSettingsService;
	
	@PostConstruct
	public void init() {
		employeeService = this.employeeServiceTemp;
		taxPayeeTypeService = this.taxPayeeTypeServiceTemp;
		taxSlabsSettingsService = this.taxSlabsSettingsServiceTemp;
	}
	
	public static JSONObject getTaxPayeeTypeList(List<TaxPayeeTypeDO> taxPayeeTypeList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (TaxPayeeTypeDO taxPayeeType : taxPayeeTypeList) {
				resultJSONArray.put(getTaxPayeeDetailObject(taxPayeeType));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	public static JSONObject getTaxPayeeSlabSetting(List<TaxPayeeTypeDO> taxPayeeTypeList, String startDate, String endDate) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (TaxPayeeTypeDO taxPayeeType : taxPayeeTypeList) {
				resultJSONArray.put(getTaxPayeeSlabSettingDetailObject(taxPayeeType, startDate, endDate));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getTaxPayeeDetailObject(TaxPayeeTypeDO taxPayeeType)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(taxPayeeType.getTaxPayeeTypeId()));
		result.put(CommonConstants.TAXPAYEETYPE, String.valueOf(taxPayeeType.getTaxpayeeType()));
		result.put(CommonConstants.STATUS, String.valueOf(taxPayeeType.getStatus()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(taxPayeeType.getUpdatedon())));
		if(taxPayeeType.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(taxPayeeType.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		return result;
	}
	
	public static JSONObject getTaxPayeeSlabSettingDetailObject(TaxPayeeTypeDO taxPayeeType, String startDate, String endDate)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(taxPayeeType.getTaxPayeeTypeId()));
		result.put(CommonConstants.NAME, String.valueOf(taxPayeeType.getTaxpayeeType()));
		JSONArray resultJSONArray = new JSONArray();
		/*List<TaxSlabsSettingsDO> adeductionSettingList = taxSlabsSettingsService.retrieveBytaxPayee(taxPayeeType.getTaxPayeeTypeId(), startDate, endDate);
		if(adeductionSettingList != null && adeductionSettingList.size()  > 0){
			for (TaxSlabsSettingsDO doc : adeductionSettingList) {
				resultJSONArray.put(getTaxSettingsDetailObject(doc));
			}
		}*/
		result.put(CommonConstants.VALUE, resultJSONArray);
		return result;
	}
	
	public static JSONObject getTaxSettingsDetailObject(TaxSlabsSettingsDO taxSlabsSettingsDO)throws JSONException {
		JSONObject result = new JSONObject();
		/*result.put(CommonConstants.ID, String.valueOf(taxSlabsSettingsDO.getId() != null ? taxSlabsSettingsDO.getId() : ""));
		result.put(CommonConstants.PERCENTAGEAMOUNT, String.valueOf(taxSlabsSettingsDO.getPercentageamount() != null ? taxSlabsSettingsDO.getPercentageamount() : ""));
		result.put(CommonConstants.FROMAMOUNT, String.valueOf(taxSlabsSettingsDO.getAmountfrom() != null ? taxSlabsSettingsDO.getAmountfrom() : ""));
		result.put(CommonConstants.TOAMOUNT, String.valueOf(taxSlabsSettingsDO.getAmountto() != null ? taxSlabsSettingsDO.getAmountto() : ""));
		if(taxSlabsSettingsDO.getUpdatedon() != null){
			result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(taxSlabsSettingsDO.getUpdatedon())));
		}else{
			result.put(CommonConstants.UPDATEDON, String.valueOf(""));
		}
		result.put(CommonConstants.STARTDATE,String.valueOf(taxSlabsSettingsDO.getStartdate() != null ? taxSlabsSettingsDO.getStartdate() : ""));
		result.put(CommonConstants.ENDDATE,String.valueOf(taxSlabsSettingsDO.getEnddate() != null ? taxSlabsSettingsDO.getEnddate() : ""));
		result.put(CommonConstants.STATUS, String.valueOf(taxSlabsSettingsDO.getStatus()));
		if(taxSlabsSettingsDO.getTaxpayeeId() != null){
			result.put(CommonConstants.TAXPAYEEID, String.valueOf(taxSlabsSettingsDO.getTaxpayeeId()));
			List<TaxPayeeTypeDO> taxpayeeList = taxPayeeTypeService.retrieveById(taxSlabsSettingsDO.getTaxpayeeId());
			if(taxpayeeList != null && taxpayeeList.size() > 0){
				result.put(CommonConstants.TAXPAYEENAME, String.valueOf(taxpayeeList.get(0).getTaxpayeeType()));
			}else{
				result.put(CommonConstants.TAXPAYEENAME, String.valueOf(""));
			}
		}else{
			result.put(CommonConstants.TAXPAYEEID, String.valueOf(""));
		}
		if(taxSlabsSettingsDO.getUpdatedBy() != null){
			String empName = CommonUtil.getEmpObject(taxSlabsSettingsDO.getUpdatedBy());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		String displayName="";
		if(taxSlabsSettingsDO.getPercentageamount() != null){
			displayName = taxSlabsSettingsDO.getPercentageamount() + "% Of Taxable Amount";
		}
		result.put(CommonConstants.SETTINGNAME, String.valueOf(displayName));*/
		return result;
	}
}
