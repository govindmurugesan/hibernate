package com.spheresuite.erp.web.util;

import java.util.List;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.ItSavingsDO;
import com.spheresuite.erp.domainobject.ItSavingsDocDO;
import com.spheresuite.erp.service.ItSavingDocService;
import com.spheresuite.erp.util.CommonConstants;
@Component
public class ItSavingsUtil {
	
	private ItSavingsUtil() {}
	
	@Autowired
	private  ItSavingDocService titSavingDocService;
	private static ItSavingDocService itSavingDocService;
	
	@PostConstruct
	public void init() {
		itSavingDocService=titSavingDocService;
	}
	
	public static JSONObject getITSavingsSettingsList(List<ItSavingsDO> itSavingsDOList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (ItSavingsDO itSavingsDO : itSavingsDOList) {
				resultJSONArray.put(getITSavingsSettingsObject(itSavingsDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getITSavingsSettingsObject(ItSavingsDO itSavingsDO)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(itSavingsDO.getItSavingsId()));
		result.put(CommonConstants.EMPID, String.valueOf(itSavingsDO.getEmployee().getEmpId()));
		result.put(CommonConstants.FROMDATE, String.valueOf(itSavingsDO.getFromYear()));
		if(itSavingsDO.getToYear() != null){
			result.put(CommonConstants.TODATE, String.valueOf(itSavingsDO.getToYear()));
		}
		result.put(CommonConstants.COMMENT, String.valueOf(itSavingsDO.getComments() != null ? itSavingsDO.getComments() : ""));
		if(itSavingsDO.getItSavingsSettings() != null){
			result.put(CommonConstants.ITSAVINGSETTINGID, String.valueOf(itSavingsDO.getItSavingsSettings().getItSavingsSettingsId()));
			result.put(CommonConstants.ITSAVINGSETTINGNAME, String.valueOf(itSavingsDO.getItSavingsSettings().getName()));
			result.put(CommonConstants.SECTION, String.valueOf(itSavingsDO.getItSavingsSettings().getItSection().getSection()));
			result.put(CommonConstants.MAXLIMIT, String.valueOf(itSavingsDO.getItSavingsSettings().getItSection().getMaxLimit()));	
		}else{
			result.put(CommonConstants.ITSAVINGSETTINGID, "");
			result.put(CommonConstants.SECTION, "");
			result.put(CommonConstants.MAXLIMIT, "");
		}
		result.put(CommonConstants.AMOUNT, String.valueOf(itSavingsDO.getAmount()));
		result.put(CommonConstants.STATUS, String.valueOf(itSavingsDO.getStatus()));
		
		if(itSavingsDO.getEmployee().getEmpId()  != null){
			String empName = CommonUtil.getEmpObject(itSavingsDO.getEmployee().getEmpId() );
			result.put(CommonConstants.EMPNAME, empName); 
		}else{
			result.put(CommonConstants.EMPNAME, ""); 
		}
		
		if(itSavingsDO.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(itSavingsDO.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(itSavingsDO.getUpdatedon())));
		JSONArray resultJSONArray = new JSONArray();
		List<ItSavingsDocDO> itSavingsDocList = itSavingDocService.retrieveByOppId(itSavingsDO.getItSavingsId());
		if(itSavingsDocList != null && itSavingsDocList.size()  > 0){
			for (ItSavingsDocDO doc : itSavingsDocList) {
				resultJSONArray.put(getDocDetailObject(doc));
			}
		}
		result.put(CommonConstants.FILE, resultJSONArray);
		return result;
	}
	

	public static JSONObject getDocDetailObject(ItSavingsDocDO doc)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.FILE, String.valueOf(doc.getPhoto()));
		if(doc.getFileName() != null){
			result.put(CommonConstants.NAME, String.valueOf(doc.getFileName()));
		}else{
			result.put(CommonConstants.NAME, "");	
		}
		
		return result;
	}
}
