package com.spheresuite.erp.web.util;

import java.util.List;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.AssignedMenuDO;
import com.spheresuite.erp.service.AssignedMenuService;
import com.spheresuite.erp.util.CommonConstants;
@Component
public class AssignedMenuUtil {
	
	private AssignedMenuUtil() {}
	
	@Autowired
	private AssignedMenuService tassignedMenuService;
	private static AssignedMenuService assignedMenuService;
	@PostConstruct
	public void init() {
		assignedMenuService = tassignedMenuService;
	}
	
	public static JSONObject getMenuList(List<AssignedMenuDO> menuList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (AssignedMenuDO menu : menuList) {
				resultJSONArray.put(getAssignmentMenuObject(menu));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getAssignmentMenuObject(AssignedMenuDO menu)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(menu.getAssignedmenuId()));
		result.put(CommonConstants.NAME, String.valueOf(menu.getMenu().getName()));
		result.put(CommonConstants.DISPLAYNAME, String.valueOf(menu.getMenu().getDisplayName()));
		if(menu.getMenu().getUrl() != null){
			result.put(CommonConstants.URL, String.valueOf(menu.getMenu().getUrl()));
		}
		if(menu.getMenu() != null){
			result.put(CommonConstants.MENUID, String.valueOf(menu.getMenu().getMenuId()));
		}
		if(menu.getMenu().getIcon() != null){
			result.put(CommonConstants.ICON, String.valueOf(menu.getMenu().getIcon()));
		}
		
		if(menu.getMenu().getActiveicon() != null){
			result.put(CommonConstants.ACTIVEICON, String.valueOf(menu.getMenu().getActiveicon()));
		}else{
			result.put(CommonConstants.ACTIVEICON, "");
		}
		if(menu.getMenu().getSupermenu() != null){
			result.put(CommonConstants.SUPERMENU, String.valueOf(menu.getMenu().getSupermenu()));
		}else{
			result.put(CommonConstants.SUPERMENU, "");
		}
		if(menu.getMenu().getProduct() != null){
			result.put(CommonConstants.PRODUCT_ID, String.valueOf(menu.getMenu().getProduct()));
		}else{
			result.put(CommonConstants.PRODUCT_ID, "");
		}
		return result;
	}
	
	public static JSONObject getMenuList1(List<AssignedMenuDO> menuList, Long roleId) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (AssignedMenuDO menu : menuList) {
				if(menu.getMenu().getSupermenu() == null || menu.getMenu().getSupermenu() == Long.parseLong("1")){
					resultJSONArray.put(getAssignmentMenuObject1(menu, roleId));
				}
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getAssignmentMenuObject1(AssignedMenuDO menu, Long roleId)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(menu.getAssignedmenuId()));
		result.put(CommonConstants.NAME, String.valueOf(menu.getMenu().getDisplayName()));
		if(menu.getMenu() != null){
			result.put(CommonConstants.MENUID, String.valueOf(menu.getMenu().getMenuId()));
		}
		if(menu.getMenu() != null && menu.getMenu().getUrl() != null){
			result.put(CommonConstants.URL, String.valueOf(menu.getMenu().getUrl()));
		}
		if(menu.getMenu() != null && menu.getMenu().getIcon() != null){
			result.put(CommonConstants.ICON, String.valueOf(menu.getMenu().getIcon()));
		}
		
		if(menu.getMenu() != null && menu.getMenu().getActiveicon() != null){
			result.put(CommonConstants.ACTIVEICON, String.valueOf(menu.getMenu().getActiveicon()));
		}else{
			result.put(CommonConstants.ACTIVEICON, "");
		}
		if(menu.getMenu() != null && menu.getMenu().getSupermenu() != null){
			result.put(CommonConstants.SUPERMENU, String.valueOf(menu.getMenu().getSupermenu()));
		}else{
			result.put(CommonConstants.SUPERMENU, "");
		}
		if(menu.getMenu() != null && menu.getMenu().getProduct() != null){
			result.put(CommonConstants.PRODUCT_ID, String.valueOf(menu.getMenu().getProduct()));
		}else{
			result.put(CommonConstants.PRODUCT_ID, "");
		}
		JSONArray resultJSONArraySubMenu = new JSONArray();
		Long menuid =  menu.getMenu().getMenuId();
			List<AssignedMenuDO> submenuList = assignedMenuService.retrieveMenuBySupermenuRoleId(roleId, menuid);
			if(submenuList != null && submenuList.size() > 0){
				for(AssignedMenuDO subMenu : submenuList){
					//if(subMenu.getSupermenu() != null || subMenu.getSupermenu() != Long.parseLong("1")){
						resultJSONArraySubMenu.put(getSubMenuListforRoleObject1(subMenu));
					//}
				}
			}
		result.put(CommonConstants.SUB_MENU,resultJSONArraySubMenu);
		return result;
	}
	
	public static JSONObject getSubMenuListforRoleObject1(AssignedMenuDO subMenu)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(subMenu.getMenu().getMenuId()));
		result.put(CommonConstants.NAME, String.valueOf(subMenu.getMenu().getName()));
		if(subMenu.getMenu().getUrl() != null){
			result.put(CommonConstants.URL, String.valueOf(subMenu.getMenu().getUrl()));
		}
		if(subMenu.getMenu().getIcon() != null){
			result.put(CommonConstants.ICON, String.valueOf(subMenu.getMenu().getIcon()));
		}
		
		if(subMenu.getMenu().getActiveicon() != null){
			result.put(CommonConstants.ACTIVEICON, String.valueOf(subMenu.getMenu().getActiveicon()));
		}else{
			result.put(CommonConstants.ACTIVEICON, "");
		}
		
		if(subMenu.getMenu().getProduct() != null){
			result.put(CommonConstants.PRODUCT_ID, String.valueOf(subMenu.getMenu().getProduct()));
		}else{
			result.put(CommonConstants.PRODUCT_ID, "");
		}
		return result;
	}
}
