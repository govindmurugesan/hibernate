package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.FyDO;
import com.spheresuite.erp.util.CommonConstants;
@Component
public class FyUtil {
	
	private FyUtil() {}
	
	public static JSONObject getFyList(List<FyDO> fyDOList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (FyDO fyDO : fyDOList) {
				resultJSONArray.put(getFyDetailObject(fyDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getFyDetailObject(FyDO fyDO)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(fyDO.getFyId()));
		result.put(CommonConstants.FYFROM, String.valueOf(fyDO.getFromyear()));
		result.put(CommonConstants.FYTO, String.valueOf(fyDO.getToyear()));
		result.put(CommonConstants.STATUS, String.valueOf(fyDO.getStatus()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(fyDO.getUpdatedon())));
		if(fyDO.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(fyDO.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		return result;
	}
}
