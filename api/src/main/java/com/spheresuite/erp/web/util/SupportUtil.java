package com.spheresuite.erp.web.util;

import java.util.List;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.SupportDO;
import com.spheresuite.erp.domainobject.SupportDocDO;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.SupportDocService;
import com.spheresuite.erp.util.CommonConstants;
@Component
public class SupportUtil {
	
	private SupportUtil() {}
	
	@Autowired
	private EmployeeService temployeeService;
	
	@Autowired
	private SupportDocService tsupportDocService;
	
	
	private static EmployeeService employeeService;
	
	private static SupportDocService supportDocService;
	
	@PostConstruct
	public void init() {
		employeeService = temployeeService;
		supportDocService = tsupportDocService;
	}
	
	
	public static JSONObject getSupportList(List<SupportDO> supportList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (SupportDO support : supportList) {
				resultJSONArray.put(getSupportDetailObject(support));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getSupportDetailObject(SupportDO support)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(support.getSupportId()));
		if(support.getIssueType() !=null){
			result.put(CommonConstants.ISSUE_TYPE, String.valueOf(support.getIssueType().getIssueTypeId()));
			result.put(CommonConstants.ISSUE_TYPE_NAME, String.valueOf(support.getIssueType().getName()));
		}else{
			result.put(CommonConstants.ISSUE_TYPE, "");
			result.put(CommonConstants.ISSUE_TYPE_NAME, "");
		}
		if(support.getPriorityLevel() !=null){
			result.put(CommonConstants.PRIORITY_LEVEL_ID, String.valueOf(support.getPriorityLevel().getPriorityLevelId()));
			result.put(CommonConstants.PRIORITY_LEVEL_NAME, String.valueOf(support.getPriorityLevel().getName()));
		}else{
			result.put(CommonConstants.PRIORITY_LEVEL_ID, "");
			result.put(CommonConstants.PRIORITY_LEVEL_NAME, "");
		}
		
		if(support.getStatusDetails() !=null){
			result.put(CommonConstants.STATUS_ID, String.valueOf(support.getStatusDetails().getStatusId()));
			result.put(CommonConstants.STATUS_NAME, String.valueOf(support.getStatusDetails().getName()));
		}else{
			result.put(CommonConstants.STATUS_ID, "");
			result.put(CommonConstants.STATUS_NAME, "");
		}
		
		
		if(support.getEmail() !=null){
			result.put(CommonConstants.EMAIL, String.valueOf(support.getEmail()));
		}else{
			result.put(CommonConstants.EMAIL, "");
		}
		
		if(support.getEmpId() !=null){
			result.put(CommonConstants.EMPID, String.valueOf(support.getEmpId()));
		}else{
			result.put(CommonConstants.EMPID, "");
		}
		
		if(support.getEmpName() !=null){
			result.put(CommonConstants.EMPNAME, String.valueOf(support.getEmpName()));
		}else{
			result.put(CommonConstants.EMPNAME, "");
		}
		
		if(support.getMobile() !=null){
			result.put(CommonConstants.MOBILE, String.valueOf(support.getMobile()));
		}else{
			result.put(CommonConstants.MOBILE, "");
		}
		
		if(support.getPhone() !=null){
			result.put(CommonConstants.PHONE, String.valueOf(support.getPhone()));
		}else{
			result.put(CommonConstants.PHONE, "");
		}
		
		if(support.getOrganization() !=null){
			result.put(CommonConstants.COMPANY, String.valueOf(support.getOrganization()));
		}else{
			result.put(CommonConstants.COMPANY, "");
		}
		
		if(support.getIssueSummary() !=null){
			result.put(CommonConstants.ISSUE_SUMMARY, String.valueOf(support.getIssueSummary()));
		}else{
			result.put(CommonConstants.ISSUE_SUMMARY, "");
		}
		
		if(support.getIssueDetails() !=null){
			result.put(CommonConstants.ISSUE_DETAILS, String.valueOf(support.getIssueDetails()));
		}else{
			result.put(CommonConstants.ISSUE_DETAILS, "");
		}
		
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(support.getUpdatedon())));
		if(support.getUpdatedby() != null){
			
			List<EmployeeDO> empList = employeeService.retrieveEmpId(support.getUpdatedby().toString());
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
			
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}		
		
		JSONArray resultJSONArray = new JSONArray();
		List<SupportDocDO> supportDocDOList = supportDocService.retrieveBySupportId(support.getSupportId());
		if(supportDocDOList != null && supportDocDOList.size()  > 0){
			for (SupportDocDO doc : supportDocDOList) {
				resultJSONArray.put(getDocDetailObject(doc));
			}
		}
		result.put(CommonConstants.FILE, resultJSONArray);
		
		return result;
	}
	
	public static JSONObject getDocDetailObject(SupportDocDO doc)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.FILE, String.valueOf(doc.getPhoto()));
		if(doc.getFileName() != null){
			result.put(CommonConstants.NAME, String.valueOf(doc.getFileName()));
		}else{
			result.put(CommonConstants.NAME, "");	
		}
		
		return result;
	}
	
}
