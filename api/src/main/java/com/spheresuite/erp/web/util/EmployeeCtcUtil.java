package com.spheresuite.erp.web.util;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.AllowanceSettingDO;
import com.spheresuite.erp.domainobject.DeductionSettingDO;
import com.spheresuite.erp.domainobject.EmployeeBonusDO;
import com.spheresuite.erp.domainobject.EmployeeCtcDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.EmployeeDeductionDO;
import com.spheresuite.erp.domainobject.EmployeeEarningsDO;
import com.spheresuite.erp.domainobject.EmployeeLopDO;
import com.spheresuite.erp.domainobject.EmployeeTaxDO;
import com.spheresuite.erp.domainobject.TaxSettingsDO;
import com.spheresuite.erp.service.AllowanceSettingsService;
import com.spheresuite.erp.service.DeductionSettingsService;
import com.spheresuite.erp.service.EmployeeBonusService;
import com.spheresuite.erp.service.EmployeeCompensationService;
import com.spheresuite.erp.service.EmployeeCtcService;
import com.spheresuite.erp.service.EmployeeDeductionService;
import com.spheresuite.erp.service.EmployeeEarningsService;
import com.spheresuite.erp.service.EmployeeLopService;
import com.spheresuite.erp.service.EmployeePayrollService;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.EmployeeTaxService;
import com.spheresuite.erp.service.PayrollBatchService;
import com.spheresuite.erp.service.PayrollDeductionDetailsService;
import com.spheresuite.erp.service.PayrollEarningsDetailsService;
import com.spheresuite.erp.service.PayrollTaxDetailsService;
import com.spheresuite.erp.service.TaxSettingsService;
import com.spheresuite.erp.util.CommonConstants;
@Component
public class EmployeeCtcUtil {
	
	private EmployeeCtcUtil() {}
	@Autowired
	private EmployeeService temployeeService;
	private static EmployeeService employeeService;
	
	@Autowired
	private PayrollEarningsDetailsService tpayrollEarningsDetailsService;
	private static PayrollEarningsDetailsService payrollEarningsDetailsService;
	
	@Autowired
	private PayrollTaxDetailsService tpayrollTaxDetailsService;
	private static PayrollTaxDetailsService payrollTaxDetailsService;
	
	@Autowired
	private PayrollDeductionDetailsService tpayrollDeductionDetailsService;
	private static PayrollDeductionDetailsService payrollDeductionDetailsService;
	
	@Autowired
	private EmployeeCtcService temployeeCtcService;
	private static EmployeeCtcService employeeCtcService;

	@Autowired
	private EmployeePayrollService temployeePayrollService;
	private static EmployeePayrollService employeePayrollService;
	
	@Autowired
	private EmployeeBonusService temployeeBonusService;
	private static EmployeeBonusService employeeBonusService;
	
	@Autowired
	private  EmployeeLopService temployeeLopService;
	private static EmployeeLopService employeeLopService;
	
	@Autowired
	private  EmployeeEarningsService temployeeEarningsService;
	private static EmployeeEarningsService employeeEarningsService;
	
	@Autowired
	private  EmployeeTaxService temployeeTaxService;
	private static EmployeeTaxService employeeTaxService;
	
	@Autowired
	private  EmployeeDeductionService temployeeDeductionService;
	private static EmployeeDeductionService employeeDeductionService;
	
	@Autowired
	private  AllowanceSettingsService tallowanceSettingsService;
	private static AllowanceSettingsService allowanceSettingsService;
	
	@Autowired
	private  DeductionSettingsService tdeductionSettingsService;
	private static DeductionSettingsService deductionSettingsService;
	
	@Autowired
	private  TaxSettingsService ttaxSettingsService;
	private static TaxSettingsService taxSettingsService;
	
	@Autowired
	private  PayrollBatchService tpayrollBatchService;
	private static PayrollBatchService payrollBatchService;
	
	@Autowired
	private  EmployeeCompensationService temployeeCompensationService;
	private static EmployeeCompensationService employeeCompensationService;
	
	@PostConstruct
	public void init() {
		employeeCtcService = temployeeCtcService;
		payrollBatchService = tpayrollBatchService;
		employeeCompensationService = temployeeCompensationService;
		employeePayrollService = temployeePayrollService;
		employeeBonusService = temployeeBonusService;
		employeeLopService = temployeeLopService;
		taxSettingsService = ttaxSettingsService;
		deductionSettingsService = tdeductionSettingsService;
		allowanceSettingsService = tallowanceSettingsService;
		employeeDeductionService = temployeeDeductionService;
		employeeTaxService = temployeeTaxService;
		employeeEarningsService = temployeeEarningsService;
		employeeService = temployeeService;
		payrollEarningsDetailsService=tpayrollEarningsDetailsService;
		payrollTaxDetailsService=tpayrollTaxDetailsService;
		payrollDeductionDetailsService=tpayrollDeductionDetailsService;
		
	}
	
	public static JSONObject getempCompensationList(List<EmployeeCtcDO> empCompensationList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeeCtcDO empCompensation : empCompensationList) {
				resultJSONArray.put(getCompensationDetailObject(empCompensation));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getempCompensationListWithEarnings(List<EmployeeCtcDO> empCompensationList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeeCtcDO empCompensation : empCompensationList) {
				resultJSONArray.put(getCompensationDetailObjectWithEarnings(empCompensation));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getempCompensationListWithEarningsDate(List<EmployeeCtcDO> empCompensationList, String month) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeeCtcDO empCompensation : empCompensationList) {
				resultJSONArray.put(getCompensationDetailObjectWithEarningsDate(empCompensation, month));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getempCompensationListWithEarningsDateYtd(List<EmployeeCtcDO> empCompensationList, String month) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeeCtcDO empCompensation : empCompensationList) {
				resultJSONArray.put(getCompensationDetailObjectWithEarningsDateYtd(empCompensation, month));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getCompensationDetailObject(EmployeeCtcDO empCompensation)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(empCompensation.getEmpCtcId()));
		if(empCompensation.getEmployee() != null) result.put(CommonConstants.EMPID, String.valueOf(empCompensation.getEmployee().getEmpId()));
		/*List<EmployeeDO> employeeList = employeeService.retrieveEmpId(empCompensation.getEmpId().toString());
		if(employeeList != null && employeeList.size() > 0){
			result.put(CommonConstants.EMPLOYEEID, String.valueOf(employeeList.get(0).getEmpId() != null ? employeeList.get(0).getEmpId() : ""));
		}else{
			result.put(CommonConstants.EMPLOYEEID, String.valueOf(""));
		}*/
		
		if(empCompensation.getEmpctc() != null){
			result.put(CommonConstants.EMPCTC, String.valueOf(empCompensation.getEmpctc()));
		}else{
			result.put(CommonConstants.EMPCTC, "");
		}
		if(empCompensation.getCtcType() != null){
			result.put(CommonConstants.CTC_TYPE, String.valueOf(empCompensation.getCtcType()));
		}else{
			result.put(CommonConstants.CTC_TYPE, "");
		}
		if(empCompensation.getPayrollBatch() != null){
				result.put(CommonConstants.PAYROLLBATCHNAME, String.valueOf(empCompensation.getPayrollBatch().getDisplayname()));
				result.put(CommonConstants.PAYROLLBATCHID, String.valueOf(empCompensation.getPayrollBatch().getPayrollBatchId()));
		}else{
			result.put(CommonConstants.PAYROLLBATCHID, "");
			result.put(CommonConstants.PAYROLLBATCHNAME, "");
		}
		result.put(CommonConstants.STARTDATE,String.valueOf(empCompensation.getStartdate() != null ? CommonUtil.convertDateToStringWithdatetime(empCompensation.getStartdate()) : ""));
		if(empCompensation.getEnddate() != null ){
			result.put(CommonConstants.ENDDATE,String.valueOf(CommonUtil.convertDateToStringWithdatetime(empCompensation.getEnddate())));
		}else{
			result.put(CommonConstants.ENDDATE, "");
		}
		result.put(CommonConstants.STATUS, String.valueOf(empCompensation.getStatus()));
		if(empCompensation.getEmployee() != null){
			String empName = CommonUtil.getEmpObject(empCompensation.getEmployee().getEmpId());
			result.put(CommonConstants.NAME, empName); 
		}else{
			result.put(CommonConstants.NAME, ""); 
		}
		if(empCompensation.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(empCompensation.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(empCompensation.getUpdatedon())));
		return result;
	}
	
	public static JSONObject getCompensationDetailObjectWithEarnings(EmployeeCtcDO empCompensation)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(empCompensation.getEmpCtcId()));
		result.put(CommonConstants.EMPID, String.valueOf(empCompensation.getEmployee().getEmpId()));
		/*List<EmployeeDO> employeeList = employeeService.retrieveEmpId(empCompensation.getEmpId().toString());
		if(employeeList != null && employeeList.size() > 0){
			result.put(CommonConstants.EMPLOYEEID, String.valueOf(employeeList.get(0).getEmpId() != null ? employeeList.get(0).getEmpId() : ""));
		}else{
			result.put(CommonConstants.EMPLOYEEID, String.valueOf(""));
		}*/
		if(empCompensation.getEmpctc() != null){
			result.put(CommonConstants.EMPCTC, String.valueOf(empCompensation.getEmpctc()));
			result.put(CommonConstants.EMPCTCID, String.valueOf(empCompensation.getEmpCtcId()));
		}else{
			
			result.put(CommonConstants.EMPCTC, "");
			result.put(CommonConstants.EMPCTCID, String.valueOf(""));
		}
		result.put(CommonConstants.STARTDATE,String.valueOf(empCompensation.getStartdate() != null ? CommonUtil.convertDateToStringWithdatetime(empCompensation.getStartdate()) : ""));
		result.put(CommonConstants.ENDDATE,String.valueOf(empCompensation.getEnddate() != null ? CommonUtil.convertDateToStringWithdatetime(empCompensation.getEnddate()) : ""));
		result.put(CommonConstants.STATUS, String.valueOf(empCompensation.getStatus()));
		if(empCompensation.getEmployee() != null){
			String empName = CommonUtil.getEmpObject(empCompensation.getEmployee().getEmpId());
			result.put(CommonConstants.NAME, empName); 
		}else{
			result.put(CommonConstants.NAME, ""); 
		}
		/*if(empCompensation.getEmpId() != null){
			if(employeeList != null && employeeList.size() > 0){
				if(employeeList.get(0).getMiddlename() != null) result.put(CommonConstants.NAME, String.valueOf(employeeList.get(0).getFirstname() + " " +employeeList.get(0).getMiddlename() + " " + employeeList.get(0).getLastname()));
				else result.put(CommonConstants.NAME, String.valueOf(employeeList.get(0).getFirstname() + " " + employeeList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.NAME, "");
			}
		}else{
			result.put(CommonConstants.NAME, "");
		}*/
		if(empCompensation.getCtcType() != null){
			result.put(CommonConstants.CTC_TYPE, String.valueOf(empCompensation.getCtcType()));
		}else{
			result.put(CommonConstants.CTC_TYPE, "");
		}
		if(empCompensation.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(empCompensation.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(empCompensation.getUpdatedon())));
		//i
		//List<EmployeeCompensationDO> compensationList = employeeCompensationService.retrieveByEmpId(empCompensation.getEmployee().getEmpId());
		/*if(compensationList != null && compensationList.size() >0){
			result.put(CommonConstants.EFFECTIVEFROM,String.valueOf(compensationList.get(0).getEffectivefrom() != null ? CommonUtil.convertDateToStringWithdatetime(compensationList.get(0).getEffectivefrom()) : ""));
			result.put(CommonConstants.EFFECTIVETO,String.valueOf(compensationList.get(0).getEffectiveto() != null ? CommonUtil.convertDateToStringWithdatetime(compensationList.get(0).getEffectiveto()) : ""));
			result.put(CommonConstants.NETMONTHLY,String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(compensationList.get(0).getNetmonth())));
			result.put(CommonConstants.NETYEAR,String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(compensationList.get(0).getNetyear())));
			result.put(CommonConstants.EARNINGMONTHLY,String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(compensationList.get(0).getEarningmonth())));
			result.put(CommonConstants.EARNINGYEAR,String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(compensationList.get(0).getEarningyear())));
			result.put(CommonConstants.DEDUCTIONMONTHLY,String.valueOf(compensationList.get(0).getDeductionmonth() != null ? CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(compensationList.get(0).getDeductionmonth())):"0"));
			result.put(CommonConstants.DEDUCTIONYEAR,String.valueOf(compensationList.get(0).getDeductionyear() != null ? CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(compensationList.get(0).getDeductionyear())):"0"));
			result.put(CommonConstants.TAXMONTHLY,String.valueOf(compensationList.get(0).getTaxmonth() != null ? CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(compensationList.get(0).getTaxmonth())):"0"));
			result.put(CommonConstants.TAXYEAR,String.valueOf(compensationList.get(0).getTaxyear() != null ? CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(compensationList.get(0).getTaxyear())):"0"));
			List<EmployeeEarningsDO> earningsList = employeeEarningsService.retrieveByEmpCompensationId(compensationList.get(0).getId());
			List<EmployeeTaxDO> taxList = employeeTaxService.retrieveByEmpCompensationId(compensationList.get(0).getId());
			List<EmployeeDeductionDO> deductionList = employeeDeductionService.retrieveByEmpCompensationId(compensationList.get(0).getId());
			JSONArray resultJSONArray = new JSONArray();
			
			if(earningsList != null && earningsList.size() > 0){
				for (EmployeeEarningsDO empEarnings : earningsList) {
					resultJSONArray.put(getEmployeeEarningsDetailObject(empEarnings, null,null));
				}
				result.put(CommonConstants.EARNINGLIST, resultJSONArray);
			}else{
				result.put(CommonConstants.EARNINGLIST,"");
			}
			JSONArray resultJSONArray1 = new JSONArray();
			
			if(deductionList != null && deductionList.size() > 0){
				for (EmployeeDeductionDO empDeduction : deductionList) {
					resultJSONArray1.put(getEmployeeDeductionDetailObject(empDeduction, null,null));
				}
				result.put(CommonConstants.DEDUCTIONLIST, resultJSONArray1);
			}else{
				result.put(CommonConstants.DEDUCTIONLIST,"");
			}
			
			JSONArray resultJSONArray2 = new JSONArray();
			
			if(taxList != null && taxList.size() > 0){
				for (EmployeeTaxDO empDeduction : taxList) {
					resultJSONArray2.put(getEmployeeTaxDetailObject(empDeduction,  null,null));
				}
				result.put(CommonConstants.TAXLIST, resultJSONArray2);
			}else{
				result.put(CommonConstants.TAXLIST,"");
			}
		}else{
			result.put(CommonConstants.EFFECTIVEFROM,"");
			result.put(CommonConstants.EARNINGLIST,"");
			result.put(CommonConstants.TAXLIST,"");
			result.put(CommonConstants.DEDUCTIONLIST,"");
			result.put(CommonConstants.EFFECTIVETO,"");
			result.put(CommonConstants.NETMONTHLY,String.valueOf("0"));
			result.put(CommonConstants.NETYEAR,String.valueOf("0"));
			result.put(CommonConstants.EARNINGMONTHLY,String.valueOf("0"));
			result.put(CommonConstants.EARNINGYEAR,String.valueOf("0"));
			result.put(CommonConstants.DEDUCTIONMONTHLY,String.valueOf("0"));
			result.put(CommonConstants.DEDUCTIONYEAR,String.valueOf("0"));
			result.put(CommonConstants.TAXMONTHLY,String.valueOf("0"));
			result.put(CommonConstants.TAXYEAR,String.valueOf("0"));
		}
		*/
		return result;
	}
	
	public static JSONObject getCompensationDetailObjectWithEarningsDate(EmployeeCtcDO empCompensation, String month)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(empCompensation.getEmpCtcId()));
		result.put(CommonConstants.EMPID, String.valueOf(empCompensation.getEmployee().getEmpId()));
		/*List<EmployeeDO> employeeList = employeeService.retrieveEmpId(empCompensation.getEmpId().toString());
		if(employeeList != null && employeeList.size() > 0){
			result.put(CommonConstants.EMPLOYEEID, String.valueOf(employeeList.get(0).getEmpId() != null ? employeeList.get(0).getEmpId() : ""));
		}else{
			result.put(CommonConstants.EMPLOYEEID, String.valueOf(""));
		}*/
		List<EmployeeLopDO> employeeLopList = employeeLopService.retrieveByEmpId(empCompensation.getEmployee().getEmpId());
		int noOfDaysLop = 0;
		Double lossOfPayAmount = 0D;
		if(month != null){
			if(employeeLopList != null && employeeLopList.size() > 0){
				Calendar start = Calendar.getInstance();
				start.setTime(employeeLopList.get(0).getStartdate());
				Calendar end = Calendar.getInstance();
				end.setTime(employeeLopList.get(0).getEnddate());
				
				for (Date date = start.getTime(); start.before(end) || start.equals(end); start.add(Calendar.DATE, 1), date = start.getTime()) {
					Calendar weekend = Calendar.getInstance();
					weekend.setTime(date);
					if (/*weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && */
						weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){
						if(month.equalsIgnoreCase(CommonUtil.convertDateToStringWithOutDate(weekend.getTime()))){
							noOfDaysLop++;
						}
					}
				}
			}
		}
		if(month != null){
			if(empCompensation.getEmpctc() != null){
				result.put(CommonConstants.EMPCTC, String.valueOf(empCompensation.getEmpctc()));
				Long ctc = empCompensation.getEmpctc()/12;
				Date s = CommonUtil.convertDateToStringWithOutDate(month);
		        Calendar c=Calendar.getInstance();
		        c.setTime(s);
		        int days = c.getActualMaximum(Calendar.DAY_OF_MONTH);
				Long perDay = ctc/days;
				if(noOfDaysLop > 0){
					lossOfPayAmount = (double) (noOfDaysLop * perDay);
				}
			}else{
				result.put(CommonConstants.EMPCTC, "");
			}
		}
		
		result.put(CommonConstants.STARTDATE,String.valueOf(empCompensation.getStartdate() != null ? CommonUtil.convertDateToStringWithdatetime(empCompensation.getStartdate()) : ""));
		result.put(CommonConstants.ENDDATE,String.valueOf(empCompensation.getEnddate() != null ? CommonUtil.convertDateToStringWithdatetime(empCompensation.getEnddate()) : ""));
		result.put(CommonConstants.STATUS, String.valueOf(empCompensation.getStatus()));
		if(empCompensation.getCtcType() != null){
			result.put(CommonConstants.CTC_TYPE, String.valueOf(empCompensation.getCtcType()));
		}else{
			result.put(CommonConstants.CTC_TYPE, "");
		}
		if(empCompensation.getEmployee().getEmpId() != null){
			String empName = CommonUtil.getEmpObject(empCompensation.getEmployee().getEmpId());
			result.put(CommonConstants.NAME, empName); 
		}else{
			result.put(CommonConstants.NAME, ""); 
		}
		
		
		/*if(empCompensation.getEmployee().getEmpId() != null){
			if(employeeList != null && employeeList.size() > 0){
				if(employeeList.get(0).getMiddlename() != null) result.put(CommonConstants.NAME, String.valueOf(employeeList.get(0).getFirstname() + " " +employeeList.get(0).getMiddlename() + " " + employeeList.get(0).getLastname()));
				else result.put(CommonConstants.NAME, String.valueOf(employeeList.get(0).getFirstname() + " " + employeeList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.NAME, "");
			}
		}else{
			result.put(CommonConstants.NAME, "");
		}*/
		/*if(empCompensation.getUpdatedBy() != null){
			
			List<EmployeeDO> empList = employeeService.retrieveEmpId(empCompensation.getUpdatedBy().toString());
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}*/
		if(empCompensation.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(empCompensation.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(empCompensation.getUpdatedon())));
		Long bonusMonth = 0L;
		Long bonusYear = 0L;
		//i
		/*List<EmployeeCompensationDO> compensationList = employeeCompensationService.retrieveByEmpId(empCompensation.getEmployee().getEmpId());
		if(compensationList != null && compensationList.size() >0){
			if(month != null){
				List<EmployeeBonusDO> employeeBonusList = employeeBonusService.retrieveByEmpIdAndMonth(empCompensation.getEmployee().getEmpId(), month);
				if(employeeBonusList != null && employeeBonusList.size() > 0){
					result.put(CommonConstants.BONUSMONTH,String.valueOf(employeeBonusList.get(0).getAmount()));
					bonusMonth = employeeBonusList.get(0).getAmount();
					String fromMnth = "";
					String toMnth = "";
					String todayMnth = CommonUtil.convertDateToStringWithOutDate(new Date());
					String currentMonth [] = todayMnth.split(" ");
					if(currentMonth[0].equalsIgnoreCase("Jan") || currentMonth[0].equalsIgnoreCase("Feb") || currentMonth[0].equalsIgnoreCase("Mar")){
						String mnthYear [] = todayMnth.split(" ");
						int year = Integer.parseInt(mnthYear[1]) - 1;
						String mnth= "Apr";
						fromMnth = mnth + " "+year;
						toMnth = todayMnth;
					}else{
						fromMnth = todayMnth;
						String mnthYear [] = todayMnth.split(" ");
						int year = Integer.parseInt(mnthYear[1]) - 1;
						String mnth= "Apr";
						toMnth = mnth + " "+year;
					}
					
					bonusYear = employeeBonusService.retrieveByEmpIdBetweenMonth(empCompensation.getEmployee().getEmpId(), fromMnth, toMnth);
					result.put(CommonConstants.BONUSYEAR,String.valueOf(bonusYear!=null?CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(bonusYear)):""));
				}else{
					result.put(CommonConstants.BONUSMONTH,String.valueOf(""));
					result.put(CommonConstants.BONUSYEAR,String.valueOf(""));
				}
			}else{
				result.put(CommonConstants.BONUSMONTH,String.valueOf(""));
				result.put(CommonConstants.BONUSYEAR,String.valueOf(""));
			}
			
			for (EmployeeCompensationDO compensation : compensationList) {
				if(CommonUtil.convertDateToStringWithOutDate(month).after(CommonUtil.convertDateToStringWithOutDate(CommonUtil.convertDateToStringWithOutDate(compensation.getEffectivefrom())))
						|| CommonUtil.convertDateToStringWithOutDate(month).equals(CommonUtil.convertDateToStringWithOutDate(CommonUtil.convertDateToStringWithOutDate(compensation.getEffectivefrom())))){
					if(compensation.getEffectiveto() == null || CommonUtil.convertDateToStringWithOutDate(month).before(CommonUtil.convertDateToStringWithOutDate(CommonUtil.convertDateToStringWithOutDate(compensation.getEffectiveto())))
							|| CommonUtil.convertDateToStringWithOutDate(month).equals(CommonUtil.convertDateToStringWithOutDate(CommonUtil.convertDateToStringWithOutDate(compensation.getEffectiveto())))){
						result.put(CommonConstants.EFFECTIVEFROM,String.valueOf(compensation.getEffectivefrom() != null ? CommonUtil.convertDateToStringWithdatetime(compensation.getEffectivefrom()) : ""));
						result.put(CommonConstants.EFFECTIVETO,String.valueOf(compensation.getEffectiveto() != null ? CommonUtil.convertDateToStringWithdatetime(compensation.getEffectiveto()) : ""));
						result.put(CommonConstants.NETMONTHLY,String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf((compensation.getNetmonth()+ bonusMonth)  - lossOfPayAmount))));
						result.put(CommonConstants.LOSSOFPAYMONTHLY,  String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(lossOfPayAmount))));
						result.put(CommonConstants.EARNINGMONTHLY,String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(compensation.getEarningmonth() + bonusMonth))));
						result.put(CommonConstants.EARNINGYEAR,String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(compensation.getEarningyear() +  bonusYear))));
						result.put(CommonConstants.DEDUCTIONMONTHLY,String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(compensation.getDeductionmonth() + lossOfPayAmount))));
						result.put(CommonConstants.TAXMONTHLY,String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(compensation.getTaxmonth()))));
						result.put(CommonConstants.TAXYEAR,String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(compensation.getTaxyear()))));
						List<EmployeeLopDO> lopList = employeeLopService.retrieveByEmpId(empCompensation.getEmployee().getEmpId());
						Double lossOfPayAmountForEmp = 0D;
						if(lopList != null && lopList.size() > 0){
							String todayMnth = CommonUtil.convertDateToStringWithOutDate(new Date());
							String fromMnth = "";
							String toMnth = "";
							String currentMonth [] = todayMnth.split(" ");
							if(currentMonth[0].equalsIgnoreCase("Jan") || currentMonth[0].equalsIgnoreCase("Feb") || currentMonth[0].equalsIgnoreCase("Mar")){
								String mnthYear [] = todayMnth.split(" ");
								int year = Integer.parseInt(mnthYear[1]) - 1;
								String mnth= "04";
								fromMnth = "30-"+mnth+"-"+year;
								toMnth = todayMnth;
							}else{
								String mnthYear [] = todayMnth.split(" ");
								int year = Integer.parseInt(mnthYear[1]) - 1;
								String mnth= "04";
								fromMnth = "30-"+mnth+"-"+year;
								toMnth = todayMnth;
							}
							for(EmployeeLopDO eLopList : lopList){
								int noOfDaysLopForEmp = 0;
								
								Date fromDate = CommonUtil.convertStringToDate(fromMnth);
								Date toDate = CommonUtil.convertDateToStringWithOutDate(toMnth);
								
								Date lopFromDate = CommonUtil.convertDateToStringWithOutDate(CommonUtil.convertDateToStringWithOutDate(eLopList.getStartdate()));
								Date lopToDate = CommonUtil.convertDateToStringWithOutDate(CommonUtil.convertDateToStringWithOutDate(eLopList.getEnddate()));
								
								if((lopFromDate.after(fromDate) || lopFromDate.equals(fromDate))
										&& lopFromDate.before(toDate) || lopFromDate.equals(toDate)){
									if((lopToDate.after(fromDate) || lopToDate.equals(fromDate))
											&& lopToDate.before(toDate) || lopToDate.equals(toDate)){
										
										Calendar start = Calendar.getInstance();
										start.setTime(eLopList.getStartdate());
										Calendar end = Calendar.getInstance();
										end.setTime(eLopList.getEnddate());
										
										for (Date date = start.getTime(); start.before(end) || start.equals(end); start.add(Calendar.DATE, 1), date = start.getTime()) {
											Calendar weekend = Calendar.getInstance();
											weekend.setTime(date);
											if (weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && 
												weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){
												if(month.equalsIgnoreCase(CommonUtil.convertDateToStringWithOutDate(weekend.getTime()))){
													noOfDaysLopForEmp++;
												}
											}
										}
										
										Long ctc = empCompensation.getEmpctc()/12;
										Date s = CommonUtil.convertDateToStringWithOutDate(month);
								        Calendar c=Calendar.getInstance();
								        c.setTime(s);
								        int days = c.getActualMaximum(Calendar.DAY_OF_MONTH);
										Long perDay = ctc/days;
										if(noOfDaysLopForEmp > 0){
											lossOfPayAmountForEmp += (double) (noOfDaysLop * perDay);
										}
									}
								}
							}
						}else{
							result.put(CommonConstants.LOSSOFPAYYEAR, String.valueOf(""));
						}
						result.put(CommonConstants.LOSSOFPAYYEAR,  String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(lossOfPayAmountForEmp))));
						result.put(CommonConstants.NETYEAR,String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(compensation.getNetyear() - lossOfPayAmountForEmp))));
						result.put(CommonConstants.DEDUCTIONYEAR,String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(compensation.getDeductionyear() + lossOfPayAmountForEmp))));
						List<EmployeeEarningsDO> earningsList = employeeEarningsService.retrieveByEmpCompensationId(compensation.getId());
						List<EmployeeTaxDO> taxList = employeeTaxService.retrieveByEmpCompensationId(compensation.getId());
						List<EmployeeDeductionDO> deductionList = employeeDeductionService.retrieveByEmpCompensationId(compensation.getId());
						JSONArray resultJSONArray = new JSONArray();
						
						if(earningsList != null && earningsList.size() > 0){
							for (EmployeeEarningsDO empEarnings : earningsList) {
								resultJSONArray.put(getEmployeeEarningsDetailObject(empEarnings, null,month));
							}
							result.put(CommonConstants.EARNINGLIST, resultJSONArray);
						}else{
							result.put(CommonConstants.EARNINGLIST,"");
						}
						JSONArray resultJSONArray1 = new JSONArray();
						
						if(deductionList != null && deductionList.size() > 0){
							for (EmployeeDeductionDO empDeduction : deductionList) {
								resultJSONArray1.put(getEmployeeDeductionDetailObject(empDeduction, null,month));
							}
							result.put(CommonConstants.DEDUCTIONLIST, resultJSONArray1);
						}else{
							result.put(CommonConstants.DEDUCTIONLIST,"");
						}
						
						JSONArray resultJSONArray2 = new JSONArray();
						
						if(taxList != null && taxList.size() > 0){
							for (EmployeeTaxDO empDeduction : taxList) {
								resultJSONArray2.put(getEmployeeTaxDetailObject(empDeduction, null, month));
							}
							result.put(CommonConstants.TAXLIST, resultJSONArray2);
						}else{
							result.put(CommonConstants.TAXLIST,"");
						}
					}
				}
			}
		}else{
			result.put(CommonConstants.EFFECTIVEFROM,"");
			result.put(CommonConstants.EARNINGLIST,"");
			result.put(CommonConstants.TAXLIST,"");
			result.put(CommonConstants.DEDUCTIONLIST,"");
			result.put(CommonConstants.EFFECTIVETO,"");
			result.put(CommonConstants.NETMONTHLY,String.valueOf("0"));
			result.put(CommonConstants.NETYEAR,String.valueOf("0"));
			result.put(CommonConstants.EARNINGMONTHLY,String.valueOf("0"));
			result.put(CommonConstants.EARNINGYEAR,String.valueOf("0"));
			result.put(CommonConstants.DEDUCTIONMONTHLY,String.valueOf("0"));
			result.put(CommonConstants.DEDUCTIONYEAR,String.valueOf("0"));
			result.put(CommonConstants.TAXMONTHLY,String.valueOf("0"));
			result.put(CommonConstants.TAXYEAR,String.valueOf("0"));
		}
		*/
		return result;
	}
	
	public static JSONObject getCompensationDetailObjectWithEarningsDateYtd(EmployeeCtcDO empCompensation, String month)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(empCompensation.getEmpCtcId()));
		result.put(CommonConstants.EMPID, String.valueOf(empCompensation.getEmployee().getEmpId()));
		/*List<EmployeeDO> employeeList = employeeService.retrieveEmpId(empCompensation.getEmpId().toString());
		if(employeeList != null && employeeList.size() > 0){
			result.put(CommonConstants.EMPLOYEEID, String.valueOf(employeeList.get(0).getEmpId() != null ? employeeList.get(0).getEmpId() : ""));
		}else{
			result.put(CommonConstants.EMPLOYEEID, String.valueOf(""));
		}*/
		List<EmployeeLopDO> employeeLopList = employeeLopService.retrieveByEmpId(empCompensation.getEmployee().getEmpId());
		int noOfDaysLop = 0;
		Double lossOfPayAmount = 0D;
		if(month != null){
			if(employeeLopList != null && employeeLopList.size() > 0){
				Calendar start = Calendar.getInstance();
				start.setTime(employeeLopList.get(0).getStartdate());
				Calendar end = Calendar.getInstance();
				end.setTime(employeeLopList.get(0).getEnddate());
				
				for (Date date = start.getTime(); start.before(end) || start.equals(end); start.add(Calendar.DATE, 1), date = start.getTime()) {
					Calendar weekend = Calendar.getInstance();
					weekend.setTime(date);
					if (/*weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && */
						weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){
						if(month.equalsIgnoreCase(CommonUtil.convertDateToStringWithOutDate(weekend.getTime()))){
							noOfDaysLop++;
						}
					}
				}
			}
		}
		if(month != null){
			if(empCompensation.getEmpctc() != null){
				result.put(CommonConstants.EMPCTC, String.valueOf(empCompensation.getEmpctc()));
				Long ctc = empCompensation.getEmpctc()/12;
				Date s = CommonUtil.convertDateToStringWithOutDate(month);
		        Calendar c=Calendar.getInstance();
		        c.setTime(s);
		        int days = c.getActualMaximum(Calendar.DAY_OF_MONTH);
				Long perDay = ctc/days;
				if(noOfDaysLop > 0){
					lossOfPayAmount = (double) (noOfDaysLop * perDay);
				}
			}else{
				result.put(CommonConstants.EMPCTC, "");
			}
		}
		
		result.put(CommonConstants.STARTDATE,String.valueOf(empCompensation.getStartdate() != null ? CommonUtil.convertDateToStringWithdatetime(empCompensation.getStartdate()) : ""));
		result.put(CommonConstants.ENDDATE,String.valueOf(empCompensation.getEnddate() != null ? CommonUtil.convertDateToStringWithdatetime(empCompensation.getEnddate()) : ""));
		result.put(CommonConstants.STATUS, String.valueOf(empCompensation.getStatus()));
		if(empCompensation.getCtcType() != null){
			result.put(CommonConstants.CTC_TYPE, String.valueOf(empCompensation.getCtcType()));
		}else{
			result.put(CommonConstants.CTC_TYPE, "");
		}
		if(empCompensation.getEmployee().getEmpId() != null){
			String empName = CommonUtil.getEmpObject(empCompensation.getEmployee().getEmpId());
			result.put(CommonConstants.NAME, empName); 
		}else{
			result.put(CommonConstants.NAME, ""); 
		}
		
		
		
		/*if(empCompensation.getEmpId() != null){
			if(employeeList != null && employeeList.size() > 0){
				if(employeeList.get(0).getMiddlename() != null) result.put(CommonConstants.NAME, String.valueOf(employeeList.get(0).getFirstname() + " " +employeeList.get(0).getMiddlename() + " " + employeeList.get(0).getLastname()));
				else result.put(CommonConstants.NAME, String.valueOf(employeeList.get(0).getFirstname() + " " + employeeList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.NAME, "");
			}
		}else{
			result.put(CommonConstants.NAME, "");
		}*/
		
		if(empCompensation.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(empCompensation.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		
		
		/*if(empCompensation.getUpdatedBy() != null){
			
			List<EmployeeDO> empList = employeeService.retrieveEmpId(empCompensation.getUpdatedBy().toString());
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}*/
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(empCompensation.getUpdatedon())));
		Long bonusMonth = 0L;
		Long bonusYear = 0L;
		//i
		/*List<EmployeeCompensationDO> compensationList = employeeCompensationService.retrieveByEmpId(empCompensation.getEmployee().getEmpId());
		String fromMnth = "";
		String toMnth = "";
		if(compensationList != null && compensationList.size() >0){
			if(month != null){
				List<EmployeeBonusDO> employeeBonusList = employeeBonusService.retrieveByEmpIdAndMonth(empCompensation.getEmployee().getEmpId(), month);
				if(employeeBonusList != null && employeeBonusList.size() > 0){
					result.put(CommonConstants.BONUSMONTH,String.valueOf(employeeBonusList.get(0).getAmount()));
					bonusMonth = employeeBonusList.get(0).getAmount();
				}else{
					result.put(CommonConstants.BONUSMONTH,String.valueOf(""));
				}
				String todayMnth = CommonUtil.convertDateToStringWithOutDate(new Date());
				String currentMonth [] = todayMnth.split(" ");
				if(currentMonth[0].equalsIgnoreCase("Jan") || currentMonth[0].equalsIgnoreCase("Feb") || currentMonth[0].equalsIgnoreCase("Mar")){
					String mnthYear [] = todayMnth.split(" ");
					int year = Integer.parseInt(mnthYear[1]) - 1;
					String mnth= "Apr";
					fromMnth = mnth + " "+year;
					toMnth = todayMnth;
				}else{
					fromMnth = todayMnth;
					String mnthYear [] = todayMnth.split(" ");
					int year = Integer.parseInt(mnthYear[1]) - 1;
					String mnth= "Apr";
					toMnth = mnth + " "+year;
				}
				
				bonusYear = employeeBonusService.retrieveByEmpIdBetweenMonth(empCompensation.getEmployee().getEmpId(), fromMnth, toMnth);
				result.put(CommonConstants.BONUSYEAR,String.valueOf(bonusYear!=null?CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(bonusYear)):""));
			}else{
				result.put(CommonConstants.BONUSMONTH,String.valueOf(""));
				result.put(CommonConstants.BONUSYEAR,String.valueOf(""));
			}
			
			if(bonusYear == null){
				bonusYear = 0L;
			}
			
			for (EmployeeCompensationDO compensation : compensationList) {
				if(CommonUtil.convertDateToStringWithOutDate(month).after(CommonUtil.convertDateToStringWithOutDate(CommonUtil.convertDateToStringWithOutDate(compensation.getEffectivefrom())))
						|| CommonUtil.convertDateToStringWithOutDate(month).equals(CommonUtil.convertDateToStringWithOutDate(CommonUtil.convertDateToStringWithOutDate(compensation.getEffectivefrom())))){
					if(compensation.getEffectiveto() == null || CommonUtil.convertDateToStringWithOutDate(month).before(CommonUtil.convertDateToStringWithOutDate(CommonUtil.convertDateToStringWithOutDate(compensation.getEffectiveto())))
							|| CommonUtil.convertDateToStringWithOutDate(month).equals(CommonUtil.convertDateToStringWithOutDate(CommonUtil.convertDateToStringWithOutDate(compensation.getEffectiveto())))){
						
						List<EmployeeDO> employee = employeeService.retrieveEmpId(compensation.getEmpId().toString());
						if(employee != null && employee.size() > 0){
							if(employee.get(0).getStartdate() != null){
								String splitMonth = employee.get(0).getStartdate().substring(0, 10);
								String joiningMonth = CommonUtil.convertDateToStringWithOutDate(CommonUtil.convertStringToSqlDate(splitMonth));
								//String[] joinMonthArray = joiningMonth.split(",");
								if(joiningMonth.equalsIgnoreCase(month)){
									Date joiningDate = CommonUtil.convertStringToSqlDate(employee.get(0).getStartdate());
									Calendar c=Calendar.getInstance();
							        c.setTime(joiningDate);
							        int days = c.getActualMaximum(Calendar.DAY_OF_MONTH);
							        String[] noofdays = splitMonth.split("-");
							        int totalDays = days;
							        if(!noofdays[2].equals("01")){
							        	 totalDays = days - Integer.parseInt(noofdays[2]);
							        }
							        Double perdaySalary = compensation.getEarningmonth()/days;
							        result.put(CommonConstants.EARNINGMONTHLY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf((perdaySalary * totalDays) + bonusMonth))));
							        Double perdayTax = 0D;
							        if(compensation.getTaxmonth() != null && compensation.getTaxmonth() > 0){
							        	perdayTax = compensation.getTaxmonth()/days;
							        	result.put(CommonConstants.TAXMONTHLY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(perdayTax * totalDays))));
							        }else{
							        	result.put(CommonConstants.TAXMONTHLY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(perdayTax * totalDays))));
							        }
							        Double perdayDeduction = 0D;
							        if(compensation.getDeductionmonth() != null && compensation.getDeductionmonth() > 0){
							        	perdayDeduction = compensation.getDeductionmonth()/days;
							        	result.put(CommonConstants.DEDUCTIONMONTHLY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf((perdayDeduction * totalDays) + lossOfPayAmount))));
							        }else{
							        	result.put(CommonConstants.DEDUCTIONMONTHLY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf((perdayDeduction * totalDays) + lossOfPayAmount))));
							        }
							        Double netmonthly = 0D;
							        netmonthly = (Double.parseDouble(result.get(CommonConstants.EARNINGMONTHLY).toString()) -  (Double.parseDouble(result.get(CommonConstants.TAXMONTHLY).toString())
							        		+ Double.parseDouble(result.get(CommonConstants.DEDUCTIONMONTHLY).toString())));
							        result.put(CommonConstants.NETMONTHLY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(netmonthly))));
									
								}else{
									result.put(CommonConstants.EARNINGMONTHLY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(compensation.getEarningmonth() + bonusMonth))));
									result.put(CommonConstants.TAXMONTHLY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(compensation.getTaxmonth()))));
									result.put(CommonConstants.DEDUCTIONMONTHLY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(compensation.getDeductionmonth() + lossOfPayAmount))));
									Double netmonthly = 0D;
							        netmonthly = Double.parseDouble(result.get(CommonConstants.EARNINGMONTHLY).toString()) - (Double.parseDouble(result.get(CommonConstants.TAXMONTHLY).toString())
							        		+ Double.parseDouble(result.get(CommonConstants.DEDUCTIONMONTHLY).toString()));
							        result.put(CommonConstants.NETMONTHLY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(netmonthly))));
								
								}
							}
						}
						
						result.put(CommonConstants.EFFECTIVEFROM,String.valueOf(compensation.getEffectivefrom() != null ? CommonUtil.convertDateToStringWithdatetime(compensation.getEffectivefrom()) : ""));
						result.put(CommonConstants.EFFECTIVETO,String.valueOf(compensation.getEffectiveto() != null ? CommonUtil.convertDateToStringWithdatetime(compensation.getEffectiveto()) : ""));
						//result.put(CommonConstants.NETMONTHLY,String.valueOf((compensation.getNetmonth()+ bonusMonth)  - lossOfPayAmount));
						result.put(CommonConstants.LOSSOFPAYMONTHLY,  String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(lossOfPayAmount))));
						//result.put(CommonConstants.EARNINGMONTHLY,String.valueOf(compensation.getEarningmonth() + bonusMonth));
						//result.put(CommonConstants.DEDUCTIONMONTHLY,String.valueOf(compensation.getDeductionmonth() + lossOfPayAmount));
						//result.put(CommonConstants.TAXMONTHLY,String.valueOf(compensation.getTaxmonth()));
						
						List<EmployeeLopDO> lopList = employeeLopService.retrieveByEmpId(empCompensation.getEmployee().getEmpId());
						Double lossOfPayAmountForEmp = 0D;
						if(lopList != null && lopList.size() > 0){
							String todayMnth = CommonUtil.convertDateToStringWithOutDate(new Date());
							fromMnth = "";
							toMnth = "";
							String currentMonth [] = todayMnth.split(" ");
							if(currentMonth[0].equalsIgnoreCase("Jan") || currentMonth[0].equalsIgnoreCase("Feb") || currentMonth[0].equalsIgnoreCase("Mar")){
								String mnthYear [] = todayMnth.split(" ");
								int year = Integer.parseInt(mnthYear[1]) - 1;
								String mnth= "04";
								fromMnth = "30-"+mnth+"-"+year;
								toMnth = todayMnth;
							}else{
								String mnthYear [] = todayMnth.split(" ");
								int year = Integer.parseInt(mnthYear[1]) - 1;
								String mnth= "04";
								fromMnth = "30-"+mnth+"-"+year;
								toMnth = todayMnth;
							}
							for(EmployeeLopDO eLopList : lopList){
								int noOfDaysLopForEmp = 0;
								
								Date fromDate = CommonUtil.convertStringToDate(fromMnth);
								Date toDate = CommonUtil.convertDateToStringWithOutDate(toMnth);
								
								Date lopFromDate = CommonUtil.convertDateToStringWithOutDate(CommonUtil.convertDateToStringWithOutDate(eLopList.getStartdate()));
								Date lopToDate = CommonUtil.convertDateToStringWithOutDate(CommonUtil.convertDateToStringWithOutDate(eLopList.getEnddate()));
								
								if((lopFromDate.after(fromDate) || lopFromDate.equals(fromDate))
										&& lopFromDate.before(toDate) || lopFromDate.equals(toDate)){
									if((lopToDate.after(fromDate) || lopToDate.equals(fromDate))
											&& lopToDate.before(toDate) || lopToDate.equals(toDate)){
										
										Calendar start = Calendar.getInstance();
										start.setTime(eLopList.getStartdate());
										Calendar end = Calendar.getInstance();
										end.setTime(eLopList.getEnddate());
										
										for (Date date = start.getTime(); start.before(end) || start.equals(end); start.add(Calendar.DATE, 1), date = start.getTime()) {
											Calendar weekend = Calendar.getInstance();
											weekend.setTime(date);
											if (weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && 
												weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){
												if(month.equalsIgnoreCase(CommonUtil.convertDateToStringWithOutDate(weekend.getTime()))){
													noOfDaysLopForEmp++;
												}
											}
										}
										
										Long ctc = empCompensation.getEmpctc()/12;
										Date s = CommonUtil.convertDateToStringWithOutDate(month);
								        Calendar c=Calendar.getInstance();
								        c.setTime(s);
								        int days = c.getActualMaximum(Calendar.DAY_OF_MONTH);
										Long perDay = ctc/days;
										if(noOfDaysLopForEmp > 0){
											lossOfPayAmountForEmp += (double) (noOfDaysLop * perDay);
										}
									}
								}
							}
						}else{
							result.put(CommonConstants.LOSSOFPAYYEAR,  String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(lossOfPayAmountForEmp))));
						}
						result.put(CommonConstants.LOSSOFPAYYEAR, String.valueOf(CommonUtil.convertfloatValueWithTwoDecimalPoints(lossOfPayAmountForEmp)));
						Object[] earningYear = employeePayrollService.retrieveByEmpIdBetweenDateSum(empCompensation.getEmployee().getEmpId(), fromMnth, toMnth);
						if(earningYear != null){
							if(earningYear[1] != null){
								result.put(CommonConstants.EARNINGYEAR,String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(compensation.getEarningmonth()+Double.parseDouble(earningYear[1].toString()) +  bonusYear))));
							}else{
								result.put(CommonConstants.EARNINGYEAR,String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.parseDouble(result.get(CommonConstants.EARNINGMONTHLY).toString()) +  bonusYear)));
							}
							
							if(earningYear[2] != null){
								result.put(CommonConstants.DEDUCTIONYEAR,String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.parseDouble(result.get(CommonConstants.DEDUCTIONMONTHLY).toString())+Double.parseDouble(earningYear[2].toString()) +  lossOfPayAmountForEmp)));
							}else{
								result.put(CommonConstants.DEDUCTIONYEAR,String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.parseDouble(result.get(CommonConstants.DEDUCTIONMONTHLY).toString()) + lossOfPayAmountForEmp)));
							}
							
							if(earningYear[3] != null){
								result.put(CommonConstants.TAXYEAR,String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.parseDouble(result.get(CommonConstants.TAXMONTHLY).toString())+Double.parseDouble(earningYear[3].toString()))));
							}else{
								result.put(CommonConstants.TAXYEAR,String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.parseDouble(result.get(CommonConstants.TAXMONTHLY).toString()))));
							}
							Double netmonthly = 0D;
					        netmonthly = Double.parseDouble(result.get(CommonConstants.EARNINGYEAR).toString()) - (Double.parseDouble(result.get(CommonConstants.TAXYEAR).toString())
					        		+ Double.parseDouble(result.get(CommonConstants.DEDUCTIONYEAR).toString()));
							result.put(CommonConstants.NETYEAR,String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(netmonthly)));
							
						}else{
							result.put(CommonConstants.EARNINGYEAR,String.valueOf(""));
							result.put(CommonConstants.DEDUCTIONYEAR,String.valueOf(""));
							result.put(CommonConstants.TAXYEAR,String.valueOf(""));
							result.put(CommonConstants.NETYEAR,String.valueOf(""));
						}
						
						List<EmployeeEarningsDO> earningsList = employeeEarningsService.retrieveByEmpCompensationId(compensation.getId());
						List<EmployeeTaxDO> taxList = employeeTaxService.retrieveByEmpCompensationId(compensation.getId());
						List<EmployeeDeductionDO> deductionList = employeeDeductionService.retrieveByEmpCompensationId(compensation.getId());
						JSONArray resultJSONArray = new JSONArray();
						
						if(earningsList != null && earningsList.size() > 0){
							for (EmployeeEarningsDO empEarnings : earningsList) {
								resultJSONArray.put(getEmployeeEarningsDetailObject(empEarnings, compensation.getEmpId(),month));
							}
							result.put(CommonConstants.EARNINGLIST, resultJSONArray);
						}else{
							result.put(CommonConstants.EARNINGLIST,"");
						}
						JSONArray resultJSONArray1 = new JSONArray();
						
						if(deductionList != null && deductionList.size() > 0){
							for (EmployeeDeductionDO empDeduction : deductionList) {
								resultJSONArray1.put(getEmployeeDeductionDetailObject(empDeduction, compensation.getEmpId(),month));
							}
							result.put(CommonConstants.DEDUCTIONLIST, resultJSONArray1);
						}else{
							result.put(CommonConstants.DEDUCTIONLIST,"");
						}
						
						JSONArray resultJSONArray2 = new JSONArray();
						
						if(taxList != null && taxList.size() > 0){
							for (EmployeeTaxDO empDeduction : taxList) {
								resultJSONArray2.put(getEmployeeTaxDetailObject(empDeduction, compensation.getEmpId(),month));
							}
							result.put(CommonConstants.TAXLIST, resultJSONArray2);
						}else{
							result.put(CommonConstants.TAXLIST,"");
						}
					}
				}
			}
		}else{
			result.put(CommonConstants.EFFECTIVEFROM,"");
			result.put(CommonConstants.EARNINGLIST,"");
			result.put(CommonConstants.TAXLIST,"");
			result.put(CommonConstants.DEDUCTIONLIST,"");
			result.put(CommonConstants.EFFECTIVETO,"");
			result.put(CommonConstants.NETMONTHLY,String.valueOf("0"));
			result.put(CommonConstants.NETYEAR,String.valueOf("0"));
			result.put(CommonConstants.EARNINGMONTHLY,String.valueOf("0"));
			result.put(CommonConstants.EARNINGYEAR,String.valueOf("0"));
			result.put(CommonConstants.DEDUCTIONMONTHLY,String.valueOf("0"));
			result.put(CommonConstants.DEDUCTIONYEAR,String.valueOf("0"));
			result.put(CommonConstants.TAXMONTHLY,String.valueOf("0"));
			result.put(CommonConstants.TAXYEAR,String.valueOf("0"));
		}
		*/
		return result;
	}
	
	public static JSONObject getEmployeeEarningsDetailObject(EmployeeEarningsDO empCompensation, Long empId, String month)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.EARNINGID, String.valueOf(empCompensation.getEarningsId()));
		List<AllowanceSettingDO> allowanceList = allowanceSettingsService.retrieveById(empCompensation.getEarningsId());
		if(allowanceList != null && allowanceList.size() > 0){
			result.put(CommonConstants.EARNINGNAME, String.valueOf(allowanceList.get(0).getDisplayname()));
		}else{
			result.put(CommonConstants.EARNINGNAME, "");
		}
		Long bonusMonth = 0L;
		Long bonusYear = 0L;
		String fromMnth = "";
		String toMnth = "";
		if(month != null && empId != null){
			List<EmployeeDO> employee = employeeService.retrieveEmpId(empId.toString());
			if(employee != null && employee.size() > 0){
				if(employee.get(0).getStartdate() != null){
					
					List<EmployeeBonusDO> employeeBonusList = employeeBonusService.retrieveByEmpIdAndMonth(empId.toString(), month);
					if(employeeBonusList != null && employeeBonusList.size() > 0){
						result.put(CommonConstants.BONUSMONTH,String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(employeeBonusList.get(0).getAmount().doubleValue())));
						bonusMonth = employeeBonusList.get(0).getAmount();
					}else{
						result.put(CommonConstants.BONUSMONTH,String.valueOf(""));
					}
					String todayMnth = CommonUtil.convertDateToStringWithOutDate(new Date());
					String currentMonth [] = todayMnth.split(" ");
					if(currentMonth[0].equalsIgnoreCase("Jan") || currentMonth[0].equalsIgnoreCase("Feb") || currentMonth[0].equalsIgnoreCase("Mar")){
						String mnthYear [] = todayMnth.split(" ");
						int year = Integer.parseInt(mnthYear[1]) - 1;
						String mnth= "Apr";
						fromMnth = mnth + " "+year;
						toMnth = todayMnth;
					}else{
						fromMnth = todayMnth;
						String mnthYear [] = todayMnth.split(" ");
						int year = Integer.parseInt(mnthYear[1]) - 1;
						String mnth= "Apr";
						toMnth = mnth + " "+year;
					}
					
					bonusYear = employeeBonusService.retrieveByEmpIdBetweenMonth(empId.toString(), fromMnth, toMnth);
					if(bonusYear != null){
						result.put(CommonConstants.BONUSYEAR,String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(bonusYear.doubleValue())));
					}else{
						result.put(CommonConstants.BONUSYEAR,String.valueOf("0.00"));
					}
					
					
					String splitMonth = employee.get(0).getStartdate().substring(0, 10);
					String joiningMonth = CommonUtil.convertDateToStringWithOutDate(CommonUtil.convertStringToSqlDate(splitMonth));
					//String[] joinMonthArray = joiningMonth.split(",");
					 Double perdaySalary = 0D;
					 int totalDays = 0;
					if(joiningMonth.equalsIgnoreCase(month)){
						Date joiningDate = CommonUtil.convertStringToSqlDate(employee.get(0).getStartdate());
						Calendar c=Calendar.getInstance();
				        c.setTime(joiningDate);
				        int days = c.getActualMaximum(Calendar.DAY_OF_MONTH);
				        String[] noofdays = splitMonth.split("-");
				        totalDays = days;
				        if(!noofdays[2].equals("01")){
				        	 totalDays = days - Integer.parseInt(noofdays[2]);
				        }
				        perdaySalary = empCompensation.getMonthly()/days;
				        result.put(CommonConstants.EARNINGMONTHLY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(perdaySalary * totalDays) + bonusMonth));
					}else{
						result.put(CommonConstants.EARNINGMONTHLY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(empCompensation.getMonthly()+ bonusMonth)));
					}
					
				}else{
					result.put(CommonConstants.EARNINGMONTHLY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(empCompensation.getMonthly())));
				}
			}
		}
		
		//System.out.println("result.get(CommonConstants.EARNINGMONTHLY)"+result.get(CommonConstants.EARNINGMONTHLY));
		//System.out.println(!result.has(CommonConstants.EARNINGMONTHLY));
		// result.put(CommonConstants.MONTHLY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(empCompensation.getMonthly())));
		
		if(result.has(CommonConstants.EARNINGMONTHLY)) result.put(CommonConstants.MONTHLY, String.valueOf(result.get(CommonConstants.EARNINGMONTHLY).toString()));
		else  result.put(CommonConstants.MONTHLY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(empCompensation.getMonthly())));
		if(empId != null){
			fromMnth = "";
			toMnth = "";
			String todayMnth = CommonUtil.convertDateToStringWithOutDate(new Date());
			String currentMonth [] = todayMnth.split(" ");
			if(currentMonth[0].equalsIgnoreCase("Jan") || currentMonth[0].equalsIgnoreCase("Feb") || currentMonth[0].equalsIgnoreCase("Mar")){
				String mnthYear [] = todayMnth.split(" ");
				int year = Integer.parseInt(mnthYear[1]) - 1;
				String mnth= "Apr";
				fromMnth = mnth + " "+year;
				toMnth = todayMnth;
			}else{
				fromMnth = todayMnth;
				String mnthYear [] = todayMnth.split(" ");
				int year = Integer.parseInt(mnthYear[1]) - 1;
				String mnth= "Apr";
				toMnth = mnth + " "+year;
			}
		//	List<EmployeePayrollDO> earningYear = employeePayrollService.retrieveByEmpIdBetweenDate(empId, fromMnth, toMnth);
			
		/*	if(earningYear != null && earningYear.size() > 0){
				List<Long> ids = new ArrayList<Long>();
				for (EmployeePayrollDO employeePayrollDO : earningYear) {
					ids.add(employeePayrollDO.getId());
				}
				Double ytd = 0D;
				List<PayrollEarningsDetailsDO> payrollEarnings = payrollEarningsDetailsService.retrieveByEmpCompensationIds(ids);
				if(payrollEarnings != null && payrollEarnings.size() > 0){
					for (PayrollEarningsDetailsDO payrollEarningsDetailsDO : payrollEarnings) {
						if(payrollEarningsDetailsDO.getEarningsId().equals(empCompensation.getEarningsId())){
							ytd = ytd+payrollEarningsDetailsDO.getMonthly();
						}
					}
				}
				if(result.has(CommonConstants.EARNINGMONTHLY))	result.put(CommonConstants.YTD, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(ytd + Double.parseDouble(result.get(CommonConstants.EARNINGMONTHLY).toString()))));
				else result.put(CommonConstants.YTD, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(ytd )));
			}else{
				if(result.has(CommonConstants.EARNINGMONTHLY)) result.put(CommonConstants.YTD, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.parseDouble(result.get(CommonConstants.MONTHLY).toString()))));
				else result.put(CommonConstants.YTD, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.parseDouble(result.get(CommonConstants.MONTHLY).toString()))));
			}*/
		}else{
			if(result.has(CommonConstants.EARNINGMONTHLY)) result.put(CommonConstants.YTD, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.parseDouble(result.get(CommonConstants.EARNINGMONTHLY).toString()))));
			else result.put(CommonConstants.YTD, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.parseDouble(result.get(CommonConstants.MONTHLY).toString()))));
		}
		
		
		return result;
	}
	
	public static JSONObject getEmployeeDeductionDetailObject(EmployeeDeductionDO empCompensation, Long empId, String month)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.DEDUCTIONID, String.valueOf(empCompensation.getEmpdeductionid()));
		List<DeductionSettingDO> deductionList = deductionSettingsService.retrieveById(empCompensation.getEmpdeductionid());
		if(deductionList != null && deductionList.size() > 0){
			result.put(CommonConstants.DEDUCTIONNAME, String.valueOf(deductionList.get(0).getDisplayname()));
		}else{
			result.put(CommonConstants.DEDUCTIONNAME, "");
		}
		
		String fromMnth = "";
		String toMnth = "";
		if(month != null && empId != null){
			List<EmployeeDO> employee = employeeService.retrieveEmpId(empId.toString());
			if(employee != null && employee.size() > 0){
				if(employee.get(0).getStartdate() != null){
					String splitMonth = employee.get(0).getStartdate().substring(0, 10);
					String joiningMonth = CommonUtil.convertDateToStringWithOutDate(CommonUtil.convertStringToSqlDate(splitMonth));
					//String[] joinMonthArray = joiningMonth.split(",");
					if(joiningMonth.equalsIgnoreCase(month)){
						Date joiningDate = CommonUtil.convertStringToSqlDate(employee.get(0).getStartdate());
						Calendar c=Calendar.getInstance();
				        c.setTime(joiningDate);
				        int days = c.getActualMaximum(Calendar.DAY_OF_MONTH);
				        String[] noofdays = splitMonth.split("-");
				        int totalDays = days;
				        if(!noofdays[2].equals("01")){
				        	 totalDays = days - Integer.parseInt(noofdays[2]);
				        }
				        Double perdaySalary = empCompensation.getMonthly()/days;
				        Long noOfDaysLop = 0L;
				        Double lossOfPayAmount = 0D;
						if(month != null){
							List<EmployeeCtcDO> ctcList = employeeCtcService.retrieveActive(empId.toString());
							if(ctcList != null && ctcList.size() > 0){
								result.put(CommonConstants.EMPCTC, String.valueOf(ctcList.get(0).getEmpctc()));
								Long ctc = ctcList.get(0).getEmpctc()/12;
								Date s = CommonUtil.convertDateToStringWithOutDate(month);
						        Calendar c1=Calendar.getInstance();
						        c1.setTime(s);
						        int days1 = c1.getActualMaximum(Calendar.DAY_OF_MONTH);
								Long perDay = ctc/days1;
								if(noOfDaysLop > 0){
									lossOfPayAmount = (double) (noOfDaysLop * perDay);
								}
								result.put(CommonConstants.BACSICPAYLABEL, String.valueOf(ctc/2));
							}else{
								result.put(CommonConstants.EMPCTC, "");
								result.put(CommonConstants.BACSICPAYLABEL, "");
							}
						}
				        result.put(CommonConstants.DEDUCTIONMONTHLY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(perdaySalary * totalDays) + lossOfPayAmount));
					}else{
						Long noOfDaysLop = 0L;
				        Double lossOfPayAmount = 0D;
						if(month != null){
							List<EmployeeCtcDO> ctcList = employeeCtcService.retrieveActive(empId.toString());
							if(ctcList != null && ctcList.size() > 0){
								result.put(CommonConstants.EMPCTC, String.valueOf(ctcList.get(0).getEmpctc()));
								Long ctc = ctcList.get(0).getEmpctc()/12;
								Date s = CommonUtil.convertDateToStringWithOutDate(month);
						        Calendar c1=Calendar.getInstance();
						        c1.setTime(s);
						        int days1 = c1.getActualMaximum(Calendar.DAY_OF_MONTH);
								Long perDay = ctc/days1;
								if(noOfDaysLop > 0){
									lossOfPayAmount = (double) (noOfDaysLop * perDay);
								}
								result.put(CommonConstants.BACSICPAYLABEL, String.valueOf(ctc/2));
							}else{
								result.put(CommonConstants.EMPCTC, "");
								result.put(CommonConstants.BACSICPAYLABEL, "");
							}
						}
						result.put(CommonConstants.DEDUCTIONMONTHLY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(empCompensation.getMonthly() + lossOfPayAmount)));
					}
				}else{
					result.put(CommonConstants.DEDUCTIONMONTHLY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(empCompensation.getMonthly())));
				}
			}
		}
		
		result.put(CommonConstants.MONTHLY, String.valueOf(result.get(CommonConstants.DEDUCTIONMONTHLY).toString()));
		if(empId != null){
			fromMnth = "";
			toMnth = "";
			String todayMnth = CommonUtil.convertDateToStringWithOutDate(new Date());
			String currentMonth [] = todayMnth.split(" ");
			if(currentMonth[0].equalsIgnoreCase("Jan") || currentMonth[0].equalsIgnoreCase("Feb") || currentMonth[0].equalsIgnoreCase("Mar")){
				String mnthYear [] = todayMnth.split(" ");
				int year = Integer.parseInt(mnthYear[1]) - 1;
				String mnth= "Apr";
				fromMnth = mnth + " "+year;
				toMnth = todayMnth;
			}else{
				fromMnth = todayMnth;
				String mnthYear [] = todayMnth.split(" ");
				int year = Integer.parseInt(mnthYear[1]) - 1;
				String mnth= "Apr";
				toMnth = mnth + " "+year;
			}
			//List<EmployeePayrollDO> earningYear = employeePayrollService.retrieveByEmpIdBetweenDate(empId, fromMnth, toMnth);
			
			/*if(earningYear != null  && earningYear.size() > 0){
				List<Long> ids = new ArrayList<Long>();
				for (EmployeePayrollDO employeePayrollDO : earningYear) {
					ids.add(employeePayrollDO.getId());
				}
				Double ytd = 0D;
				List<PayrollDeductionDetailsDO> payrollDeductionList = payrollDeductionDetailsService.retrieveByEmpCompensationIds(ids);
				if(payrollDeductionList !=null && payrollDeductionList.size() > 0){
					for (PayrollDeductionDetailsDO payrollDeductionDetailsDO : payrollDeductionList) {
						if(payrollDeductionDetailsDO.getDeductionId().equals(empCompensation.getEmpdeductionid())){
							ytd = ytd+payrollDeductionDetailsDO.getMonthly();
						}
					}
				}
				result.put(CommonConstants.YTD, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(ytd + Double.parseDouble(result.get(CommonConstants.DEDUCTIONMONTHLY).toString()))));
			}else{
				result.put(CommonConstants.YTD, String.valueOf(result.get(CommonConstants.DEDUCTIONMONTHLY).toString()));
				
			}*/
		}else{
			result.put(CommonConstants.YTD, String.valueOf(result.get(CommonConstants.DEDUCTIONMONTHLY).toString()));
		}
		return result;
	}
	
	public static JSONObject getEmployeeTaxDetailObject(EmployeeTaxDO empCompensation, Long empId, String month)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.TAXID, String.valueOf(empCompensation.getEmptaxid()));
		List<TaxSettingsDO> deductionList = taxSettingsService.retrieveById(empCompensation.getEmptaxid());
		if(deductionList != null && deductionList.size() > 0){
			result.put(CommonConstants.TAXNAME, String.valueOf(deductionList.get(0).getDisplayname()));
		}else{
			result.put(CommonConstants.TAXNAME, "");
		}
		
		String fromMnth = "";
		String toMnth = "";
		if(month != null && empId != null){
			List<EmployeeDO> employee = employeeService.retrieveEmpId(empId.toString());
			if(employee != null && employee.size() > 0){
				if(employee.get(0).getStartdate() != null){
					String splitMonth = employee.get(0).getStartdate().substring(0, 10);
					String joiningMonth = CommonUtil.convertDateToStringWithOutDate(CommonUtil.convertStringToSqlDate(splitMonth));
					//String[] joinMonthArray = joiningMonth.split(",");
					Double perdaySalary = 0D;
					 int totalDays = 0;
					if(joiningMonth.equalsIgnoreCase(month)){
						Date joiningDate = CommonUtil.convertStringToSqlDate(employee.get(0).getStartdate());
						Calendar c=Calendar.getInstance();
				        c.setTime(joiningDate);
				        int days = c.getActualMaximum(Calendar.DAY_OF_MONTH);
				        String[] noofdays = splitMonth.split("-");
				        totalDays = days;
				        if(!noofdays[2].equals("01")){
				        	 totalDays = days - Integer.parseInt(noofdays[2]);
				        }
				        perdaySalary = empCompensation.getMonthly()/days;
				        result.put(CommonConstants.TAXMONTHLY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(perdaySalary * totalDays)));
					}else{
						result.put(CommonConstants.TAXMONTHLY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(empCompensation.getMonthly())));
					}
					
				}else{
					result.put(CommonConstants.TAXMONTHLY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(empCompensation.getMonthly())));
				}
			}
		}
		
		
		result.put(CommonConstants.MONTHLY, String.valueOf(result.get(CommonConstants.TAXMONTHLY).toString()));
		
		if(empId != null){
			fromMnth = "";
			toMnth = "";
			String todayMnth = CommonUtil.convertDateToStringWithOutDate(new Date());
			String currentMonth [] = todayMnth.split(" ");
			if(currentMonth[0].equalsIgnoreCase("Jan") || currentMonth[0].equalsIgnoreCase("Feb") || currentMonth[0].equalsIgnoreCase("Mar")){
				String mnthYear [] = todayMnth.split(" ");
				int year = Integer.parseInt(mnthYear[1]) - 1;
				String mnth= "Apr";
				fromMnth = mnth + " "+year;
				toMnth = todayMnth;
			}else{
				fromMnth = todayMnth;
				String mnthYear [] = todayMnth.split(" ");
				int year = Integer.parseInt(mnthYear[1]) - 1;
				String mnth= "Apr";
				toMnth = mnth + " "+year;
			}
			//List<EmployeePayrollDO> earningYear = employeePayrollService.retrieveByEmpIdBetweenDate(empId, fromMnth, toMnth);
		/*	
			if(earningYear != null && earningYear.size() > 0){
				List<Long> ids = new ArrayList<Long>();
				for (EmployeePayrollDO employeePayrollDO : earningYear) {
					ids.add(employeePayrollDO.getId());
				}
				Double ytd = 0D;
				List<PayrollTaxDetailsDO> paytaxList = payrollTaxDetailsService.retrieveByEmpCompensationIds(ids);
				
				if(paytaxList != null && paytaxList.size() > 0){
					for (PayrollTaxDetailsDO payrollTaxDetailsDO : paytaxList) {
						if(payrollTaxDetailsDO.getTaxId().equals(empCompensation.getEmptaxid())){
							ytd = ytd+payrollTaxDetailsDO.getMonthly();
						}
					}
				}
				
				result.put(CommonConstants.YTD, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(ytd + Double.parseDouble(result.get(CommonConstants.TAXMONTHLY).toString()))));
			}else{
				result.put(CommonConstants.YTD, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.parseDouble(result.get(CommonConstants.TAXMONTHLY).toString()))));
				
			}*/
		}else{
			result.put(CommonConstants.YTD, String.valueOf(Double.parseDouble(result.get(CommonConstants.TAXMONTHLY).toString())));
		}
		return result;
	}
}
