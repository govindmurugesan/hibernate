package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.TimesheetTypeDO;
import com.spheresuite.erp.util.CommonConstants;

@Component
public class TimesheetTypeUtil {
	
	private TimesheetTypeUtil() {}

	public static JSONObject getTimesheetTypeList(List<TimesheetTypeDO> timesheetTypeList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (TimesheetTypeDO timesheetType : timesheetTypeList) {
				resultJSONArray.put(getTimesheetTypeDetailObject(timesheetType));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getTimesheetTypeDetailObject(TimesheetTypeDO timesheetType)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(timesheetType.getTimesheetTypeId()));
		result.put(CommonConstants.NAME, String.valueOf(timesheetType.getName()));
		result.put(CommonConstants.STATUS, String.valueOf(timesheetType.getStatus()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(timesheetType.getUpdatedon())));
		if(timesheetType.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(timesheetType.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		return result;
	}
}
