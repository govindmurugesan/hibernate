package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.DASettingsDO;
import com.spheresuite.erp.util.CommonConstants;
@Component
public class DASettingsUtil {
	
	private DASettingsUtil() {}
	
	public static JSONObject getDASettingsList(List<DASettingsDO> daList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (DASettingsDO daDo: daList) {
				resultJSONArray.put(getEmployeeTypeDetailObject(daDo));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getEmployeeTypeDetailObject(DASettingsDO daDo)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(daDo.getDearnessallowanceId()));
		result.put(CommonConstants.MONTHLY, String.valueOf(daDo.getMonth()));
		result.put(CommonConstants.YEAR, String.valueOf(daDo.getYear()));
		result.put(CommonConstants.DAPOINTS, String.valueOf(daDo.getDApoints()));
		result.put(CommonConstants.RATE, String.valueOf(daDo.getDArate()));
		if(daDo.getDAdeductionpoints() != null){
			result.put(CommonConstants.DADEDUCTIONPOINT, String.valueOf(daDo.getDAdeductionpoints()));
		}else{
			result.put(CommonConstants.DADEDUCTIONPOINT, String.valueOf(""));
		}
		
		if(daDo.getSubDA() != null){
			result.put(CommonConstants.SUBDA, String.valueOf(daDo.getSubDA()));
		}else{
			result.put(CommonConstants.SUBDA, String.valueOf(""));
		}
		
		if(daDo.getUnitOrBranch() != null){
			result.put(CommonConstants.UNIT, String.valueOf(daDo.getUnitOrBranch().getUnitOrBranchId()));
			result.put(CommonConstants.UNITNAME, String.valueOf(daDo.getUnitOrBranch().getUnitOrBranch()));
		}else{
			result.put(CommonConstants.UNIT, String.valueOf(""));
		}
		if(daDo.getAddDA() != null){
			result.put(CommonConstants.ADDDA, String.valueOf(daDo.getAddDA()));
		}else{
			result.put(CommonConstants.ADDDA, String.valueOf(""));
		}
		
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(daDo.getUpdatedon())));
		if(daDo.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(daDo.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		return result;
	}
}
