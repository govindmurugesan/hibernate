package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.StatusDO;
import com.spheresuite.erp.util.CommonConstants;

@Component
public class StatusUtil {
	
	private StatusUtil() {}
	
	public static JSONObject getStatusList(List<StatusDO> statusList){
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (StatusDO status : statusList) {
				resultJSONArray.put(getStatusDetailObject(status));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getStatusDetailObject(StatusDO status)throws JSONException{
		
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(status.getStatusId()));
		result.put(CommonConstants.NAME, String.valueOf(status.getName()));
		result.put(CommonConstants.STATUS, String.valueOf(status.getStatus()));
		if(status.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(status.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(status.getUpdatedon())));
		return result;
	}
}
