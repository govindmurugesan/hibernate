package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.ItSlabEffectiveDO;
import com.spheresuite.erp.util.CommonConstants;
@Component
public class ItSlabEffetctiveUtil {
	
	private ItSlabEffetctiveUtil() {}
	
	public static JSONObject getItSlabEffectiveList(List<ItSlabEffectiveDO> itSlabEffectiveList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (ItSlabEffectiveDO itSlabEffective : itSlabEffectiveList) {
				resultJSONArray.put(getITSavingsSettingsObject(itSlabEffective));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getITSavingsSettingsObject(ItSlabEffectiveDO itSlabEffective)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(itSlabEffective.getItSlabEffectiveId()));
		result.put(CommonConstants.FROM, String.valueOf(CommonUtil.convertDateToStringWithdatetime(itSlabEffective.getEffectivefromDate())));
		if(itSlabEffective.getEffectiveToDate() != null){
			result.put(CommonConstants.TO, String.valueOf(CommonUtil.convertDateToStringWithdatetime(itSlabEffective.getEffectiveToDate())));
		}else{
			result.put(CommonConstants.TO, String.valueOf(""));
		}
		result.put(CommonConstants.STATUS, String.valueOf(itSlabEffective.getStatus()));
		if(itSlabEffective.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(itSlabEffective.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(itSlabEffective.getUpdatedon())));
		return result;
	}
}
