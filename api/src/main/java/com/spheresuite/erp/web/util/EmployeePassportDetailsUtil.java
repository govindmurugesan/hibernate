package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.spheresuite.erp.domainobject.EmployeePassportDetailsDO;
import com.spheresuite.erp.util.CommonConstants;

public class EmployeePassportDetailsUtil {
	
	private EmployeePassportDetailsUtil() {}
	
	public static JSONObject getEmployeePassportInfoList(List<EmployeePassportDetailsDO> employeePassportInfoList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeePassportDetailsDO employeePassportInfoDO : employeePassportInfoList) {
				resultJSONArray.put(getEmployeePassportInfoDetailObject(employeePassportInfoDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getEmployeePassportInfoDetailObject(EmployeePassportDetailsDO employeePassportInfoDO)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(employeePassportInfoDO.getEmpPassportDetailId()));
		result.put(CommonConstants.NUMBER, String.valueOf(employeePassportInfoDO.getPassportNumber()));
		result.put(CommonConstants.DATEISSUED, String.valueOf(CommonUtil.convertDateToStringWithdatetime(employeePassportInfoDO.getDateOfIssue())));
		result.put(CommonConstants.DATEOFEXPIRY, String.valueOf(CommonUtil.convertDateToStringWithdatetime(employeePassportInfoDO.getDateOfExpiry())));
		result.put(CommonConstants.PLACEOFISSUE, String.valueOf(employeePassportInfoDO.getPlaceOfIssue()));
		
		return result;
	}
}
