package com.spheresuite.erp.web.util;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.AttendanceDO;
import com.spheresuite.erp.util.CommonConstants;
@Component
public class AttendanceUtil {
	
	private AttendanceUtil() {}
	
	public static JSONObject getAttendanceList(List<AttendanceDO> attendanceList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (AttendanceDO attendance : attendanceList) {
				resultJSONArray.put(getAttendancDetailObject(attendance));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getAttendanceListWithOutIntime(List<AttendanceDO> attendanceList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (AttendanceDO attendance : attendanceList) {
				resultJSONArray.put(getAttendancDetailObjectWithOutInTime(attendance));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getAttendanceListSpecific(List<Object[]> attendanceList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (Object[] attendance : attendanceList) {
				resultJSONArray.put(getAttendancSpecificDetailObject(attendance));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getAttendancSpecificDetailObject(Object[] attendance)throws JSONException {
		JSONObject result = new JSONObject();
		if(attendance[0] != null){
			
			result.put(CommonConstants.ID, String.valueOf(attendance[0]));
		}
		
		if(attendance[1] != null){
			result.put(CommonConstants.TIMEINDATE, String.valueOf(attendance[1]));
			result.put(CommonConstants.TIMEINDATEDISPLAY, String.valueOf(CommonUtil.convertDateToStringWithdatetime((Date)attendance[1])));
		}else{
			result.put(CommonConstants.TIMEINDATE, String.valueOf(""));
			result.put(CommonConstants.TIMEINDATEDISPLAY, String.valueOf(""));
		}
		
		if(attendance[2] != null){
			result.put(CommonConstants.TIMEOUTDATE, String.valueOf(attendance[2]));
			result.put(CommonConstants.TIMEOUTDATEDISPLAY, String.valueOf(CommonUtil.convertDateToStringWithdatetime((Date)attendance[2])));
		}else{
			result.put(CommonConstants.TIMEOUTDATE, String.valueOf(""));
			result.put(CommonConstants.TIMEOUTDATEDISPLAY, String.valueOf(""));
		}
		
		if(attendance[3] != null){
			
			String name="";
			if(attendance[4] != null){
				result.put(CommonConstants.FIRSTNAME, String.valueOf(attendance[4]));
				name = name+String.valueOf(attendance[4]);
			}else{
				result.put(CommonConstants.FIRSTNAME,"");
			}
			if(attendance[5] != null){
				result.put(CommonConstants.MIDDLENAME, String.valueOf(attendance[5]));
				name = name+" "+String.valueOf(attendance[5]);
			}else{
				result.put(CommonConstants.MIDDLENAME,"");
			}
			if(attendance[6] != null){
				result.put(CommonConstants.LASTNAME, String.valueOf(attendance[6]));
				name = name+" "+String.valueOf(attendance[6]);
			}else{
				result.put(CommonConstants.LASTNAME,"");
			}
			result.put(CommonConstants.EMP_NAME, name); 
			result.put(CommonConstants.EMPID, String.valueOf(attendance[3])); 
		}else{
			result.put(CommonConstants.EMP_NAME, "");
			result.put(CommonConstants.EMPID, ""); 
		}
		
		
		if(attendance[7] != null){
			result.put(CommonConstants.TIMEIN, String.valueOf(attendance[7])); 
		}else{
			result.put(CommonConstants.TIMEIN, ""); 
		}
		if(attendance[8] != null){
			result.put(CommonConstants.TIMEOUT, String.valueOf(attendance[8])); 
		}else{
			result.put(CommonConstants.TIMEOUT, ""); 
		}
		
		if(attendance[9] != null){
			result.put(CommonConstants.ATTENDANCETYPE, String.valueOf(attendance[9])); 
		}else{
			result.put(CommonConstants.ATTENDANCETYPE, ""); 
		}
		
		
		if(attendance[7] != null && attendance[1] != null && attendance[2] != null && attendance[8] != null && !attendance[7].toString().isEmpty() && !attendance[8].toString().isEmpty()){
			String[] timeInDetail = attendance[7].toString().split(":");
			Calendar timeInDate = Calendar.getInstance();
			timeInDate.setTime((Date)attendance[1]);
			timeInDate.set(Calendar.HOUR_OF_DAY, (int) Long.parseLong(timeInDetail[0]));
			timeInDate.set(Calendar.MINUTE, (int) Long.parseLong(timeInDetail[1]));
			timeInDate.set(Calendar.SECOND, (int) Long.parseLong(timeInDetail[2]));
			
			String[] timeOutDetail = attendance[8].toString().split(":");
			Calendar timeOutDate = Calendar.getInstance();
			timeOutDate.setTime((Date)attendance[2]);
			if(timeOutDetail[0] != null && !timeOutDetail[0].toString().isEmpty()){
				timeOutDate.set(Calendar.HOUR_OF_DAY, (int) Long.parseLong(timeOutDetail[0]));
			}
			
			if(timeOutDetail[1] != null && !timeOutDetail[1].toString().isEmpty()){
				timeOutDate.set(Calendar.MINUTE, (int) Long.parseLong(timeOutDetail[1]));
			}
			
			if(timeOutDetail[2] != null && !timeOutDetail[2].toString().isEmpty()){
				timeOutDate.set(Calendar.SECOND, (int) Long.parseLong(timeOutDetail[2]));
			}
			
			
			
			//long diff = (timeInDate.getTimeInMillis() - timeOutDate.getTimeInMillis());
			long diff = (timeOutDate.getTimeInMillis() - timeInDate.getTimeInMillis());
			
			long diffSeconds = diff / 1000 % 60;
			long diffMinutes = diff / (60 * 1000) % 60;
			long diffHours = diff / (60 * 60 * 1000) % 24;
			//long diffDays = diff / (24 * 60 * 60 * 1000);
			NumberFormat formatter = new DecimalFormat("00"); 
			result.put(CommonConstants.TIME, String.valueOf(formatter.format(diffHours)+":"+formatter.format(diffMinutes)+":"+formatter.format(diffSeconds))); 
			
			//attendance.getTimeInDate().setHours((int) Long.parseLong(timeDetail[0]));
			
		}else{
			result.put(CommonConstants.TIME, String.valueOf(00+":"+00+":"+00));
		}
		
		return result;
	}

	public static JSONObject getAttendancDetailObject(AttendanceDO attendance)throws JSONException {
		JSONObject result = new JSONObject();
		/*if(attendance.getTimeIn() != null && !attendance.getTimeIn().isEmpty() &&
				attendance.getTimeOut() != null && !attendance.getTimeOut().isEmpty()){*/
			result.put(CommonConstants.ID, String.valueOf(attendance.getAttendanceId()));
			if(attendance.getEmployee() != null){
				String empName = CommonUtil.getEmpObject(attendance.getEmployee().getEmpId());
				result.put(CommonConstants.EMP_NAME, empName); 
				result.put(CommonConstants.EMPID, attendance.getEmployee().getEmpId()); 
			}else{
				result.put(CommonConstants.EMP_NAME, "");
				result.put(CommonConstants.EMPID, ""); 
			}
			
			if(attendance.getAttendanceType() != null){
				result.put(CommonConstants.ATTENDANCETYPE, String.valueOf(attendance.getAttendanceType())); 
			}else{
				result.put(CommonConstants.ATTENDANCETYPE, ""); 
			}
			
			if(attendance.getTimeInDate() != null){
				result.put(CommonConstants.TIMEINDATE, String.valueOf(attendance.getTimeInDate()));
				result.put(CommonConstants.TIMEINDATEDISPLAY, String.valueOf(CommonUtil.convertDateToStringWithdatetime(attendance.getTimeInDate())));
			}else{
				result.put(CommonConstants.TIMEINDATE, String.valueOf(""));
				result.put(CommonConstants.TIMEINDATEDISPLAY, String.valueOf(""));
			}
			
			if(attendance.getTimeOutDate() != null){
				result.put(CommonConstants.TIMEOUTDATE, String.valueOf(attendance.getTimeOutDate()));
				result.put(CommonConstants.TIMEOUTDATEDISPLAY, String.valueOf(CommonUtil.convertDateToStringWithdatetime(attendance.getTimeOutDate())));
			}else{
				result.put(CommonConstants.TIMEOUTDATE, String.valueOf(""));
				result.put(CommonConstants.TIMEOUTDATEDISPLAY, String.valueOf(""));
			}
			if(attendance.getTimeIn() != null){
				result.put(CommonConstants.TIMEIN, String.valueOf(attendance.getTimeIn())); 
			}else{
				result.put(CommonConstants.TIMEIN, ""); 
			}
			if(attendance.getTimeOut() != null){
				result.put(CommonConstants.TIMEOUT, String.valueOf(attendance.getTimeOut())); 
			}else{
				result.put(CommonConstants.TIMEOUT, ""); 
			}
			if(attendance.getTimeIn() != null && attendance.getTimeInDate() != null && attendance.getTimeOutDate() != null && attendance.getTimeOut() != null && !attendance.getTimeIn().isEmpty() && !attendance.getTimeOut().isEmpty()){
				String[] timeInDetail = attendance.getTimeIn().split(":");
				Calendar timeInDate = Calendar.getInstance();
				timeInDate.setTime(attendance.getTimeInDate());
				timeInDate.set(Calendar.HOUR_OF_DAY, (int) Long.parseLong(timeInDetail[0]));
				timeInDate.set(Calendar.MINUTE, (int) Long.parseLong(timeInDetail[1]));
				timeInDate.set(Calendar.SECOND, (int) Long.parseLong(timeInDetail[2]));
				
				String[] timeOutDetail = attendance.getTimeOut().split(":");
				Calendar timeOutDate = Calendar.getInstance();
				timeOutDate.setTime(attendance.getTimeOutDate());
				if(timeOutDetail[0] != null && !timeOutDetail[0].toString().isEmpty()){
					timeOutDate.set(Calendar.HOUR_OF_DAY, (int) Long.parseLong(timeOutDetail[0]));
				}
				
				if(timeOutDetail[1] != null && !timeOutDetail[1].toString().isEmpty()){
					timeOutDate.set(Calendar.MINUTE, (int) Long.parseLong(timeOutDetail[1]));
				}
				
				if(timeOutDetail[2] != null && !timeOutDetail[2].toString().isEmpty()){
					timeOutDate.set(Calendar.SECOND, (int) Long.parseLong(timeOutDetail[2]));
				}
				
				//long diff = (timeInDate.getTimeInMillis() - timeOutDate.getTimeInMillis());
				long diff = (timeOutDate.getTimeInMillis() - timeInDate.getTimeInMillis());
				
				long diffSeconds = diff / 1000 % 60;
				long diffMinutes = diff / (60 * 1000) % 60;
				long diffHours = diff / (60 * 60 * 1000) % 24;
				//long diffDays = diff / (24 * 60 * 60 * 1000);
				NumberFormat formatter = new DecimalFormat("00"); 
				result.put(CommonConstants.TIME, String.valueOf(formatter.format(diffHours)+":"+formatter.format(diffMinutes)+":"+formatter.format(diffSeconds))); 
				
				//attendance.getTimeInDate().setHours((int) Long.parseLong(timeDetail[0]));
				
			}else{
				result.put(CommonConstants.TIME, String.valueOf(00+":"+00+":"+00)); 
			}
			result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(attendance.getUpdatedon())));
			if(attendance.getUpdatedby() != null){
				String empName = CommonUtil.getEmpObject(attendance.getUpdatedby());
				result.put(CommonConstants.UPDATED_BY, empName); 
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		//}
		
		return result;
	}
	
	public static JSONObject getAttendancDetailObjectWithOutInTime(AttendanceDO attendance)throws JSONException {
		JSONObject result = new JSONObject();
		/*if(attendance.getTimeIn() != null && !attendance.getTimeIn().isEmpty() &&
				attendance.getTimeOut() != null && !attendance.getTimeOut().isEmpty()){*/
			result.put(CommonConstants.TIMEIN, String.valueOf(attendance.getTimeIn())); 
		result.put(CommonConstants.ID, String.valueOf(attendance.getAttendanceId()));
		if(attendance.getEmployee() != null){
			String empName = CommonUtil.getEmpObject(attendance.getEmployee().getEmpId());
			result.put(CommonConstants.EMP_NAME, empName); 
			result.put(CommonConstants.EMPID, attendance.getEmployee().getEmpId()); 
		}else{
			result.put(CommonConstants.EMP_NAME, "");
			result.put(CommonConstants.EMPID, ""); 
		}
		
		if(attendance.getTimeInDate() != null){
			result.put(CommonConstants.TIMEINDATE, String.valueOf(attendance.getTimeInDate()));
			result.put(CommonConstants.TIMEINDATEDISPLAY, String.valueOf(CommonUtil.convertDateToStringWithdatetime(attendance.getTimeInDate())));
		}else{
			result.put(CommonConstants.TIMEINDATE, String.valueOf(""));
			result.put(CommonConstants.TIMEINDATEDISPLAY, String.valueOf(""));
		}
		
		if(attendance.getTimeOutDate() != null){
			result.put(CommonConstants.TIMEOUTDATE, String.valueOf(attendance.getTimeOutDate()));
			result.put(CommonConstants.TIMEOUTDATEDISPLAY, String.valueOf(CommonUtil.convertDateToStringWithdatetime(attendance.getTimeOutDate())));
		}else{
			result.put(CommonConstants.TIMEOUTDATE, String.valueOf(""));
			result.put(CommonConstants.TIMEOUTDATEDISPLAY, String.valueOf(""));
		}
		if(attendance.getTimeIn() != null){
			result.put(CommonConstants.TIMEIN, String.valueOf(attendance.getTimeIn())); 
		}else{
			result.put(CommonConstants.TIMEIN, ""); 
		}
		if(attendance.getTimeOut() != null){
			result.put(CommonConstants.TIMEOUT, String.valueOf(attendance.getTimeOut())); 
		}else{
			result.put(CommonConstants.TIMEOUT, ""); 
		}
		
		if(attendance.getAttendanceType() != null){
			result.put(CommonConstants.ATTENDANCETYPE, String.valueOf(attendance.getAttendanceType())); 
		}else{
			result.put(CommonConstants.ATTENDANCETYPE, ""); 
		}
		
		if(attendance.getTimeIn() != null && attendance.getTimeInDate() != null && attendance.getTimeOutDate() != null && attendance.getTimeOut() != null && !attendance.getTimeIn().isEmpty() && !attendance.getTimeOut().isEmpty()){
			String[] timeInDetail = attendance.getTimeIn().split(":");
			Calendar timeInDate = Calendar.getInstance();
			timeInDate.setTime(attendance.getTimeInDate());
			timeInDate.set(Calendar.HOUR_OF_DAY, (int) Long.parseLong(timeInDetail[0]));
			timeInDate.set(Calendar.MINUTE, (int) Long.parseLong(timeInDetail[1]));
			timeInDate.set(Calendar.SECOND, (int) Long.parseLong(timeInDetail[2]));
			
			String[] timeOutDetail = attendance.getTimeOut().split(":");
			Calendar timeOutDate = Calendar.getInstance();
			timeOutDate.setTime(attendance.getTimeOutDate());
			timeOutDate.set(Calendar.HOUR_OF_DAY, (int) Long.parseLong(timeOutDetail[0]));
			timeOutDate.set(Calendar.MINUTE, (int) Long.parseLong(timeOutDetail[1]));
			timeOutDate.set(Calendar.SECOND, (int) Long.parseLong(timeOutDetail[2]));
			
			long diff = (timeOutDate.getTimeInMillis() - timeInDate.getTimeInMillis());
			
			long diffSeconds = diff / 1000 % 60;
			long diffMinutes = diff / (60 * 1000) % 60;
			long diffHours = diff / (60 * 60 * 1000) % 24;
			NumberFormat formatter = new DecimalFormat("00"); 
			result.put(CommonConstants.TIME, String.valueOf(formatter.format(diffHours)+":"+formatter.format(diffMinutes)+":"+formatter.format(diffSeconds))); 
		}else{
			result.put(CommonConstants.TIME, String.valueOf(00+":"+00+":"+00)); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(attendance.getUpdatedon())));
		if(attendance.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(attendance.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		//}
		return result;
	}
}
