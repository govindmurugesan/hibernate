package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.CategoryDO;
import com.spheresuite.erp.util.CommonConstants;

@Component
public class CategoryUtil {
	
	private CategoryUtil() {}
	
	public static JSONObject getCategoryList(List<CategoryDO> categoryList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (CategoryDO category : categoryList) {
				resultJSONArray.put(getCategoryDetailObject(category));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getCategoryDetailObject(CategoryDO category)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(category.getCategoryId()));
		result.put(CommonConstants.NAME, String.valueOf(category.getName()));
		if(category.getDescripetion() != null){
			result.put(CommonConstants.DESC, String.valueOf(category.getDescripetion()));
		}else{
			result.put(CommonConstants.DESC, "");
		}
		result.put(CommonConstants.STATUS, String.valueOf(category.getStatus()));
		if(category.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(category.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(category.getUpdatedon())));
		return result;
	}
}
