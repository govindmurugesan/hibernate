package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.InsuranceTypeDO;
import com.spheresuite.erp.util.CommonConstants;

@Component
public class InsuranceTypeUtil {
	
	private InsuranceTypeUtil() {}
	
	public static JSONObject getInsuranceTypeList(List<InsuranceTypeDO> insuranceTypeList){
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (InsuranceTypeDO insuranceType : insuranceTypeList) {
				resultJSONArray.put(getAdvanceDetailObject(insuranceType));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getAdvanceDetailObject(InsuranceTypeDO insuranceType)throws JSONException{
		
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(insuranceType.getInsuranceTypeId()));
		result.put(CommonConstants.NAME, String.valueOf(insuranceType.getName()));
		result.put(CommonConstants.STATUS, String.valueOf(insuranceType.getStatus()));
		if(insuranceType.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(insuranceType.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(insuranceType.getUpdatedon())));
		return result;
	}
}
