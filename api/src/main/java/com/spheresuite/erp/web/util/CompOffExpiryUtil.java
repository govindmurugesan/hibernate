package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.CompOffExpiryDO;
import com.spheresuite.erp.util.CommonConstants;

@Component
public class CompOffExpiryUtil {
	
	private CompOffExpiryUtil() {}
	
	public static JSONObject getCompOffList(List<CompOffExpiryDO> compOffExpiryList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (CompOffExpiryDO compOffExpiryDO : compOffExpiryList) {
				resultJSONArray.put(getCompOffDetailObject(compOffExpiryDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getCompOffDetailObject(CompOffExpiryDO compOffExpiryDO)throws JSONException{
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(compOffExpiryDO.getCompOffId()));
		result.put(CommonConstants.NUMBEROFDAYS, String.valueOf(compOffExpiryDO.getNoofdays()));
		if(compOffExpiryDO.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(compOffExpiryDO.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(compOffExpiryDO.getUpdatedon())));
		return result;
	}
}
