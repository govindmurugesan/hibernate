package com.spheresuite.erp.web.util;

import java.util.List;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.RecentActivityDO;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.util.CommonConstants;
@Component
public class RecentActivityUtil {
	
	private RecentActivityUtil() {}
	
	@Autowired
	private EmployeeService employeeServiceTemp;
	
	private static EmployeeService employeeService;
	
	@PostConstruct
	public void init() {
		employeeService = this.employeeServiceTemp;
	}
	
	public static JSONObject getRecentActivityList(List<RecentActivityDO> recentActivityDOList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (RecentActivityDO recentActivityDO : recentActivityDOList) {
				resultJSONArray.put(getRecentActivityDetailObject(recentActivityDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getRecentActivityDetailObject(RecentActivityDO recentActivityDO)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(recentActivityDO.getId()));
		if(recentActivityDO.getDescripetion() != null){
			result.put(CommonConstants.DESC, String.valueOf(recentActivityDO.getDescripetion()));
		}else{
			result.put(CommonConstants.DESC, "");
		}
		
		/*if(recentActivityDO.getUpdatedBy() != null){
			List<EmployeeDO> empList = employeeService.retriveByEmpId(Long.parseLong(recentActivityDO.getUpdatedBy()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}*/
		if(recentActivityDO.getUpdatedBy() != null){
			String empName = CommonUtil.getEmpObject(recentActivityDO.getUpdatedBy());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		if(recentActivityDO.getEmpId() != null){
			result.put(CommonConstants.EMPID, recentActivityDO.getEmpId()); 
			List<EmployeeDO> empList = employeeService.retrieveEmpId(recentActivityDO.getEmpId().toString());
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.NAME, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.NAME, String.valueOf((empList.get(0).getFirstname()!=null?empList.get(0).getFirstname():"") + " " + (empList.get(0).getLastname()!=null?empList.get(0).getLastname():"")));
				if(empList.get(0).getPhoto() != null){
					result.put(CommonConstants.PHOTO,empList.get(0).getPhoto());
				}else{
					result.put(CommonConstants.PHOTO,"");
				}
			}else{
				result.put(CommonConstants.NAME, ""); 
				result.put(CommonConstants.PHOTO,"");
			}
		}else{
			result.put(CommonConstants.EMPID, ""); 
			result.put(CommonConstants.NAME, ""); 
			result.put(CommonConstants.PHOTO,"");
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(recentActivityDO.getUpdatedon())));
		return result;
	}
}
