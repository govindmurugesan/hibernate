package com.spheresuite.erp.web.util;

import java.util.List;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.GeneralDeductionDO;
import com.spheresuite.erp.domainobject.GeneralDeductionDetailDO;
import com.spheresuite.erp.service.GeneralDeductionDetailService;
import com.spheresuite.erp.util.CommonConstants;
@Component
public class GeneralDeducationUtil {
	
	private GeneralDeducationUtil() {}
	
	@Autowired
	private GeneralDeductionDetailService tloanPaymentDetailService;
	private static GeneralDeductionDetailService loanPaymentDetailService;
	
	@PostConstruct
	public void init() {
		loanPaymentDetailService = tloanPaymentDetailService;
	}
	

	public static JSONObject getAdvancePaymentList(List<GeneralDeductionDO> loanPaymentList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (GeneralDeductionDO loanPayment : loanPaymentList) {
				resultJSONArray.put(getAdvancePaymentDetailObject(loanPayment));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getAdvancePaymentDetailObject(GeneralDeductionDO loanPayment)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(loanPayment.getGeneralDeductionId()));
		if(loanPayment.getEmployee() != null ){
			result.put(CommonConstants.EMPID, String.valueOf(loanPayment.getEmployee().getEmpId()));
			if(loanPayment.getEmployee().getMiddlename() != null) result.put(CommonConstants.EMPNAME, String.valueOf(loanPayment.getEmployee().getFirstname() + " " +loanPayment.getEmployee().getMiddlename() + " " + loanPayment.getEmployee().getLastname()));
			else if(loanPayment.getEmployee().getMiddlename() != null && loanPayment.getEmployee().getLastname() == null ) result.put(CommonConstants.EMPNAME, String.valueOf(loanPayment.getEmployee().getFirstname() + " " +loanPayment.getEmployee().getMiddlename()));
			else if(loanPayment.getEmployee().getMiddlename() == null && loanPayment.getEmployee().getLastname() == null ) result.put(CommonConstants.EMPNAME,String.valueOf(loanPayment.getEmployee().getFirstname()));
			else result.put(CommonConstants.EMPNAME, String.valueOf(loanPayment.getEmployee().getFirstname() + " " + loanPayment.getEmployee().getLastname()));
		}else{
			result.put(CommonConstants.EMPID, "");
			result.put(CommonConstants.EMPNAME, ""); 
		}
		result.put(CommonConstants.AMOUNT, String.valueOf(loanPayment.getAmount()));
		if(loanPayment.getMonth() != null){
			result.put(CommonConstants.MONTHLY, String.valueOf(loanPayment.getMonth()));
		}else{
			result.put(CommonConstants.MONTHLY, "");
		}
		if(loanPayment.getDeductionSubType() != null){
			result.put(CommonConstants.TYPE_ID, String.valueOf(loanPayment.getDeductionSubType().getDeductionSubTypeId()));
			result.put(CommonConstants.TYPE, String.valueOf(loanPayment.getDeductionSubType().getName()));
		}else{
			result.put(CommonConstants.TYPE_ID, "");
			result.put(CommonConstants.TYPE, "");
		}
		
		if(loanPayment.getDeductionType() != null){
			result.put(CommonConstants.DEDUCTION_TYPE_ID, String.valueOf(loanPayment.getDeductionType().getDeductionTypeId()));
			result.put(CommonConstants.DEDUCTIONTYPE_NAME, String.valueOf(loanPayment.getDeductionType().getDisplayname()));
		}else{
			result.put(CommonConstants.DEDUCTION_TYPE_ID, "");
			result.put(CommonConstants.DEDUCTIONTYPE_NAME, "");
		}
		
		if(loanPayment.getUnitOrBranch() != null){
			result.put(CommonConstants.UNIT, String.valueOf(loanPayment.getUnitOrBranch().getUnitOrBranchId()));
			result.put(CommonConstants.UNITNAME, String.valueOf(loanPayment.getUnitOrBranch().getUnitOrBranch()));
		}else{
			result.put(CommonConstants.UNIT, "");
			result.put(CommonConstants.UNITNAME, "");
		}
		
		if(loanPayment.getInstallmentAmount() != null){
			result.put(CommonConstants.INSTALLMENTAOUNT, String.valueOf(loanPayment.getInstallmentAmount()));
		}else{
			result.put(CommonConstants.INSTALLMENTAOUNT, "");
		}
		if(loanPayment.getNoOfInstallments() != null){
			result.put(CommonConstants.NOOFINSTALLMENT, String.valueOf(loanPayment.getNoOfInstallments()));
		}else{
			result.put(CommonConstants.NOOFINSTALLMENT, "");
		}
		result.put(CommonConstants.STATUS, String.valueOf(loanPayment.getStatus()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(loanPayment.getUpdatedon())));
		if(loanPayment.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(loanPayment.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		
		Double pendingAmount = 0d;
		List<GeneralDeductionDetailDO> paymentDetailList = loanPaymentDetailService.getByAdvanceId(loanPayment.getGeneralDeductionId());
		if(paymentDetailList != null && paymentDetailList.size() > 0){
			JSONArray resultJSONArray = new JSONArray();
			for (GeneralDeductionDetailDO paymentDetail : paymentDetailList) {
				if(paymentDetail != null && paymentDetail.getAmount() != null &&  paymentDetail.getStatus().equalsIgnoreCase(CommonConstants.ACTIVESTRING)){
					pendingAmount += Double.parseDouble(paymentDetail.getAmount().toString());
				}
				resultJSONArray.put(getPaymentDetailObject(paymentDetail));
			}
			result.put(CommonConstants.LOANDETAIL, resultJSONArray);
			result.put(CommonConstants.PENDINGAMOUNT, pendingAmount);
		}
		
		return result;
	}
	
	
	public static JSONObject getPaymentDetailObject(GeneralDeductionDetailDO paymentDetail)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(paymentDetail.getGeneralDeductionDetailId()));
		result.put(CommonConstants.AMOUNT, String.valueOf(paymentDetail.getAmount()));
		if(paymentDetail.getMonth() != null){
			result.put(CommonConstants.MONTH, String.valueOf(paymentDetail.getMonth()));
		}else{
			result.put(CommonConstants.MONTH, "");
		}
		result.put(CommonConstants.STATUS, String.valueOf(paymentDetail.getStatus()));
		return result;
	}
}
