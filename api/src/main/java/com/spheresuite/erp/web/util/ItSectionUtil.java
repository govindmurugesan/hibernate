package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.ItSectionDO;
import com.spheresuite.erp.util.CommonConstants;
@Component
public class ItSectionUtil {
	
	private ItSectionUtil() {}
	
	public static JSONObject getSectionList(List<ItSectionDO> itSectionList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (ItSectionDO itSection : itSectionList) {
				resultJSONArray.put(getITSavingsSettingsObject(itSection));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getITSavingsSettingsObject(ItSectionDO itSection)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(itSection.getItSectionId()));
		result.put(CommonConstants.FROMDATE, String.valueOf(CommonUtil.convertDateToStringWithdatetime(itSection.getFromYear())));
		if(itSection.getToYear() != null){
			result.put(CommonConstants.TODATE, String.valueOf(CommonUtil.convertDateToStringWithdatetime(itSection.getToYear())));
		}else{
			result.put(CommonConstants.TODATE, String.valueOf(""));
		}
		result.put(CommonConstants.SECTION, String.valueOf(itSection.getSection()));
		result.put(CommonConstants.MAXLIMIT, String.valueOf(itSection.getMaxLimit()));
		result.put(CommonConstants.STATUS, String.valueOf(itSection.getStatus()));
		if(itSection.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(itSection.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(itSection.getUpdatedon())));
		return result;
	}
}
