package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.TaxSlabsSettingsDO;
import com.spheresuite.erp.util.CommonConstants;

@Component
public class TaxSlabsSettingsUtil {
	
	private TaxSlabsSettingsUtil() {}
	
	public static JSONObject getTaxSettingsList(List<TaxSlabsSettingsDO> taxSlabsSettingsList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (TaxSlabsSettingsDO taxSlabsSettingsDO : taxSlabsSettingsList) {
				resultJSONArray.put(getTaxSettingsDetailObject(taxSlabsSettingsDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getTaxSettingsDetailObject(TaxSlabsSettingsDO taxSlabsSettingsDO)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(taxSlabsSettingsDO.getTaxSlabId() != null ? taxSlabsSettingsDO.getTaxSlabId() : ""));
		result.put(CommonConstants.PERCENTAGEAMOUNT, String.valueOf(taxSlabsSettingsDO.getPercentageamount() != null ? taxSlabsSettingsDO.getPercentageamount() : ""));
		result.put(CommonConstants.FROMAMOUNT, String.valueOf(taxSlabsSettingsDO.getAmountfrom() != null ? taxSlabsSettingsDO.getAmountfrom() : ""));
		result.put(CommonConstants.TOAMOUNT, String.valueOf(taxSlabsSettingsDO.getAmountto() != null ? taxSlabsSettingsDO.getAmountto() : ""));
		result.put(CommonConstants.FROMAGE, String.valueOf(taxSlabsSettingsDO.getEmpAgeFrom() != null ? taxSlabsSettingsDO.getEmpAgeFrom() : ""));
		result.put(CommonConstants.TOAGE, String.valueOf(taxSlabsSettingsDO.getEmpAgeTo() != null ? taxSlabsSettingsDO.getEmpAgeTo() : ""));
		
		if(taxSlabsSettingsDO.getUpdatedon() != null){
			result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(taxSlabsSettingsDO.getUpdatedon())));
		}else{
			result.put(CommonConstants.UPDATEDON, String.valueOf(""));
		}
		result.put(CommonConstants.STARTDATE,String.valueOf(taxSlabsSettingsDO.getStartdate() != null ? taxSlabsSettingsDO.getStartdate() : ""));
		result.put(CommonConstants.ENDDATE,String.valueOf(taxSlabsSettingsDO.getEnddate() != null ? taxSlabsSettingsDO.getEnddate() : ""));
		result.put(CommonConstants.STATUS, String.valueOf(taxSlabsSettingsDO.getStatus()));
		if(taxSlabsSettingsDO.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(taxSlabsSettingsDO.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		/*String displayName="";
		if(taxSlabsSettingsDO.getPercentageamount() != null){
			displayName = taxSlabsSettingsDO.getPercentageamount() + "% Of Taxable Amount";
		}*/
		
		//result.put(CommonConstants.SETTINGNAME, String.valueOf(displayName));
		return result;
	}
}
