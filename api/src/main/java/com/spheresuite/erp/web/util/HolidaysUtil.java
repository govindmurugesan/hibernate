package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.auth0.jwt.internal.org.apache.commons.lang3.StringUtils;
import com.spheresuite.erp.domainobject.HolidaysDO;
import com.spheresuite.erp.util.CommonConstants;
@Component
public class HolidaysUtil {
	
	private HolidaysUtil() {}

	public static JSONObject getHoildaysList(List<HolidaysDO> hoildayList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (HolidaysDO hoildayDetail : hoildayList) {
				resultJSONArray.put(getHoildayDetailObject(hoildayDetail));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getHoildayDetailObject(HolidaysDO holidayDetail)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(holidayDetail.getHolidayId()));
		result.put(CommonConstants.DESC, String.valueOf(holidayDetail.getDescription()));
		if(holidayDetail.getLeavedate() != null){
			String array1[]= holidayDetail.getLeavedate().split(" ");
			if(StringUtils.isNumeric(array1[1])){
				result.put(CommonConstants.DATE, String.valueOf(array1[0] + " " +array1[1]));
			}else{
				result.put(CommonConstants.DATE, String.valueOf(array1[1] + " " +array1[0]));
			}
		}else{
			result.put(CommonConstants.DATE, String.valueOf(""));
		}
		result.put(CommonConstants.YEAR, String.valueOf(holidayDetail.getYear()));
		result.put(CommonConstants.STATUS, String.valueOf(holidayDetail.getStatus()));
		if(holidayDetail.getComment() != null){
			result.put(CommonConstants.COMMENT, holidayDetail.getComment()); 
		}else{
			result.put(CommonConstants.COMMENT, ""); 
		}
		
		if(holidayDetail.getWorklocation() != null){
			result.put(CommonConstants.WORKLOCATION, String.valueOf(holidayDetail.getWorklocation().getWorklocationId())); 
			result.put(CommonConstants.WORKLOCATION_CITY, holidayDetail.getWorklocation().getWorklocation()); 
		}else{
			result.put(CommonConstants.WORKLOCATION, ""); 
			result.put(CommonConstants.WORKLOCATION_CITY, "");
		}
		
		if(holidayDetail.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(holidayDetail.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		if(holidayDetail.getCreatedby() != null){
			String empName = CommonUtil.getEmpObject(holidayDetail.getCreatedby());
			result.put(CommonConstants.CREATED_BY, empName); 
			result.put(CommonConstants.CREATED_BY_ID, holidayDetail.getCreatedby()); 
		}else{
			result.put(CommonConstants.CREATED_BY, ""); 
			result.put(CommonConstants.CREATED_BY_ID, ""); 
		}
		return result;
	}
	
	public static JSONObject notApproverResponse() {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
}
