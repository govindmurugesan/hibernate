package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.EmployeeOnboardDO;
import com.spheresuite.erp.util.CommonConstants;
@Component
public class EmployeeOnboardUtil {
	
	private EmployeeOnboardUtil() {}
	
	public static JSONObject getEmployeeList(List<EmployeeOnboardDO> employeeOnboardList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeeOnboardDO employee : employeeOnboardList) {
				resultJSONArray.put(getEmployeeDetailObject(employee));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getEmployeeDetailObject(EmployeeOnboardDO employee)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(employee.getEmployeeOnboardId()));
		String name="";
		if(employee.getFirstname() != null){
			result.put(CommonConstants.FIRSTNAME, String.valueOf(employee.getFirstname()));
			name = name+employee.getFirstname();
		}else{
			result.put(CommonConstants.FIRSTNAME,"");
		}
		if(employee.getMiddlename() != null){
			result.put(CommonConstants.MIDDLENAME, String.valueOf(employee.getMiddlename()));
			name = name+" "+employee.getMiddlename();
		}else{
			result.put(CommonConstants.MIDDLENAME,"");
		}
		if(employee.getLastname() != null){
			result.put(CommonConstants.LASTNAME, String.valueOf(employee.getLastname()));
			name = name+" "+employee.getLastname();
		}else{
			result.put(CommonConstants.LASTNAME,"");
		}
		
		if(employee.getPersonalcontact() != null){
			result.put(CommonConstants.PERSONALCONTACT, String.valueOf(employee.getPersonalcontact()));
		}else{
			result.put(CommonConstants.PERSONALCONTACT,"");
		}
		if(employee.getPrimarycontact() != null){
			result.put(CommonConstants.PRIMARYCONTACT, String.valueOf(employee.getPrimarycontact()));
		}else{
			result.put(CommonConstants.PRIMARYCONTACT,"");
		}
		if(employee.getPersonalemail() != null){
			result.put(CommonConstants.PERSONALEMAIL, String.valueOf(employee.getPersonalemail()));
		}else{
			result.put(CommonConstants.PERSONALEMAIL,"");
		}
		if(employee.getAadhar() != null){
			result.put(CommonConstants.AADHAR, String.valueOf(employee.getAadhar()));
		}else{
			result.put(CommonConstants.AADHAR,"");
		}
		if(employee.getPanno() != null){
			result.put(CommonConstants.PANNO, String.valueOf(employee.getPanno()));
		}else{
			result.put(CommonConstants.PANNO,"");
		}
		if(employee.getDateofbirth() != null){
			result.put(CommonConstants.DATEOFBIRTH, String.valueOf(employee.getDateofbirth()));
		}else{
			result.put(CommonConstants.DATEOFBIRTH,"");
		}
		result.put(CommonConstants.GENDER, String.valueOf(employee.getGender()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(employee.getUpdatedon())));
		if(employee.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(employee.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.STATUS,String.valueOf(employee.getStatus()));
		return result;
	}
	
}
