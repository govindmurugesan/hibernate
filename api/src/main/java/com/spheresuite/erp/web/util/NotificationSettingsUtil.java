package com.spheresuite.erp.web.util;

import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.NotificationSettingsDO;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.util.CommonConstants;
@Component
public class NotificationSettingsUtil {
	
	private NotificationSettingsUtil() {}
	
	@Autowired
	private  EmployeeService temployeeService;
	private static EmployeeService employeeService;
	
	@PostConstruct
	public void init() {
		employeeService = temployeeService;
	}
	
	
	public static JSONObject getNotificationSettingsList(List<NotificationSettingsDO> notificationSettingsList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (NotificationSettingsDO notificationSettingsDO : notificationSettingsList) {
				resultJSONArray.put(getNotificationSettingsDetailObject(notificationSettingsDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getNotificationSettingsDetailObject(NotificationSettingsDO notificationSettingsDO)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(notificationSettingsDO.getNotificationSettingId() != null ? notificationSettingsDO.getNotificationSettingId() : ""));
		JSONArray hrRequestApproverList= new JSONArray();
		if(notificationSettingsDO.getHrRequest() != null){
			List<String> hrRequestList = Arrays.asList(notificationSettingsDO.getHrRequest().split(CommonConstants.COMMA));
			if(hrRequestList !=null && hrRequestList.size() > 0){
				for(String approver : hrRequestList ){
					List<EmployeeDO> empList = employeeService.retriveByEmpId(approver.toString());
					if(empList != null && empList.size() > 0){
							JSONObject hrRequestApprover = new JSONObject();
							hrRequestApprover.put(CommonConstants.EMPID, String.valueOf(empList.get(0).getEmpId()));
							hrRequestApprover.put(CommonConstants.FIRSTNAME, String.valueOf(empList.get(0).getFirstname()));
							if(empList.get(0).getMiddlename() != null) hrRequestApprover.put(CommonConstants.MIDDLENAME, String.valueOf(empList.get(0).getMiddlename()));
							if(empList.get(0).getLastname() != null) hrRequestApprover.put(CommonConstants.LASTNAME, String.valueOf(empList.get(0).getLastname()));
							hrRequestApproverList.put(hrRequestApprover);
					}
				}
			}
		}
		result.put(CommonConstants.HRREQUEST, hrRequestApproverList);
		JSONArray leavesApproverList= new JSONArray();
		if(notificationSettingsDO.getLeaves() != null){
			List<String> leaveList = Arrays.asList(notificationSettingsDO.getLeaves().split(CommonConstants.COMMA));
			if(leaveList !=null && leaveList.size() > 0){
				for(String approver : leaveList ){
					List<EmployeeDO> empList = employeeService.retriveByEmpId(approver.toString());
					if(empList != null && empList.size() > 0){
							JSONObject leaveRequestApprover = new JSONObject();
							leaveRequestApprover.put(CommonConstants.EMPID, String.valueOf(empList.get(0).getEmpId()));
							leaveRequestApprover.put(CommonConstants.FIRSTNAME, String.valueOf(empList.get(0).getFirstname()));
							if(empList.get(0).getMiddlename() != null)leaveRequestApprover.put(CommonConstants.MIDDLENAME, String.valueOf(empList.get(0).getMiddlename()));
							if(empList.get(0).getLastname() != null) leaveRequestApprover.put(CommonConstants.LASTNAME, String.valueOf(empList.get(0).getLastname()));
							leavesApproverList.put(leaveRequestApprover);
					}
				}
			}
		}
		result.put(CommonConstants.LEAVES, leavesApproverList);
		JSONArray payrollApproverList= new JSONArray();
		if(notificationSettingsDO.getPayroll() != null){
			List<String> payrollList = Arrays.asList(notificationSettingsDO.getPayroll().split(CommonConstants.COMMA));
			if(payrollList !=null && payrollList.size() > 0){
				for(String approver : payrollList ){
					List<EmployeeDO> empList = employeeService.retriveByEmpId(approver.toString());
					if(empList != null && empList.size() > 0){
							JSONObject payrollRequestApprover = new JSONObject();
							payrollRequestApprover.put(CommonConstants.EMPID, String.valueOf(empList.get(0).getEmpId()));
							payrollRequestApprover.put(CommonConstants.FIRSTNAME, String.valueOf(empList.get(0).getFirstname()));
							if(empList.get(0).getMiddlename() != null) payrollRequestApprover.put(CommonConstants.MIDDLENAME, String.valueOf(empList.get(0).getMiddlename()));
							if(empList.get(0).getLastname() != null)payrollRequestApprover.put(CommonConstants.LASTNAME, String.valueOf(empList.get(0).getLastname()));
							payrollApproverList.put(payrollRequestApprover);
					}
				}
			}
		}
		result.put(CommonConstants.PAYROLL, payrollApproverList);
		JSONArray hoildayApproverList= new JSONArray();
		if(notificationSettingsDO.getHolidays() != null){
			List<String> holidayList = Arrays.asList(notificationSettingsDO.getHolidays().split(CommonConstants.COMMA));
			if(holidayList !=null && holidayList.size() > 0){
				for(String approver : holidayList ){
					List<EmployeeDO> empList = employeeService.retriveByEmpId(approver.toString());
					if(empList != null && empList.size() > 0){
							JSONObject holidayApprover = new JSONObject();
							holidayApprover.put(CommonConstants.EMPID, String.valueOf(empList.get(0).getEmpId()));
							holidayApprover.put(CommonConstants.FIRSTNAME, String.valueOf(empList.get(0).getFirstname()));
							if(empList.get(0).getMiddlename() != null) holidayApprover.put(CommonConstants.MIDDLENAME, String.valueOf(empList.get(0).getMiddlename()));
							if(empList.get(0).getLastname() != null) holidayApprover.put(CommonConstants.LASTNAME, String.valueOf(empList.get(0).getLastname()));
							hoildayApproverList.put(holidayApprover);
					}
				}
			}
		}
		result.put(CommonConstants.HOLIDAYS, hoildayApproverList);
		JSONArray hrpoliciesApproverList= new JSONArray();
		if(notificationSettingsDO.getHrpolicies() != null){
			List<String> hrPolicyList = Arrays.asList(notificationSettingsDO.getHrpolicies().split(CommonConstants.COMMA));
			if(hrPolicyList !=null && hrPolicyList.size() > 0){
				for(String approver : hrPolicyList ){
					List<EmployeeDO> empList = employeeService.retriveByEmpId(approver.toString());
					if(empList != null && empList.size() > 0){
							JSONObject hrPoliciesApprover = new JSONObject();
							hrPoliciesApprover.put(CommonConstants.EMPID, String.valueOf(empList.get(0).getEmpId()));
							hrPoliciesApprover.put(CommonConstants.FIRSTNAME, String.valueOf(empList.get(0).getFirstname()));
							if(empList.get(0).getMiddlename() != null) hrPoliciesApprover.put(CommonConstants.MIDDLENAME, String.valueOf(empList.get(0).getMiddlename()));
							if(empList.get(0).getLastname() != null) hrPoliciesApprover.put(CommonConstants.LASTNAME, String.valueOf(empList.get(0).getLastname()));
							hrpoliciesApproverList.put(hrPoliciesApprover);
					}
				}
			}
		}
		
		result.put(CommonConstants.HRPOLICY, hrpoliciesApproverList);
		//result.put(CommonConstants.HRREQUEST, String.valueOf(notificationSettingsDO.getHrRequest() != null ? notificationSettingsDO.getHrRequest() : ""));
		//result.put(CommonConstants.PAYROLL, String.valueOf(notificationSettingsDO.getPayroll() != null ? notificationSettingsDO.getPayroll() : ""));
		//result.put(CommonConstants.LEAVES, String.valueOf(notificationSettingsDO.getLeaves() != null ? notificationSettingsDO.getLeaves() : ""));
		//result.put(CommonConstants.HOLIDAYS, String.valueOf(notificationSettingsDO.getHolidays() != null ? notificationSettingsDO.getHolidays() : ""));
		//result.put(CommonConstants.HRPOLICY, String.valueOf(notificationSettingsDO.getHrpolicies() != null ? notificationSettingsDO.getHrpolicies() : ""));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(notificationSettingsDO.getUpdatedon())));
		if(notificationSettingsDO.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(notificationSettingsDO.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		return result;
	}
}
