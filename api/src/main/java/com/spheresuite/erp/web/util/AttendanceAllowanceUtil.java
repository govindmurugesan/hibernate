package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.AttendanceAllowanceDO;
import com.spheresuite.erp.util.CommonConstants;

@Component
public class AttendanceAllowanceUtil {
	
	private AttendanceAllowanceUtil() {}
	
	public static JSONObject getAttendanceAllowanceList(List<AttendanceAllowanceDO> attendanceAllowanceList){
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (AttendanceAllowanceDO attendanceAllowance : attendanceAllowanceList) {
				resultJSONArray.put(getAttendanceDetailObject(attendanceAllowance));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getAttendanceDetailObject(AttendanceAllowanceDO attendanceAllowance)throws JSONException{
		
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(attendanceAllowance.getAttendanceAllowanceId()));
		result.put(CommonConstants.NAME, String.valueOf(attendanceAllowance.getName()));
		result.put(CommonConstants.AMOUNT, String.valueOf(attendanceAllowance.getAmount()));
		result.put(CommonConstants.STATUS, String.valueOf(attendanceAllowance.getStatus()));
		if(attendanceAllowance.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(attendanceAllowance.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(attendanceAllowance.getUpdatedon())));
		return result;
	}
}
