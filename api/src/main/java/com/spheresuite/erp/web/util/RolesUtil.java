package com.spheresuite.erp.web.util;

import java.util.List;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.AssignedMenuDO;
import com.spheresuite.erp.domainobject.RolesDO;
import com.spheresuite.erp.service.AssignedMenuService;
import com.spheresuite.erp.util.CommonConstants;

@Component
public class RolesUtil {
	
	@Autowired
	private  AssignedMenuService assignedMenuService1;
	
	private static AssignedMenuService assignedMenuService;
	  
	@PostConstruct
	public void init() {
		assignedMenuService = this.assignedMenuService1;
	}
	
	public static JSONObject getRolesList(List<RolesDO> rolesList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (RolesDO role : rolesList) {
				resultJSONArray.put(getRoleDetailObject(role));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getRoleDetailObject(RolesDO role)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(role.getRoleId()));
		result.put(CommonConstants.NAME, String.valueOf(role.getName()));
		result.put(CommonConstants.STATUS, String.valueOf(role.getStatus()));
		result.put(CommonConstants.DESCRIPTION, String.valueOf(role.getDescription() != null ? role.getDescription() : ""));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(role.getUpdatedon())));
		if(role.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(role.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		//AssignedMenuDAO  obj = new AssignedMenuDAO();
		List<AssignedMenuDO> list = assignedMenuService.retrieveByRoleId(role.getRoleId());
		JSONArray resultJSONArray = new JSONArray();
		if(list != null && list.size() > 0){
			for (AssignedMenuDO l : list) {
				resultJSONArray.put(getMenuObject(l));
			}
		}
		result.put(CommonConstants.MENU, resultJSONArray);
		return result;
	}
	
	public static JSONObject getMenuObject(AssignedMenuDO menu)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.SUB_MENU, String.valueOf(menu.getMenu().getMenuId()));
		return result;
	}
	
}
