package com.spheresuite.erp.web.util;

import java.util.List;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.crm.service.ProjectService;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.ProjectDO;
import com.spheresuite.erp.domainobject.TimesheetDO;
import com.spheresuite.erp.domainobject.WeeklyTimesheetDO;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.TimesheetService;
import com.spheresuite.erp.util.CommonConstants;

@Component
public class WeeklyTimesheetUtil {
	
	private WeeklyTimesheetUtil() {}
	
	@Autowired
	private EmployeeService employeeServiceTemp;
	
	@Autowired
	private ProjectService projectServiceTemp;
	
	@Autowired
	private TimesheetService timesheetServiceTemp;
	
	private static TimesheetService timesheetService;
	
	private static EmployeeService employeeService;
	
	private static ProjectService projectService;
	
	@PostConstruct
	public void init() {
		employeeService = this.employeeServiceTemp;
		projectService = this.projectServiceTemp;
		timesheetService = this.timesheetServiceTemp;
	}
	
	public static JSONObject WeeklyTimesheetList(List<WeeklyTimesheetDO> timesheetList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (WeeklyTimesheetDO timesheet : timesheetList) {
				resultJSONArray.put(getTimesheetDetailObject(timesheet));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getTimesheetDetailObject(WeeklyTimesheetDO timesheet)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(timesheet.getWeeklyTimesheetId()));
		result.put(CommonConstants.DATE, String.valueOf(timesheet.getDate()));
		result.put(CommonConstants.HOURS, String.valueOf(timesheet.getHours()));
		result.put(CommonConstants.STATUS, String.valueOf(timesheet.getStatus()));
		if(timesheet.getUpdatedby() != null){
			if(timesheet.getUpdatedby() != null){
				String empName = CommonUtil.getEmpObject(timesheet.getUpdatedby());
				result.put(CommonConstants.UPDATED_BY, empName); 
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
			
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		return result;
	}
	
	public static JSONObject EmpWeeklyTimesheetList(List<WeeklyTimesheetDO> weeklyTimesheetList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (WeeklyTimesheetDO weeklytimesheet : weeklyTimesheetList) {
				resultJSONArray.put(getEmpTimesheetDetailObject(weeklytimesheet));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getEmpTimesheetDetailObject(WeeklyTimesheetDO weeklytimesheet)throws JSONException {
		JSONObject result = new JSONObject();
		List<TimesheetDO> timesheetList = timesheetService.retrieveByWeekendDate(weeklytimesheet.getDate(), weeklytimesheet.getEmpId());
		int totalHours = 0;
		JSONArray resultJSONArray = new JSONArray();
		if(timesheetList != null && timesheetList.size() > 0){
			for (TimesheetDO timesheet : timesheetList) {
				resultJSONArray.put(getEmpTimesheetObj(timesheet));
				totalHours = (int) (totalHours + timesheet.getHours());
			}
		}
		result.put(CommonConstants.TIMESHEETLIST, resultJSONArray);
		result.put(CommonConstants.WEEKENDDATE, String.valueOf(weeklytimesheet.getDate()));
		result.put(CommonConstants.TOTAl, totalHours);
		result.put(CommonConstants.STATUS, String.valueOf(weeklytimesheet.getStatus()));
		if(weeklytimesheet.getEmpId() != null){
			if(weeklytimesheet.getEmpId() != null){
				String empName = CommonUtil.getEmpObject(weeklytimesheet.getEmpId());
				result.put(CommonConstants.CREATED_BY, empName); 
				result.put(CommonConstants.EMPID, weeklytimesheet.getEmpId());
			}else{
				result.put(CommonConstants.CREATED_BY, ""); 
				result.put(CommonConstants.EMPID, "");
			}
		}else{
			result.put(CommonConstants.CREATED_BY, ""); 
			result.put(CommonConstants.EMPID, "");
		}
		
		if(timesheetList.size() > 0 &&  timesheetList.get(0).getUpdatedby() != null){
			List<EmployeeDO> empList = employeeService.retrieveEmpId(timesheetList.get(0).getUpdatedby().toString());
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		return result;
		
		
	}
	
	public static JSONObject getEmpTimesheetObj(TimesheetDO timesheet)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(timesheet.getTimesheetId()));
		result.put(CommonConstants.DATE, String.valueOf(timesheet.getDate()));
		result.put(CommonConstants.HOURS, String.valueOf(timesheet.getHours()));
		result.put(CommonConstants.DESCRIPTION, String.valueOf(timesheet.getDescription()));
		result.put(CommonConstants.TYPE, String.valueOf(timesheet.getTimesheetType()));
		List<ProjectDO> projectList = projectService.retrieveById(timesheet.getTimesheetType().getTimesheetTypeId());
		if(projectList != null && projectList.size() > 0){
			result.put(CommonConstants.TYPENAME, String.valueOf(projectList.get(0).getProjectname()));
		}else{
			result.put(CommonConstants.TYPENAME, String.valueOf(""));
		}
		result.put(CommonConstants.WEEKENDDATE, String.valueOf(timesheet.getWeeklyTimesheet().getDate()));
		//result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(timesheet.getUpdatedon())));
		
		if(timesheet.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(timesheet.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		return result;
	}
}
