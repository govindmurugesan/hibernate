package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.EarnedLeavesDO;
import com.spheresuite.erp.util.CommonConstants;
@Component
public class EarnedLeavesUtil {
	
	private EarnedLeavesUtil() {}

	public static JSONObject getEmployeeEarnedLeavesList(List<EarnedLeavesDO> earnedLeaveList){
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EarnedLeavesDO earnedLeave : earnedLeaveList) {
				resultJSONArray.put(getEmployeeEarnedLaeveDetailObject(earnedLeave));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getEmployeeEarnedLaeveDetailObject(EarnedLeavesDO earnedLeave)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(earnedLeave.getEarnedLeaveId()));
		if(earnedLeave.getAvailableLeaves() != null){
			result.put(CommonConstants.AVAILABLELEAVES, String.valueOf(earnedLeave.getAvailableLeaves()));
		}else{
			result.put(CommonConstants.AVAILABLELEAVES, "");
		}
		/*if(earnedLeave.getAvailedLeaves() != null){
			result.put(CommonConstants.AVAILEDLEAVES, String.valueOf(earnedLeave.getAvailedLeaves()));
		}else{
			result.put(CommonConstants.AVAILEDLEAVES, "");
		}*/
		if(earnedLeave.getFromDate() != null){
			result.put(CommonConstants.FROMDATE, String.valueOf(CommonUtil.convertDateToStringApp(earnedLeave.getFromDate())));
		}else{
			result.put(CommonConstants.FROMDATE, "");
		}
		if(earnedLeave.getFromDate() != null){
			result.put(CommonConstants.TODATE, String.valueOf(CommonUtil.convertDateToStringApp(earnedLeave.getToDate())));
		}else{
			result.put(CommonConstants.TODATE, "");
		}
		
		if(earnedLeave.getEmployee().getEmpId() != null){
			String empName = CommonUtil.getEmpObject(earnedLeave.getEmployee().getEmpId());
			result.put(CommonConstants.EMPID, String.valueOf(earnedLeave.getEmployee().getEmpId()));
			result.put(CommonConstants.EMPNAME, empName); 
		}else{
			result.put(CommonConstants.EMPID, "");
			result.put(CommonConstants.EMPNAME, ""); 
		}
		
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(earnedLeave.getUpdatedon())));
		if(earnedLeave.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(earnedLeave.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.CREATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(earnedLeave.getCreatedon())));
		if(earnedLeave.getCreatedby() != null){
			String empName = CommonUtil.getEmpObject(earnedLeave.getCreatedby());
			result.put(CommonConstants.CREATED_BY, empName); 
		}else{
			result.put(CommonConstants.CREATED_BY, ""); 
		}
		return result;
	}
}
