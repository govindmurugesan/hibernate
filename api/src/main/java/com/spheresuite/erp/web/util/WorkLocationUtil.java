package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.WorkLocationDO;
import com.spheresuite.erp.util.CommonConstants;

@Component
public class WorkLocationUtil {
	
	private WorkLocationUtil() {}
	
	public static JSONObject getWorkLocationList(List<WorkLocationDO> worklocationList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (WorkLocationDO worklocation : worklocationList) {
				resultJSONArray.put(getUnitOrBranchDetailObject(worklocation));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getUnitOrBranchDetailObject(WorkLocationDO worklocation)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(worklocation.getWorklocationId()));
		result.put(CommonConstants.CITY, String.valueOf(worklocation.getWorklocation()));
		result.put(CommonConstants.COUNTRY_ID, String.valueOf(worklocation.getCountry().getCountryId()));
		result.put(CommonConstants.STATE_ID, String.valueOf(worklocation.getState().getStateId()));
		result.put(CommonConstants.STATUS, String.valueOf(worklocation.getStatus()));
		result.put(CommonConstants.ADDRESS, String.valueOf(worklocation.getAddress()));
		result.put(CommonConstants.COUNTRY_NAME, String.valueOf(worklocation.getCountry().getCountryName()));
		result.put(CommonConstants.STATE_NAME, String.valueOf(worklocation.getState().getStateName()));
		if(worklocation.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(worklocation.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(worklocation.getUpdatedon())));
		return result;
	}
}
