package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.ESIGroupDO;
import com.spheresuite.erp.domainobject.ESIGroupEarningsDO;
import com.spheresuite.erp.util.CommonConstants;

@Component
public class ESIGroupUtil {
	
	private ESIGroupUtil() {}
	
	public static JSONObject getESIGroupListDetails(List<ESIGroupDO> esiGroupList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (ESIGroupDO esiGroup : esiGroupList) {
				resultJSONArray.put(getESIGroupWithAllDetailObject(esiGroup));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getESIGroupWithAllDetailObject(List<ESIGroupDO> esiGroup) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (ESIGroupDO esiGroupData : esiGroup) {
				resultJSONArray.put(getesiGroupDetailObject(esiGroupData));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getesiGroupDetailObject(ESIGroupDO esiGroup)throws JSONException{
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(esiGroup.getEsiGroupId()));
		result.put(CommonConstants.NAME, String.valueOf(esiGroup.getName()));
		if(esiGroup.getDescription() != null){
			result.put(CommonConstants.DESC, String.valueOf(esiGroup.getDescription()));
		}else{
			result.put(CommonConstants.DESC, "");
		}
		if(esiGroup.getAmount() != null){
			result.put(CommonConstants.AMOUNT, String.valueOf(esiGroup.getAmount()));
		}else{
			result.put(CommonConstants.AMOUNT, "");
		}
		result.put(CommonConstants.STATUS, String.valueOf(esiGroup.getStatus()));
		if(esiGroup.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(esiGroup.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		if(esiGroup.getUpdatedon() != null) result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(esiGroup.getUpdatedon())));
		return result;
	}
	
	
	public static JSONObject getESIGroupWithAllDetailObject(ESIGroupDO esiGroup)throws JSONException{
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(esiGroup.getEsiGroupId()));
		result.put(CommonConstants.NAME, String.valueOf(esiGroup.getName()));
		JSONArray payrollGroupEarningDetailsJSONArray= new JSONArray();
		if(esiGroup.getEsiGroupEarnings() != null){
			if(esiGroup.getEsiGroupEarnings().size() >0){
				for(ESIGroupEarningsDO earningGrp : esiGroup.getEsiGroupEarnings()){
					JSONObject payrollGroupEarningDetails = new JSONObject();
					payrollGroupEarningDetails.put(CommonConstants.ID, String.valueOf(earningGrp.getEsiGroupEarningsId()));
					payrollGroupEarningDetails.put(CommonConstants.ALLOWENCE_ID, String.valueOf(earningGrp.getAllowanceSettings().getAllowanceSettingsId()));
					payrollGroupEarningDetails.put(CommonConstants.NAME, String.valueOf(earningGrp.getAllowanceSettings().getDisplayname()));
					payrollGroupEarningDetails.put(CommonConstants.STATUS, String.valueOf(earningGrp.getStatus()));
					payrollGroupEarningDetailsJSONArray.put(payrollGroupEarningDetails);
				}
			}
		}
		if(esiGroup.getAmount() != null){
			result.put(CommonConstants.AMOUNT, String.valueOf(esiGroup.getAmount()));
		}else{
			result.put(CommonConstants.AMOUNT, "");
		}
		if(esiGroup.getDescription() != null){
			result.put(CommonConstants.DISPLAYNAME, String.valueOf(esiGroup.getDescription()));
		}else{
			result.put(CommonConstants.DISPLAYNAME, "");
		}
		result.put(CommonConstants.STATUS, String.valueOf(esiGroup.getStatus()));
		result.put(CommonConstants.EARNINGID, payrollGroupEarningDetailsJSONArray);
		if(esiGroup.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(esiGroup.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(esiGroup.getUpdatedon())));
		return result;
	}
}
