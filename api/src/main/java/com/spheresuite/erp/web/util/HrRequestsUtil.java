package com.spheresuite.erp.web.util;

import java.util.List;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.HrRequestDocDO;
import com.spheresuite.erp.domainobject.HrRequestsDO;
import com.spheresuite.erp.service.HrRequestDocService;
import com.spheresuite.erp.util.CommonConstants;
@Component
public class HrRequestsUtil {
	
	@Autowired
	private  HrRequestDocService thrRequestDocService;
	private static HrRequestDocService hrRequestDocService;
	
	@PostConstruct
	public void init() {
		hrRequestDocService=thrRequestDocService;
	}
	
	private HrRequestsUtil() {}
	
	public static JSONObject getHrRequestsList(List<HrRequestsDO> hrRequestsList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (HrRequestsDO hrRequest : hrRequestsList) {
				resultJSONArray.put(getHrRequestsDetailObject(hrRequest));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getHrRequestsDetailObject(HrRequestsDO hrRequest)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(hrRequest.getHrRequestId()));
		result.put(CommonConstants.DESC, String.valueOf(hrRequest.getDescription()));
		
		if(hrRequest.getHrRequestType() != null){
			result.put(CommonConstants.TYPE, String.valueOf(hrRequest.getHrRequestType().getHrRequestTypeId()));
			result.put(CommonConstants.TYPENAME, String.valueOf(hrRequest.getHrRequestType().getRequestType()));
		}else{
			result.put(CommonConstants.TYPE, "");
			result.put(CommonConstants.TYPENAME, "");
		}
		if(hrRequest.getDepartment() != null){
			result.put(CommonConstants.DEPT, String.valueOf(hrRequest.getDepartment().getDepartmentId()));
			result.put(CommonConstants.DEPT_NAME, String.valueOf(hrRequest.getDepartment().getName()));
		}else{
			result.put(CommonConstants.DEPT, "");
			result.put(CommonConstants.DEPT_NAME, "");
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(hrRequest.getUpdatedon())));
		result.put(CommonConstants.CREATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(hrRequest.getCreatedon())));
		result.put(CommonConstants.STATUS, String.valueOf(hrRequest.getStatus()));
		if(hrRequest.getComments() != null ){
			result.put(CommonConstants.COMMENT, String.valueOf(hrRequest.getComments()));
		}else{
			result.put(CommonConstants.COMMENT, "");
		}
		if(hrRequest.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(hrRequest.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		if(hrRequest.getCreatedby() != null){
			String empName = CommonUtil.getEmpObject(hrRequest.getCreatedby());
			result.put(CommonConstants.CREATED_BY, String.valueOf(empName));
			result.put(CommonConstants.CREATED_BY_ID, hrRequest.getCreatedby());
		}else{
			result.put(CommonConstants.CREATED_BY, "");
			result.put(CommonConstants.CREATED_BY_ID, "");
		}
		JSONArray resultJSONArray = new JSONArray();
		List<HrRequestDocDO> hrRequestDocList = hrRequestDocService.retrieveByOppId(hrRequest.getHrRequestId());
		if(hrRequestDocList != null && hrRequestDocList.size()  > 0){
			for (HrRequestDocDO doc : hrRequestDocList) {
				resultJSONArray.put(getDocDetailObject(doc));
			}
		}
		result.put(CommonConstants.FILE, resultJSONArray);
		return result;
	}
	
	public static JSONObject getDocDetailObject(HrRequestDocDO doc)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.FILE, String.valueOf(doc.getPhoto()));
		if(doc.getFileName() != null){
			result.put(CommonConstants.NAME, String.valueOf(doc.getFileName()));
		}else{
			result.put(CommonConstants.NAME, "");	
		}
		
		return result;
	}
	
	public static JSONObject notApproverResponse() {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
}
