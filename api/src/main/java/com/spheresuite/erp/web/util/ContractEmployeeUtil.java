package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.AddressTypeDO;
import com.spheresuite.erp.domainobject.ContractEmployeeDO;
import com.spheresuite.erp.util.CommonConstants;

@Component
public class ContractEmployeeUtil {
	
	private ContractEmployeeUtil() {}
	
	public static JSONObject getContractLabourList(List<ContractEmployeeDO> contractLaboursList){
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (ContractEmployeeDO contractLabours : contractLaboursList) {
				resultJSONArray.put(getContractDetailObject(contractLabours));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getContractDetailObject(ContractEmployeeDO contractLabours)throws JSONException{
		
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(contractLabours.getContractLabourId()));
		result.put(CommonConstants.NAME, String.valueOf(contractLabours.getName()));
		if(contractLabours.getSupplier() != null){
			result.put(CommonConstants.SUPPLIERID, contractLabours.getSupplier().getSupplierId()); 
			result.put(CommonConstants.SUPPLIERNAME, contractLabours.getSupplier().getName()); 
			
		}else{
			result.put(CommonConstants.SUPPLIERNAME, ""); 
			result.put(CommonConstants.SUPPLIERID, ""); 
		}
		
		if(contractLabours.getEmpId() != null){
			result.put(CommonConstants.EMPID, contractLabours.getEmpId()); 
		}else{
			result.put(CommonConstants.EMPID, ""); 
		}
		if(contractLabours.getAadharNo() != null){
			result.put(CommonConstants.AADHARNO, String.valueOf(contractLabours.getAadharNo()));
		}else{
			result.put(CommonConstants.AADHARNO,"");
		}
		if(contractLabours.getPanNo() != null){
			result.put(CommonConstants.PAN_NO, String.valueOf(contractLabours.getPanNo()));
		}else{
			result.put(CommonConstants.PAN_NO,"");
		}
		if(contractLabours.getDob() != null){
			result.put(CommonConstants.DOB, String.valueOf(contractLabours.getDob()));
		}else{
			result.put(CommonConstants.DOB,"");
		}
		if(contractLabours.getDoj() != null){
			result.put(CommonConstants.DOJ, String.valueOf(contractLabours.getDoj()));
		}else{
			result.put(CommonConstants.DOJ,"");
		}
		result.put(CommonConstants.GENDER, String.valueOf(contractLabours.getGender()));
		if(contractLabours.getDepartment() != null){
			result.put(CommonConstants.DEPARTMENT, String.valueOf(contractLabours.getDepartment().getDepartmentId()));
			result.put(CommonConstants.DEPT_NAME, String.valueOf(contractLabours.getDepartment().getName()));
		}
		if(contractLabours.getDesignation()!= null){
			result.put(CommonConstants.DESIGNATION, String.valueOf(contractLabours.getDesignation().getDesignationId()));
		}
		result.put(CommonConstants.FATHERNAME, String.valueOf(contractLabours.getFatherName()));
		result.put(CommonConstants.SPOUSENAME, String.valueOf(contractLabours.getSpouseName()));
		result.put(CommonConstants.MARITALSTATUS, String.valueOf(contractLabours.getMaritalStatus()));
		result.put(CommonConstants.CAT, String.valueOf(contractLabours.getCat()));
		result.put(CommonConstants.QUALIFICATION, String.valueOf(contractLabours.getQualification()));
		result.put(CommonConstants.BLOODGROUP, String.valueOf(contractLabours.getBloodGroup()));
		
		result.put(CommonConstants.WAGES, String.valueOf(contractLabours.getWages()));
		
		result.put(CommonConstants.CONTACTNO, String.valueOf(contractLabours.getContactNo()));
		result.put(CommonConstants.EMERGENCY_CONTACTNO, String.valueOf(contractLabours.getEmergencyContactNo()));
		result.put(CommonConstants.PREVIOUSEXPERIENCE, String.valueOf(contractLabours.getPreviousExperience()));
		result.put(CommonConstants.BANKNAME, String.valueOf(contractLabours.getBankName()));
		result.put(CommonConstants.IFSCCODE, String.valueOf(contractLabours.getIfsc()));
		result.put(CommonConstants.BANKACNO, String.valueOf(contractLabours.getBankAcNo()));
		
		result.put(CommonConstants.PRESENTADDRESS, String.valueOf(contractLabours.getPresentAddress()));
		result.put(CommonConstants.PERMANENTADDRESS, String.valueOf(contractLabours.getPermanentAddress()));
		
		if(contractLabours.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(contractLabours.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(contractLabours.getUpdatedon())));
		return result;
	}
}
