package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.PFGroupDO;
import com.spheresuite.erp.domainobject.PFGroupEarningsDO;
import com.spheresuite.erp.util.CommonConstants;

@Component
public class PFGroupUtil {
	
	private PFGroupUtil() {}
	
	
	
	public static JSONObject getPFGroupListDetails(List<PFGroupDO> pfGroupList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (PFGroupDO pfGroup : pfGroupList) {
				resultJSONArray.put(getPayrollGroupWithAllDetailObject(pfGroup));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getPFGroupWithAllDetailObject(List<PFGroupDO> pfGroup) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (PFGroupDO pfGroupData : pfGroup) {
				resultJSONArray.put(getPfGroupDetailObject(pfGroupData));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getPfGroupDetailObject(PFGroupDO pfGroup)throws JSONException{
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(pfGroup.getPfGroupId()));
		result.put(CommonConstants.NAME, String.valueOf(pfGroup.getName()));
		/*if(payrollGroup.getPayrollGroupEarning() != null){
			if(payrollGroup.getPayrollGroupEarning().size() >0){
				for(PayrollGroupEarningsDO grp1 : payrollGroup.getPayrollGroupEarning()){
					System.out.println("grp1"+grp1.getAllowanceSettings().getName());
				}
			}
		}*/
		if(pfGroup.getDescription() != null){
			result.put(CommonConstants.DESC, String.valueOf(pfGroup.getDescription()));
		}else{
			result.put(CommonConstants.DESC, "");
		}
		result.put(CommonConstants.STATUS, String.valueOf(pfGroup.getStatus()));
		if(pfGroup.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(pfGroup.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		if(pfGroup.getUpdatedon() != null) result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(pfGroup.getUpdatedon())));
		return result;
	}
	
	
	public static JSONObject getPayrollGroupWithAllDetailObject(PFGroupDO pfGroup)throws JSONException{
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(pfGroup.getPfGroupId()));
		result.put(CommonConstants.NAME, String.valueOf(pfGroup.getName()));
		JSONArray payrollGroupEarningDetailsJSONArray= new JSONArray();
		if(pfGroup.getPfGroupEarning() != null){
			if(pfGroup.getPfGroupEarning().size() >0){
				for(PFGroupEarningsDO earningGrp : pfGroup.getPfGroupEarning()){
					JSONObject payrollGroupEarningDetails = new JSONObject();
					payrollGroupEarningDetails.put(CommonConstants.ID, String.valueOf(earningGrp.getPfGroupEarningsId()));
					payrollGroupEarningDetails.put(CommonConstants.ALLOWENCE_ID, String.valueOf(earningGrp.getAllowanceSettings().getAllowanceSettingsId()));
					payrollGroupEarningDetails.put(CommonConstants.NAME, String.valueOf(earningGrp.getAllowanceSettings().getDisplayname()));
					payrollGroupEarningDetails.put(CommonConstants.STATUS, String.valueOf(earningGrp.getStatus()));
					payrollGroupEarningDetailsJSONArray.put(payrollGroupEarningDetails);
				}
			}
		}
		/*JSONArray payrollGroupDeductionDetailsJSONArray= new JSONArray();
		if(pfGroup.getPayrollGroupDeduction() != null){
			if(pfGroup.getPayrollGroupDeduction().size() >0){
				for(PayrollGroupDeductionDO deductionGrp : pfGroup.getPayrollGroupDeduction()){
					JSONObject payrollGroupDeductionDetails = new JSONObject();
					payrollGroupDeductionDetails.put(CommonConstants.ID, String.valueOf(deductionGrp.getPayrollGroupDeductionId()));
					payrollGroupDeductionDetails.put(CommonConstants.DEDUCTIONID, String.valueOf(deductionGrp.getDeductionSettings().getDeductionsettingId()));
					payrollGroupDeductionDetails.put(CommonConstants.NAME, String.valueOf(deductionGrp.getDeductionSettings().getDisplayname()));
					payrollGroupDeductionDetails.put(CommonConstants.STATUS, String.valueOf(deductionGrp.getStatus()));
					payrollGroupDeductionDetailsJSONArray.put(payrollGroupDeductionDetails);
				}
			}
		}
		JSONArray payrollGroupTaxDetailsJSONArray= new JSONArray();
		if(pfGroup.getPayrollGroupTax() != null){
			if(pfGroup.getPayrollGroupTax().size() >0){
				for(PayrollGroupTaxDO taxGrp : pfGroup.getPayrollGroupTax()){
					JSONObject payrollGroupTaxDetails = new JSONObject();
					if(taxGrp.getPt() != null && taxGrp.getPt().toString().equalsIgnoreCase(CommonConstants.APPLY)){
						if(taxGrp.getStatus() == CommonConstants.ACTIVE){
							result.put(CommonConstants.PT_ID,  String.valueOf("true"));
						}else{
							result.put(CommonConstants.PT_ID,  String.valueOf("false"));
						}
					}else if(taxGrp.getTds() != null && taxGrp.getTds().toString().equalsIgnoreCase(CommonConstants.APPLY)){
						if(taxGrp.getStatus() == CommonConstants.ACTIVE){
							result.put(CommonConstants.TDSID,  String.valueOf("true"));
						}else{
							result.put(CommonConstants.TDSID,  String.valueOf("false"));
						}
					}else if(taxGrp.getEducationCess() != null && taxGrp.getEducationCess().toString().equalsIgnoreCase(CommonConstants.APPLY)){
						if(taxGrp.getStatus() == CommonConstants.ACTIVE){
							result.put(CommonConstants.EDUCATIONCESS,  String.valueOf("true"));
						}else{
							result.put(CommonConstants.EDUCATIONCESS,  String.valueOf("false"));
						}
					}else{
						payrollGroupTaxDetails.put(CommonConstants.ID, String.valueOf(taxGrp.getPayrollGroupTaxId()));
						payrollGroupTaxDetails.put(CommonConstants.TAXID, String.valueOf(taxGrp.getTaxSettings().getTaxSettingId()));
						payrollGroupTaxDetails.put(CommonConstants.NAME, String.valueOf(taxGrp.getTaxSettings().getDisplayname()));
						payrollGroupTaxDetails.put(CommonConstants.STATUS, String.valueOf(taxGrp.getStatus()));
						payrollGroupTaxDetailsJSONArray.put(payrollGroupTaxDetails);
					}
					
				}
			}
		}*/
		
		if(pfGroup.getDescription() != null){
			result.put(CommonConstants.DISPLAYNAME, String.valueOf(pfGroup.getDescription()));
		}else{
			result.put(CommonConstants.DISPLAYNAME, "");
		}
		
		if(pfGroup.getMaxAmount() != null){
			result.put(CommonConstants.AMOUNT, String.valueOf(pfGroup.getMaxAmount()));
		}else{
			result.put(CommonConstants.AMOUNT, "");
		}
		result.put(CommonConstants.STATUS, String.valueOf(pfGroup.getStatus()));
		result.put(CommonConstants.EARNINGID, payrollGroupEarningDetailsJSONArray);
		/*result.put(CommonConstants.DEDUCTIONID, payrollGroupDeductionDetailsJSONArray);
		result.put(CommonConstants.TAXID, payrollGroupTaxDetailsJSONArray);*/
		
		
		if(pfGroup.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(pfGroup.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(pfGroup.getUpdatedon())));
		return result;
	}
}
