package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.PayrollBatchDO;
import com.spheresuite.erp.util.CommonConstants;
@Component
public class PayrollBatchUtil {
	
	private PayrollBatchUtil() {}
	
	public static JSONObject getBatchList(List<PayrollBatchDO> batchDOList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (PayrollBatchDO batchDO : batchDOList) {
				resultJSONArray.put(getBatchDetailObject(batchDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getBatchDetailObject(PayrollBatchDO batchDO)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(batchDO.getPayrollBatchId()));
		result.put(CommonConstants.NAME, String.valueOf(batchDO.getName()));
		if(batchDO.getDisplayname() != null){
			result.put(CommonConstants.DISPLAYNAME, String.valueOf(batchDO.getDisplayname()));
		}else{
			result.put(CommonConstants.DISPLAYNAME, "");
		}
		if(batchDO.getPayrollType() != null){
			result.put(CommonConstants.TYPE, String.valueOf(batchDO.getPayrollType()));
		}else{
			result.put(CommonConstants.TYPE, "");
		}
		if(batchDO.getOtCheck() != null){
			result.put(CommonConstants.OTCHECK, String.valueOf(batchDO.getOtCheck()));
		}else{
			result.put(CommonConstants.OTCHECK, "");
		}
		if(batchDO.getMinHrOT() != null){
			result.put(CommonConstants.MIN_HR_OT, String.valueOf(batchDO.getMinHrOT()));
		}else{
			result.put(CommonConstants.MIN_HR_OT, "");
		}
		result.put(CommonConstants.STATUS, String.valueOf(batchDO.getStatus()));
		if(batchDO.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(batchDO.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		
		if(batchDO.getUnitOrBranch() != null){
			result.put(CommonConstants.UNIT, String.valueOf(batchDO.getUnitOrBranch().getUnitOrBranchId()));
			result.put(CommonConstants.UNITNAME, String.valueOf(batchDO.getUnitOrBranch().getUnitOrBranch()+", "+batchDO.getUnitOrBranch().getState().getStateName()+", "+batchDO.getUnitOrBranch().getCountry().getCountryName()));
		}else{
			result.put(CommonConstants.UNIT, "");
			result.put(CommonConstants.UNITNAME,"");
		}
		
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(batchDO.getUpdatedon())));
		return result;
	}
}
