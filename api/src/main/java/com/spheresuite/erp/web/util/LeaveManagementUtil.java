package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.LeaveManagementDO;
import com.spheresuite.erp.util.CommonConstants;
@Component
public class LeaveManagementUtil {
	
	private LeaveManagementUtil() {}
	
	public static JSONObject getLeaveList(List<LeaveManagementDO> leaveLists) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (LeaveManagementDO leaveDetail : leaveLists) {
				resultJSONArray.put(getLeaveRequestsDetailObject(leaveDetail));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getLeaveRequestsDetailObject(LeaveManagementDO leaveDetail)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(leaveDetail.getLeaveManagementId()));
		if(leaveDetail.getNumberOfDays() != null){
			result.put(CommonConstants.DAYS, String.valueOf(leaveDetail.getNumberOfDays()));
		}else{
			result.put(CommonConstants.DAYS, String.valueOf(""));
		}
		
		if(leaveDetail.getDaysAllowed() != null){
			result.put(CommonConstants.DAYS_ALLOWED, String.valueOf(leaveDetail.getDaysAllowed()));
		}else{
			result.put(CommonConstants.DAYS_ALLOWED, "");
		}
		
		if(leaveDetail.getDividedBy() != null){
			result.put(CommonConstants.DIVIDEDBY, String.valueOf(leaveDetail.getDividedBy()));
		}else{
			result.put(CommonConstants.DIVIDEDBY, "");
		}
		
		if(leaveDetail.getMultiplyby() != null){
			result.put(CommonConstants.MULTIPLYBY, String.valueOf(leaveDetail.getMultiplyby()));
		}else{
			result.put(CommonConstants.MULTIPLYBY, "");
		}
		
		if(leaveDetail.getEarnedLeave() != null){
			result.put(CommonConstants.EARNEDLEAVE, String.valueOf(leaveDetail.getEarnedLeave()));
		}else{
			result.put(CommonConstants.EARNEDLEAVE, "");
		}
		
		result.put(CommonConstants.TYPE, String.valueOf(leaveDetail.getLeaveType()));
		/*result.put(CommonConstants.TYPE, String.valueOf(leaveDetail.getLeaveType().getLeaveTypeId()));
		result.put(CommonConstants.TYPENAME, String.valueOf(leaveDetail.getLeaveType().getType()));*/
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(leaveDetail.getUpdatedon())));
		result.put(CommonConstants.STATUS, String.valueOf(leaveDetail.getStatus()));
		result.put(CommonConstants.CARRYFORWORD, String.valueOf(leaveDetail.getCarryForward()));
		result.put(CommonConstants.COMPOFF, String.valueOf(leaveDetail.getCompoff()));
		result.put(CommonConstants.PERMISSION, String.valueOf(leaveDetail.getPermission()));
		result.put(CommonConstants.SATURDAY, String.valueOf(leaveDetail.getSaturday()));
		result.put(CommonConstants.SUNDAY, String.valueOf(leaveDetail.getSunday()));
		
		if(leaveDetail.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(leaveDetail.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		if(leaveDetail.getCreatedby() != null){
			String empName = CommonUtil.getEmpObject(leaveDetail.getCreatedby());
			result.put(CommonConstants.CREATED_BY, empName); 
		}else{
			result.put(CommonConstants.CREATED_BY, ""); 
		}
		if(leaveDetail.getFromDate()!= null){
			result.put(CommonConstants.FROMDATE, String.valueOf(CommonUtil.convertDateToStringWithOutComma(leaveDetail.getFromDate())));
		}else{
			result.put(CommonConstants.FROMDATE, "");
		}
		if(leaveDetail.getToDate() != null){
			result.put(CommonConstants.TODATE, String.valueOf(CommonUtil.convertDateToStringWithOutComma(leaveDetail.getToDate())));
		}else{
			result.put(CommonConstants.TODATE, "");
		}
		return result;
	}
}
