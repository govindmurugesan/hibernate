package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.EmployeeCompOffDO;
import com.spheresuite.erp.util.CommonConstants;

@Component
public class EmployeeCompOffUtil {
	
	private EmployeeCompOffUtil() {}
	
	public static JSONObject getEmployeeCompOffList(List<EmployeeCompOffDO> employeeCompOffList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeeCompOffDO employeeCompOffDO : employeeCompOffList) {
				resultJSONArray.put(getEmployeeCompOffDetailObject(employeeCompOffDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getEmployeeCompOffDetailObject(EmployeeCompOffDO employeeCompOffDO)throws JSONException{
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(employeeCompOffDO.getEmpCompOffId()));
		
		String name="";
		if(employeeCompOffDO.getEmployee().getFirstname() != null){
			result.put(CommonConstants.FIRSTNAME, String.valueOf(employeeCompOffDO.getEmployee().getFirstname()));
			name = name+employeeCompOffDO.getEmployee().getFirstname();
		}else{
			result.put(CommonConstants.FIRSTNAME,"");
		}
		if(employeeCompOffDO.getEmployee().getMiddlename() != null){
			result.put(CommonConstants.MIDDLENAME, String.valueOf(employeeCompOffDO.getEmployee().getMiddlename()));
			name = name+" "+employeeCompOffDO.getEmployee().getMiddlename();
		}else{
			result.put(CommonConstants.MIDDLENAME,"");
		}
		if(employeeCompOffDO.getEmployee().getLastname() != null){
			result.put(CommonConstants.LASTNAME, String.valueOf(employeeCompOffDO.getEmployee().getLastname()));
			name = name+" "+employeeCompOffDO.getEmployee().getLastname();
		}else{
			result.put(CommonConstants.LASTNAME,"");
		}
		
		result.put(CommonConstants.NAME, String.valueOf(name));
		result.put(CommonConstants.EMPID, String.valueOf(employeeCompOffDO.getEmployee().getEmpId()));
		
		result.put(CommonConstants.EXPIRYDATE, String.valueOf(CommonUtil.convertDateToStringWithdatetime(employeeCompOffDO.getExpiryDate())));
		result.put(CommonConstants.APPLYDATE, String.valueOf(CommonUtil.convertDateToStringWithdatetime(employeeCompOffDO.getApplyDate())));
		result.put(CommonConstants.COMPOFFTYPE, String.valueOf(employeeCompOffDO.getCompOffType()));
		result.put(CommonConstants.STATUS, String.valueOf(employeeCompOffDO.getStatus()));
		if(employeeCompOffDO.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(employeeCompOffDO.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(employeeCompOffDO.getUpdatedon())));
		return result;
	}
}
