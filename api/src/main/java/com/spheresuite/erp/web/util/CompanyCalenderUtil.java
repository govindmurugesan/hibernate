package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.CompanyCalenderDO;
import com.spheresuite.erp.util.CommonConstants;
@Component
public class CompanyCalenderUtil {
	
	private CompanyCalenderUtil() {}
	
	public static JSONObject getCalenderList(List<CompanyCalenderDO> calenderList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (CompanyCalenderDO calenderObject : calenderList) {
				resultJSONArray.put(getEmployeeTypeDetailObject(calenderObject));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getEmployeeTypeDetailObject(CompanyCalenderDO calenderObject)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(calenderObject.getCompanyCalenderId()));
	
		if(calenderObject.getMonth() != null){
			result.put(CommonConstants.MONTHLY, String.valueOf(calenderObject.getMonth()));
		}else{
			result.put(CommonConstants.MONTHLY, String.valueOf(""));
		}
		result.put(CommonConstants.DATE, String.valueOf(CommonUtil.convertDateToYearWithOutTime(calenderObject.getDate())));
		if(calenderObject.getPayrollBatch() != null){
			result.put(CommonConstants.PAYROLLBATCHID, String.valueOf(calenderObject.getPayrollBatch().getPayrollBatchId()));
			result.put(CommonConstants.PAYROLLBATCHNAME, String.valueOf(calenderObject.getPayrollBatch().getName()));
		}else{
			result.put(CommonConstants.PAYROLLBATCHID, String.valueOf(""));
			result.put(CommonConstants.PAYROLLBATCHNAME, String.valueOf(""));
		}
		if(calenderObject.getUnitOrBranch() != null){
			result.put(CommonConstants.UNIT, String.valueOf(calenderObject.getUnitOrBranch().getUnitOrBranchId()));
			result.put(CommonConstants.UNITNAME, String.valueOf(calenderObject.getUnitOrBranch().getUnitOrBranch()+", "+calenderObject.getUnitOrBranch().getState().getStateName()+", "+calenderObject.getUnitOrBranch().getCountry().getCountryName()));
		}else{
			result.put(CommonConstants.UNIT, "");
			result.put(CommonConstants.UNITNAME,"");
		}
		if(calenderObject.getMinimunWorkingDays() != null){
			result.put(CommonConstants.DAYS, String.valueOf(calenderObject.getMinimunWorkingDays()));
		}else{
			result.put(CommonConstants.DAYS, String.valueOf(""));
		}
		if(calenderObject.getNoOfSalaryDays() != null){
			result.put(CommonConstants.NO_OF_SALARY_DAYS, String.valueOf(calenderObject.getNoOfSalaryDays()));
		}else{
			result.put(CommonConstants.NO_OF_SALARY_DAYS, String.valueOf(""));
		}
		//result.put(CommonConstants.DAYS, String.valueOf(calenderObject.getMinimunWorkingDays()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(calenderObject.getUpdatedon())));
		if(calenderObject.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(calenderObject.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		return result;
	}
}
