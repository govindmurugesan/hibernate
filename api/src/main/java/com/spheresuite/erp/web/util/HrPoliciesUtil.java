package com.spheresuite.erp.web.util;

import java.util.List;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.HrPoliciesDO;
import com.spheresuite.erp.domainobject.HrPoliciesDocDO;
import com.spheresuite.erp.service.HrPoliciesDocService;
import com.spheresuite.erp.util.CommonConstants;
@Component
public class HrPoliciesUtil {
	
	@Autowired
	private HrPoliciesDocService thrPoliciesDocService;
	
	private static HrPoliciesDocService hrPoliciesDocService;
	
	@PostConstruct
	public void init() {
		hrPoliciesDocService=thrPoliciesDocService;
	}
	private HrPoliciesUtil() {}
	
	public static JSONObject getHrPoliciesList(List<HrPoliciesDO> hrPoliciesList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (HrPoliciesDO hrPolicies : hrPoliciesList) {
				resultJSONArray.put(getHrPoliciesDetailObject(hrPolicies));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getHrPoliciesDetailObject(HrPoliciesDO hrPolicies)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(hrPolicies.getHrPoliceId()));
		result.put(CommonConstants.NAME, String.valueOf(hrPolicies.getHrPolice()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(hrPolicies.getUpdatedon())));
		result.put(CommonConstants.STATUS, String.valueOf(hrPolicies.getStatus()));
		if(hrPolicies.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(hrPolicies.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		if(hrPolicies.getCreatedby() != null){
			String empName = CommonUtil.getEmpObject(hrPolicies.getCreatedby());
			result.put(CommonConstants.CREATED_BY, empName); 
			result.put(CommonConstants.CREATED_BY_ID, hrPolicies.getCreatedby()); 
		}else{
			result.put(CommonConstants.CREATED_BY, ""); 
			result.put(CommonConstants.CREATED_BY_ID, hrPolicies.getCreatedby()); 
		}
		if(hrPolicies.getComment() != null){
			result.put(CommonConstants.COMMENT, hrPolicies.getComment()); 
		}else{
			result.put(CommonConstants.COMMENT, ""); 
		}
		if(hrPolicies.getRole() != null){
			result.put(CommonConstants.ROLE_ID, String.valueOf(hrPolicies.getRole().getRoleId()));
			result.put(CommonConstants.ROLE_NAME, String.valueOf(hrPolicies.getRole().getName()));
		}else{
			result.put(CommonConstants.ROLE_ID, "");
			result.put(CommonConstants.ROLE_NAME, "");
		}
		JSONArray resultJSONArray = new JSONArray();
		List<HrPoliciesDocDO> hrpoliciesdocList = hrPoliciesDocService.retrieveByPoliciesId(hrPolicies.getHrPoliceId());
		if(hrpoliciesdocList != null && hrpoliciesdocList.size()  > 0){
			for (HrPoliciesDocDO doc : hrpoliciesdocList) {
				resultJSONArray.put(getDocDetailObject(doc));
			}
		}
		result.put(CommonConstants.FILE, resultJSONArray);
		return result;
	}
	
	public static JSONObject getDocDetailObject(HrPoliciesDocDO doc)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.FILE, String.valueOf(doc.getPhoto()));
		if(doc.getFileName() != null){
			result.put(CommonConstants.NAME, String.valueOf(doc.getFileName()));
		}else{
			result.put(CommonConstants.NAME, "");	
		}
		
		return result;
	}
}
