package com.spheresuite.erp.web.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.DepartmentDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.Greige_NotificationSettingsDO;
import com.spheresuite.erp.domainobject.LeaveRequestsDO;
import com.spheresuite.erp.domainobject.NotificationSettingsDO;
import com.spheresuite.erp.domainobject.RecentActivityDO;
import com.spheresuite.erp.domainobject.SupplierAccessDO;
import com.spheresuite.erp.domainobject.SuppliersDO;
import com.spheresuite.erp.domainobject.UserDO;
import com.spheresuite.erp.procurement.service.Greige_NotificationSettingsService;
import com.spheresuite.erp.service.DepartmentService;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.LeaveRequestsService;
import com.spheresuite.erp.service.NotificationSettingsService;
import com.spheresuite.erp.service.RecentActivityService;
import com.spheresuite.erp.service.UserService;
import com.spheresuite.erp.supplier.service.SupplierAccessService;
import com.spheresuite.erp.supplier.service.SupplierService;
import com.spheresuite.erp.util.CommonConstants;
@Component
public final class CommonUtil {

	private CommonUtil() {

	}
	@Autowired
	private DepartmentService tdeptservice;
	
	@Autowired
	private LeaveRequestsService  tleaveRequestsService;
	
	@Autowired
	private RecentActivityService trecentActivityService;
	
	@Autowired
	private EmployeeService temployeeService;
	
	@Autowired
	private SupplierService tsuppliersService;
	
	@Autowired
	private SupplierAccessService tsupplieraccessService;
	
	@Autowired
	private UserService tuserService;
	
	@Autowired
	private NotificationSettingsService tnotificationSettingsService;
	
	@Autowired
	private Greige_NotificationSettingsService tgreigeNotificationSettingsService;
	
	
	private static RecentActivityService recentActivityService;
	
	private static LeaveRequestsService leaveRequestsService;
	
	private static EmployeeService employeeService;
	
	private static UserService userService;
	
	private static NotificationSettingsService notificationSettingsService;
	
	private static Greige_NotificationSettingsService greigeNotificationSettingsService;
	
	private static DepartmentService deptservice;
	
	private static SupplierService suppliersService;
	
	private static SupplierAccessService supplieraccessService;
	
	@PostConstruct
	public void init() {
		leaveRequestsService = tleaveRequestsService;
		deptservice = tdeptservice;
		employeeService = temployeeService;
		recentActivityService = trecentActivityService;
		userService = tuserService;
		notificationSettingsService = tnotificationSettingsService;
		suppliersService = tsuppliersService;
		greigeNotificationSettingsService = tgreigeNotificationSettingsService;
		supplieraccessService = tsupplieraccessService;
	}
	
	
	public static String convertDateToString(Date date){
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		if(date != null){
			return formatter.format(date);
		}else{
			return null;
		}
	}
	
	public static String convertDateToString1(Date date){
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		if(date != null){
			return formatter.format(date);
		}else{
			return null;
		}
	}
	
	public static String convertDateToStringWithOutTime(Date date){
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		if(date != null){
			return formatter.format(date);
		}else{
			return null;
		}
	}
	
	public static String convertDateToYearWithOutTime(Date date){
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		if(date != null){
			return formatter.format(date);
		}else{
			return null;
		}
	}
	
	public static String convertDateToStringWithdatetime(Date date){
		SimpleDateFormat formatter = new SimpleDateFormat("MMM dd, yyyy");
		if(date != null){
			return formatter.format(date);
		}else{
			return null;
		}
	}
	
	public static String convertDateToStringWithOutComma(Date date){
		SimpleDateFormat formatter = new SimpleDateFormat("MMM dd yyyy");
		if(date != null){
			return formatter.format(date);
		}else{
			return null;
		}
	}
	
	public static String convertDateToStringWithOutDate(Date date){
		SimpleDateFormat formatter = new SimpleDateFormat("MMM yyyy");
		if(date != null){
			return formatter.format(date);
		}else{
			return null;
		}
	}
	
	public static String convertDateToStringApp(Date date){
		SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
		if(date != null){
			return formatter.format(date);
		}else{
			return null;
		}
	}
	
	public static Date convertDateToStringWithOutDate(String date){
		SimpleDateFormat formatter = new SimpleDateFormat("MMM yyyy");
		if(date != null){
			try {
				return formatter.parse(date);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else{
			return null;
		}
		return null;
	}
	
	public static String convertDateToStringWithtime(Date date){
		SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy hh:mm a");
		if(date != null){
			return formatter.format(date);
		}else{
			return null;
		}
	}
	
	public static Date convertEmailDate(String sentDate){
		DateFormat readFormat = new SimpleDateFormat( "EEE MMM dd HH:mm:ss z yyyy");
		Date date = null;
	    try {
	    	if(sentDate != null){
	    		date = readFormat.parse( sentDate );
	    		return date;
	    	}else{
	    		return null;
	    	}
	    } catch ( ParseException e ) {
	        e.printStackTrace();
	    }
		return null;
	}
	
	public static String convertEmailDateToDate(Date date){
		DateFormat writeFormat = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss");
		if( date != null ) {
			return writeFormat.format( date );
	    }
		return null;
	}
	
	public static Date convertEmailDateToDateTime(String date){
		SimpleDateFormat writeFormat = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss");
		Date dateResp = null;
		if(date != null){
			try{
				dateResp = writeFormat.parse(date);
			}catch(ParseException e){
				e.printStackTrace();
			}
		}else{
			dateResp =  null;
		}
		return dateResp;
	}

	public static String convertDateToOnlyTime(Date date){
		SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
		if(date != null){
			return formatter.format(date);
		}else{
			return null;
		}
	}
	
	public static Date convertStringToDate(String date){
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		Date dateResp = null;
		if(date != null){
			try{
				dateResp = formatter.parse(date);
			}catch(ParseException e){
				e.printStackTrace();
			}
		}else{
			dateResp =  null;
		}
		return dateResp;
	}
	
	public static Date convertStringToDateMonth(String date){
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
		Date dateResp = null;
		if(date != null){
			try{
				dateResp = formatter.parse(date);
			}catch(ParseException e){
				e.printStackTrace();
			}
		}else{
			dateResp =  null;
		}
		return dateResp;
	}
	
	public static Date convertStringToDateMMM(String date){
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
		Date dateResp = null;
		if(date != null){
			try{
				dateResp = formatter.parse(date);
			}catch(ParseException e){
				e.printStackTrace();
			}
		}else{
			dateResp =  null;
		}
		return dateResp;
	}
	
	public static String convertStringToDateMMM(Date date){
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
		if(date != null){
			return formatter.format(date);
		}else{
			return null;
		}
	}
	
	public static Date convertStringToSqlDate(String date){
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date dateResp = null;
		if(date != null){
			try{
				dateResp = formatter.parse(date);
			}catch(ParseException e){
				e.printStackTrace();
			}
		}else{
			dateResp =  null;
		}
		return dateResp;
	}
	
	public static Date convertMonthToSqlDate(String date){
		SimpleDateFormat formatter = new SimpleDateFormat("MMM yyyy");
		Date dateResp = null;
		if(date != null){
			try{
				dateResp = formatter.parse(date);
			}catch(ParseException e){
				e.printStackTrace();
			}
		}else{
			dateResp =  null;
		}
		return dateResp;
	}
	
	public static Date convertStringToSqlDateWithLocale(String dateStr){
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat parser = new SimpleDateFormat("dd MM yyyy");
		if(dateStr != null){
			try{
				Date thedate = parser.parse(dateStr);
				String dateStrReformatted = formatter.format(thedate);
				return convertStringToSqlDate(dateStrReformatted);
			}catch(ParseException e){
				e.printStackTrace();
			}
		}else{
			return null;
		}
		return null;
	}
	
	public static Date convertStringToSqlDateMonth(String dateStr){
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat parser = new SimpleDateFormat("MMM dd yyyy");
		if(dateStr != null){
			try{
				Date thedate = parser.parse(dateStr);
				String dateStrReformatted = formatter.format(thedate);
				return convertStringToSqlDate(dateStrReformatted);
			}catch(ParseException e){
				e.printStackTrace();
			}
		}else{
			return null;
		}
		return null;
	}
	
	public static Date convertStringToSqlDateMonthTimePick(String dateStr){
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		SimpleDateFormat parser = new SimpleDateFormat("MMM dd yyyy'T'HH:mm:ss.SSS'Z'");
		if(dateStr != null){
			try{
				Date thedate = parser.parse(dateStr);
				String dateStrReformatted = formatter.format(thedate);
				return convertStringToSqlDate(dateStrReformatted);
			}catch(ParseException e){
				e.printStackTrace();
			}
		}else{
			return null;
		}
		return null;
	}
	
	public static Date convertForLeave(String dateStr){
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat parser = new SimpleDateFormat("MMM dd, yyyy");
		if(dateStr != null){
			try{
				Date thedate = parser.parse(dateStr);
				String dateStrReformatted = formatter.format(thedate);
				return convertStringToSqlDate(dateStrReformatted);
			}catch(ParseException e){
				e.printStackTrace();
			}
		}else{
			return null;
		}
		return null;
	}
	
	public static String simpleFormat(Date dateStr){
		SimpleDateFormat formatter = new SimpleDateFormat("dd MM yyyy");
		if(dateStr != null){
			try{
				String dateStrReformatted = formatter.format(dateStr);
				return dateStrReformatted;
			}catch(Exception e){
				e.printStackTrace();
			}
		}else{
			return null;
		}
		return null;
	}
	
	public static String simpleFormat(String dateStr){
		SimpleDateFormat formatter = new SimpleDateFormat("dd MM yyyy");
		if(dateStr != null){
			try{
				String dateStrReformatted = formatter.format(dateStr);
				return dateStrReformatted;
			}catch(Exception e){
				e.printStackTrace();
			}
		}else{
			return null;
		}
		return null;
	}
	
	public static Date convertStringToDateTime(String date){
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		Date dateResp = null;
		if(date != null){
			try{
				dateResp = formatter.parse(date);
			}catch(ParseException e){
				e.printStackTrace();
			}
		}else{
			dateResp =  null;
		}
		return dateResp;
	}
	
	public static Date convertStringToDateTimePick(String date){
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		Date dateResp = null;
		if(date != null){
			try{
				dateResp = formatter.parse(date);
			}catch(ParseException e){
				e.printStackTrace();
			}
		}else{
			dateResp =  null;
		}
		return dateResp;
	}
	
	public static Date convertStringToDateTimeReturnDate(String date){
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date dateResp = null;
		if(date != null){
			try{
				dateResp = formatter.parse(date);
			}catch(ParseException e){
				e.printStackTrace();
			}
		}else{
			dateResp =  null;
		}
		return dateResp;
	}
	
	public static boolean compareDates(String d1,String d2)
    {
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date date1 = sdf.parse(d1);
            Date date2 = sdf.parse(d2);

            if(date1.before(date2)){
                return true;
            }
            return false;
        }
        catch(ParseException ex){
            ex.printStackTrace();
            return false;
        }
    }
	
	public static Date convertStringToDateTimeFmt(String date){
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date dateResp = null;
		if(date != null){
			try{
				dateResp = formatter.parse(date);
			}catch(ParseException e){
				e.printStackTrace();
			}
		}else{
			dateResp =  null;
		}
		return dateResp;
	}
	
	public static Date convertStringToTime(String date){
		SimpleDateFormat formatter = new SimpleDateFormat("hh:mm:ss");
		Date dateResp = null;
		if(date != null){
			try {
				dateResp = formatter.parse(date);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}else{
			dateResp =  null;
		}
		return dateResp;
	}
	public static String convertDoubleValueWithTwoDecimalPoints(Double number){
		DecimalFormat format = new DecimalFormat("0.00");
	    String formatted = format.format(number);
	    return formatted;
	}
	
	public static String convertDoubleValueWithTwoAbsDecimalPoints(Double number){
		String formatted = "0.00";
		if(number != null){
			DecimalFormat format = new DecimalFormat("0.00");
		    formatted = format.format(number);
		    Double fractionalPart = Double.valueOf(formatted) % 1;
		    Double integralPart = Double.valueOf(formatted) - fractionalPart;
	    	if(fractionalPart>.50){
	    		formatted = format.format(integralPart + 1);
	    	} else {
	    		if(fractionalPart > .00 && fractionalPart < .50){
	    			formatted = format.format(integralPart + .50);
	    		} else {
	    			formatted = format.format(integralPart);
	    		}
	    	}
		}
	    return formatted;
	}
	
	/*public static Double convertDoubleValueWithTwoAbsDecimalPoints1(Double number){
		String formatted = "0.00";
		if(number != null){
			DecimalFormat format = new DecimalFormat("0.00");
		    formatted = format.format(number);
		    Double fractionalPart = Double.valueOf(formatted) % 1;
		    Double integralPart = Double.valueOf(formatted) - fractionalPart;
	    	if(fractionalPart>.50){
	    		formatted = format.format(integralPart + 1);
	    	} else {
	    		if(fractionalPart > .00 && fractionalPart < .50){
	    			formatted = format.format(integralPart + .50);
	    		} else {
	    			formatted = format.format(integralPart);
	    		}
	    	}
		}
	    return Double.parseDouble(formatted);
	}*/
	
	public static String convertfloatValueWithTwoDecimalPoints(double number){
        DecimalFormat dec = new DecimalFormat("#.00");
        double rawPercent = ((double)(number)) * 1.00;
        String formatted = dec.format(rawPercent);
	    return formatted;
	}
	
	public static Double convertfloatValueWithTwoDecimalPoints2(double number){
        DecimalFormat dec = new DecimalFormat("#.00");
        double rawPercent = ((double)(number)) * 1.00;
        String formatted = dec.format(rawPercent);
	    return Double.parseDouble(formatted);
	}
	
	public static String convertnumberValueWithcomma(double value) {
	    if(value < 1000) {
	    	return new DecimalFormat("###.00").format(value);
	    } else {
	        double hundreds = value % 1000;
	        int other = (int)(value / 1000);
	        return new DecimalFormat(",##").format(other) + ',' + new DecimalFormat("000.00").format(hundreds);
	    }
	}
	
	public static float convertFloatWithDoublePrecision(float number){
		DecimalFormat df = new DecimalFormat();
		df.setMaximumFractionDigits(2);
		df.format(number);
	    return number;
	}
	
	public static double convertFloatWithDoublePrecision(double number){
		DecimalFormat df = new DecimalFormat();
		df.setMaximumFractionDigits(2);
		df.format(number);
	    return number;
	}
	
	
	 public static Double findExchangeRateAndConvert(String from, String to, double amount) {
        try {
            URL url = new URL("http://finance.yahoo.com/d/quotes.csv?f=l1&s="+ from + to + "=X");
            BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
            String line = reader.readLine();
            if (line.length() > 0) {
                return Double.parseDouble(line) * amount;
            }
            reader.close();
        } catch (Exception e) {
        }
        return null;
    }
	 
	 public static long convertDatesToDaysCount(Date startdate, Date enddate){
			long noofdays = 0;
			if(startdate != null && enddate != null){
				noofdays =  (enddate.getTime() - startdate.getTime()) / (1000 * 60 * 60 * 24) + 1;
			}else{
				noofdays =  0;
			}
			return noofdays;
		}

	public static String encrypt( String salt, String plainText) throws Exception {
		Base64 base64 = new Base64();
		String Concatpassword = salt + plainText;
        String encoded = new String(base64.encode(Concatpassword.getBytes()));
        return encoded;
	}
	 
	public static String decrypt(String encryptedText) throws Exception {
		Base64 decoder = new Base64();
		String decoded = new String(decoder.decode(encryptedText.getBytes()));
		return decoded;
	}
	
	public static String getUIUrl(HttpServletRequest request) throws NamingException{
		String env = request.getServletContext().getInitParameter(CommonConstants.ENV);
		String url = request.getServletContext().getInitParameter(CommonConstants.UIURL);
		return env+url;
	}
	
	 public static String encryptText(String seed,String password) throws Exception{
		 StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
		 encryptor.setPassword(seed);
		 String encrypted= encryptor.encrypt(password);
        return encrypted;
     }
    public static String decryptText(String seed,String password) throws Exception {
    	StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
    	encryptor.setPassword(seed);
    	String decrypted = encryptor.decrypt(password);
        return decrypted;
    }
    
    public static void persistRecentAcvtivity(String empId, String desc){
    	try {
	    	RecentActivityDO recentActivity = new RecentActivityDO();
	    	recentActivity.setEmpId(empId);
	    	recentActivity.setDescripetion(desc);
	    	recentActivity.setUpdatedBy(empId.toString());
	    	recentActivity.setUpdatedon(new Date());
			recentActivityService.persist(recentActivity);
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
    
	@SuppressWarnings("unused")
	public static void sendMailToReportingHeads(HttpServletRequest request, JSONObject inputJSON, String pageUrl, String mailType, String status) throws Exception{
    	try {
	    	String userName = null;
	    	//String toEmails = null;
	    	//String[] CCList = new String[ 100 ];
	    	//List<String> ccEmailList ;
	    	String requestBy = "";
	    	ArrayList<String> ccemailslist = new ArrayList<String>();  
	    	
	    	ArrayList<String> toEmails = new ArrayList<String>();  
	    	
	    	String host = request.getServletContext().getInitParameter(CommonConstants.HOST);
			String fromEmail = CommonConstants.SENDMAIL;
			String emailBody = "";
			String fromDate = "";
			String toDate = "";
			/*if(inputJSON.get(CommonConstants.REPORTTO_EMAIL) != null && !inputJSON.get(CommonConstants.REPORTTO_EMAIL).toString().isEmpty()){
				toEmails = inputJSON.get(CommonConstants.REPORTTO_EMAIL).toString();
				userName = inputJSON.get(CommonConstants.REPORTTO_NAME).toString(); 
			}else{
				toEmails = inputJSON.get(CommonConstants.DEPHEAD_EMAIL).toString();
				userName = inputJSON.get(CommonConstants.DEPTMNGR_NAME).toString(); 
			}*/
			/*List<UserDO> adminList = userService.getSuperAdmins();
			for(int i=0; i< adminList.size(); i++){
				List<EmployeeDO> adminList2 = employeeService.retrieveEmpId(adminList.get(i).getEmployee().getEmpId().toString() );
				if(adminList2 != null && adminList2.size() > 0){
					emailslist.add(adminList2.get(0).getCompanyemail());
				}
			}*/
			if(mailType.equalsIgnoreCase("leaveRequest")){
				if(status.equalsIgnoreCase("pending") ||  status.equalsIgnoreCase("cancel Requrst")){
					if(inputJSON.get(CommonConstants.EMPID) != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
				 		if(employeeList != null && employeeList.size() > 0 && employeeList.get(0).getCompanyemail() != null ){
				 			ccemailslist.add(employeeList.get(0).getCompanyemail());
				 			if(employeeList.get(0).getUnitOrBranch() != null && employeeList.get(0).getUnitOrBranch().getManagerId() != null && employeeList.get(0).getUnitOrBranch().getManagerId().getCompanyemail() != null){
				 				ccemailslist.add(employeeList.get(0).getUnitOrBranch().getManagerId().getCompanyemail());
				 			}
				 			//requestBy = employeeList.get(0).getFirstname();
				 		}
			 		}
					List<NotificationSettingsDO> notificationSettingsList = notificationSettingsService.retrieve();
					if(notificationSettingsList != null && notificationSettingsList.size() > 0){
						List<String> toEmailList = Arrays.asList(notificationSettingsList.get(0).getLeaves().split(CommonConstants.COMMA));
						if(toEmailList !=null && toEmailList.size() > 0){
							for(String toEmail : toEmailList ){
								List<EmployeeDO> approverList = employeeService.retrieveEmpId(toEmail);
								if(approverList != null && approverList.size() > 0 && approverList.get(0).getCompanyemail() != null){
									toEmails.add(approverList.get(0).getCompanyemail());
								}
							}
							
						}
					}
					
					if(inputJSON.get(CommonConstants.FROMDATE) != null  && !inputJSON.get(CommonConstants.FROMDATE).toString().isEmpty()){
		 				fromDate = CommonUtil.convertDateToStringApp(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.FROMDATE).toString())).toString();
		 			}
		 			if(inputJSON.get(CommonConstants.TODATE) != null  && !inputJSON.get(CommonConstants.TODATE).toString().isEmpty()){
		 				toDate = CommonUtil.convertDateToStringApp(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.TODATE).toString())).toString();
		 			}
		 			
		 			if(inputJSON.get(CommonConstants.PERMISSIONFROM) != null && !inputJSON.get(CommonConstants.PERMISSIONFROM).toString().isEmpty()){
		 				fromDate = (CommonUtil.convertStringToDateTimePick(inputJSON.get(CommonConstants.PERMISSIONFROM).toString())).toString();
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.PERMISSIONTO) != null && !inputJSON.get(CommonConstants.PERMISSIONTO).toString().isEmpty()){
			 			toDate = (CommonUtil.convertStringToDateTimePick(inputJSON.get(CommonConstants.PERMISSIONTO).toString())).toString();
			 		}
					
					if(inputJSON.get(CommonConstants.REPORTTO_EMAIL) != null && !inputJSON.get(CommonConstants.REPORTTO_EMAIL).toString().isEmpty()){
						toEmails.add(inputJSON.get(CommonConstants.REPORTTO_EMAIL).toString());
					}
				}else{
					if(inputJSON.get(CommonConstants.EMPID) != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
				 		if(employeeList != null && employeeList.size() > 0){
				 			if(employeeList.get(0).getCompanyemail() != null)toEmails.add(employeeList.get(0).getCompanyemail());
				 			//requestBy = employeeList.get(0).getFirstname();
				 			if(employeeList.get(0).getMiddlename() != null) requestBy = String.valueOf(employeeList.get(0).getFirstname() + " " +employeeList.get(0).getMiddlename() + " " + employeeList.get(0).getLastname());
							else requestBy = String.valueOf(employeeList.get(0).getFirstname() + " " + employeeList.get(0).getLastname());
				 		}
			 		}
				}
			}
			
			if(mailType.equalsIgnoreCase("hrrequests")){
				if(status.equalsIgnoreCase("pending")){
				 List<NotificationSettingsDO> notificationSettingsList = notificationSettingsService.retrieve();
					if(notificationSettingsList != null && notificationSettingsList.size() > 0){
						List<String> toEmailList = Arrays.asList(notificationSettingsList.get(0).getHrRequest().split(CommonConstants.COMMA));
						if(toEmailList !=null && toEmailList.size() > 0){
							for(String toEmail : toEmailList ){
								List<EmployeeDO> approverList = employeeService.retrieveEmpId(toEmail);
								if(approverList != null && approverList.size() > 0 && approverList.get(0).getCompanyemail() != null){
									toEmails.add(approverList.get(0).getCompanyemail());
								}
								//emailslist.add(ccEmail);
							}
							
						}
					}
				}
				if(status.equalsIgnoreCase("approved")){
					if(inputJSON.get(CommonConstants.CREATED_BY_ID) != null && !inputJSON.get(CommonConstants.CREATED_BY_ID).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.CREATED_BY_ID).toString());
				 		if(employeeList != null && employeeList.size() > 0 && employeeList.get(0).getCompanyemail() != null){
				 			toEmails.add(employeeList.get(0).getCompanyemail());
				 		}
			 		}
				}
				if(status.equalsIgnoreCase("cancel")){
					if(inputJSON.get(CommonConstants.CREATED_BY_ID) != null && !inputJSON.get(CommonConstants.CREATED_BY_ID).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.CREATED_BY_ID).toString());
				 		if(employeeList != null && employeeList.size() > 0 && employeeList.get(0).getCompanyemail() != null){
				 			toEmails.add(employeeList.get(0).getCompanyemail());
				 		}
			 		}
				}
				if(status.equalsIgnoreCase("reject")){
					if(inputJSON.get(CommonConstants.CREATED_BY_ID) != null && !inputJSON.get(CommonConstants.CREATED_BY_ID).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.CREATED_BY_ID).toString());
				 		if(employeeList != null && employeeList.size() > 0 && employeeList.get(0).getCompanyemail() != null){
				 			toEmails.add(employeeList.get(0).getCompanyemail());
				 		}
			 		}
				}
			}
			if(mailType.equalsIgnoreCase("hrpolicies")){
				if(status.equalsIgnoreCase("pending")){
					List<NotificationSettingsDO> notificationSettingsList = notificationSettingsService.retrieve();
					if(notificationSettingsList != null && notificationSettingsList.size() > 0){
						List<String> toEmailList = Arrays.asList(notificationSettingsList.get(0).getHrpolicies().split(CommonConstants.COMMA));
						if(toEmailList !=null && toEmailList.size() > 0){
							for(String toEmail : toEmailList ){
								List<EmployeeDO> approverList = employeeService.retrieveEmpId(toEmail);
								if(approverList != null && approverList.size() > 0 && approverList.get(0).getCompanyemail() != null){
									toEmails.add(approverList.get(0).getCompanyemail());
								}
								//emailslist.add(ccEmail);
							}
							
						}
					}
				}
				if(status.equalsIgnoreCase("approved")){
					if(inputJSON.get(CommonConstants.CREATED_BY_ID) != null && !inputJSON.get(CommonConstants.CREATED_BY_ID).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.CREATED_BY_ID).toString());
				 		if(employeeList != null && employeeList.size() > 0 && employeeList.get(0).getCompanyemail() != null){
				 			toEmails.add(employeeList.get(0).getCompanyemail());
				 		}
			 		}
				}
				if(status.equalsIgnoreCase("rejected")){
					if(inputJSON.get(CommonConstants.CREATED_BY_ID) != null && !inputJSON.get(CommonConstants.CREATED_BY_ID).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.CREATED_BY_ID).toString());
				 		if(employeeList != null && employeeList.size() > 0 && employeeList.get(0).getCompanyemail() != null){
				 			toEmails.add(employeeList.get(0).getCompanyemail());
				 		}
			 		}
				}
				 
			}
			if(mailType.equalsIgnoreCase("holiday")){
				if(status.equalsIgnoreCase("pending")){
				 List<NotificationSettingsDO> notificationSettingsList = notificationSettingsService.retrieve();
					if(notificationSettingsList != null && notificationSettingsList.size() > 0){
						List<String> toEmailList = Arrays.asList(notificationSettingsList.get(0).getHolidays().split(CommonConstants.COMMA));
						if(toEmailList !=null && toEmailList.size() > 0){
							for(String toEmail : toEmailList ){
								List<EmployeeDO> approverList = employeeService.retrieveEmpId(toEmail);
								if(approverList != null && approverList.size() > 0 && approverList.get(0).getCompanyemail() != null) {
									toEmails.add(approverList.get(0).getCompanyemail());
								}
								//emailslist.add(ccEmail);
							}
							
						}
					}
				}
				if(status.equalsIgnoreCase("approved")){
					if(inputJSON.get(CommonConstants.CREATED_BY_ID) != null && !inputJSON.get(CommonConstants.CREATED_BY_ID).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.CREATED_BY_ID).toString());
				 		if(employeeList != null && employeeList.size() > 0 && employeeList.get(0).getCompanyemail() != null){
				 			toEmails.add(employeeList.get(0).getCompanyemail());
				 		}
			 		}
				}
				if(status.equalsIgnoreCase("reject")){
					if(inputJSON.get(CommonConstants.CREATED_BY_ID) != null && !inputJSON.get(CommonConstants.CREATED_BY_ID).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.CREATED_BY_ID).toString());
				 		if(employeeList != null && employeeList.size() > 0 && employeeList.get(0).getCompanyemail() != null) {
				 			toEmails.add(employeeList.get(0).getCompanyemail());
				 		}
			 		}
				}
			}
			if(mailType.equalsIgnoreCase("suppliers")){
				if(status.equalsIgnoreCase("submit")){
					List<SupplierAccessDO> supplierList = supplieraccessService.retrieveActive();
					if(supplierList != null && supplierList.size() > 0){
						for(SupplierAccessDO supplier: supplierList){
							toEmails.add(supplier.getContact().getEmail());
						}
					}
				}
			}
			if(mailType.equalsIgnoreCase("greigeApproval")){
				if(status.equalsIgnoreCase("submit")){
					List<Greige_NotificationSettingsDO> notificationSettingsList = greigeNotificationSettingsService.retrieve();
					if(notificationSettingsList != null && notificationSettingsList.size() > 0){
						List<String> toEmailList = Arrays.asList(notificationSettingsList.get(0).getApprover().split(CommonConstants.COMMA));
						if(toEmailList !=null && toEmailList.size() > 0){
							for(String toEmail : toEmailList ){
								List<EmployeeDO> approverList = employeeService.retrieveEmpId(toEmail);
								if(approverList != null && approverList.size() > 0 && approverList.get(0).getCompanyemail() != null) {
									toEmails.add(approverList.get(0).getCompanyemail());
								}
							}
							
						}
					}
				}
			}
			String URL = CommonUtil.getUIUrl(request);
			URL = CommonConstants.HTTP+URL+"#!/"+pageUrl;
			String url = URL;  
		
			if(inputJSON.get(CommonConstants.REQUESTBY) != null && !inputJSON.get(CommonConstants.REQUESTBY).toString().isEmpty()){
				
				if(!status.equalsIgnoreCase("pending") ||  !status.equalsIgnoreCase("cancel Requrst")){
					requestBy = inputJSON.get(CommonConstants.REQUESTBY).toString();
				}
				
			}
			if(inputJSON.get(CommonConstants.CREATED_BY) != null && !inputJSON.get(CommonConstants.CREATED_BY).toString().isEmpty()){
				requestBy = inputJSON.get(CommonConstants.CREATED_BY).toString();
			}
			
			//String emailBody = "Cick Here Login : " +URL+userDO.getTemppassowrd()+"/"+userDO.getEmail()+"/N";
			if(inputJSON.get(CommonConstants.COMMENT) != null && !inputJSON.get(CommonConstants.COMMENT).toString().isEmpty()){
				emailBody = inputJSON.get(CommonConstants.COMMENT).toString();
			}
			if(mailType.equalsIgnoreCase("hrpolicies")){
				if(status.equalsIgnoreCase("pending")){
					emailBody = inputJSON.get(CommonConstants.VALUE).toString();
				}
			}
			
			if(mailType.equalsIgnoreCase("holiday")){
				if(status.equalsIgnoreCase("pending")){
					emailBody = inputJSON.get(CommonConstants.DESC).toString();
				}
			}
			
	 		boolean mailStatus = EmailProxyUtil.notificationEmail(request, fromEmail, toEmails, ccemailslist, "", emailBody, false, url, mailType, userName, requestBy, status, fromDate, toDate);
	    	
	    	} catch (Exception e) {
			e.printStackTrace();
		}
    }
	
	
	@SuppressWarnings("unused")
	public static void sendMailToReportingHeadsForUpdate(HttpServletRequest request, JSONObject inputJSON, String pageUrl, String mailType) throws Exception{
    	try {
	    	String userName = "";
	    	String toEmails = "";
	    	//String[] CCList = new String[ 100 ];
	    	//List<String> ccEmailList ;
	    	
	    	ArrayList<String> emailslist=new ArrayList<String>();  
	    	
	    	String host = request.getServletContext().getInitParameter(CommonConstants.HOST);
			String fromEmail = CommonConstants.SENDMAIL;
			List<LeaveRequestsDO> leaveRequestList = leaveRequestsService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			
			List<EmployeeDO> employeeDetails = employeeService.retrieveEmpId(leaveRequestList.get(0).getEmployee().getEmpId().toString());
				if(employeeDetails != null && employeeDetails.size() > 0 && employeeDetails.get(0).getReportto() != null){
					List<EmployeeDO> employeeList = employeeService.retriveByEmpId(employeeDetails.get(0).getReportto());
					if(employeeList != null && employeeList.size() > 0){
						String firstName="";
						String middleName="";
						String lastName="";
						if(employeeList.get(0).getFirstname() != null){
							firstName = employeeList.get(0).getFirstname();
						}
						if(employeeList.get(0).getLastname() != null){
							middleName = employeeList.get(0).getLastname(); 		
						}
						if(employeeList.get(0).getMiddlename() != null){
							lastName = employeeList.get(0).getMiddlename();
						}
						
						toEmails = employeeList.get(0).getCompanyemail();
						userName = firstName+" "+middleName+" "+lastName; 
					}
					if(employeeDetails.get(0).getDepartment().getDepartmentId() != null){
						List<DepartmentDO> deptserviceList = deptservice.retrieveById(employeeDetails.get(0).getDepartment().getDepartmentId());
						if(deptserviceList != null && deptserviceList.size() > 0){
							List<EmployeeDO> deptMangr = employeeService.retrieveEmpId(deptserviceList.get(0).getIsManager());
							if(deptMangr != null && deptMangr.size() > 0){
								String deptfirstName="";
								String deptmiddleName="";
								String deptlastName="";
								if(deptMangr.get(0).getFirstname() != null){
									deptfirstName = deptMangr.get(0).getFirstname();
								}
								if(deptMangr.get(0).getLastname() != null){
									deptmiddleName = deptMangr.get(0).getLastname(); 		
								}
								if(deptMangr.get(0).getMiddlename() != null){
									deptlastName = deptMangr.get(0).getMiddlename();
								}
								
								toEmails = deptMangr.get(0).getCompanyemail();
								userName = deptfirstName+" "+deptmiddleName+" "+deptlastName; 
							}
						}
					}
					
				}
			
			/*if(inputJSON.get(CommonConstants.REPORTTO_EMAIL).toString() != null && !inputJSON.get(CommonConstants.REPORTTO_EMAIL).toString().isEmpty()){
				toEmails = inputJSON.get(CommonConstants.REPORTTO_EMAIL).toString();
				userName = inputJSON.get(CommonConstants.REPORTTO_NAME).toString(); 
			}else{
				toEmails = inputJSON.get(CommonConstants.DEPHEAD_EMAIL).toString();
				userName = inputJSON.get(CommonConstants.DEPTMNGR_NAME).toString(); 
			}*/
			List<UserDO> adminList = userService.getSuperAdmins();
			for(int i=0; i< adminList.size(); i++){
				List<EmployeeDO> adminList2 = employeeService.retrieveEmpId(adminList.get(i).getEmployee().getEmpId().toString() );
				if(adminList2 != null && adminList2.size() > 0){
					emailslist.add(adminList2.get(0).getCompanyemail());
				}
			}
			
			List<NotificationSettingsDO> notificationSettingsList = notificationSettingsService.retrieve();
			if(notificationSettingsList != null && notificationSettingsList.size() > 0){
				List<String> ccEmailList = Arrays.asList(notificationSettingsList.get(0).getHrRequest().split(CommonConstants.COMMA));
				if(ccEmailList !=null && ccEmailList.size() > 0){
					for(String ccEmail : ccEmailList ){
						emailslist.add(ccEmail);
					}
					
				}
			}
			String URL = CommonUtil.getUIUrl(request);
			URL = CommonConstants.HTTP+URL+"#!/"+pageUrl;
			String url = URL;  
			String requestBy = "";
			
			String firstName="";
			String middleName="";
			String lastName="";
			if(employeeDetails.get(0).getFirstname() != null){
				firstName = employeeDetails.get(0).getFirstname();
			}
			if(employeeDetails.get(0).getLastname() != null){
				middleName = employeeDetails.get(0).getLastname(); 		
			}
			if(employeeDetails.get(0).getMiddlename() != null){
				lastName = employeeDetails.get(0).getMiddlename();
			}
			
			requestBy = firstName +" "+middleName+" "+lastName;
			/*if(inputJSON != null && !inputJSON.get(CommonConstants.REQUESTBY).toString().isEmpty()){
				requestBy = inputJSON.get(CommonConstants.REQUESTBY).toString();
			}*/
			
			String emailBody = "";
			//String emailBody = "Cick Here Login : " +URL+userDO.getTemppassowrd()+"/"+userDO.getEmail()+"/N";
			if(inputJSON != null && !inputJSON.get(CommonConstants.DESC).toString().isEmpty()){
				emailBody = inputJSON.get(CommonConstants.DESC).toString();
			}
	 	//	boolean mailStatus = EmailProxyUtil.notificationEmail(request,host,  fromEmail,  toEmails, emailslist, "", emailBody, false, url, mailType, userName, requestBy);
	    	
	    	} catch (Exception e) {
			e.printStackTrace();
		}
    }
	
	public static String getEmpObject(String empId) {
		List<Object[]> empList = employeeService.retrieveByEmpIdOnlyNames(empId);
		String empName = null;
		if(empList != null && empList.size() > 0){
			for (Object[] objects : empList) {
				if(objects[1] != null) empName = String.valueOf(objects[0] + " " +objects[1] + " " + objects[2]);
				else if(objects[1] != null && objects[2] == null ) empName = String.valueOf(objects[0] + " " +objects[1]);
				else if(objects[1] == null && objects[2] == null  ) empName = String.valueOf(objects[0]);
				else empName = String.valueOf(objects[0] + " " + objects[2]);
			}
			
		}else{
			empName = "" ;
		}
		
		return empName;
	}
	
	public static String getSupplierObject(String supplierId) {
		List<SuppliersDO> supplierList = suppliersService.retrieveById(Long.parseLong(supplierId));
		String empName = null;
		if(supplierList != null && supplierList.size() > 0){
			 empName = String.valueOf(supplierList.get(0).getName());
		}else{
			empName = "" ;
		}
		
		return empName;
	}
	
	
	public static String totalHours(ArrayList<String> timestampsList){
        long tm = 0;
        for (String tmp : timestampsList){
            String[] arr = tmp.split(":");
            tm += Integer.parseInt(arr[2]);
            tm += 60 * Integer.parseInt(arr[1]);
            tm += 3600 * Integer.parseInt(arr[0]);
        }

        long hh = tm / 3600;
        tm %= 3600;
        long mm = tm / 60;
        tm %= 60;
        long ss = tm;
        return format(hh) + ":" + format(mm) + ":" + format(ss);
    }

    private static String format(long s){
        if (s < 10) return "0" + s;
        else return "" + s;
    }
    
    
}


