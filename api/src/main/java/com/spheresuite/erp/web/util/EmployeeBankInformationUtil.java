package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.EmployeeBankInformationDO;
import com.spheresuite.erp.util.CommonConstants;
@Component
public class EmployeeBankInformationUtil {
	
	private EmployeeBankInformationUtil() {}

	public static JSONObject getEmployeeBankInfoList(List<EmployeeBankInformationDO> employeeBankInformationList){
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeeBankInformationDO employeeBankInformationDO : employeeBankInformationList) {
				resultJSONArray.put(getEmployeeBankInfoDetailObject(employeeBankInformationDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getEmployeeBankInfoDetailObject(EmployeeBankInformationDO employeeBankInformationDO)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(employeeBankInformationDO.getEmpBankInfoId()));
		result.put(CommonConstants.BANKNAME, String.valueOf(employeeBankInformationDO.getBankName()));
		result.put(CommonConstants.IFSCCODE, String.valueOf(employeeBankInformationDO.getIfscCode()));
		result.put(CommonConstants.BRANCHNAME, String.valueOf(employeeBankInformationDO.getBranchName()));
		result.put(CommonConstants.ACCOUNTNAME, String.valueOf(employeeBankInformationDO.getAccountName()));
		result.put(CommonConstants.ACCOUNTNUMBER, String.valueOf(employeeBankInformationDO.getAccountNumber()));
		result.put(CommonConstants.EMPID, String.valueOf(employeeBankInformationDO.getEmployee().getEmpId()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(employeeBankInformationDO.getUpdatedon())));
		if(employeeBankInformationDO.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(employeeBankInformationDO.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		return result;
	}
}
