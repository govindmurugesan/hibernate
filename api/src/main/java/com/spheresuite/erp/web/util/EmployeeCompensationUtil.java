package com.spheresuite.erp.web.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.time.Period;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.AdvancePaymentDetailDO;
import com.spheresuite.erp.domainobject.AllowanceSettingDO;
import com.spheresuite.erp.domainobject.AttendanceAllowanceDO;
import com.spheresuite.erp.domainobject.AttendanceDO;
import com.spheresuite.erp.domainobject.CompanyCalenderDO;
import com.spheresuite.erp.domainobject.DASettingsDO;
import com.spheresuite.erp.domainobject.DeductionSettingDO;
import com.spheresuite.erp.domainobject.ESIDO;
import com.spheresuite.erp.domainobject.ESIGroupDO;
import com.spheresuite.erp.domainobject.ESIGroupEarningsDO;
import com.spheresuite.erp.domainobject.EducationCessDO;
import com.spheresuite.erp.domainobject.EmployeeBonusDO;
import com.spheresuite.erp.domainobject.EmployeeCompensationDO;
import com.spheresuite.erp.domainobject.EmployeeCtcDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.EmployeeESIInfoDO;
import com.spheresuite.erp.domainobject.EmployeeExpenseDO;
import com.spheresuite.erp.domainobject.EmployeeLeavegroupDO;
import com.spheresuite.erp.domainobject.EmployeeLopDO;
import com.spheresuite.erp.domainobject.EmployeePFInfoDO;
import com.spheresuite.erp.domainobject.EmployeePayrollDO;
import com.spheresuite.erp.domainobject.GeneralDeductionDetailDO;
import com.spheresuite.erp.domainobject.GeneralEarningDO;
import com.spheresuite.erp.domainobject.HolidaysDO;
import com.spheresuite.erp.domainobject.ItSavingsDO;
import com.spheresuite.erp.domainobject.ItSectionDO;
import com.spheresuite.erp.domainobject.ItSlabEffectiveDO;
import com.spheresuite.erp.domainobject.LeaveGroupSettingsDO;
import com.spheresuite.erp.domainobject.LeaveRequestsDO;
import com.spheresuite.erp.domainobject.PFDO;
import com.spheresuite.erp.domainobject.PFGroupDO;
import com.spheresuite.erp.domainobject.PFGroupEarningsDO;
import com.spheresuite.erp.domainobject.PayrollDeductionDetailsDO;
import com.spheresuite.erp.domainobject.PayrollEarningsDetailsDO;
import com.spheresuite.erp.domainobject.PayrollGroupAttendanceAllowanceDO;
import com.spheresuite.erp.domainobject.PayrollGroupDeductionDO;
import com.spheresuite.erp.domainobject.PayrollGroupEarningsDO;
import com.spheresuite.erp.domainobject.PayrollGroupTaxDO;
import com.spheresuite.erp.domainobject.PayrollGroupWorkingdaysAllowanceDO;
import com.spheresuite.erp.domainobject.PayrollTaxDetailsDO;
import com.spheresuite.erp.domainobject.PtDO;
import com.spheresuite.erp.domainobject.TaxSettingsDO;
import com.spheresuite.erp.domainobject.TaxSlabsSettingsDO;
import com.spheresuite.erp.domainobject.WorkingDayAllowanceDO;
import com.spheresuite.erp.domainobject.WorkingDayLeavetypeDO;
import com.spheresuite.erp.service.AdvancePaymentDetailService;
import com.spheresuite.erp.service.AttendanceService;
import com.spheresuite.erp.service.CompanyCalenderService;
import com.spheresuite.erp.service.DASettingsService;
import com.spheresuite.erp.service.ESIService;
import com.spheresuite.erp.service.EducationCessService;
import com.spheresuite.erp.service.EmployeeBonusService;
import com.spheresuite.erp.service.EmployeeCompensationService;
import com.spheresuite.erp.service.EmployeeCtcService;
import com.spheresuite.erp.service.EmployeeESIInfoService;
import com.spheresuite.erp.service.EmployeeExpenseService;
import com.spheresuite.erp.service.EmployeeLeavegroupService;
import com.spheresuite.erp.service.EmployeeLopService;
import com.spheresuite.erp.service.EmployeePFInfoService;
import com.spheresuite.erp.service.EmployeePayrollService;
import com.spheresuite.erp.service.GeneralDeductionDetailService;
import com.spheresuite.erp.service.GeneralEarningService;
import com.spheresuite.erp.service.HolidaysService;
import com.spheresuite.erp.service.InsurancePaymentDetailService;
import com.spheresuite.erp.service.ItSavingsService;
import com.spheresuite.erp.service.ItSectionService;
import com.spheresuite.erp.service.ItSlabEffectiveService;
import com.spheresuite.erp.service.LeaveGroupSettingsService;
import com.spheresuite.erp.service.LeaveRequestsService;
import com.spheresuite.erp.service.PFService;
import com.spheresuite.erp.service.PtService;
import com.spheresuite.erp.service.TaxSlabsSettingsService;
import com.spheresuite.erp.service.WorkingDayAllowanceService;
import com.spheresuite.erp.service.WorkingDayLeaveTypeService;
import com.spheresuite.erp.util.CommonConstants;
@Component
public class EmployeeCompensationUtil {
	
	private EmployeeCompensationUtil() {}
	
	 public static  Double basicPay = 0D;
	 public static	Double earingTotal = 0.0;
	 public static	Double deductionTotal = 0.0;
	 public static	Double taxTotal = 0.0;
	 public static	Double bonus = 0.0;
	 public static	Double taxableIncome = 0.0;
	 public static  Double taxbleSalary = 0.0;
	 public static Double finalTax = 0.0;
	 public static Double finalTDS = 0.0;
	 public static Long totalLop = 0L;
	 public static	Double totalAllowance = 0.0;
	 public static Double leaveCount= 0.0;
	 public static Double monthlyDA= 0.0;
	 public static Double monthlyAddDA= 0.0;
	 public static Double adjustment = 0.0;
	 
	 
	/*@Autowired
	private EmployeeService temployeeService;
	private static EmployeeService employeeService;*/
	

	@Autowired
	private EmployeeCtcService temployeeCtcService;
	private static EmployeeCtcService employeeCtcService;
	
	@Autowired
	private EmployeePayrollService temployeePayrollService;
	private static EmployeePayrollService employeePayrollService;
	
	@Autowired
	private EmployeeBonusService temployeeBonusService;
	private static EmployeeBonusService employeeBonusService;
	
	@Autowired
	private  EmployeeLopService temployeeLopService;
	private static EmployeeLopService employeeLopService;
	
	/*@Autowired
	private  EmployeeEarningsService temployeeEarningsService;
	private static EmployeeEarningsService employeeEarningsService;
	
	@Autowired
	private  EmployeeTaxService temployeeTaxService;
	private static EmployeeTaxService employeeTaxService;
	
	@Autowired
	private  EmployeeDeductionService temployeeDeductionService;
	private static EmployeeDeductionService employeeDeductionService;
	
	@Autowired
	private  AllowanceSettingsService tallowanceSettingsService;
	private static AllowanceSettingsService allowanceSettingsService;
	
	@Autowired
	private  DeductionSettingsService tdeductionSettingsService;
	private static DeductionSettingsService deductionSettingsService;
	
	@Autowired
	private  TaxSettingsService ttaxSettingsService;
	private static TaxSettingsService taxSettingsService;*/
	
	@Autowired
	private EmployeeCompensationService temployeeCompensationService;
	private static EmployeeCompensationService employeeCompensationService;
	
	@Autowired
	private DASettingsService tdaSettingsService;
	private static DASettingsService daSettingsService;
	
	@Autowired
	private CompanyCalenderService tcompanyCalenderService;
	private static CompanyCalenderService companyCalenderService;
	
	@Autowired
	private PtService tptService;
	private static PtService ptService;
	
	@Autowired
	private ItSavingsService titSavingsService;
	private static ItSavingsService itSavingsService;
	

	@Autowired
	private ItSectionService titSectionService;
	private static ItSectionService itSectionService;
	
	@Autowired
	private TaxSlabsSettingsService ttaxSlabsSettingsService;
	private static TaxSlabsSettingsService taxSlabsSettingsService;
	
	@Autowired
	private EducationCessService teducationCessService;
	private static EducationCessService educationCessService;
	
	@Autowired
	private WorkingDayAllowanceService tworkingDayAllowanceService;
	private static WorkingDayAllowanceService workingDayAllowanceService;
	
	@Autowired
	private AttendanceService tattendanceService;
	private static AttendanceService attendanceService;
	
	@Autowired
	private PFService tpfService;
	private static PFService pfService;
	
	@Autowired
	private ESIService tesiService;
	private static ESIService esiService;
	
	@Autowired
	private EmployeePFInfoService tempPFService;
	private static EmployeePFInfoService empPFService;
	
	@Autowired
	private EmployeeESIInfoService tempESIService;
	private static EmployeeESIInfoService empESIService;
	
	
	@Autowired
	private LeaveRequestsService tleaveRequestsService;
	private static LeaveRequestsService leaveRequestsService;
	
	@Autowired
	private EmployeeLeavegroupService temployeeLeavegroupService;
	private static EmployeeLeavegroupService employeeLeavegroupService;
	
	@Autowired
	private LeaveGroupSettingsService tleaveGroupSettingsService;
	private static LeaveGroupSettingsService leaveGroupSettingsService;
	
	@Autowired
	private AdvancePaymentDetailService tadvancePaymentDetailService;
	private static AdvancePaymentDetailService advancePaymentDetailService;
	
	@Autowired
	private InsurancePaymentDetailService tinsurancePaymentDetailService;
	private static InsurancePaymentDetailService insurancePaymentDetailService;
	
	@Autowired
	private GeneralDeductionDetailService tloanPaymentDetailService;
	private static GeneralDeductionDetailService loanPaymentDetailService;
	
	@Autowired
	private ItSlabEffectiveService titSlabEffectiveService;
	private static ItSlabEffectiveService itSlabEffectiveService;
	
	@Autowired
	private GeneralEarningService tgeneralEarningService;
	private static GeneralEarningService generalEarningService;
	
	@Autowired
	private HolidaysService tholidaysService;
	private static HolidaysService holidaysService;
	
	@Autowired
	private EmployeeExpenseService temployeeExpenseService;
	private static EmployeeExpenseService employeeExpenseService;
	
	@Autowired
	private WorkingDayLeaveTypeService tworkingDayLeaveTypeService;
	private static WorkingDayLeaveTypeService workingDayLeaveTypeService;
	
	
	
	
	@PostConstruct
	public void init() {
		employeeCtcService = temployeeCtcService;
		employeePayrollService = temployeePayrollService;
		employeeBonusService = temployeeBonusService;
		employeeLopService = temployeeLopService;
		/*taxSettingsService = ttaxSettingsService;
		deductionSettingsService = tdeductionSettingsService;
		allowanceSettingsService = tallowanceSettingsService;
		employeeDeductionService = temployeeDeductionService;*/
		itSavingsService = titSavingsService;
		/*employeeTaxService = temployeeTaxService;
		employeeEarningsService = temployeeEarningsService;*/
		//employeeService = temployeeService;
		employeeCompensationService = temployeeCompensationService;
		daSettingsService = tdaSettingsService;
		companyCalenderService = tcompanyCalenderService;
		ptService = tptService;
		itSectionService = titSectionService;
		taxSlabsSettingsService = ttaxSlabsSettingsService;
		educationCessService = teducationCessService;
		workingDayAllowanceService = tworkingDayAllowanceService;
		attendanceService = tattendanceService;
		pfService = tpfService;
		esiService = tesiService;
		empPFService = tempPFService;
		empESIService = tempESIService;
		leaveRequestsService = tleaveRequestsService;
		leaveGroupSettingsService = tleaveGroupSettingsService;
		employeeLeavegroupService = temployeeLeavegroupService;
		advancePaymentDetailService = tadvancePaymentDetailService;
		loanPaymentDetailService = tloanPaymentDetailService;
		insurancePaymentDetailService = tinsurancePaymentDetailService;
		itSlabEffectiveService = titSlabEffectiveService;
		generalEarningService = tgeneralEarningService;
		holidaysService = tholidaysService;
		employeeExpenseService = temployeeExpenseService;
		workingDayLeaveTypeService = tworkingDayLeaveTypeService;
	}
	
	
	public static JSONObject getempActiveCompensation(List<EmployeeCompensationDO> empCompensationList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeeCompensationDO empCompensation : empCompensationList) {
				if(empCompensation.getEmployee() != null){
					if(empCompensation.getEmployee().getStatus() == CommonConstants.ACTIVE){
						resultJSONArray.put(getCompensationDetailObject(empCompensation));
					}
				}
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getempCompensationDetails(List<EmployeeCompensationDO> empCompensationList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeeCompensationDO empCompensation : empCompensationList) {
					resultJSONArray.put(getCompensationDetailInfoObject(empCompensation));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getCompensationDetailObject(EmployeeCompensationDO empCompensation)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(empCompensation.getEmployeeCompensationId()));
		result.put(CommonConstants.EMPID, String.valueOf(empCompensation.getEmployee().getEmpId()));
		/*if(empCompensation.getEmployee().getMiddlename() != null) result.put(CommonConstants.EMPNAME, String.valueOf(empCompensation.getEmployee().getFirstname() + " " +empCompensation.getEmployee().getMiddlename() + " " + empCompensation.getEmployee().getLastname()));
		else result.put(CommonConstants.EMPNAME, String.valueOf(empCompensation.getEmployee().getFirstname() + " " + empCompensation.getEmployee().getLastname()));*/
		//result.put(CommonConstants.EMPCTC, String.valueOf(empCompensation.getEmpCtc().getEmpctc()));
		if(empCompensation.getEmployee().getMiddlename() != null) result.put(CommonConstants.EMPNAME, String.valueOf(empCompensation.getEmployee().getFirstname() + " " +empCompensation.getEmployee().getMiddlename() + " " + empCompensation.getEmployee().getLastname()));
		else if(empCompensation.getEmployee().getMiddlename() != null && empCompensation.getEmployee().getLastname() == null ) result.put(CommonConstants.EMPNAME, String.valueOf(empCompensation.getEmployee().getFirstname() + " " +empCompensation.getEmployee().getMiddlename()));
		else if(empCompensation.getEmployee().getMiddlename() == null && empCompensation.getEmployee().getLastname() == null ) result.put(CommonConstants.EMPNAME,String.valueOf(empCompensation.getEmployee().getFirstname()));
		else result.put(CommonConstants.EMPNAME, String.valueOf(empCompensation.getEmployee().getFirstname() + " " + empCompensation.getEmployee().getLastname()));
		
		
		
		List<EmployeeCtcDO> ctcList = employeeCtcService.retrieveActive(empCompensation.getEmployee().getEmpId());
		if(ctcList != null && ctcList.size() > 0){
			Calendar c=Calendar.getInstance();
	        int days = 0;
	        days = c.getActualMaximum(Calendar.DAY_OF_MONTH); 
			 double permothSalary = 0.0;
		        if(ctcList.get(0).getCtcType() != null){
		        	 if(ctcList.get(0).getCtcType().equalsIgnoreCase("y"))  permothSalary = (double) (ctcList.get(0).getEmpctc()/12);
				     if(ctcList.get(0).getCtcType().equalsIgnoreCase("m"))  permothSalary = (double) ctcList.get(0).getEmpctc();
				     if(ctcList.get(0).getCtcType().equalsIgnoreCase("d"))  permothSalary = (double) ctcList.get(0).getEmpctc()*days;
		        }else{
		        	
		        	double monthctc = ctcList.get(0).getEmpctc()/12;
		        	permothSalary = (double) (monthctc/days);
		        }
			result.put(CommonConstants.EMPCTC, String.valueOf(Math.round((double) (permothSalary*12))));
			result.put(CommonConstants.EMPCTCID, String.valueOf(ctcList.get(0).getEmpCtcId()));
			result.put(CommonConstants.STARTDATE, String.valueOf(ctcList.get(0).getStartdate()));
			result.put(CommonConstants.ENDDATE, String.valueOf(ctcList.get(0).getEnddate()));
		}else{
			result.put(CommonConstants.EMPCTC, String.valueOf("Active Ctc Not Available"));
		}
		if(empCompensation.getPayrollGroup() != null){
			result.put(CommonConstants.PAYROLLGROUPID, String.valueOf(empCompensation.getPayrollGroup().getPayrollGroupId()));
			result.put(CommonConstants.PAYROLLGROUPNAME, String.valueOf(empCompensation.getPayrollGroup().getName()));
		}
		if(empCompensation.getEmpCtc().getPayrollBatch() != null){
			result.put(CommonConstants.PAYROLLBATCHNAME, String.valueOf(empCompensation.getEmpCtc().getPayrollBatch().getName()));
			result.put(CommonConstants.PAYROLLBATCHID, String.valueOf(empCompensation.getEmpCtc().getPayrollBatch().getPayrollBatchId()));
		}
		
		return result;
	}
	
	public static JSONObject getempPayrollDetails(List<EmployeeCtcDO> empCtcList, String month, Date endDate) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeeCtcDO empCtc : empCtcList) {
				List<EmployeePayrollDO> payrollList = employeePayrollService.retriveByMonthAndEmp(month, empCtc.getEmployee().getEmpId());
				if(payrollList == null || payrollList.size() <= 0 || payrollList.get(0).getStatus() == 'v'){
					List<EmployeeCompensationDO> empCompensationList = employeeCompensationService.retrieveWithComponsesationDateAndEmpID(empCtc.getEmployee().getEmpId().toString(), endDate);
					if(empCompensationList != null && empCompensationList.size() >0){
						resultJSONArray.put(getempPayrollDetailsObject(empCompensationList.get(0), month, empCtc));
					}
				}else{
					resultJSONArray.put(getempPayrollDetailsObject1(payrollList.get(0), month, empCtc));
					
				}
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	/*public static JSONArray getempPayrollDetailsObject(EmployeeCtcDO empCtc, String month)throws JSONException {
		JSONArray resultJSONArray = new JSONArray();
		List<EmployeePayrollDO> payrollList = employeePayrollService.retriveByMonthAndEmp(month, empCtc.getEmployee().getEmpId());
			if(payrollList == null || payrollList.size() <= 0 || payrollList.get(0).getStatus() == 'v'){
				List<EmployeeCompensationDO> empCompensationList = employeeCompensationService.retrieveWithComponsesationDateAndEmpID(empCtc.getEmployee().getEmpId().toString(), CommonUtil.convertMonthToSqlDate(month));
				if(empCompensationList != null && empCompensationList.size() >0){
					resultJSONArray.put(getempPayrollDetailsObject(empCompensationList.get(0), month, empCtc));
				}
				
			
			}else{
				resultJSONArray.put(getempPayrollDetailsObject1(payrollList.get(0), month, empCtc));
				
			}
		
		
		return resultJSONArray;
	}*/
	
	/*public static JSONObject getempPayrollDetails(List<EmployeeCompensationDO> empCompensationList, String month) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeeCompensationDO empCompensation : empCompensationList) {
					resultJSONArray.put(getempPayrollDetailsObject(empCompensation, month));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}*/
	
	
	public static JSONObject getCompensationDetailInfoObject(EmployeeCompensationDO empCompensation)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(empCompensation.getEmployeeCompensationId()));
		result.put(CommonConstants.EMPID, String.valueOf(empCompensation.getEmployee().getEmpId()));
		/*if(empCompensation.getEmployee().getMiddlename() != null) result.put(CommonConstants.EMPNAME, String.valueOf(empCompensation.getEmployee().getFirstname() + " " +empCompensation.getEmployee().getMiddlename() + " " + empCompensation.getEmployee().getLastname()));
		else result.put(CommonConstants.EMPNAME, String.valueOf(empCompensation.getEmployee().getFirstname() + " " + empCompensation.getEmployee().getLastname()));*/
		
		if(empCompensation.getEmployee().getMiddlename() != null) result.put(CommonConstants.EMPNAME, String.valueOf(empCompensation.getEmployee().getFirstname() + " " +empCompensation.getEmployee().getMiddlename() + " " + empCompensation.getEmployee().getLastname()));
		else if(empCompensation.getEmployee().getMiddlename() != null && empCompensation.getEmployee().getLastname() == null ) result.put(CommonConstants.EMPNAME, String.valueOf(empCompensation.getEmployee().getFirstname() + " " +empCompensation.getEmployee().getMiddlename()));
		else if(empCompensation.getEmployee().getMiddlename() == null && empCompensation.getEmployee().getLastname() == null ) result.put(CommonConstants.EMPNAME,String.valueOf(empCompensation.getEmployee().getFirstname()));
		else result.put(CommonConstants.EMPNAME, String.valueOf(empCompensation.getEmployee().getFirstname() + " " + empCompensation.getEmployee().getLastname()));
		
		
		//result.put(CommonConstants.EMPCTC, String.valueOf(empCompensation.getEmpCtc().getEmpctc()));
		
		Double activeCtc = 0.0;
		List<EmployeeCtcDO> ctcList = employeeCtcService.retrieveActive(empCompensation.getEmployee().getEmpId());
		if(ctcList != null && ctcList.size() > 0){
			Calendar c=Calendar.getInstance();
	        int days = 0;
	        days = c.getActualMaximum(Calendar.DAY_OF_MONTH); 
			 double permothSalary = 0.0;
		        if(ctcList.get(0).getCtcType() != null){
		        	 if(ctcList.get(0).getCtcType().equalsIgnoreCase("y"))  permothSalary = (double) (ctcList.get(0).getEmpctc()/12);
				     if(ctcList.get(0).getCtcType().equalsIgnoreCase("m"))  permothSalary = (double) ctcList.get(0).getEmpctc();
				     if(ctcList.get(0).getCtcType().equalsIgnoreCase("d"))  permothSalary = (double) ctcList.get(0).getEmpctc()*days;
		        }else{
		        	double monthctc = ctcList.get(0).getEmpctc()/12;
		        	permothSalary = (double) (monthctc/days);
		        }
			activeCtc = (double) (permothSalary*12);
			result.put(CommonConstants.EMPCTC, String.valueOf(Math.round(activeCtc)));
			result.put(CommonConstants.EMPCTCID, String.valueOf(ctcList.get(0).getEmpCtcId()));
			result.put(CommonConstants.STARTDATE, String.valueOf(ctcList.get(0).getStartdate()));
			result.put(CommonConstants.ENDDATE, String.valueOf(ctcList.get(0).getEnddate()));
		}else{
			result.put(CommonConstants.EMPCTC, String.valueOf("Active Ctc Not Available"));
		}
		
		
		result.put(CommonConstants.PAYROLLGROUPID, String.valueOf(empCompensation.getPayrollGroup().getPayrollGroupId()));
		result.put(CommonConstants.PAYROLLGROUPNAME, String.valueOf(empCompensation.getPayrollGroup().getName()));
		basicPay = 0.0;
		for (PayrollGroupEarningsDO earningDetails :  empCompensation.getPayrollGroup().getPayrollGroupEarning()) {
			if(earningDetails.getStatus() == CommonConstants.ACTIVE && earningDetails.getAllowanceSettings().getEarningType().getName().equalsIgnoreCase(CommonConstants.BASIC)){
				if(earningDetails.getAllowanceSettings().getEarningType().getStatus() == CommonConstants.ACTIVE){
					if(earningDetails.getAllowanceSettings().getPercentageamount() != null){
						basicPay = ((Double.parseDouble(empCompensation.getEmpCtc().getEmpctc().toString())/12) * Double.parseDouble(earningDetails.getAllowanceSettings().getPercentageamount()))/100;
					}
				}
			}
				
		}
		
		if(basicPay != 0){
			JSONArray earningResultJSONArray = new JSONArray();
			 earingTotal = 0.0;
			 taxableIncome = 0.0;
			 Boolean Adjustment = false;
			  PayrollGroupEarningsDO adjustData = new PayrollGroupEarningsDO();
			for (PayrollGroupEarningsDO earningDetails :  empCompensation.getPayrollGroup().getPayrollGroupEarning()) {
				if(earningDetails.getStatus() == CommonConstants.ACTIVE){
					if(!earningDetails.getAllowanceSettings().getType().equalsIgnoreCase("A")){
						if(!earningDetails.getAllowanceSettings().getEarningType().getName().equalsIgnoreCase(CommonConstants.DEARNESSALLOWEANCE) && !earningDetails.getAllowanceSettings().getEarningType().getName().equalsIgnoreCase(CommonConstants.ADD_DA) && !earningDetails.getAllowanceSettings().getEarningType().getName().equalsIgnoreCase(CommonConstants.ADDDEARNESSALLOWEANCE) ){ 
							earningResultJSONArray.put(getCompensationAllowenceDetailInfoObject(earningDetails.getPayrollGroupEarningsId(),earningDetails.getAllowanceSettings(), Double.parseDouble(activeCtc.toString()),0,0));
						}
					}else{
						Adjustment = true;
						adjustData = earningDetails;
					}
				}
			}
			if(Adjustment){
				earningResultJSONArray.put(getAdustmentDetails(adjustData.getPayrollGroupEarningsId(),adjustData.getAllowanceSettings(), Double.parseDouble(activeCtc.toString())));
					
				
			}
			result.put(CommonConstants.EARNINGMONTHLY, String.valueOf(Math.round(earingTotal)));
			//result.put(CommonConstants.EARNINGMONTHLY, String.valueOf(CommonUtil.convertfloatValueWithTwoDecimalPoints(earingTotal)));
			result.put(CommonConstants.EARNINGLIST, earningResultJSONArray);
			
			deductionTotal = 0.0;
			JSONArray deductionResultJSONArray = new JSONArray();
			for (PayrollGroupDeductionDO deductionDetails :  empCompensation.getPayrollGroup().getPayrollGroupDeduction()) {
				if(deductionDetails.getStatus() == CommonConstants.ACTIVE){
					if(!deductionDetails.getDeductionSettings().getDeductionType().getName().equalsIgnoreCase(CommonConstants.SUBDEARNESSALLOWEANCE)){
						deductionResultJSONArray.put(getCompensationDeductionDetailInfoObject(deductionDetails.getPayrollGroupDeductionId(),deductionDetails.getDeductionSettings(), Double.parseDouble(activeCtc.toString())));
					}
				}
					
			}
			result.put(CommonConstants.DEDUCTIONMONTHLY, String.valueOf(Math.round(deductionTotal)));
			//result.put(CommonConstants.DEDUCTIONMONTHLY, String.valueOf(CommonUtil.convertfloatValueWithTwoDecimalPoints(deductionTotal)));
			result.put(CommonConstants.DEDUCTIONLIST, deductionResultJSONArray);
			
			taxTotal = 0.0;
			Boolean applyPt = false;
			Boolean applyEducationCess = false;
			Boolean applyTDS = false;
			JSONArray taxResultJSONArray = new JSONArray();
			for (PayrollGroupTaxDO taxDetails :  empCompensation.getPayrollGroup().getPayrollGroupTax()) {
				if(taxDetails.getStatus() == CommonConstants.ACTIVE && taxDetails.getPt() != null  && taxDetails.getPt().toString().equalsIgnoreCase(CommonConstants.APPLY))  applyPt = true; 
				if(taxDetails.getStatus() == CommonConstants.ACTIVE && taxDetails.getTds() != null  && taxDetails.getTds().toString().equalsIgnoreCase(CommonConstants.APPLY)) applyTDS = true;//taxResultJSONArray.put(getTDSObject(empCompensation.getEmployee(), Double.parseDouble(activeCtc.toString())));
				if(taxDetails.getStatus() == CommonConstants.ACTIVE && taxDetails.getEducationCess() != null  && taxDetails.getEducationCess().equalsIgnoreCase(CommonConstants.APPLY))  applyEducationCess = true;
				if(taxDetails.getStatus() == CommonConstants.ACTIVE && taxDetails.getPt() == null && taxDetails.getTds() == null  && taxDetails.getEducationCess()  == null){
					taxResultJSONArray.put(getCompensationTaxDetailInfoObject(taxDetails.getPayrollGroupTaxId(),taxDetails.getTaxSettings(), Double.parseDouble(empCompensation.getEmpCtc().getEmpctc().toString())));
				}
			}
			//result.put(CommonConstants.TAXMONTHLY, String.valueOf(taxTotal));
		//	result.put(CommonConstants.TAXLIST, taxResultJSONArray);
			
			/*if(applyPt) {
				taxResultJSONArray.put(getPTObject(empCompensation.getEmployee(),(earingTotal - deductionTotal)));
				if(applyEducationCess){
					taxResultJSONArray.put(getEducationCessObject());
				}
				result.put(CommonConstants.TAXMONTHLY, String.valueOf(taxTotal));
				result.put(CommonConstants.TAXLIST, taxResultJSONArray);
			}else{
				result.put(CommonConstants.TAXMONTHLY, String.valueOf(taxTotal));
				result.put(CommonConstants.TAXLIST, taxResultJSONArray);
			}*/
			
			if(applyTDS){
				if(applyPt) {
					taxResultJSONArray.put(getPTObject(empCompensation.getEmployee(),(earingTotal - deductionTotal)));
				}
				taxResultJSONArray.put(getTDSObject(empCompensation.getEmployee(), Double.parseDouble(activeCtc.toString()), ""));
				if(applyEducationCess){
					taxResultJSONArray.put(getEducationCessObject());
				}
				result.put(CommonConstants.TAXMONTHLY, String.valueOf(Math.round(taxTotal)));
				//result.put(CommonConstants.TAXMONTHLY, String.valueOf(CommonUtil.convertfloatValueWithTwoDecimalPoints(taxTotal)));
				result.put(CommonConstants.TAXLIST, taxResultJSONArray);
			}else{
				if(applyPt) {
					taxResultJSONArray.put(getPTObject(empCompensation.getEmployee(),(earingTotal - deductionTotal)));
				}
				if(applyEducationCess){
					taxResultJSONArray.put(getEducationCessObject());
				}
				result.put(CommonConstants.TAXMONTHLY, String.valueOf(Math.round(taxTotal)));
				result.put(CommonConstants.TAXLIST, taxResultJSONArray);
			}
			Double mnthlyNet = earingTotal - (deductionTotal + taxTotal );
			result.put(CommonConstants.NETMONTHLY, String.valueOf(Math.round(mnthlyNet)));
		}else{
			JSONArray earningResultJSONArray = new JSONArray();
			 earingTotal = 0.0;
			 taxableIncome = 0.0;
			 Boolean Adjustment = false;
			  PayrollGroupEarningsDO adjustData = new PayrollGroupEarningsDO();
			for (PayrollGroupEarningsDO earningDetails :  empCompensation.getPayrollGroup().getPayrollGroupEarning()) {
				if(earningDetails.getStatus() == CommonConstants.ACTIVE){
					if(!earningDetails.getAllowanceSettings().getType().equalsIgnoreCase("A")){
						if(!earningDetails.getAllowanceSettings().getEarningType().getName().equalsIgnoreCase(CommonConstants.DEARNESSALLOWEANCE) && !earningDetails.getAllowanceSettings().getEarningType().getName().equalsIgnoreCase(CommonConstants.ADD_DA) && !earningDetails.getAllowanceSettings().getEarningType().getName().equalsIgnoreCase(CommonConstants.ADDDEARNESSALLOWEANCE) ){ 
							earningResultJSONArray.put(getCompensationAllowenceDetailInfoObject(earningDetails.getPayrollGroupEarningsId(),earningDetails.getAllowanceSettings(), Double.parseDouble(activeCtc.toString()), 0, 0));
						}
					}else{
						Adjustment = true;
						adjustData = earningDetails;
					}
				}
			}
			if(Adjustment){
				earningResultJSONArray.put(getAdustmentDetails(adjustData.getPayrollGroupEarningsId(),adjustData.getAllowanceSettings(), Double.parseDouble(activeCtc.toString())));
					
				
			}
			result.put(CommonConstants.EARNINGMONTHLY, String.valueOf(CommonUtil.convertfloatValueWithTwoDecimalPoints(earingTotal)));
			result.put(CommonConstants.EARNINGLIST, earningResultJSONArray);
			
			deductionTotal = 0.0;
			JSONArray deductionResultJSONArray = new JSONArray();
			for (PayrollGroupDeductionDO deductionDetails :  empCompensation.getPayrollGroup().getPayrollGroupDeduction()) {
				if(deductionDetails.getStatus() == CommonConstants.ACTIVE){
					if(!deductionDetails.getDeductionSettings().getDeductionType().getName().equalsIgnoreCase(CommonConstants.SUBDEARNESSALLOWEANCE)){
						deductionResultJSONArray.put(getCompensationDeductionDetailInfoObject(deductionDetails.getPayrollGroupDeductionId(),deductionDetails.getDeductionSettings(), Double.parseDouble(activeCtc.toString())));
					}
				}
					
			}
			result.put(CommonConstants.DEDUCTIONMONTHLY, String.valueOf(CommonUtil.convertfloatValueWithTwoDecimalPoints(deductionTotal)));
			result.put(CommonConstants.DEDUCTIONLIST, deductionResultJSONArray);
			
			taxTotal = 0.0;
			Boolean applyPt = false;
			Boolean applyEducationCess = false;
			Boolean applyTDS = false;
			JSONArray taxResultJSONArray = new JSONArray();
			for (PayrollGroupTaxDO taxDetails :  empCompensation.getPayrollGroup().getPayrollGroupTax()) {
				if(taxDetails.getStatus() == CommonConstants.ACTIVE && taxDetails.getPt() != null  && taxDetails.getPt().toString().equalsIgnoreCase(CommonConstants.APPLY))  applyPt = true; 
				if(taxDetails.getStatus() == CommonConstants.ACTIVE && taxDetails.getTds() != null  && taxDetails.getTds().toString().equalsIgnoreCase(CommonConstants.APPLY)) applyTDS = true;//taxResultJSONArray.put(getTDSObject(empCompensation.getEmployee(), Double.parseDouble(activeCtc.toString())));
				if(taxDetails.getStatus() == CommonConstants.ACTIVE && taxDetails.getEducationCess() != null  && taxDetails.getEducationCess().equalsIgnoreCase(CommonConstants.APPLY))  applyEducationCess = true;
				if(taxDetails.getStatus() == CommonConstants.ACTIVE && taxDetails.getPt() == null && taxDetails.getTds() == null  && taxDetails.getEducationCess()  == null){
					taxResultJSONArray.put(getCompensationTaxDetailInfoObject(taxDetails.getPayrollGroupTaxId(),taxDetails.getTaxSettings(), Double.parseDouble(empCompensation.getEmpCtc().getEmpctc().toString())));
				}
			}
			//result.put(CommonConstants.TAXMONTHLY, String.valueOf(taxTotal));
		//	result.put(CommonConstants.TAXLIST, taxResultJSONArray);
			
			/*if(applyPt) {
				taxResultJSONArray.put(getPTObject(empCompensation.getEmployee(),(earingTotal - deductionTotal)));
				if(applyEducationCess){
					taxResultJSONArray.put(getEducationCessObject());
				}
				result.put(CommonConstants.TAXMONTHLY, String.valueOf(taxTotal));
				result.put(CommonConstants.TAXLIST, taxResultJSONArray);
			}else{
				result.put(CommonConstants.TAXMONTHLY, String.valueOf(taxTotal));
				result.put(CommonConstants.TAXLIST, taxResultJSONArray);
			}*/
			
			
			if(applyTDS){
				if(applyPt) {
					taxResultJSONArray.put(getPTObject(empCompensation.getEmployee(),(earingTotal - deductionTotal)));
				}
				taxResultJSONArray.put(getTDSObject(empCompensation.getEmployee(), Double.parseDouble(activeCtc.toString()),""));
				if(applyEducationCess){
					taxResultJSONArray.put(getEducationCessObject());
				}
				result.put(CommonConstants.TAXMONTHLY, String.valueOf(CommonUtil.convertfloatValueWithTwoDecimalPoints(taxTotal)));
				result.put(CommonConstants.TAXLIST, taxResultJSONArray);
			}else{
				if(applyPt) {
					taxResultJSONArray.put(getPTObject(empCompensation.getEmployee(),(earingTotal - deductionTotal)));
				}
				if(applyEducationCess){
					taxResultJSONArray.put(getEducationCessObject());
				}
				result.put(CommonConstants.TAXMONTHLY, String.valueOf(CommonUtil.convertfloatValueWithTwoDecimalPoints(taxTotal)));
				result.put(CommonConstants.TAXLIST, taxResultJSONArray);
			}
			Double mnthlyNet = earingTotal - (deductionTotal + taxTotal );
			result.put(CommonConstants.NETMONTHLY, String.valueOf(CommonUtil.convertfloatValueWithTwoDecimalPoints(mnthlyNet)));
		}
		
		return result;
	}
	
	
	public static JSONObject getCompensationAllowenceDetailInfoObject(Long earninId, AllowanceSettingDO allowance, Double ctc, double noOfWorkingday, int salaryDays)throws JSONException {
		JSONObject result = new JSONObject();
		Double grossPay = ctc/12;
		//Double basicPay = 0D;
		Double monthlypay=0D;
		Double ytd=0D;
			if(allowance.getEarningType().getName() != null && allowance.getEarningType().getName().equalsIgnoreCase("Basic")){
				if(allowance.getBasicgrosspay() != null && allowance.getBasicgrosspay().equalsIgnoreCase("G")){
					if(allowance.getPercentageamount() != null){
						basicPay = (grossPay * Double.parseDouble(allowance.getPercentageamount()))/100;
						ytd = basicPay * 12;
						monthlypay = basicPay;
					}
				}
			}else{
				//basicPay = (grossPay*Double.parseDouble(CommonConstants.BACSICPAY))/100;
				
				if(allowance.getType() != null && allowance.getType().equalsIgnoreCase("P")){
					if(allowance.getBasicgrosspay() != null && allowance.getBasicgrosspay().equalsIgnoreCase("B")){
						if(allowance.getPercentageamount() != null){
							monthlypay = (basicPay * Long.parseLong(allowance.getPercentageamount()))/100;
							ytd = monthlypay*12;
						}
					}else if(allowance.getBasicgrosspay() != null && allowance.getBasicgrosspay().equalsIgnoreCase("G")){
						if(allowance.getPercentageamount() != null){
							monthlypay = (grossPay * Double.parseDouble(allowance.getPercentageamount()))/100;
							ytd = monthlypay*12;
						}
					}
				}else if(allowance.getType() != null && allowance.getType().equalsIgnoreCase("F")){
					if(allowance.getFixedamount() != null){
						ytd = Double.parseDouble(allowance.getFixedamount());
						monthlypay = ytd/12;
						if(salaryDays > 0 && noOfWorkingday > 0 ){
							monthlypay = (monthlypay/salaryDays)*noOfWorkingday;
						}
					}
				}
			}
			
			earingTotal += Math.round(monthlypay);
			if(allowance.getTaxable().equalsIgnoreCase("Y")){
				taxableIncome += ytd;
			}
		//result.put(CommonConstants.MONTHLY, String.valueOf(CommonUtil.convertfloatValueWithTwoDecimalPoints(monthlypay)));
			result.put(CommonConstants.MONTHLY, String.valueOf(Math.round(monthlypay)));
		result.put(CommonConstants.YTD, String.valueOf(ytd));
		result.put(CommonConstants.NAME, String.valueOf(allowance.getDisplayname()));
		result.put(CommonConstants.EARNINGID, String.valueOf(earninId));
		
		
		return result;
	}
	
	
	
	
	public static JSONObject getGeneralEarningObject(GeneralEarningDO earningList)throws JSONException {
		JSONObject result = new JSONObject();
		
		Long monthlypay = 0L;
		if(earningList.getAmount() != null){
			monthlypay += earningList.getAmount();
		}
			earingTotal += Math.round(monthlypay);
			totalAllowance += Math.round(monthlypay);
			/*if(allowance.getTaxable().equalsIgnoreCase("Y")){
				taxableIncome += ytd;
			}*/
		//result.put(CommonConstants.MONTHLY, String.valueOf(CommonUtil.convertfloatValueWithTwoDecimalPoints(monthlypay)));
			result.put(CommonConstants.MONTHLY, String.valueOf(Math.round(monthlypay)));
		//result.put(CommonConstants.YTD, String.valueOf(ytd));
		result.put(CommonConstants.NAME, String.valueOf(earningList.getEarningSubType().getName()));
		result.put(CommonConstants.EARNINGID, String.valueOf(earningList.getGeneralEarningId()));
		
		
		return result;
	}
	
	public static JSONObject getEmpAvailbleComponsationDeatils(List<EmployeeCompensationDO> empCompensationList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeeCompensationDO empCompensation : empCompensationList) {
					resultJSONArray.put(getEmpAvailbleComponsationDeatilsObject(empCompensation));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getCompensationDeductionDetailInfoObject(Long deductionId, DeductionSettingDO deduction, Double ctc)throws JSONException {
		JSONObject result = new JSONObject();
		Double grossPay = ctc/12;
		//Double basicPay = basic;
		Double monthlypay=0D;
		Double ytd=0D;
		if(deduction.getType() != null && deduction.getType().equalsIgnoreCase("P")){
			if(deduction.getBasicgrosspay() != null && deduction.getBasicgrosspay().equalsIgnoreCase("B")){
				if(deduction.getPercentageamount() != null){
					monthlypay = (basicPay * Double.parseDouble(deduction.getPercentageamount()))/100;
					ytd = monthlypay*12;
				}
			}else if(deduction.getBasicgrosspay() != null && deduction.getBasicgrosspay().equalsIgnoreCase("G")){
				if(deduction.getPercentageamount() != null){
					monthlypay = (grossPay * Double.parseDouble(deduction.getPercentageamount()))/100;
					ytd = monthlypay*12;
				}
			}
		}else if(deduction.getType() != null && deduction.getType().equalsIgnoreCase("F")){
			if(deduction.getFixedamount() != null){
				ytd = Double.parseDouble(deduction.getFixedamount());
				monthlypay = ytd/12;
			}
		}
		deductionTotal += Math.round(monthlypay);
		result.put(CommonConstants.MONTHLY, String.valueOf(Math.round(monthlypay)));
		//result.put(CommonConstants.MONTHLY, String.valueOf(CommonUtil.convertfloatValueWithTwoDecimalPoints(monthlypay)));
		result.put(CommonConstants.YTD, String.valueOf(ytd));
		result.put(CommonConstants.NAME, String.valueOf(deduction.getDisplayname()));
		result.put(CommonConstants.DEDUCTIONID, String.valueOf(deductionId));
		return result;
	}
	
	
	public static double getLeaveLOPDetails(List<LeaveRequestsDO> leaveRequestsList, Double ctc)throws JSONException {
		
		double leaveCount = 0;
		for (LeaveRequestsDO leaveRequest : leaveRequestsList) {
				Date dateBeforeString = leaveRequest.getFromDate();
				 if(leaveRequest.getToDate() != null){
					 Calendar end = Calendar.getInstance();
					 Date dateAfterString = leaveRequest.getToDate();
					  end.setTime(dateAfterString);
					  Calendar start = Calendar.getInstance();
					  start.setTime(dateBeforeString);
					 for (Date date = start.getTime(); start.before(end) || start.equals(end); start.add(Calendar.DATE, 1), date = start.getTime()) {
						Calendar weekend = Calendar.getInstance();
						weekend.setTime(date);
						if (/*weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && */
							weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){
							 String leaveType =  String.valueOf(leaveRequest.getType());
							 if( leaveType  != null && leaveType.equalsIgnoreCase("p") ){
								   leaveCount = leaveCount+1;
							 }else{
								 if( leaveType  != null && leaveType.equalsIgnoreCase("h") ){
									 leaveCount =  (leaveCount+ 0.5);
								 }else{
									 if( leaveType  != null && leaveType.equalsIgnoreCase("f") ){
										 leaveCount = leaveCount+1;
									 }
								 }
							  }
							
						}
					}
				 }else{
					  Calendar start = Calendar.getInstance();
					  start.setTime(dateBeforeString);
					  Date date = start.getTime();
						 Calendar weekend = Calendar.getInstance();
						 weekend.setTime(date);
							if (/*weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY*/
								weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){
							   String leaveType =  String.valueOf(leaveRequest.getType());
							   if( leaveType  != null && leaveType.equalsIgnoreCase("p") ){
								   leaveCount = leaveCount+1;
							   }else{
								 if( leaveType  != null && leaveType.equalsIgnoreCase("h") ){
									 leaveCount =  (leaveCount+ 0.5);
								 }else{
									 if( leaveType  != null && leaveType.equalsIgnoreCase("f") ){
										 leaveCount = leaveCount+1;
									 }
								 }
							   }
								
								
							}
				 }
			     
		}
		return leaveCount;
	}
	
	
	public static double getLeaveDetails(List<LeaveRequestsDO> leaveRequestsList, Double ctc)throws JSONException {
		
		double leaveCount = 0;
		for (LeaveRequestsDO leaveRequest : leaveRequestsList) {
			if(leaveRequest.getStatus() == 'a' && leaveRequest.getStatus() != 'r' && leaveRequest.getStatus() != 'p' &&  leaveRequest.getStatus() != 'c'){
				Date dateBeforeString = leaveRequest.getFromDate();
				 if(leaveRequest.getToDate() != null){
					 Calendar end = Calendar.getInstance();
					 Date dateAfterString = leaveRequest.getToDate();
					  end.setTime(dateAfterString);
					  Calendar start = Calendar.getInstance();
					  start.setTime(dateBeforeString);
					 for (Date date = start.getTime(); start.before(end) || start.equals(end); start.add(Calendar.DATE, 1), date = start.getTime()) {
						Calendar weekend = Calendar.getInstance();
						weekend.setTime(date);
						if (/*weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && */
							weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){
							 String leaveType =  String.valueOf(leaveRequest.getType());
							 if( leaveType  != null && leaveType.equalsIgnoreCase("p") ){
								   leaveCount = leaveCount+1;
							 }else{
								 if( leaveType  != null && leaveType.equalsIgnoreCase("h") ){
									 leaveCount =  (leaveCount+ 0.5);
								 }else{
									 if( leaveType  != null && leaveType.equalsIgnoreCase("f") ){
										 leaveCount = leaveCount+1;
									 }
								 }
							  }
							
						}
					}
				 }else{
					  Calendar start = Calendar.getInstance();
					  start.setTime(dateBeforeString);
					  Date date = start.getTime();
						 Calendar weekend = Calendar.getInstance();
						 weekend.setTime(date);
							if (/*weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && */
								weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){
							   String leaveType =  String.valueOf(leaveRequest.getType());
							   if( leaveType  != null && leaveType.equalsIgnoreCase("p") ){
								   leaveCount = leaveCount+1;
							   }else{
								 if( leaveType  != null && leaveType.equalsIgnoreCase("h") ){
									 leaveCount =  (leaveCount+ 0.5);
								 }else{
									 if( leaveType  != null && leaveType.equalsIgnoreCase("f") ){
										 leaveCount = leaveCount+1;
									 }
								 }
							   }
								
								
							}
				 }
			     
		}
		}
		return leaveCount;
	}
	
	
	public static JSONObject getPFCalualation(EmployeePFInfoDO pfDetail,PFGroupDO pfgroupDetail, Double ctc,Double noOfWorkingday, int salaryDays)throws JSONException {
		JSONObject result = new JSONObject();
		Double grossPay = ctc/12;
		Double monthlypay=0D;
		Double ytd=0D;
		Double pf = 0.00;
		if(pfDetail.getType() != null && pfDetail.getType().equalsIgnoreCase("v")){
			if(pfgroupDetail != null && pfgroupDetail.getPfGroupEarning() != null && pfgroupDetail.getPfGroupEarning().size() >0){
				for (PFGroupEarningsDO pfgrp: pfgroupDetail.getPfGroupEarning() ) {
					if(pfgrp.getStatus() == 'a' && pfgrp.getAllowanceSettings() != null && pfgrp.getAllowanceSettings().getStatus() == 'a' ){
						if(pfgrp.getAllowanceSettings().getEarningType().getName() != null && pfgrp.getAllowanceSettings().getEarningType().getName().equalsIgnoreCase("Basic")){
							if(pfgrp.getAllowanceSettings().getBasicgrosspay() != null && pfgrp.getAllowanceSettings().getBasicgrosspay().equalsIgnoreCase("G")){
								if(pfgrp.getAllowanceSettings().getPercentageamount() != null){
									basicPay = (grossPay * Double.parseDouble(pfgrp.getAllowanceSettings().getPercentageamount()))/100;
									ytd += basicPay * 12;
								}
							}else{
								if(pfgrp.getAllowanceSettings().getType().equalsIgnoreCase("A")){
									ytd += adjustment * 12;
								}
							}
						}else{
							
							if(pfgrp.getAllowanceSettings().getEarningType().getName().equalsIgnoreCase(CommonConstants.DEARNESSALLOWEANCE)){
								ytd += monthlyDA*12 ;
							}else if(pfgrp.getAllowanceSettings().getEarningType().getName().equalsIgnoreCase(CommonConstants.ADD_DA) || pfgrp.getAllowanceSettings().getEarningType().getName().equalsIgnoreCase(CommonConstants.ADDDEARNESSALLOWEANCE)){
								ytd += monthlyAddDA*12 ;
							}else{
								if(pfgrp.getAllowanceSettings().getType() != null && pfgrp.getAllowanceSettings().getType().equalsIgnoreCase("P")){
									if(pfgrp.getAllowanceSettings().getBasicgrosspay() != null && pfgrp.getAllowanceSettings().getBasicgrosspay().equalsIgnoreCase("B")){
										if(pfgrp.getAllowanceSettings().getPercentageamount() != null){
											monthlypay = (basicPay * Long.parseLong(pfgrp.getAllowanceSettings().getPercentageamount()))/100;
											ytd += monthlypay*12;
										}
									}else if(pfgrp.getAllowanceSettings().getBasicgrosspay() != null && pfgrp.getAllowanceSettings().getBasicgrosspay().equalsIgnoreCase("G")){
										if(pfgrp.getAllowanceSettings().getPercentageamount() != null){
											monthlypay = (grossPay * Double.parseDouble(pfgrp.getAllowanceSettings().getPercentageamount()))/100;
											ytd += monthlypay*12;
										}
									}
								}else if(pfgrp.getAllowanceSettings().getType() != null && pfgrp.getAllowanceSettings().getType().equalsIgnoreCase("F")){
									if(pfgrp.getAllowanceSettings().getFixedamount() != null){
										Double fixedAmount =  Double.parseDouble(pfgrp.getAllowanceSettings().getFixedamount());
										monthlypay = fixedAmount/12;
										if(salaryDays > 0 && noOfWorkingday > 0 ){
											monthlypay = (monthlypay/salaryDays)*noOfWorkingday;
										}
										ytd += monthlypay*12;
										
									}
								}
							}
						}
					}
				}
			}
			
			if(ytd > 0.0){
				Double monthSalary = (double) Math.round(ytd/12);
				if(pfgroupDetail.getMaxAmount() != null && pfgroupDetail.getMaxAmount() > 0){
					if(monthSalary > pfgroupDetail.getMaxAmount()){
						monthSalary = pfgroupDetail.getMaxAmount();
					}
				}
				List<PFDO> pfSettingList = pfService.retrieveActive();
				if(pfSettingList.get(0).getEmployeePercentage() != null && pfSettingList.get(0).getEmployeePercentage() > 0){
					pf =  (monthSalary*pfSettingList.get(0).getEmployeePercentage()) /100;
				}
				
				
				//System.out.println("monthlypay"+monthlypay);
				/*if(pfSettingList != null && pfSettingList.size() > 0){
					for(PFDO pfSetting: pfSettingList){
						if(pfSetting.getSalaryFrom() != null && pfSetting.getSalaryTo() != null){
						//	System.out.println("pfSetting.getSalaryFrom()"+pfSetting.getSalaryFrom()+pfSetting.getSalaryTo());
							//.out.println("pfSetting.getPercentage()"+pfSetting.getPercentage());
							if(ytd > pfSetting.getSalaryFrom() && ytd < pfSetting.getSalaryTo()){
								pf =  (ytd*pfSetting.getPercentage()) /100;
							}
							
							if(ytd > pfSetting.getSalaryFrom()){
								if(ytd < pfSetting.getSalaryTo()){
									pf =  (ytd*pfSetting.getPercentage()) /100;
								}else{
									pf =  (ytd*pfSetting.getPercentage()) /100;
								}
								
							}
						}
					}
				}*/
				/*Double diffrenceAmount  = 0.00 ;
				Double additional = 1.00;
				if(pfSettingList != null && pfSettingList.size() > 0){
					for(PFDO pfSlab: pfSettingList){
						diffrenceAmount  = 0.00;
						diffrenceAmount = Double.parseDouble(pfSlab.getSalaryTo().toString()) -  Double.parseDouble((pfSlab.getSalaryFrom()).toString()) ;
						diffrenceAmount += additional;
						if(diffrenceAmount < ytd){
							pf += (diffrenceAmount*pfSlab.getPercentage()) /100;
							ytd = ytd - diffrenceAmount;
						}else{
							pf += (ytd * pfSlab.getPercentage()) /100;
							break; 
						}
					}
				}*/
				
				
				
			}
		}
		
		if(pfDetail.getType() != null && pfDetail.getType().equalsIgnoreCase("f")){
			if(pfDetail.getAmount() != null){
				pf = (double) (pfDetail.getAmount());
			}
		}
		deductionTotal += Math.round( pf);
		//result.put(CommonConstants.MONTHLY, String.valueOf(CommonUtil.convertfloatValueWithTwoDecimalPoints(pf/12)));
		result.put(CommonConstants.MONTHLY, String.valueOf(Math.round(pf)));
		result.put(CommonConstants.YTD, String.valueOf(pf*12));
		result.put(CommonConstants.NAME, String.valueOf("Provident Fund"));
		result.put(CommonConstants.DEDUCTIONID, String.valueOf(pfgroupDetail.getPfGroupId()));
		return result;
	}
	
	
	public static JSONObject getESICalualation(EmployeeESIInfoDO empESI,ESIGroupDO esigroupDetail, Double ctc, Double noOfWorkingday, int salaryDays)throws JSONException {
		JSONObject result = new JSONObject();
		Double grossPay = ctc/12;
		//Double basicPay = basic;
		Double monthlypay=0D;
		Double ytd=0D;
		Double esi = 0.0;
		
		if(empESI.getType() != null && empESI.getType().equalsIgnoreCase("v")){
			
			if(esigroupDetail != null && esigroupDetail.getEsiGroupEarnings() != null && esigroupDetail.getEsiGroupEarnings().size() >0){
				for (ESIGroupEarningsDO esigrp: esigroupDetail.getEsiGroupEarnings() ) {
					if(esigrp.getStatus() == 'a' && esigrp.getAllowanceSettings() != null && esigrp.getAllowanceSettings().getStatus() == 'a' ){
							if(esigrp.getAllowanceSettings().getEarningType().getName() != null && esigrp.getAllowanceSettings().getEarningType().getName().equalsIgnoreCase("Basic")){
								if(esigrp.getAllowanceSettings().getBasicgrosspay() != null && esigrp.getAllowanceSettings().getBasicgrosspay().equalsIgnoreCase("G")){
									if(esigrp.getAllowanceSettings().getPercentageamount() != null){
										basicPay = (grossPay * Double.parseDouble(esigrp.getAllowanceSettings().getPercentageamount()))/100;
										ytd += basicPay * 12;
									}
								}else{
									if(esigrp.getAllowanceSettings().getType().equalsIgnoreCase("A")){
										ytd += adjustment * 12;
									}
								}
							}else{
								if(esigrp.getAllowanceSettings().getEarningType().getName().equalsIgnoreCase(CommonConstants.DEARNESSALLOWEANCE)){
									ytd += monthlyDA*12 ;
								}else if(esigrp.getAllowanceSettings().getEarningType().getName().equalsIgnoreCase(CommonConstants.ADD_DA) || esigrp.getAllowanceSettings().getEarningType().getName().equalsIgnoreCase(CommonConstants.ADDDEARNESSALLOWEANCE)){
									ytd += monthlyAddDA*12 ;
								}else{
									if(esigrp.getAllowanceSettings().getType() != null && esigrp.getAllowanceSettings().getType().equalsIgnoreCase("P")){
										if(esigrp.getAllowanceSettings().getBasicgrosspay() != null && esigrp.getAllowanceSettings().getBasicgrosspay().equalsIgnoreCase("B")){
											if(esigrp.getAllowanceSettings().getPercentageamount() != null){
												monthlypay = (basicPay * Long.parseLong(esigrp.getAllowanceSettings().getPercentageamount()))/100;
												ytd += (monthlypay*12);
											}
										}else if(esigrp.getAllowanceSettings().getBasicgrosspay() != null && esigrp.getAllowanceSettings().getBasicgrosspay().equalsIgnoreCase("G")){
											if(esigrp.getAllowanceSettings().getPercentageamount() != null){
												monthlypay = (grossPay * Double.parseDouble(esigrp.getAllowanceSettings().getPercentageamount()))/100;
												ytd += monthlypay*12;
											}
										}
									}else if(esigrp.getAllowanceSettings().getType() != null && esigrp.getAllowanceSettings().getType().equalsIgnoreCase("F")){
										if(esigrp.getAllowanceSettings().getFixedamount() != null){
											Double fixedAmount =  Double.parseDouble(esigrp.getAllowanceSettings().getFixedamount());
											monthlypay = fixedAmount/12;
											if(salaryDays > 0 && noOfWorkingday > 0 ){
												monthlypay = (monthlypay/salaryDays)*noOfWorkingday;
											}
											ytd += monthlypay*12;
											
										}
									}
								}
							}
					}
				}
				
			}
			//System.out.println("ytd"+ytd);
		
			if(ytd > 0.0){
				List<ESIDO> esiSettingList = esiService.retrieveActive();
				Double monthlySalary = (double) Math.round(ytd/12);
				if(esigroupDetail.getAmount() != null && esigroupDetail.getAmount() > 0){
					if(monthlySalary > esigroupDetail.getAmount()){
						monthlySalary = esigroupDetail.getAmount();
					}
				}
				
				//System.out.println("monthlySalary"+monthlySalary);
				
				if(esiSettingList.get(0).getEmployeePercentage() != null && esiSettingList.get(0).getEmployeePercentage() > 0){
				//	System.out.println("esiSettingList.get(0).getEmployeePercentage()"+esiSettingList.get(0).getEmployeePercentage());
					esi = (monthlySalary*esiSettingList.get(0).getEmployeePercentage()) /100;
				}
				
				
				/*Double diffrenceAmount  = 0.00 ;
				Double additional = 1.00;
				if(esiSettingList != null && esiSettingList.size() > 0){
					for(ESIDO esiSlab: esiSettingList){
						diffrenceAmount  = 0.00;
						diffrenceAmount = Double.parseDouble(esiSlab.getSalaryTo().toString()) -  Double.parseDouble((esiSlab.getSalaryFrom()).toString()) ;
						diffrenceAmount += additional;
						if(diffrenceAmount < ytd){
							esi += (diffrenceAmount*esiSlab.getPercentage()) /100;
							ytd = ytd - diffrenceAmount;
						}else{
							esi += (ytd * esiSlab.getPercentage()) /100;
							break; 
						}
					}
				}*/
			}
		}
		
		if(empESI.getType() != null && empESI.getType().equalsIgnoreCase("f")){
			//System.out.println("esi fixed"+empESI.getAmount());
			if(empESI.getAmount() != null){
				esi = (double) (empESI.getAmount());
			}
			
		}
		//System.out.println("esi"+esi);
		deductionTotal += Math.round(esi);
		
		result.put(CommonConstants.MONTHLY, String.valueOf(Math.round(esi)));
		//result.put(CommonConstants.MONTHLY, String.valueOf(CommonUtil.convertfloatValueWithTwoDecimalPoints(esi/12)));
		result.put(CommonConstants.YTD, String.valueOf(esi*12));
		result.put(CommonConstants.NAME, String.valueOf("Employees' State Insurance"));
		result.put(CommonConstants.DEDUCTIONID, String.valueOf(esigroupDetail.getEsiGroupId()));
		return result;
	}
	
	public static JSONObject getCompensationTaxDetailInfoObject(Long taxId,TaxSettingsDO taxSettings, Double ctc)throws JSONException {
		JSONObject result = new JSONObject();
		Double grossPay = ctc/12;
	//	Double basicPay = basic;
		Double monthlypay=0D;
		Double ytd=0D;
		if(taxSettings.getType() != null && taxSettings.getType().equalsIgnoreCase("P")){
			if(taxSettings.getBasicgrosspay() != null && taxSettings.getBasicgrosspay().equalsIgnoreCase("B")){
				if(taxSettings.getPercentageamount() != null){
					monthlypay = (basicPay * Double.parseDouble(taxSettings.getPercentageamount()))/100;
					ytd = monthlypay*12;
				}
			}else if(taxSettings.getBasicgrosspay() != null && taxSettings.getBasicgrosspay().equalsIgnoreCase("G")){
				if(taxSettings.getPercentageamount() != null){
					monthlypay = (grossPay * Double.parseDouble(taxSettings.getPercentageamount()))/100;
					ytd = monthlypay*12;
				}
			}
		}else if(taxSettings.getType() != null && taxSettings.getType().equalsIgnoreCase("F")){
			if(taxSettings.getFixedamount() != null){
				ytd = Double.parseDouble(taxSettings.getFixedamount());
				monthlypay = ytd/12;
			}
		}
		taxTotal += Math.round(monthlypay);

		result.put(CommonConstants.MONTHLY, String.valueOf(Math.round(monthlypay)));
	//	result.put(CommonConstants.MONTHLY, String.valueOf(CommonUtil.convertfloatValueWithTwoDecimalPoints(monthlypay)));
		result.put(CommonConstants.YTD, String.valueOf(ytd));
		result.put(CommonConstants.NAME, String.valueOf(taxSettings.getDisplayname()));
		result.put(CommonConstants.TAXID, String.valueOf(taxId));
		
		
		return result;
	}

	public static JSONObject getPTObject(EmployeeDO emp, Double net)throws JSONException {
		JSONObject result = new JSONObject();
		Double monthlypay=0D;
		long totalEarning = (new Double(net*12)).longValue() ;
		if(emp.getUnitOrBranch() != null && emp.getUnitOrBranch().getState().getStateId() != null){
			List<PtDO> ptDOList = ptService.retrieveByCtcAndState(totalEarning, emp.getUnitOrBranch().getState().getStateId());
			if(ptDOList != null && ptDOList.size() > 0){
				monthlypay = (double) (ptDOList.get(0).getPtamount()/12) ;
				taxTotal += Math.round(monthlypay);
				result.put(CommonConstants.MONTHLY, String.valueOf(Math.round(monthlypay)));
				result.put(CommonConstants.YTD, String.valueOf(ptDOList.get(0).getPtamount()));
				result.put(CommonConstants.NAME, String.valueOf("Professional Tax"));
				result.put(CommonConstants.TAXID, String.valueOf(ptDOList.get(0).getPtId()));
			}else{
				result.put(CommonConstants.MONTHLY, String.valueOf(0));
				result.put(CommonConstants.YTD, String.valueOf(0));
				result.put(CommonConstants.NAME, String.valueOf("Professional Tax"));
				//result.put(CommonConstants.TAXID, String.valueOf(taxId));
			}
		}else{
			result.put(CommonConstants.MONTHLY, String.valueOf(0));
			result.put(CommonConstants.YTD, String.valueOf(0));
			result.put(CommonConstants.NAME, String.valueOf("Professional Tax"));
			//result.put(CommonConstants.TAXID, String.valueOf(taxId));
		}
		
		return result;
	}
	
	public static JSONObject getTDSObject(EmployeeDO emp, Double ctc, String month)throws JSONException {
		JSONObject result = new JSONObject();
		Date currentDate = new Date();
		taxbleSalary = 0.0;
		finalTDS = 0.0;
		 int monthdiff = 0;
		if(month != null && !month.isEmpty()){
			Calendar c = Calendar.getInstance();
			c.setTime(CommonUtil.convertMonthToSqlDate(month));
			Calendar endDate = Calendar.getInstance(); 
			int  year = c.get(Calendar.YEAR);
			 int  monthNumber= c.get(Calendar.MONTH);
			endDate.set(year,monthNumber,c.getActualMaximum(Calendar.DAY_OF_MONTH)); 
			List < ItSlabEffectiveDO > itSlabEffectiveList = itSlabEffectiveService.retrieveByDate(c.getTime(),endDate.getTime());
			if( itSlabEffectiveList != null && itSlabEffectiveList.size() >0){
				if(itSlabEffectiveList.get(0).getEffectivefromDate() != null && itSlabEffectiveList.get(0).getEffectiveToDate() != null){
					Calendar calFrom = Calendar.getInstance();
					calFrom.setTime(itSlabEffectiveList.get(0).getEffectivefromDate());
					int fromyear = calFrom.get(Calendar.YEAR);
					//String frommonth = calFrom.getDisplayName(tes, Calendar.LONG, Locale.getDefault());
					String frommonth  = new SimpleDateFormat("MMMM").format(calFrom.getTime());
					int fromday = calFrom.get(Calendar.DAY_OF_MONTH);
					
					Calendar calTo = Calendar.getInstance();
					calTo.setTime(itSlabEffectiveList.get(0).getEffectiveToDate());
					int toyear = calTo.get(Calendar.YEAR);
					//String  tomonth = calTo.getDisplayName(calTo.get(Calendar.MONTH), Calendar.LONG, Locale.getDefault());
					String  tomonth = new SimpleDateFormat("MMMM").format(calTo.getTime());
					int today = calTo.get(Calendar.DAY_OF_MONTH);
					
					
					
					LocalDate fromthis = LocalDate.of(fromyear,Month.valueOf(frommonth.toUpperCase()),fromday);
					LocalDate tothis = LocalDate.of(toyear,Month.valueOf(tomonth.toUpperCase()),today);
							
				        
				     Period diff = Period.between(fromthis, tothis);
				       // int years = diff.getYears();
				         monthdiff = diff.getMonths();
				}
				List<ItSectionDO> itSectionList = itSectionService.retrieveByCurrentDate(currentDate);
				if(itSectionList != null && itSectionList.size() > 0){
					for(ItSectionDO section :itSectionList){
						List<ItSavingsDO> itSavingsList = itSavingsService.retrieveByEmpIdSectionIdAndDate(currentDate, emp.getEmpId(), section.getItSectionId());
						if(itSavingsList != null && itSavingsList.size() > 0){
							double itSavingAmount = 0.0 ;
							for (ItSavingsDO itSaving :itSavingsList) {
								itSavingAmount += itSaving.getAmount();
							}
							if(section.getMaxLimit() < itSavingAmount){
								 taxbleSalary +=  (itSavingAmount - section.getMaxLimit());
							}else{
								taxbleSalary += itSavingAmount;
							}
						}/*else{
							Double tds = taxSlabCal(ctc,emp);
							result.put(CommonConstants.MONTHLY, String.valueOf(tds/12));
							result.put(CommonConstants.YTD, String.valueOf(tds));
							result.put(CommonConstants.NAME, String.valueOf("TDS"));
							result.put(CommonConstants.TAXID, String.valueOf(taxId));
						}*/
					}
					taxbleSalary = (taxableIncome- ((deductionTotal + taxTotal)*12)) - taxbleSalary;
					Double tds = taxSlabCal(taxbleSalary,emp);
					finalTDS = tds;
					if(monthdiff > 0 ){
						taxTotal += tds/monthdiff;
						result.put(CommonConstants.MONTHLY, String.valueOf(Math.round(tds/monthdiff)));
					}else{
						taxTotal += tds/12;
						result.put(CommonConstants.MONTHLY, String.valueOf(Math.round(tds/12)));
					}
					
					
					
					//result.put(CommonConstants.MONTHLY, String.valueOf(CommonUtil.convertfloatValueWithTwoDecimalPoints(tds/12)));
					result.put(CommonConstants.YTD, String.valueOf(tds));
					result.put(CommonConstants.NAME, String.valueOf("TDS"));
					//result.put(CommonConstants.TAXID, String.valueOf(taxId));
				}else{
					Double tds = taxSlabCal((taxableIncome- ((deductionTotal + taxTotal)*12)),emp);
					finalTDS = tds;
					if(monthdiff > 0 ){
						taxTotal += tds/monthdiff;
						result.put(CommonConstants.MONTHLY, String.valueOf(Math.round(tds/monthdiff)));
					}else{
						taxTotal += tds/12;
						result.put(CommonConstants.MONTHLY, String.valueOf(Math.round(tds/12)));
					}
				
					//result.put(CommonConstants.MONTHLY, String.valueOf(CommonUtil.convertfloatValueWithTwoDecimalPoints(tds/12)));
					result.put(CommonConstants.YTD, String.valueOf(tds));
					result.put(CommonConstants.NAME, String.valueOf("TDS"));
					//result.put(CommonConstants.TAXID, String.valueOf(taxId));
				}
			}
		
		}
		return result;
	}
	
	public static JSONObject getEducationCessObject()throws JSONException {
		JSONObject result = new JSONObject();
		if(finalTDS != 0.0){
			Date currentDate = new Date();
			List<EducationCessDO> educationCessList = educationCessService.retrieveByCurrentDate(currentDate);
	 		if(educationCessList != null && educationCessList.size() > 0){
	 			if(educationCessList.get(0).getPercentage() != null){
	 				Double educationcess = ( finalTDS* educationCessList.get(0).getPercentage())/100 ;
	 				taxTotal += educationcess/12;
	 				result.put(CommonConstants.MONTHLY, String.valueOf(Math.round(educationcess/12)));
	 				//result.put(CommonConstants.MONTHLY, String.valueOf(CommonUtil.convertfloatValueWithTwoDecimalPoints(educationcess/12)));
					result.put(CommonConstants.YTD, String.valueOf(educationcess));
					result.put(CommonConstants.NAME, String.valueOf("Education Cess "));
	 			}
	 		}else{
				result.put(CommonConstants.MONTHLY, String.valueOf(0));
				result.put(CommonConstants.YTD, String.valueOf(0));
				result.put(CommonConstants.NAME, String.valueOf("Education Cess "));
			}
		}else{
			result.put(CommonConstants.MONTHLY, String.valueOf(0));
			result.put(CommonConstants.YTD, String.valueOf(0));
			result.put(CommonConstants.NAME, String.valueOf("Education Cess "));
		}
		return result;
	}
	
	
	
	public static Double taxSlabCal(Double taxamount, EmployeeDO emp)throws JSONException {
		Date currentDate = new Date();
		Calendar today = Calendar.getInstance();
		int curYear = today.get(Calendar.YEAR);
		 SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
         Calendar dob = Calendar.getInstance();
         try {
			dob.setTime(sdf.parse(emp.getDateofbirth()));
			int dobYear = dob.get(Calendar.YEAR);
			long age =  (curYear - dobYear);
			int curMonth = today.get(Calendar.MONTH);
			int dobMonth = dob.get(Calendar.MONTH);
			if (dobMonth > curMonth) { 
				            age--;
	        } else if (dobMonth == curMonth) { // same month? check for day
	            int curDay = today.get(Calendar.DAY_OF_MONTH);
	            int dobDay = dob.get(Calendar.DAY_OF_MONTH);
	            if (dobDay > curDay) { // this year can't be counted!
	                age--;
	            }
	        }
			finalTax = 0.0 ;
			Long diffrenceAmount  = 0L ;
				List<TaxSlabsSettingsDO> taxSlabSettingList = taxSlabsSettingsService.retrieveByAgeDate(age,currentDate);
				if(taxSlabSettingList != null && taxSlabSettingList.size() > 0){
					//diffrenceAmount = taxbleSalary;
					for(TaxSlabsSettingsDO taxSlab: taxSlabSettingList){
						diffrenceAmount  = 0L;
						diffrenceAmount = taxSlab.getAmountto() - taxSlab.getAmountfrom();
						if(diffrenceAmount < taxamount){
							finalTax += (diffrenceAmount*taxSlab.getPercentageamount()) /100;
							taxamount = taxamount - diffrenceAmount;
						}else{
							finalTax += (taxamount * taxSlab.getPercentageamount()) /100;
							break; 
						}
					}
				}
         } catch (ParseException e) {
				e.printStackTrace();
			}
				return finalTax; 
	}
	
	public static JSONObject getLOPDetailsObject(List<EmployeeLopDO> lopList, Double ctc, int totalWorkingDays, Double lopLeaves)throws JSONException {
		JSONObject result = new JSONObject();
		Double grossPay = ctc/12;
		//Double basicPay = basic;
		leaveCount = lopLeaves;
		for(EmployeeLopDO lop : lopList){
			Date dateBeforeString = lop.getStartdate();
			if(lop.getEnddate() != null){
				Calendar end = Calendar.getInstance();
				 Date dateAfterString = lop.getEnddate();
				  end.setTime(dateAfterString);
				  Calendar start = Calendar.getInstance();
				  start.setTime(dateBeforeString);
				 for (Date date = start.getTime(); start.before(end) || start.equals(end); start.add(Calendar.DATE, 1), date = start.getTime()) {
					Calendar weekend = Calendar.getInstance();
					weekend.setTime(date);
					if (/*weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && */
						weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){
						 //leaveCount +=1;
						if(lop.getType() != null && lop.getType().equalsIgnoreCase("h")){
							leaveCount +=0.5;
						}else{
							leaveCount +=1;
						}
					}
				 }
			}else{
				if(lop.getType() != null && lop.getType().equalsIgnoreCase("h")){
					leaveCount +=0.5;
				}else{
					leaveCount +=1;
				}
				
			}
		}
		 Double perdaySalary = (double) (grossPay/totalWorkingDays);
	    // Long lop  = (long) (perdaySalary * leaveCount);
		 
		 Double lop  =  (perdaySalary * leaveCount);
	    // deductionTotal += Math.round(lop);
	     
	     totalLop = Math.round(lop);
			//result.put(CommonConstants.MONTHLY, String.valueOf(Math.round(lop)));
	   //  result.put(CommonConstants.MONTHLY, String.valueOf(CommonUtil.convertfloatValueWithTwoDecimalPoints(lop)));
	    // result.put(CommonConstants.NAME, String.valueOf("Loss Of Pay"));
		return result;
	}
	
	
	public static JSONObject getAdvancePaymentObject(List<AdvancePaymentDetailDO> advancePaymentList, Double ctc, String month)throws JSONException {
		JSONObject result = new JSONObject();
		Double grossPay = ctc/12;
		//Double basicPay = basic;
		Double installment = 0.0;
		for(AdvancePaymentDetailDO advancePayment : advancePaymentList){
			
			
			if(advancePayment.getMonth() != null && advancePayment.getAmount() != null && Double.parseDouble(advancePayment.getAmount().toString()) >  0){
				installment += Double.parseDouble(advancePayment.getAmount());
				/*System.out.println("ddf"+Month.valueOf("MAY"));
				System.out.println("month1"+advancePayment.getMonth());
				System.out.println("month1"+month);
				LocalDate bday = LocalDate.of((int) Long.parseLong(advancePayment.getMonth().substring(advancePayment.getMonth().length() - 4)), CommonUtil.convertMonthToSqlDate(advancePayment.getMonth()).getMonth(), (int) Long.parseLong("01"));
		        LocalDate today = LocalDate.of((int) Long.parseLong(month.substring(month.length() - 4)), CommonUtil.convertMonthToSqlDate(month).getMonth(), (int) Long.parseLong("01"));
			        
			     Period age = Period.between(bday, today);
			       // int years = age.getYears();
			        int months = age.getMonths();
			        if((months+1) >0 && (months+1) <= Integer.parseInt(advancePayment.getNoOfInstallments().toString())){
			        	installment += Double.parseDouble(advancePayment.getInstallmentAmount().toString());
			        }
			        System.out.println("number of months: " + (months+1) );
			        System.out.println("oooo"+Integer.parseInt(advancePayment.getNoOfInstallments().toString()));*/
				
			}
			
			
			
		}
		
		if(installment > 0){
			 deductionTotal += Math.round(installment);
					result.put(CommonConstants.MONTHLY, String.valueOf(Math.round(installment)));
			//result.put(CommonConstants.MONTHLY, String.valueOf(CommonUtil.convertfloatValueWithTwoDecimalPoints(installment)));
		    result.put(CommonConstants.NAME, String.valueOf("Advance Deduction"));
		}
		
		return result;
	}
	
	
	
	public static JSONObject getLoanPaymentObject(GeneralDeductionDetailDO loanPayment, Double ctc, String month)throws JSONException {
		JSONObject result = new JSONObject();
		//Double basicPay = basic;
		Double installment = 0.0;
	//	for(GeneralDeductionDetailDO loanPayment : loanPaymentList){
			if(loanPayment.getMonth() != null && loanPayment.getAmount() != null && Double.parseDouble(loanPayment.getAmount().toString()) >  0){
				installment += Double.parseDouble(loanPayment.getAmount());
			}
		//}
		if(installment > 0){
			 deductionTotal += Math.round(installment);
				result.put(CommonConstants.MONTHLY, String.valueOf(Math.round(installment)));
			//result.put(CommonConstants.MONTHLY, String.valueOf(CommonUtil.convertfloatValueWithTwoDecimalPoints(installment)));
		    result.put(CommonConstants.NAME, String.valueOf(loanPayment.getLoanPayment().getDeductionSubType().getName()));
		}
		return result;
	}
	
	
	/*public static JSONObject getInsurancePaymentObject(List<InsurancePaymentDetailDO> insurancePaymentList, Double ctc, String month)throws JSONException {
		JSONObject result = new JSONObject();
		//Double basicPay = basic;
		Double installment = 0.0;
		for(InsurancePaymentDetailDO insurancePayment : insurancePaymentList){
			if(insurancePayment.getMonth() != null && insurancePayment.getAmount() != null && Double.parseDouble(insurancePayment.getAmount().toString()) >  0){
				installment += Double.parseDouble(insurancePayment.getAmount());
			}
		}
		if(installment > 0){
			 deductionTotal += Math.round(installment);
				result.put(CommonConstants.MONTHLY, String.valueOf(Math.round(installment)));
			//result.put(CommonConstants.MONTHLY, String.valueOf(CommonUtil.convertfloatValueWithTwoDecimalPoints(installment)));
		}
		return result;
	}*/
	
	public static JSONObject getAdustmentDetails(Long earningId, AllowanceSettingDO allowance, Double ctc)throws JSONException {
		JSONObject result = new JSONObject();
		Double grossPay = ctc/12;
		adjustment = ((grossPay-totalLop) - (earingTotal - totalAllowance));
		//ajustment = (grossPay-totalLop) - (test ); 
		earingTotal += Math.round(adjustment);
		if(allowance.getTaxable().equalsIgnoreCase("Y")){
			taxableIncome += Math.round(adjustment*12);
		}
		//result.put(CommonConstants.MONTHLY, String.valueOf(CommonUtil.convertfloatValueWithTwoDecimalPoints(ajustment)));
				
		result.put(CommonConstants.MONTHLY, String.valueOf(Math.round(adjustment)));
		result.put(CommonConstants.YTD, String.valueOf(adjustment*12));
		result.put(CommonConstants.NAME, String.valueOf(allowance.getDisplayname()));
		result.put(CommonConstants.EARNINGID, String.valueOf(earningId));
		
		return result;
	}
	
	public static JSONObject getWorkingDayAllowanceCalculation(WorkingDayAllowanceDO workingDayAllowance, double noOfDaysWorked,Long noOfDaysInMonth, double pcount)throws JSONException {
		JSONObject result = new JSONObject();
		Double allowance = 0.0;
		if(workingDayAllowance.getWorkingDayAllowanceId() != null){
			List<WorkingDayLeavetypeDO>  workingdayLeaveList = workingDayLeaveTypeService.retrieveByWorkingDayId(workingDayAllowance.getWorkingDayAllowanceId());
			if(workingdayLeaveList != null && workingdayLeaveList.size() > 0){
				if(workingDayAllowance.getCost() != null){
					allowance = noOfDaysWorked * workingDayAllowance.getCost();
				}
			}else{
				if(workingDayAllowance.getCost() != null){
					allowance = pcount * workingDayAllowance.getCost();
				}
			}
		}
		//if(workingDayAllowance.getType() != null){
			
			/*if(workingDayAllowance.getCost() != null && workingDayAllowance.getType().equalsIgnoreCase("M")){
				allowance = workingDayAllowance.getCost();
			}
			if(workingDayAllowance.getCost() != null && workingDayAllowance.getType().equalsIgnoreCase("Y")){
				allowance = workingDayAllowance.getCost() / 12;
			}
			if(workingDayAllowance.getCost() != null && workingDayAllowance.getType().equalsIgnoreCase("D")){
				allowance = workingDayAllowance.getCost() * noOfDaysWorked;
			}*/
			/*if(workingDayAllowance.getType().equalsIgnoreCase("F")){
				if(workingDayAllowance.getCostEffective() != null && workingDayAllowance.getCostEffective().equalsIgnoreCase("M")){
					allowance = workingDayAllowance.getCost();
				}
				if(workingDayAllowance.getCostEffective() != null && workingDayAllowance.getCostEffective().equalsIgnoreCase("Y")){
					allowance = workingDayAllowance.getCost() / 12;
				}
				if(workingDayAllowance.getCostEffective() != null && workingDayAllowance.getCostEffective().equalsIgnoreCase("D")){
					allowance = workingDayAllowance.getCost() * noOfDaysWorked;
				}
			}else{
				if(workingDayAllowance.getType().equalsIgnoreCase("V")){
					if(workingDayAllowance.getCalculationType() != null){
						if(workingDayAllowance.getCalculationType().equalsIgnoreCase("M")){
							if(workingDayAllowance.getCostEffective().equalsIgnoreCase("M")){
								allowance = noOfDaysWorked * workingDayAllowance.getCost();
							}
							if(workingDayAllowance.getCostEffective().equalsIgnoreCase("Y")){
								allowance = noOfDaysWorked * workingDayAllowance.getCost()/12;
							}
							if(workingDayAllowance.getCostEffective().equalsIgnoreCase("D")){
								allowance = noOfDaysWorked * workingDayAllowance.getCost();
							}
						}
						if(workingDayAllowance.getCalculationType().equalsIgnoreCase("GE")){
							if(workingDayAllowance.getCostEffective().equalsIgnoreCase("M")){
								allowance = noOfDaysWorked * workingDayAllowance.getCost();
							}
							if(workingDayAllowance.getCostEffective().equalsIgnoreCase("Y")){
								allowance = noOfDaysWorked * workingDayAllowance.getCost()/12;
							}
							if(workingDayAllowance.getCostEffective().equalsIgnoreCase("D")){
								allowance = noOfDaysWorked * workingDayAllowance.getCost();
							}
						}
					}
				}
			}*/
		//}
		totalAllowance += Math.round(allowance);
		earingTotal += Math.round(allowance);
		//result.put(CommonConstants.MONTHLY, String.valueOf(CommonUtil.convertfloatValueWithTwoDecimalPoints(allowance)));
		result.put(CommonConstants.MONTHLY, String.valueOf(Math.round(allowance)));
		result.put(CommonConstants.YTD, String.valueOf(allowance*12));
		result.put(CommonConstants.NAME, String.valueOf(workingDayAllowance.getName()));
		result.put(CommonConstants.EARNINGID, String.valueOf(workingDayAllowance.getWorkingDayAllowanceId()));
		
		return result;
	}
	
	
	
	public static JSONObject getEmployeeExpenseDetail(List <EmployeeExpenseDO> empExpenseList)throws JSONException {
		JSONObject result = new JSONObject();
		Long expense = 0L;
		
			for(EmployeeExpenseDO expenseDetail: empExpenseList){
				if(expenseDetail.getAmount() != null){
					expense += expenseDetail.getAmount();
				}
			}
			
			
		earingTotal += Math.round(expense);
		totalAllowance += Math.round(expense);
		result.put(CommonConstants.MONTHLY, String.valueOf(Math.round(expense)));
		result.put(CommonConstants.YTD, String.valueOf(expense*12));
		result.put(CommonConstants.NAME, String.valueOf("Claims"));
		result.put(CommonConstants.EXPENSE, String.valueOf(empExpenseList.get(0)));
		
		return result;
	}
	
	public static JSONObject getAttendanceAllowanceCalculation(AttendanceAllowanceDO attendanceDayAllowance, Double noOfDaysWorked,Long noOfDaysInMonth)throws JSONException {
		JSONObject result = new JSONObject();
		Double allowance = 0.0;
			
			if(attendanceDayAllowance.getAmount() != null){
				allowance = attendanceDayAllowance.getAmount();
			}
		earingTotal += Math.round(allowance);
		totalAllowance += Math.round(allowance);
		result.put(CommonConstants.MONTHLY, String.valueOf(Math.round(allowance)));
		result.put(CommonConstants.YTD, String.valueOf(allowance*12));
		result.put(CommonConstants.NAME, String.valueOf(attendanceDayAllowance.getName()));
		result.put(CommonConstants.EARNINGID, String.valueOf(attendanceDayAllowance.getAttendanceAllowanceId()));
		
		return result;
	}
	
	public static JSONObject getDearnessAlloweanceDetails(String month, AllowanceSettingDO allwenceDeatils, Long salaryDays, Double noofDaysPresent, Long unitId)throws JSONException {
		JSONObject result = new JSONObject();
		Double da = 0.0;
		Long year = Long.parseLong(month.substring(month.length() - 4));
		String selectedMonth = month.substring(0,3);
		List<DASettingsDO> daList = daSettingsService.retrieveByMonthAndYearUnit(selectedMonth,year,unitId);
 		if(daList != null && daList.size() > 0){
 			if(daList.get(0).getDApoints() != null && daList.get(0).getDAdeductionpoints() != null && daList.get(0).getDArate() != null){
 				da =  ((double)daList.get(0).getDApoints() - daList.get(0).getDAdeductionpoints()) * daList.get(0).getDArate();
 			}else{
 				if(daList.get(0).getDAdeductionpoints() == null)
 				da =  ((double)daList.get(0).getDApoints()) * daList.get(0).getDArate();
 			}
 			
 			/*String monthYear = 01+" "+selectedMonth+ " "+ year;
 			System.out.println("monthYear"+monthYear);
				List<CompanyCalenderDO> companyCalenderList = companyCalenderService.retrieveByMonth(monthYear);
 			System.out.println("companyCalenderList"+companyCalenderList.size());
 			
			
			Calendar c=Calendar.getInstance();
	        c.setTime(joiningDate);
	        int days = c.getActualMaximum(Calendar.DAY_OF_MONTH);
	        System.out.println("days"+days);
 			*/
 			//noofDaysPresent = noofDaysPresent-leaveCount;
 			
 			totalAllowance += Math.round((da/salaryDays)*noofDaysPresent);
 			earingTotal += Math.round((da/salaryDays)*noofDaysPresent);
 			monthlyDA += Math.round((da/salaryDays)*noofDaysPresent);
 			
 			result.put(CommonConstants.MONTHLY, String.valueOf(Math.round((da/salaryDays)*noofDaysPresent)));
 			result.put(CommonConstants.YTD, String.valueOf(((da/salaryDays)*noofDaysPresent)*12));
 			result.put(CommonConstants.NAME, String.valueOf(allwenceDeatils.getDisplayname()));
 			result.put(CommonConstants.DEARNESSALLOWEANCEID, String.valueOf(daList.get(0).getDearnessallowanceId()));
 		}else{
 			result.put(CommonConstants.MONTHLY,  String.valueOf("0"));
 			result.put(CommonConstants.YTD,  String.valueOf("0"));
 			result.put(CommonConstants.NAME,  String.valueOf(allwenceDeatils.getDisplayname()));
 			result.put(CommonConstants.DEARNESSALLOWEANCEID, "");
 		}
		return result;
	}
	
	
	public static JSONObject getADDDearnessAlloweanceDetails(String month, AllowanceSettingDO allwenceDeatils, Long unitId,String empId, Long branchId)throws JSONException {
		JSONObject result = new JSONObject();
		Double da = 0.0;
		Double finalDa = 0.0;
		Double salaryDay = 0.0;
		Double attendanceCount = 0.0; 
		List<CompanyCalenderDO> companyCalenderList = companyCalenderService.retrieveByMonth(month,branchId);
		if(companyCalenderList != null && companyCalenderList.size() > 0 && companyCalenderList.get(0).getNoOfSalaryDays() != null){
			salaryDay = Double.parseDouble(companyCalenderList.get(0).getNoOfSalaryDays());
			Calendar c = Calendar.getInstance();
			c.setTime(CommonUtil.convertMonthToSqlDate(month));
			c.add(Calendar.MONTH, -1);
			Calendar endDate = Calendar.getInstance(); 
			int  year1 = c.get(Calendar.YEAR);
			int  monthNumber= c.get(Calendar.MONTH);
			endDate.set(year1,monthNumber,c.getActualMaximum(Calendar.DAY_OF_MONTH)); 
			
			List<AttendanceDO> attendenceList = attendanceService.retrieveByEmpId_Date(empId,c.getTime(),endDate.getTime());
			if(attendenceList != null && attendenceList.size() > 0){
				
				for(AttendanceDO attndeance : attendenceList){
					if(attndeance.getAttendanceType().equalsIgnoreCase("p")){
						attendanceCount += 1;
					}
					/*if(attndeance.getAttendanceType().equalsIgnoreCase("cf")) {
						attendanceCount+=1;
					}
					
					if(attndeance.getAttendanceType().equalsIgnoreCase("lpf")) {
						attendanceCount+=1;
					}*/
					if(attndeance.getAttendanceType().equalsIgnoreCase("ch")) {
						attendanceCount+=0.5;
					}
					if(attndeance.getAttendanceType().equalsIgnoreCase("lph")){
						attendanceCount+=0.5;
					}
					if(attndeance.getAttendanceType().equalsIgnoreCase("apc")){
						attendanceCount+=0.5;
					}
					if(attndeance.getAttendanceType().equalsIgnoreCase("alc")){
						attendanceCount+=1;
					}
					if(attndeance.getAttendanceType().equalsIgnoreCase("h")){
						attendanceCount+=0.5;
					}
					/*if(attndeance.getAttendanceType().equalsIgnoreCase("hlhc")){
						attendanceCount+=1;
					}*/
				}
				List<HolidaysDO> hoildayList = null;
				String month1 = new SimpleDateFormat("MMM").format(c.getTime());//month.substring(0, 3)new SimpleDateFormat("MMM").format(c.getTime())
				hoildayList = holidaysService.retrieveByDate(Integer.toString(year1), month1);
				if(hoildayList != null && hoildayList.size() > 0) attendanceCount+=hoildayList.size();
			}
		}
		
		Long year = Long.parseLong(month.substring(month.length() - 4));
		String selectedMonth = month.substring(0,3);
		List<DASettingsDO> daList = daSettingsService.retrieveByMonthAndYearUnit(selectedMonth,year, unitId);
 		if(daList != null && daList.size() > 0 && daList.get(0).getAddDA() != null){
 			da =  (double) (daList.get(0).getAddDA() * daList.get(0).getDArate());
 			finalDa = (da/salaryDay)*attendanceCount;
 			
 			totalAllowance += Math.round( finalDa);
 			earingTotal += Math.round( finalDa);
 			monthlyAddDA += Math.round( finalDa);
 			result.put(CommonConstants.MONTHLY, String.valueOf(Math.round(finalDa)));
 			//result.put(CommonConstants.MONTHLY, String.valueOf(CommonUtil.convertfloatValueWithTwoDecimalPoints(da)));
 			result.put(CommonConstants.YTD, String.valueOf(finalDa*12));
 			result.put(CommonConstants.NAME, String.valueOf(allwenceDeatils.getDisplayname()));
 			result.put(CommonConstants.DEARNESSALLOWEANCEID, String.valueOf(daList.get(0).getDearnessallowanceId()));
 		}else{
 			result.put(CommonConstants.MONTHLY,  String.valueOf("0"));
 			result.put(CommonConstants.YTD,  String.valueOf("0"));
 			result.put(CommonConstants.NAME,  String.valueOf(allwenceDeatils.getDisplayname()));
 			result.put(CommonConstants.DEARNESSALLOWEANCEID, "");
 		}
		return result;
	}
	public static JSONObject getSubDADetails(String month, DeductionSettingDO deductionDetails, Long unitId, Long branchId, String empId)throws JSONException {
		JSONObject result = new JSONObject();
		Double da = 0.0;
		Double finalDa = 0.0;
		Double salaryDay = 0.0;
		int attendanceCount = 0;
		
		List<CompanyCalenderDO> companyCalenderList = companyCalenderService.retrieveByMonth(month,branchId);
		if(companyCalenderList != null && companyCalenderList.size() > 0){
			salaryDay = Double.parseDouble(companyCalenderList.get(0).getNoOfSalaryDays());
			Calendar c = Calendar.getInstance();
			c.setTime(CommonUtil.convertMonthToSqlDate(month));
			c.add(Calendar.MONTH, -1);
			Calendar endDate = Calendar.getInstance(); 
			int  year1 = c.get(Calendar.YEAR);
			int  monthNumber= c.get(Calendar.MONTH);
			endDate.set(year1,monthNumber,c.getActualMaximum(Calendar.DAY_OF_MONTH)); 
			
			List<AttendanceDO> attendenceList = attendanceService.retrieveByEmpId_Date(empId,c.getTime(),endDate.getTime());
			if(attendenceList != null && attendenceList.size() > 0){
				
				for(AttendanceDO attndeance : attendenceList){
					if(attndeance.getAttendanceType().equalsIgnoreCase("p")){
						attendanceCount += 1;
					}
					if(attndeance.getAttendanceType().equalsIgnoreCase("cf")) {
						attendanceCount+=1;
					}
					if(attndeance.getAttendanceType().equalsIgnoreCase("ch")) {
						attendanceCount+=1;
					}
					if(attndeance.getAttendanceType().equalsIgnoreCase("lpf")) {
						attendanceCount+=1;
					}
					if(attndeance.getAttendanceType().equalsIgnoreCase("lph")){
						attendanceCount+=1;
					}
					if(attndeance.getAttendanceType().equalsIgnoreCase("hlhc")){
						attendanceCount+=1;
					}
					if(attndeance.getAttendanceType().equalsIgnoreCase("apc")){
						attendanceCount+=1;
					}
					if(attndeance.getAttendanceType().equalsIgnoreCase("alc")){
						attendanceCount+=1;
					}
					if(attndeance.getAttendanceType().equalsIgnoreCase("h")){
						attendanceCount+=1;
					}
				}
				List<HolidaysDO> hoildayList = null;
				hoildayList = holidaysService.retrieveByDate(Integer.toString(year1), month.substring(0, 3));
				if(hoildayList != null && hoildayList.size() > 0) attendanceCount+=hoildayList.size();
			}
		}
		
		
		
		
		Long year = Long.parseLong(month.substring(month.length() - 4));
		String selectedMonth = month.substring(0,3);
		List<DASettingsDO> daList = daSettingsService.retrieveByMonthAndYearUnit(selectedMonth,year,unitId);
 		if(daList != null && daList.size() > 0 && daList.get(0).getSubDA() != null){
 			da =  (double) (daList.get(0).getSubDA() * daList.get(0).getDArate());
 			finalDa = (da/salaryDay)*attendanceCount;
 			deductionTotal += Math.round( finalDa);
 			//result.put(CommonConstants.MONTHLY, String.valueOf(CommonUtil.convertfloatValueWithTwoDecimalPoints(da)));
 			result.put(CommonConstants.MONTHLY, String.valueOf(Math.round(finalDa)));
 			result.put(CommonConstants.YTD, String.valueOf(finalDa*12));
 			result.put(CommonConstants.NAME, String.valueOf(deductionDetails.getDisplayname()));
 			result.put(CommonConstants.DEARNESSALLOWEANCEID, String.valueOf(daList.get(0).getDearnessallowanceId()));
 		}else{
 			result.put(CommonConstants.MONTHLY,  String.valueOf("0"));
 			result.put(CommonConstants.YTD,  String.valueOf("0"));
 			result.put(CommonConstants.NAME,  String.valueOf(deductionDetails.getDisplayname()));
 			result.put(CommonConstants.DEARNESSALLOWEANCEID, "");
 		}
		return result;
	}
	
/*	public static JSONObject getAdustmentDetails1(TaxSettingsDO taxSettings, Double ctc)throws JSONException {
		JSONObject result = new JSONObject();
		Double grossPay = ctc/12;
	//	Double basicPay = basic;
		Double monthlypay=0D;
		Double ytd=0D;
		if(taxSettings.getType() != null && taxSettings.getType().equalsIgnoreCase("P")){
			if(taxSettings.getBasicgrosspay() != null && taxSettings.getBasicgrosspay().equalsIgnoreCase("B")){
				if(taxSettings.getPercentageamount() != null){
					monthlypay = (basicPay * Double.parseDouble(taxSettings.getPercentageamount()))/100;
					ytd = monthlypay*12;
				}
			}else if(taxSettings.getBasicgrosspay() != null && taxSettings.getBasicgrosspay().equalsIgnoreCase("G")){
				if(taxSettings.getPercentageamount() != null){
					monthlypay = (grossPay * Double.parseDouble(taxSettings.getPercentageamount()))/100;
					ytd = monthlypay*12;
				}
			}
		}else if(taxSettings.getType() != null && taxSettings.getType().equalsIgnoreCase("F")){
			if(taxSettings.getFixedamount() != null){
				ytd = Double.parseDouble(taxSettings.getFixedamount());
				monthlypay = ytd/12;
			}
		}
		taxTotal += monthlypay;
		result.put(CommonConstants.MONTHLY, String.valueOf(monthlypay));
		result.put(CommonConstants.YTD, String.valueOf(ytd));
		result.put(CommonConstants.NAME, String.valueOf(taxSettings.getDisplayname()));
		
		return result;
	}*/
	
	
	public static JSONObject getempPayrollDetailsObject1(EmployeePayrollDO payroll, String month, EmployeeCtcDO empCtc)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(payroll.getEmployeeCompensation().getEmployeeCompensationId()));
		result.put(CommonConstants.EMPID, String.valueOf(payroll.getEmployee().getEmpId()));
		if(payroll.getEmployee().getDesignation() != null) result.put(CommonConstants.DESIGNATION, String.valueOf(payroll.getEmployee().getDesignation().getName()));
		else result.put(CommonConstants.DESIGNATION, String.valueOf(""));
		/*if(payroll.getEmployee().getMiddlename() != null) result.put(CommonConstants.EMPNAME, String.valueOf(payroll.getEmployee().getFirstname() + " " +payroll.getEmployee().getMiddlename() + " " + payroll.getEmployee().getLastname()));
		else result.put(CommonConstants.EMPNAME, String.valueOf(payroll.getEmployee().getFirstname() + " " + payroll.getEmployee().getLastname()));*/
		if(payroll.getEmployee().getMiddlename() != null) result.put(CommonConstants.EMPNAME, String.valueOf(payroll.getEmployee().getFirstname() + " " +payroll.getEmployee().getMiddlename() + " " + payroll.getEmployee().getLastname()));
		else if(payroll.getEmployee().getMiddlename() != null && payroll.getEmployee().getLastname() == null ) result.put(CommonConstants.EMPNAME, String.valueOf(payroll.getEmployee().getFirstname() + " " +payroll.getEmployee().getMiddlename()));
		else if(payroll.getEmployee().getMiddlename() == null && payroll.getEmployee().getLastname() == null ) result.put(CommonConstants.EMPNAME,String.valueOf(payroll.getEmployee().getFirstname()));
		else result.put(CommonConstants.EMPNAME, String.valueOf(payroll.getEmployee().getFirstname() + " " + payroll.getEmployee().getLastname()));
		result.put(CommonConstants.EMPCTC, String.valueOf(Math.round(empCtc.getEmpctc())));
		result.put(CommonConstants.EMPCTCID, String.valueOf(empCtc.getEmpCtcId()));
		result.put(CommonConstants.PAYROLLBATCHID, String.valueOf(empCtc.getPayrollBatch().getPayrollBatchId()));
		result.put(CommonConstants.PAYROLLGROUPID, String.valueOf(payroll.getPayrollGroup().getPayrollGroupId()));
		result.put(CommonConstants.PAYROLLGROUPNAME, String.valueOf(payroll.getPayrollGroup().getName()));
		JSONArray earningResultJSONArray = new JSONArray();
		for (PayrollEarningsDetailsDO earningDetails :  payroll.getPayrollEarning()) {
			JSONObject earningResult = new JSONObject();
			earningResult.put(CommonConstants.MONTHLY, String.valueOf(Math.round(earningDetails.getMonthly())));
			if(earningDetails.getYtd() != null)earningResult.put(CommonConstants.YTD, String.valueOf(Math.round(earningDetails.getYtd())));
			earningResult.put(CommonConstants.NAME, String.valueOf(earningDetails.getName()));
			earningResult.put(CommonConstants.EARNINGID, String.valueOf(earningDetails.getPayrollEarningId()));
			earningResultJSONArray.put(earningResult);
		}
		result.put(CommonConstants.EARNINGMONTHLY, String.valueOf(Math.round(payroll.getEarningmonth())));
		result.put(CommonConstants.EARNINGLIST, earningResultJSONArray);
		
		JSONArray deductionResultJSONArray = new JSONArray();
		for (PayrollDeductionDetailsDO deductionDetails :  payroll.getPayrollDeduction()) {
			JSONObject deductionResult = new JSONObject();
			if(deductionDetails.getMonthly() > 0)deductionResult.put(CommonConstants.MONTHLY, String.valueOf(Math.round(deductionDetails.getMonthly())));
			else deductionResult.put(CommonConstants.MONTHLY, String.valueOf(Math.round(deductionDetails.getMonthly())));
			//deductionResult.put(CommonConstants.YTD, String.valueOf(Math.round(deductionDetails.getYtd())));
			deductionResult.put(CommonConstants.NAME, String.valueOf(deductionDetails.getName()));
			deductionResult.put(CommonConstants.DEDUCTIONID, String.valueOf(deductionDetails.getPayrollDeductionId()));
			deductionResultJSONArray.put(deductionResult);
				
		}
		result.put(CommonConstants.DEDUCTIONMONTHLY, String.valueOf(Math.round(payroll.getDeductionmonth())));
		result.put(CommonConstants.DEDUCTIONLIST, deductionResultJSONArray);
		JSONArray taxResultJSONArray = new JSONArray();
		for (PayrollTaxDetailsDO taxDetails :  payroll.getPayrollTax()) {
			JSONObject taxResult = new JSONObject();
			if(taxDetails.getMonthly()  != null && taxDetails.getMonthly() > 0)taxResult.put(CommonConstants.MONTHLY, String.valueOf(Math.round(taxDetails.getMonthly())));
			else taxResult.put(CommonConstants.MONTHLY, String.valueOf(""));
			//taxResult.put(CommonConstants.YTD, String.valueOf(Math.round(taxDetails.getYtd())));
			if(taxDetails.getName() != null)taxResult.put(CommonConstants.NAME, String.valueOf(taxDetails.getName()));
			else taxResult.put(CommonConstants.NAME, String.valueOf(""));
			taxResult.put(CommonConstants.TAXID, String.valueOf(taxDetails.getPayrollTaxId()));
			taxResultJSONArray.put(taxResult);
		}
		if(payroll.getPayrollWeekFrom() != null){
			result.put(CommonConstants.PAYROLLWEEKFROM, String.valueOf(payroll.getPayrollWeekFrom()));
		}else{
			result.put(CommonConstants.PAYROLLWEEKFROM, String.valueOf(""));
		}
		if(payroll.getPayrollWeekTo() != null){
			result.put(CommonConstants.PAYROLLWEEKTO, String.valueOf(payroll.getPayrollWeekTo()));
		}else{
			result.put(CommonConstants.PAYROLLWEEKTO, String.valueOf(""));
		}
		
		result.put(CommonConstants.BASICGROSS_PAY, String.valueOf(Math.round(payroll.getGrossPay())));
		result.put(CommonConstants.TAXMONTHLY, String.valueOf(Math.round(payroll.getTaxMonth())));
		result.put(CommonConstants.TAXLIST, taxResultJSONArray);
		
		result.put(CommonConstants.STATUS, String.valueOf(payroll.getStatus()));
		result.put(CommonConstants.PAYROLLMONTH, month);
		result.put(CommonConstants.NETMONTHLY, String.valueOf(Math.round(payroll.getNetmonth())));
		return result;
	}
	
	
	public static JSONObject getempPayrollDetailsObject(EmployeeCompensationDO empCompensation, String month, EmployeeCtcDO empCtc)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(empCompensation.getEmployeeCompensationId()));
		result.put(CommonConstants.EMPID, String.valueOf(empCompensation.getEmployee().getEmpId()));
		if(empCompensation.getEmployee().getDesignation() != null) result.put(CommonConstants.DESIGNATION, String.valueOf(empCompensation.getEmployee().getDesignation().getName()));
		else result.put(CommonConstants.DESIGNATION, String.valueOf(""));
		/*if(empCompensation.getEmployee().getMiddlename() != null) result.put(CommonConstants.EMPNAME, String.valueOf(empCompensation.getEmployee().getFirstname() + " " +empCompensation.getEmployee().getMiddlename() + " " + empCompensation.getEmployee().getLastname()));
		else result.put(CommonConstants.EMPNAME, String.valueOf(empCompensation.getEmployee().getFirstname() + " " + empCompensation.getEmployee().getLastname()));*/
		if(empCompensation.getEmployee().getMiddlename() != null) result.put(CommonConstants.EMPNAME, String.valueOf(empCompensation.getEmployee().getFirstname() + " " +empCompensation.getEmployee().getMiddlename() + " " + empCompensation.getEmployee().getLastname()));
		else if(empCompensation.getEmployee().getMiddlename() != null && empCompensation.getEmployee().getLastname() == null ) result.put(CommonConstants.EMPNAME, String.valueOf(empCompensation.getEmployee().getFirstname() + " " +empCompensation.getEmployee().getMiddlename()));
		else if(empCompensation.getEmployee().getMiddlename() == null && empCompensation.getEmployee().getLastname() == null ) result.put(CommonConstants.EMPNAME,String.valueOf(empCompensation.getEmployee().getFirstname()));
		else result.put(CommonConstants.EMPNAME, String.valueOf(empCompensation.getEmployee().getFirstname() + " " + empCompensation.getEmployee().getLastname()));
		int noOfwrkingDays = 0;
		int salaryDays = 0;
		List<CompanyCalenderDO> companyCalenderList = null;
		if(empCtc.getPayrollBatch() != null){
			 companyCalenderList = companyCalenderService.retrieveByMonth(month,empCtc.getPayrollBatch().getPayrollBatchId());
			if(companyCalenderList != null && companyCalenderList.size() > 0){
				if(companyCalenderList.get(0).getMinimunWorkingDays() != null && !companyCalenderList.get(0).getMinimunWorkingDays().toString().isEmpty()){
					noOfwrkingDays = Integer.parseInt(companyCalenderList.get(0).getMinimunWorkingDays().toString());
				}
				if(companyCalenderList.get(0).getNoOfSalaryDays() != null && !companyCalenderList.get(0).getNoOfSalaryDays().toString().isEmpty()){
					salaryDays = Integer.parseInt(companyCalenderList.get(0).getNoOfSalaryDays().toString());
				}
			}
		}
		Calendar c1 = Calendar.getInstance();
		c1.setTime(CommonUtil.convertMonthToSqlDate(month));
		int maxDayOfMonth = c1.getActualMaximum(Calendar.DAY_OF_MONTH);
		//c1.set(Calendar.DAY_OF_MONTH, c1.getActualMaximum(Calendar.DAY_OF_MONTH));
		Double activeCtc = 0.0;
		if(empCtc.getStartdate() != null){
			String startDate = CommonUtil.convertDateToStringWithOutDate(empCtc.getStartdate());
			String endDate = null;
			if(empCtc.getEnddate() != null)endDate = CommonUtil.convertDateToStringWithOutDate(empCtc.getEnddate());
			if(startDate.equalsIgnoreCase(month)){
				Calendar c=Calendar.getInstance();
		        c.setTime(empCtc.getStartdate());
		        int days = 0;
		     // int days = c.getActualMaximum(Calendar.DAY_OF_MONTH);
		        if(salaryDays == 0){
		        	  days = c.getActualMaximum(Calendar.DAY_OF_MONTH); 
			    }else{
			    	days = salaryDays;
			    }
		        
		        String[] noofdays = CommonUtil.convertDateToYearWithOutTime(empCtc.getStartdate()).split("-");
		        int totalDays = days;
		        if (endDate != null && endDate.equalsIgnoreCase(month)){
		        	 String[] noofenddays = CommonUtil.convertDateToYearWithOutTime(empCtc.getEnddate()).split("-");
		        	 totalDays =(int)( Long.parseLong( noofenddays[2]) - Long.parseLong(noofdays[2])+1);
		        }else{
		        	if(!noofdays[2].equals("01")){
			        	 totalDays = (days - Integer.parseInt(noofdays[2]))+1;
			        }
		        }
		        double perdaySalary = 0.0;
		        if(empCtc.getCtcType() != null){
		        	 if(empCtc.getCtcType().equalsIgnoreCase("y"))  perdaySalary = (double) (empCtc.getEmpctc()/12)/days;
				     if(empCtc.getCtcType().equalsIgnoreCase("m"))  perdaySalary = (double) empCtc.getEmpctc()/days;
				     if(empCtc.getCtcType().equalsIgnoreCase("d"))  perdaySalary = (double) empCtc.getEmpctc();
		        }else{
		        	
		        	double monthctc = empCtc.getEmpctc()/12;
		        	perdaySalary = (double) (monthctc/days);
		        }
		        
		       // double monthctc = empCtc.getEmpctc()/12;
		      //  Double perdaySalary = (monthctc/days);
		        activeCtc = (double) ((perdaySalary * noOfwrkingDays)*12);
			}else if (endDate != null && endDate.equalsIgnoreCase(month)){
				Calendar c=Calendar.getInstance();
		        c.setTime(empCtc.getEnddate());
		       // int days = c.getActualMaximum(Calendar.DAY_OF_MONTH);
		        int days = 0;
		        if(salaryDays == 0){
		        	  days = c.getActualMaximum(Calendar.DAY_OF_MONTH); 
			    }else{
			    	days = salaryDays;
			    }
		        int totalDays = days;
		        String[] noofenddays = CommonUtil.convertDateToYearWithOutTime(empCtc.getStartdate()).split("-");
		        if(startDate.equalsIgnoreCase(month)){
		        	  String[] noofStartdays = CommonUtil.convertDateToYearWithOutTime(empCtc.getStartdate()).split("-");
		        	  totalDays =(int)( Long.parseLong( noofenddays[2]) - Long.parseLong(noofStartdays[2])+1);
		        }else{
		        	totalDays = (int)(Long.parseLong( noofenddays[2]));
		        }
		        
		        double perdaySalary = 0.0;
		        if(empCtc.getCtcType() != null){
		        	 if(empCtc.getCtcType().equalsIgnoreCase("y"))  perdaySalary = (double) (empCtc.getEmpctc()/12)/days;
				     if(empCtc.getCtcType().equalsIgnoreCase("m"))  perdaySalary = (double) empCtc.getEmpctc()/days;
				     if(empCtc.getCtcType().equalsIgnoreCase("d"))  perdaySalary = (double) empCtc.getEmpctc();
		        }else{
		        	double monthctc = empCtc.getEmpctc()/12;
		        	perdaySalary = (double)(monthctc/days);
		        }
		        
		        activeCtc = ((perdaySalary * noOfwrkingDays)*12);
			}else{
				
				double perdaySalary = 0.0;
		        if(empCtc.getCtcType() != null && noOfwrkingDays > 0 && salaryDays > 0 ){
		        	 if(empCtc.getCtcType().equalsIgnoreCase("y"))  perdaySalary = (double) (empCtc.getEmpctc()/12)/salaryDays;
				     if(empCtc.getCtcType().equalsIgnoreCase("m"))  perdaySalary = (double) empCtc.getEmpctc()/salaryDays;
				     if(empCtc.getCtcType().equalsIgnoreCase("d"))  perdaySalary = (double) empCtc.getEmpctc();
				     activeCtc = ((perdaySalary * noOfwrkingDays)*12);
		        }else{
		        	if(empCtc.getCtcType() != null && salaryDays <= 0){
		        		if(empCtc.getCtcType().equalsIgnoreCase("y"))  perdaySalary = (double) (empCtc.getEmpctc()/12)/maxDayOfMonth;
					    if(empCtc.getCtcType().equalsIgnoreCase("m"))  perdaySalary = (double) empCtc.getEmpctc()/maxDayOfMonth;
					    if(empCtc.getCtcType().equalsIgnoreCase("d"))  perdaySalary = (double) empCtc.getEmpctc();
					    if(noOfwrkingDays >0) activeCtc =  ((perdaySalary * noOfwrkingDays)*12);
					    else activeCtc =  ((perdaySalary * maxDayOfMonth)*12);
					     
		        	}else{
		        		
		        		activeCtc = (double) empCtc.getEmpctc();
		        	}
		        }
			}
		}else{
			double perdaySalary = 0.0;
	        if(empCtc.getCtcType() != null && noOfwrkingDays > 0 && salaryDays > 0){
	        	 if(empCtc.getCtcType().equalsIgnoreCase("y"))  perdaySalary = (double) (empCtc.getEmpctc()/12)/salaryDays;
			     if(empCtc.getCtcType().equalsIgnoreCase("m"))  perdaySalary = (double) empCtc.getEmpctc()/salaryDays;
			     if(empCtc.getCtcType().equalsIgnoreCase("d"))  perdaySalary = (double) empCtc.getEmpctc();
			     activeCtc =  ((perdaySalary * noOfwrkingDays)*12);
	        }else{
	        	if(empCtc.getCtcType() != null && salaryDays <= 0){
	        		if(empCtc.getCtcType().equalsIgnoreCase("y"))  perdaySalary = (double) (empCtc.getEmpctc()/12)/maxDayOfMonth;
				    if(empCtc.getCtcType().equalsIgnoreCase("m"))  perdaySalary = (double) empCtc.getEmpctc()/maxDayOfMonth;
				    if(empCtc.getCtcType().equalsIgnoreCase("d"))  perdaySalary = (double) empCtc.getEmpctc();
				    if(noOfwrkingDays >0) activeCtc =  ((perdaySalary * noOfwrkingDays)*12);
				    else activeCtc =  ((perdaySalary * maxDayOfMonth)*12);
				     
	        	}else{
	        		activeCtc = (double) empCtc.getEmpctc();
	        	}
	        	
	        }
			//activeCtc = empCtc.getEmpctc();
		}
		
		result.put(CommonConstants.EMPCTC, String.valueOf(empCtc.getEmpctc()));
		result.put(CommonConstants.EMPCTCID, String.valueOf(empCtc.getEmpCtcId()));
		
		result.put(CommonConstants.PAYROLLBATCHID, String.valueOf(empCtc.getPayrollBatch().getPayrollBatchId()));
	//	List<EmployeeCtcDO> compensationList = employeeCtcService.retrieveEmpIdeWithDate(empCompensation.getEmployee().getEmpId(),CommonUtil.convertMonthToSqlDate(month));
		
		/*if(compensationList != null && compensationList.size() > 0){
			activeCtc = compensationList.get(0).getEmpctc();
			result.put(CommonConstants.EMPCTC, String.valueOf(compensationList.get(0).getEmpctc()));
		}*/
		
		result.put(CommonConstants.PAYROLLGROUPID, String.valueOf(empCompensation.getPayrollGroup().getPayrollGroupId()));
		result.put(CommonConstants.PAYROLLGROUPNAME, String.valueOf(empCompensation.getPayrollGroup().getName()));
		
		basicPay = 0.0;
		totalLop = 0L;
		deductionTotal = 0.0;
		leaveCount = 0.0;
		monthlyDA= 0.0;
		monthlyAddDA= 0.0;
		JSONArray deductionResultJSONArray = new JSONArray();
		
		int year = (int) Long.parseLong(month.substring(month.length() - 4));
		String selectedMonth = month.substring(0,3);
		try{   
	        Date date = new SimpleDateFormat("MMM").parse(selectedMonth);
	        Calendar cal = Calendar.getInstance();
	        cal.setTime(date);
	        int  monthNumber=cal.get(Calendar.MONTH);
	        Calendar satrtDate = Calendar.getInstance();    
			satrtDate.set(year,monthNumber,1); 
			satrtDate.set(Calendar.DAY_OF_MONTH, satrtDate.getActualMinimum(Calendar.DAY_OF_MONTH));
			Calendar endDate = Calendar.getInstance(); 
			endDate.set(year,monthNumber,1); 
			endDate.set(Calendar.DAY_OF_MONTH, endDate.getActualMaximum(Calendar.DAY_OF_MONTH));
			double lopleave = 0.0;
			double leaveTaken = 0.0;
			
			
			    Calendar calendarStart=Calendar.getInstance();
			    calendarStart.set(Calendar.YEAR,year);
			    calendarStart.set(Calendar.MONTH,0);
			    calendarStart.set(Calendar.DAY_OF_MONTH,1);
			    Date yearStartDate=calendarStart.getTime();

			    Calendar calendarEnd=Calendar.getInstance();
			    calendarEnd.set(Calendar.YEAR,year);
			    calendarEnd.set(Calendar.MONTH,11);
			    calendarEnd.set(Calendar.DAY_OF_MONTH,31);

			    Date yearEndDate=calendarEnd.getTime();
			    long noOfDays = 0;
			List <EmployeeLeavegroupDO> empLeavegrpList = employeeLeavegroupService.retrieveByEmpId(empCompensation.getEmployee().getEmpId().toString());
			if(empLeavegrpList != null && empLeavegrpList.size() > 0){
				if(empLeavegrpList.get(0).getLeaveGroup() != null && empLeavegrpList.get(0).getLeaveGroup().getLeaveGroupId() != null){
					
					List<LeaveGroupSettingsDO> leaveGroupList = leaveGroupSettingsService.retrieveByGroupIdWithStatus(empLeavegrpList.get(0).getLeaveGroup().getLeaveGroupId());
					if(leaveGroupList != null && leaveGroupList.size() > 0){
						for (LeaveGroupSettingsDO leaveType : leaveGroupList) {
							if(leaveType.getLeaveManagement().getNumberOfDays() != null)noOfDays += leaveType.getLeaveManagement().getNumberOfDays();
						}
					}
				}
				List <LeaveRequestsDO> appliedleaveRequestsList = leaveRequestsService.retrieveLeavesByFromToDate(empCompensation.getEmployee().getEmpId(),yearStartDate,yearEndDate);
				if(appliedleaveRequestsList != null && appliedleaveRequestsList.size() >0){
					 leaveTaken = getLeaveDetails(appliedleaveRequestsList, Double.parseDouble(activeCtc.toString()));
				}
				if(leaveTaken > noOfDays){
					lopleave = leaveTaken-noOfDays;
				}
			}
			
			
			List <LeaveRequestsDO> leaveRequestList = leaveRequestsService.retrieveLeavesForPayroll(empCompensation.getEmployee().getEmpId(),satrtDate.getTime(), endDate.getTime());
			if(leaveRequestList != null && leaveRequestList.size() >0){
				 lopleave += getLeaveLOPDetails(leaveRequestList, Double.parseDouble(activeCtc.toString()));
			}
			List<EmployeeLopDO> lopList = employeeLopService.retrieveByEmpIdDateStatus(empCompensation.getEmployee().getEmpId(), satrtDate.getTime(), endDate.getTime());
			if(lopList != null && lopList.size()>0){
				Date joiningDate = satrtDate.getTime();
				Calendar c=Calendar.getInstance();
		        c.setTime(joiningDate);
		       // int totalWorkingDays = c.getActualMaximum(Calendar.DAY_OF_MONTH);
		        int totalWorkingDays = 0;
		        if(noOfwrkingDays == 0){
		        	totalWorkingDays = c.getActualMaximum(Calendar.DAY_OF_MONTH); 
			    }else{
			    	totalWorkingDays = noOfwrkingDays;
			    }
		        getLOPDetailsObject(lopList,Double.parseDouble(activeCtc.toString()),totalWorkingDays,0.0);
		        
				//deductionResultJSONArray.put(getLOPDetailsObject(lopList,Double.parseDouble(activeCtc.toString()),totalWorkingDays,lopleave));
		    }else{
		    	if(lopleave >= 1){
		    		Date joiningDate = satrtDate.getTime();
					Calendar c=Calendar.getInstance();
			        c.setTime(joiningDate);
			       // int totalWorkingDays = c.getActualMaximum(Calendar.DAY_OF_MONTH);
			        int totalWorkingDays = 0;
			        if(noOfwrkingDays == 0){
			        	totalWorkingDays = c.getActualMaximum(Calendar.DAY_OF_MONTH); 
				    }else{
				    	totalWorkingDays = noOfwrkingDays;
				    }
			        getLOPDetailsObject(lopList,Double.parseDouble(activeCtc.toString()),totalWorkingDays,0.0);
					//deductionResultJSONArray.put(getLOPDetailsObject(lopList,Double.parseDouble(activeCtc.toString()),totalWorkingDays,lopleave));
		    	}
		    }
		 }
		catch(Exception e)
		{
		 e.printStackTrace();
		}
		Double grossAmount = 0.0;
		
		grossAmount = (double) ((activeCtc/12) - totalLop) *12;
		for (PayrollGroupEarningsDO earningDetails :  empCompensation.getPayrollGroup().getPayrollGroupEarning()) {
			if(earningDetails.getStatus() == CommonConstants.ACTIVE && earningDetails.getAllowanceSettings().getEarningType().getName().equalsIgnoreCase(CommonConstants.BASIC)){
				if(earningDetails.getAllowanceSettings().getEarningType().getStatus() == CommonConstants.ACTIVE){
					if(earningDetails.getAllowanceSettings().getPercentageamount() != null){
						basicPay = (grossAmount * Double.parseDouble(earningDetails.getAllowanceSettings().getPercentageamount()))/100;
					}
				}
			}
		}
		JSONArray earningResultJSONArray = new JSONArray();
		if(basicPay != 0){
			earingTotal = 0.0;
			taxableIncome = 0.0;
			totalAllowance = 0.0;
			Boolean Adjustment = false;
			PayrollGroupEarningsDO adjustData = new PayrollGroupEarningsDO();
			
			
			/*if(empCompensation.getPayrollGroup().getPayrollGroupWorkingdays().size() > 0){*/
			//List<CompanyCalenderDO> companyCalenderList = companyCalenderService.retrieveByMonth(month,empCtc.getPayrollBatch().getPayrollBatchId());
			Calendar c = Calendar.getInstance();
				c.setTime(CommonUtil.convertMonthToSqlDate(month));
				Calendar endDate = Calendar.getInstance(); 
				int  year1 = c.get(Calendar.YEAR);
				 int  monthNumber= c.get(Calendar.MONTH);
				endDate.set(year1,monthNumber,c.getActualMaximum(Calendar.DAY_OF_MONTH)); 
				/* Date date = new SimpleDateFormat("MMM").parse(selectedMonth);
 		        Calendar cal = Calendar.getInstance();
 		        cal.setTime(date);
 		        int  monthNumber=cal.get(Calendar.MONTH);
 		        Calendar satrtDate = Calendar.getInstance();    
 				satrtDate.set(year,monthNumber,1); 
 				satrtDate.set(Calendar.DAY_OF_MONTH, satrtDate.getActualMinimum(Calendar.DAY_OF_MONTH));
 				Calendar endDate = Calendar.getInstance(); 
 				endDate.set(year,monthNumber,1); 
 				endDate.set(Calendar.DAY_OF_MONTH, endDate.getActualMaximum(Calendar.DAY_OF_MONTH));*/
 				
 				/*System.out.println("c.getTime(),endDate.getTime()"+c.getTime());
 				System.out.println("hh"+endDate.getTime());*/
 				
				//result.put(CommonConstants.DAYS, String.valueOf("0"));
				double attendanceCount = 0; 
				List<AttendanceDO> attendenceList = attendanceService.retrieveByEmpId_Date(empCompensation.getEmployee().getEmpId(),c.getTime(),endDate.getTime());
				if(attendenceList != null && attendenceList.size() > 0){
					int count = 0;
					int pcount = 0;
					double actallyPcount = 0;
					
					for(AttendanceDO attndeance : attendenceList){
						if(attndeance.getAttendanceType().equalsIgnoreCase("p")){
							count+=1;
							pcount+=1;
							actallyPcount+=1;
							attendanceCount += 1;
						}
						if(attndeance.getAttendanceType().equalsIgnoreCase("cf")) {
							count+=1;
							attendanceCount+=1;
						}
						if(attndeance.getAttendanceType().equalsIgnoreCase("ch")) {
							count+=1;
							attendanceCount+=1;
							actallyPcount +=0.5;
						}
						if(attndeance.getAttendanceType().equalsIgnoreCase("lpf")) {
							count+=1;
							attendanceCount+=1;
						}
						if(attndeance.getAttendanceType().equalsIgnoreCase("lph")){
							count+=1;
							attendanceCount+=1;
							actallyPcount+=0.5;
						}
						if(attndeance.getAttendanceType().equalsIgnoreCase("hlhc")){
							count+=1;
							attendanceCount+=1;
						}
						if(attndeance.getAttendanceType().equalsIgnoreCase("apc")){
							attendanceCount+=1;
							count+= 1;
							actallyPcount +=0.5;
						}
						if(attndeance.getAttendanceType().equalsIgnoreCase("alc")){
							attendanceCount+=1;
							count+= 1;
							actallyPcount +=0.5;
						}
						if(attndeance.getAttendanceType().equalsIgnoreCase("h")){
							attendanceCount+=1;
							count+= 1;
							actallyPcount +=0.5;
						}
					}
					List<HolidaysDO> hoildayList = null;
					/*if(empCompensation.getEmployee().getWorklocation() != null && empCompensation.getEmployee().getWorklocation().getWorklocationId() != null){
						hoildayList = holidaysService.retrieveByDateAndWorkLocation(Integer.toString(year1), month.substring(0, 3),empCompensation.getEmployee().getWorklocation().getWorklocationId());
					}else{
						hoildayList = holidaysService.retrieveByDate(Integer.toString(year1), month.substring(0, 3));
					}*/
					hoildayList = holidaysService.retrieveByDate(Integer.toString(year1), month.substring(0, 3));
					if(hoildayList != null && hoildayList.size() > 0) attendanceCount+=hoildayList.size();
					result.put(CommonConstants.DAYS, String.valueOf(count));
					result.put(CommonConstants.PRESENTDAYS, String.valueOf(pcount));
			
			
			if(Long.parseLong(companyCalenderList.get(0).getMinimunWorkingDays()) <= attendanceCount){
				for (PayrollGroupAttendanceAllowanceDO attendanceAllowanceDetails :  empCompensation.getPayrollGroup().getPayrollGroupAttendanceAllowance()) {
					if(attendanceAllowanceDetails.getStatus() == CommonConstants.ACTIVE){
						earningResultJSONArray.put(getAttendanceAllowanceCalculation(attendanceAllowanceDetails.getAttendanceAllowance(),(noOfwrkingDays - leaveCount),Long.parseLong(companyCalenderList.get(0).getMinimunWorkingDays())));
					}
				}
			}
						
						//if(Long.parseLong(companyCalenderList.get(0).getMinimunWorkingDays()) <= count){
							for (PayrollGroupWorkingdaysAllowanceDO workingAllowanceDetails :  empCompensation.getPayrollGroup().getPayrollGroupWorkingdays()) {
								if(workingAllowanceDetails.getStatus() == CommonConstants.ACTIVE){
									/*List <LeaveRequestsDO> leaveRequestList = leaveRequestsService.retrieveLeavesForPayroll(empCompensation.getEmployee().getEmpId(),satrtDate.getTime(), endDate.getTime());
									if(leaveRequestList != null && leaveRequestList.size() >0){
										if(leaveRequestList.get(0).getLeaveManagement() != null && leaveRequestList.get(0).getLeaveManagement().getLeaveManagementId() != null){
											
										}
										 //lopleave += getLeaveLOPDetails(leaveRequestList, Double.parseDouble(activeCtc.toString()));
									}*/
									earningResultJSONArray.put(getWorkingDayAllowanceCalculation(workingAllowanceDetails.getWorkingDayAllowance(),attendanceCount,Long.parseLong(companyCalenderList.get(0).getMinimunWorkingDays()),actallyPcount));
								}
							}
						//}
					}
 				
				for (PayrollGroupEarningsDO earningDetails :  empCompensation.getPayrollGroup().getPayrollGroupEarning()) {
					if(earningDetails.getStatus() == CommonConstants.ACTIVE){
						if(!earningDetails.getAllowanceSettings().getType().equalsIgnoreCase("A")){
							if(earningDetails.getAllowanceSettings().getEarningType().getName().equalsIgnoreCase(CommonConstants.DEARNESSALLOWEANCE) && companyCalenderList != null && companyCalenderList.get(0).getNoOfSalaryDays() != null && empCompensation.getEmployee().getUnitOrBranch() != null){
								earningResultJSONArray.put(getDearnessAlloweanceDetails(month, earningDetails.getAllowanceSettings(),Long.parseLong(companyCalenderList.get(0).getNoOfSalaryDays()),(noOfwrkingDays - leaveCount),empCompensation.getEmployee().getUnitOrBranch().getUnitOrBranchId()));
							}else if(earningDetails.getAllowanceSettings().getEarningType().getName().equalsIgnoreCase(CommonConstants.ADD_DA) || earningDetails.getAllowanceSettings().getEarningType().getName().equalsIgnoreCase(CommonConstants.ADDDEARNESSALLOWEANCE) && empCompensation.getEmployee().getUnitOrBranch() != null){
								earningResultJSONArray.put(getADDDearnessAlloweanceDetails(month, earningDetails.getAllowanceSettings(), empCompensation.getEmployee().getUnitOrBranch().getUnitOrBranchId(),empCompensation.getEmployee().getEmpId(),empCtc.getPayrollBatch().getPayrollBatchId()));
							}else{
								earningResultJSONArray.put(getCompensationAllowenceDetailInfoObject(earningDetails.getPayrollGroupEarningsId(),earningDetails.getAllowanceSettings(), grossAmount, (noOfwrkingDays - leaveCount),salaryDays));
							}
							
						}else{
							if(earningDetails.getAllowanceSettings().getEarningType().getName().equalsIgnoreCase(CommonConstants.DEARNESSALLOWEANCE) && companyCalenderList != null && companyCalenderList.get(0).getNoOfSalaryDays() != null && empCompensation.getEmployee().getUnitOrBranch() != null){
								earningResultJSONArray.put(getDearnessAlloweanceDetails(month, earningDetails.getAllowanceSettings(),Long.parseLong(companyCalenderList.get(0).getNoOfSalaryDays()),(noOfwrkingDays - leaveCount),empCompensation.getEmployee().getUnitOrBranch().getUnitOrBranchId()));
							}else if(earningDetails.getAllowanceSettings().getEarningType().getName().equalsIgnoreCase(CommonConstants.ADD_DA) || earningDetails.getAllowanceSettings().getEarningType().getName().equalsIgnoreCase(CommonConstants.ADDDEARNESSALLOWEANCE) && empCompensation.getEmployee().getUnitOrBranch() != null){
								earningResultJSONArray.put(getADDDearnessAlloweanceDetails(month, earningDetails.getAllowanceSettings(), empCompensation.getEmployee().getUnitOrBranch().getUnitOrBranchId(),empCompensation.getEmployee().getEmpId(),empCtc.getPayrollBatch().getPayrollBatchId()));
							}else{
								Adjustment = true;
								adjustData = earningDetails;
							}
							
						}
					}
				}
				if(Adjustment){
					earningResultJSONArray.put(getAdustmentDetails(adjustData.getPayrollGroupEarningsId(),adjustData.getAllowanceSettings(), grossAmount));
				}
				List<GeneralEarningDO> generalEarningList = generalEarningService.retrieveActiveByEmpIdAndMonth(empCompensation.getEmployee().getEmpId(), month);
				if(generalEarningList != null && generalEarningList.size() > 0){
					for(GeneralEarningDO generalEarning: generalEarningList){
						//monthlypay += generalEarning.getAmount();
						earningResultJSONArray.put(getGeneralEarningObject(generalEarning));
					}
					//earningResultJSONArray.put(getGeneralEarningObject(generalEarningList));
				}
				
				
 				List<EmployeeExpenseDO>  empExpenseList = employeeExpenseService.retrieveByMonthAndEmpId(empCompensation.getEmployee().getEmpId(), c.getTime(), endDate.getTime(), CommonConstants.ACTIVESTRING);
				if(empExpenseList != null && empExpenseList.size() > 0){
					earningResultJSONArray.put(getEmployeeExpenseDetail(empExpenseList));
				}
 				
 				
 				/*}else{
				Calendar c = Calendar.getInstance();
 				c.setTime(CommonUtil.convertMonthToSqlDate(month));
 				Calendar endDate = Calendar.getInstance(); 
 				int  year2 = c.get(Calendar.YEAR);
 				 int  monthNumber= c.get(Calendar.MONTH);
 				endDate.set(year2,monthNumber,c.getActualMaximum(Calendar.DAY_OF_MONTH)); 
	 				List<AttendanceDO> attendenceList = attendanceService.retrieveByEmpId_Date(empCompensation.getEmployee().getEmpId(),c.getTime(),endDate.getTime());
					if(attendenceList != null && attendenceList.size() > 0){
						int count = 0;
						int pcount = 0;
						for(AttendanceDO attndeance : attendenceList){
							if(attndeance.getAttendanceType().equalsIgnoreCase("p")) {
								count+=1;
								pcount+=1;
							}
							if(attndeance.getAttendanceType().equalsIgnoreCase("cf")) count+=1;
							if(attndeance.getAttendanceType().equalsIgnoreCase("ch")) count+=0.5;
							if(attndeance.getAttendanceType().equalsIgnoreCase("lpf")) count+=1;
							if(attndeance.getAttendanceType().equalsIgnoreCase("lph")) count+=0.5;
						}
						result.put(CommonConstants.DAYS, String.valueOf(count));
						result.put(CommonConstants.PRESENTDAYS, String.valueOf(pcount));
					}else{
						result.put(CommonConstants.DAYS, String.valueOf(0));
						result.put(CommonConstants.PRESENTDAYS, String.valueOf(0));
					}
			}*/
			/*
			*/
			
			
			
			/*List<WorkingDayAllowanceDO> workingDayAllowanceList = workingDayAllowanceService.retrieveActive();
			if(workingDayAllowanceList != null && workingDayAllowanceList.size()> 0){
				List<CompanyCalenderDO> companyCalenderList = companyCalenderService.retrieveByMonth(month,empCtc.getPayrollBatch().getPayrollBatchId());
	 			if(companyCalenderList != null && companyCalenderList.size() > 0){
	 				Calendar c = Calendar.getInstance();
	 				c.setTime(CommonUtil.convertMonthToSqlDate(month));
	 				Calendar endDate = Calendar.getInstance(); 
	 				int  year = c.get(Calendar.YEAR);
	 				 int  monthNumber= c.get(Calendar.MONTH);
	 				endDate.set(year,monthNumber,c.getActualMaximum(Calendar.DAY_OF_MONTH)); 
	 				
	 				
	 				 Date date = new SimpleDateFormat("MMM").parse(selectedMonth);
	 		        Calendar cal = Calendar.getInstance();
	 		        cal.setTime(date);
	 		        int  monthNumber=cal.get(Calendar.MONTH);
	 		        Calendar satrtDate = Calendar.getInstance();    
	 				satrtDate.set(year,monthNumber,1); 
	 				satrtDate.set(Calendar.DAY_OF_MONTH, satrtDate.getActualMinimum(Calendar.DAY_OF_MONTH));
	 				Calendar endDate = Calendar.getInstance(); 
	 				endDate.set(year,monthNumber,1); 
	 				endDate.set(Calendar.DAY_OF_MONTH, endDate.getActualMaximum(Calendar.DAY_OF_MONTH));
	 				
	 				
	 				
	 				
	 				List<AttendanceDO> attendenceList = attendanceService.retrieveByEmpIdAndDate(empCompensation.getEmployee().getEmpId(),c.getTime(),endDate.getTime());
					if(attendenceList != null && attendenceList.size() > 0){
						if(Long.parseLong(companyCalenderList.get(0).getMinimunWorkingDays()) <= attendenceList.size()){
							for(WorkingDayAllowanceDO workingDayAllowance: workingDayAllowanceList) earningResultJSONArray.put(getWorkingDayAllowanceCalculation(workingDayAllowance,empCompensation.getEmployee().getEmpId(),attendenceList.size(),c.getActualMaximum(Calendar.DAY_OF_MONTH)));
						}
					}
	 			}
			}*/
			//deductionTotal = 0.0;
			//JSONArray deductionResultJSONArray = new JSONArray();
			for (PayrollGroupDeductionDO deductionDetails :  empCompensation.getPayrollGroup().getPayrollGroupDeduction()) {
				if(deductionDetails.getStatus() == CommonConstants.ACTIVE){
				//	deductionResultJSONArray.put(getCompensationDeductionDetailInfoObject(deductionDetails.getPayrollGroupDeductionId(),deductionDetails.getDeductionSettings(), Double.parseDouble(activeCtc.toString())));
					if(deductionDetails.getDeductionSettings().getDeductionType().getName().equalsIgnoreCase(CommonConstants.SUBDEARNESSALLOWEANCE)){
						deductionResultJSONArray.put(getSubDADetails(month, deductionDetails.getDeductionSettings(),empCompensation.getEmployee().getUnitOrBranch().getUnitOrBranchId(),empCtc.getPayrollBatch().getPayrollBatchId(),empCompensation.getEmployee().getEmpId()));
					}else{
						deductionResultJSONArray.put(getCompensationDeductionDetailInfoObject(deductionDetails.getPayrollGroupDeductionId(),deductionDetails.getDeductionSettings(), grossAmount));
					}
					
				}
			}
			
			//List <EmployeePFInfoDO> pfDetail = empPFService.retrieveByEmpId(empCompensation.getEmployee().getEmpId());
			List <EmployeePFInfoDO> pfDetail = empPFService.retrieveByEmpIdAndDate(empCompensation.getEmployee().getEmpId(), c.getTime(), endDate.getTime());
			if(pfDetail != null && pfDetail.size() > 0 ){
				deductionResultJSONArray.put(getPFCalualation(pfDetail.get(0),pfDetail.get(0).getPfGroup(), grossAmount,(noOfwrkingDays - leaveCount),salaryDays));
				/*if(pfDetail.get(0).getPfGroup() != null){
					if(pf_estDetail.get(0).getPfGroup().getPfGroupEarning() != null && pf_estDetail.get(0).getPfGroup().getPfGroupEarning().size() >0 ){
						deductionResultJSONArray.put(getPFCalualation(pf_estDetail.get(0).getPfGroup(), grossAmount));
					}
				}
				
				if(pf_estDetail.get(0).getEsiGroup() != null){
					if(pf_estDetail.get(0).getEsiGroup().getEsiGroupEarnings() != null && pf_estDetail.get(0).getEsiGroup().getEsiGroupEarnings().size() >0 ){
						deductionResultJSONArray.put(getESICalualation(pf_estDetail.get(0).getEsiGroup(), grossAmount));
					}
				}*/
			}
			//List <EmployeeESIInfoDO> esiDetail = empESIService.retrieveByEmpId(empCompensation.getEmployee().getEmpId());
			List <EmployeeESIInfoDO> esiDetail = empESIService.retrieveByEmpIdAndDate(empCompensation.getEmployee().getEmpId(), c.getTime(), endDate.getTime());
			if(esiDetail != null && esiDetail.size() > 0 ){
				deductionResultJSONArray.put(getESICalualation(esiDetail.get(0),esiDetail.get(0).getEsiGroup(), grossAmount,(noOfwrkingDays - leaveCount),salaryDays));
				
				/*if(pf_estDetail.get(0).getEsiGroup() != null){
					if(pf_estDetail.get(0).getEsiGroup().getEsiGroupEarnings() != null && pf_estDetail.get(0).getEsiGroup().getEsiGroupEarnings().size() >0 ){
						deductionResultJSONArray.put(getESICalualation(pf_estDetail.get(0).getEsiGroup(), grossAmount));
					}
				}*/
			}
			
			
			
			
			/*int year = (int) Long.parseLong(month.substring(month.length() - 4));
			String selectedMonth = month.substring(0,3);
			try{   
		        Date date = new SimpleDateFormat("MMM").parse(selectedMonth);
		        Calendar cal = Calendar.getInstance();
		        cal.setTime(date);
		        int  monthNumber=cal.get(Calendar.MONTH);
		        Calendar satrtDate = Calendar.getInstance();    
				satrtDate.set(year,monthNumber,1); 
				satrtDate.set(Calendar.DAY_OF_MONTH, satrtDate.getActualMinimum(Calendar.DAY_OF_MONTH));
				Calendar endDate = Calendar.getInstance(); 
				endDate.set(year,monthNumber,1); 
				endDate.set(Calendar.DAY_OF_MONTH, endDate.getActualMaximum(Calendar.DAY_OF_MONTH));
				double lopleave = 0.0;
				double leaveTaken = 0.0;
				
				
				    Calendar calendarStart=Calendar.getInstance();
				    calendarStart.set(Calendar.YEAR,year);
				    calendarStart.set(Calendar.MONTH,0);
				    calendarStart.set(Calendar.DAY_OF_MONTH,1);
				    Date yearStartDate=calendarStart.getTime();

				    Calendar calendarEnd=Calendar.getInstance();
				    calendarEnd.set(Calendar.YEAR,year);
				    calendarEnd.set(Calendar.MONTH,11);
				    calendarEnd.set(Calendar.DAY_OF_MONTH,31);

				    Date yearEndDate=calendarEnd.getTime();
				    long noOfDays = 0;
				List <EmployeeLeavegroupDO> empLeavegrpList = employeeLeavegroupService.retrieveByEmpId(empCompensation.getEmployee().getEmpId().toString());
				if(empLeavegrpList != null && empLeavegrpList.size() > 0){
					if(empLeavegrpList.get(0).getLeaveGroup() != null && empLeavegrpList.get(0).getLeaveGroup().getLeaveGroupId() != null){
						
						List<LeaveGroupSettingsDO> leaveGroupList = leaveGroupSettingsService.retrieveByGroupIdWithStatus(empLeavegrpList.get(0).getLeaveGroup().getLeaveGroupId());
						if(leaveGroupList != null && leaveGroupList.size() > 0){
							for (LeaveGroupSettingsDO leaveType : leaveGroupList) {
								if(leaveType.getLeaveManagement().getNumberOfDays() != null)noOfDays += leaveType.getLeaveManagement().getNumberOfDays();
							}
						}
					}
					List <LeaveRequestsDO> appliedleaveRequestsList = leaveRequestsService.retrieveLeavesByFromToDate(empCompensation.getEmployee().getEmpId(),yearStartDate,yearEndDate);
					if(appliedleaveRequestsList != null && appliedleaveRequestsList.size() >0){
						 leaveTaken = getLeaveDetails(appliedleaveRequestsList, Double.parseDouble(activeCtc.toString()));
					}
					if(leaveTaken > noOfDays){
						lopleave = leaveTaken-noOfDays;
					}
				}
				
				
				List <LeaveRequestsDO> leaveRequestList = leaveRequestsService.retrieveLeavesForPayroll(empCompensation.getEmployee().getEmpId(),satrtDate.getTime(), endDate.getTime());
				if(leaveRequestList != null && leaveRequestList.size() >0){
					 lopleave += getLeaveLOPDetails(leaveRequestList, Double.parseDouble(activeCtc.toString()));
				}
				List<EmployeeLopDO> lopList = employeeLopService.retrieveByEmpIdDateStatus(empCompensation.getEmployee().getEmpId(), satrtDate.getTime(), endDate.getTime());
				if(lopList != null && lopList.size()>0){
					Date joiningDate = satrtDate.getTime();
					Calendar c=Calendar.getInstance();
			        c.setTime(joiningDate);
			       // int totalWorkingDays = c.getActualMaximum(Calendar.DAY_OF_MONTH);
			        int totalWorkingDays = 0;
			        if(noOfwrkingDays == 0){
			        	totalWorkingDays = c.getActualMaximum(Calendar.DAY_OF_MONTH); 
				    }else{
				    	totalWorkingDays = noOfwrkingDays;
				    }
			        
					deductionResultJSONArray.put(getLOPDetailsObject(lopList,Double.parseDouble(activeCtc.toString()),totalWorkingDays,lopleave));
			    }else{
			    	if(lopleave >= 1){
			    		Date joiningDate = satrtDate.getTime();
						Calendar c=Calendar.getInstance();
				        c.setTime(joiningDate);
				       // int totalWorkingDays = c.getActualMaximum(Calendar.DAY_OF_MONTH);
				        int totalWorkingDays = 0;
				        if(noOfwrkingDays == 0){
				        	totalWorkingDays = c.getActualMaximum(Calendar.DAY_OF_MONTH); 
					    }else{
					    	totalWorkingDays = noOfwrkingDays;
					    }
						deductionResultJSONArray.put(getLOPDetailsObject(lopList,Double.parseDouble(activeCtc.toString()),totalWorkingDays,lopleave));
			    	}
			    }
			 }
			catch(Exception e)
			{
			 e.printStackTrace();
			}
*/
			
			//advancepayment
			/*List<AdvancePaymentDetailDO> advancepaymentList = advancePaymentDetailService.getByMonthAndEmpId(empCompensation.getEmployee().getEmpId(),month);
			if(advancepaymentList != null && advancepaymentList.size() > 0){
				deductionResultJSONArray.put(getAdvancePaymentObject(advancepaymentList,Double.parseDouble(activeCtc.toString()), month));
			}*/
			//loanpayment
			List<GeneralDeductionDetailDO> loanpaymentList = loanPaymentDetailService.getByMonthAndEmpId(empCompensation.getEmployee().getEmpId(),month);
			if(loanpaymentList != null && loanpaymentList.size() > 0){
				for(GeneralDeductionDetailDO loanPayment : loanpaymentList){
					if(loanPayment.getMonth() != null && loanPayment.getAmount() != null && Double.parseDouble(loanPayment.getAmount().toString()) >  0){
						deductionResultJSONArray.put(getLoanPaymentObject(loanPayment,grossAmount, month));
					}
				}
				//deductionResultJSONArray.put(getLoanPaymentObject(loanpaymentList,grossAmount, month));
			}
			
			//advance payment
			
			//loanpayment
			/*List<InsurancePaymentDetailDO> insurancepaymentList = insurancePaymentDetailService.getByMonthAndEmpId(empCompensation.getEmployee().getEmpId(),month);
			if(insurancepaymentList != null && insurancepaymentList.size() > 0){
				deductionResultJSONArray.put(getInsurancePaymentObject(insurancepaymentList,grossAmount, month));
			}*/
			
			result.put(CommonConstants.DEDUCTIONMONTHLY, String.valueOf(Math.round(deductionTotal)));
			result.put(CommonConstants.DEDUCTIONLIST, deductionResultJSONArray);
			taxTotal = 0.0;
			Boolean applyPt = false;
			Boolean applyEducationCess = false;
			Boolean applyTDS = false;
			
			JSONArray taxResultJSONArray = new JSONArray();
			
			for (PayrollGroupTaxDO taxDetails :  empCompensation.getPayrollGroup().getPayrollGroupTax()) {
				if(taxDetails.getStatus() == CommonConstants.ACTIVE && taxDetails.getPt() != null  && taxDetails.getPt().toString().equalsIgnoreCase(CommonConstants.APPLY))  applyPt = true; 
				if(taxDetails.getStatus() == CommonConstants.ACTIVE && taxDetails.getTds() != null  && taxDetails.getTds().toString().equalsIgnoreCase(CommonConstants.APPLY)) applyTDS = true;
				if(taxDetails.getStatus() == CommonConstants.ACTIVE && taxDetails.getEducationCess() != null  && taxDetails.getEducationCess().equalsIgnoreCase(CommonConstants.APPLY))  applyEducationCess = true;
				if(taxDetails.getStatus() == CommonConstants.ACTIVE && taxDetails.getPt() == null && taxDetails.getTds() == null  && taxDetails.getEducationCess()  == null){
					taxResultJSONArray.put(getCompensationTaxDetailInfoObject(taxDetails.getPayrollGroupTaxId(),taxDetails.getTaxSettings(), grossAmount));
				}
					
			}
			result.put(CommonConstants.BASICGROSS_PAY, String.valueOf(Math.round((activeCtc/12)+totalAllowance)));
			result.put(CommonConstants.BASIC, String.valueOf(Math.round(basicPay)));
			//result.put(CommonConstants.BASICGROSS_PAY, String.valueOf(activeCtc/12));
			//result.put(CommonConstants.BASIC, String.valueOf(basicPay));
		//	result.put(CommonConstants.TAXMONTHLY, String.valueOf(taxTotal));
		//	====result.put(CommonConstants.TAXLIST, taxResultJSONArray);
			
			result.put(CommonConstants.STATUS, String.valueOf(CommonConstants.NEW));
			result.put(CommonConstants.PAYROLLMONTH, month);
			bonus = 0.0;
			List<EmployeeBonusDO> bonusList = employeeBonusService.retrieveByEmpIdAndMonth(empCompensation.getEmployee().getEmpId(),month);
			if(bonusList != null && bonusList.size() > 0){
				for(EmployeeBonusDO bonuDO : bonusList){
					earningResultJSONArray.put(getBonuDetail(bonuDO));
				}
				if(applyTDS){
					if(applyPt) {
						taxResultJSONArray.put(getPTObject(empCompensation.getEmployee(),(earingTotal - deductionTotal)));
					}
					taxResultJSONArray.put(getTDSObject(empCompensation.getEmployee(), grossAmount, month));
					if(applyEducationCess){
						taxResultJSONArray.put(getEducationCessObject());
					}
					
							result.put(CommonConstants.TAXMONTHLY, String.valueOf(Math.round(taxTotal)));
					//result.put(CommonConstants.TAXMONTHLY, String.valueOf(CommonUtil.convertfloatValueWithTwoDecimalPoints(taxTotal)));
					result.put(CommonConstants.TAXLIST, taxResultJSONArray);
				}else{
					if(applyPt) {
						taxResultJSONArray.put(getPTObject(empCompensation.getEmployee(),(earingTotal - deductionTotal)));
					}
					if(applyEducationCess){
						taxResultJSONArray.put(getEducationCessObject());
					}
					result.put(CommonConstants.TAXMONTHLY, String.valueOf(Math.round(taxTotal)));
					//result.put(CommonConstants.TAXMONTHLY, String.valueOf(CommonUtil.convertfloatValueWithTwoDecimalPoints(taxTotal)));
					result.put(CommonConstants.TAXLIST, taxResultJSONArray);
				}
				result.put(CommonConstants.EARNINGLIST, earningResultJSONArray);
				Double mnthlyNet = earingTotal - ((deductionTotal) + taxTotal );
				//result.put(CommonConstants.NETMONTHLY, String.valueOf(CommonUtil.convertfloatValueWithTwoDecimalPoints(mnthlyNet+bonus)));
				//result.put(CommonConstants.EARNINGMONTHLY, String.valueOf(CommonUtil.convertfloatValueWithTwoDecimalPoints(earingTotal+bonus)));
				
				result.put(CommonConstants.NETMONTHLY, String.valueOf(Math.round(mnthlyNet+bonus)));
				result.put(CommonConstants.EARNINGMONTHLY, String.valueOf(Math.round(earingTotal+bonus)));
				
			}else{
				result.put(CommonConstants.EARNINGMONTHLY, String.valueOf(Math.round(earingTotal)));
				if(applyPt) {
					taxResultJSONArray.put(getPTObject(empCompensation.getEmployee(),(earingTotal - deductionTotal)));
				}
				taxResultJSONArray.put(getTDSObject(empCompensation.getEmployee(), grossAmount,month));
				if(applyEducationCess){
					taxResultJSONArray.put(getEducationCessObject());
				}
				result.put(CommonConstants.TAXMONTHLY, String.valueOf(Math.round(taxTotal)));
				//result.put(CommonConstants.TAXMONTHLY, String.valueOf(CommonUtil.convertfloatValueWithTwoDecimalPoints(taxTotal)));
				result.put(CommonConstants.TAXLIST, taxResultJSONArray);
				result.put(CommonConstants.EARNINGLIST, earningResultJSONArray);
				Double mnthlyNet = earingTotal - ((deductionTotal) + taxTotal );
				result.put(CommonConstants.NETMONTHLY, String.valueOf(Math.round(mnthlyNet)));
				//result.put(CommonConstants.NETMONTHLY, String.valueOf(CommonUtil.convertfloatValueWithTwoDecimalPoints(mnthlyNet)));
			}
			
		}else{
			
			earingTotal = 0.0;
			totalAllowance = 0.0;
			/*for (PayrollGroupEarningsDO earningDetails :  empCompensation.getPayrollGroup().getPayrollGroupEarning()) {
				if(earningDetails.getStatus() == CommonConstants.ACTIVE){
					
					if(earningDetails.getAllowanceSettings().getEarningType().getName().equalsIgnoreCase(CommonConstants.DEARNESSALLOWEANCE)){
						earningResultJSONArray.put(getDearnessAlloweanceDetails(month, earningDetails.getAllowanceSettings()));
					}else{
						if(earningDetails.getAllowanceSettings().getEarningType().getName().equalsIgnoreCase(CommonConstants.ADD_DA) || earningDetails.getAllowanceSettings().getEarningType().getName().equalsIgnoreCase(CommonConstants.ADDDEARNESSALLOWEANCE)){
							earningResultJSONArray.put(getADDDearnessAlloweanceDetails(month, earningDetails.getAllowanceSettings()));
						}
					
					}
				}
			}
*/			
			Calendar c = Calendar.getInstance();
			c.setTime(CommonUtil.convertMonthToSqlDate(month));
			Calendar endDate = Calendar.getInstance(); 
			int  year1 = c.get(Calendar.YEAR);
			 int  monthNumber= c.get(Calendar.MONTH);
			endDate.set(year1,monthNumber,c.getActualMaximum(Calendar.DAY_OF_MONTH)); 
			double attendanceCount = 0; 
			int pcount = 0;
			int count = 0;
			double actallyPcount = 0.0;
			List<AttendanceDO> attendenceList = attendanceService.retrieveByEmpId_Date(empCompensation.getEmployee().getEmpId(),c.getTime(),endDate.getTime());
			if(attendenceList != null && attendenceList.size() > 0){
				
				for(AttendanceDO attndeance : attendenceList){
					if(attndeance.getAttendanceType().equalsIgnoreCase("p")){
						attendanceCount += 1;
						pcount += 1;
						count+= 1;
						actallyPcount +=1;
						
					}
					if(attndeance.getAttendanceType().equalsIgnoreCase("cf")) {
						attendanceCount+=1;
						count+= 1;
					}
					if(attndeance.getAttendanceType().equalsIgnoreCase("ch")) {
						attendanceCount+=1;
						count+= 1;
						actallyPcount +=0.5;
					}
					if(attndeance.getAttendanceType().equalsIgnoreCase("lpf")) {
						attendanceCount+=1;
						count+= 1;
					}
					if(attndeance.getAttendanceType().equalsIgnoreCase("lph")){
						attendanceCount+=1;
						count+= 1;
						actallyPcount +=0.5;
					}
					if(attndeance.getAttendanceType().equalsIgnoreCase("apc")){
						attendanceCount+=1;
						count+= 1;
						actallyPcount +=0.5;
					}
					if(attndeance.getAttendanceType().equalsIgnoreCase("alc")){
						attendanceCount+=1;
						count+= 1;
						actallyPcount +=0.5;
					}
					if(attndeance.getAttendanceType().equalsIgnoreCase("h")){
						attendanceCount+=1;
						count+= 1;
						actallyPcount +=0.5;
					}
					/*if(attndeance.getAttendanceType().equalsIgnoreCase("lo")){
						attendanceCount+=0.5;
					}*/
					if(attndeance.getAttendanceType().equalsIgnoreCase("hlhc")){
						attendanceCount+=1;
						count+= 1;
					}
				}
				List<HolidaysDO> hoildayList = null;
				hoildayList = holidaysService.retrieveByDate(Integer.toString(year1), month.substring(0, 3));
				if(hoildayList != null && hoildayList.size() > 0){
					attendanceCount+=hoildayList.size();
				}
				
				for (PayrollGroupWorkingdaysAllowanceDO workingAllowanceDetails :  empCompensation.getPayrollGroup().getPayrollGroupWorkingdays()) {
					if(workingAllowanceDetails.getStatus() == CommonConstants.ACTIVE){
						earningResultJSONArray.put(getWorkingDayAllowanceCalculation(workingAllowanceDetails.getWorkingDayAllowance(),(noOfwrkingDays - leaveCount),Long.parseLong(companyCalenderList.get(0).getMinimunWorkingDays()),actallyPcount));
					}
				}
				if(Long.parseLong(companyCalenderList.get(0).getMinimunWorkingDays()) <= attendanceCount){
					for (PayrollGroupAttendanceAllowanceDO attendanceAllowanceDetails :  empCompensation.getPayrollGroup().getPayrollGroupAttendanceAllowance()) {
						if(attendanceAllowanceDetails.getStatus() == CommonConstants.ACTIVE){
							earningResultJSONArray.put(getAttendanceAllowanceCalculation(attendanceAllowanceDetails.getAttendanceAllowance(),(noOfwrkingDays - leaveCount),Long.parseLong(companyCalenderList.get(0).getMinimunWorkingDays())));
						}
					}
				}
				//List<EmployeeLopDO> lopList = employeeLopService.retrieveB(empCompensation.getEmployee().getEmpId(),c.getTime(),endDate.getTime());
				List<EmployeeLopDO> lopList = employeeLopService.retrieveByEmpIdDateStatus(empCompensation.getEmployee().getEmpId(), c.getTime(), endDate.getTime());
				Double lopCnt = 0.0;
				if(lopList != null && lopList.size()>0){
					for(EmployeeLopDO lop : lopList){
						Date dateBeforeString = lop.getStartdate();
						if(lop.getEnddate() != null){
							Calendar endLop = Calendar.getInstance();
							 Date dateAfterString = lop.getEnddate();
							 endLop.setTime(dateAfterString);
							  Calendar start = Calendar.getInstance();
							  start.setTime(dateBeforeString);
							 for (Date date = start.getTime(); start.before(endLop) || start.equals(endLop); start.add(Calendar.DATE, 1), date = start.getTime()) {
								Calendar weekend = Calendar.getInstance();
								weekend.setTime(date);
								if (/*weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && */
									weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){
									 //leaveCount +=1;
									if(lop.getType() != null && lop.getType().equalsIgnoreCase("h")){
										lopCnt +=0.5;
									}
								}
							 }
						}else{
							if(lop.getType() != null && lop.getType().equalsIgnoreCase("h")){
								lopCnt +=0.5;
							}							
						}
					}
				}
				
				result.put(CommonConstants.DAYS, String.valueOf(attendanceCount-lopCnt));
				result.put(CommonConstants.PRESENTDAYS, String.valueOf(actallyPcount));
				/*result.put(CommonConstants.DAYS, String.valueOf(count));
				result.put(CommonConstants.PRESENTDAYS, String.valueOf(pcount));*/
		
					
			}
			
			
			taxableIncome = 0.0;
			 Boolean Adjustment = false;
			  PayrollGroupEarningsDO adjustData = new PayrollGroupEarningsDO();
			for (PayrollGroupEarningsDO earningDetails :  empCompensation.getPayrollGroup().getPayrollGroupEarning()) {
				if(earningDetails.getStatus() == CommonConstants.ACTIVE){
					if(!earningDetails.getAllowanceSettings().getType().equalsIgnoreCase("A")){
						
						if(earningDetails.getAllowanceSettings().getEarningType().getName().equalsIgnoreCase(CommonConstants.DEARNESSALLOWEANCE) && companyCalenderList != null && companyCalenderList.get(0).getNoOfSalaryDays() != null && empCompensation.getEmployee().getUnitOrBranch() != null ){
							earningResultJSONArray.put(getDearnessAlloweanceDetails(month, earningDetails.getAllowanceSettings(),Long.parseLong(companyCalenderList.get(0).getNoOfSalaryDays()),(noOfwrkingDays - leaveCount),empCompensation.getEmployee().getUnitOrBranch().getUnitOrBranchId()));
						}else if(earningDetails.getAllowanceSettings().getEarningType().getName().equalsIgnoreCase(CommonConstants.ADD_DA) || earningDetails.getAllowanceSettings().getEarningType().getName().equalsIgnoreCase(CommonConstants.ADDDEARNESSALLOWEANCE) && empCompensation.getEmployee().getUnitOrBranch() != null){
							earningResultJSONArray.put(getADDDearnessAlloweanceDetails(month, earningDetails.getAllowanceSettings(),empCompensation.getEmployee().getUnitOrBranch().getUnitOrBranchId(),empCompensation.getEmployee().getEmpId(),empCtc.getPayrollBatch().getPayrollBatchId()));
						}else{
							earningResultJSONArray.put(getCompensationAllowenceDetailInfoObject(earningDetails.getPayrollGroupEarningsId(),earningDetails.getAllowanceSettings(), Double.parseDouble(activeCtc.toString()),(noOfwrkingDays - leaveCount),salaryDays));
						}
					}else{
						Adjustment = true;
						if(earningDetails.getAllowanceSettings().getEarningType().getName().equalsIgnoreCase(CommonConstants.DEARNESSALLOWEANCE) && companyCalenderList != null && companyCalenderList.get(0).getNoOfSalaryDays() != null && empCompensation.getEmployee().getUnitOrBranch() != null ){
							earningResultJSONArray.put(getDearnessAlloweanceDetails(month, earningDetails.getAllowanceSettings(),Long.parseLong(companyCalenderList.get(0).getNoOfSalaryDays()),(noOfwrkingDays - leaveCount),empCompensation.getEmployee().getUnitOrBranch().getUnitOrBranchId()));
						}else if(earningDetails.getAllowanceSettings().getEarningType().getName().equalsIgnoreCase(CommonConstants.ADD_DA) || earningDetails.getAllowanceSettings().getEarningType().getName().equalsIgnoreCase(CommonConstants.ADDDEARNESSALLOWEANCE) && empCompensation.getEmployee().getUnitOrBranch() != null){
							earningResultJSONArray.put(getADDDearnessAlloweanceDetails(month, earningDetails.getAllowanceSettings(),empCompensation.getEmployee().getUnitOrBranch().getUnitOrBranchId(),empCompensation.getEmployee().getEmpId(),empCtc.getPayrollBatch().getPayrollBatchId()));
						}else{
							adjustData = earningDetails;
						}
						
					}
				}
			}
			if(Adjustment){
				earningResultJSONArray.put(getAdustmentDetails(adjustData.getPayrollGroupEarningsId(),adjustData.getAllowanceSettings(), Double.parseDouble(activeCtc.toString())));
			}
			List<GeneralEarningDO> generalEarningList = generalEarningService.retrieveActiveByEmpIdAndMonth(empCompensation.getEmployee().getEmpId(), month);
			if(generalEarningList != null && generalEarningList.size() > 0){
				for(GeneralEarningDO generalEarning: generalEarningList){
					earningResultJSONArray.put(getGeneralEarningObject(generalEarning));
				}
			}
			
			
				List<EmployeeExpenseDO>  empExpenseList = employeeExpenseService.retrieveByMonthAndEmpId(empCompensation.getEmployee().getEmpId(), c.getTime(), endDate.getTime(), CommonConstants.ACTIVESTRING);
			if(empExpenseList != null && empExpenseList.size() > 0){
				earningResultJSONArray.put(getEmployeeExpenseDetail(empExpenseList));
			}
			
			deductionTotal = 0.0;
			JSONArray deductionResultJSONArray1 = new JSONArray();
			for (PayrollGroupDeductionDO deductionDetails :  empCompensation.getPayrollGroup().getPayrollGroupDeduction()) {
				if(deductionDetails.getStatus() == CommonConstants.ACTIVE){
					if(deductionDetails.getDeductionSettings().getDeductionType().getName().equalsIgnoreCase(CommonConstants.SUBDEARNESSALLOWEANCE)){
						deductionResultJSONArray1.put(getSubDADetails(month, deductionDetails.getDeductionSettings(),empCompensation.getEmployee().getUnitOrBranch().getUnitOrBranchId(),empCtc.getPayrollBatch().getPayrollBatchId(),empCompensation.getEmployee().getEmpId()));
					}else{
						deductionResultJSONArray1.put(getCompensationDeductionDetailInfoObject(deductionDetails.getPayrollGroupDeductionId(),deductionDetails.getDeductionSettings(), Double.parseDouble(activeCtc.toString())));
					}
				}
					
			}
			List<GeneralDeductionDetailDO> loanpaymentList = loanPaymentDetailService.getByMonthAndEmpId(empCompensation.getEmployee().getEmpId(),month);
			if(loanpaymentList != null && loanpaymentList.size() > 0){
				for(GeneralDeductionDetailDO loanPayment : loanpaymentList){
					if(loanPayment.getMonth() != null && loanPayment.getAmount() != null && Double.parseDouble(loanPayment.getAmount().toString()) >  0){
						deductionResultJSONArray1.put(getLoanPaymentObject(loanPayment,grossAmount, month));
					}
				}
			}
			
			List <EmployeePFInfoDO> pfDetail = empPFService.retrieveByEmpIdAndDate(empCompensation.getEmployee().getEmpId(), c.getTime(), endDate.getTime());
			if(pfDetail != null && pfDetail.size() > 0 ){
				deductionResultJSONArray1.put(getPFCalualation(pfDetail.get(0),pfDetail.get(0).getPfGroup(), grossAmount,(noOfwrkingDays - leaveCount),salaryDays));
			}
			//System.out.println("empCompensation.getEmployee().getEmpId()"+empCompensation.getEmployee().getEmpId());
			
			List <EmployeeESIInfoDO> esiDetail = empESIService.retrieveByEmpIdAndDate(empCompensation.getEmployee().getEmpId(), c.getTime(), endDate.getTime());
			if(esiDetail != null && esiDetail.size() > 0 ){
				deductionResultJSONArray1.put(getESICalualation(esiDetail.get(0),esiDetail.get(0).getEsiGroup(), grossAmount,(noOfwrkingDays - leaveCount),salaryDays));
			}
			
			result.put(CommonConstants.DEDUCTIONMONTHLY, String.valueOf(CommonUtil.convertfloatValueWithTwoDecimalPoints(deductionTotal)));
			result.put(CommonConstants.DEDUCTIONLIST, deductionResultJSONArray1);
			
			taxTotal = 0.0;
			Boolean applyPt = false;
			Boolean applyEducationCess = false;
			Boolean applyTDS = false;
			JSONArray taxResultJSONArray = new JSONArray();
			for (PayrollGroupTaxDO taxDetails :  empCompensation.getPayrollGroup().getPayrollGroupTax()) {
				if(taxDetails.getStatus() == CommonConstants.ACTIVE && taxDetails.getPt() != null  && taxDetails.getPt().toString().equalsIgnoreCase(CommonConstants.APPLY))  applyPt = true; 
				if(taxDetails.getStatus() == CommonConstants.ACTIVE && taxDetails.getTds() != null  && taxDetails.getTds().toString().equalsIgnoreCase(CommonConstants.APPLY)) applyTDS = true;//taxResultJSONArray.put(getTDSObject(empCompensation.getEmployee(), Double.parseDouble(activeCtc.toString())));
				if(taxDetails.getStatus() == CommonConstants.ACTIVE && taxDetails.getEducationCess() != null  && taxDetails.getEducationCess().equalsIgnoreCase(CommonConstants.APPLY))  applyEducationCess = true;
				if(taxDetails.getStatus() == CommonConstants.ACTIVE && taxDetails.getPt() == null && taxDetails.getTds() == null  && taxDetails.getEducationCess()  == null){
					taxResultJSONArray.put(getCompensationTaxDetailInfoObject(taxDetails.getPayrollGroupTaxId(),taxDetails.getTaxSettings(), Double.parseDouble(empCompensation.getEmpCtc().getEmpctc().toString())));
				}
			}
			
			if(applyTDS){
				if(applyPt) {
					taxResultJSONArray.put(getPTObject(empCompensation.getEmployee(),(earingTotal - deductionTotal)));
				}
				taxResultJSONArray.put(getTDSObject(empCompensation.getEmployee(), Double.parseDouble(activeCtc.toString()),""));
				if(applyEducationCess){
					taxResultJSONArray.put(getEducationCessObject());
				}
				result.put(CommonConstants.TAXMONTHLY, String.valueOf(CommonUtil.convertfloatValueWithTwoDecimalPoints(taxTotal)));
				result.put(CommonConstants.TAXLIST, taxResultJSONArray);
			}else{
				if(applyPt) {
					taxResultJSONArray.put(getPTObject(empCompensation.getEmployee(),(earingTotal - deductionTotal)));
				}
				if(applyEducationCess){
					taxResultJSONArray.put(getEducationCessObject());
				}
				result.put(CommonConstants.TAXMONTHLY, String.valueOf(CommonUtil.convertfloatValueWithTwoDecimalPoints(taxTotal)));
				result.put(CommonConstants.TAXLIST, taxResultJSONArray);
			}
			Double mnthlyNet = 0.0;
			result.put(CommonConstants.BASICGROSS_PAY, String.valueOf(Math.round((activeCtc/12))+totalAllowance));
			result.put(CommonConstants.EARNINGMONTHLY, String.valueOf(Math.round(earingTotal)));
			result.put(CommonConstants.EARNINGLIST, earningResultJSONArray);
			
			/*deductionTotal = 0.0;
			taxTotal = 0.0;
			for (PayrollGroupDeductionDO deductionDetails :  empCompensation.getPayrollGroup().getPayrollGroupDeduction()) {
				if(deductionDetails.getStatus() == CommonConstants.ACTIVE){
					if(deductionDetails.getDeductionSettings().getDeductionType().getName().equalsIgnoreCase(CommonConstants.SUBDEARNESSALLOWEANCE)){
						deductionResultJSONArray.put(getSubDADetails(month, deductionDetails.getDeductionSettings()));
					}
				}
					
			}
			
			result.put(CommonConstants.DEDUCTIONMONTHLY, String.valueOf(Math.round(deductionTotal)));
			result.put(CommonConstants.DEDUCTIONLIST, deductionResultJSONArray);
			result.put(CommonConstants.TAXMONTHLY, String.valueOf(0));
			Double mnthlyNet = 0.0;
			result.put(CommonConstants.EARNINGMONTHLY, String.valueOf(Math.round(earingTotal)));
			result.put(CommonConstants.EARNINGLIST, earningResultJSONArray);*/
			 mnthlyNet = earingTotal - ((deductionTotal) + taxTotal );
			 result.put(CommonConstants.STATUS, String.valueOf(CommonConstants.NEW));
			 result.put(CommonConstants.NETMONTHLY, String.valueOf(Math.round(mnthlyNet)));
			if(mnthlyNet == 0.0){
				result.put(CommonConstants.ERROR_CODE, String.valueOf("Basic not available"));
			}
			
		}
		
		return result;
	}
	
	
	
	public static JSONObject getBonuDetail(EmployeeBonusDO bonuInfo)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.MONTHLY, String.valueOf(Math.round(bonuInfo.getAmount())));
		//result.put(CommonConstants.MONTHLY, String.valueOf(CommonUtil.convertfloatValueWithTwoDecimalPoints(bonuInfo.getAmount())));
		//result.put(CommonConstants.YTD, String.valueOf(ytd));
		result.put(CommonConstants.NAME, String.valueOf("Bonus"));
		result.put(CommonConstants.BONUSID, String.valueOf(bonuInfo.getEmpBonusId()));
		bonus += bonuInfo.getAmount();
		taxableIncome += bonus*12;
		return result;
	}
	
	public static JSONObject getEmpAvailbleComponsationDeatilsObject(EmployeeCompensationDO empCompensation)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(empCompensation.getEmployeeCompensationId()));
		result.put(CommonConstants.EMPID, String.valueOf(empCompensation.getEmployee().getEmpId()));
		/*if(empCompensation.getEmployee().getMiddlename() != null) result.put(CommonConstants.EMPNAME, String.valueOf(empCompensation.getEmployee().getFirstname() + " " +empCompensation.getEmployee().getMiddlename() + " " + empCompensation.getEmployee().getLastname()));
		else result.put(CommonConstants.EMPNAME, String.valueOf(empCompensation.getEmployee().getFirstname() + " " + empCompensation.getEmployee().getLastname()));*/
		
		if(empCompensation.getEmployee().getMiddlename() != null) result.put(CommonConstants.EMPNAME, String.valueOf(empCompensation.getEmployee().getFirstname() + " " +empCompensation.getEmployee().getMiddlename() + " " + empCompensation.getEmployee().getLastname()));
		else if(empCompensation.getEmployee().getMiddlename() != null && empCompensation.getEmployee().getLastname() == null ) result.put(CommonConstants.EMPNAME, String.valueOf(empCompensation.getEmployee().getFirstname() + " " +empCompensation.getEmployee().getMiddlename()));
		else if(empCompensation.getEmployee().getMiddlename() == null && empCompensation.getEmployee().getLastname() == null ) result.put(CommonConstants.EMPNAME,String.valueOf(empCompensation.getEmployee().getFirstname()));
		else result.put(CommonConstants.EMPNAME, String.valueOf(empCompensation.getEmployee().getFirstname() + " " + empCompensation.getEmployee().getLastname()));
		
		List<EmployeeCtcDO> ctcList = employeeCtcService.retrieveActive(empCompensation.getEmployee().getEmpId());
		if(ctcList != null && ctcList.size() > 0){
			result.put(CommonConstants.EMPCTC, String.valueOf(ctcList.get(0).getEmpctc()));
			result.put(CommonConstants.EMPCTCID, String.valueOf(ctcList.get(0).getEmpCtcId()));
			result.put(CommonConstants.STARTDATE, String.valueOf(ctcList.get(0).getStartdate()));
			if(ctcList.get(0).getEnddate() != null) result.put(CommonConstants.ENDDATE, String.valueOf(ctcList.get(0).getEnddate()));
			else result.put(CommonConstants.ENDDATE, String.valueOf(""));
			
			Long activeCtc = 0L;
			activeCtc = ctcList.get(0).getEmpctc();

			basicPay = 0.0;
			
			
			for (PayrollGroupEarningsDO earningDetails :  empCompensation.getPayrollGroup().getPayrollGroupEarning()) {
				if(earningDetails.getStatus() == CommonConstants.ACTIVE && earningDetails.getAllowanceSettings().getEarningType().getName().equalsIgnoreCase(CommonConstants.BASIC)){
					if(earningDetails.getAllowanceSettings().getEarningType().getStatus() == CommonConstants.ACTIVE){
						if(earningDetails.getAllowanceSettings().getPercentageamount() != null){
							basicPay = ((Double.parseDouble(empCompensation.getEmpCtc().getEmpctc().toString())/12) * Double.parseDouble(earningDetails.getAllowanceSettings().getPercentageamount()))/100;
						}
						
					}
				}
					
			}
			
			if(basicPay != 0){
				JSONArray earningResultJSONArray = new JSONArray();
				earingTotal = 0.0;
				taxableIncome = 0.0;
				Boolean applyPt = false;
				Boolean applyEducationCess = false;
				Boolean Adjustment = false;
				Boolean applyTDS = false;
				PayrollGroupEarningsDO adjustData = new PayrollGroupEarningsDO();
				for (PayrollGroupEarningsDO earningDetails :  empCompensation.getPayrollGroup().getPayrollGroupEarning()) {
					if(earningDetails.getStatus() == CommonConstants.ACTIVE){
						if(!earningDetails.getAllowanceSettings().getType().equalsIgnoreCase("A")){
							earningResultJSONArray.put(getCompensationAllowenceDetailInfoObject(earningDetails.getPayrollGroupEarningsId(),earningDetails.getAllowanceSettings(), Double.parseDouble(activeCtc.toString()),0,0));
						}else{
							Adjustment = true;
							adjustData = earningDetails;
						}
					}
				}
				if(Adjustment){
					earningResultJSONArray.put(getAdustmentDetails(adjustData.getPayrollGroupEarningsId(),adjustData.getAllowanceSettings(), Double.parseDouble(activeCtc.toString())));
				}
				
				result.put(CommonConstants.EARNINGMONTHLY, String.valueOf(Math.round(earingTotal)));
				//result.put(CommonConstants.EARNINGMONTHLY, String.valueOf(CommonUtil.convertfloatValueWithTwoDecimalPoints(earingTotal)));
				result.put(CommonConstants.EARNINGLIST, earningResultJSONArray);
				
				deductionTotal = 0.0;
				JSONArray deductionResultJSONArray = new JSONArray();
				for (PayrollGroupDeductionDO deductionDetails :  empCompensation.getPayrollGroup().getPayrollGroupDeduction()) {
					if(deductionDetails.getStatus() == CommonConstants.ACTIVE){
						deductionResultJSONArray.put(getCompensationDeductionDetailInfoObject(deductionDetails.getPayrollGroupDeductionId(),deductionDetails.getDeductionSettings(), Double.parseDouble(activeCtc.toString())));
					}
						
				}
				result.put(CommonConstants.DEDUCTIONMONTHLY, String.valueOf(Math.round(deductionTotal)));
				//result.put(CommonConstants.DEDUCTIONMONTHLY, String.valueOf(CommonUtil.convertfloatValueWithTwoDecimalPoints(deductionTotal)));
				result.put(CommonConstants.DEDUCTIONLIST, deductionResultJSONArray);
				taxTotal = 0.0;
				JSONArray taxResultJSONArray = new JSONArray();
				for (PayrollGroupTaxDO taxDetails :  empCompensation.getPayrollGroup().getPayrollGroupTax()) {
					if(taxDetails.getStatus() == CommonConstants.ACTIVE && taxDetails.getPt() != null  && taxDetails.getPt().toString().equalsIgnoreCase(CommonConstants.APPLY))  applyPt = true; 
					if(taxDetails.getStatus() == CommonConstants.ACTIVE && taxDetails.getTds() != null  && taxDetails.getTds().toString().equalsIgnoreCase(CommonConstants.APPLY)) applyTDS = true;//taxResultJSONArray.put(getTDSObject(empCompensation.getEmployee(), Double.parseDouble(activeCtc.toString())));
					if(taxDetails.getStatus() == CommonConstants.ACTIVE && taxDetails.getEducationCess() != null  && taxDetails.getEducationCess().equalsIgnoreCase(CommonConstants.APPLY))  applyEducationCess = true;
					if(taxDetails.getStatus() == CommonConstants.ACTIVE && taxDetails.getPt() == null && taxDetails.getTds() == null  && taxDetails.getEducationCess()  == null){
						taxResultJSONArray.put(getCompensationTaxDetailInfoObject(taxDetails.getPayrollGroupTaxId(),taxDetails.getTaxSettings(), Double.parseDouble(empCompensation.getEmpCtc().getEmpctc().toString())));
					}
						
				}
				result.put(CommonConstants.BASICGROSS_PAY, String.valueOf(Math.round(activeCtc/12)));
				result.put(CommonConstants.BASIC, String.valueOf(Math.round(basicPay)));
				//result.put(CommonConstants.TAXMONTHLY, String.valueOf(taxTotal));
				//result.put(CommonConstants.TAXLIST, taxResultJSONArray);
				/*if(applyPt) {
					taxResultJSONArray.put(getPTObject(empCompensation.getEmployee(),earingTotal - (deductionTotal + taxTotal )));
					if(applyEducationCess){
						taxResultJSONArray.put(getEducationCessObject());
					}
					result.put(CommonConstants.TAXMONTHLY, String.valueOf(taxTotal));
					result.put(CommonConstants.TAXLIST, taxResultJSONArray);
				}else{
					result.put(CommonConstants.TAXMONTHLY, String.valueOf(taxTotal));
					result.put(CommonConstants.TAXLIST, taxResultJSONArray);
				}
			*/
				
				if(applyTDS){
					if(applyPt) {
						taxResultJSONArray.put(getPTObject(empCompensation.getEmployee(),(earingTotal - deductionTotal)));
					}
					taxResultJSONArray.put(getTDSObject(empCompensation.getEmployee(), Double.parseDouble(activeCtc.toString()),""));
					if(applyEducationCess){
						taxResultJSONArray.put(getEducationCessObject());
					}
					
							result.put(CommonConstants.TAXMONTHLY, String.valueOf(Math.round(taxTotal)));
					//result.put(CommonConstants.TAXMONTHLY, String.valueOf(CommonUtil.convertfloatValueWithTwoDecimalPoints(taxTotal)));
					result.put(CommonConstants.TAXLIST, taxResultJSONArray);
				}else{
					if(applyPt) {
						taxResultJSONArray.put(getPTObject(empCompensation.getEmployee(),(earingTotal - deductionTotal)));
					}
					result.put(CommonConstants.TAXMONTHLY, String.valueOf(Math.round(taxTotal)));
					//result.put(CommonConstants.TAXMONTHLY, String.valueOf(CommonUtil.convertfloatValueWithTwoDecimalPoints(taxTotal)));
					result.put(CommonConstants.TAXLIST, taxResultJSONArray);
				}
				
				Double mnthlyNet = earingTotal - ((deductionTotal) + taxTotal );
				result.put(CommonConstants.NETMONTHLY, String.valueOf(Math.round(mnthlyNet)));
				//result.put(CommonConstants.NETMONTHLY, String.valueOf(CommonUtil.convertfloatValueWithTwoDecimalPoints(mnthlyNet)));
				
			}else{
				JSONArray earningResultJSONArray = new JSONArray();
				earingTotal = 0.0;
				taxableIncome = 0.0;
				Boolean applyPt = false;
				Boolean applyEducationCess = false;
				Boolean Adjustment = false;
				Boolean applyTDS = false;
				PayrollGroupEarningsDO adjustData = new PayrollGroupEarningsDO();
				for (PayrollGroupEarningsDO earningDetails :  empCompensation.getPayrollGroup().getPayrollGroupEarning()) {
					if(earningDetails.getStatus() == CommonConstants.ACTIVE){
						if(!earningDetails.getAllowanceSettings().getType().equalsIgnoreCase("A")){
							earningResultJSONArray.put(getCompensationAllowenceDetailInfoObject(earningDetails.getPayrollGroupEarningsId(),earningDetails.getAllowanceSettings(), Double.parseDouble(activeCtc.toString()),0,0));
						}else{
							Adjustment = true;
							adjustData = earningDetails;
						}
					}
				}
				if(Adjustment){
					earningResultJSONArray.put(getAdustmentDetails(adjustData.getPayrollGroupEarningsId(),adjustData.getAllowanceSettings(), Double.parseDouble(activeCtc.toString())));
				}
				
				result.put(CommonConstants.EARNINGMONTHLY, String.valueOf(Math.round(earingTotal)));
				//result.put(CommonConstants.EARNINGMONTHLY, String.valueOf(CommonUtil.convertfloatValueWithTwoDecimalPoints(earingTotal)));
				result.put(CommonConstants.EARNINGLIST, earningResultJSONArray);
				
				deductionTotal = 0.0;
				JSONArray deductionResultJSONArray = new JSONArray();
				for (PayrollGroupDeductionDO deductionDetails :  empCompensation.getPayrollGroup().getPayrollGroupDeduction()) {
					if(deductionDetails.getStatus() == CommonConstants.ACTIVE){
						deductionResultJSONArray.put(getCompensationDeductionDetailInfoObject(deductionDetails.getPayrollGroupDeductionId(),deductionDetails.getDeductionSettings(), Double.parseDouble(activeCtc.toString())));
					}
						
				}
				result.put(CommonConstants.DEDUCTIONMONTHLY, String.valueOf(Math.round(deductionTotal)));
				//result.put(CommonConstants.DEDUCTIONMONTHLY, String.valueOf(CommonUtil.convertfloatValueWithTwoDecimalPoints(deductionTotal)));
				result.put(CommonConstants.DEDUCTIONLIST, deductionResultJSONArray);
				taxTotal = 0.0;
				JSONArray taxResultJSONArray = new JSONArray();
				for (PayrollGroupTaxDO taxDetails :  empCompensation.getPayrollGroup().getPayrollGroupTax()) {
					if(taxDetails.getStatus() == CommonConstants.ACTIVE && taxDetails.getPt() != null  && taxDetails.getPt().toString().equalsIgnoreCase(CommonConstants.APPLY))  applyPt = true; 
					if(taxDetails.getStatus() == CommonConstants.ACTIVE && taxDetails.getTds() != null  && taxDetails.getTds().toString().equalsIgnoreCase(CommonConstants.APPLY)) applyTDS = true;//taxResultJSONArray.put(getTDSObject(empCompensation.getEmployee(), Double.parseDouble(activeCtc.toString())));
					if(taxDetails.getStatus() == CommonConstants.ACTIVE && taxDetails.getEducationCess() != null  && taxDetails.getEducationCess().equalsIgnoreCase(CommonConstants.APPLY))  applyEducationCess = true;
					if(taxDetails.getStatus() == CommonConstants.ACTIVE && taxDetails.getPt() == null && taxDetails.getTds() == null  && taxDetails.getEducationCess()  == null){
						taxResultJSONArray.put(getCompensationTaxDetailInfoObject(taxDetails.getPayrollGroupTaxId(),taxDetails.getTaxSettings(), Double.parseDouble(empCompensation.getEmpCtc().getEmpctc().toString())));
					}
						
				}
				//result.put(CommonConstants.BASICGROSS_PAY, String.valueOf(Math.round(activeCtc/12)));
				//result.put(CommonConstants.BASIC, String.valueOf(Math.round(basicPay)));
				//result.put(CommonConstants.TAXMONTHLY, String.valueOf(taxTotal));
				//result.put(CommonConstants.TAXLIST, taxResultJSONArray);
				/*if(applyPt) {
					taxResultJSONArray.put(getPTObject(empCompensation.getEmployee(),earingTotal - (deductionTotal + taxTotal )));
					if(applyEducationCess){
						taxResultJSONArray.put(getEducationCessObject());
					}
					result.put(CommonConstants.TAXMONTHLY, String.valueOf(taxTotal));
					result.put(CommonConstants.TAXLIST, taxResultJSONArray);
				}else{
					result.put(CommonConstants.TAXMONTHLY, String.valueOf(taxTotal));
					result.put(CommonConstants.TAXLIST, taxResultJSONArray);
				}
			*/
				
				if(applyTDS){
					if(applyPt) {
						taxResultJSONArray.put(getPTObject(empCompensation.getEmployee(),(earingTotal - deductionTotal)));
					}
					taxResultJSONArray.put(getTDSObject(empCompensation.getEmployee(), Double.parseDouble(activeCtc.toString()),""));
					if(applyEducationCess){
						taxResultJSONArray.put(getEducationCessObject());
					}
					
							result.put(CommonConstants.TAXMONTHLY, String.valueOf(Math.round(taxTotal)));
					//result.put(CommonConstants.TAXMONTHLY, String.valueOf(CommonUtil.convertfloatValueWithTwoDecimalPoints(taxTotal)));
					result.put(CommonConstants.TAXLIST, taxResultJSONArray);
				}else{
					if(applyPt) {
						taxResultJSONArray.put(getPTObject(empCompensation.getEmployee(),(earingTotal - deductionTotal)));
					}
					result.put(CommonConstants.TAXMONTHLY, String.valueOf(Math.round(taxTotal)));
					//result.put(CommonConstants.TAXMONTHLY, String.valueOf(CommonUtil.convertfloatValueWithTwoDecimalPoints(taxTotal)));
					result.put(CommonConstants.TAXLIST, taxResultJSONArray);
				}
				
				Double mnthlyNet = earingTotal - ((deductionTotal) + taxTotal );
				result.put(CommonConstants.NETMONTHLY, String.valueOf(Math.round(mnthlyNet)));
				//result.put(CommonConstants.NETMONTHLY, String.valueOf(CommonUtil.convertfloatValueWithTwoDecimalPoints(mnthlyNet)));
				
			}
			
			
		}else{
			result.put(CommonConstants.ERROR_CODE, String.valueOf("Active Ctc Not Available"));
		}
		result.put(CommonConstants.PAYROLLGROUPID, String.valueOf(empCompensation.getPayrollGroup().getPayrollGroupId()));
		if(empCompensation.getPayrollGroup().getUnitOrBranch() != null)	result.put(CommonConstants.UNITNAME, String.valueOf(empCompensation.getPayrollGroup().getUnitOrBranch().getUnitOrBranch()));
		result.put(CommonConstants.PAYROLLGROUPNAME, String.valueOf(empCompensation.getPayrollGroup().getName()));
		result.put(CommonConstants.EFFECTIVEFROM, String.valueOf(empCompensation.getEffectivefrom()));
		result.put(CommonConstants.EFFECTIVETO, String.valueOf(empCompensation.getEffectiveto()));
		return result;
	}
	/*public static JSONObject getempCompensationList(List<EmployeeCompensationDO> empCompensationList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeeCompensationDO empCompensation : empCompensationList) {
				List<EmployeeDO> employeeList = employeeService.isEmpActive(empCompensation.getEmpId());
				if(employeeList != null && employeeList.size() > 0){
					if(employeeList.get(0).getStatus() == 'a'){
						resultJSONArray.put(getCompensationDetailObject(empCompensation, null));
					}
				}
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getempCompensationListWithEarningList(List<EmployeeCompensationDO> empCompensationList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeeCompensationDO empCompensation : empCompensationList) {
				List<EmployeeDO> employeeList = employeeService.isEmpActive(empCompensation.getEmployee().getEmpId());
				if(employeeList != null && employeeList.size() > 0){
					if(employeeList.get(0).getStatus() == 'a'){
						resultJSONArray.put(getCompensationDetailObjectWithEarnings(empCompensation, null));
					}
				}
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getempCompensationListWithBatch(List<EmployeeCompensationDO> empCompensationList, Long batchId) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeeCompensationDO empCompensation : empCompensationList) {
				List<EmployeeDO> employeeList = employeeService.isEmpActive(empCompensation.getEmpId());
				if(employeeList != null && employeeList.size() > 0){
					if(employeeList.get(0).getStatus() == 'a'){
						List<EmployeeCtcDO> compensationList = employeeCtcService.retrieveActive(empCompensation.getEmpId().toString());
						if(compensationList != null && compensationList.size() > 0){
							if(compensationList.get(0).getPayrollBatch().getPayrollBatchId() != null && compensationList.get(0).getPayrollBatch().getPayrollBatchId().equals(batchId)){
								resultJSONArray.put(getCompensationDetailObject(empCompensation, null));
							}
						}
					}
				}
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getempCompensationListWithBatch(List<EmployeeCompensationDO> empCompensationList, Long batchId, String payMonth) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			Map<Long, Long> empIds = new HashMap<Long,Long>();  
			for (EmployeeCompensationDO empCompensation : empCompensationList) {
				if(CommonUtil.convertDateToStringWithOutDate(payMonth).after(CommonUtil.convertDateToStringWithOutDate(CommonUtil.convertDateToStringWithOutDate(empCompensation.getEffectivefrom())))
						|| CommonUtil.convertDateToStringWithOutDate(payMonth).equals(CommonUtil.convertDateToStringWithOutDate(CommonUtil.convertDateToStringWithOutDate(empCompensation.getEffectivefrom())))){
					if(empCompensation.getEffectiveto() == null || CommonUtil.convertDateToStringWithOutDate(payMonth).before(CommonUtil.convertDateToStringWithOutDate(CommonUtil.convertDateToStringWithOutDate(empCompensation.getEffectiveto())))
							|| CommonUtil.convertDateToStringWithOutDate(payMonth).equals(CommonUtil.convertDateToStringWithOutDate(CommonUtil.convertDateToStringWithOutDate(empCompensation.getEffectiveto())))){
						List<EmployeeDO> employeeList = employeeService.isEmpActive(empCompensation.getEmpId());
						if(employeeList != null && employeeList.size() > 0){
							if(employeeList.get(0).getStatus() == 'a'){
								List<EmployeeCtcDO> compensationList = employeeCtcService.retrieveActive(empCompensation.getEmpId().toString());
								if(compensationList != null && compensationList.size() > 0){
									System.out.println("payMonth"+CommonUtil.convertDateToStringWithOutDate(payMonth));
									System.out.println("startdate"+CommonUtil.convertDateToStringWithOutDate(CommonUtil.convertDateToStringWithOutDate(compensationList.get(0).getStartdate())));
									
									if(compensationList.get(0).getStartdate() != null && CommonUtil.convertDateToStringWithOutDate(payMonth).after(CommonUtil.convertDateToStringWithOutDate(CommonUtil.convertDateToStringWithOutDate(compensationList.get(0).getStartdate())))
											|| CommonUtil.convertDateToStringWithOutDate(payMonth).equals(CommonUtil.convertDateToStringWithOutDate(CommonUtil.convertDateToStringWithOutDate(compensationList.get(0).getStartdate())))){
										if(compensationList.get(0).getEnddate() == null || CommonUtil.convertDateToStringWithOutDate(payMonth).before(CommonUtil.convertDateToStringWithOutDate(CommonUtil.convertDateToStringWithOutDate(compensationList.get(0).getEnddate())))){
											if(compensationList.get(0).getPayrollBatch().getPayrollBatchId() != null && compensationList.get(0).getPayrollBatch().getPayrollBatchId().equals(batchId)){
												if(!empIds.containsKey(empCompensation.getEmpId())){
													int i = 0;
													for (EmployeeCompensationDO tempList : empCompensationList) {
														if(empCompensation.getEmpId().equals(tempList.getEmpId())){
															i++;
														}
													}
													
													if(i > 1){
														for (EmployeeCompensationDO tempList : empCompensationList) {
															if(empCompensation.getEmpId().equals(tempList.getEmpId()) && tempList.getStatus() == 'a'){
																empIds.put(empCompensation.getEmpId(), empCompensation.getEmpId());
																resultJSONArray.put(getCompensationDetailObject(empCompensation,payMonth));
															}
														}
													}else{
														empIds.put(empCompensation.getEmpId(), empCompensation.getEmpId());
														resultJSONArray.put(getCompensationDetailObject(empCompensation,payMonth));
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
*/
	/*public static JSONObject getCompensationDetailObject(EmployeeCompensationDO empCompensation , String payrollMonth)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(empCompensation.getId()));
		result.put(CommonConstants.EMPID, String.valueOf(empCompensation.getEmpId()));
		result.put(CommonConstants.STARTDATE,String.valueOf(empCompensation.getEffectivefrom() != null ? CommonUtil.convertDateToStringWithdatetime(empCompensation.getEffectivefrom()) : ""));
		result.put(CommonConstants.ENDDATE,String.valueOf(empCompensation.getEffectiveto() != null ? CommonUtil.convertDateToStringWithdatetime(empCompensation.getEffectiveto()) : ""));
		result.put(CommonConstants.STATUS, String.valueOf(empCompensation.getStatus()));
		List<EmployeePayrollDO> payrollList = employeePayrollService.retriveByMonthAndEmp(payrollMonth, empCompensation.getEmpId());
		if(payrollList.size() > 0){
			result.put(CommonConstants.PAYROLLSTATUS, String.valueOf(payrollList.get(0).getStatus()));
		}else{
			result.put(CommonConstants.PAYROLLSTATUS, String.valueOf(CommonConstants.NEW));
		}
		
		Long bonusAmountMonth = 0L;
		if(payrollMonth != null){
			List<EmployeeBonusDO> employeeBonusList = employeeBonusService.retrieveByEmpIdAndMonth(empCompensation.getEmpId().toString(), payrollMonth);
			if(employeeBonusList != null && employeeBonusList.size() > 0){
				bonusAmountMonth = employeeBonusList.get(0).getAmount();
			}
		}
		//bonusAmountMonth = (long) (bonusAmountMonth + empCompensation.getNetmonth());
		int noOfDaysLop = 0;
		if(payrollMonth != null){
			List<EmployeeLopDO> employeeLopList = employeeLopService.retrieveByEmpId(empCompensation.getEmpId().toString());
			if(employeeLopList != null && employeeLopList.size() > 0){
				Calendar start = Calendar.getInstance();
				start.setTime(employeeLopList.get(0).getStartdate());
				Calendar end = Calendar.getInstance();
				end.setTime(employeeLopList.get(0).getEnddate());
				
				for (Date date = start.getTime(); start.before(end) || start.equals(end); start.add(Calendar.DATE, 1), date = start.getTime()) {
					Calendar weekend = Calendar.getInstance();
					weekend.setTime(date);
					if (weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && 
						weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){
						if(payrollMonth.equalsIgnoreCase(CommonUtil.convertDateToStringWithOutDate(weekend.getTime()))){
							noOfDaysLop++;
						}
					}
				}
			}
		}
		
		Double lossOfPayAmount = 0D;
		if(payrollMonth != null){
			List<EmployeeCtcDO> ctcList = employeeCtcService.retrieveActive(empCompensation.getEmpId().toString());
			if(ctcList != null && ctcList.size() > 0){
				result.put(CommonConstants.EMPCTC, String.valueOf(ctcList.get(0).getEmpctc()));
				Long ctc = ctcList.get(0).getEmpctc()/12;
				Date s = CommonUtil.convertDateToStringWithOutDate(payrollMonth);
		        Calendar c=Calendar.getInstance();
		        c.setTime(s);
		        int days = c.getActualMaximum(Calendar.DAY_OF_MONTH);
				Long perDay = ctc/days;
				if(noOfDaysLop > 0){
					lossOfPayAmount = (double) (noOfDaysLop * perDay);
				}
				result.put(CommonConstants.BACSICPAYLABEL, String.valueOf(ctc/2));
			}else{
				result.put(CommonConstants.EMPCTC, "");
				result.put(CommonConstants.BACSICPAYLABEL, "");
			}
		}else{
			result.put(CommonConstants.EMPCTC, "");
			result.put(CommonConstants.BACSICPAYLABEL, "");
		}
		
		result.put(CommonConstants.LOSSOFPAYMONTHLY,  String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(lossOfPayAmount))));
		result.put(CommonConstants.LOSSOFPAYYEAR,  String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(lossOfPayAmount * 12))));
		
		
		if(empCompensation.getEmpId() != null && payrollMonth != null){
			List<EmployeeDO> employee = employeeService.retrieveEmpId(empCompensation.getEmpId().toString());
			if(employee != null && employee.size() > 0){
				if(employee.get(0).getStartdate() != null){
					String splitMonth = employee.get(0).getStartdate().substring(0, 10);
					String joiningMonth = CommonUtil.convertDateToStringWithOutDate(CommonUtil.convertStringToSqlDate(splitMonth));
					//String[] joinMonthArray = joiningMonth.split(",");
					if(joiningMonth.equalsIgnoreCase(payrollMonth)){
						Date joiningDate = CommonUtil.convertStringToSqlDate(employee.get(0).getStartdate());
						Calendar c=Calendar.getInstance();
				        c.setTime(joiningDate);
				        int days = c.getActualMaximum(Calendar.DAY_OF_MONTH);
				        String[] noofdays = splitMonth.split("-");
				        int totalDays = days;
				        if(!noofdays[2].equals("01")){
				        	 totalDays = days - Integer.parseInt(noofdays[2]);
				        }
				        Double perdaySalary = empCompensation.getEarningmonth()/days;
				        result.put(CommonConstants.EARNINGMONTHLY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf((perdaySalary * totalDays) + bonusAmountMonth))));
				        Double perdayTax = 0D;
				        if(empCompensation.getTaxmonth() != null && empCompensation.getTaxmonth() > 0){
				        	perdayTax = empCompensation.getTaxmonth()/days;
				        	result.put(CommonConstants.TAXMONTHLY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(perdayTax * totalDays))));
				        }else{
				        	result.put(CommonConstants.TAXMONTHLY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(perdayTax * totalDays))));
				        }
				        Double perdayDeduction = 0D;
				        if(empCompensation.getDeductionmonth() != null && empCompensation.getDeductionmonth() > 0){
				        	perdayDeduction = empCompensation.getDeductionmonth()/days;
				        	result.put(CommonConstants.DEDUCTIONMONTHLY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf((perdayDeduction * totalDays) + lossOfPayAmount))));
				        }else{
				        	result.put(CommonConstants.DEDUCTIONMONTHLY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf((perdayDeduction * totalDays) + lossOfPayAmount))));
				        }
				        Double netmonthly = 0D;
				        netmonthly = Double.parseDouble(result.get(CommonConstants.EARNINGMONTHLY).toString()) - (Double.parseDouble(result.get(CommonConstants.TAXMONTHLY).toString())
				        		+ Double.parseDouble(result.get(CommonConstants.DEDUCTIONMONTHLY).toString()));
						result.put(CommonConstants.NETMONTHLY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(netmonthly))));
						
					}else{
						result.put(CommonConstants.EARNINGMONTHLY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(empCompensation.getEarningmonth() + bonusAmountMonth))));
						result.put(CommonConstants.TAXMONTHLY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(empCompensation.getTaxmonth()))));
						result.put(CommonConstants.DEDUCTIONMONTHLY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(empCompensation.getDeductionmonth() + lossOfPayAmount))));
						Double netmonthly = 0D;
				        netmonthly = Double.parseDouble(result.get(CommonConstants.EARNINGMONTHLY).toString()) - (Double.parseDouble(result.get(CommonConstants.TAXMONTHLY).toString())
				        		+ Double.parseDouble(result.get(CommonConstants.DEDUCTIONMONTHLY).toString()));
						result.put(CommonConstants.NETMONTHLY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(netmonthly))));
					}
				}else{
					result.put(CommonConstants.EARNINGMONTHLY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(empCompensation.getEarningmonth() + bonusAmountMonth))));
					result.put(CommonConstants.TAXMONTHLY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(empCompensation.getTaxmonth()))));
					result.put(CommonConstants.DEDUCTIONMONTHLY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(empCompensation.getDeductionmonth() + lossOfPayAmount))));
					Double netmonthly = 0D;
			        netmonthly = Double.parseDouble(result.get(CommonConstants.EARNINGMONTHLY).toString()) - (Double.parseDouble(result.get(CommonConstants.TAXMONTHLY).toString())
			        		+ Double.parseDouble(result.get(CommonConstants.DEDUCTIONMONTHLY).toString()));
					result.put(CommonConstants.NETMONTHLY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(netmonthly))));
				}
				
			}else{
				result.put(CommonConstants.EARNINGMONTHLY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(empCompensation.getEarningmonth() + bonusAmountMonth))));
				result.put(CommonConstants.TAXMONTHLY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(empCompensation.getTaxmonth()))));
				result.put(CommonConstants.DEDUCTIONMONTHLY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(empCompensation.getDeductionmonth() + lossOfPayAmount))));
				Double netmonthly = 0D;
		        netmonthly = Double.parseDouble(result.get(CommonConstants.EARNINGMONTHLY).toString()) - (Double.parseDouble(result.get(CommonConstants.TAXMONTHLY).toString())
		        		+ Double.parseDouble(result.get(CommonConstants.DEDUCTIONMONTHLY).toString()));
				result.put(CommonConstants.NETMONTHLY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(netmonthly))));
			}
		}else{
			result.put(CommonConstants.EARNINGMONTHLY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(empCompensation.getEarningmonth()))));
			result.put(CommonConstants.TAXMONTHLY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(empCompensation.getTaxmonth()))));
			result.put(CommonConstants.DEDUCTIONMONTHLY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(empCompensation.getDeductionmonth()))));
			Double netmonthly = 0D;
	        netmonthly = Double.parseDouble(result.get(CommonConstants.EARNINGMONTHLY).toString()) - (Double.parseDouble(result.get(CommonConstants.TAXMONTHLY).toString())
	        		+ Double.parseDouble(result.get(CommonConstants.DEDUCTIONMONTHLY).toString()));
			result.put(CommonConstants.NETMONTHLY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(netmonthly))));
		}
		
		result.put(CommonConstants.EARNINGYEAR, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(empCompensation.getEarningyear()))));
		result.put(CommonConstants.NETYEAR, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(empCompensation.getNetyear()))));
		result.put(CommonConstants.TAXYEAR, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(empCompensation.getTaxyear()))));
		result.put(CommonConstants.BASICGROSS_PAY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(empCompensation.getGrossPay()))));
		result.put(CommonConstants.DEDUCTIONYEAR, String.valueOf(empCompensation.getDeductionyear() != null ? CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(empCompensation.getDeductionyear())) : 0));
		if(empCompensation.getEmpId() != null){
			List<EmployeeDO> employeeList = employeeService.retrieveEmpId(empCompensation.getEmpId().toString());
			if(employeeList != null && employeeList.size() > 0){
				if(employeeList.get(0).getMiddlename() != null) result.put(CommonConstants.NAME, String.valueOf(employeeList.get(0).getFirstname() + " " +employeeList.get(0).getMiddlename() + " " + employeeList.get(0).getLastname()));
				else result.put(CommonConstants.NAME, String.valueOf(employeeList.get(0).getFirstname() + " " + employeeList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.NAME, "");
			}
		}else{
			result.put(CommonConstants.NAME, "");
		}
		if(empCompensation.getUpdatedBy() != null){
			List<EmployeeDO> empList = employeeService.retrieveEmpId(empCompensation.getUpdatedBy().toString());
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(empCompensation.getUpdatedon())));
		return result;
	}
	
	public static JSONObject getCompensationDetailObjectWithEarnings(EmployeeCompensationDO empCompensation , String payrollMonth)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(empCompensation.getId()));
		result.put(CommonConstants.EMPID, String.valueOf(empCompensation.getEmpId()));
		result.put(CommonConstants.STARTDATE,String.valueOf(empCompensation.getEffectivefrom() != null ? CommonUtil.convertDateToStringWithdatetime(empCompensation.getEffectivefrom()) : ""));
		result.put(CommonConstants.ENDDATE,String.valueOf(empCompensation.getEffectiveto() != null ? CommonUtil.convertDateToStringWithdatetime(empCompensation.getEffectiveto()) : ""));
		result.put(CommonConstants.STATUS, String.valueOf(empCompensation.getStatus()));
		Long bonusAmountMonth = 0L;
		if(payrollMonth != null){
			List<EmployeeBonusDO> employeeBonusList = employeeBonusService.retrieveByEmpIdAndMonth(empCompensation.getEmpId().toString(), payrollMonth);
			if(employeeBonusList != null && employeeBonusList.size() > 0){
				bonusAmountMonth = employeeBonusList.get(0).getAmount();
			}
		}
		
		bonusAmountMonth = (long) (bonusAmountMonth + empCompensation.getNetmonth());
		int noOfDaysLop = 0;
		if(payrollMonth != null){
			List<EmployeeLopDO> employeeLopList = employeeLopService.retrieveByEmpId(empCompensation.getEmpId().toString());
			if(employeeLopList != null && employeeLopList.size() > 0){
				Calendar start = Calendar.getInstance();
				start.setTime(employeeLopList.get(0).getStartdate());
				Calendar end = Calendar.getInstance();
				end.setTime(employeeLopList.get(0).getEnddate());
				
				for (Date date = start.getTime(); start.before(end) || start.equals(end); start.add(Calendar.DATE, 1), date = start.getTime()) {
					Calendar weekend = Calendar.getInstance();
					weekend.setTime(date);
					if (weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && 
						weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){
						if(payrollMonth.equalsIgnoreCase(CommonUtil.convertDateToStringWithOutDate(weekend.getTime()))){
							noOfDaysLop++;
						}
					}
				}
			}
		}
		
		
		result.put(CommonConstants.NETYEAR, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(empCompensation.getNetyear()))));
		result.put(CommonConstants.EARNINGMONTHLY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(empCompensation.getEarningmonth()))));
		result.put(CommonConstants.EARNINGYEAR, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(empCompensation.getEarningyear()))));
		
		result.put(CommonConstants.TAXMONTHLY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(empCompensation.getTaxmonth()))));
		result.put(CommonConstants.TAXYEAR, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(empCompensation.getTaxyear()))));
		result.put(CommonConstants.BASICGROSS_PAY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(empCompensation.getGrossPay()))));
		Double lossOfPayAmount = 0D;
		if(payrollMonth != null){
			List<EmployeeCtcDO> ctcList = employeeCtcService.retrieveActive(empCompensation.getEmpId().toString());
			if(ctcList != null && ctcList.size() > 0){
				result.put(CommonConstants.EMPCTC, String.valueOf(ctcList.get(0).getEmpctc()));
				Long ctc = ctcList.get(0).getEmpctc()/12;
				Date s = CommonUtil.convertDateToStringWithOutDate(payrollMonth);
		        Calendar c=Calendar.getInstance();
		        c.setTime(s);
		        int days = c.getActualMaximum(Calendar.DAY_OF_MONTH);
				Long perDay = ctc/days;
				if(noOfDaysLop > 0){
					lossOfPayAmount = (double) (noOfDaysLop * perDay);
				}
				result.put(CommonConstants.BACSICPAYLABEL, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(ctc/2))));
			}else{
				result.put(CommonConstants.EMPCTC, "");
				result.put(CommonConstants.BACSICPAYLABEL, "");
			}
		}else{
			result.put(CommonConstants.EMPCTC, "");
			result.put(CommonConstants.BACSICPAYLABEL, "");
		}
		
		result.put(CommonConstants.LOSSOFPAYMONTHLY,  String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(lossOfPayAmount))));
		result.put(CommonConstants.LOSSOFPAYYEAR,  String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(lossOfPayAmount * 12))));
		
		result.put(CommonConstants.DEDUCTIONMONTHLY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(empCompensation.getDeductionmonth() != null ?  empCompensation.getDeductionmonth()+ lossOfPayAmount : lossOfPayAmount))));
		result.put(CommonConstants.NETMONTHLY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(bonusAmountMonth  - lossOfPayAmount))));
		result.put(CommonConstants.DEDUCTIONYEAR, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(empCompensation.getDeductionyear() != null ? empCompensation.getDeductionyear() : 0))));
		List<EmployeeEarningsDO> earningsList = employeeEarningsService.retrieveByEmpCompensationId(empCompensation.getId());
		List<EmployeeTaxDO> taxList = employeeTaxService.retrieveByEmpCompensationId(empCompensation.getId());
		List<EmployeeDeductionDO> deductionList = employeeDeductionService.retrieveByEmpCompensationId(empCompensation.getId());
		JSONArray resultJSONArray = new JSONArray();
		
		if(earningsList != null && earningsList.size() > 0){
			for (EmployeeEarningsDO empEarnings : earningsList) {
				resultJSONArray.put(getEmployeeEarningsDetailObject(empEarnings));
			}
			result.put(CommonConstants.EARNINGLIST, resultJSONArray);
		}else{
			result.put(CommonConstants.EARNINGLIST,"");
		}
		JSONArray resultJSONArray1 = new JSONArray();
		
		if(deductionList != null && deductionList.size() > 0){
			for (EmployeeDeductionDO empDeduction : deductionList) {
				resultJSONArray1.put(getEmployeeDeductionDetailObject(empDeduction));
			}
			result.put(CommonConstants.DEDUCTIONLIST, resultJSONArray1);
		}else{
			result.put(CommonConstants.DEDUCTIONLIST,"");
		}
		
		JSONArray resultJSONArray2 = new JSONArray();
		
		if(taxList != null && taxList.size() > 0){
			for (EmployeeTaxDO empDeduction : taxList) {
				resultJSONArray2.put(getEmployeeTaxDetailObject(empDeduction));
			}
			result.put(CommonConstants.TAXLIST, resultJSONArray2);
		}else{
			result.put(CommonConstants.TAXLIST,"");
		}
		if(empCompensation.getEmpId() != null){
			List<EmployeeDO> employeeList = employeeService.retrieveEmpId(empCompensation.getEmpId().toString());
			if(employeeList != null && employeeList.size() > 0){
				if(employeeList.get(0).getMiddlename() != null) result.put(CommonConstants.NAME, String.valueOf(employeeList.get(0).getFirstname() + " " +employeeList.get(0).getMiddlename() + " " + employeeList.get(0).getLastname()));
				else result.put(CommonConstants.NAME, String.valueOf(employeeList.get(0).getFirstname() + " " + employeeList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.NAME, "");
			}
		}else{
			result.put(CommonConstants.NAME, "");
		}
		if(empCompensation.getUpdatedBy() != null){
			List<EmployeeDO> empList = employeeService.retrieveEmpId(empCompensation.getUpdatedBy().toString());
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(empCompensation.getUpdatedon())));
		return result;
	}*/
	
	/*public static JSONObject getEmployeeEarningsDetailObject(EmployeeEarningsDO empCompensation)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.EARNINGID, String.valueOf(empCompensation.getEarningsId()));
		List<AllowanceSettingDO> allowanceList = allowanceSettingsService.retrieveById(empCompensation.getEarningsId());
		if(allowanceList != null && allowanceList.size() > 0){
			result.put(CommonConstants.EARNINGNAME, String.valueOf(allowanceList.get(0).getDisplayname()));
		}else{
			result.put(CommonConstants.EARNINGNAME, "");
		}
		result.put(CommonConstants.MONTHLY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(empCompensation.getMonthly()))));
		result.put(CommonConstants.YTD, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(empCompensation.getYtd()))));
		return result;
	}
	
	public static JSONObject getEmployeeDeductionDetailObject(EmployeeDeductionDO empCompensation)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.DEDUCTIONID, String.valueOf(empCompensation.getEmpdeductionid()));
		List<DeductionSettingDO> deductionList = deductionSettingsService.retrieveById(empCompensation.getEmpdeductionid());
		if(deductionList != null && deductionList.size() > 0){
			result.put(CommonConstants.DEDUCTIONNAME, String.valueOf(deductionList.get(0).getDisplayname()));
		}else{
			result.put(CommonConstants.DEDUCTIONNAME, "");
		}
		result.put(CommonConstants.MONTHLY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(empCompensation.getMonthly()))));
		result.put(CommonConstants.YTD, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(empCompensation.getYtd()))));
		return result;
	}
	
	public static JSONObject getEmployeeTaxDetailObject(EmployeeTaxDO empCompensation)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.TAXID, String.valueOf(empCompensation.getEmptaxid()));
		List<TaxSettingsDO> deductionList = taxSettingsService.retrieveById(empCompensation.getEmptaxid());
		if(deductionList != null && deductionList.size() > 0){
			result.put(CommonConstants.TAXNAME, String.valueOf(deductionList.get(0).getDisplayname()));
		}else{
			result.put(CommonConstants.TAXNAME, "");
		}
		result.put(CommonConstants.MONTHLY, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(empCompensation.getMonthly()))));
		result.put(CommonConstants.YTD, String.valueOf(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(empCompensation.getYtd()))));
		return result;
	}*/
	
	public static JSONObject getempWeeklyPayrollDetails(List<EmployeeCtcDO> empCtcList, String month, Date endDate, int daysdiff, Date d1, Date d2) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			
			
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
			String strFromDate= formatter.format(d1);
			
			SimpleDateFormat formatter2 = new SimpleDateFormat("dd-MM-yyyy");
			String strToDate= formatter2.format(d2);
			
			
			for (EmployeeCtcDO empCtc : empCtcList) {
				//List<EmployeePayrollDO> payrollList = employeePayrollService.retriveByMonthAndEmp(month, empCtc.getEmployee().getEmpId());
				List<EmployeePayrollDO> payrollList = employeePayrollService.retriveByWeekAndEmp(strFromDate, strToDate,empCtc.getEmployee().getEmpId());
				if(payrollList == null || payrollList.size() <= 0 || payrollList.get(0).getStatus() == 'v'){
					List<EmployeeCompensationDO> empCompensationList = employeeCompensationService.retrieveWithComponsesationDateAndEmpID(empCtc.getEmployee().getEmpId().toString(), endDate);
					if(empCompensationList != null && empCompensationList.size() >0){
						resultJSONArray.put(getempWeeklyPayrollDetailsObject(empCompensationList.get(0), month, empCtc,daysdiff,d1,d2));
					}
				}else{
					resultJSONArray.put(getempPayrollDetailsObject1(payrollList.get(0), month, empCtc));
					
				}
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getempWeeklyPayrollDetailsObject(EmployeeCompensationDO empCompensation, String month, EmployeeCtcDO empCtc, int daysdiff, Date d1, Date d2)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(empCompensation.getEmployeeCompensationId()));
		result.put(CommonConstants.EMPID, String.valueOf(empCompensation.getEmployee().getEmpId()));
		if(empCompensation.getEmployee().getDesignation() != null) result.put(CommonConstants.DESIGNATION, String.valueOf(empCompensation.getEmployee().getDesignation().getName()));
		else result.put(CommonConstants.DESIGNATION, String.valueOf(""));
		result.put(CommonConstants.PAYROLLWEEKFROM, String.valueOf(CommonUtil.convertDateToStringWithOutTime(d1)));
		result.put(CommonConstants.PAYROLLWEEKTO, String.valueOf(CommonUtil.convertDateToStringWithOutTime(d2)));
		if(empCompensation.getEmployee().getMiddlename() != null) result.put(CommonConstants.EMPNAME, String.valueOf(empCompensation.getEmployee().getFirstname() + " " +empCompensation.getEmployee().getMiddlename() + " " + empCompensation.getEmployee().getLastname()));
		else if(empCompensation.getEmployee().getMiddlename() != null && empCompensation.getEmployee().getLastname() == null ) result.put(CommonConstants.EMPNAME, String.valueOf(empCompensation.getEmployee().getFirstname() + " " +empCompensation.getEmployee().getMiddlename()));
		else if(empCompensation.getEmployee().getMiddlename() == null && empCompensation.getEmployee().getLastname() == null ) result.put(CommonConstants.EMPNAME,String.valueOf(empCompensation.getEmployee().getFirstname()));
		else result.put(CommonConstants.EMPNAME, String.valueOf(empCompensation.getEmployee().getFirstname() + " " + empCompensation.getEmployee().getLastname()));
		int noOfwrkingDays = 0;
		int salaryDays = 0;
		if(empCtc.getPayrollBatch() != null){
			List<CompanyCalenderDO> companyCalenderList = companyCalenderService.retrieveByMonth(month,empCtc.getPayrollBatch().getPayrollBatchId());
			if(companyCalenderList != null && companyCalenderList.size() > 0){
				if(companyCalenderList.get(0).getMinimunWorkingDays() != null && !companyCalenderList.get(0).getMinimunWorkingDays().toString().isEmpty()){
					noOfwrkingDays = Integer.parseInt(companyCalenderList.get(0).getMinimunWorkingDays().toString());
				}
				if(companyCalenderList.get(0).getNoOfSalaryDays() != null && !companyCalenderList.get(0).getNoOfSalaryDays().toString().isEmpty()){
					salaryDays = Integer.parseInt(companyCalenderList.get(0).getNoOfSalaryDays().toString());
				}
			}
		}
		//Long activeCtc = empCtc.getEmpctc();
		
		
		
		String startDate = CommonUtil.convertDateToStringWithOutDate(empCtc.getStartdate());
		if(startDate.equalsIgnoreCase(month)){
			if(empCtc.getStartdate() != null){
				Date joinDate = empCtc.getStartdate();
				if(empCtc.getEnddate() == null && empCtc.getStartdate() != null && d1.before(empCtc.getStartdate()) && (d2.after(empCtc.getStartdate()) || d2.equals(empCtc.getStartdate()))){
					long diff = d2.getTime() - empCtc.getStartdate().getTime();
					long diffDays = diff / (24 * 60 * 60 * 1000) + 1;
					daysdiff = (int) diffDays;
				}
				if(empCtc.getEnddate() != null && empCtc.getStartdate() != null && d1.after(empCtc.getStartdate()) && d2.after(empCtc.getEnddate())){
					long diff =   empCtc.getEnddate().getTime() - d1.getTime();
					long diffDays = diff / (24 * 60 * 60 * 1000) + 1;
					daysdiff = (int) diffDays;
				}
			}
		}
		double activeCtc = empCtc.getEmpctc();
		double perdaySalary = 0.0;
        if(empCtc.getCtcType() != null && noOfwrkingDays > 0 && salaryDays > 0){
        	 if(empCtc.getCtcType().equalsIgnoreCase("y"))  perdaySalary = (double) (empCtc.getEmpctc()/12)/salaryDays;
		     if(empCtc.getCtcType().equalsIgnoreCase("m"))  perdaySalary = (double) empCtc.getEmpctc()/salaryDays;
		     if(empCtc.getCtcType().equalsIgnoreCase("d"))  perdaySalary = (double) empCtc.getEmpctc();
		   activeCtc =  ((perdaySalary * noOfwrkingDays)*12);
        }else{
        	activeCtc =  empCtc.getEmpctc();
        }
			/*String startDate = CommonUtil.convertDateToStringWithOutDate(empCtc.getStartdate());
			System.out.println("startDate"+startDate);
			String endDate = null;
			if(empCtc.getEnddate() != null)endDate = CommonUtil.convertDateToStringWithOutDate(empCtc.getEnddate());
			if(startDate.equalsIgnoreCase(month)){
				Calendar c=Calendar.getInstance();
		        c.setTime(empCtc.getStartdate());
		        int days = 0;
		     // int days = c.getActualMaximum(Calendar.DAY_OF_MONTH);
		        if(noOfwrkingDays == 0){
		        	  days = c.getActualMaximum(Calendar.DAY_OF_MONTH); 
			    }else{
			    	days = noOfwrkingDays;
			    }
		        
		        String[] noofdays = CommonUtil.convertDateToYearWithOutTime(empCtc.getStartdate()).split("-");
		        int totalDays = days;
		        if (endDate != null && endDate.equalsIgnoreCase(month)){
		        	 String[] noofenddays = CommonUtil.convertDateToYearWithOutTime(empCtc.getEnddate()).split("-");
		        	 totalDays =(int)( Long.parseLong( noofenddays[2]) - Long.parseLong(noofdays[2])+1);
		        }else{
		        	if(!noofdays[2].equals("01")){
			        	 totalDays = (days - Integer.parseInt(noofdays[2]))+1;
			        }
		        }
		        double monthctc = empCtc.getEmpctc()/12;
		        Double perdaySalary = (monthctc/days);
		        activeCtc = (long) ((perdaySalary * totalDays)*12);
			}else if (endDate != null && endDate.equalsIgnoreCase(month)){
				Calendar c=Calendar.getInstance();
		        c.setTime(empCtc.getEnddate());
		       // int days = c.getActualMaximum(Calendar.DAY_OF_MONTH);
		        int days = 0;
		        if(noOfwrkingDays == 0){
		        	  days = c.getActualMaximum(Calendar.DAY_OF_MONTH); 
			    }else{
			    	days = noOfwrkingDays;
			    }
		        int totalDays = days;
		        String[] noofenddays = CommonUtil.convertDateToYearWithOutTime(empCtc.getStartdate()).split("-");
		        if(startDate.equalsIgnoreCase(month)){
		        	  String[] noofStartdays = CommonUtil.convertDateToYearWithOutTime(empCtc.getStartdate()).split("-");
		        	  totalDays =(int)( Long.parseLong( noofenddays[2]) - Long.parseLong(noofStartdays[2])+1);
		        }else{
		        	totalDays = (int)(Long.parseLong( noofenddays[2]));
		        }
		        double monthctc = empCtc.getEmpctc()/12;
		        Double perdaySalary = (monthctc/days);
		        activeCtc = (long) ((perdaySalary * totalDays)*12);
			}else{
				activeCtc = empCtc.getEmpctc();
			}*/
		
		result.put(CommonConstants.EMPCTC, String.valueOf(empCtc.getEmpctc()));
		result.put(CommonConstants.EMPCTCID, String.valueOf(empCtc.getEmpCtcId()));
		result.put(CommonConstants.PAYROLLBATCHID, String.valueOf(empCtc.getPayrollBatch().getPayrollBatchId()));
		
		result.put(CommonConstants.PAYROLLGROUPID, String.valueOf(empCompensation.getPayrollGroup().getPayrollGroupId()));
		result.put(CommonConstants.PAYROLLGROUPNAME, String.valueOf(empCompensation.getPayrollGroup().getName()));
		
		basicPay = 0.0;
		
		for (PayrollGroupEarningsDO earningDetails :  empCompensation.getPayrollGroup().getPayrollGroupEarning()) {
			if(earningDetails.getStatus() == CommonConstants.ACTIVE && earningDetails.getAllowanceSettings().getEarningType().getName().equalsIgnoreCase(CommonConstants.BASIC)){
				if(earningDetails.getAllowanceSettings().getEarningType().getStatus() == CommonConstants.ACTIVE){
					basicPay = ((Double.parseDouble(empCompensation.getEmpCtc().getEmpctc().toString())/12) * Double.parseDouble(earningDetails.getAllowanceSettings().getPercentageamount()))/100;
				}
			}
		}
		JSONArray earningResultJSONArray = new JSONArray();
		if(basicPay != 0){
			earingTotal = 0.0;
			taxableIncome = 0.0;
			Boolean Adjustment = false;
			PayrollGroupEarningsDO adjustData = new PayrollGroupEarningsDO();
			for (PayrollGroupEarningsDO earningDetails :  empCompensation.getPayrollGroup().getPayrollGroupEarning()) {
				if(earningDetails.getStatus() == CommonConstants.ACTIVE){
					if(!earningDetails.getAllowanceSettings().getType().equalsIgnoreCase("A")){
						earningResultJSONArray.put(getCompensationWeeklyAllowenceDetailInfoObject(earningDetails.getPayrollGroupEarningsId(),earningDetails.getAllowanceSettings(), activeCtc, daysdiff,noOfwrkingDays));
					}else{
						Adjustment = true;
						adjustData = earningDetails;
					}
				}
			}
			if(Adjustment){
				earningResultJSONArray.put(getWeeklyAdustmentDetails(adjustData.getPayrollGroupEarningsId(),adjustData.getAllowanceSettings(), activeCtc,daysdiff,noOfwrkingDays));
			}
			
			deductionTotal = 0.0;
			JSONArray deductionResultJSONArray = new JSONArray();
			for (PayrollGroupDeductionDO deductionDetails :  empCompensation.getPayrollGroup().getPayrollGroupDeduction()) {
				if(deductionDetails.getStatus() == CommonConstants.ACTIVE){
						deductionResultJSONArray.put(getCompensationWeeklyDeductionDetailInfoObject(deductionDetails.getPayrollGroupDeductionId(),deductionDetails.getDeductionSettings(), activeCtc,daysdiff,noOfwrkingDays));
				}
			}
			
			
			
			List<EmployeeLopDO> lopList = employeeLopService.retrieveByEmpIdDateStatus(empCompensation.getEmployee().getEmpId(), d1, d2);
			if(lopList != null && lopList.size()>0){
				
				double lopleave = 0.0;
				
				deductionResultJSONArray.put(getWeeklyLOPDetailsObject(lopList,activeCtc,noOfwrkingDays,lopleave));
				/*Date joiningDate = satrtDate.getTime();
				Calendar c=Calendar.getInstance();
		        c.setTime(joiningDate);
		        int totalWorkingDays = 0;
		        if(noOfwrkingDays == 0){
		        	totalWorkingDays = c.getActualMaximum(Calendar.DAY_OF_MONTH); 
			    }else{
			    	totalWorkingDays = noOfwrkingDays;
			    }
		        
				deductionResultJSONArray.put(getLOPDetailsObject(lopList,Double.parseDouble(activeCtc.toString()),totalWorkingDays,lopleave));*/
		    }
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			/*int year = (int) Long.parseLong(month.substring(month.length() - 4));
			String selectedMonth = month.substring(0,3);
			try{   
		        Date date = new SimpleDateFormat("MMM").parse(selectedMonth);
		        Calendar cal = Calendar.getInstance();
		        cal.setTime(date);
		        int  monthNumber=cal.get(Calendar.MONTH);
		        Calendar satrtDate = Calendar.getInstance();    
				satrtDate.set(year,monthNumber,1); 
				satrtDate.set(Calendar.DAY_OF_MONTH, satrtDate.getActualMinimum(Calendar.DAY_OF_MONTH));
				Calendar endDate = Calendar.getInstance(); 
				endDate.set(year,monthNumber,1); 
				endDate.set(Calendar.DAY_OF_MONTH, endDate.getActualMaximum(Calendar.DAY_OF_MONTH));
				double lopleave = 0.0;
				double leaveTaken = 0.0;
				
				
				    Calendar calendarStart=Calendar.getInstance();
				    calendarStart.set(Calendar.YEAR,year);
				    calendarStart.set(Calendar.MONTH,0);
				    calendarStart.set(Calendar.DAY_OF_MONTH,1);
				    // returning the first date
				    Date yearStartDate=calendarStart.getTime();

				    Calendar calendarEnd=Calendar.getInstance();
				    calendarEnd.set(Calendar.YEAR,year);
				    calendarEnd.set(Calendar.MONTH,11);
				    calendarEnd.set(Calendar.DAY_OF_MONTH,31);

				    // returning the last date
				    Date yearEndDate=calendarEnd.getTime();
				    long noOfDays = 0;
				List <EmployeeLeavegroupDO> empLeavegrpList = employeeLeavegroupService.retrieveByEmpId(empCompensation.getEmployee().getEmpId().toString());
				if(empLeavegrpList != null && empLeavegrpList.size() > 0){
					if(empLeavegrpList.get(0).getLeaveGroup() != null && empLeavegrpList.get(0).getLeaveGroup().getLeaveGroupId() != null){
						
						List<LeaveGroupSettingsDO> leaveGroupList = leaveGroupSettingsService.retrieveByGroupIdWithStatus(empLeavegrpList.get(0).getLeaveGroup().getLeaveGroupId());
						if(leaveGroupList != null && leaveGroupList.size() > 0){
							for (LeaveGroupSettingsDO leaveType : leaveGroupList) {
								noOfDays += leaveType.getLeaveManagement().getNumberOfDays();
							}
						}
					}
					List <LeaveRequestsDO> appliedleaveRequestsList = leaveRequestsService.retrieveLeavesByFromToDate(empCompensation.getEmployee().getEmpId(),yearStartDate,yearEndDate);
					if(appliedleaveRequestsList != null && appliedleaveRequestsList.size() >0){
						 leaveTaken = getLeaveDetails(appliedleaveRequestsList, Double.parseDouble(activeCtc.toString()));
					}
					if(leaveTaken > noOfDays){
						lopleave = leaveTaken-noOfDays;
					}
				}
				
				
				List <LeaveRequestsDO> leaveRequestList = leaveRequestsService.retrieveLeavesForPayroll(empCompensation.getEmployee().getEmpId(),satrtDate.getTime(), endDate.getTime());
				if(leaveRequestList != null && leaveRequestList.size() >0){
					 lopleave += getLeaveLOPDetails(leaveRequestList, Double.parseDouble(activeCtc.toString()));
				}
				List<EmployeeLopDO> lopList = employeeLopService.retrieveByEmpIdDateStatus(empCompensation.getEmployee().getEmpId(), satrtDate.getTime(), endDate.getTime());
				if(lopList != null && lopList.size()>0){
					Date joiningDate = satrtDate.getTime();
					Calendar c=Calendar.getInstance();
			        c.setTime(joiningDate);
			       // int totalWorkingDays = c.getActualMaximum(Calendar.DAY_OF_MONTH);
			        int totalWorkingDays = 0;
			        if(noOfwrkingDays == 0){
			        	totalWorkingDays = c.getActualMaximum(Calendar.DAY_OF_MONTH); 
				    }else{
				    	totalWorkingDays = noOfwrkingDays;
				    }
			        
					deductionResultJSONArray.put(getLOPDetailsObject(lopList,Double.parseDouble(activeCtc.toString()),totalWorkingDays,lopleave));
			    }else{
			    	if(lopleave >= 1){
			    		Date joiningDate = satrtDate.getTime();
						Calendar c=Calendar.getInstance();
				        c.setTime(joiningDate);
				       // int totalWorkingDays = c.getActualMaximum(Calendar.DAY_OF_MONTH);
				        int totalWorkingDays = 0;
				        if(noOfwrkingDays == 0){
				        	totalWorkingDays = c.getActualMaximum(Calendar.DAY_OF_MONTH); 
					    }else{
					    	totalWorkingDays = noOfwrkingDays;
					    }
						deductionResultJSONArray.put(getLOPDetailsObject(lopList,Double.parseDouble(activeCtc.toString()),totalWorkingDays,lopleave));
			    	}
			    }
			 }
			catch(Exception e)
			{
			 e.printStackTrace();
			}
*/
			result.put(CommonConstants.DEDUCTIONMONTHLY, String.valueOf(Math.round(deductionTotal)));
			result.put(CommonConstants.DEDUCTIONLIST, deductionResultJSONArray);
			Double dd1 =  (activeCtc/12);
			Double dd2 = dd1/noOfwrkingDays;
			result.put(CommonConstants.BASICGROSS_PAY, String.valueOf(Math.round(dd2*daysdiff)));
			result.put(CommonConstants.BASIC, String.valueOf(Math.round(basicPay)));
			result.put(CommonConstants.STATUS, String.valueOf(CommonConstants.NEW));
			result.put(CommonConstants.PAYROLLMONTH, month);
			result.put(CommonConstants.EARNINGMONTHLY, String.valueOf(Math.round(earingTotal)));
			result.put(CommonConstants.TAXMONTHLY, String.valueOf(Math.round(taxTotal)));
			//result.put(CommonConstants.TAXMONTHLY, String.valueOf(CommonUtil.convertfloatValueWithTwoDecimalPoints(taxTotal)));
			result.put(CommonConstants.EARNINGLIST, earningResultJSONArray);
			Double mnthlyNet = earingTotal - (deductionTotal );
			result.put(CommonConstants.NETMONTHLY, String.valueOf(Math.round(mnthlyNet)));
			
		}else{
			earingTotal = 0.0;
			for (PayrollGroupEarningsDO earningDetails :  empCompensation.getPayrollGroup().getPayrollGroupEarning()) {
				if(earningDetails.getStatus() == CommonConstants.ACTIVE){
					
					if(earningDetails.getAllowanceSettings().getEarningType().getName().equalsIgnoreCase(CommonConstants.DEARNESSALLOWEANCE) && empCompensation.getEmployee().getUnitOrBranch() != null){
						earningResultJSONArray.put(getDearnessAlloweanceDetails(month, earningDetails.getAllowanceSettings(), Long.parseLong("1"), Double.parseDouble("1"),empCompensation.getEmployee().getUnitOrBranch().getUnitOrBranchId()));
					}else{
						if(earningDetails.getAllowanceSettings().getEarningType().getName().equalsIgnoreCase(CommonConstants.ADD_DA) || earningDetails.getAllowanceSettings().getEarningType().getName().equalsIgnoreCase(CommonConstants.ADDDEARNESSALLOWEANCE) && empCompensation.getEmployee().getUnitOrBranch() != null){
							earningResultJSONArray.put(getADDDearnessAlloweanceDetails(month, earningDetails.getAllowanceSettings(),empCompensation.getEmployee().getUnitOrBranch().getUnitOrBranchId(),empCompensation.getEmployee().getEmpId(),empCtc.getPayrollBatch().getPayrollBatchId()));
						}
					
					}
				}
			}
			
			deductionTotal = 0.0;
			JSONArray deductionResultJSONArray = new JSONArray();
			for (PayrollGroupDeductionDO deductionDetails :  empCompensation.getPayrollGroup().getPayrollGroupDeduction()) {
				if(deductionDetails.getStatus() == CommonConstants.ACTIVE){
					if(deductionDetails.getDeductionSettings().getDeductionType().getName().equalsIgnoreCase(CommonConstants.SUBDEARNESSALLOWEANCE)){
						deductionResultJSONArray.put(getSubDADetails(month, deductionDetails.getDeductionSettings(),empCompensation.getEmployee().getUnitOrBranch().getUnitOrBranchId(),empCtc.getPayrollBatch().getPayrollBatchId(),empCompensation.getEmployee().getEmpId()));
					}
				}
					
			}
			
			result.put(CommonConstants.DEDUCTIONMONTHLY, String.valueOf(Math.round(deductionTotal)));
			result.put(CommonConstants.DEDUCTIONLIST, deductionResultJSONArray);
			Double mnthlyNet = 0.0;
			result.put(CommonConstants.EARNINGMONTHLY, String.valueOf(Math.round(earingTotal)));
			result.put(CommonConstants.EARNINGLIST, earningResultJSONArray);
			 mnthlyNet = earingTotal - (deductionTotal);
			 result.put(CommonConstants.NETMONTHLY, String.valueOf(Math.round(mnthlyNet)));
			if(mnthlyNet == 0.0){
				result.put(CommonConstants.ERROR_CODE, String.valueOf("Basic not available"));
			}
			
		}
		
		return result;
	}
	
	public static JSONObject getCompensationWeeklyAllowenceDetailInfoObject(Long earninId, AllowanceSettingDO allowance, Double ctc, int daysdiff, int noOfwrkingDays)throws JSONException {
		JSONObject result = new JSONObject();
		Double grossPay = ctc/12;
		//Double basicPay = 0D;
		Double monthlypay=0D;
		Double ytd=0D;
		if(allowance.getEarningType().getName() != null && allowance.getEarningType().getName().equalsIgnoreCase("Basic")){
			if(allowance.getBasicgrosspay() != null && allowance.getBasicgrosspay().equalsIgnoreCase("G")){
				if(allowance.getPercentageamount() != null){
					basicPay = (grossPay * Double.parseDouble(allowance.getPercentageamount()))/100;
					ytd = basicPay * 12;
					monthlypay = basicPay;
				}
			}
		}else{
			if(allowance.getType() != null && allowance.getType().equalsIgnoreCase("P")){
				if(allowance.getBasicgrosspay() != null && allowance.getBasicgrosspay().equalsIgnoreCase("B")){
					if(allowance.getPercentageamount() != null){
						monthlypay = (basicPay * Long.parseLong(allowance.getPercentageamount()))/100;
						ytd = monthlypay*12;
					}
				}else if(allowance.getBasicgrosspay() != null && allowance.getBasicgrosspay().equalsIgnoreCase("G")){
					if(allowance.getPercentageamount() != null){
						monthlypay = (grossPay * Double.parseDouble(allowance.getPercentageamount()))/100;
						ytd = monthlypay*12;
					}
				}
			}else if(allowance.getType() != null && allowance.getType().equalsIgnoreCase("F")){
				if(allowance.getFixedamount() != null){
					ytd = Double.parseDouble(allowance.getFixedamount());
					monthlypay = ytd/12;
				}
			}
		}
		earingTotal += Math.round((monthlypay/noOfwrkingDays)*daysdiff);
		
		result.put(CommonConstants.MONTHLY, String.valueOf(Math.round((monthlypay/noOfwrkingDays)*daysdiff)));
		result.put(CommonConstants.YTD, String.valueOf(ytd));
		result.put(CommonConstants.NAME, String.valueOf(allowance.getDisplayname()));
		result.put(CommonConstants.EARNINGID, String.valueOf(earninId));
		return result;
	}
	
	public static JSONObject getWeeklyAdustmentDetails(Long earningId, AllowanceSettingDO allowance, Double ctc, int daysdiff, int noOfwrkingDays)throws JSONException {
		JSONObject result = new JSONObject();
		Double grossPay = (ctc/12);
		Double ajustment = 0.0;
		
		ajustment = ((grossPay/noOfwrkingDays)*daysdiff) - earingTotal;
		earingTotal += Math.round(ajustment);
		result.put(CommonConstants.MONTHLY, String.valueOf(Math.round(ajustment)));
		result.put(CommonConstants.YTD, String.valueOf(ajustment*12));
		result.put(CommonConstants.NAME, String.valueOf(allowance.getDisplayname()));
		result.put(CommonConstants.EARNINGID, String.valueOf(earningId));
		
		return result;
	}
	
	public static JSONObject getCompensationWeeklyDeductionDetailInfoObject(Long deductionId, DeductionSettingDO deduction, Double ctc,int daysdiff, int noOfwrkingDays)throws JSONException {
		JSONObject result = new JSONObject();
		Double grossPay = ctc/12;
		//Double basicPay = basic;
		Double monthlypay=0D;
		Double ytd=0D;
		if(deduction.getType() != null && deduction.getType().equalsIgnoreCase("P")){
			if(deduction.getBasicgrosspay() != null && deduction.getBasicgrosspay().equalsIgnoreCase("B")){
				if(deduction.getPercentageamount() != null){
					monthlypay = (basicPay * Double.parseDouble(deduction.getPercentageamount()))/100;
					ytd = monthlypay*12;
				}
			}else if(deduction.getBasicgrosspay() != null && deduction.getBasicgrosspay().equalsIgnoreCase("G")){
				if(deduction.getPercentageamount() != null){
					monthlypay = (grossPay * Double.parseDouble(deduction.getPercentageamount()))/100;
					ytd = monthlypay*12;
				}
			}
		}else if(deduction.getType() != null && deduction.getType().equalsIgnoreCase("F")){
			if(deduction.getFixedamount() != null){
				ytd = Double.parseDouble(deduction.getFixedamount());
				monthlypay = ytd/12;
			}
		}
		deductionTotal += Math.round((monthlypay/noOfwrkingDays)*daysdiff);
		result.put(CommonConstants.MONTHLY, String.valueOf(Math.round((monthlypay/noOfwrkingDays)*daysdiff)));
		result.put(CommonConstants.YTD, String.valueOf(ytd));
		result.put(CommonConstants.NAME, String.valueOf(deduction.getDisplayname()));
		result.put(CommonConstants.DEDUCTIONID, String.valueOf(deductionId));
		return result;
	}
	
	public static JSONObject getWeeklyLOPDetailsObject(List<EmployeeLopDO> lopList, Double ctc, int totalWorkingDays, Double lopLeaves)throws JSONException {
		JSONObject result = new JSONObject();
		Double grossPay = ctc/12;
		Double leaveCount = lopLeaves;
		for(EmployeeLopDO lop : lopList){
			Date dateBeforeString = lop.getStartdate();
			if(lop.getEnddate() != null){
				Calendar end = Calendar.getInstance();
				 Date dateAfterString = lop.getEnddate();
				  end.setTime(dateAfterString);
				  Calendar start = Calendar.getInstance();
				  start.setTime(dateBeforeString);
				 for (Date date = start.getTime(); start.before(end) || start.equals(end); start.add(Calendar.DATE, 1), date = start.getTime()) {
					Calendar weekend = Calendar.getInstance();
					weekend.setTime(date);
					if (/*weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && */
						weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){
						 leaveCount +=1;
					}
				 }
			}else{
				leaveCount +=1;
			}
		}
		
		 Double perdaySalary = (double) (grossPay/totalWorkingDays);
	    // Long lop  = (long) (perdaySalary * leaveCount);
		 
		 Double lop  =  (perdaySalary * leaveCount);
	     deductionTotal += Math.round(lop);
			result.put(CommonConstants.MONTHLY, String.valueOf(Math.round(lop)));
	     result.put(CommonConstants.NAME, String.valueOf("Loss Of Pay"));
		return result;
	}
	
	public static JSONObject getEmpDailyPayrollDetails(List<EmployeeCtcDO> empCtcList, Date payrollDate,Long BatchId, String month) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			
			for (EmployeeCtcDO empCtc : empCtcList) {
				//List<EmployeePayrollDO> payrollList = employeePayrollService.retriveByMonthAndEmp(month, empCtc.getEmployee().getEmpId());
				SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
				String strDate= formatter.format(payrollDate);
				
				
				List<EmployeePayrollDO> payrollList = employeePayrollService.retriveDailyEmpPayroll(empCtc.getEmployee().getEmpId(),strDate,BatchId);
				if(payrollList == null || payrollList.size() <= 0 || payrollList.get(0).getStatus() == 'v'){
					List<EmployeeCompensationDO> empCompensationList = employeeCompensationService.retrieveWithComponsesationDateAndEmpID(empCtc.getEmployee().getEmpId().toString(), payrollDate);
					if(empCompensationList != null && empCompensationList.size() >0){
						resultJSONArray.put(getEmpDailyPayrollDetailsObject(empCompensationList.get(0), payrollDate,empCompensationList.get(0).getEmpCtc(),month ));
					}
				}else{
						resultJSONArray.put(getempDailyPayrollDetailsObject1(payrollList.get(0), month, empCtc));
					
				}
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getEmpDailyPayrollDetailsObject(EmployeeCompensationDO empCompensation, Date payrollDate, EmployeeCtcDO empCtc, String month)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(empCompensation.getEmployeeCompensationId()));
		result.put(CommonConstants.EMPID, String.valueOf(empCompensation.getEmployee().getEmpId()));
		if(empCompensation.getEmployee().getDesignation() != null) result.put(CommonConstants.DESIGNATION, String.valueOf(empCompensation.getEmployee().getDesignation().getName()));
		else result.put(CommonConstants.DESIGNATION, String.valueOf(""));
		result.put(CommonConstants.PAYROLLDATE, String.valueOf(CommonUtil.convertDateToStringWithOutTime(payrollDate)));
		if(empCompensation.getEmployee().getMiddlename() != null) result.put(CommonConstants.EMPNAME, String.valueOf(empCompensation.getEmployee().getFirstname() + " " +empCompensation.getEmployee().getMiddlename() + " " + empCompensation.getEmployee().getLastname()));
		else if(empCompensation.getEmployee().getMiddlename() != null && empCompensation.getEmployee().getLastname() == null ) result.put(CommonConstants.EMPNAME, String.valueOf(empCompensation.getEmployee().getFirstname() + " " +empCompensation.getEmployee().getMiddlename()));
		else if(empCompensation.getEmployee().getMiddlename() == null && empCompensation.getEmployee().getLastname() == null ) result.put(CommonConstants.EMPNAME,String.valueOf(empCompensation.getEmployee().getFirstname()));
		else result.put(CommonConstants.EMPNAME, String.valueOf(empCompensation.getEmployee().getFirstname() + " " + empCompensation.getEmployee().getLastname()));
		int noOfwrkingDays = 0;
		int salaryDays = 0;
		if(empCtc.getPayrollBatch() != null){
			List<CompanyCalenderDO> companyCalenderList = companyCalenderService.retrieveByMonth(month,empCtc.getPayrollBatch().getPayrollBatchId());
			if(companyCalenderList != null && companyCalenderList.size() > 0){
				if(companyCalenderList.get(0).getMinimunWorkingDays() != null && !companyCalenderList.get(0).getMinimunWorkingDays().toString().isEmpty()){
					noOfwrkingDays = Integer.parseInt(companyCalenderList.get(0).getMinimunWorkingDays().toString());
				}
				if(companyCalenderList.get(0).getNoOfSalaryDays() != null && !companyCalenderList.get(0).getNoOfSalaryDays().toString().isEmpty()){
					salaryDays = Integer.parseInt(companyCalenderList.get(0).getNoOfSalaryDays().toString());
				}
			}
		}
		double activeCtc = empCtc.getEmpctc();
		double perdaySalary = 0.0;
        if(empCtc.getCtcType() != null && noOfwrkingDays > 0 && salaryDays > 0){
        	 if(empCtc.getCtcType().equalsIgnoreCase("y"))  perdaySalary = (double) (empCtc.getEmpctc()/12)/salaryDays;
		     if(empCtc.getCtcType().equalsIgnoreCase("m"))  perdaySalary = (double) empCtc.getEmpctc()/salaryDays;
		     if(empCtc.getCtcType().equalsIgnoreCase("d"))  perdaySalary = (double) empCtc.getEmpctc();
		    // activeCtc =  ((perdaySalary * noOfwrkingDays)*12);
		     activeCtc = perdaySalary;
        }else{
        	activeCtc =  empCtc.getEmpctc();
        }
		result.put(CommonConstants.EMPCTC, String.valueOf(empCtc.getEmpctc()));
		result.put(CommonConstants.EMPCTCID, String.valueOf(empCtc.getEmpCtcId()));
		result.put(CommonConstants.PAYROLLBATCHID, String.valueOf(empCtc.getPayrollBatch().getPayrollBatchId()));
		
		result.put(CommonConstants.PAYROLLGROUPID, String.valueOf(empCompensation.getPayrollGroup().getPayrollGroupId()));
		result.put(CommonConstants.PAYROLLGROUPNAME, String.valueOf(empCompensation.getPayrollGroup().getName()));
		
		basicPay = 0.0;
		
		for (PayrollGroupEarningsDO earningDetails :  empCompensation.getPayrollGroup().getPayrollGroupEarning()) {
			if(earningDetails.getStatus() == CommonConstants.ACTIVE && earningDetails.getAllowanceSettings().getEarningType().getName().equalsIgnoreCase(CommonConstants.BASIC)){
				if(earningDetails.getAllowanceSettings().getEarningType().getStatus() == CommonConstants.ACTIVE){
					basicPay = (activeCtc * Double.parseDouble(earningDetails.getAllowanceSettings().getPercentageamount()))/100;
				}
			}
		}
		JSONArray earningResultJSONArray = new JSONArray();
		if(basicPay != 0){
			earingTotal = 0.0;
			taxableIncome = 0.0;
			Boolean Adjustment = false;
			PayrollGroupEarningsDO adjustData = new PayrollGroupEarningsDO();
			for (PayrollGroupEarningsDO earningDetails :  empCompensation.getPayrollGroup().getPayrollGroupEarning()) {
				if(earningDetails.getStatus() == CommonConstants.ACTIVE){
					if(!earningDetails.getAllowanceSettings().getType().equalsIgnoreCase("A")){
						earningResultJSONArray.put(getCompensationDailyAllowenceDetailInfoObject(earningDetails.getPayrollGroupEarningsId(),earningDetails.getAllowanceSettings(), activeCtc,salaryDays));
					}else{
						Adjustment = true;
						adjustData = earningDetails;
					}
				}
			}
			if(Adjustment){
				earningResultJSONArray.put(getDailyAdustmentDetails(adjustData.getPayrollGroupEarningsId(),adjustData.getAllowanceSettings(),activeCtc,salaryDays));
			}
			
			deductionTotal = 0.0;
			JSONArray deductionResultJSONArray = new JSONArray();
			for (PayrollGroupDeductionDO deductionDetails :  empCompensation.getPayrollGroup().getPayrollGroupDeduction()) {
				if(deductionDetails.getStatus() == CommonConstants.ACTIVE){
						deductionResultJSONArray.put(getCompensationDailyDeductionDetailInfoObject(deductionDetails.getPayrollGroupDeductionId(),deductionDetails.getDeductionSettings(),activeCtc,salaryDays));
				}
			}
			
			/*List<EmployeeLopDO> lopList = employeeLopService.retrieveByEmpIdDateStatus(empCompensation.getEmployee().getEmpId(), d1, d2);
			if(lopList != null && lopList.size()>0){
				
				double lopleave = 0.0;
				
				deductionResultJSONArray.put(getWeeklyLOPDetailsObject(lopList,Double.parseDouble(activeCtc.toString()),noOfwrkingDays,lopleave));
		    }*/
			result.put(CommonConstants.DEDUCTIONMONTHLY, String.valueOf(Math.round(deductionTotal)));
			result.put(CommonConstants.DEDUCTIONLIST, deductionResultJSONArray);
			/*Double dd1 = (double) (activeCtc/12);
			Double dd2 = dd1/noOfwrkingDays;*/
			result.put(CommonConstants.BASICGROSS_PAY, String.valueOf(Math.round(activeCtc)));
			result.put(CommonConstants.BASIC, String.valueOf(Math.round(basicPay)));
			result.put(CommonConstants.STATUS, String.valueOf(CommonConstants.NEW));
			result.put(CommonConstants.PAYROLLMONTH, month);
			result.put(CommonConstants.EARNINGMONTHLY, String.valueOf(Math.round(earingTotal)));
			result.put(CommonConstants.TAXMONTHLY, String.valueOf(Math.round(taxTotal)));
			//result.put(CommonConstants.TAXMONTHLY, String.valueOf(CommonUtil.convertfloatValueWithTwoDecimalPoints(taxTotal)));
			result.put(CommonConstants.EARNINGLIST, earningResultJSONArray);
			Double mnthlyNet = earingTotal - (deductionTotal );
			result.put(CommonConstants.NETMONTHLY, String.valueOf(Math.round(mnthlyNet)));
			
		}else{
			earingTotal = 0.0;
			for (PayrollGroupEarningsDO earningDetails :  empCompensation.getPayrollGroup().getPayrollGroupEarning()) {
				if(earningDetails.getStatus() == CommonConstants.ACTIVE){
					
					if(earningDetails.getAllowanceSettings().getEarningType().getName().equalsIgnoreCase(CommonConstants.DEARNESSALLOWEANCE) && empCompensation.getEmployee().getUnitOrBranch() != null){
						earningResultJSONArray.put(getDearnessAlloweanceDetails(month, earningDetails.getAllowanceSettings(),Long.parseLong("1"),Double.parseDouble("1"),empCompensation.getEmployee().getUnitOrBranch().getUnitOrBranchId()));
					}else{
						if(earningDetails.getAllowanceSettings().getEarningType().getName().equalsIgnoreCase(CommonConstants.ADD_DA) || earningDetails.getAllowanceSettings().getEarningType().getName().equalsIgnoreCase(CommonConstants.ADDDEARNESSALLOWEANCE) && empCompensation.getEmployee().getUnitOrBranch() != null){
							earningResultJSONArray.put(getADDDearnessAlloweanceDetails(month, earningDetails.getAllowanceSettings(),empCompensation.getEmployee().getUnitOrBranch().getUnitOrBranchId(),empCompensation.getEmployee().getEmpId(),empCtc.getPayrollBatch().getPayrollBatchId()));
						}
					
					}
				}
			}
			
			deductionTotal = 0.0;
			JSONArray deductionResultJSONArray = new JSONArray();
			for (PayrollGroupDeductionDO deductionDetails :  empCompensation.getPayrollGroup().getPayrollGroupDeduction()) {
				if(deductionDetails.getStatus() == CommonConstants.ACTIVE){
					if(deductionDetails.getDeductionSettings().getDeductionType().getName().equalsIgnoreCase(CommonConstants.SUBDEARNESSALLOWEANCE)){
						deductionResultJSONArray.put(getSubDADetails(month, deductionDetails.getDeductionSettings(),empCompensation.getEmployee().getUnitOrBranch().getUnitOrBranchId(),empCtc.getPayrollBatch().getPayrollBatchId(),empCompensation.getEmployee().getEmpId()));
					}
				}
					
			}
			
			result.put(CommonConstants.DEDUCTIONMONTHLY, String.valueOf(Math.round(deductionTotal)));
			result.put(CommonConstants.DEDUCTIONLIST, deductionResultJSONArray);
			Double mnthlyNet = 0.0;
			result.put(CommonConstants.EARNINGMONTHLY, String.valueOf(Math.round(earingTotal)));
			result.put(CommonConstants.EARNINGLIST, earningResultJSONArray);
			 mnthlyNet = earingTotal - (deductionTotal);
			 result.put(CommonConstants.NETMONTHLY, String.valueOf(Math.round(mnthlyNet)));
			if(mnthlyNet == 0.0){
				result.put(CommonConstants.ERROR_CODE, String.valueOf("Basic not available"));
			}
			
		}
		
		return result;
	}
	
	public static JSONObject getCompensationDailyAllowenceDetailInfoObject(Long earninId, AllowanceSettingDO allowance, Double ctc, int noOfwrkingDays)throws JSONException {
		JSONObject result = new JSONObject();
		Double grossPay = ctc;
		//Double basicPay = 0D;
		Double monthlypay=0D;
		Double ytd=0D;
		if(allowance.getEarningType().getName() != null && allowance.getEarningType().getName().equalsIgnoreCase("Basic")){
			if(allowance.getBasicgrosspay() != null && allowance.getBasicgrosspay().equalsIgnoreCase("G")){
				if(allowance.getPercentageamount() != null){
					basicPay = (grossPay * Double.parseDouble(allowance.getPercentageamount()))/100;
					ytd = basicPay * 12;
					monthlypay = basicPay;
				}
			}
		}else{
			if(allowance.getType() != null && allowance.getType().equalsIgnoreCase("P")){
				if(allowance.getBasicgrosspay() != null && allowance.getBasicgrosspay().equalsIgnoreCase("B")){
					if(allowance.getPercentageamount() != null){
						monthlypay = (basicPay * Long.parseLong(allowance.getPercentageamount()))/100;
						ytd = monthlypay*12;
					}
				}else if(allowance.getBasicgrosspay() != null && allowance.getBasicgrosspay().equalsIgnoreCase("G")){
					if(allowance.getPercentageamount() != null){
						monthlypay = (grossPay * Double.parseDouble(allowance.getPercentageamount()))/100;
						ytd = monthlypay*12;
					}
				}
			}else if(allowance.getType() != null && allowance.getType().equalsIgnoreCase("F")){
				if(allowance.getFixedamount() != null){
					ytd = Double.parseDouble(allowance.getFixedamount());
					monthlypay = ytd/12;
				}
			}
		}
		earingTotal += Math.round((monthlypay));
		result.put(CommonConstants.MONTHLY, String.valueOf(Math.round(monthlypay)));
		result.put(CommonConstants.YTD, String.valueOf(ytd));
		result.put(CommonConstants.NAME, String.valueOf(allowance.getDisplayname()));
		result.put(CommonConstants.EARNINGID, String.valueOf(earninId));
		return result;
	}
	
	public static JSONObject getDailyAdustmentDetails(Long earningId, AllowanceSettingDO allowance, Double ctc, int noOfwrkingDays)throws JSONException {
		JSONObject result = new JSONObject();
		Double grossPay = (ctc);
		Double ajustment = 0.0;
		ajustment = ((grossPay)) - earingTotal;
		earingTotal += Math.round(ajustment);
		result.put(CommonConstants.MONTHLY, String.valueOf(Math.round(ajustment)));
		result.put(CommonConstants.YTD, String.valueOf(ajustment*12));
		result.put(CommonConstants.NAME, String.valueOf(allowance.getDisplayname()));
		result.put(CommonConstants.EARNINGID, String.valueOf(earningId));
		
		return result;
	}
	
	public static JSONObject getCompensationDailyDeductionDetailInfoObject(Long deductionId, DeductionSettingDO deduction, Double ctc, int noOfwrkingDays)throws JSONException {
		JSONObject result = new JSONObject();
		Double grossPay = ctc;
		//Double basicPay = basic;
		Double monthlypay=0D;
		Double ytd=0D;
		if(deduction.getType() != null && deduction.getType().equalsIgnoreCase("P")){
			if(deduction.getBasicgrosspay() != null && deduction.getBasicgrosspay().equalsIgnoreCase("B")){
				if(deduction.getPercentageamount() != null){
					monthlypay = (basicPay * Double.parseDouble(deduction.getPercentageamount()))/100;
					ytd = monthlypay*12;
				}
			}else if(deduction.getBasicgrosspay() != null && deduction.getBasicgrosspay().equalsIgnoreCase("G")){
				if(deduction.getPercentageamount() != null){
					monthlypay = (grossPay * Double.parseDouble(deduction.getPercentageamount()))/100;
					ytd = monthlypay*12;
				}
			}
		}else if(deduction.getType() != null && deduction.getType().equalsIgnoreCase("F")){
			if(deduction.getFixedamount() != null){
				ytd = Double.parseDouble(deduction.getFixedamount());
				monthlypay = ytd/12;
			}
		}
		deductionTotal += Math.round((monthlypay));
		result.put(CommonConstants.MONTHLY, String.valueOf(Math.round((monthlypay))));
		result.put(CommonConstants.YTD, String.valueOf(ytd));
		result.put(CommonConstants.NAME, String.valueOf(deduction.getDisplayname()));
		result.put(CommonConstants.DEDUCTIONID, String.valueOf(deductionId));
		return result;
	}
	
	public static JSONObject getempDailyPayrollDetailsObject1(EmployeePayrollDO payroll, String month, EmployeeCtcDO empCtc)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(payroll.getEmployeeCompensation().getEmployeeCompensationId()));
		result.put(CommonConstants.EMPID, String.valueOf(payroll.getEmployee().getEmpId()));
		if(payroll.getEmployee().getDesignation() != null) result.put(CommonConstants.DESIGNATION, String.valueOf(payroll.getEmployee().getDesignation().getName()));
		else result.put(CommonConstants.DESIGNATION, String.valueOf(""));
		if(payroll.getEmployee().getMiddlename() != null) result.put(CommonConstants.EMPNAME, String.valueOf(payroll.getEmployee().getFirstname() + " " +payroll.getEmployee().getMiddlename() + " " + payroll.getEmployee().getLastname()));
		else if(payroll.getEmployee().getMiddlename() != null && payroll.getEmployee().getLastname() == null ) result.put(CommonConstants.EMPNAME, String.valueOf(payroll.getEmployee().getFirstname() + " " +payroll.getEmployee().getMiddlename()));
		else if(payroll.getEmployee().getMiddlename() == null && payroll.getEmployee().getLastname() == null ) result.put(CommonConstants.EMPNAME,String.valueOf(payroll.getEmployee().getFirstname()));
		else result.put(CommonConstants.EMPNAME, String.valueOf(payroll.getEmployee().getFirstname() + " " + payroll.getEmployee().getLastname()));
		result.put(CommonConstants.EMPCTC, String.valueOf(Math.round(empCtc.getEmpctc())));
		result.put(CommonConstants.EMPCTCID, String.valueOf(empCtc.getEmpCtcId()));
		result.put(CommonConstants.PAYROLLBATCHID, String.valueOf(empCtc.getPayrollBatch().getPayrollBatchId()));
		result.put(CommonConstants.PAYROLLGROUPID, String.valueOf(payroll.getPayrollGroup().getPayrollGroupId()));
		result.put(CommonConstants.PAYROLLGROUPNAME, String.valueOf(payroll.getPayrollGroup().getName()));
		JSONArray earningResultJSONArray = new JSONArray();
		for (PayrollEarningsDetailsDO earningDetails :  payroll.getPayrollEarning()) {
			JSONObject earningResult = new JSONObject();
			earningResult.put(CommonConstants.MONTHLY, String.valueOf(Math.round(earningDetails.getMonthly())));
			if(earningDetails.getYtd() != null)earningResult.put(CommonConstants.YTD, String.valueOf(Math.round(earningDetails.getYtd())));
			earningResult.put(CommonConstants.NAME, String.valueOf(earningDetails.getName()));
			earningResult.put(CommonConstants.EARNINGID, String.valueOf(earningDetails.getPayrollEarningId()));
			earningResultJSONArray.put(earningResult);
		}
		result.put(CommonConstants.EARNINGMONTHLY, String.valueOf(Math.round(payroll.getEarningmonth())));
		result.put(CommonConstants.EARNINGLIST, earningResultJSONArray);
		
		JSONArray deductionResultJSONArray = new JSONArray();
		for (PayrollDeductionDetailsDO deductionDetails :  payroll.getPayrollDeduction()) {
			JSONObject deductionResult = new JSONObject();
			if(deductionDetails.getMonthly() > 0)deductionResult.put(CommonConstants.MONTHLY, String.valueOf(Math.round(deductionDetails.getMonthly())));
			else deductionResult.put(CommonConstants.MONTHLY, String.valueOf(Math.round(deductionDetails.getMonthly())));
			//deductionResult.put(CommonConstants.YTD, String.valueOf(Math.round(deductionDetails.getYtd())));
			deductionResult.put(CommonConstants.NAME, String.valueOf(deductionDetails.getName()));
			deductionResult.put(CommonConstants.DEDUCTIONID, String.valueOf(deductionDetails.getPayrollDeductionId()));
			deductionResultJSONArray.put(deductionResult);
				
		}
		result.put(CommonConstants.DEDUCTIONMONTHLY, String.valueOf(Math.round(payroll.getDeductionmonth())));
		result.put(CommonConstants.DEDUCTIONLIST, deductionResultJSONArray);
		JSONArray taxResultJSONArray = new JSONArray();
		for (PayrollTaxDetailsDO taxDetails :  payroll.getPayrollTax()) {
			JSONObject taxResult = new JSONObject();
			if(taxDetails.getMonthly()  != null && taxDetails.getMonthly() > 0)taxResult.put(CommonConstants.MONTHLY, String.valueOf(Math.round(taxDetails.getMonthly())));
			else taxResult.put(CommonConstants.MONTHLY, String.valueOf(""));
			if(taxDetails.getName() != null)taxResult.put(CommonConstants.NAME, String.valueOf(taxDetails.getName()));
			else taxResult.put(CommonConstants.NAME, String.valueOf(""));
			taxResult.put(CommonConstants.TAXID, String.valueOf(taxDetails.getPayrollTaxId()));
			taxResultJSONArray.put(taxResult);
		}
		if(payroll.getPayrollDate() != null){
			result.put(CommonConstants.PAYROLLDATE, String.valueOf(payroll.getPayrollDate()));
		}else{
			result.put(CommonConstants.PAYROLLDATE, String.valueOf(""));
		}
		
		result.put(CommonConstants.BASICGROSS_PAY, String.valueOf(Math.round(payroll.getGrossPay())));
		result.put(CommonConstants.TAXMONTHLY, String.valueOf(Math.round(payroll.getTaxMonth())));
		result.put(CommonConstants.TAXLIST, taxResultJSONArray);
		
		result.put(CommonConstants.STATUS, String.valueOf(payroll.getStatus()));
		result.put(CommonConstants.PAYROLLMONTH, month);
		result.put(CommonConstants.NETMONTHLY, String.valueOf(Math.round(payroll.getNetmonth())));
		return result;
	}
	
}
