package com.spheresuite.erp.web.util;

import java.util.List;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.DepartmentDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.util.CommonConstants;
@Component
public class DepartmentUtil {
	
	private DepartmentUtil() {}

	@Autowired
	private EmployeeService temployeeService;
	
	private static EmployeeService employeeService;
	
	@PostConstruct
	public void init() {
		employeeService = temployeeService;
	}
	
	public static JSONObject getDepartmentList(List<DepartmentDO> departmentList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (DepartmentDO department : departmentList) {
				resultJSONArray.put(getDepartmentDetailObject(department));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getDepartmentDetailObject(DepartmentDO department)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(department.getDepartmentId()));
		result.put(CommonConstants.NAME, String.valueOf(department.getName()));
		result.put(CommonConstants.STATUS, String.valueOf(department.getStatus()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(department.getUpdatedon())));
		result.put(CommonConstants.FROMTIME, String.valueOf(department.getFromTime() != null ? department.getFromTime() : ""));
		result.put(CommonConstants.TOTIME, String.valueOf(department.getToTime() != null ? department.getToTime() : ""));
		result.put(CommonConstants.MAX_HOUR_EXCUSE, String.valueOf(department.getMaxHrExcuse() != null ? department.getMaxHrExcuse() : ""));
		result.put(CommonConstants.NO_OF_EXCUSE, String.valueOf(department.getNoOfExcuse() != null ? department.getNoOfExcuse() : ""));
		if(department.getFromTime() != null){
			String[] s = department.getFromTime().split(":");
			if(s != null && s.length > 0 && s[0] != null){
				Long time = Long.parseLong(s[0].trim());
				if(time > 12){
					time = time - 12;
					result.put(CommonConstants.FROMTIMEDISPLAY, String.valueOf(time+" PM"));
				}else{
					result.put(CommonConstants.FROMTIMEDISPLAY, String.valueOf(time+" AM"));
				}
			}
			
		}else{
			result.put(CommonConstants.FROMTIMEDISPLAY, String.valueOf(""));
		}
		if(department.getToTime() != null){
			String[] s = department.getToTime().split(":");
			if(s != null && s.length > 0 && s[0] != null){
				Long time = Long.parseLong(s[0].trim());
				if(time > 12){
					time = time - 12;
					result.put(CommonConstants.TOTIMEDISPLAY, String.valueOf(time+" PM"));
				}else{
					result.put(CommonConstants.TOTIMEDISPLAY, String.valueOf(time+" AM"));
				}
			}
			
		}else{
			result.put(CommonConstants.TOTIMEDISPLAY, String.valueOf(""));
		}
		result.put(CommonConstants.STATUS, String.valueOf(department.getStatus()));
		if(department.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(department.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		if(department.getIsManager() != null){
			List<EmployeeDO> empList = employeeService.retrieveEmpId(department.getIsManager());
			if(empList != null && empList.size() > 0){
				result.put(CommonConstants.EMPID, String.valueOf(department.getIsManager()));
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.EMPNAME, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else if(empList.get(0).getMiddlename() != null && empList.get(0).getLastname() == null ) result.put(CommonConstants.EMPNAME,  String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename()));
				else if(empList.get(0).getMiddlename() == null && empList.get(0).getLastname() == null ) result.put(CommonConstants.EMPNAME, String.valueOf(empList.get(0).getFirstname()));
				else result.put(CommonConstants.EMPNAME, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.EMPID, "");
				result.put(CommonConstants.EMPNAME, "");
			}
			
		}else{
			result.put(CommonConstants.EMPID, "");
			result.put(CommonConstants.EMPNAME, "");
		}
		return result;
	}
}
