package com.spheresuite.erp.web.util;

import java.util.List;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.ShiftDO;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.util.CommonConstants;
@Component
public class ShiftUtil {
	
	private ShiftUtil() {}
	
	@Autowired
	private EmployeeService temployeeService;
	
	private static EmployeeService employeeService;
	
	@PostConstruct
	public void init() {
		employeeService = temployeeService;
	}
	
	public static JSONObject getShiftList(List<ShiftDO> shiftList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (ShiftDO shift : shiftList) {
				resultJSONArray.put(getShiftDetailObject(shift));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getShiftDetailObject(ShiftDO shift)throws JSONException {
		JSONObject result = new JSONObject();
	//	System.out.println("cc"+shift.getState().iterator().next());
		result.put(CommonConstants.ID, String.valueOf(shift.getShiftId()));
		result.put(CommonConstants.NAME, String.valueOf(shift.getName()));
		result.put(CommonConstants.STARTTIME, String.valueOf(shift.getStartTime()));
		result.put(CommonConstants.STATUS, String.valueOf(shift.getStatus()));
		result.put(CommonConstants.ENDTIME, String.valueOf(shift.getEndTime()));
		result.put(CommonConstants.MAX_HOUR_EXCUSE, String.valueOf(shift.getMaxHrExcuse() != null ? shift.getMaxHrExcuse() : ""));
		result.put(CommonConstants.NO_OF_EXCUSE, String.valueOf(shift.getNoOfExcuse() != null ? shift.getNoOfExcuse() : ""));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(shift.getUpdatedon())));
		if(shift.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(shift.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		if(shift.getIsManager() != null){
			List<EmployeeDO> empList = employeeService.retrieveEmpId(shift.getIsManager());
			if(empList != null && empList.size() > 0){
				result.put(CommonConstants.EMPID, String.valueOf(shift.getIsManager()));
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.EMPNAME, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else if(empList.get(0).getMiddlename() != null && empList.get(0).getLastname() == null ) result.put(CommonConstants.EMPNAME,  String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename()));
				else if(empList.get(0).getMiddlename() == null && empList.get(0).getLastname() == null ) result.put(CommonConstants.EMPNAME, String.valueOf(empList.get(0).getFirstname()));
				else result.put(CommonConstants.EMPNAME, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.EMPID, "");
				result.put(CommonConstants.EMPNAME, "");
			}
			
		}else{
			result.put(CommonConstants.EMPID, "");
			result.put(CommonConstants.EMPNAME, "");
		}
		return result;
	}
}
