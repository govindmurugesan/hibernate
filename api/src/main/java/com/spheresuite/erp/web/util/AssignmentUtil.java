package com.spheresuite.erp.web.util;

import java.util.List;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.crm.service.CustomerService;
import com.spheresuite.erp.crm.service.OpportunitiesService;
import com.spheresuite.erp.crm.service.PaymentTermService;
import com.spheresuite.erp.crm.service.ProjectService;
import com.spheresuite.erp.domainobject.AssignmentDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.util.CommonConstants;

@Component
public class AssignmentUtil {
	
	private AssignmentUtil() {}
	
	@Autowired
	private EmployeeService temployeeService;
	
	@Autowired
	private OpportunitiesService topportunitiesService;
	
	@Autowired
	private ProjectService tprojectService;
	
	@Autowired
	private CustomerService tcustomerService;
	
	@Autowired
	private PaymentTermService tpaymentTermService;
	
	
	private static EmployeeService employeeService;
	
	private static OpportunitiesService opportunitiesService;
	
	private static ProjectService projectService;
	
	private static CustomerService customerService;
	
	private static PaymentTermService paymentTermService;
	
	@PostConstruct
	public void init() {
		employeeService = temployeeService;
		opportunitiesService =topportunitiesService;
		projectService =tprojectService;
		customerService =tcustomerService;
		paymentTermService =tpaymentTermService;
	}
	
	
	public static JSONObject getAllowanceList(List<AssignmentDO> assignmentList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (AssignmentDO assignment : assignmentList) {
				resultJSONArray.put(getAllowanceDetailObject(assignment));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getAllowanceDetailObject(AssignmentDO assignment)throws JSONException{
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(assignment.getAssignementId()));
		
		if(assignment.getOpportunity() != null){
			result.put(CommonConstants.OPPORTUNITY_ID, String.valueOf(assignment.getOpportunity().getOpportunityId()));
			result.put(CommonConstants.OPPORTUNITY_NAME, String.valueOf(assignment.getOpportunity().getProjectname()));
		}else{
			result.put(CommonConstants.OPPORTUNITY_ID, "");
			result.put(CommonConstants.OPPORTUNITY_NAME, "");
		}
		
		if(assignment.getProject() != null){
			result.put(CommonConstants.PROJECTID, String.valueOf(assignment.getProject().getProjectId()));
			result.put(CommonConstants.PROJECTNAME, String.valueOf(assignment.getProject().getProjectname()));
		}else{
			result.put(CommonConstants.PROJECTID, "");
			result.put(CommonConstants.PROJECTNAME, "");
		}
		
		if(assignment.getCustomer() != null){
			result.put(CommonConstants.CUSTOMERID, String.valueOf(assignment.getCustomer().getLeadId()));
			result.put(CommonConstants.CUSTOMERNAME, String.valueOf(assignment.getCustomer().getName()));
		}else{
			result.put(CommonConstants.CUSTOMERID, "");
			result.put(CommonConstants.CUSTOMERNAME, "");
		}
		
		if(assignment.getPaymentTerms() != null){
			result.put(CommonConstants.PAYMENTTERM, String.valueOf(assignment.getPaymentTerms().getPaymentTermId()));
			result.put(CommonConstants.PAYMENTTERM_NAME, String.valueOf(assignment.getPaymentTerms().getName()));
		}else{
			result.put(CommonConstants.PAYMENTTERM, "");
			result.put(CommonConstants.PAYMENTTERM_NAME, "");
		}
		
		if(assignment.getProjectType() != null){
			result.put(CommonConstants.PROJECTTYPE,String.valueOf(assignment.getProjectType()));
		}else{
			result.put(CommonConstants.PROJECTTYPE, "");
		}
		
		if(assignment.getOrderNumber() != null){
			result.put(CommonConstants.ORDERNUMBER,String.valueOf(assignment.getOrderNumber()));
		}else{
			result.put(CommonConstants.ORDERNUMBER, "");
		}
		
		if(assignment.getStartdate() != null){
			result.put(CommonConstants.STARTDATE, String.valueOf(assignment.getStartdate()));
		}else{
			result.put(CommonConstants.STARTDATE,"");
		}
		if(assignment.getEnddate() != null){
			result.put(CommonConstants.ENDDATE, String.valueOf(assignment.getEnddate()));
		}else{
			result.put(CommonConstants.ENDDATE,"");
		}
		if(assignment.getEmpId() != null){
			result.put(CommonConstants.UPDATED_BY, String.valueOf(assignment.getUpdatedby()));
		}else{
			result.put(CommonConstants.UPDATED_BY,"");
		}
		result.put(CommonConstants.STATUS, String.valueOf(assignment.getStatus()));
		
		if(assignment.getComments() != null){
			result.put(CommonConstants.COMMENT, String.valueOf(assignment.getComments()));
		}else{
			result.put(CommonConstants.COMMENT,"");
		}
		if(assignment.getUpdatedby() != null){
			List<EmployeeDO> empList = employeeService.retriveByEmpId(assignment.getUpdatedby());
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY_NAME, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY_NAME, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY_NAME, ""); 
			}
			
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		if(assignment.getEmpId() != null){
			List<EmployeeDO> empList = employeeService.retrieveEmpId(assignment.getEmpId().toString());
			result.put(CommonConstants.EMPID,String.valueOf(assignment.getEmpId()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.EMPNAME, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.EMPNAME, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.EMPNAME, ""); 
				result.put(CommonConstants.EMPID, "");
			}
			
		}else{
			result.put(CommonConstants.EMPNAME, ""); 
		}
		if(assignment.getUpdatedon() != null){
			result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(assignment.getUpdatedon())));
		}else{
			result.put(CommonConstants.UPDATEDON,"");
		}
		
		if(assignment.getAmount() != null){
			result.put(CommonConstants.AMOUNT, String.valueOf(assignment.getAmount()));
		}else{
			result.put(CommonConstants.AMOUNT, "");
		}
		
		return result;
	}
}
