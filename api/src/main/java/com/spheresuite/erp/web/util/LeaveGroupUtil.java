package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.LeaveGroupDO;
import com.spheresuite.erp.util.CommonConstants;
@Component
public class LeaveGroupUtil {
	
	private LeaveGroupUtil() {}
	
	public static JSONObject getLeaveGroupList(List<LeaveGroupDO> leaveGroupList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (LeaveGroupDO leaveGroup : leaveGroupList) {
				resultJSONArray.put(getLeaveGroupDetailObject(leaveGroup));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getLeaveGroupDetailObject(LeaveGroupDO leaveGroup)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(leaveGroup.getLeaveGroupId()));
		result.put(CommonConstants.NAME, String.valueOf(leaveGroup.getLeaveGroup()));
		result.put(CommonConstants.DESCRIPTION, String.valueOf(leaveGroup.getDescription()));
		result.put(CommonConstants.STATUS, String.valueOf(leaveGroup.getStatus()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(leaveGroup.getUpdatedon())));
		if(leaveGroup.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(leaveGroup.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		return result;
	}
}
