package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.EmployeeEncashLeaveDO;
import com.spheresuite.erp.util.CommonConstants;
@Component
public class EmployeeEncashLeaveUtil {
	
	private EmployeeEncashLeaveUtil() {}

	public static JSONObject getEmployeeEncashLeaveList(List<EmployeeEncashLeaveDO> employeeEncashLeaveList){
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeeEncashLeaveDO employeeEncashLeave : employeeEncashLeaveList) {
				resultJSONArray.put(getEmployeeEncashLeaveObject(employeeEncashLeave));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getEmployeeEncashLeave(List<Object[]> employeeEncashLeaveList){
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (Object[] employeeEncashLeave : employeeEncashLeaveList) {
				resultJSONArray.put(getEmployeeNumberOfEncashLeave(employeeEncashLeave));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getEmployeeNumberOfEncashLeave(Object[] employeeEncashLeave)throws JSONException {
		JSONObject result = new JSONObject();
		if(employeeEncashLeave[0] != null && !employeeEncashLeave[0].toString().isEmpty()){
			result.put(CommonConstants.DAYS, String.valueOf(Long.parseLong(employeeEncashLeave[0].toString())));
		}else{
			result.put(CommonConstants.DAYS, String.valueOf(0));
		}
		
		return result;
	}

	public static JSONObject getEmployeeEncashLeaveObject(EmployeeEncashLeaveDO employeeEncashLeave)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(employeeEncashLeave.getEncashleaveid()));
		if(employeeEncashLeave.getFromDate()!= null){
			result.put(CommonConstants.FROMDATE, String.valueOf(CommonUtil.convertDateToStringWithOutComma(employeeEncashLeave.getFromDate())));
		}else{
			result.put(CommonConstants.FROMDATE, "");
		}
		if(employeeEncashLeave.getToDate() != null){
			result.put(CommonConstants.TODATE, String.valueOf(CommonUtil.convertDateToStringWithOutComma(employeeEncashLeave.getToDate())));
		}else{
			result.put(CommonConstants.TODATE, "");
		}
		result.put(CommonConstants.DAYS, String.valueOf(employeeEncashLeave.getNumberofenchashleave()));
		result.put(CommonConstants.EMPID, String.valueOf(employeeEncashLeave.getEmployee().getEmpId()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(employeeEncashLeave.getUpdatedon())));
		if(employeeEncashLeave.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(employeeEncashLeave.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		return result;
	}
}
