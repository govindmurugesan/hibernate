package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.EmployeeEducationDO;
import com.spheresuite.erp.util.CommonConstants;
@Component
public class EmployeeEducationUtil {
	
	private EmployeeEducationUtil() {}
	
	public static JSONObject getEmployeeEducationList(List<EmployeeEducationDO> employeeEducationList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeeEducationDO empEducationDO : employeeEducationList) {
				resultJSONArray.put(getEmployeeEducationDetailObject(empEducationDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getEmployeeEducationDetailObject(EmployeeEducationDO empEducationDO)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(empEducationDO.getEmpEducationId()));
		if(empEducationDO.getCountry() != null){
			result.put(CommonConstants.COUNTRY_ID, String.valueOf(empEducationDO.getCountry().getCountryId()));
			result.put(CommonConstants.COUNTRY_NAME, String.valueOf(empEducationDO.getCountry().getCountryName()));
		}else{
			result.put(CommonConstants.COUNTRY_ID, " ");
			result.put(CommonConstants.COUNTRY_NAME, " " );
		}
		
		if(empEducationDO.getState() != null){
			result.put(CommonConstants.STATE_ID, String.valueOf(empEducationDO.getState().getStateId()));
			result.put(CommonConstants.STATE_NAME, String.valueOf(empEducationDO.getState().getStateName()));
		}else{
			result.put(CommonConstants.STATE_ID, " ");
			result.put(CommonConstants.STATE_NAME, " " );
		}
		
		if(empEducationDO.getEducationLevel() != null){
			result.put(CommonConstants.EDUCATIONLEVELID, String.valueOf(empEducationDO.getEducationLevel().getEducationLevelId()));
			result.put(CommonConstants.EDUCATIONLEVELNAME, String.valueOf(empEducationDO.getEducationLevel().getName()));
		}else{
			result.put(CommonConstants.EDUCATIONLEVELID, " ");
			result.put(CommonConstants.EDUCATIONLEVELNAME, " " );
		}
		result.put(CommonConstants.CITY, String.valueOf(empEducationDO.getCity()));
		result.put(CommonConstants.UNIVERSITY, String.valueOf(empEducationDO.getUniversity()));
		result.put(CommonConstants.AREA, String.valueOf(empEducationDO.getArea()));
		result.put(CommonConstants.YEAR, String.valueOf(empEducationDO.getYearPassed()));
		return result;
	}
}
