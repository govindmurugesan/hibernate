package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.DesignationDO;
import com.spheresuite.erp.util.CommonConstants;
@Component
public class DesignationUtil {
	
	private DesignationUtil() {}
	
	public static JSONObject getDesignationList(List<DesignationDO> designationList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (DesignationDO desigantion : designationList) {
				resultJSONArray.put(getEmployeeTypeDetailObject(desigantion));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getEmployeeTypeDetailObject(DesignationDO desigantion)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(desigantion.getDesignationId()));
		result.put(CommonConstants.NAME, String.valueOf(desigantion.getName()));
		result.put(CommonConstants.STATUS, String.valueOf(desigantion.getStatus()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(desigantion.getUpdatedon())));
		if(desigantion.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(desigantion.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		return result;
	}
}
