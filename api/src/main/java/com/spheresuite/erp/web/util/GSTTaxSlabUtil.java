package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.GSTTaxSlabDO;
import com.spheresuite.erp.util.CommonConstants;
@Component
public class GSTTaxSlabUtil {
	
	private GSTTaxSlabUtil() {}
	
	public static JSONObject getTaxSlabList(List<GSTTaxSlabDO> gstTaxSlabList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (GSTTaxSlabDO gstTaxSlab : gstTaxSlabList) {
				resultJSONArray.put(getLeadTypeDetailObject(gstTaxSlab));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getLeadTypeDetailObject(GSTTaxSlabDO gstTaxSlab)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(gstTaxSlab.getGstTaxSlabId()));
		result.put(CommonConstants.NAME, String.valueOf(gstTaxSlab.getPercentage()));
		result.put(CommonConstants.STATUS, String.valueOf(gstTaxSlab.getStatus()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(""));
		if(gstTaxSlab.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(gstTaxSlab.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		return result;
	}
}
