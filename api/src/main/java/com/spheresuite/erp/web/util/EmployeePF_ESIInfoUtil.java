package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.EmployeeESIInfoDO;
import com.spheresuite.erp.domainobject.EmployeePFInfoDO;
import com.spheresuite.erp.util.CommonConstants;
@Component
public class EmployeePF_ESIInfoUtil {
	
	private EmployeePF_ESIInfoUtil() {}

	public static JSONObject getEmployeePFESIInfoList(List<EmployeePFInfoDO> employeeInformationList){
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeePFInfoDO employeeInformation : employeeInformationList) {
				resultJSONArray.put(getEmployeeInfoDetailObject(employeeInformation));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	
	
	public static JSONObject getEmployeeESIInfoList(List<EmployeeESIInfoDO> employeeInformationList){
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeeESIInfoDO employeeInformation : employeeInformationList) {
				resultJSONArray.put(getEmployeeESIInfoDetailObject(employeeInformation));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getEmployeeInfoDetailObject(EmployeePFInfoDO employeeInformation)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(employeeInformation.getEmpPFInfoId()));
		if(employeeInformation.getPfGroup() != null){
			result.put(CommonConstants.PFGROUPNAME, String.valueOf(employeeInformation.getPfGroup().getName()));
			result.put(CommonConstants.PFGROUPID, String.valueOf(employeeInformation.getPfGroup().getPfGroupId()));
		}else{
			result.put(CommonConstants.PFGROUPNAME, "");
			result.put(CommonConstants.PFGROUPID,"");
		}
		/*if(employeeInformation.getEsiGroup() != null){
			result.put(CommonConstants.ESIGROUPNAME, String.valueOf(employeeInformation.getEsiGroup().getName()));
			result.put(CommonConstants.ESIGROUPID, String.valueOf(employeeInformation.getEsiGroup().getEsiGroupId()));
		}else{
			result.put(CommonConstants.ESIGROUPNAME, "");
			result.put(CommonConstants.ESIGROUPID,"");
		}*/
		if(employeeInformation.getPFAccNo() != null){
			result.put(CommonConstants.PFACCOUNTNO, String.valueOf(employeeInformation.getPFAccNo()));
		}else{
			result.put(CommonConstants.PFACCOUNTNO, "");
		}
		
		if(employeeInformation.getUan() != null){
			result.put(CommonConstants.UAN, String.valueOf(employeeInformation.getUan()));
		}else{
			result.put(CommonConstants.UAN, "");
		}
		
		if(employeeInformation.getType() != null){
			result.put(CommonConstants.PFTYPE, String.valueOf(employeeInformation.getType()));
		}else{
			result.put(CommonConstants.PFTYPE, "");
		}
		if(employeeInformation.getAmount() != null){
			result.put(CommonConstants.PFAMOUNT, String.valueOf(employeeInformation.getAmount()));
		}else{
			result.put(CommonConstants.PFAMOUNT, "");
		}
		if(employeeInformation.getFromDate() != null){
			result.put(CommonConstants.PFFROM, String.valueOf(CommonUtil.convertDateToStringApp(employeeInformation.getFromDate())));
		}else{
			result.put(CommonConstants.PFFROM, "");
		}
		if(employeeInformation.getToDate() != null){
			result.put(CommonConstants.PFTO, String.valueOf(CommonUtil.convertDateToStringApp(employeeInformation.getToDate())));
		}else{
			result.put(CommonConstants.PFTO, "");
		}
		/*if(employeeInformation.getESIAccNo() != null){
			result.put(CommonConstants.ESIACCOUNTNO, String.valueOf(employeeInformation.getESIAccNo()));
		}else{
			result.put(CommonConstants.ESIACCOUNTNO, "");
		}*/
		result.put(CommonConstants.EMPID, String.valueOf(employeeInformation.getEmployee().getEmpId()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(employeeInformation.getUpdatedon())));
		if(employeeInformation.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(employeeInformation.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		return result;
	}
	
	
	public static JSONObject getEmployeeESIInfoDetailObject(EmployeeESIInfoDO employeeInformation)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(employeeInformation.getEmpESIInfoId()));
		if(employeeInformation.getEsiGroup() != null){
			result.put(CommonConstants.ESIGROUPNAME, String.valueOf(employeeInformation.getEsiGroup().getName()));
			result.put(CommonConstants.ESIGROUPID, String.valueOf(employeeInformation.getEsiGroup().getEsiGroupId()));
		}else{
			result.put(CommonConstants.ESIGROUPNAME, "");
			result.put(CommonConstants.ESIGROUPID,"");
		}
		if(employeeInformation.getESIAccNo() != null){
			result.put(CommonConstants.ESIACCOUNTNO, String.valueOf(employeeInformation.getESIAccNo()));
		}else{
			result.put(CommonConstants.ESIACCOUNTNO, "");
		}
		if(employeeInformation.getType() != null){
			result.put(CommonConstants.ESITYPE, String.valueOf(employeeInformation.getType()));
		}else{
			result.put(CommonConstants.ESITYPE, "");
		}
		if(employeeInformation.getAmount() != null){
			result.put(CommonConstants.ESIAMOUNT, String.valueOf(employeeInformation.getAmount()));
		}else{
			result.put(CommonConstants.ESIAMOUNT, "");
		}
		if(employeeInformation.getFromDate() != null){
			result.put(CommonConstants.ESIFROM, String.valueOf(CommonUtil.convertDateToStringApp(employeeInformation.getFromDate())));
		}else{
			result.put(CommonConstants.ESIFROM, "");
		}
		if(employeeInformation.getToDate() != null){
			result.put(CommonConstants.ESITO, String.valueOf(CommonUtil.convertDateToStringApp(employeeInformation.getToDate())));
		}else{
			result.put(CommonConstants.ESITO, "");
		}
		result.put(CommonConstants.EMPID, String.valueOf(employeeInformation.getEmployee().getEmpId()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(employeeInformation.getUpdatedon())));
		if(employeeInformation.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(employeeInformation.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		return result;
	}
}
