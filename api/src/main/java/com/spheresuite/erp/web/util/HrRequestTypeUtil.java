package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.HrRequestTypeDO;
import com.spheresuite.erp.util.CommonConstants;
@Component
public class HrRequestTypeUtil {
	
	private HrRequestTypeUtil() {}
	
	public static JSONObject getHrRequestTypeList(List<HrRequestTypeDO> hrRequestTypeList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (HrRequestTypeDO hrRequestType : hrRequestTypeList) {
				resultJSONArray.put(getHRRequestTypeDetailObject(hrRequestType));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getHRRequestTypeDetailObject(HrRequestTypeDO hrRequestType)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(hrRequestType.getHrRequestTypeId()));
		result.put(CommonConstants.REQUESTTYPE, String.valueOf(hrRequestType.getRequestType()));
		result.put(CommonConstants.STATUS, String.valueOf(hrRequestType.getStatus()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(hrRequestType.getUpdatedon())));
		if(hrRequestType.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(hrRequestType.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		return result;
	}
}
