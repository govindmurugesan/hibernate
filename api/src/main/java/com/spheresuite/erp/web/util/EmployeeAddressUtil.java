package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.EmployeeAddressDO;
import com.spheresuite.erp.util.CommonConstants;
@Component
public class EmployeeAddressUtil {
	
	private EmployeeAddressUtil() {}

	public static JSONObject getEmployeeAddressList(List<EmployeeAddressDO> employeeAddressList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeeAddressDO employeeAddressDO : employeeAddressList) {
				resultJSONArray.put(getEmployeeAddressDetailObject(employeeAddressDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getEmployeeAddressDetailObject(EmployeeAddressDO employeeAddressDO)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(employeeAddressDO.getEmpAddressInfoId()));
		if(employeeAddressDO.getCountry() != null){
			result.put(CommonConstants.COUNTRY_ID, String.valueOf(employeeAddressDO.getCountry().getCountryId()));
		}
		if(employeeAddressDO.getState() != null){
			result.put(CommonConstants.STATE_ID, String.valueOf(employeeAddressDO.getState().getStateId()));
		}
		if(employeeAddressDO.getCountry() != null){
			result.put(CommonConstants.COUNTRY_NAME, String.valueOf(employeeAddressDO.getCountry().getCountryName()));
		}
		if(employeeAddressDO.getState() != null){
			result.put(CommonConstants.STATE_NAME, String.valueOf(employeeAddressDO.getState().getStateName()));
		}
		result.put(CommonConstants.CITY, String.valueOf(employeeAddressDO.getCity()));
		if(employeeAddressDO.getAddressType() != null){
			result.put(CommonConstants.ADDRESSTYPEID, String.valueOf(employeeAddressDO.getAddressType().getAddressTypeId()));
			result.put(CommonConstants.ADDRESSTYPENAME, String.valueOf(employeeAddressDO.getAddressType().getName()));
		}else{
			result.put(CommonConstants.ADDRESSTYPEID, "");
			result.put(CommonConstants.ADDRESSTYPENAME, "");
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(employeeAddressDO.getUpdatedon())));
		if(employeeAddressDO.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(employeeAddressDO.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.ADDRESS1, String.valueOf(employeeAddressDO.getAddress1()));
		result.put(CommonConstants.ADDRESS2, String.valueOf(employeeAddressDO.getAddress2()!=null?employeeAddressDO.getAddress2():""));
		result.put(CommonConstants.NOTE, String.valueOf(employeeAddressDO.getNotes()!=null?employeeAddressDO.getNotes():""));
		if(employeeAddressDO.getZip() != null){
			result.put(CommonConstants.ZIP, String.valueOf(employeeAddressDO.getZip()));
		}else{
			result.put(CommonConstants.ZIP, "");
		}
		return result;
	}
}
