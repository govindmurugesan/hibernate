package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.DeductionSubTypeDO;
import com.spheresuite.erp.util.CommonConstants;

@Component
public class LoanTypeUtil {
	
	private LoanTypeUtil() {}
	
	public static JSONObject getLoanTypeList(List<DeductionSubTypeDO> loanTypeList){
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (DeductionSubTypeDO loanType : loanTypeList) {
				resultJSONArray.put(getLoanDetailObject(loanType));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getLoanDetailObject(DeductionSubTypeDO loanType)throws JSONException{
		
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(loanType.getDeductionSubTypeId()));
		result.put(CommonConstants.NAME, String.valueOf(loanType.getName()));
		result.put(CommonConstants.STATUS, String.valueOf(loanType.getStatus()));
		if(loanType.getDeductionType() != null){
			result.put(CommonConstants.DEDUCTION_TYPE_ID, String.valueOf(loanType.getDeductionType().getDeductionTypeId()));
			result.put(CommonConstants.DEDUCTIONTYPE_NAME, String.valueOf(loanType.getDeductionType().getDisplayname()));
		}else{
			result.put(CommonConstants.DEDUCTION_TYPE_ID, String.valueOf(" "));
			result.put(CommonConstants.DEDUCTIONTYPE_NAME, String.valueOf(" "));
		}
		if(loanType.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(loanType.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(loanType.getUpdatedon())));
		return result;
	}
}
