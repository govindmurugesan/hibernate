package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.DeductionSettingDO;
import com.spheresuite.erp.util.CommonConstants;
@Component
public class DeductionSettingsUtil {
	
	private DeductionSettingsUtil() {}
	
	public static JSONObject getDeductionSettingsList(List<DeductionSettingDO> deductionSettingList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (DeductionSettingDO deductionSettingDO : deductionSettingList) {
				resultJSONArray.put(getDeductionSettingsDetailObject(deductionSettingDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getDeductionSettingsListForAmount(List<DeductionSettingDO> deductionSettingList, Double ctc,Double basic) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (DeductionSettingDO deductionSettingDO : deductionSettingList) {
				resultJSONArray.put(getDeductionSettingsAmount(deductionSettingDO, ctc, basic));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getDeductionSettingsAmount(DeductionSettingDO deductionSettingDO, Double ctc, Double basic)throws JSONException {
		JSONObject result = new JSONObject();
		Double grossPay = ctc/12;
		Double basicPay = basic;
		Double monthlypay=0D;
		Double ytd=0D;
		if(deductionSettingDO.getType() != null && deductionSettingDO.getType().equalsIgnoreCase("P")){
			if(deductionSettingDO.getBasicgrosspay() != null && deductionSettingDO.getBasicgrosspay().equalsIgnoreCase("B")){
				if(deductionSettingDO.getPercentageamount() != null){
					monthlypay = (basicPay * Double.parseDouble(deductionSettingDO.getPercentageamount()))/100;
					ytd = monthlypay*12;
				}
			}else if(deductionSettingDO.getBasicgrosspay() != null && deductionSettingDO.getBasicgrosspay().equalsIgnoreCase("G")){
				if(deductionSettingDO.getPercentageamount() != null){
					monthlypay = (grossPay * Double.parseDouble(deductionSettingDO.getPercentageamount()))/100;
					ytd = monthlypay*12;
				}
			}
		}else if(deductionSettingDO.getType() != null && deductionSettingDO.getType().equalsIgnoreCase("F")){
			if(deductionSettingDO.getFixedamount() != null){
				ytd = Double.parseDouble(deductionSettingDO.getFixedamount());
				monthlypay = ytd/12;
			}
		}
		
		result.put(CommonConstants.MONTHLY, String.valueOf(monthlypay));
		result.put(CommonConstants.YTD, String.valueOf(ytd));
		
		return result;
	}

	public static JSONObject getDeductionSettingsDetailObject(DeductionSettingDO deductionSettingDO)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(deductionSettingDO.getDeductionsettingId() != null ? deductionSettingDO.getDeductionsettingId() : ""));
		result.put(CommonConstants.NAME, String.valueOf(deductionSettingDO.getName() != null ? deductionSettingDO.getName() : ""));
		result.put(CommonConstants.NOTES, String.valueOf(deductionSettingDO.getNotes() != null ? deductionSettingDO.getNotes() : ""));
		result.put(CommonConstants.PREPOST_TAX, String.valueOf(deductionSettingDO.getPreposttax() != null ? deductionSettingDO.getPreposttax() : ""));
		result.put(CommonConstants.TYPE, String.valueOf(deductionSettingDO.getType() != null ? deductionSettingDO.getType() : ""));
		result.put(CommonConstants.SECTION, String.valueOf(deductionSettingDO.getSection() != null ? deductionSettingDO.getSection() : ""));
		result.put(CommonConstants.FIXEDAMOUNT, String.valueOf(deductionSettingDO.getFixedamount() != null ? deductionSettingDO.getFixedamount() : ""));
		result.put(CommonConstants.PERCENTAGEAMOUNT, String.valueOf(deductionSettingDO.getPercentageamount() != null ? deductionSettingDO.getPercentageamount() : ""));
		result.put(CommonConstants.BASICGROSS_PAY, String.valueOf(deductionSettingDO.getBasicgrosspay() != null ? deductionSettingDO.getBasicgrosspay() : ""));
		if(deductionSettingDO.getUpdatedon() != null){
			result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(deductionSettingDO.getUpdatedon())));
		}else{
			result.put(CommonConstants.UPDATEDON, String.valueOf(""));
		}
		String displayName="";
		if(deductionSettingDO.getType().equalsIgnoreCase("P") ){
			String name="";
			if(deductionSettingDO.getBasicgrosspay().equalsIgnoreCase("G")){
				name = "Gross";
			}else if(deductionSettingDO.getBasicgrosspay().equalsIgnoreCase("B")){
				name = "Basic";
			}
			displayName = deductionSettingDO.getPercentageamount()+"% Of "+name;
		}else{
			displayName = " Rs "+ deductionSettingDO.getFixedamount();
		}
		
		result.put(CommonConstants.SETTINGNAME, String.valueOf(displayName));
		result.put(CommonConstants.DISPLAYNAME, String.valueOf(deductionSettingDO.getDisplayname() != null ? deductionSettingDO.getDisplayname() : ""));
		result.put(CommonConstants.STARTDATE,String.valueOf(deductionSettingDO.getStartdate() != null ? CommonUtil.convertDateToStringWithdatetime(deductionSettingDO.getStartdate()) : ""));
		result.put(CommonConstants.ENDDATE,String.valueOf(deductionSettingDO.getEnddate() != null ? CommonUtil.convertDateToStringWithdatetime(deductionSettingDO.getEnddate()) : ""));
		result.put(CommonConstants.STATUS, String.valueOf(deductionSettingDO.getStatus()));
		
		if(deductionSettingDO.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(deductionSettingDO.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		if(deductionSettingDO.getDeductionType() != null){
				result.put(CommonConstants.DEDUCTIONTYPE_NAME, String.valueOf(deductionSettingDO.getDeductionType().getName()));
				result.put(CommonConstants.DEDUCTION_TYPE_ID, String.valueOf(deductionSettingDO.getDeductionType().getDeductionTypeId()));
		}else{
			result.put(CommonConstants.DEDUCTIONTYPE_NAME,"");
			result.put(CommonConstants.DEDUCTION_TYPE_ID, "");
		}
		return result;
	}
}
