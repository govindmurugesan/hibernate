package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.DeductionTypeDO;
import com.spheresuite.erp.util.CommonConstants;
@Component
public class DeductionTypeUtil {
	
	private DeductionTypeUtil() {}
	
	public static JSONObject geteDeductionTypeList(List<DeductionTypeDO> deductiontypeList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (DeductionTypeDO deductiontype : deductiontypeList) {
				resultJSONArray.put(getDeductionDetailObject(deductiontype));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getDeductionDetailObject(DeductionTypeDO deductiontype)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(deductiontype.getDeductionTypeId()));
		result.put(CommonConstants.NAME, String.valueOf(deductiontype.getName()));
		if(deductiontype.getDisplayname() != null){
			result.put(CommonConstants.DISPLAYNAME, String.valueOf(deductiontype.getDisplayname()));
		}else{
			result.put(CommonConstants.DISPLAYNAME, "");
		}
		result.put(CommonConstants.STATUS, String.valueOf(deductiontype.getStatus()));
		
		if(deductiontype.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(deductiontype.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(deductiontype.getUpdatedon())));
		return result;
	}
}
