package com.spheresuite.erp.web.util;

import java.util.List;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.LeaveManagementDO;
import com.spheresuite.erp.domainobject.WorkingDayAllowanceDO;
import com.spheresuite.erp.domainobject.WorkingDayLeavetypeDO;
import com.spheresuite.erp.service.WorkingDayLeaveTypeService;
import com.spheresuite.erp.util.CommonConstants;

@Component
public class WorkingDayAllowanceUtil {
	
	private WorkingDayAllowanceUtil() {}
	
	@Autowired
	private WorkingDayLeaveTypeService tworkingDayLeaveTypeService;
	private static WorkingDayLeaveTypeService workingDayLeaveTypeService;
	
	@PostConstruct
	public void init() {
		workingDayLeaveTypeService = tworkingDayLeaveTypeService;
	}
	
	public static JSONObject getWorkingDayAllowanceList(List<WorkingDayAllowanceDO> workingDayAllowanceList){
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (WorkingDayAllowanceDO workingDayAllowanceDO : workingDayAllowanceList) {
				resultJSONArray.put(getWorkingDayAllowanceDetailObject(workingDayAllowanceDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getWorkingDayAllowanceDetailObject(WorkingDayAllowanceDO workingDayAllowanceDO)throws JSONException{
		
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(workingDayAllowanceDO.getWorkingDayAllowanceId()));
		result.put(CommonConstants.NAME, String.valueOf(workingDayAllowanceDO.getName()));
		result.put(CommonConstants.STATUS, String.valueOf(workingDayAllowanceDO.getStatus()));
		if(workingDayAllowanceDO.getCost() != null){
			result.put(CommonConstants.COST, String.valueOf(workingDayAllowanceDO.getCost()));
		}else{
			result.put(CommonConstants.COST, String.valueOf(""));
		}
		
		List<WorkingDayLeavetypeDO>  workingdayLeaveList = workingDayLeaveTypeService.retrieveByWorkingDayId(workingDayAllowanceDO.getWorkingDayAllowanceId());
		JSONArray resultJSONArraySubList = new JSONArray();
		if(workingdayLeaveList != null && workingdayLeaveList.size() > 0){
			
			for(int i =0 ; i< workingdayLeaveList.size(); i++ ){
				if(workingdayLeaveList.get(i) != null) resultJSONArraySubList.put(getWorkingDayList(workingdayLeaveList.get(i).getLeaveManagement()));
				
			}
		}
		result.put(CommonConstants.LEAVETYPE, resultJSONArraySubList);
		
		/*if(workingDayAllowanceDO.getCostEffective() != null){
			result.put(CommonConstants.COSTEFFECTIVE, String.valueOf(workingDayAllowanceDO.getCostEffective()));
		}else{
			result.put(CommonConstants.COSTEFFECTIVE, String.valueOf(""));
		}
		if(workingDayAllowanceDO.getCalculationType() != null){
			result.put(CommonConstants.CONDITION, String.valueOf(workingDayAllowanceDO.getCalculationType()));
		}else{
			result.put(CommonConstants.CONDITION, String.valueOf(""));
		}*/
		/*if(workingDayAllowanceDO.getType() != null){
			result.put(CommonConstants.TYPE, String.valueOf(workingDayAllowanceDO.getType()));
		}else{
			result.put(CommonConstants.TYPE, String.valueOf(""));
		}*/
		if(workingDayAllowanceDO.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(workingDayAllowanceDO.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(workingDayAllowanceDO.getUpdatedon())));
		return result;
	}
	
	public static JSONObject getWorkingDayList(LeaveManagementDO leaveManagment)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.TYPEID, String.valueOf(leaveManagment.getLeaveManagementId()));
		
		return result;
	}
	
}
