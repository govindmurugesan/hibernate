package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.EmployeeTerminateDO;
import com.spheresuite.erp.util.CommonConstants;
@Component
public class EmployeeTerminateUtil {
	
	private EmployeeTerminateUtil() {}
	
	public static JSONObject getEmployeeTerminateList(List<EmployeeTerminateDO> employeeTerminateList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeeTerminateDO employeeTerminateDO : employeeTerminateList) {
				resultJSONArray.put(getEmployeeTerminateDetailObject(employeeTerminateDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getEmployeeTerminateDetailObject(EmployeeTerminateDO employeeTerminateDO)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(employeeTerminateDO.getEmpTerminateId()));
		result.put(CommonConstants.COMMENT, String.valueOf(employeeTerminateDO.getComments()));
		if(employeeTerminateDO.getTerminateReason() != null ){
			result.put(CommonConstants.TERMINATETYPEID, String.valueOf(employeeTerminateDO.getTerminateReason().getTerminationReasonId()));
			result.put(CommonConstants.TERMINATEREASON, String.valueOf(employeeTerminateDO.getTerminateReason().getName()));
		}else{
			result.put(CommonConstants.TERMINATETYPEID, "");
			result.put(CommonConstants.TERMINATEREASON, "");
		}
		result.put(CommonConstants.TERMINATEDATE, String.valueOf(CommonUtil.convertDateToStringWithdatetime(employeeTerminateDO.getTerminateDate())));
		result.put(CommonConstants.RESIGNEDDATE, String.valueOf(CommonUtil.convertDateToStringWithdatetime(employeeTerminateDO.getResignDate())));
		result.put(CommonConstants.EMPID, String.valueOf(employeeTerminateDO.getEmployee().getEmpId()));
		if(employeeTerminateDO.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(employeeTerminateDO.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(employeeTerminateDO.getUpdatedon())));
		return result;
	}
}
