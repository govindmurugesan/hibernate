package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.PtDO;
import com.spheresuite.erp.util.CommonConstants;
@Component
public class PtUtil {
	
	private PtUtil() {}
	
	public static JSONObject getPtList(List<PtDO> ptDOList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (PtDO ptDO : ptDOList) {
				resultJSONArray.put(getPtDetailObject(ptDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getPtDetailObject(PtDO ptDO)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(ptDO.getPtId()));
		result.put(CommonConstants.FROMAMOUNT, String.valueOf(ptDO.getFromamount()));
		result.put(CommonConstants.TOAMOUNT, String.valueOf(ptDO.getToamount()));
		result.put(CommonConstants.PTAMOUNT, String.valueOf(ptDO.getPtamount()));
		result.put(CommonConstants.COUNTRY_ID, String.valueOf(ptDO.getCountry().getCountryId()));
		result.put(CommonConstants.STATE_ID, String.valueOf(ptDO.getState().getStateId()));
		result.put(CommonConstants.STATUS, String.valueOf(ptDO.getStatus()));
		result.put(CommonConstants.COUNTRY_NAME, String.valueOf(ptDO.getCountry().getCountryName()));
		result.put(CommonConstants.STATE_NAME, String.valueOf(ptDO.getState().getStateName()));
		if(ptDO.getWorklocation() != null){
			result.put(CommonConstants.WORKLOCATION, String.valueOf(ptDO.getWorklocation().getWorklocationId())); 
			result.put(CommonConstants.WORKLOCATION_CITY, ptDO.getWorklocation().getWorklocation()); 
		}else{
			result.put(CommonConstants.WORKLOCATION, ""); 
			result.put(CommonConstants.WORKLOCATION_CITY, "");
		}
		if(ptDO.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(ptDO.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(ptDO.getUpdatedon())));

		return result;
	}
}
