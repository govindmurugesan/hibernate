package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.EmployeeLeavegroupDO;
import com.spheresuite.erp.util.CommonConstants;
@Component
public class EmployeeLeaveGroupUtil {
	
	private EmployeeLeaveGroupUtil() {}

	public static JSONObject getEmployeeLeavegroupList(List<EmployeeLeavegroupDO> employeeLeavegrpList){
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeeLeavegroupDO employeeLeavegrp : employeeLeavegrpList) {
				resultJSONArray.put(getEmployeeLeaveGroupObject(employeeLeavegrp));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getEmployeeLeaveGroupObject(EmployeeLeavegroupDO employeeLeavegrp)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(employeeLeavegrp.getEmpLeavegroupId()));
		
		if(employeeLeavegrp.getLeaveGroup() != null){
			result.put(CommonConstants.LEAVEGROUP, String.valueOf(employeeLeavegrp.getLeaveGroup().getLeaveGroup()));
			result.put(CommonConstants.LEAVEGROUP_ID, String.valueOf(employeeLeavegrp.getLeaveGroup().getLeaveGroupId()));
		}else{
			result.put(CommonConstants.LEAVEGROUP, "");
			result.put(CommonConstants.LEAVEGROUP_ID,"");
		}
		result.put(CommonConstants.EMPID, String.valueOf(employeeLeavegrp.getEmployee().getEmpId()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(employeeLeavegrp.getUpdatedon())));
		if(employeeLeavegrp.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(employeeLeavegrp.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		return result;
	}
}
