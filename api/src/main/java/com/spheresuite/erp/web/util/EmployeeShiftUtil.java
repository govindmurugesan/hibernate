package com.spheresuite.erp.web.util;

import java.util.List;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.EmployeeShiftDO;
import com.spheresuite.erp.service.EmployeeShiftService;
import com.spheresuite.erp.util.CommonConstants;
@Component
public class EmployeeShiftUtil {
	
	private EmployeeShiftUtil() {}
	
	@Autowired
	private  EmployeeShiftService tempShiftService;
	private static EmployeeShiftService empShiftService;
	
	@PostConstruct
	public void init() {
		empShiftService = tempShiftService;
		
	}
	
	public static JSONObject getShiftList(List<EmployeeShiftDO> shiftList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeeShiftDO shift : shiftList) {
				resultJSONArray.put(getShiftDetailObject(shift));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getShiftDetailObject(EmployeeShiftDO shift)throws JSONException {
		JSONObject result = new JSONObject();
	//	System.out.println("cc"+shift.getState().iterator().next());
		result.put(CommonConstants.ID, String.valueOf(shift.getEmpShiftId()));
		if(shift.getEmployee() != null){
			result.put(CommonConstants.EMPID, String.valueOf(shift.getEmployee().getEmpId()));
			String empName = CommonUtil.getEmpObject(shift.getEmployee().getEmpId());
			result.put(CommonConstants.EMP_NAME, String.valueOf(empName));
		}else{
			result.put(CommonConstants.EMPID, "");
			result.put(CommonConstants.EMP_NAME, "");
		}
		
		if(shift.getShift() != null){
			result.put(CommonConstants.SHIFT_CODE, String.valueOf(shift.getShift().getShiftId()));
			result.put(CommonConstants.SHIFT_NAME, String.valueOf(shift.getShift().getName()));
		}else{
			result.put(CommonConstants.SHIFT_CODE, "");
			result.put(CommonConstants.SHIFT_NAME, "");
		}
		
		if(shift.getFromDate() != null){
			result.put(CommonConstants.FROMDATE, String.valueOf(shift.getFromDate()));
		}else{
			result.put(CommonConstants.FROMDATE, "");
		}
		
		if(shift.getToDate() != null){
			result.put(CommonConstants.TODATE, String.valueOf(shift.getToDate()));
		}else{
			result.put(CommonConstants.TODATE, "");
		}
		
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(shift.getUpdatedon())));
		if(shift.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(shift.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		
		return result;
	}
	
	public static JSONObject getEmpShiftList(List<EmployeeShiftDO> shiftList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeeShiftDO shift : shiftList) {
				resultJSONArray.put(getEmpShiftDetailObject(shift));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getEmpShiftDetailObject(EmployeeShiftDO shift)throws JSONException {
		JSONObject result = new JSONObject();
	//	System.out.println("cc"+shift.getState().iterator().next());
		result.put(CommonConstants.ID, String.valueOf(shift.getEmpShiftId()));
		if(shift.getEmployee() != null){
			result.put(CommonConstants.EMPID, String.valueOf(shift.getEmployee().getEmpId()));
			String empName = CommonUtil.getEmpObject(shift.getEmployee().getEmpId());
			result.put(CommonConstants.EMP_NAME, String.valueOf(empName));
		}else{
			result.put(CommonConstants.EMPID, "");
			result.put(CommonConstants.EMP_NAME, "");
		}
		
		if(shift.getShift() != null){
			result.put(CommonConstants.SHIFT_CODE, String.valueOf(shift.getShift().getShiftId()));
			result.put(CommonConstants.SHIFT_NAME, String.valueOf(shift.getShift().getName()));
		}else{
			result.put(CommonConstants.SHIFT_CODE, "");
			result.put(CommonConstants.SHIFT_NAME, "");
		}
		
		if(shift.getFromDate() != null){
			result.put(CommonConstants.FROMDATE, String.valueOf(shift.getFromDate()));
		}else{
			result.put(CommonConstants.FROMDATE, "");
		}
		
		if(shift.getToDate() != null){
			result.put(CommonConstants.TODATE, String.valueOf(shift.getToDate()));
		}else{
			result.put(CommonConstants.TODATE, "");
		}
		List<EmployeeShiftDO> empList = empShiftService.retrieveByDateValidate(shift.getEmpShiftId());
		if(empList != null && empList.size() > 0){
			result.put(CommonConstants.ISEDIT, "y");
		}else{
			result.put(CommonConstants.ISEDIT, "");
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(shift.getUpdatedon())));
		if(shift.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(shift.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		
		return result;
	}
}
