package com.spheresuite.erp.web.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.Writer;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.annotation.PostConstruct;
import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Part;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;
import javax.mail.util.ByteArrayDataSource;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.io.IOUtils;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.EmailSettingsDO;
import com.spheresuite.erp.domainobject.LeadEmailDO;
import com.spheresuite.erp.service.AttachmentsService;
import com.spheresuite.erp.service.EmailSettingsService;
import com.spheresuite.erp.service.LeadEmailService;
import com.spheresuite.erp.util.CommonConstants;

import freemarker.template.Configuration;
import freemarker.template.TemplateException;
@Component	
public class EmailProxyUtil {
	
	@Autowired
	private AttachmentsService tattachmentsService;
	
	//private static AttachmentsService attachmentsService;
	
	@Autowired
	private LeadEmailService tleadEmailService;
	
	private static LeadEmailService leadEmailService;
	
	@Autowired
	private EmailSettingsService temailSettingsService;
	
	private static EmailSettingsService emailSettingsService;
	
	@PostConstruct
	public void init() {
		leadEmailService = tleadEmailService;
		//attachmentsService=tattachmentsService;
		emailSettingsService = temailSettingsService;
	}
	
	
	public static boolean sendEmail(final HttpServletRequest request, String host, final String fromEmail, String toEmails,
			String ccEmails, String bccEmails, String emailBody, boolean htmlContent, String url, String mailType, String userName) throws AddressException,
			MessagingException, IOException, TemplateException{
		// sets SMTP server properties
		Properties properties = new Properties();
		List<EmailSettingsDO> emailSettingList;
		try {
			Session session = null;
			emailSettingList = emailSettingsService.retrieveActive();
			if(emailSettingList != null && emailSettingList.size() > 0){
				if(emailSettingList.get(0).getImaphost() != null &&
						emailSettingList.get(0).getImapport() != null){
					String userId = emailSettingList.get(0).getSendemail();
					byte[] decodedBytes = Base64.getDecoder().decode(emailSettingList.get(0).getSendpassword());
					String stringDecode = new String(decodedBytes, "UTF-8");
					String pwd = stringDecode.toString();
					if(emailSettingList.get(0).getAuthType() != null && emailSettingList.get(0).getAuthType() == 1){
						userId = emailSettingList.get(0).getSendemail().split("@")[0];
					}else{
						userId = emailSettingList.get(0).getSendemail();
					}
					if (emailSettingList.get(0).getMailtype().contains("Gmail")){
						properties.put(CommonConstants.MAIL_SMTP_HOST, emailSettingList.get(0).getSmtphost());
						properties.put(CommonConstants.MAIL_SMTP_STARTTLS, CommonConstants.TRUE);
						properties.put(CommonConstants.MAIL_SMTP_PORT, emailSettingList.get(0).getSmtpport());
						properties.put(CommonConstants.MAIL_SMTP_AUTH, CommonConstants.TRUE);
				        properties.put(CommonConstants.MAIL_SMTP_STARTTLS, CommonConstants.TRUE);
					} else if (emailSettingList.get(0).getMailtype().contains("Exchange")){
						properties.put(CommonConstants.MAIL_SMTP_HOST, emailSettingList.get(0).getSmtphost());  
						properties.put(CommonConstants.MAIL_SMTP_AUTH, CommonConstants.TRUE);
						if(emailSettingList.get(0).getTlsEnable() != null && emailSettingList.get(0).getTlsEnable() == 0){
							properties.put(CommonConstants.MAIL_SMTP_STARTTLS, CommonConstants.TRUE);
							properties.put(CommonConstants.MAIL_SMTP_PORT, emailSettingList.get(0).getSmtpport());
						}
						
					} else {
						properties.put(CommonConstants.MAIL_SMTP_HOST, emailSettingList.get(0).getSmtphost());  
						properties.put(CommonConstants.MAIL_SMTP_AUTH, CommonConstants.TRUE);
						if(emailSettingList.get(0).getTlsEnable() != null && emailSettingList.get(0).getTlsEnable() == 0){
							properties.put(CommonConstants.MAIL_SMTP_STARTTLS, CommonConstants.TRUE);
							properties.put(CommonConstants.MAIL_SMTP_PORT, emailSettingList.get(0).getSmtpport());
						}
					}
					// creates a new session with an authenticator
					final String user = userId;//.split("@")[0];
					final String password = pwd;
								Authenticator auth = new Authenticator() {
									
									public PasswordAuthentication getPasswordAuthentication() {
										PasswordAuthentication passwordAuthentication = null;
										try {
											passwordAuthentication = new PasswordAuthentication(user,password);
										} catch (Exception e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
										return passwordAuthentication;
									}
								};
						
								session = Session.getInstance(properties, auth);
								
			
					//Session session = Session.getInstance(properties,null);
			
					// Create Message
					Message msg = new MimeMessage(session);
					msg.setFrom(new InternetAddress(emailSettingList.get(0).getSendemail()));
					
					msg.setSentDate(new Date());
			        // Create the message part
			        BodyPart messageBodyPart = new MimeBodyPart();
			        Configuration cfg = new Configuration();
			        cfg.setDirectoryForTemplateLoading(new File(request.getServletContext().getRealPath("resources/templates")));
			        freemarker.template.Template template = null;
			        if(mailType.equalsIgnoreCase("inviteUser")){
			        	msg.setSubject("Welcome to SphereSuite");
			        	template = cfg.getTemplate("inviteUser.ftl");
			        }
			        
			        if(mailType.equalsIgnoreCase("onboardEmployee")){
			        	msg.setSubject("Welcome to SphereSuite");
			        	template = cfg.getTemplate("inviteUser.ftl");
			        }
			        
			        if(mailType.equalsIgnoreCase("onboardSupplier")){
			        	msg.setSubject("Welcome to SphereSuite");
			        	template = cfg.getTemplate("inviteUser.ftl");
			        }
			        
			        if(mailType.equalsIgnoreCase("resetPassword")) {
			        	msg.setSubject("Reset Password");
			        	template = cfg.getTemplate("resetPassword.ftl");
			        }
			        
			        if(mailType.equalsIgnoreCase("reInvite")) {
			        	msg.setSubject("Welcome to SphereSuite");
			        	template = cfg.getTemplate("reInvite.ftl");
			        }
			        Map<String, String> rootMap = new HashMap<String, String>();
			        rootMap.put("link",url);
			        rootMap.put("userName",userName);
			        
			        if(mailType.equalsIgnoreCase("offerApproval")){
			        	msg.setSubject("New offer from "+userName+" is pending for approval");
			        	template = cfg.getTemplate("offerApproval.ftl");
			        	rootMap.put("userName",ccEmails);
			        }
			        
			        Writer out = new StringWriter();
			        template.process(rootMap, out);
			        messageBodyPart.setContent(out.toString(), "text/html");
			       
			        // Create a multipart message
			        Multipart multipart = new MimeMultipart();
			        multipart.addBodyPart(messageBodyPart);
			        msg.setContent(multipart);
					// To Email List
					List<String> toEmailList = Arrays.asList(toEmails.split(CommonConstants.COMMA));
					if(toEmailList != null){
						for(String toEmail : toEmailList ){
							if(emailValidation(toEmail.trim())){
								msg.addRecipient(Message.RecipientType.TO, new InternetAddress(toEmail.trim()));			
							}
						}
					}
					
			
					// CC Email List
					/*	List<String> ccEmailList = Arrays.asList(ccEmails.split(CommonConstants.COMMA));
					if(toEmailList != null){
						for(String ccEmail : ccEmailList ){
							if(emailValidation(ccEmail.trim())){
								msg.addRecipient(Message.RecipientType.CC, new InternetAddress(ccEmail.trim()));			
							}
						}
					}
			
					//BCC Email List
					List<String> bccEmailList = Arrays.asList(bccEmails.split(CommonConstants.COMMA));
					if(toEmailList != null){
						for(String bccEmail : bccEmailList ){
							if(emailValidation(bccEmail.trim())){
								msg.addRecipient(Message.RecipientType.BCC, new InternetAddress(bccEmail.trim()));			
							}
						}
					}
					*/
			
					// sends the e-mail
					Transport.send(msg);				
					
				}
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		return true;
	}
	
	public static boolean  emailValidation(String email){
		
		String EMAIL_REGEX = CommonConstants.REG_EXP;
		return email.matches(EMAIL_REGEX)? true : false;
	}
	
	@SuppressWarnings("resource")
	public static boolean sendEmails(final HttpServletRequest request, final String fromEmail, String toEmails,
			String ccEmails, String bccEmails, String subject, String emailBody, final String password, String type, String[] fileUrl, String[] fileName, String[] fileType) throws IOException, NumberFormatException, MessagingException {
		
			// sets SMTP server properties
			Properties properties = new Properties();
			Session session = null;
			List<EmailSettingsDO> emailSettingList;
			try {
				emailSettingList = emailSettingsService.retrieveByMailType(type);
				if(emailSettingList != null && emailSettingList.size() > 0){
					if(emailSettingList.get(0).getSmtphost() != null &&
							emailSettingList.get(0).getSmtpport() != null){
						if(type.equalsIgnoreCase("Exchange")){
							properties.put(CommonConstants.MAIL_SMTP_HOST, emailSettingList.get(0).getImaphost());  
							properties.put(CommonConstants.MAIL_SMTP_AUTH, CommonConstants.TRUE);
							// creates a new session with an authenticator
							Authenticator auth = new Authenticator() {
								
								public PasswordAuthentication getPasswordAuthentication() {
									PasswordAuthentication passwordAuthentication = null;
									try {
										passwordAuthentication = new PasswordAuthentication(fromEmail.split("@")[0],password);
									} catch (Exception e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									return passwordAuthentication;
								}
							};
							session = Session.getInstance(properties, auth);
						} else if(type.equalsIgnoreCase("Gmail")){
							properties.put(CommonConstants.MAIL_SMTP_HOST, emailSettingList.get(0).getSmtphost());
							properties.put(CommonConstants.MAIL_SMTP_STARTTLS, CommonConstants.TRUE);
							properties.put(CommonConstants.MAIL_SMTP_PORT, emailSettingList.get(0).getSmtpport());
							properties.put(CommonConstants.MAIL_SMTP_AUTH, CommonConstants.TRUE);
					        properties.put(CommonConstants.MAIL_SMTP_STARTTLS, CommonConstants.TRUE);
					        // creates a new session with an authenticator
					        session = Session.getDefaultInstance(properties,  
					    		    new javax.mail.Authenticator() {  
					    		      protected PasswordAuthentication getPasswordAuthentication() {  
					    		    return new PasswordAuthentication(fromEmail,password);  
					    		      }  
					    		    });
							//session = Session.getInstance(properties, auth);
						} else {
							properties.put(CommonConstants.MAIL_SMTP_HOST, emailSettingList.get(0).getImaphost());  
							properties.put(CommonConstants.MAIL_SMTP_AUTH, CommonConstants.TRUE);
							// creates a new session with an authenticator
							Authenticator auth = new Authenticator() {
								
								public PasswordAuthentication getPasswordAuthentication() {
									PasswordAuthentication passwordAuthentication = null;
									try {
										passwordAuthentication = new PasswordAuthentication(fromEmail,password);
									} catch (Exception e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									return passwordAuthentication;
								}
							};
							session = Session.getInstance(properties, auth);
						}
						
				
						//List<AttachmentsDO> attachmentsList = attachmentsService.retrieve();
						// creates a new e-mail message
						Message msg = new MimeMessage(session);
				
						msg.setFrom(new InternetAddress(fromEmail));
						
						// To Email List
						List<String> toEmailList = Arrays.asList(toEmails.split(CommonConstants.COMMA));
						if(toEmailList != null){
							for(String toEmail : toEmailList ){
								if(emailValidation(toEmail.trim())){
									msg.addRecipient(Message.RecipientType.TO, new InternetAddress(toEmail.trim()));			
								}
							}
						}
						
						// CC Email List
						List<String> ccEmailList = Arrays.asList(ccEmails.split(CommonConstants.COMMA));
						if(ccEmailList != null){
							for(String ccEmail : ccEmailList ){
								if(emailValidation(ccEmail.trim())){
									msg.addRecipient(Message.RecipientType.CC, new InternetAddress(ccEmail.trim()));			
								}
							}
						}
			
						//BCC Email List
						List<String> bccEmailList = Arrays.asList(bccEmails.split(CommonConstants.COMMA));
						if(bccEmailList != null){
							for(String bccEmail : bccEmailList ){
								if(emailValidation(bccEmail.trim())){
									msg.addRecipient(Message.RecipientType.BCC, new InternetAddress(bccEmail.trim()));			
								}
							}
						}
						
						
						msg.setSubject(subject);
						msg.setSentDate(new Date());
						
						// Create Multipart E-Mail.
						Multipart multipart = new MimeMultipart();
						
						// Set the email msg text.
						/*BodyPart messagePart = new MimeBodyPart();
			            messagePart.setText(emailBody);*/
			            BodyPart messagePart = new MimeBodyPart();
			            messagePart.setText(emailBody);
			            multipart.addBodyPart(messagePart);
						if(fileUrl != null && fileUrl.length > 0){
							for(int i=0; i<fileUrl.length; i++){
						        // Create the message part
								MimeBodyPart attachmentPart = new MimeBodyPart();
						            
						        // Part two is attachment
						        DataSource source;
						        InputStream in = new ByteArrayInputStream(fileUrl[i].getBytes("UTF-8"));
						        try {
						            in = MimeUtility.decode(in, "base64");
						            System.out.println("mimeType  2 "+fileType[i]);
						            try {
						            	source = new ByteArrayDataSource(in , fileType[i]);
						            } finally {
						                in.close();
						            }
						        } catch (IOException ioe) {
						            throw new MessagingException(fileUrl[i], ioe);
						        }
				
						        // "Attachment"
						        attachmentPart.setDataHandler(new DataHandler(source));
					            attachmentPart.setFileName(fileName[i]);			        
					         // Create Multipart E-Mail.
					            //multipart.addBodyPart(messagePart);
					            multipart.addBodyPart(attachmentPart);
			
							}
							msg.setContent(multipart);
						} else {
							msg.setContent(multipart);
						}
						// Send message
				        Transport.send(msg);
					}
					return true;
				}
		
			}catch(Exception ex){
				ex.printStackTrace();
				System.out.println("Exception   " + ex.getMessage());
				return false;
			}
		return true;
	}
	
	@SuppressWarnings("unused")
	public static List<LeadEmailDO> getAllEmails(JSONObject inputJSON, Message[] arrayMessages) throws Exception {
		List<LeadEmailDO> leadEmailDOlist = new ArrayList<LeadEmailDO>();
		try {
			for (int i = arrayMessages.length-1; i >= 0 ; i--) {
				Message message = arrayMessages[i];
				Address[] fromAddress = message.getFrom();
				String from = fromAddress[0].toString();
				String subject = message.getSubject();
				String sentDate = message.getSentDate().toString();
				String messageContent = "";
				String attachFiles = "";
				Object content = message.getContent();
				LeadEmailDO leadEmailDO = new LeadEmailDO();
				
				 if (content instanceof String) {
		                messageContent = content.toString();
		            } else if (content instanceof Multipart) {
		                Multipart multiPart = (Multipart) content;
		                for (int j = 0; j < multiPart.getCount(); j++) {
		                    MimeBodyPart bodyPart = (MimeBodyPart) multiPart.getBodyPart(j);
		                    Object o = bodyPart.getContent();
		                    String msgContent = getText(multiPart.getBodyPart(j));
	                    	if(msgContent != null ){
	                    		messageContent = msgContent;
	                    	}
		                    
		                    if (o instanceof String) {
		                        messageContent = o.toString();
		                    } else if (null != bodyPart.getDisposition()
		                            && bodyPart.getDisposition().equalsIgnoreCase(
		                                    Part.ATTACHMENT)) {
		                    	
		                        String fileName = bodyPart.getFileName();
		                        leadEmailDO.setAttachFiles(fileName);
		                        InputStream inStream = bodyPart.getInputStream();
		                        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		                        byte[] tempBuffer = new byte[4096];// 4 KB
		                        int numRead;
		                        while ((numRead = inStream.read(tempBuffer)) != -1) {
		                            outStream.write(tempBuffer);
		                        }
		                        byte[] attachment = outStream.toByteArray();
                                if(attachment.length > 1){
                                     leadEmailDO.setAttachmentFile(attachment);
                                }
                                inStream.close();
		                        outStream.close();
		                    }
		                }
		            }
				    
				    Date date = null;
			        date = CommonUtil.convertEmailDate(sentDate);
				    String formattedDate = "";
				    if( date != null ) {
				    	formattedDate = CommonUtil.convertEmailDateToDate( date );
				    }
		 			leadEmailDO.setContactId(null);
		 			leadEmailDO.setFromAddress(from);
			 		
			 		leadEmailDO.setSubject(subject);
			 		
			 		leadEmailDO.setMessageContent(messageContent);
			 		if(!attachFiles.isEmpty() && attachFiles.length() > 0){
			 			leadEmailDO.setAttachFiles(attachFiles);
			 		}
			 		
			 		
			 		leadEmailDO.setSentDate(CommonUtil.convertStringToDateTimeFmt(formattedDate));
			 					 		
			 		leadEmailDO.setUpdatedon(new Date());
			 		
			 		leadEmailDOlist.add(leadEmailDO);
			}
			leadEmailService.persistList(leadEmailDOlist);
		} catch (NoSuchProviderException ex) {
			ex.printStackTrace();
		} catch (MessagingException ex) {
			ex.printStackTrace();
		} 
		return leadEmailDOlist;
	}
	
	
	@SuppressWarnings("unused")
	public static List<LeadEmailDO> getEmailBasedOnDate(JSONObject inputJSON, Message[] arrayMessages
			,java.util.Date utilDate,String email) throws ParseException, IOException{
		List<LeadEmailDO> leadEmailDOlist = new ArrayList<LeadEmailDO>();
		try {
			for (int i = arrayMessages.length-1; i >= 0 ; i--) {
				Message message = arrayMessages[i];
		        String dt=message.getSentDate().toString();
		        java.util.Date dDate = CommonUtil.convertEmailDate(dt);
				if (dDate.after(utilDate)){
					Address[] fromAddress = message.getFrom();
					String from = fromAddress[0].toString();
					String subject = message.getSubject();
					String sentDate = message.getSentDate().toString();
					String messageContent = "";
					String attachFiles = "";
					Object content = message.getContent();
					LeadEmailDO leadEmailDO = new LeadEmailDO();
					if (content instanceof String) {
		                messageContent = content.toString();
	            } else if (content instanceof Multipart) {
		                Multipart multiPart = (Multipart) content;
		                for (int j = 0; j < multiPart.getCount(); j++) {
		                    MimeBodyPart bodyPart = (MimeBodyPart) multiPart.getBodyPart(j);
		                    Object o = bodyPart.getContent();
		                    String msgContent = getText(multiPart.getBodyPart(j));
	                    	if(msgContent != null ){
	                    		messageContent = msgContent;
	                    		
	                    	}
		                    
		                    if (o instanceof String) {
		                        messageContent = o.toString();
		                    } else if (null != bodyPart.getDisposition()
		                            && bodyPart.getDisposition().equalsIgnoreCase(
		                                    Part.ATTACHMENT)) {
		                        String fileName = bodyPart.getFileName();
		                        leadEmailDO.setAttachFiles(fileName);
		                        InputStream inStream = bodyPart.getInputStream();
		                        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		                        byte[] tempBuffer = new byte[4096];// 4 KB
		                        int numRead;
		                        while ((numRead = inStream.read(tempBuffer)) != -1) {
		                            outStream.write(tempBuffer);
		                        }
		                       
		                         byte[] attachment = outStream.toByteArray();
	                                if(attachment.length > 1){
	                                     leadEmailDO.setAttachmentFile(attachment);
	                                }
	                                inStream.close();
			                        outStream.close();
		                         
		                    }
		                }
		            }
					
				 	Date date = null;
			        date = CommonUtil.convertEmailDate(sentDate);
				    String formattedDate = "";
				    if( date != null ) {
				    	formattedDate = CommonUtil.convertEmailDateToDate( date );
				    }
		 			leadEmailDO.setContactId(null);
		 			leadEmailDO.setFromAddress(from);
			 		
			 		leadEmailDO.setSubject(subject);
			 		
			 		leadEmailDO.setMessageContent(messageContent);
			 		if(!attachFiles.isEmpty() && attachFiles.length() > 0){
			 			leadEmailDO.setAttachFiles(attachFiles);
			 		}
			 		leadEmailDO.setSentDate(CommonUtil.convertStringToDateTimeFmt(formattedDate));
			 					 		
			 		leadEmailDO.setUpdatedon(new Date());
			 		
			 		leadEmailDOlist.add(leadEmailDO);
			}
				
		}
	} catch (NoSuchProviderException ex) {
			ex.printStackTrace();
		} catch (MessagingException ex) {
			ex.printStackTrace();
		} 		return leadEmailDOlist;
}
	
	public static List<LeadEmailDO> getAllInbox(Message[] arrayMessages) throws Exception {
		List<LeadEmailDO> leadEmailDOlist = new ArrayList<LeadEmailDO>();
		try {
			for (int i = arrayMessages.length-1; i >= 0 ; i--) {
				Message message = arrayMessages[i];
				Address[] fromAddress = message.getFrom();
				String from = fromAddress[0].toString();
				String subject = message.getSubject();
				String sentDate = message.getSentDate().toString();
				String messageContent = "";
				String attachFiles = "";
				Object content = message.getContent();
				LeadEmailDO leadEmailDO = new LeadEmailDO();
				
				 if (content instanceof String) {
		                messageContent = content.toString();
		            } else if (content instanceof Multipart) {
		                Multipart multiPart = (Multipart) content;
		                for (int j = 0; j < multiPart.getCount(); j++) {
		                    MimeBodyPart bodyPart = (MimeBodyPart) multiPart.getBodyPart(j);
		                    Object o = bodyPart.getContent();
		                    String msgContent = getText(multiPart.getBodyPart(j));
	                    	if(msgContent != null ){
	                    		messageContent = msgContent;
	                    	}
		                    
		                    if (o instanceof String) {
		                        messageContent = o.toString();
		                    } else if (null != bodyPart.getDisposition()
		                            && bodyPart.getDisposition().equalsIgnoreCase(
		                                    Part.ATTACHMENT)) {
		                    	
		                        String fileName = bodyPart.getFileName();
		                        leadEmailDO.setAttachFiles(fileName);
		                        InputStream inStream = bodyPart.getInputStream();
		                       /* ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		                        
		                        byte[] tempBuffer = new byte[4096];// 4 KB
		                        int numRead;
		                        while ((numRead = inStream.read(tempBuffer)) != -1) {
		                            outStream.write(tempBuffer);
		                        }
		                        */
		                        byte[] bytes = IOUtils.toByteArray(inStream);
		                        leadEmailDO.setContentType(bodyPart.getContentType());
		                        leadEmailDO.setAttachment(DatatypeConverter.printBase64Binary(bytes));
                                inStream.close();
		                    }
		                }
		            }
				    
				    Date date = null;
			        date = CommonUtil.convertEmailDate(sentDate);
				    String formattedDate = "";
				    if( date != null ) {
				    	formattedDate = CommonUtil.convertEmailDateToDate( date );
				    }
		 			leadEmailDO.setContactId(null);
		 			leadEmailDO.setFromAddress(from);
			 		
			 		leadEmailDO.setSubject(subject);
			 		
			 		leadEmailDO.setMessageContent(messageContent);
			 		if(!attachFiles.isEmpty() && attachFiles.length() > 0){
			 			leadEmailDO.setAttachFiles(attachFiles);
			 		}
			 		
			 		
			 		leadEmailDO.setSentDate(CommonUtil.convertStringToDateTimeFmt(formattedDate));
			 					 		
			 		leadEmailDO.setUpdatedon(new Date());
			 		
			 		leadEmailDOlist.add(leadEmailDO);
			}
			return leadEmailDOlist;
		} catch (NoSuchProviderException ex) {
			ex.printStackTrace();
		} catch (MessagingException ex) {
			ex.printStackTrace();
		} 
		return leadEmailDOlist;
	}
	private static String getText(Part p) throws MessagingException, IOException {
		if (p.isMimeType("text/*")) {
			String s = (String)p.getContent();
			return s;
		}

		if (p.isMimeType("multipart/alternative")) {
			Multipart mp = (Multipart)p.getContent();
			String text = null;
			for (int i = 0; i < mp.getCount(); i++) {
			    Part bp = mp.getBodyPart(i);
			    if (bp.isMimeType("text/plain")) {
			        if (text == null)
			            text = getText(bp);
			        continue;
			    } else if (bp.isMimeType("text/html")) {
			        String s = getText(bp);
			        if (s != null)
			            return s;
			    } else {
			        return getText(bp);
			    }
			}
		return text;
		} else if (p.isMimeType("multipart/*")) {
			Multipart mp = (Multipart)p.getContent();
			for (int i = 0; i < mp.getCount(); i++) {
				String s = getText(mp.getBodyPart(i));
				if (s != null)
					return s;
				}
		}

		return null;
	}

	public static boolean notificationEmail(final HttpServletRequest request, final String fromEmail, ArrayList<String> toEmails,
			ArrayList<String> ccEmails, String bccEmails, String emailBody, boolean htmlContent, String url, String mailType, String userName, String requestBy, String status, String fromDate, String toDate) throws AddressException,
			MessagingException, IOException, TemplateException{
		Properties properties = new Properties();
		List<EmailSettingsDO> emailSettingList;
		try {
			Session session = null;
			emailSettingList = emailSettingsService.retrieveActive();
			if(emailSettingList != null && emailSettingList.size() > 0){
				if(emailSettingList.get(0).getImaphost() != null &&
						emailSettingList.get(0).getImapport() != null){
					String userId = emailSettingList.get(0).getSendemail();
					byte[] decodedBytes = Base64.getDecoder().decode(emailSettingList.get(0).getSendpassword());
					String stringDecode = new String(decodedBytes, "UTF-8");
					String pwd = stringDecode.toString();
					if (emailSettingList.get(0).getMailtype().contains("Gmail")){
						properties.put(CommonConstants.MAIL_SMTP_HOST, emailSettingList.get(0).getSmtphost());
						properties.put(CommonConstants.MAIL_SMTP_STARTTLS, CommonConstants.TRUE);
						properties.put(CommonConstants.MAIL_SMTP_PORT, emailSettingList.get(0).getSmtpport());
						properties.put(CommonConstants.MAIL_SMTP_AUTH, CommonConstants.TRUE);
				        properties.put(CommonConstants.MAIL_SMTP_STARTTLS, CommonConstants.TRUE);
					} else if (emailSettingList.get(0).getMailtype().contains("Exchange")){
						properties.put(CommonConstants.MAIL_SMTP_HOST, emailSettingList.get(0).getSmtphost());  
						properties.put(CommonConstants.MAIL_SMTP_AUTH, CommonConstants.TRUE);
						//userId = emailSettingList.get(0).getSendemail().split("@")[0];
						if(emailSettingList.get(0).getTlsEnable() != null && emailSettingList.get(0).getTlsEnable() == 0){
							properties.put(CommonConstants.MAIL_SMTP_STARTTLS, CommonConstants.TRUE);
							properties.put(CommonConstants.MAIL_SMTP_PORT, emailSettingList.get(0).getSmtpport());
						}
						if(emailSettingList.get(0).getAuthType() != null && emailSettingList.get(0).getAuthType() == 1){
							userId = emailSettingList.get(0).getSendemail().split("@")[0];
						}else{
							userId = emailSettingList.get(0).getSendemail();
						}
					} else {
						if(emailSettingList.get(0).getTlsEnable() != null && emailSettingList.get(0).getTlsEnable() == 0){
							properties.put(CommonConstants.MAIL_SMTP_STARTTLS, CommonConstants.TRUE);
							properties.put(CommonConstants.MAIL_SMTP_PORT, emailSettingList.get(0).getSmtpport());
						}
						properties.put(CommonConstants.MAIL_SMTP_HOST, emailSettingList.get(0).getSmtphost());  
						properties.put(CommonConstants.MAIL_SMTP_AUTH, CommonConstants.TRUE);
					}
					// creates a new session with an authenticator
					final String user = userId;//.split("@")[0];
					final String password = pwd;
								Authenticator auth = new Authenticator() {
									
									public PasswordAuthentication getPasswordAuthentication() {
										PasswordAuthentication passwordAuthentication = null;
										try {
											passwordAuthentication = new PasswordAuthentication(user,password);
										} catch (Exception e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
										return passwordAuthentication;
									}
								};
						
								session = Session.getInstance(properties, auth);
								
			
			
					// Create Message
					Message msg = new MimeMessage(session);
					msg.setFrom(new InternetAddress(emailSettingList.get(0).getSendemail()));
					
					msg.setSentDate(new Date());
			        // Create the message part
			        BodyPart messageBodyPart = new MimeBodyPart();
			        Configuration cfg = new Configuration();
			        cfg.setDirectoryForTemplateLoading(new File(request.getServletContext().getRealPath("resources/templates")));
			        freemarker.template.Template template = null;
			        Map<String, String> rootMap = new HashMap<String, String>();
			        
			        if(mailType.equalsIgnoreCase("hrrequests")){
			        	//msg.setSubject("New HR request Created by "+requestBy);
			        	
			        	if(status.equalsIgnoreCase("pending")){
			        		msg.setSubject("New HR Request Created By "+requestBy);
			        		  rootMap.put("requestMsg","New HR Request by "+requestBy+" waiting for your approval");
			        		  rootMap.put("bodySection","Request: "+emailBody);
			        	}
			        	if(status.equalsIgnoreCase("cancel")){
			        		msg.setSubject("HR Request Cancelled" );
			        		rootMap.put("requestMsg","Your HR Request cancelled");
			        		rootMap.put("bodySection","Comment: "+emailBody);
			        	}
			        	if(status.equalsIgnoreCase("approved")){
			        		msg.setSubject("HR Request approved");
			        		rootMap.put("requestMsg","Your HR Request approved");
			        		rootMap.put("bodySection","Comment: "+emailBody);
			        	}
			        	if(status.equalsIgnoreCase("reject")){
			        		msg.setSubject("HR Request rejected");
			        		rootMap.put("requestMsg","Your HR Request rejected");
			        		rootMap.put("bodySection","Comment: "+emailBody);
			        	}
			        	
			        	template = cfg.getTemplate("hrRequestNotification.ftl");
			        }
			        
			        if(mailType.equalsIgnoreCase("leaveRequest")){
			        	if(status.equalsIgnoreCase("pending")){
			        		msg.setSubject("New Leave Request From "+requestBy);
			        		  rootMap.put("requestMsg","New leave request from "+requestBy+" waiting for your approval");
			        		  rootMap.put("requestTo","Hello,");
			        		  if(fromDate != null && toDate != null){
			        			  rootMap.put("date","Date : "+ fromDate + " - " + toDate);
			        		  }else{
			        			  rootMap.put("date","");
			        		  }
			        	}
			        	if(status.equalsIgnoreCase("cancel Requrst")){
			        		msg.setSubject("New Cancel Request From "+requestBy);
			        		rootMap.put("requestMsg","Cancel leave request from "+requestBy+" waiting for your approval");
			        		rootMap.put("requestTo","Hello,");
			        		if(fromDate != null && toDate != null){
			        			  rootMap.put("date","Date : "+ fromDate + " - " + toDate);
			        		  }else{
			        			  rootMap.put("date","");
			        		  }
			        	}
			        	if(status.equalsIgnoreCase("cancel")){
			        		msg.setSubject("Your Leave Request Cancelled");
			        		rootMap.put("requestMsg","Your leave request cancelled");
			        		rootMap.put("requestTo","Hello "+requestBy+",");
			        		if(fromDate != null && toDate != null){
			        			  rootMap.put("date","Date : "+ fromDate + " - " + toDate);
			        		  }else{
			        			  rootMap.put("date","");
			        		  }
			        	}
			        	if(status.equalsIgnoreCase("approved")){
			        		msg.setSubject("Your Leave Request Approved");
			        		rootMap.put("requestMsg","Your leave request approved");
			        		rootMap.put("requestTo","Hello "+requestBy+",");
			        		if(fromDate != null && toDate != null){
			        			  rootMap.put("date","Date : "+ fromDate + " - " + toDate);
			        		  }else{
			        			  rootMap.put("date","");
			        		  }
			        	}
			        	if(status.equalsIgnoreCase("rejected")){
			        		msg.setSubject("Your Leave Request Rejected");
			        		rootMap.put("requestMsg","Your leave request rejected");
			        		rootMap.put("requestTo","Hello "+requestBy+",");
			        		if(fromDate != null && toDate != null){
			        			  rootMap.put("date","Date : "+ fromDate + " - " + toDate);
			        		  }else{
			        			  rootMap.put("date","");
			        		  }
			        	}
			        	template = cfg.getTemplate("leaveRequestNotification.ftl");
			        }
			        
			        if(mailType.equalsIgnoreCase("hrpolicies")) {
			        	if(status.equalsIgnoreCase("pending")){
			        		msg.setSubject("New HR Policy Created By "+requestBy);
			        		  rootMap.put("requestMsg","New HR policy created by "+requestBy+" waiting for your approval");
			        		  rootMap.put("bodySection","Policy: "+emailBody);
			        	}
			        	if(status.equalsIgnoreCase("cancel")){
			        		msg.setSubject("HR Policy Cancelled" );
			        		rootMap.put("requestMsg","Your HR policy Cancelled ");
			        		rootMap.put("bodySection","Comment: "+emailBody);
			        	}
			        	if(status.equalsIgnoreCase("approved")){
			        		msg.setSubject("HR Policy approved");
			        		rootMap.put("requestMsg","Your HR policy approved");
			        		rootMap.put("bodySection","Comment: "+emailBody);
			        	}
			        	if(status.equalsIgnoreCase("rejected")){
			        		msg.setSubject("HR Policy rejected");
			        		rootMap.put("requestMsg","Your HR Policy rejected");
			        		rootMap.put("bodySection","Comment: "+emailBody);
			        	}
			        	template = cfg.getTemplate("hrPoliciesNotification.ftl");
			        }
			        
			        if(mailType.equalsIgnoreCase("holiday")) {
			        	//msg.setSubject("New Holiday added to calender");
			        	template = cfg.getTemplate("holidayNotification.ftl");
			        	if(status.equalsIgnoreCase("pending")){
			        		msg.setSubject("New Holiday added to calender");
			        		rootMap.put("requestMsg","New hoilday is created by "+requestBy+" waiting for your approval");
			        		rootMap.put("bodySection","Description: "+emailBody);
			        	}
			        	if(status.equalsIgnoreCase("approved")){
			        		msg.setSubject("Holiday approved");
			        		rootMap.put("requestMsg","Holiday approved");
			        		rootMap.put("bodySection","Comment: "+emailBody);
			        	}
			        	if(status.equalsIgnoreCase("reject")){
			        		msg.setSubject("Holiday rejected");
			        		rootMap.put("requestMsg","Holiday rejected");
			        		rootMap.put("bodySection","Comment: "+emailBody);
			        	}
			        }
			        
			        if(mailType.equalsIgnoreCase("greigeApproval")){
			        	if(status.equalsIgnoreCase("submit")){
			        		  msg.setSubject("Submitted Quote "+requestBy);
			        		  rootMap.put("requestTo","Hi,");
			        	}
			        	template = cfg.getTemplate("quoteApproval.ftl");
			        }
			        
			        if(mailType.equalsIgnoreCase("suppliers")){
			        	if(status.equalsIgnoreCase("submit")){
			        		  msg.setSubject("Quote "+requestBy);
			        		  rootMap.put("requestTo","Hi,");
			        	}
			        	template = cfg.getTemplate("quoteApproval.ftl");
			        }
			        
			    //    Map<String, String> rootMap = new HashMap<String, String>();
			        if(url != null && !url.toString().isEmpty()){
			        	rootMap.put("link",url);
			        }
			        if(userName != null && !userName.toString().isEmpty()){
			        	rootMap.put("userName",userName);
			        }
			        if(emailBody != null && !emailBody.toString().isEmpty()){
			        	rootMap.put("comments",emailBody);
			        }
			        if(requestBy != null && !requestBy.toString().isEmpty()){
			        	rootMap.put("requestBy",requestBy);
			        }
			        Writer out = new StringWriter();
			        template.process(rootMap, out);
			        messageBodyPart.setContent(out.toString(), "text/html");
			       
			        // Create a multipart message
			        Multipart multipart = new MimeMultipart();
			        multipart.addBodyPart(messageBodyPart);
			        msg.setContent(multipart);
					// To Email List
			        
			        if(toEmails != null){
						for(String toEmail : toEmails ){
							if(emailValidation(toEmail.trim())){
								msg.addRecipient(Message.RecipientType.TO, new InternetAddress(toEmail.trim()));			
							}
						}
					}
			        
			        if(ccEmails != null){
				        for(String ccEmail : ccEmails ){
							if(ccEmail != null){
								if(emailValidation(ccEmail.trim())){
									msg.addRecipient(Message.RecipientType.CC, new InternetAddress(ccEmail.trim()));			
								}
							}
						}
			        }
			        
					// sends the e-mail
					Transport.send(msg);				
					
				}
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		return true;
		
	}

	/*public static boolean notificationEmail1(final HttpServletRequest request, String host, final String fromEmail, ArrayList<String> toEmails,
			ArrayList<String> ccEmails, String bccEmails, String emailBody, boolean htmlContent, String url, String mailType, String userName, String requestBy) throws AddressException,
			MessagingException, IOException, TemplateException{
		// sets SMTP server properties
		Properties properties = new Properties();
		properties.put(CommonConstants.MAIL_SMTP_HOST, host);
		properties.put(CommonConstants.MAIL_SMTP_STARTTLS, CommonConstants.TRUE);
		properties.put(CommonConstants.MAIL_SMTP_PORT, request.getServletContext().getInitParameter(CommonConstants.PORT));
		properties.put(CommonConstants.MAIL_SMTP_AUTH, CommonConstants.TRUE);
        properties.put(CommonConstants.MAIL_SMTP_STARTTLS, CommonConstants.TRUE);
		// creates a new session with an authenticator
		Authenticator auth = new Authenticator() {
			
			public PasswordAuthentication getPasswordAuthentication() {
				PasswordAuthentication passwordAuthentication = null;
				try {
					passwordAuthentication = new PasswordAuthentication(CommonConstants.SENDMAIL,CommonConstants.SENDPASSWORD);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return passwordAuthentication;
			}
		};

		Session session = Session.getInstance(properties, auth);

		//Session session = Session.getInstance(properties,null);

		// Create Message
		Message msg = new MimeMessage(session);
		msg.setFrom(new InternetAddress(fromEmail));
		
		msg.setSentDate(new Date());
        // Create the message part
        BodyPart messageBodyPart = new MimeBodyPart();
        Configuration cfg = new Configuration();
        cfg.setDirectoryForTemplateLoading(new File(request.getServletContext().getRealPath("resources/templates")));
        freemarker.template.Template template = null;
        if(mailType.equalsIgnoreCase("hrRequest")){
        	msg.setSubject("New HR Request created by "+requestBy);
        	template = cfg.getTemplate("hrRequestNotification.ftl");
        }
        
        if(mailType.equalsIgnoreCase("leaveRequest")){
        	msg.setSubject("New leave request from "+requestBy);
        	template = cfg.getTemplate("leaveRequestNotification.ftl");
        }
        
        if(mailType.equalsIgnoreCase("hrpolicies")) {
        	msg.setSubject("New HR Policy Created By "+requestBy);
        	template = cfg.getTemplate("hrPoliciesNotification.ftl");
        }
        
        if(mailType.equalsIgnoreCase("holiday")) {
        	msg.setSubject("New Holiday added to calender");
        	template = cfg.getTemplate("holidayNotification.ftl");
        }
        
        Map<String, String> rootMap = new HashMap<String, String>();
        rootMap.put("link",url);
        rootMap.put("userName",userName);
        rootMap.put("comments",emailBody);
        rootMap.put("requestBy",requestBy);
        if(mailType.equalsIgnoreCase("offerApproval")){
        	msg.setSubject("New offer from "+userName+" is pending for approval");
        	template = cfg.getTemplate("offerApproval.ftl");
        	rootMap.put("userName",ccEmails);
        }
        
        Writer out = new StringWriter();
        template.process(rootMap, out);
        messageBodyPart.setContent(out.toString(), "text/html");
       
        // Create a multipart message
        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(messageBodyPart);
        msg.setContent(multipart);
		// To Email List
        
        if(toEmails != null){
			for(String toEmail : toEmails ){
				if(emailValidation(toEmail.trim())){
					msg.addRecipient(Message.RecipientType.TO, new InternetAddress(toEmail.trim()));			
				}
			}
		}
        
        
        
        
		List<String> toEmailList = Arrays.asList(toEmails.split(CommonConstants.COMMA));
		if(toEmailList != null){
			for(String toEmail : toEmailList ){
				if(emailValidation(toEmail.trim())){
					msg.addRecipient(Message.RecipientType.TO, new InternetAddress(toEmail.trim()));			
				}
			}
		}
		
		// CC Email List
			for(String ccEmail : ccEmails ){
				if(ccEmail != null){
					if(emailValidation(ccEmail.trim())){
						msg.addRecipient(Message.RecipientType.CC, new InternetAddress(ccEmail.trim()));			
					}
				}
			}

		// sends the e-mail
		Transport.send(msg);
		
		return true;
	}*/
}
