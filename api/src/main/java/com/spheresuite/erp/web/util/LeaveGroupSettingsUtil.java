package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.LeaveGroupSettingsDO;
import com.spheresuite.erp.util.CommonConstants;

@Component
public class LeaveGroupSettingsUtil {
	
	private LeaveGroupSettingsUtil() {}
	
	public static JSONObject getLeaveGroupDetails(List<LeaveGroupSettingsDO> leaveGroupSettingsList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONObject result = new JSONObject();
			
			JSONArray leaveGroupSettingsDetailsJSONArray= new JSONArray();
			for (LeaveGroupSettingsDO leaveGroupDetails : leaveGroupSettingsList) {
				JSONObject leaveGroup = new JSONObject();
				leaveGroup.put(CommonConstants.ID, String.valueOf(leaveGroupDetails.getLeaveGroupSettingsId()));
				leaveGroup.put(CommonConstants.LEAVES, leaveGroupDetails.getLeaveManagement()!=null?String.valueOf(leaveGroupDetails.getLeaveManagement().getLeaveManagementId()):"");
				leaveGroup.put(CommonConstants.PERMISSION_CHECKED, leaveGroupDetails.getPermission()!=null && leaveGroupDetails.getPermission().equalsIgnoreCase("y")?String.valueOf(leaveGroupDetails.getPermission()):"");
				leaveGroup.put(CommonConstants.COMPENSATION_CHECKED, leaveGroupDetails.getCompOff()!=null && leaveGroupDetails.getCompOff().equalsIgnoreCase("y")?String.valueOf(leaveGroupDetails.getCompOff()):"");
				leaveGroupSettingsDetailsJSONArray.put(leaveGroup);
			}
			result.put(CommonConstants.LEAVEGROUP, leaveGroupSettingsDetailsJSONArray);
			resultJSON.put(CommonConstants.RESULTS, result);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getLeaveTypesForEmployee(List<LeaveGroupSettingsDO> leaveGroupList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (LeaveGroupSettingsDO leaveGroup : leaveGroupList) {
				resultJSONArray.put(getLeaveTypesForEmployeeDetailObject(leaveGroup));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getLeaveTypesForEmployeeDetailObject(LeaveGroupSettingsDO leaveGroup)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(leaveGroup.getLeaveGroupSettingsId()));
		result.put(CommonConstants.LEAVEGROUP, String.valueOf(leaveGroup.getLeaveGroup().getLeaveGroup()));
		//result.put(CommonConstants.LEAVES, String.valueOf(leaveGroup.getLeaveManagement().getLeaveManagementId()));
		result.put(CommonConstants.LEAVEMANAGEMENTID, String.valueOf(leaveGroup.getLeaveManagement().getLeaveManagementId()));
		//result.put(CommonConstants.NAME, String.valueOf(leaveGroup.getLeaveManagement().getLeaveType().getType()));
		result.put(CommonConstants.NAME, String.valueOf(leaveGroup.getLeaveManagement().getLeaveType()));
		if(leaveGroup.getLeaveManagement().getCompoff() != null){
			result.put(CommonConstants.COMPOFF, String.valueOf(leaveGroup.getLeaveManagement().getCompoff()));
		}else{
			result.put(CommonConstants.COMPOFF, String.valueOf(""));
		}
		if(leaveGroup.getLeaveManagement().getPermission() != null){
			result.put(CommonConstants.PERMISSION, String.valueOf(leaveGroup.getLeaveManagement().getPermission()));
		}else{
			result.put(CommonConstants.PERMISSION, String.valueOf(""));
		}
		if(leaveGroup.getLeaveManagement().getEarnedLeave() != null){
			result.put(CommonConstants.EARNEDLEAVE, String.valueOf(leaveGroup.getLeaveManagement().getEarnedLeave()));
		}else{
			result.put(CommonConstants.EARNEDLEAVE, String.valueOf(""));
		}
		
		//result.put(CommonConstants.LEAVETYPEID, String.valueOf(leaveGroup.getLeaveManagement().getLeaveType().getLeaveTypeId()));
		result.put(CommonConstants.FROMDATE, String.valueOf(leaveGroup.getLeaveManagement().getFromDate()));
		result.put(CommonConstants.TODATE, String.valueOf(leaveGroup.getLeaveManagement().getToDate()));
		result.put(CommonConstants.DAYS_ALLOWED, String.valueOf(leaveGroup.getLeaveManagement().getDaysAllowed()));
		result.put(CommonConstants.DAYS, String.valueOf(leaveGroup.getLeaveManagement().getNumberOfDays()));
		return result;
	}
	
}
