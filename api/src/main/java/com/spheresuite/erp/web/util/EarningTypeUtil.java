package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.EarningTypeDO;
import com.spheresuite.erp.util.CommonConstants;

@Component
public class EarningTypeUtil {
	
	private EarningTypeUtil() {}
	
	public static JSONObject getEarningTypeList(List<EarningTypeDO> earningTypeList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EarningTypeDO earningType : earningTypeList) {
				resultJSONArray.put(getEarningTypeDetailObject(earningType));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getEarningTypeDetailObject(EarningTypeDO earningType)throws JSONException{
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(earningType.getEarningTypeId()));
		result.put(CommonConstants.NAME, String.valueOf(earningType.getName()));
		if(earningType.getDisplayname() != null){
			result.put(CommonConstants.DISPLAYNAME, String.valueOf(earningType.getDisplayname()));
		}else{
			result.put(CommonConstants.DISPLAYNAME, "");
		}
		result.put(CommonConstants.STATUS, String.valueOf(earningType.getStatus()));
		if(earningType.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(earningType.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(earningType.getUpdatedon())));
		return result;
	}
}
