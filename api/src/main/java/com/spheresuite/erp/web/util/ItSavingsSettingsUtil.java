package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.ItSavingsSettingsDO;
import com.spheresuite.erp.util.CommonConstants;
@Component
public class ItSavingsSettingsUtil {
	
	private ItSavingsSettingsUtil() {}
	
	public static JSONObject getITSavingsSettingsList(List<ItSavingsSettingsDO> itSavingsSettingsList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (ItSavingsSettingsDO itSavingsSettings : itSavingsSettingsList) {
				resultJSONArray.put(getITSavingsSettingsObject(itSavingsSettings));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getITSavingsSettingsObject(ItSavingsSettingsDO itSavingsSettings)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(itSavingsSettings.getItSavingsSettingsId()));
		result.put(CommonConstants.NAME, String.valueOf(itSavingsSettings.getName()));
		result.put(CommonConstants.FROMDATE, String.valueOf(CommonUtil.convertDateToStringWithdatetime(itSavingsSettings.getFromYear())));
		if(itSavingsSettings.getToYear() != null){
			result.put(CommonConstants.TODATE, String.valueOf(CommonUtil.convertDateToStringWithdatetime(itSavingsSettings.getToYear())));
		}else{
			result.put(CommonConstants.TODATE, String.valueOf(""));
		}
		
		result.put(CommonConstants.SECTIONID, String.valueOf(itSavingsSettings.getItSection().getItSectionId()));
		result.put(CommonConstants.SECTION, String.valueOf(itSavingsSettings.getItSection().getSection()));
		result.put(CommonConstants.MAXLIMIT, String.valueOf(itSavingsSettings.getItSection().getMaxLimit()));
		result.put(CommonConstants.STATUS, String.valueOf(itSavingsSettings.getStatus()));
		if(itSavingsSettings.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(itSavingsSettings.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(itSavingsSettings.getUpdatedon())));
		return result;
	}
}
