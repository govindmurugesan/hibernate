package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.PriorityLevelDO;
import com.spheresuite.erp.util.CommonConstants;

@Component
public class PriorityLevelUtil {
	
	private PriorityLevelUtil() {}
	
	public static JSONObject getPriorityLevelList(List<PriorityLevelDO> priorityLevelList){
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (PriorityLevelDO priorityLevel : priorityLevelList) {
				resultJSONArray.put(getPriorityLevelDetailObject(priorityLevel));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getPriorityLevelDetailObject(PriorityLevelDO priorityLevel)throws JSONException{
		
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(priorityLevel.getPriorityLevelId()));
		result.put(CommonConstants.NAME, String.valueOf(priorityLevel.getName()));
		result.put(CommonConstants.STATUS, String.valueOf(priorityLevel.getStatus()));
		if(priorityLevel.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(priorityLevel.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(priorityLevel.getUpdatedon())));
		return result;
	}
}
