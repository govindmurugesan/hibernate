package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.PFDO;
import com.spheresuite.erp.util.CommonConstants;
@Component
public class PFUtil {
	
	private PFUtil() {}
	
	public static JSONObject getpfList(List<PFDO> pfList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (PFDO pf : pfList) {
				resultJSONArray.put(getPFDetailObject(pf));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getPFDetailObject(PFDO pf)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(pf.getPfId()));
		if(pf.getEmployerPercentage() != null){
			result.put(CommonConstants.EMPLOYERPERCENTAGE, String.valueOf(pf.getEmployerPercentage()));
		}else{
			result.put(CommonConstants.EMPLOYERPERCENTAGE, String.valueOf(""));
		}
		
		if(pf.getEmployeePercentage() != null){
			result.put(CommonConstants.PERCENTAGE, String.valueOf(pf.getEmployeePercentage()));
		}else{
			result.put(CommonConstants.PERCENTAGE, String.valueOf(""));
		}
		result.put(CommonConstants.STATUS, String.valueOf(pf.getStatus()));
		if(pf.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(pf.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		if(pf.getUpdatedon() != null)result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(pf.getUpdatedon())));

		return result;
	}
}
