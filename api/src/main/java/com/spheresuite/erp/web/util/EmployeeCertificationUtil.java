package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.EmployeeCertificationDO;
import com.spheresuite.erp.util.CommonConstants;
@Component
public class EmployeeCertificationUtil {
	
	private EmployeeCertificationUtil() {}
	
	public static JSONObject getEmployeeCertificationList(List<EmployeeCertificationDO> employeeCertificationList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeeCertificationDO employeeCertificationDO : employeeCertificationList) {
				resultJSONArray.put(getEmployeeCertificationDetailObject(employeeCertificationDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getEmployeeCertificationDetailObject(EmployeeCertificationDO employeeCertificationDO)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(employeeCertificationDO.getEmpCertificationId()));
		result.put(CommonConstants.MONTHLY, String.valueOf(employeeCertificationDO.getMonth()));
		result.put(CommonConstants.TITLE, String.valueOf(employeeCertificationDO.getTitle()));
		result.put(CommonConstants.COMMENT, String.valueOf(employeeCertificationDO.getComment()));
		result.put(CommonConstants.EMPID, String.valueOf(employeeCertificationDO.getEmployee().getEmpId()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(employeeCertificationDO.getUpdatedon())));
		if(employeeCertificationDO.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(employeeCertificationDO.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		return result;
	}
}
