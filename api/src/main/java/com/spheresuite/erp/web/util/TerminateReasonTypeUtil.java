package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.TerminateReasonTypeDO;
import com.spheresuite.erp.util.CommonConstants;

@Component
public class TerminateReasonTypeUtil {
	
	private TerminateReasonTypeUtil() {}
	
	public static JSONObject getTerminateReasonTypeList(List<TerminateReasonTypeDO> terminateReasonTypeList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (TerminateReasonTypeDO terminateReasonTypeDO : terminateReasonTypeList) {
				resultJSONArray.put(getTerminateReasonDetailObject(terminateReasonTypeDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getTerminateReasonDetailObject(TerminateReasonTypeDO terminateReasonTypeDO)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(terminateReasonTypeDO.getTerminationReasonId()));
		result.put(CommonConstants.NAME, String.valueOf(terminateReasonTypeDO.getName()));
		result.put(CommonConstants.STATUS, String.valueOf(terminateReasonTypeDO.getStatus()));
		if(terminateReasonTypeDO.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(terminateReasonTypeDO.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(terminateReasonTypeDO.getUpdatedon())));
		return result;
	}
}
