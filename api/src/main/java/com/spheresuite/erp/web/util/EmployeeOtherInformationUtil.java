package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.EmployeeOtherInformationDO;
import com.spheresuite.erp.util.CommonConstants;
@Component
public class EmployeeOtherInformationUtil {
	
	private EmployeeOtherInformationUtil() {}
	
	public static JSONObject getEmployeeOtherInfoList(List<EmployeeOtherInformationDO> employeeOtherInformationList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeeOtherInformationDO employeeOtherInformationDO : employeeOtherInformationList) {
				resultJSONArray.put(getEmployeeOtherInfoDetailObject(employeeOtherInformationDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getEmployeeOtherInfoDetailObject(EmployeeOtherInformationDO employeeOtherInformationDO)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(employeeOtherInformationDO.getEmpAdditionalInfoId()));
		result.put(CommonConstants.VOTERID, String.valueOf(employeeOtherInformationDO.getVoterId()));
		result.put(CommonConstants.RATIONCARD, String.valueOf(employeeOtherInformationDO.getRationCard()));
		result.put(CommonConstants.MARITALSTATUS, String.valueOf(employeeOtherInformationDO.getMaritalStatus()));
		result.put(CommonConstants.SPOUSENAME, String.valueOf(employeeOtherInformationDO.getSpouseName()!=null?employeeOtherInformationDO.getSpouseName():""));
		result.put(CommonConstants.CHILDREN, String.valueOf(employeeOtherInformationDO.getChildern()!=null?employeeOtherInformationDO.getChildern():""));
		result.put(CommonConstants.BLOODGROUP, String.valueOf(employeeOtherInformationDO.getBloodGroup()));
		result.put(CommonConstants.EMPID, String.valueOf(employeeOtherInformationDO.getEmployee().getEmpId()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(employeeOtherInformationDO.getUpdatedon())));
		if(employeeOtherInformationDO.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(employeeOtherInformationDO.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		return result;
	}
}
