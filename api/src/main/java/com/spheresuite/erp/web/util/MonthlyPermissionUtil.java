package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.MonthlyPermissionDO;
import com.spheresuite.erp.util.CommonConstants;

@Component
public class MonthlyPermissionUtil {
	
	private MonthlyPermissionUtil() {}
	
	public static JSONObject getMonthlyPermissionList(List<MonthlyPermissionDO> monthlyPermissionList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (MonthlyPermissionDO monthlyPermissionDO : monthlyPermissionList) {
				resultJSONArray.put(getMonthlyPermissionDetailObject(monthlyPermissionDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getMonthlyPermissionDetailObject(MonthlyPermissionDO monthlyPermissionDO)throws JSONException{
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(monthlyPermissionDO.getMonthlyPermissionId()));
		result.put(CommonConstants.PERMISSIONHOURSPERMONTH, String.valueOf(monthlyPermissionDO.getPermissionHoursPerMonth()));
		result.put(CommonConstants.PERMISSIONHOURSPERDAY, String.valueOf(monthlyPermissionDO.getPermissionHoursPerDay()));
		if(monthlyPermissionDO.getNumberOfPermissionPerMonth() != null){
			result.put(CommonConstants.NUMBEROFPERMISSION, String.valueOf(monthlyPermissionDO.getNumberOfPermissionPerMonth()));
		}else{
			result.put(CommonConstants.NUMBEROFPERMISSION, String.valueOf(""));
		}
		
		result.put(CommonConstants.STATUS, String.valueOf(monthlyPermissionDO.getStatus()));
		if(monthlyPermissionDO.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(monthlyPermissionDO.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(monthlyPermissionDO.getUpdatedon())));
		return result;
	}
}
