package com.spheresuite.erp.supplier.rs;

import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.RolesDO;
import com.spheresuite.erp.domainobject.SupplierAccessDO;
import com.spheresuite.erp.domainobject.SuppliersContactDO;
import com.spheresuite.erp.service.RolesService;
import com.spheresuite.erp.supplier.service.SupplierAccessService;
import com.spheresuite.erp.supplier.service.SuppliersContactService;
import com.spheresuite.erp.supplier.web.util.SupplierAccessUtil;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.util.JwtTokenGenerator;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.EmailProxyUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/supplieruser")
public class SupplierAccessRS {
	
	String validation = null;
	
	static Logger logger = Logger.getLogger(SupplierAccessRS.class.getName());


	@Autowired
	private SupplierAccessService userService;
	

	@Autowired
	private SuppliersContactService supplierContactService;
	
	@Autowired
	private RolesService rolesService;
	
	

	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				SupplierAccessDO userDO = new SupplierAccessDO();
				List<SuppliersContactDO> supplierList = null;
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.SUPPLIERCONTACT) != null && !inputJSON.get(CommonConstants.SUPPLIERCONTACT).toString().isEmpty()){
			 			supplierList = supplierContactService.retriveById(Long.parseLong(inputJSON.get(CommonConstants.SUPPLIERCONTACT).toString()));
			 			if(supplierList != null && supplierList.size() > 0){
				 			userDO.setContact(supplierList.get(0));
				 		}
			 		}
			 		if(inputJSON.get(CommonConstants.ROLE_ID) != null && !inputJSON.get(CommonConstants.ROLE_ID).toString().isEmpty()){
			 			List<RolesDO> rolesList = rolesService.retriveById(Long.parseLong(inputJSON.get(CommonConstants.ROLE_ID).toString()));
						if(rolesList != null){
							userDO.setRole(rolesList.get(0));
						}
			 		}
			 		/*if(inputJSON.get(CommonConstants.EMAIL) != null && !inputJSON.get(CommonConstants.EMAIL).toString().isEmpty()){
			 			userDO.setEmail(inputJSON.get(CommonConstants.EMAIL).toString().toString());
			 		}*/
					userDO.setIsDeleted(0L);
					userDO.setUpdatedon(new Date());
					if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			userDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			userDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
					char[] temppasssword = SupplierAccessUtil.geek_Password(5);
					String pwd = temppasssword.toString();
					userDO.setTemppassowrd(Base64.getEncoder().encodeToString(pwd.getBytes("UTF-8")));
					userDO.setStatus(CommonConstants.PEDING);
					
					if(!userService.persist(userDO)){
						return CommonWebUtil.buildErrorResponse("Supplier Access Already Exist").toString();
					}
					CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New User Created");
			 	if(userDO != null && userDO.getSupplierUserId() != null){
			 		if(supplierList != null && supplierList.size() > 0){ 
			 			if(supplierList.get(0).getEmail() != null ){
				 			String host = request.getServletContext().getInitParameter(CommonConstants.HOST);
							String fromEmail = CommonConstants.SENDMAIL;
							String toEmails = supplierList.get(0).getEmail();
							String userName = null;
							
							List<SuppliersContactDO> supplierList1 = supplierContactService.retriveById(userDO.getContact().getSupplierContactId());
							if(supplierList1 != null && supplierList1.size() > 0){
								userName =  String.valueOf(supplierList1.get(0).getFname());
							}
							String URL = CommonUtil.getUIUrl(request);
							URL = CommonConstants.HTTP+URL+"#!/changepassword/";
							String url = URL+userDO.getTemppassowrd()+"/"+supplierList1.get(0).getEmail()+"/N"+"/S";
							String emailBody = "Cick Here Login : " +URL+userDO.getTemppassowrd()+"/"+supplierList1.get(0).getEmail()+"/N"+"/S";
					 		boolean mailStatus = EmailProxyUtil.sendEmail(request,host,  fromEmail,  toEmails, "", "", emailBody, false, url, "inviteUser", userName); 
							if(mailStatus){
								return CommonWebUtil.buildSuccessResponse().toString();
							}else{
								return CommonWebUtil.buildErrorResponse("").toString();
							}
			 			}else{
					 		return CommonWebUtil.buildErrorResponse("Email Id not available to send mail").toString();
					 	}
			 		}else{
				 		return CommonWebUtil.buildErrorResponse("Employee not available").toString();
				 	}
					
			 	}else{
			 		return CommonWebUtil.buildErrorResponse("Supplier Access Id not available to send mail").toString();
			 	}
			}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/resendInvite/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String resendInvite(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<SupplierAccessDO> userList = userService.retriveBySupplierId(inputJSON.get(CommonConstants.ID).toString());
					if(userList != null && userList.size() > 0){
						if(userList.get(0).getTemppassowrd() != null && userList.get(0).getContact().getEmail() != null){
							String host = request.getServletContext().getInitParameter(CommonConstants.HOST);
							String fromEmail = CommonConstants.SENDMAIL;
							String toEmails = userList.get(0).getContact().getEmail();
							String URL = CommonUtil.getUIUrl(request);
							URL = CommonConstants.HTTP+URL+"#!/changepassword/";
							String emailBody = "Cick Here Login : " +URL+userList.get(0).getTemppassowrd()+"/"+userList.get(0).getContact().getEmail()+"/N"+"/S";
							String url = URL+userList.get(0).getTemppassowrd()+"/"+userList.get(0).getContact().getEmail()+"/N"+"/S";

							String userName = null;
							List<SuppliersContactDO> supplierList = supplierContactService.retriveById(userList.get(0).getContact().getSupplierContactId());
							if(supplierList != null && supplierList.size() > 0){
								userName =  String.valueOf(supplierList.get(0).getFname() );
							}
							boolean mailStatus = EmailProxyUtil.sendEmail(request,host,  fromEmail,  toEmails, "", "", emailBody, false, url, "reInvite", userName);
							if(mailStatus){
								return CommonWebUtil.buildSuccessResponse().toString();
							}else{
								return CommonWebUtil.buildErrorResponse("").toString();
							}
						}
					}
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<SupplierAccessDO> userList = userService.retrieve();
				respJSON = SupplierAccessUtil.getUserList(userList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<SupplierAccessDO> userList = userService.retrieveActive();
				respJSON = SupplierAccessUtil.getUserList(userList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retriveById/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveById(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<SupplierAccessDO> userList = userService.retriveBySupplierId(inputJSON.get(CommonConstants.ID).toString());
					respJSON = SupplierAccessUtil.getUserList(userList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		SupplierAccessDO userDO = new SupplierAccessDO();
			 		List<SupplierAccessDO>userDOList = userService.retriveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
				 	if (userDOList != null && userDOList.size() > 0){
				 		userDO = userDOList.get(0);
				 		if(inputJSON.get(CommonConstants.ROLE_ID) != null && !inputJSON.get(CommonConstants.ROLE_ID).toString().isEmpty()){
				 			List<RolesDO> rolesList = rolesService.retriveById(Long.parseLong(inputJSON.get(CommonConstants.ROLE_ID).toString()));
							if(rolesList != null){
								userDO.setRole(rolesList.get(0));
							}
				 		}
				 		/*if(inputJSON.get(CommonConstants.EMAIL) != null && !inputJSON.get(CommonConstants.EMAIL).toString().isEmpty()){
				 			userDO.setEmail(inputJSON.get(CommonConstants.EMAIL).toString().toString());
				 		}*/
						userDO.setUpdatedon(new Date());
						if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			userDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
						if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
							userDO.setStatus((char)  inputJSON.get(CommonConstants.STATUS).toString().charAt(0));
				 		}
						if(userDO.getStatus() != 'd'){
							userDO.setIsDeleted(0L);
						}else{
							userDO.setIsDeleted(1L);
						}
						//userService.update(userDO);
						if(!userService.update(userDO)){
							return CommonWebUtil.buildErrorResponse("Supplier Access Already Added").toString();
						}
						CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "User Updated");
				 	}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/updatepassword", method = RequestMethod.POST)
	public @ResponseBody String updatepassword(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.EMAIL).toString().isEmpty() && !inputJSON.get(CommonConstants.PASSWORD).toString().isEmpty()){
			 		SupplierAccessDO userDO = new SupplierAccessDO();
			 		List<SupplierAccessDO>userDOList = userService.retrieveByEmailId(inputJSON.get(CommonConstants.EMAIL).toString());
				 	if (userDOList != null){
				 		userDO = userDOList.get(0);
				 		/*Base64.Encoder encoder = Base64.getEncoder(); 
						byte[] enString = encoder.encode(inputJSON.get(CommonConstants.PASSWORD).toString().getBytes());*/
						String pwd = inputJSON.get(CommonConstants.PASSWORD).toString();
						userDO.setPassword(Base64.getEncoder().encodeToString(pwd.getBytes("UTF-8")));
				 		//String enString = CommonUtil.encryptText("testseed",inputJSON != null ? CommonConstants.PASSWORD.toString() : "");
						userDO.setUpdatedon(new Date());
						userService.update(userDO);
						CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Password Updated");
				 	}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/delete/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String delete(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		SupplierAccessDO userDO = new SupplierAccessDO();
			 		List<SupplierAccessDO> userList = userService.retriveBySupplierId(inputJSON.get(CommonConstants.ID).toString());
				 	if (userList != null){
				 		userDO = userList.get(0);
				 		userDO.setIsDeleted(1L);
				 		userDO.setStatus('d');
						userDO.setUpdatedon(new Date());
						userService.update(userDO);
				 	}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/cancelUser/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String cancelUser(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		SupplierAccessDO userDO = new SupplierAccessDO();
			 		List<SupplierAccessDO> userList = userService.retriveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
				 	if (userList != null && userList.size() > 0){
				 		userDO = userList.get(0);
				 		userDO.setIsDeleted(1L);
				 		userDO.setStatus('c');
						userDO.setUpdatedon(new Date());
						userService.update(userDO);
				 	}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveForLogin", method = RequestMethod.POST)
	public @ResponseBody String retrieveForLogin(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			//if (WebManager.authenticateSession(request)) {
			JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			if(inputJSON != null && !inputJSON.get(CommonConstants.EMAIL).toString().isEmpty()
					&& !inputJSON.get(CommonConstants.PASSWORD).toString().isEmpty()){
				List<SupplierAccessDO> userList = userService.retrieveByEmailId(inputJSON.get(CommonConstants.EMAIL).toString());
				if(userList.size() > 0) {
					byte[] decodedBytes = Base64.getDecoder().decode(userList.get(0).getPassword());
					String stringDecode = new String(decodedBytes, "UTF-8");
				if(stringDecode.toString().equals(inputJSON.get(CommonConstants.PASSWORD).toString())){
						String token = new JwtTokenGenerator().createSupplierJWT(userList);
						HttpSession session = request.getSession();
						session.setAttribute("token", token);
						//session.setMaxInactiveInterval(60*60);
						respJSON = SupplierAccessUtil.getUserForLogin(userList,token).toString();
					}
					
				}
			}
			//}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
			
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/resetpassword", method = RequestMethod.POST)
	public @ResponseBody String resetpassword(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.EMAIL).toString().isEmpty() 
			 			&& !inputJSON.get(CommonConstants.TEMP_PASSWORD).toString().isEmpty()
			 			&& !inputJSON.get(CommonConstants.NEWPASSWORD).toString().isEmpty()){
			 		List<SuppliersContactDO> supplierList =  supplierContactService.retrieveByEmail(inputJSON.get(CommonConstants.EMAIL).toString());
			 		if(supplierList != null && supplierList.size() > 0){
			 			SupplierAccessDO userDO = new SupplierAccessDO();
				 		List<SupplierAccessDO>userDOList = userService.retrieveByTempPassword(inputJSON.get(CommonConstants.TEMP_PASSWORD).toString(),
				 				supplierList.get(0).getSupplierContactId());
					 	if (userDOList != null && userDOList.size() > 0){
					 		userDO = userDOList.get(0);
					 		String pwd = inputJSON.get(CommonConstants.NEWPASSWORD).toString();
							userDO.setPassword(Base64.getEncoder().encodeToString(pwd.getBytes("UTF-8")));
							userDO.setTemppassowrd(null);
							userDO.setUpdatedon(new Date());
							userDO.setStatus(CommonConstants.ACTIVE);
							userService.update(userDO);
					 	}else{
					 		return CommonWebUtil.buildErrorResponse("Supplier Access not availble").toString();
					 	}
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/sendPassword", method = RequestMethod.POST)
	public @ResponseBody String sendPassword(Model model, HttpServletRequest request) {
		try {
			JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			if(inputJSON != null && !inputJSON.get(CommonConstants.EMAIL).toString().isEmpty()){
				List<SupplierAccessDO> userList = userService.retriveBySupplierId(inputJSON.get(CommonConstants.EMAIL).toString());
				if(userList != null && userList.size() > 0){
					if(userList.get(0).getContact().getEmail() != null){
						String host = request.getServletContext().getInitParameter(CommonConstants.HOST);
						String fromEmail = request.getServletContext().getInitParameter(CommonConstants.FROMEMAIL);
						String toEmails = userList.get(0).getContact().getEmail();
						if(userList.get(0).getPassword() != null){
							SupplierAccessDO userDO = new SupplierAccessDO();
							userDO = userList.get(0);
							/*char[] temppasssword = SupplierUserUtil.geek_Password(5);
							userDO.setTemppassowrd(temppasssword.toString());*/
							char[] temppasssword = SupplierAccessUtil.geek_Password(5);
							//Base64.Encoder encoder = Base64.getEncoder(); 
							//byte[] enString = encoder.encode(temppasssword.toString().getBytes());
							//String enString = CommonUtil.encryptText("testseed",temppasssword.toString());
							//userDO.setTemppassowrd(enString.toString());
							String pwd = temppasssword.toString();
							userDO.setTemppassowrd(Base64.getEncoder().encodeToString(pwd.getBytes("UTF-8")));
							userDO.setPassword(null);
							//userDO = userService.update(userDO);
							if(userService.update(userDO)){
								String URL = CommonUtil.getUIUrl(request);
								URL = CommonConstants.HTTP+URL+"#!/changepassword/";
								String emailBody = URL+userDO.getTemppassowrd()+"/"+userList.get(0).getContact().getEmail()+"/F"+"/S";
								String userName = null;
								List<SuppliersContactDO> supplierList = supplierContactService.retriveById(userList.get(0).getContact().getSupplierContactId());
								if(supplierList != null && supplierList.size() > 0){
									userName =  String.valueOf(supplierList.get(0).getFname() + supplierList.get(0).getMname() + supplierList.get(0).getLname());
								}
								
								String url = URL+userDO.getTemppassowrd()+"/"+userList.get(0).getContact().getEmail()+"/F"+"/S";
						 		boolean mailStatus = EmailProxyUtil.sendEmail(request,host,  fromEmail,  toEmails, "", "", emailBody, false, url, "resetPassword", userName); 
	
								if(mailStatus){
									return CommonWebUtil.buildSuccessResponse().toString();
								}else{
									return CommonWebUtil.buildErrorResponse(CommonConstants.EMAIL_NOT_SENT).toString();
								}
							}else{
								return CommonWebUtil.buildErrorResponse("").toString();
							}
							
						}else{
							return CommonWebUtil.buildErrorResponse(CommonConstants.EMPID_NOT_REGISTER).toString();
						}
					}else{
						return CommonWebUtil.buildErrorResponse("Email Id Not found").toString();
					}
				}else{
					return CommonWebUtil.buildErrorResponse(CommonConstants.EMPID_NOT_REGISTER).toString();
				}
			}else{
				return CommonWebUtil.buildErrorResponse("").toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
	}
	
	@RequestMapping(value = "/resetpasswordWithoutAuth", method = RequestMethod.POST)
	public @ResponseBody String resetpasswordWithoutAuth(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty() && !inputJSON.get(CommonConstants.NEWPASSWORD).toString().isEmpty()){
			 		SupplierAccessDO userDO = new SupplierAccessDO();
			 		List<SupplierAccessDO>userDOList = userService.retriveBySupplierId(inputJSON.get(CommonConstants.EMPID).toString());
				 	if (userDOList != null && userDOList.size() > 0){
				 		userDO = userDOList.get(0);
				 		String pwd = inputJSON.get(CommonConstants.NEWPASSWORD).toString();
						userDO.setPassword(Base64.getEncoder().encodeToString(pwd.getBytes("UTF-8")));
						userDO.setTemppassowrd(null);
						userDO.setUpdatedon(new Date());
						userDO.setStatus(CommonConstants.ACTIVE);
						userService.updatePassword(userDO);
				 	}else{
				 		return CommonWebUtil.buildErrorResponse("").toString();
				 	}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/importUser/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String importEmp(@PathVariable String inputParams,  Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
				JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
			//	List<SupplierUserDO> userlist = new ArrayList<SupplierUserDO>();
				String updatedBy = null;
				if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY).toString();
				}
				for (int i=0; i < fileData.size(); i++){
					SupplierAccessDO userDO = new SupplierAccessDO();
					JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
					if(rowJSON.get(colName.get(CommonConstants.EMPID)) != null && !rowJSON.get(colName.get(CommonConstants.EMPID)).toString().isEmpty()){
			 			List<SuppliersContactDO> supplierList = supplierContactService.retriveById(Long.parseLong(rowJSON.get(colName.get(CommonConstants.EMPID)).toString()));
			 			if(supplierList != null && supplierList.size() > 0){
				 			userDO.setContact(supplierList.get(0));
				 		
					 		if(rowJSON.get(colName.get(CommonConstants.ROLE_NAME)) != null && ! rowJSON.get(colName.get(CommonConstants.ROLE_NAME)).toString().isEmpty()){
					 			List<RolesDO> rolesList = rolesService.retriveByName(rowJSON.get(colName.get(CommonConstants.ROLE_NAME)).toString());
								if(rolesList != null){
									userDO.setRole(rolesList.get(0));
								}
					 		}
					 		/*if(rowJSON.get(colName.get(CommonConstants.EMAIL)) != null && !rowJSON.get(colName.get(CommonConstants.EMAIL)).toString().isEmpty()){
					 			userDO.setEmail(rowJSON.get(colName.get(CommonConstants.EMAIL)).toString());
					 		}*/
							userDO.setIsDeleted(0L);
							userDO.setUpdatedon(new Date());
							if(updatedBy != null){
					 			userDO.setUpdatedby((String) updatedBy);
					 			userDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 		}
							char[] temppasssword = SupplierAccessUtil.geek_Password(5);
							String pwd = temppasssword.toString();
							userDO.setTemppassowrd(Base64.getEncoder().encodeToString(pwd.getBytes("UTF-8")));
							userDO.setStatus(CommonConstants.PEDING);
							
							if(!userService.persist(userDO)){
								//return CommonWebUtil.buildErrorResponse("User Already Exist").toString();
							}else{
								if(userDO != null && userDO.getSupplierUserId() != null && userDO.getContact().getEmail() != null ){
									List<SuppliersContactDO> supplierList1 = supplierContactService.retriveById(userDO.getContact().getSupplierContactId());
									if(supplierList1 != null && supplierList1.size() > 0){
										String host = request.getServletContext().getInitParameter(CommonConstants.HOST);
										String fromEmail = CommonConstants.SENDMAIL;
										String toEmails = userDO.getContact().getEmail();
										String userName = null;
										userName =  String.valueOf(supplierList1.get(0).getFname());
										String URL = CommonUtil.getUIUrl(request);
										URL = CommonConstants.HTTP+URL+"#!/changepassword/";
										String url = URL+userDO.getTemppassowrd()+"/"+userDO.getContact().getEmail()+"/N"+"/S";
										String emailBody = "Cick Here Login : " +URL+userDO.getTemppassowrd()+"/"+userDO.getContact().getEmail()+"/N"+"/S";
								 		boolean mailStatus = EmailProxyUtil.sendEmail(request,host,  fromEmail,  toEmails, "", "", emailBody, false, url, "inviteUser", userName); 
										if(mailStatus){
											return CommonWebUtil.buildSuccessResponse().toString();
										}else{
											return CommonWebUtil.buildErrorResponse("").toString();
										}
									}
							 	}
							}
			 			}
					
					}	
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
}