package com.spheresuite.erp.supplier.web.util;

import java.util.List;
import java.util.Random;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.AssignedMenuDO;
import com.spheresuite.erp.domainobject.SupplierAccessDO;
import com.spheresuite.erp.service.AssignedMenuService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;

@Component
public class SupplierAccessUtil {
	
	private SupplierAccessUtil() {}
	
	@Autowired
	private AssignedMenuService assignedMenuServiceTemp;
	
	private static AssignedMenuService assignedMenuService;
	
	@PostConstruct
	public void init() {
		assignedMenuService = this.assignedMenuServiceTemp;
	}
	
	public static JSONObject getUserList(List<SupplierAccessDO> userList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (SupplierAccessDO user : userList) {
				resultJSONArray.put(getUserDetailObject(user));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getUserDetailObject(SupplierAccessDO user)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(user.getSupplierUserId() != null ? user.getSupplierUserId() : ""));
		result.put(CommonConstants.SUPPLIERID, String.valueOf(user.getContact().getSuppliers().getSupplierId()));
		result.put(CommonConstants.EMPID, String.valueOf(user.getContact().getSupplierContactId() != null ? user.getContact().getSupplierContactId() : ""));
		result.put(CommonConstants.STATUS, String.valueOf(user.getStatus()));
		result.put(CommonConstants.EMAIL, String.valueOf(user.getContact().getEmail() != null ? user.getContact().getEmail() : ""));
		result.put(CommonConstants.ROLE_ID, String.valueOf(user.getRole().getRoleId() != null ? user.getRole().getRoleId() : ""));
	//	result.put(CommonConstants.PASSWORD, String.valueOf(user.getPassword() != null ? user.getPassword(): ""));
		result.put(CommonConstants.TEMP_PASSWORD, String.valueOf(user.getPassword() != null ? user.getPassword() : ""));
		result.put(CommonConstants.ISDELETED, String.valueOf(user.getIsDeleted() != null ? user.getIsDeleted() : ""));
		result.put(CommonConstants.ROLE_NAME, String.valueOf(user.getRole().getName() != null ? user.getRole().getName() : ""));
		result.put(CommonConstants.TYPE, "S");
		if(user.getContact() != null){
			String empName = user.getContact().getFname()+" "+ user.getContact().getMname() + " "+ user.getContact().getLname();
			result.put(CommonConstants.NAME, empName); 
		}else{
			result.put(CommonConstants.NAME, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(user.getUpdatedon())));
		if(user.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(user.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		return result;
	}
	
	public static JSONObject getUserForLogin(List<SupplierAccessDO> userList, String token) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (SupplierAccessDO user : userList) {
				resultJSONArray.put(getUserForLoginDetailObject(user));
			}
			resultJSON.put(CommonConstants.TOKEN, token);
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getUserForLoginDetailObject(SupplierAccessDO user)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(user.getContact().getSupplierContactId()));
		result.put(CommonConstants.SUPPLIERID, String.valueOf(user.getContact().getSuppliers().getSupplierId()));
		result.put(CommonConstants.EMAIL, String.valueOf(user.getContact().getEmail()));
		result.put(CommonConstants.TYPE, "S");
		if(user.getRole().getRoleId() != null){
			result.put(CommonConstants.ROLE_ID, String.valueOf(user.getRole().getRoleId()));
			result.put(CommonConstants.ROLE_NAME, String.valueOf(user.getRole().getName()));
		}
		if(user.getContact() != null){
			String empName = user.getContact().getFname()+" "+ user.getContact().getMname() + " "+ user.getContact().getLname();
			result.put(CommonConstants.NAME, empName); 
		}else{
			result.put(CommonConstants.NAME, ""); 
		}
		List<AssignedMenuDO> list = assignedMenuService.retrieveByRoleId(user.getRole().getRoleId());
		JSONArray resultJSONArray = new JSONArray();
		if(list.size() > 0){
			for (AssignedMenuDO l : list) {
				resultJSONArray.put(getMenuObject(l));
			}
		}
		result.put(CommonConstants.MENU, resultJSONArray);
		return result;
	}
	
	public static JSONObject getMenuObject(AssignedMenuDO menu)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.SUB_MENU, String.valueOf(menu.getMenu().getMenuId()));
		return result;
	}
	
	public static JSONObject generateUrl(String email, String tempPassword ) {
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.EMAIL, email);
			resultJSON.put(CommonConstants.TEMP_PASSWORD, tempPassword);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return resultJSON;
	}
	
	public static char[] geek_Password(int len)
    {
        // A strong password has Cap_chars, Lower_chars,
        // numeric value and symbols. So we are using all of
        // them to generate our password
        String Capital_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String Small_chars = "abcdefghijklmnopqrstuvwxyz";
        String numbers = "0123456789";
                String symbols = "!@#$%^&*_=+-/.?<>)";
 
 
        String values = Capital_chars + Small_chars +
                        numbers + symbols;
 
        // Using random method
        Random rndm_method = new Random();
 
        char[] password = new char[len];
 
        for (int i = 0; i < len; i++)
        {
            // Use of charAt() method : to get character value
            // Use of nextInt() as it is scanning the value as int
            password[i] =
              values.charAt(rndm_method.nextInt(values.length()));
 
        }
        return password;
    }
}
