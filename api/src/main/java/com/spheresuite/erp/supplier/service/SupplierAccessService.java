package com.spheresuite.erp.supplier.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.domainobject.SupplierAccessDO;
import com.spheresuite.erp.supplier.dao.SupplierAccessDAO;

@Service
@Transactional
public class SupplierAccessService {
	static Logger logger = Logger.getLogger(SupplierAccessService.class.getName());
	
	@Autowired
	private SupplierAccessDAO userDAO;
	
	@Transactional
	public Boolean persist(SupplierAccessDO supplierUserDO) {
		return userDAO.persist(supplierUserDO);
	}

	@Transactional
	public List<SupplierAccessDO> retrieveActive() {
		return userDAO.retrieveActive();
	}
	
	@Transactional
	public List<SupplierAccessDO> retrieveByEmail(String personal, String secondary) {
		return userDAO.retrieveByEmail(personal, secondary);
	}
	
	@Transactional
	public List<SupplierAccessDO> retriveById(Long id) {
		return userDAO.retrieveById(id);
	}
	
	@Transactional
	public List<SupplierAccessDO> retrieveByTempPassword(String tempPassword, Long empId) {
		return userDAO.retrieveByTempPassword(tempPassword, empId);
	}
	
	@Transactional
	public List<SupplierAccessDO> retrieveByEmailId(String email) {
		return userDAO.retrieveByEmailId(email);
	}
	
	@Transactional
	public List<SupplierAccessDO> retrieve() {
		return userDAO.retrieve();
	}
	
	@Transactional
	public Boolean update(SupplierAccessDO supplierUserDO) {
		return userDAO.update(supplierUserDO);
	}
	
	@Transactional
	public Boolean updatePassword(SupplierAccessDO supplierUserDO) {
		return userDAO.updatePassword(supplierUserDO);
	}
	
	@Transactional
	public List<SupplierAccessDO> retrieveForLogin(String email, String pasword) {
		return userDAO.retrieveForLogin(email, pasword);
	}
	
	@Transactional
	public List<SupplierAccessDO> retriveBySupplierId(String id) {
		return userDAO.retriveBySupplierId(id);
	}
	
	@Transactional
	public List<SupplierAccessDO> getSuperAdmins() {
		return userDAO.getSuperAdmins();
	}
}
