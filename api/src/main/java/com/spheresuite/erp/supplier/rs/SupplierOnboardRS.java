package com.spheresuite.erp.supplier.rs;

import io.jsonwebtoken.SignatureException;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.auth0.jwt.JWTVerifyException;
import com.spheresuite.erp.domainobject.SupplierOnboardDO;
import com.spheresuite.erp.supplier.service.SupplierOnboardService;
import com.spheresuite.erp.supplier.web.util.SupplierOnboardUtil;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.EmailProxyUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/supplieronboard")
public class SupplierOnboardRS {

	String validation = null;
	static Logger logger = Logger.getLogger(SupplierOnboardRS.class.getName());

	@Autowired
	private SupplierOnboardService supplierOnboardService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		SupplierOnboardDO supplierOnboardDO = new SupplierOnboardDO();
		try {
			if (WebManager.authenticateSession(request)) {
				
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
			 			supplierOnboardDO.setName(inputJSON.get(CommonConstants.NAME).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.PERSONALCONTACT) != null && !inputJSON.get(CommonConstants.PERSONALCONTACT).toString().isEmpty()){
			 			supplierOnboardDO.setMobile(inputJSON.get(CommonConstants.PERSONALCONTACT).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.PERSONALEMAIL) != null && !inputJSON.get(CommonConstants.PERSONALEMAIL).toString().isEmpty()){
			 			supplierOnboardDO.setEmail(inputJSON.get(CommonConstants.PERSONALEMAIL).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			supplierOnboardDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			supplierOnboardDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		supplierOnboardDO.setUpdatedon(new Date());
			 		supplierOnboardDO.setCreatedon(new Date());
			 		supplierOnboardDO.setStatus(CommonConstants.PENDING_STATUS);
		 			}else{
		 				return CommonWebUtil.buildErrorResponse("").toString();
		 			}
			 		List<SupplierOnboardDO> supplierOnboardList = supplierOnboardService.retrieveByEmail(inputJSON.get(CommonConstants.PERSONALEMAIL).toString());
			 		if(supplierOnboardList != null && supplierOnboardList.size() > 0){
			 			return CommonWebUtil.buildErrorResponse("Email Already Available").toString();
			 		} else {
			 			supplierOnboardService.persist(supplierOnboardDO);
				 		String URL = CommonUtil.getUIUrl(request);
						URL = CommonConstants.HTTP+URL+"#!/supplier/onboardform";
						String url = URL+"/"+supplierOnboardDO.getSupplierOnboardId();
						String emailBody = "Cick Here to Onboard : " +url;
						String host = request.getServletContext().getInitParameter(CommonConstants.HOST);
						String fromEmail = CommonConstants.SENDMAIL;
						String toEmails = supplierOnboardDO.getEmail();
				 		boolean mailStatus = EmailProxyUtil.sendEmail(request,host,  fromEmail,  toEmails, "", "", emailBody, false, url, "onboardSupplier", supplierOnboardDO.getFname()); 
						if(mailStatus){
							CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New SupplierOnboard Created");
							return CommonWebUtil.buildSuccessResponseId(supplierOnboardDO.getSupplierOnboardId().toString()).toString();
						}else{
							supplierOnboardService.delete(supplierOnboardDO.getSupplierOnboardId());
							return CommonWebUtil.buildErrorResponse("Network Error").toString();
						}
			 		}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			supplierOnboardService.delete(supplierOnboardDO.getSupplierOnboardId());
			return CommonWebUtil.buildErrorResponse("Network Error").toString();
		}		
	}
	
	
	@RequestMapping(value = "/retrieveByOnboardId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByOnboardId(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<SupplierOnboardDO> supplierOnboardList = supplierOnboardService.retriveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = SupplierOnboardUtil.getSupplierOnboardList(supplierOnboardList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				SupplierOnboardDO supplierDO = new SupplierOnboardDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
		 				List<SupplierOnboardDO> supplierOnboardList = supplierOnboardService.retriveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
				 		if(supplierOnboardList != null && supplierOnboardList.size() > 0){
				 			supplierDO = supplierOnboardList.get(0);
				 			if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
					 			supplierDO.setName(inputJSON.get(CommonConstants.NAME).toString().toUpperCase());
					 		}
					 		
					 		if(inputJSON.get(CommonConstants.REGISTERED_ADDRESS) != null && !inputJSON.get(CommonConstants.REGISTERED_ADDRESS).toString().isEmpty()){
					 			supplierDO.setRegisteredAddr(inputJSON.get(CommonConstants.REGISTERED_ADDRESS).toString().toUpperCase());
					 		}
					 		
					 		if(inputJSON.get(CommonConstants.COMMUNICATION_ADDRESS) != null && !inputJSON.get(CommonConstants.COMMUNICATION_ADDRESS).toString().isEmpty()){
					 			supplierDO.setCommunicationAddr(inputJSON.get(CommonConstants.COMMUNICATION_ADDRESS).toString());
					 		}
					 		
					 		
					 		if(inputJSON.get(CommonConstants.ENTITY_OF_STATUS) != null && !inputJSON.get(CommonConstants.ENTITY_OF_STATUS).toString().isEmpty()){
					 			supplierDO.setEntityStatus(inputJSON.get(CommonConstants.ENTITY_OF_STATUS).toString());
					 		}
					 		
					 		if(inputJSON.get(CommonConstants.NAME_OF_DIRECTORS) != null && !inputJSON.get(CommonConstants.NAME_OF_DIRECTORS).toString().isEmpty()){
					 			supplierDO.setNameOfDirectors(inputJSON.get(CommonConstants.NAME_OF_DIRECTORS).toString());
					 		}
					 		
					 		if(inputJSON.get(CommonConstants.PANNO) != null && !inputJSON.get(CommonConstants.PANNO).toString().isEmpty()){
					 			supplierDO.setPan(inputJSON.get(CommonConstants.PANNO).toString());
					 		}
					 		
					 		if(inputJSON.get(CommonConstants.GST_REG_NO) != null && !inputJSON.get(CommonConstants.GST_REG_NO).toString().isEmpty()){
					 			supplierDO.setGstRegNo(inputJSON.get(CommonConstants.GST_REG_NO).toString());
					 		}
					 		
					 		if(inputJSON.get(CommonConstants.CIN) != null && !inputJSON.get(CommonConstants.CIN).toString().isEmpty()){
					 			supplierDO.setCin(inputJSON.get(CommonConstants.CIN).toString());
					 		}
					 		
					 		if(inputJSON.get(CommonConstants.CST_REG_NO) != null && !inputJSON.get(CommonConstants.CST_REG_NO).toString().isEmpty()){
					 			supplierDO.setCstRegNo(inputJSON.get(CommonConstants.CST_REG_NO).toString());
					 		}
					 		
					 		if(inputJSON.get(CommonConstants.CENTRAL_EXCISE_REG_NO) != null && !inputJSON.get(CommonConstants.CENTRAL_EXCISE_REG_NO).toString().isEmpty()){
					 			supplierDO.setCentralExciseRegNo(inputJSON.get(CommonConstants.CENTRAL_EXCISE_REG_NO).toString());
					 		}
					 		
					 		if(inputJSON.get(CommonConstants.ESI_REG_NO) != null && !inputJSON.get(CommonConstants.ESI_REG_NO).toString().isEmpty()){
					 			supplierDO.setEsiRegNo(inputJSON.get(CommonConstants.ESI_REG_NO).toString());
					 		}
					 		
					 		if(inputJSON.get(CommonConstants.PF_REG_NO) != null && !inputJSON.get(CommonConstants.PF_REG_NO).toString().isEmpty()){
					 			supplierDO.setPfRegNo(inputJSON.get(CommonConstants.PF_REG_NO).toString());
					 		}
					 		
					 		if(inputJSON.get(CommonConstants.BANKNAME) != null && !inputJSON.get(CommonConstants.BANKNAME).toString().isEmpty()){
					 			supplierDO.setBankName(inputJSON.get(CommonConstants.BANKNAME).toString());
					 		}
					 		
					 		if(inputJSON.get(CommonConstants.BRANCHNAME) != null && !inputJSON.get(CommonConstants.BRANCHNAME).toString().isEmpty()){
					 			supplierDO.setBankBranch(inputJSON.get(CommonConstants.BRANCHNAME).toString());
					 		}
					 		
					 		if(inputJSON.get(CommonConstants.TYPE_OF_ACCOUNT) != null && !inputJSON.get(CommonConstants.TYPE_OF_ACCOUNT).toString().isEmpty()){
					 			supplierDO.setTypeOfAccount(inputJSON.get(CommonConstants.TYPE_OF_ACCOUNT).toString());
					 		}
					 		
					 		if(inputJSON.get(CommonConstants.ACCOUNTNUMBER) != null && !inputJSON.get(CommonConstants.ACCOUNTNUMBER).toString().isEmpty()){
					 			supplierDO.setAccountNo(Long.parseLong(inputJSON.get(CommonConstants.ACCOUNTNUMBER).toString()));
					 		}
					 		
					 		if(inputJSON.get(CommonConstants.IFSCCODE) != null && !inputJSON.get(CommonConstants.IFSCCODE).toString().isEmpty()){
					 			supplierDO.setIfsCode(inputJSON.get(CommonConstants.IFSCCODE).toString());
					 		}
					 		
					 		if(inputJSON.get(CommonConstants.LIST_OF_PRODUCTS) != null && !inputJSON.get(CommonConstants.LIST_OF_PRODUCTS).toString().isEmpty()){
					 			supplierDO.setListOfProducts(inputJSON.get(CommonConstants.LIST_OF_PRODUCTS).toString());
					 		}
					 		
					 		if(inputJSON.get(CommonConstants.SPECIALIZED_IN) != null && !inputJSON.get(CommonConstants.SPECIALIZED_IN).toString().isEmpty()){
					 			supplierDO.setSpecializedIn(inputJSON.get(CommonConstants.SPECIALIZED_IN).toString());
					 		}
					 		
					 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			supplierDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 		}
					 		
					 		supplierDO.setSupplierContactId(inputJSON.get(CommonConstants.SUPPLIERCONTACT) != null && !inputJSON.get(CommonConstants.SUPPLIERCONTACT).toString().isEmpty() ? inputJSON.get(CommonConstants.SUPPLIERCONTACT).toString() : "");
					 		supplierDO.setFname(inputJSON.get(CommonConstants.FIRSTNAME) != null && !inputJSON.get(CommonConstants.FIRSTNAME).toString().isEmpty() ? inputJSON.get(CommonConstants.FIRSTNAME).toString() : "");
					 		supplierDO.setLname(inputJSON.get(CommonConstants.LASTNAME) != null && !inputJSON.get(CommonConstants.LASTNAME).toString().isEmpty() ? inputJSON.get(CommonConstants.LASTNAME).toString() : "");
					 		supplierDO.setMname(inputJSON.get(CommonConstants.MIDDLENAME) != null && !inputJSON.get(CommonConstants.MIDDLENAME).toString().isEmpty() ? inputJSON.get(CommonConstants.MIDDLENAME).toString() : "");
					 		supplierDO.setDesignation((inputJSON.get(CommonConstants.DESIGNATION) != null && !inputJSON.get(CommonConstants.DESIGNATION).toString().isEmpty() ? inputJSON.get(CommonConstants.DESIGNATION).toString() : ""));
					 		
					 		supplierDO.setEmail((inputJSON.get(CommonConstants.EMAIL) != null && !inputJSON.get(CommonConstants.EMAIL).toString().isEmpty() ? inputJSON.get(CommonConstants.EMAIL).toString() :""));
					 		if(inputJSON.get(CommonConstants.MOBILE) != null && !inputJSON.get(CommonConstants.MOBILE).toString().isEmpty()){
					 			supplierDO.setMobile(inputJSON.get(CommonConstants.MOBILE).toString());
					 		}
					 		if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
					 			supplierDO.setStatus((char)  inputJSON.get(CommonConstants.STATUS).toString().charAt(0));
					 		}
					 		if(inputJSON.get(CommonConstants.REASON) != null && !inputJSON.get(CommonConstants.REASON).toString().isEmpty()){
					 			supplierDO.setReason(inputJSON.get(CommonConstants.REASON).toString());
					 		}
					 		supplierDO.setUpdatedon(new Date());
					 		if(!supplierOnboardService.update(supplierDO)){
								return CommonWebUtil.buildErrorResponse("Supplier OnBoard Details Already Added").toString();
							}
					 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Supplier OnBoard Updated");
				 		}
		 		}else{
		 			return CommonWebUtil.buildErrorResponse(CommonConstants.COMPANYEMAIL_FOUND).toString();
		 		}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) throws InvalidKeyException, NoSuchAlgorithmException, IllegalStateException, SignatureException, IOException, JWTVerifyException {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<SupplierOnboardDO> supplierOnboardList = supplierOnboardService.retrieve();
				respJSON = SupplierOnboardUtil.getSupplierOnboardList(supplierOnboardList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
}
