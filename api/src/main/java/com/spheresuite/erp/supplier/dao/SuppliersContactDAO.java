package com.spheresuite.erp.supplier.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.crm.dao.ContactDAO;
import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.SuppliersContactDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class SuppliersContactDAO {
	static Logger logger = Logger.getLogger(ContactDAO.class.getName());
	@Autowired
	private GenericDAOImpl<SuppliersContactDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public SuppliersContactDO persist(SuppliersContactDO contactDO) {
		try {
			genericObject.persist(contactDO);
		} catch (Exception eException) {
		} finally {
		}
		return contactDO;
	}
	
	
	/*@SuppressWarnings("unchecked")
	public List<Supplier_SuppliersContactDO> retrieve(List<String> ids) {
		List<Supplier_SuppliersContactDO> contactList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(Supplier_SuppliersContactDO.FIND_ALL)
					.setParameter(CommonConstants.ID, ids)
					.list(); 
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return contactList;
	}*/
	
	@SuppressWarnings("unchecked")
	public List<SuppliersContactDO> retrieveAll() {
		List<SuppliersContactDO> contactList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(SuppliersContactDO.FIND_ALL_DATA)
					.list(); 
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return contactList;
	}
	
	@SuppressWarnings("unchecked")
	public List<SuppliersContactDO> retrieveById(Long string) {
		List<SuppliersContactDO> contactList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(SuppliersContactDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, string)
					.list(); 
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return contactList;
	}
	
	@SuppressWarnings("unchecked")
	public List<SuppliersContactDO> retrieveByEmail(String email) {
		List<SuppliersContactDO> contactList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(SuppliersContactDO.FIND_BY_EMAIL_ID)
					.setParameter(CommonConstants.EMAIL, email)
					.list(); 
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return contactList;
	}
	
	@SuppressWarnings("unchecked")
	public List<SuppliersContactDO> retrieveByLeadId(Long id) {
		List<SuppliersContactDO> contactList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(SuppliersContactDO.FIND_BY_SUPPLIERID)
					.setParameter(CommonConstants.ID, id)
					.list(); 
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return contactList;
	}
	
	public SuppliersContactDO update(SuppliersContactDO contactDO) {
		try {
			genericObject.merge(contactDO);
		} catch (Exception eException) {
		} finally {
		}
		return contactDO;
	}
	
	public boolean update(List<SuppliersContactDO> contactDO) {
		try {
			for (SuppliersContactDO contactDO2 : contactDO) {
				genericObject.merge(contactDO2);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}
	
	public boolean persistList(List<SuppliersContactDO> contactDO) {
		try {
			for (SuppliersContactDO contactDO2 : contactDO) {
				genericObject.persist(contactDO2);
			}
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
		}
		return true;
	}
}
