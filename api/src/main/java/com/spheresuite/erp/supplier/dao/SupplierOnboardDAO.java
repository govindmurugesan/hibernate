package com.spheresuite.erp.supplier.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.SupplierOnboardDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class SupplierOnboardDAO {
	static Logger logger = Logger.getLogger(SupplierOnboardDAO.class.getName());
	@Autowired
	private GenericDAOImpl<SupplierOnboardDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(SupplierOnboardDO empDetail) {
		boolean persistStatus = true;
		try {
			List<SupplierOnboardDO> supplierList = null;
			supplierList = this.sessionFactory.getCurrentSession().getNamedQuery(SupplierOnboardDO.FIND_BY_EMAILID_STATUS)
					.setParameter(CommonConstants.EMAIL, empDetail.getEmail())
					.list();
					if(supplierList != null && supplierList.size() > 0){
						persistStatus = false;
					}else{
						genericObject.persist(empDetail);
					}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	public List<SupplierOnboardDO> retrieve() {
		List<SupplierOnboardDO> employeeOnboardList = null;
		try {
			employeeOnboardList = genericObject.retrieve(SupplierOnboardDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return employeeOnboardList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<SupplierOnboardDO> retrieveById(Long id){
		List<SupplierOnboardDO> employeeOnboardList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(SupplierOnboardDO.FIND_BY_ID)
			.setParameter(CommonConstants.ID, id)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeOnboardList;
	}
	
	@SuppressWarnings("unchecked")
	public List<SupplierOnboardDO> retrieveByEmail(String personalEmail){
		List<SupplierOnboardDO> employeeOnboardList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(SupplierOnboardDO.FIND_BY_EMAIL)
			.setParameter(CommonConstants.PERSONALEMAIL, personalEmail)
			.setParameter(CommonConstants.STATUS, CommonConstants.RSTATUS)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeOnboardList;
	}
	

	@SuppressWarnings("unchecked")
	public boolean update(SupplierOnboardDO empDetails) {
		boolean updateStatus = true;
		try {
			List<SupplierOnboardDO> empDetailsList = null;
			empDetailsList = this.sessionFactory.getCurrentSession().getNamedQuery(SupplierOnboardDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.EMAIL, empDetails.getEmail())
					.setParameter(CommonConstants.ID, empDetails.getSupplierOnboardId())
					.list();
			if(empDetailsList != null && empDetailsList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(empDetails);
			}
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}

	public boolean delete(Long id) {
		try {
			this.sessionFactory.getCurrentSession().createQuery("delete from SupplierOnboardDO e where e.id=:id")
										.setParameter(CommonConstants.ID, id)
										.executeUpdate();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return true;
	}

}
