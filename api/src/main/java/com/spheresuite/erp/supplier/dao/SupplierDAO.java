package com.spheresuite.erp.supplier.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.SuppliersDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class SupplierDAO {
	static Logger logger = Logger.getLogger(SupplierDAO.class.getName());
	@Autowired
	private GenericDAOImpl<SuppliersDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public SuppliersDO persist(SuppliersDO supplierDO)  {
		try {
			genericObject.persist(supplierDO);
		} catch (Exception eException) {
		} finally {
		}
		return supplierDO;
	}
	
	@SuppressWarnings("unchecked")
	public List<SuppliersDO> retrieveAll()  {
		List<SuppliersDO> supplierList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(SuppliersDO.FIND_ALL_SUPPLIER)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return supplierList;
	}
	
	@SuppressWarnings("unchecked")
	public List<SuppliersDO> retrieveById(Long Id)  {
		List<SuppliersDO> supplierList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(SuppliersDO.FIND_BY_ID)
			.setParameter(CommonConstants.ID, Id)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return supplierList;
	}
	
	
	public SuppliersDO update(SuppliersDO supplier)  {
		try {
			genericObject.merge(supplier);
		} catch (Exception eException) {
		} finally {
		}
		return supplier;
	}
	
	public boolean persistList(List<SuppliersDO> supplierDO)  {
		try {
			for (SuppliersDO supplier : supplierDO) {
				genericObject.persist(supplier);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}

	public boolean update(List<SuppliersDO> supplierList)  {
		try {
			for (SuppliersDO supplier : supplierList) {
				genericObject.merge(supplier);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}
}
