package com.spheresuite.erp.supplier.web.util;

import java.util.List;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.SuppliersDO;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
@Component
public class SupplierUtil {
	
	private SupplierUtil() {}
	@Autowired
	private EmployeeService temployeeService;
	private static EmployeeService employeeService;
	
	@PostConstruct
	public void init() {
		employeeService = temployeeService;
	}
	
	public static JSONObject getSupplierList(List<SuppliersDO> supplierList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (SuppliersDO supplier : supplierList) {
				resultJSONArray.put(getSupplierDetailObject(supplier));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getLeadList(Long totalCustomer) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			JSONObject result = new JSONObject();
			result.put(CommonConstants.CUSTOMERLIST, String.valueOf(totalCustomer));
			resultJSONArray.put(result);
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getSupplierListWithOutPic(List<SuppliersDO> leadList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (SuppliersDO lead : leadList) {
				resultJSONArray.put(getSupplierDetailObjectWithOutPic(lead));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getLeadLimitData(List<SuppliersDO> leadList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (SuppliersDO lead : leadList) {
				resultJSONArray.put(getLeadLimitDataDetailObject(lead));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	

	public static JSONObject getSupplierDetailObjectWithOutPic(SuppliersDO supplier)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(supplier.getSupplierId()));
		result.put(CommonConstants.NAME, String.valueOf(supplier.getName()));
		if(supplier.getRegisteredAddr() !=null){
			result.put(CommonConstants.REGISTERED_ADDRESS, String.valueOf(supplier.getRegisteredAddr()));
		}else{
			result.put(CommonConstants.REGISTERED_ADDRESS, "");
		}
		
		if(supplier.getCommunicationAddr() !=null){
			result.put(CommonConstants.COMMUNICATION_ADDRESS, String.valueOf(supplier.getCommunicationAddr()));
		}else{
			result.put(CommonConstants.COMMUNICATION_ADDRESS, "");
		}
		
		if(supplier.getEntityStatus() !=null){
			result.put(CommonConstants.ENTITY_OF_STATUS, String.valueOf(supplier.getEntityStatus()));
		}else{
			result.put(CommonConstants.ENTITY_OF_STATUS, "");
		}
		
		if(supplier.getNameOfDirectors() !=null){
			result.put(CommonConstants.NAME_OF_DIRECTORS, String.valueOf(supplier.getNameOfDirectors()));
		}else{
			result.put(CommonConstants.NAME_OF_DIRECTORS, "");
		}
		if(supplier.getPan() !=null){
			result.put(CommonConstants.PANNO, String.valueOf(supplier.getPan()));
		}else{
			result.put(CommonConstants.PANNO, "");
		}
		if(supplier.getGstRegNo() !=null){
			result.put(CommonConstants.GST_REG_NO, String.valueOf(supplier.getGstRegNo()));
		}else{
			result.put(CommonConstants.GST_REG_NO, "");
		}
		if(supplier.getCin() !=null){
			result.put(CommonConstants.CIN, String.valueOf(supplier.getCin()));
		}else{
			result.put(CommonConstants.CIN, "");
		}
		if(supplier.getCstRegNo() !=null){
			result.put(CommonConstants.CST_REG_NO, String.valueOf(supplier.getCstRegNo()));
		}else{
			result.put(CommonConstants.CST_REG_NO, "");
		}
		
		if(supplier.getCentralExciseRegNo() !=null){
			result.put(CommonConstants.CENTRAL_EXCISE_REG_NO, String.valueOf(supplier.getCentralExciseRegNo()));
		}else{
			result.put(CommonConstants.CENTRAL_EXCISE_REG_NO, "");
		}
		if(supplier.getEsiRegNo() != null){
			result.put(CommonConstants.ESI_REG_NO, String.valueOf(supplier.getEsiRegNo()));
		}else{
			result.put(CommonConstants.ESI_REG_NO, "");
		}
		
		if(supplier.getPfRegNo() != null){
			result.put(CommonConstants.PF_REG_NO, String.valueOf(supplier.getPfRegNo()));
		}else{
			result.put(CommonConstants.PF_REG_NO, "");
		}
		
		if(supplier.getBankName() != null){
			result.put(CommonConstants.BANKNAME, String.valueOf(supplier.getBankName()));
		}else{
			result.put(CommonConstants.BANKNAME, "");
		}
		
		if(supplier.getBankBranch() != null){
			result.put(CommonConstants.BRANCHNAME, String.valueOf(supplier.getBankBranch()));
		}else{
			result.put(CommonConstants.BRANCHNAME, "");
		}
		
		if(supplier.getAccountNo() != null){
			result.put(CommonConstants.ACCOUNTNUMBER, String.valueOf(supplier.getAccountNo()));
		}else{
			result.put(CommonConstants.ACCOUNTNUMBER, "");
		}
		
		if(supplier.getIfsCode() != null){
			result.put(CommonConstants.IFSCCODE, String.valueOf(supplier.getIfsCode()));
		}else{
			result.put(CommonConstants.IFSCCODE, "");
		}
		
		if(supplier.getTypeOfAccount() != null){
			result.put(CommonConstants.TYPE_OF_ACCOUNT, String.valueOf(supplier.getTypeOfAccount()));
		}else{
			result.put(CommonConstants.TYPE_OF_ACCOUNT, "");
		}
		
		if(supplier.getListOfProducts() != null){
			result.put(CommonConstants.LIST_OF_PRODUCTS, String.valueOf(supplier.getListOfProducts()));
		}else{
			result.put(CommonConstants.LIST_OF_PRODUCTS, "");
		}
		
		if(supplier.getSpecializedIn() != null){
			result.put(CommonConstants.SPECIALIZED_IN, String.valueOf(supplier.getSpecializedIn()));
		}else{
			result.put(CommonConstants.SPECIALIZED_IN, "");
		}
		
		if(supplier.getUpdatedon() != null) result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(supplier.getUpdatedon())));

		if(supplier.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(supplier.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}		
		
		return result;
	}
	
	public static JSONObject getSupplierDetailObject(SuppliersDO supplier)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(supplier.getSupplierId()));
		result.put(CommonConstants.NAME, String.valueOf(supplier.getName()));
		if(supplier.getRegisteredAddr() !=null){
			result.put(CommonConstants.REGISTERED_ADDRESS, String.valueOf(supplier.getRegisteredAddr()));
		}else{
			result.put(CommonConstants.REGISTERED_ADDRESS, "");
		}
		
		if(supplier.getCommunicationAddr() !=null){
			result.put(CommonConstants.COMMUNICATION_ADDRESS, String.valueOf(supplier.getCommunicationAddr()));
		}else{
			result.put(CommonConstants.COMMUNICATION_ADDRESS, "");
		}
		
		if(supplier.getEntityStatus() !=null){
			result.put(CommonConstants.ENTITY_OF_STATUS, String.valueOf(supplier.getEntityStatus()));
		}else{
			result.put(CommonConstants.ENTITY_OF_STATUS, "");
		}
		
		if(supplier.getNameOfDirectors() !=null){
			result.put(CommonConstants.NAME_OF_DIRECTORS, String.valueOf(supplier.getNameOfDirectors()));
		}else{
			result.put(CommonConstants.NAME_OF_DIRECTORS, "");
		}
		if(supplier.getPan() !=null){
			result.put(CommonConstants.PANNO, String.valueOf(supplier.getPan()));
		}else{
			result.put(CommonConstants.PANNO, "");
		}
		if(supplier.getGstRegNo() !=null){
			result.put(CommonConstants.GST_REG_NO, String.valueOf(supplier.getGstRegNo()));
		}else{
			result.put(CommonConstants.GST_REG_NO, "");
		}
		if(supplier.getCin() !=null){
			result.put(CommonConstants.CIN, String.valueOf(supplier.getCin()));
		}else{
			result.put(CommonConstants.CIN, "");
		}
		if(supplier.getCstRegNo() !=null){
			result.put(CommonConstants.CST_REG_NO, String.valueOf(supplier.getCstRegNo()));
		}else{
			result.put(CommonConstants.CST_REG_NO, "");
		}
		
		if(supplier.getCentralExciseRegNo() !=null){
			result.put(CommonConstants.CENTRAL_EXCISE_REG_NO, String.valueOf(supplier.getCentralExciseRegNo()));
		}else{
			result.put(CommonConstants.CENTRAL_EXCISE_REG_NO, "");
		}
		if(supplier.getEsiRegNo() != null){
			result.put(CommonConstants.ESI_REG_NO, String.valueOf(supplier.getEsiRegNo()));
		}else{
			result.put(CommonConstants.ESI_REG_NO, "");
		}
		
		if(supplier.getPfRegNo() != null){
			result.put(CommonConstants.PF_REG_NO, String.valueOf(supplier.getPfRegNo()));
		}else{
			result.put(CommonConstants.PF_REG_NO, "");
		}
		
		if(supplier.getBankName() != null){
			result.put(CommonConstants.BANKNAME, String.valueOf(supplier.getBankName()));
		}else{
			result.put(CommonConstants.BANKNAME, "");
		}
		
		if(supplier.getBankBranch() != null){
			result.put(CommonConstants.BRANCHNAME, String.valueOf(supplier.getBankBranch()));
		}else{
			result.put(CommonConstants.BRANCHNAME, "");
		}
		
		if(supplier.getAccountNo() != null){
			result.put(CommonConstants.ACCOUNTNUMBER, String.valueOf(supplier.getAccountNo()));
		}else{
			result.put(CommonConstants.ACCOUNTNUMBER, "");
		}
		
		if(supplier.getIfsCode() != null){
			result.put(CommonConstants.IFSCCODE, String.valueOf(supplier.getIfsCode()));
		}else{
			result.put(CommonConstants.IFSCCODE, "");
		}
		
		if(supplier.getTypeOfAccount() != null){
			result.put(CommonConstants.TYPE_OF_ACCOUNT, String.valueOf(supplier.getTypeOfAccount()));
		}else{
			result.put(CommonConstants.TYPE_OF_ACCOUNT, "");
		}
		
		if(supplier.getListOfProducts() != null){
			result.put(CommonConstants.LIST_OF_PRODUCTS, String.valueOf(supplier.getListOfProducts()));
		}else{
			result.put(CommonConstants.LIST_OF_PRODUCTS, "");
		}
		
		if(supplier.getSpecializedIn() != null){
			result.put(CommonConstants.SPECIALIZED_IN, String.valueOf(supplier.getSpecializedIn()));
		}else{
			result.put(CommonConstants.SPECIALIZED_IN, "");
		}
		
		if(supplier.getUpdatedon() != null) result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(supplier.getUpdatedon())));

		if(supplier.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(supplier.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		
		return result;
	}
	
	public static JSONObject getLeadLimitDataDetailObject(SuppliersDO supplier)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(supplier.getSupplierId()));
		result.put(CommonConstants.NAME, String.valueOf(supplier.getName()));
		return result;
	}
	
	public static JSONObject getCustomerListForChart(List<SuppliersDO> supplierList, List<SuppliersDO> newsupplierList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			int newSupplier = 0, totalSupplier = 0;
			totalSupplier = (supplierList.size() - newsupplierList.size());
			newSupplier  = newsupplierList.size();
			JSONArray resultJSONArray = new JSONArray();
			JSONObject result = new JSONObject();
			result.put(CommonConstants.NEWCUSTOMER, String.valueOf(newSupplier));
			result.put(CommonConstants.CUSTOMERLIST, String.valueOf(totalSupplier));
			resultJSONArray.put(result);			
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
}
