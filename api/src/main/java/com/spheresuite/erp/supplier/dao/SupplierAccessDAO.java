package com.spheresuite.erp.supplier.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.SupplierAccessDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class SupplierAccessDAO {
	static Logger logger = Logger.getLogger(SupplierAccessDAO.class.getName());
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private GenericDAOImpl<SupplierAccessDO> genericObject;
	
	@SuppressWarnings("unchecked")
	public boolean persist(SupplierAccessDO supplierUserDO) {
		boolean persistStatus = true;
		try {
			List<SupplierAccessDO> supplierUserDOList = null;
			supplierUserDOList = this.sessionFactory.getCurrentSession().getNamedQuery(SupplierAccessDO.FIND_BY_SUPPLIER_ID)
					.setParameter(CommonConstants.USERID, supplierUserDO.getContact().getSupplierContactId())
					.list();
					if(supplierUserDOList != null && supplierUserDOList.size() > 0){
						persistStatus = false;
					}else{
						genericObject.persist(supplierUserDO);
					}
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
		}
		return persistStatus;
	}
	
	
	public List<SupplierAccessDO> retrieve() {
		List<SupplierAccessDO> userList = null;
		try {
			userList = genericObject.retrieve(SupplierAccessDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return userList;
	}
	
	public List<SupplierAccessDO> retrieveActive() {
		List<SupplierAccessDO> userList = null;
		try {
			userList =  genericObject.retrieveActive(CommonConstants.ACTIVE, SupplierAccessDO.FIND_BY_STATUS, SupplierAccessDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return userList;
	}
	
	@SuppressWarnings("unchecked")
	public List<SupplierAccessDO> retrieveById(Long id) {
		List<SupplierAccessDO> user = null;
		try {
			user = this.sessionFactory.getCurrentSession().getNamedQuery(SupplierAccessDO.FIND_BY_ID)
										.setParameter(CommonConstants.ID, id)
										.list();
		} catch (Exception eException) {
		} finally {
		}
		return user;
	}
	
	@SuppressWarnings("unchecked")
	public List<SupplierAccessDO> retrieveByEmail(String personal, String secondary) {
		List<SupplierAccessDO> user = null;
		try {
			user = this.sessionFactory.getCurrentSession().getNamedQuery(SupplierAccessDO.FIND_BY_EMAIL_FOR_EMP)
										.setParameter(CommonConstants.PERSONAL, personal)
										.setParameter(CommonConstants.SECONDARY, secondary)
										.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return user;
	}
	
	@SuppressWarnings("unchecked")
	public List<SupplierAccessDO> retrieveByEmailId(String email) {
		List<SupplierAccessDO> user = null;
		try {
			user = this.sessionFactory.getCurrentSession().getNamedQuery(SupplierAccessDO.FIND_BY_EMAIL)
					.setParameter(CommonConstants.EMAIL, email)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return user;
	}
	
	@SuppressWarnings("unchecked")
	public List<SupplierAccessDO> retrieveByTempPassword(String tempPassword, Long empId) {
		List<SupplierAccessDO> user = null;
		try {
			user = this.sessionFactory.getCurrentSession().getNamedQuery(SupplierAccessDO.FIND_BY_TEMPPASSWORD)
					.setParameter(CommonConstants.TEMP_PASSWORD, tempPassword)
					.setParameter(CommonConstants.EMPID, empId)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return user;
	}
	
	/*public SupplierUserDO update(SupplierUserDO supplierUserDO) {
		try {
			genericObject.merge(supplierUserDO);
		} catch (Exception eException) {
		} finally {
		}
		return supplierUserDO;
	}*/
	
	@SuppressWarnings("unchecked")
	public boolean update(SupplierAccessDO supplierUserDO) {
		boolean updateStatus = true;
		try {
			List<SupplierAccessDO> supplierUserDOList = null;
			supplierUserDOList = this.sessionFactory.getCurrentSession().getNamedQuery(SupplierAccessDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.EMAIL, supplierUserDO.getContact().getEmail())
					.setParameter(CommonConstants.ID, supplierUserDO.getSupplierUserId())
					.list();
			if(supplierUserDOList != null && supplierUserDOList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(supplierUserDO);
			}
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
		}
		return updateStatus;
	}
	
	public boolean updatePassword(SupplierAccessDO supplierUserDO) {
		boolean updateStatus = true;
		try {
				genericObject.merge(supplierUserDO);
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
		}
		return updateStatus;
	}
	
	@SuppressWarnings("unchecked")
	public List<SupplierAccessDO> retrieveForLogin(String email,String password) {
		List<SupplierAccessDO> user = null;
		try {
			user = this.sessionFactory.getCurrentSession().getNamedQuery(SupplierAccessDO.FIND_FOR_LOGIN)
					.setParameter(CommonConstants.EMAIL, email)
					.setParameter(CommonConstants.PASSWORD, password)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return user;
	}
	
	@SuppressWarnings("unchecked")
	public List<SupplierAccessDO> retriveBySupplierId(String id) {
		List<SupplierAccessDO> user = null;
		try {
			user = this.sessionFactory.getCurrentSession().getNamedQuery(SupplierAccessDO.FIND_BY_SUPPLIER_ID)
										.setParameter(CommonConstants.USERID, Long.parseLong(id))
										.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return user;
	}
	
	@SuppressWarnings("unchecked")
	public List<SupplierAccessDO> getSuperAdmins() {
		List<SupplierAccessDO> userList = null;
		try {
			userList = this.sessionFactory.getCurrentSession().getNamedQuery(SupplierAccessDO.FIND_SUPERADMIN)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.setParameter(CommonConstants.ADMIN, Long.parseLong(CommonConstants.ADMINID))
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return userList;
	}

}
