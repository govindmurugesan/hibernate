package com.spheresuite.erp.supplier.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.domainobject.SupplierOnboardDO;
import com.spheresuite.erp.supplier.dao.SupplierOnboardDAO;
@Service
@Transactional
public class SupplierOnboardService {
	static Logger logger = Logger.getLogger(SupplierOnboardService.class.getName());
	@Autowired
	private SupplierOnboardDAO supplierOnboardDAO;

	@Transactional
	public Boolean persist(SupplierOnboardDO supplierOnboardDO) {
		return supplierOnboardDAO.persist(supplierOnboardDO);
	}
	
	@Transactional
	public boolean delete(Long id) {
		return supplierOnboardDAO.delete(id);
	}

	@Transactional
	public List<SupplierOnboardDO> retriveById(Long id) {
		return supplierOnboardDAO.retrieveById(id);
	}

	@Transactional
	public List<SupplierOnboardDO> retrieve() {
		return supplierOnboardDAO.retrieve();
	}

	@Transactional
	public List<SupplierOnboardDO> retrieveByEmail(String personalEmail) {
		return supplierOnboardDAO.retrieveByEmail(personalEmail);
	}

	@Transactional
	public Boolean update(SupplierOnboardDO supplierOnboardDO) {
		return supplierOnboardDAO.update(supplierOnboardDO);
	}

}
