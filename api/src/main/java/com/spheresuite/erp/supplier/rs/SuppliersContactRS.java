package com.spheresuite.erp.supplier.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.SuppliersContactDO;
import com.spheresuite.erp.domainobject.SuppliersDO;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.RolesService;
import com.spheresuite.erp.service.UserService;
import com.spheresuite.erp.supplier.service.SupplierService;
import com.spheresuite.erp.supplier.service.SuppliersContactService;
import com.spheresuite.erp.supplier.web.util.SupplierContactUtil;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/suppliersContact")
public class SuppliersContactRS {

	String validation = null;
	static Logger logger = Logger.getLogger(SuppliersContactRS.class.getName());

	@Autowired
	private SuppliersContactService contactService;
	
	@Autowired
	private EmployeeService employeeService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private RolesService rolesService;
	
	@Autowired
	private SupplierService supplierService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				SuppliersContactDO supplierContactDO = new SuppliersContactDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		//supplierContactDO.setSupplierContactId(inputJSON.get(CommonConstants.SUPPLIERCONTACT) != null && !inputJSON.get(CommonConstants.SUPPLIERCONTACT).toString().isEmpty() ? inputJSON.get(CommonConstants.SUPPLIERCONTACT).toString() : "");
			 		if(inputJSON.get(CommonConstants.SUPPLIERID) != null && !inputJSON.get(CommonConstants.SUPPLIERID).toString().isEmpty()){
			 			List<SuppliersDO> supplierList = supplierService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.SUPPLIERID).toString()));
			 			if(supplierList != null && supplierList.size() > 0){
			 				supplierContactDO.setSuppliers(supplierList.get(0));
			 			}
			 		}
			 		supplierContactDO.setFname(inputJSON.get(CommonConstants.FIRSTNAME) != null && !inputJSON.get(CommonConstants.FIRSTNAME).toString().isEmpty() ? inputJSON.get(CommonConstants.FIRSTNAME).toString() : "");
			 		supplierContactDO.setLname(inputJSON.get(CommonConstants.LASTNAME) != null && !inputJSON.get(CommonConstants.LASTNAME).toString().isEmpty() ? inputJSON.get(CommonConstants.LASTNAME).toString() : "");
			 		supplierContactDO.setMname(inputJSON.get(CommonConstants.MIDDLENAME) != null && !inputJSON.get(CommonConstants.MIDDLENAME).toString().isEmpty() ? inputJSON.get(CommonConstants.MIDDLENAME).toString() : "");
			 		supplierContactDO.setDesignation((inputJSON.get(CommonConstants.DESIGNATION) != null && !inputJSON.get(CommonConstants.DESIGNATION).toString().isEmpty() ? inputJSON.get(CommonConstants.DESIGNATION).toString() : ""));
			 		
			 		supplierContactDO.setEmail((inputJSON.get(CommonConstants.EMAIL) != null && !inputJSON.get(CommonConstants.EMAIL).toString().isEmpty() ? inputJSON.get(CommonConstants.EMAIL).toString() :""));
			 		if(inputJSON.get(CommonConstants.MOBILE) != null && !inputJSON.get(CommonConstants.MOBILE).toString().isEmpty()){
			 			supplierContactDO.setMobile(inputJSON.get(CommonConstants.MOBILE).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			supplierContactDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			supplierContactDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		supplierContactDO.setUpdatedon(new Date());
			 		supplierContactDO.setCreatedon(new Date());
			 		contactService.persist(supplierContactDO);
			 	}
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Contact Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retriveById/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveById(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<SuppliersContactDO> contactList = contactService.retriveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = SupplierContactUtil.getContactList(contactList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	
		
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<SuppliersContactDO> contactList = new ArrayList<SuppliersContactDO>();
				contactList = contactService.retrieveAll();
				respJSON = SupplierContactUtil.getContactList(contactList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				SuppliersContactDO supplierContactDO = new SuppliersContactDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.SUPPLIERCONTACT).toString().isEmpty()){
			 		List<SuppliersContactDO> contactList = contactService.retriveById(Long.parseLong(inputJSON.get(CommonConstants.SUPPLIERCONTACT).toString()));
			 		supplierContactDO = contactList.get(0);
			 		if(inputJSON.get(CommonConstants.SUPPLIERID) != null && !inputJSON.get(CommonConstants.SUPPLIERID).toString().isEmpty()){
			 			List<SuppliersDO> supplierList = supplierService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.SUPPLIERID).toString()));
			 			if(supplierList != null && supplierList.size() > 0){
			 				supplierContactDO.setSuppliers(supplierList.get(0));
			 			}
			 		}
			 		supplierContactDO.setFname(inputJSON.get(CommonConstants.FIRSTNAME) != null && !inputJSON.get(CommonConstants.FIRSTNAME).toString().isEmpty() ? inputJSON.get(CommonConstants.FIRSTNAME).toString() : "");
			 		supplierContactDO.setLname(inputJSON.get(CommonConstants.LASTNAME) != null && !inputJSON.get(CommonConstants.LASTNAME).toString().isEmpty() ? inputJSON.get(CommonConstants.LASTNAME).toString() : "");
			 		supplierContactDO.setMname(inputJSON.get(CommonConstants.MIDDLENAME) != null && !inputJSON.get(CommonConstants.MIDDLENAME).toString().isEmpty() ? inputJSON.get(CommonConstants.MIDDLENAME).toString() : "");
			 		supplierContactDO.setDesignation((inputJSON.get(CommonConstants.DESIGNATION) != null && !inputJSON.get(CommonConstants.DESIGNATION).toString().isEmpty() ? inputJSON.get(CommonConstants.DESIGNATION).toString() : ""));
			 		
			 		supplierContactDO.setEmail((inputJSON.get(CommonConstants.EMAIL) != null && !inputJSON.get(CommonConstants.EMAIL).toString().isEmpty() ? inputJSON.get(CommonConstants.EMAIL).toString() :""));
			 		if(inputJSON.get(CommonConstants.MOBILE) != null && !inputJSON.get(CommonConstants.MOBILE).toString().isEmpty()){
			 			supplierContactDO.setMobile(inputJSON.get(CommonConstants.MOBILE).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			supplierContactDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		supplierContactDO.setUpdatedon(new Date());
			 		contactService.update(supplierContactDO);
			 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Contact Updated");
			 	}

			 }else{
					return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveBySupplierId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByLeadId(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.SUPPLIERID).toString().isEmpty()){
					List<SuppliersContactDO> contactList = contactService.retriveByLeadId(Long.parseLong(inputJSON.get(CommonConstants.SUPPLIERID).toString()));
					respJSON = SupplierContactUtil.getContactList(contactList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/importContact/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String importEmp(@PathVariable String inputParams,  Model model, HttpServletRequest request) {
			
		try {
			//if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY); 
				org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
				JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
				List<SuppliersContactDO> contactDOlist = new ArrayList<SuppliersContactDO>();
				
				for (int i=0; i < fileData.size(); i++){
					SuppliersContactDO contactDO = new SuppliersContactDO();
					JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
			 		
					if(colName.get(CommonConstants.LEADTYPE) != null){
						if(!rowJSON.get(colName.get(CommonConstants.LEADTYPE)).toString().isEmpty() && rowJSON.get(colName.get(CommonConstants.LEADTYPE)) != null){
							List<SuppliersDO> leadList = supplierService.retrieveById(Long.parseLong(rowJSON.get(colName.get(CommonConstants.LEADTYPE)).toString()));
				 			if(leadList != null && leadList.size() > 0){
				 				contactDO.setSuppliers(leadList.get(0));
				 			}
				 			//contactDO.setLead(Long.parseLong(rowJSON.get(colName.get(CommonConstants.LEADTYPE)).toString()));
				 		}
					}
			 		if(colName.get(CommonConstants.FIRSTNAME) != null){
				 		contactDO.setFname(!rowJSON.get(colName.get(CommonConstants.FIRSTNAME)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.FIRSTNAME)).toString() : "");
			 		}
					if(colName.get(CommonConstants.LASTNAME) != null){
				 		contactDO.setLname(!rowJSON.get(colName.get(CommonConstants.LASTNAME)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.LASTNAME)).toString() : "");
					}
					if(colName.get(CommonConstants.MIDDLENAME) != null){
						contactDO.setMname(!rowJSON.get(colName.get(CommonConstants.MIDDLENAME)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.MIDDLENAME)).toString() : "");
					}
					if(colName.get(CommonConstants.DESIGNATION) != null){
						contactDO.setDesignation((!rowJSON.get(colName.get(CommonConstants.DESIGNATION)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.DESIGNATION)).toString() : ""));
					}
					
					if(colName.get(CommonConstants.EMAIL) != null){
				 		contactDO.setEmail((!rowJSON.get(colName.get(CommonConstants.EMAIL)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.EMAIL)).toString() : ""));
					}
					
					if(colName.get(CommonConstants.MOBILE) != null){
						if(!rowJSON.get(colName.get(CommonConstants.MOBILE)).toString().isEmpty()){
				 			contactDO.setMobile(rowJSON != null ? rowJSON.get(colName.get(CommonConstants.MOBILE)).toString() : "");
				 		}
					}
					
			 		contactDO.setUpdatedon(new Date());
			 		contactDO.setCreatedon(new Date());
			 		if(updatedBy != null && ! updatedBy.toString().isEmpty()){
			 			contactDO.setUpdatedby(updatedBy.toString());
			 			contactDO.setCreatedby(updatedBy.toString());
			 		}
					contactDOlist.add(contactDO);
				}
				
				contactService.persistList(contactDOlist);
				
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
		
}
