package com.spheresuite.erp.supplier.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.ContactDO;
import com.spheresuite.erp.domainobject.SuppliersContactDO;
import com.spheresuite.erp.domainobject.SuppliersDO;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.RolesService;
import com.spheresuite.erp.service.UserService;
import com.spheresuite.erp.supplier.service.SupplierService;
import com.spheresuite.erp.supplier.service.SuppliersContactService;
import com.spheresuite.erp.supplier.web.util.SupplierUtil;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/supplier")
public class SupplierRS {

	String validation = null;
	static Logger logger = Logger.getLogger(SupplierRS.class.getName());
	
	@Autowired
	private SupplierService supplierService;
	
	@Autowired
	private SuppliersContactService contactService;
	
	@Autowired
	private EmployeeService employeeService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private RolesService rolesService;
	
	
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		SuppliersDO supplierDetails	= new SuppliersDO();
		try {
			if (WebManager.authenticateSession(request)) {
				SuppliersDO supplierDO = new SuppliersDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
			 			supplierDO.setName(inputJSON.get(CommonConstants.NAME).toString().toUpperCase());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.REGISTERED_ADDRESS) != null && !inputJSON.get(CommonConstants.REGISTERED_ADDRESS).toString().isEmpty()){
			 			supplierDO.setRegisteredAddr(inputJSON.get(CommonConstants.REGISTERED_ADDRESS).toString().toUpperCase());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.COMMUNICATION_ADDRESS) != null && !inputJSON.get(CommonConstants.COMMUNICATION_ADDRESS).toString().isEmpty()){
			 			supplierDO.setCommunicationAddr(inputJSON.get(CommonConstants.COMMUNICATION_ADDRESS).toString());
			 		}
			 		
			 		
			 		if(inputJSON.get(CommonConstants.ENTITY_OF_STATUS) != null && !inputJSON.get(CommonConstants.ENTITY_OF_STATUS).toString().isEmpty()){
			 			supplierDO.setEntityStatus(inputJSON.get(CommonConstants.ENTITY_OF_STATUS).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.NAME_OF_DIRECTORS) != null && !inputJSON.get(CommonConstants.NAME_OF_DIRECTORS).toString().isEmpty()){
			 			supplierDO.setNameOfDirectors(inputJSON.get(CommonConstants.NAME_OF_DIRECTORS).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.PANNO) != null && !inputJSON.get(CommonConstants.PANNO).toString().isEmpty()){
			 			supplierDO.setPan(inputJSON.get(CommonConstants.PANNO).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.GST_REG_NO) != null && !inputJSON.get(CommonConstants.GST_REG_NO).toString().isEmpty()){
			 			supplierDO.setGstRegNo(inputJSON.get(CommonConstants.GST_REG_NO).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.CIN) != null && !inputJSON.get(CommonConstants.CIN).toString().isEmpty()){
			 			supplierDO.setCin(inputJSON.get(CommonConstants.CIN).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.CST_REG_NO) != null && !inputJSON.get(CommonConstants.CST_REG_NO).toString().isEmpty()){
			 			supplierDO.setCstRegNo(inputJSON.get(CommonConstants.CST_REG_NO).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.CENTRAL_EXCISE_REG_NO) != null && !inputJSON.get(CommonConstants.CENTRAL_EXCISE_REG_NO).toString().isEmpty()){
			 			supplierDO.setCentralExciseRegNo(inputJSON.get(CommonConstants.CENTRAL_EXCISE_REG_NO).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.ESI_REG_NO) != null && !inputJSON.get(CommonConstants.ESI_REG_NO).toString().isEmpty()){
			 			supplierDO.setEsiRegNo(inputJSON.get(CommonConstants.ESI_REG_NO).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.PF_REG_NO) != null && !inputJSON.get(CommonConstants.PF_REG_NO).toString().isEmpty()){
			 			supplierDO.setPfRegNo(inputJSON.get(CommonConstants.PF_REG_NO).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.BANKNAME) != null && !inputJSON.get(CommonConstants.BANKNAME).toString().isEmpty()){
			 			supplierDO.setBankName(inputJSON.get(CommonConstants.BANKNAME).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.BRANCHNAME) != null && !inputJSON.get(CommonConstants.BRANCHNAME).toString().isEmpty()){
			 			supplierDO.setBankBranch(inputJSON.get(CommonConstants.BRANCHNAME).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.TYPE_OF_ACCOUNT) != null && !inputJSON.get(CommonConstants.TYPE_OF_ACCOUNT).toString().isEmpty()){
			 			supplierDO.setTypeOfAccount(inputJSON.get(CommonConstants.TYPE_OF_ACCOUNT).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.ACCOUNTNUMBER) != null && !inputJSON.get(CommonConstants.ACCOUNTNUMBER).toString().isEmpty()){
			 			supplierDO.setAccountNo(Long.parseLong(inputJSON.get(CommonConstants.ACCOUNTNUMBER).toString()));
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.IFSCCODE) != null && !inputJSON.get(CommonConstants.IFSCCODE).toString().isEmpty()){
			 			supplierDO.setIfsCode(inputJSON.get(CommonConstants.IFSCCODE).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.LIST_OF_PRODUCTS) != null && !inputJSON.get(CommonConstants.LIST_OF_PRODUCTS).toString().isEmpty()){
			 			supplierDO.setListOfProducts(inputJSON.get(CommonConstants.LIST_OF_PRODUCTS).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.SPECIALIZED_IN) != null && !inputJSON.get(CommonConstants.SPECIALIZED_IN).toString().isEmpty()){
			 			supplierDO.setSpecializedIn(inputJSON.get(CommonConstants.SPECIALIZED_IN).toString());
			 		}
			 		
			 		supplierDO.setCreatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			supplierDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			supplierDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		supplierDO.setUpdatedon(new Date());
			 		
			 	}
			 	supplierDetails	= supplierService.persist(supplierDO);
			 	if(!inputJSON.get(CommonConstants.SKIP).toString().isEmpty()){
			 		if(inputJSON.get(CommonConstants.SKIP).toString().equalsIgnoreCase("no")){
			 			if(supplierDetails.getSupplierId() != null){
					 		SuppliersContactDO supplierContactDO = new SuppliersContactDO();
					 		supplierContactDO.setSuppliers(supplierDO);
					 		//supplierContactDO.setSupplierContactId(inputJSON.get(CommonConstants.SUPPLIERCONTACT) != null && !inputJSON.get(CommonConstants.SUPPLIERCONTACT).toString().isEmpty() ? inputJSON.get(CommonConstants.SUPPLIERCONTACT).toString() : "");
					 		supplierContactDO.setFname(inputJSON.get(CommonConstants.FIRSTNAME) != null && !inputJSON.get(CommonConstants.FIRSTNAME).toString().isEmpty() ? inputJSON.get(CommonConstants.FIRSTNAME).toString() : "");
					 		supplierContactDO.setLname(inputJSON.get(CommonConstants.LASTNAME) != null && !inputJSON.get(CommonConstants.LASTNAME).toString().isEmpty() ? inputJSON.get(CommonConstants.LASTNAME).toString() : "");
					 		supplierContactDO.setMname(inputJSON.get(CommonConstants.MIDDLENAME) != null && !inputJSON.get(CommonConstants.MIDDLENAME).toString().isEmpty() ? inputJSON.get(CommonConstants.MIDDLENAME).toString() : "");
					 		supplierContactDO.setDesignation((inputJSON.get(CommonConstants.DESIGNATION) != null && !inputJSON.get(CommonConstants.DESIGNATION).toString().isEmpty() ? inputJSON.get(CommonConstants.DESIGNATION).toString() : ""));
					 		
					 		supplierContactDO.setEmail((inputJSON.get(CommonConstants.EMAIL) != null && !inputJSON.get(CommonConstants.EMAIL).toString().isEmpty() ? inputJSON.get(CommonConstants.EMAIL).toString() :""));
					 		if(inputJSON.get(CommonConstants.MOBILE) != null && !inputJSON.get(CommonConstants.MOBILE).toString().isEmpty()){
					 			supplierContactDO.setMobile(inputJSON.get(CommonConstants.MOBILE).toString());
					 		}
					 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			supplierContactDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			supplierContactDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 		}
					 		supplierContactDO.setUpdatedon(new Date());
					 		supplierContactDO.setCreatedon(new Date());
					 		contactService.persist(supplierContactDO);
					 	}else{
					 		return CommonWebUtil.buildErrorResponse("").toString();
					 	}
			 		}
			 	}
			 	CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Supplier Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponseId(supplierDetails.getSupplierId().toString()).toString();
	}
	
	
	@RequestMapping(value = "/retrieveById/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveById(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<SuppliersDO> supplierList = supplierService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = SupplierUtil.getSupplierList(supplierList).toString();
				}
				
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
		
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<SuppliersDO> leadList = new ArrayList<SuppliersDO>();
				leadList = supplierService.retrieveAll();
				respJSON = SupplierUtil.getSupplierList(leadList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		SuppliersDO supplierDO = new SuppliersDO();
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<SuppliersDO> LeadList = supplierService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		supplierDO = LeadList.get(0);
			 		if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
			 			supplierDO.setName(inputJSON.get(CommonConstants.NAME).toString().toUpperCase());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.REGISTERED_ADDRESS) != null && !inputJSON.get(CommonConstants.REGISTERED_ADDRESS).toString().isEmpty()){
			 			supplierDO.setRegisteredAddr(inputJSON.get(CommonConstants.REGISTERED_ADDRESS).toString().toUpperCase());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.COMMUNICATION_ADDRESS) != null && !inputJSON.get(CommonConstants.COMMUNICATION_ADDRESS).toString().isEmpty()){
			 			supplierDO.setCommunicationAddr(inputJSON.get(CommonConstants.COMMUNICATION_ADDRESS).toString());
			 		}
			 		
			 		
			 		if(inputJSON.get(CommonConstants.ENTITY_OF_STATUS) != null && !inputJSON.get(CommonConstants.ENTITY_OF_STATUS).toString().isEmpty()){
			 			supplierDO.setEntityStatus(inputJSON.get(CommonConstants.ENTITY_OF_STATUS).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.NAME_OF_DIRECTORS) != null && !inputJSON.get(CommonConstants.NAME_OF_DIRECTORS).toString().isEmpty()){
			 			supplierDO.setNameOfDirectors(inputJSON.get(CommonConstants.NAME_OF_DIRECTORS).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.PANNO) != null && !inputJSON.get(CommonConstants.PANNO).toString().isEmpty()){
			 			supplierDO.setPan(inputJSON.get(CommonConstants.PANNO).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.GST_REG_NO) != null && !inputJSON.get(CommonConstants.GST_REG_NO).toString().isEmpty()){
			 			supplierDO.setGstRegNo(inputJSON.get(CommonConstants.GST_REG_NO).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.CIN) != null && !inputJSON.get(CommonConstants.CIN).toString().isEmpty()){
			 			supplierDO.setCin(inputJSON.get(CommonConstants.CIN).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.CST_REG_NO) != null && !inputJSON.get(CommonConstants.CST_REG_NO).toString().isEmpty()){
			 			supplierDO.setCstRegNo(inputJSON.get(CommonConstants.CST_REG_NO).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.CENTRAL_EXCISE_REG_NO) != null && !inputJSON.get(CommonConstants.CENTRAL_EXCISE_REG_NO).toString().isEmpty()){
			 			supplierDO.setCentralExciseRegNo(inputJSON.get(CommonConstants.CENTRAL_EXCISE_REG_NO).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.ESI_REG_NO) != null && !inputJSON.get(CommonConstants.ESI_REG_NO).toString().isEmpty()){
			 			supplierDO.setEsiRegNo(inputJSON.get(CommonConstants.ESI_REG_NO).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.PF_REG_NO) != null && !inputJSON.get(CommonConstants.PF_REG_NO).toString().isEmpty()){
			 			supplierDO.setPfRegNo(inputJSON.get(CommonConstants.PF_REG_NO).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.BANKNAME) != null && !inputJSON.get(CommonConstants.BANKNAME).toString().isEmpty()){
			 			supplierDO.setBankName(inputJSON.get(CommonConstants.BANKNAME).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.BRANCHNAME) != null && !inputJSON.get(CommonConstants.BRANCHNAME).toString().isEmpty()){
			 			supplierDO.setBankBranch(inputJSON.get(CommonConstants.BRANCHNAME).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.TYPE_OF_ACCOUNT) != null && !inputJSON.get(CommonConstants.TYPE_OF_ACCOUNT).toString().isEmpty()){
			 			supplierDO.setTypeOfAccount(inputJSON.get(CommonConstants.TYPE_OF_ACCOUNT).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.ACCOUNTNUMBER) != null && !inputJSON.get(CommonConstants.ACCOUNTNUMBER).toString().isEmpty()){
			 			supplierDO.setAccountNo(Long.parseLong(inputJSON.get(CommonConstants.ACCOUNTNUMBER).toString()));
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.IFSCCODE) != null && !inputJSON.get(CommonConstants.IFSCCODE).toString().isEmpty()){
			 			supplierDO.setIfsCode(inputJSON.get(CommonConstants.IFSCCODE).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.LIST_OF_PRODUCTS) != null && !inputJSON.get(CommonConstants.LIST_OF_PRODUCTS).toString().isEmpty()){
			 			supplierDO.setListOfProducts(inputJSON.get(CommonConstants.LIST_OF_PRODUCTS).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.SPECIALIZED_IN) != null && !inputJSON.get(CommonConstants.SPECIALIZED_IN).toString().isEmpty()){
			 			supplierDO.setSpecializedIn(inputJSON.get(CommonConstants.SPECIALIZED_IN).toString());
			 		}
			 		
			 		supplierDO.setCreatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			supplierDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			supplierDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		supplierDO.setUpdatedon(new Date());
			 		supplierService.update(supplierDO);
			 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Supplier Updated");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponseId(supplierDO.getSupplierId().toString()).toString();
	}
	
	
	@RequestMapping(value = "/importLeads/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String importEmp(@PathVariable String inputParams,  Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY);
				Object type =  inputJSON.get(CommonConstants.TYPE); 
				org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
				
				JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
				List<SuppliersDO> leadDOlist = new ArrayList<SuppliersDO>();
				for (int i=0; i < fileData.size(); i++){
					SuppliersDO leadDO = new SuppliersDO();
					JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
					/*if(colName.get(CommonConstants.NAME) != null && !colName.get(CommonConstants.NAME).toString().isEmpty()){
						leadDO.setName(rowJSON.get(colName.get(CommonConstants.NAME))!=null && !rowJSON.get(colName.get(CommonConstants.NAME)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.NAME)).toString().toUpperCase().trim() : "");
					}
					if(colName.get(CommonConstants.STATUS) != null && !colName.get(CommonConstants.STATUS).toString().isEmpty()){
						if(rowJSON.get(colName.get(CommonConstants.STATUS))!=null && !rowJSON.get(colName.get(CommonConstants.STATUS)).toString().isEmpty()){
							List<LeadStatusDO> leadStatusList = leadStatusService.retrieveByActiveName(rowJSON.get(colName.get(CommonConstants.STATUS)).toString());
							if(leadStatusList != null && leadStatusList.size() > 0){
								 leadDO.setLeadStatus(leadStatusList.get(0));
							}
				 		}
					}
					if(colName.get(CommonConstants.STATE) != null && !colName.get(CommonConstants.STATE).toString().isEmpty()){
						if(rowJSON.get(colName.get(CommonConstants.STATE))!=null && !rowJSON.get(colName.get(CommonConstants.STATE)).toString().isEmpty()){
							List<StateDO> StateList = stateService.retrieveByActiveName(rowJSON.get(colName.get(CommonConstants.STATE)).toString());
							if(StateList != null && StateList.size() > 0){
								leadDO.setState(StateList.get(0));
							}
				 		}
					}
					if(colName.get(CommonConstants.LEADTYPE) != null && !colName.get(CommonConstants.LEADTYPE).toString().isEmpty()){
						if(rowJSON.get(colName.get(CommonConstants.LEADTYPE))!=null && !rowJSON.get(colName.get(CommonConstants.LEADTYPE)).toString().isEmpty()){
							List<LeadTypeDO> leadTypeList = leadTypeService.retrieveByActvieName(rowJSON.get(colName.get(CommonConstants.LEADTYPE)).toString());
							if(leadTypeList != null && leadTypeList.size() > 0){
								leadDO.setLeadType(leadTypeList.get(0));
							}
				 		}
					}
					if(colName.get(CommonConstants.INDUSTRY) != null && !colName.get(CommonConstants.INDUSTRY).toString().isEmpty()){
						if(rowJSON.get(colName.get(CommonConstants.INDUSTRY))!=null && !rowJSON.get(colName.get(CommonConstants.INDUSTRY)).toString().isEmpty()){
							List<IndustryDO> industryList = industryService.retrieveByActiveName(rowJSON.get(colName.get(CommonConstants.INDUSTRY)).toString());
							if(industryList != null && industryList.size() > 0){
								leadDO.setIndustry(industryList.get(0));
							}
				 		}
					}
					if(colName.get(CommonConstants.CITY) != null && !colName.get(CommonConstants.CITY).toString().isEmpty()){
				 		if(rowJSON.get(colName.get(CommonConstants.CITY))!=null && !rowJSON.get(colName.get(CommonConstants.CITY)).toString().isEmpty()){
				 			leadDO.setCity(rowJSON != null ? rowJSON.get(colName.get(CommonConstants.CITY)).toString() : null);
				 		}
					}
					if(colName.get(CommonConstants.FOLLOW_UP_DATE) != null && !colName.get(CommonConstants.FOLLOW_UP_DATE).toString().isEmpty()){
				 		if(rowJSON.get(colName.get(CommonConstants.FOLLOW_UP_DATE))!=null && !rowJSON.get(colName.get(CommonConstants.FOLLOW_UP_DATE)).toString().isEmpty()){
				 			leadDO.setFollowupdate(CommonUtil.convertStringToSqlDate(rowJSON != null ? rowJSON.get(colName.get(CommonConstants.FOLLOW_UP_DATE)).toString() : null));
				 		}
					}
					if(colName.get(CommonConstants.LEAD_DATE) != null && !colName.get(CommonConstants.LEAD_DATE).toString().isEmpty()){
				 		if(rowJSON.get(colName.get(CommonConstants.LEAD_DATE))!=null && !rowJSON.get(colName.get(CommonConstants.LEAD_DATE)).toString().isEmpty()){
				 			leadDO.setLeaddate(CommonUtil.convertStringToSqlDate(rowJSON != null ? rowJSON.get(colName.get(CommonConstants.LEAD_DATE)).toString() : null));
				 		}
					}
					if(colName.get(CommonConstants.SOURCE) != null && !colName.get(CommonConstants.SOURCE).toString().isEmpty()){
						leadDO.setSource(rowJSON.get(colName.get(CommonConstants.SOURCE))!=null && !rowJSON.get(colName.get(CommonConstants.SOURCE)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.SOURCE)).toString() : null);
					}
					if(colName.get(CommonConstants.MOBILE) != null && !colName.get(CommonConstants.MOBILE).toString().isEmpty()){
				 		if(rowJSON.get(colName.get(CommonConstants.MOBILE))!=null && !rowJSON.get(colName.get(CommonConstants.MOBILE)).toString().isEmpty()){
				 			leadDO.setMobile(rowJSON != null  ? rowJSON.get(colName.get(CommonConstants.MOBILE)).toString() : "");
				 		}
					}
					if(colName.get(CommonConstants.PHONE) != null && !colName.get(CommonConstants.PHONE).toString().isEmpty()){
				 		if(rowJSON.get(colName.get(CommonConstants.PHONE))!=null && !rowJSON.get(colName.get(CommonConstants.PHONE)).toString().isEmpty()){
				 			leadDO.setPhone(rowJSON != null ? rowJSON.get(colName.get(CommonConstants.PHONE)).toString() : "");
				 		}
					}
					if(colName.get(CommonConstants.EMAIL) != null && !colName.get(CommonConstants.EMAIL).toString().isEmpty()){
						leadDO.setEmail(rowJSON.get(colName.get(CommonConstants.EMAIL))!=null && !rowJSON.get(colName.get(CommonConstants.EMAIL)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.EMAIL)).toString() : null);
					}
					if(colName.get(CommonConstants.ADDRESS) != null && !colName.get(CommonConstants.ADDRESS).toString().isEmpty()){
						leadDO.setAddress(rowJSON.get(colName.get(CommonConstants.ADDRESS))!=null && !rowJSON.get(colName.get(CommonConstants.ADDRESS)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.ADDRESS)).toString() : null);
					}
					if(colName.get(CommonConstants.COMMENT) != null && !colName.get(CommonConstants.COMMENT).toString().isEmpty()){
						leadDO.setComment(rowJSON.get(colName.get(CommonConstants.COMMENT))!=null && !rowJSON.get(colName.get(CommonConstants.COMMENT)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.COMMENT)).toString() : null);
					}
					if(colName.get(CommonConstants.LEADTYPE) != null){
				 		if(rowJSON.get(colName.get(CommonConstants.LEADTYPE)) != null && !rowJSON.get(colName.get(CommonConstants.LEADTYPE)).toString().isEmpty()){
				 			if(leadTypeList != null && leadTypeList.size() > 0){
								for(LeadTypeDO leadTypeDO : leadTypeList){
									if(leadTypeDO.getName().toLowerCase().equalsIgnoreCase(rowJSON.get(colName.get(CommonConstants.LEADTYPE)).toString().toLowerCase())){
										leadDO.setLeadType(leadTypeDO.getId());
									}
								}
							} else{
								leadDO.setLeadType(Long.parseLong(rowJSON != null ? rowJSON.get(colName.get(CommonConstants.LEADTYPE)).toString() : ""));
							}
				 			//leadDO.setLeadType(Long.parseLong(rowJSON != null ? rowJSON.get(colName.get(CommonConstants.LEADTYPE)).toString() : ""));
				 		}
					}
			 		leadDO.setType(!type.toString().isEmpty() ? type.toString() : null);
			 		leadDO.setCreatedon(new Date());
			 		if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
			 			leadDO.setUpdatedby(updatedBy.toString());
			 			leadDO.setEmpId((updatedBy.toString()));
			 		}
			 		leadDO.setUpdatedon(new Date());
			 		leadDO.setIsDeleted("n");
			 		leadDOlist.add(leadDO);
			 		supplierService.persist(leadDO);
			 		
			 		String fname = null;
			 		String lname =  null;
			 		String mname =  null;*/
			 		if(leadDO.getSupplierId() != null){
				 		ContactDO contactDO = new ContactDO();
				 		/*NotesDO notesDO = new NotesDO();
				 		if(colName.get(CommonConstants.NOTES) != null && !colName.get(CommonConstants.NOTES).toString().isEmpty()){
				 			if(rowJSON.get(colName.get(CommonConstants.NOTES)) != null && !rowJSON.get(colName.get(CommonConstants.NOTES)).toString().isEmpty()){
				 				notesDO.setNote(rowJSON.get(colName.get(CommonConstants.NOTES)).toString());
				 				notesDO.set(leadDO);
				 				if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
					 				notesDO.setUpdatedBy(updatedBy.toString());
					 				notesDO.setEmpId(Long.parseLong(updatedBy.toString()));
						 		}
						 		notesDO.setUpdatedon(new Date());
						 		notesService.persist(notesDO);
					 		}
				 		}*/
				 		/*if(colName.get(CommonConstants.CONTACTNAME) != null && !colName.get(CommonConstants.CONTACTNAME).toString().isEmpty()){
				 			if(rowJSON.get(colName.get(CommonConstants.CONTACTNAME))!=null && !rowJSON.get(colName.get(CommonConstants.CONTACTNAME)).toString().isEmpty()){
					 			if(rowJSON.get(colName.get(CommonConstants.CONTACTNAME)).toString().split(" ").length > 2){
					 				fname = rowJSON.get(colName.get(CommonConstants.CONTACTNAME))!=null && !rowJSON.get(colName.get(CommonConstants.CONTACTNAME)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.CONTACTNAME)).toString().split(" ")[0] : null;
						 			lname = rowJSON.get(colName.get(CommonConstants.CONTACTNAME))!=null && !rowJSON.get(colName.get(CommonConstants.CONTACTNAME)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.CONTACTNAME)).toString().split(" ")[1] : null;
						 			mname = rowJSON.get(colName.get(CommonConstants.CONTACTNAME))!=null && !rowJSON.get(colName.get(CommonConstants.CONTACTNAME)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.CONTACTNAME)).toString().split(" ")[2] : null;
						 			contactDO.setFirstname(fname);
						 			contactDO.setLastname(lname);
						 			contactDO.setMiddlename(mname);
					 			} else if(rowJSON.get(colName.get(CommonConstants.CONTACTNAME)).toString().split(" ").length > 1){
					 				fname = rowJSON.get(colName.get(CommonConstants.CONTACTNAME))!=null && !rowJSON.get(colName.get(CommonConstants.CONTACTNAME)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.CONTACTNAME)).toString().split(" ")[0] : null;
						 			lname = rowJSON.get(colName.get(CommonConstants.CONTACTNAME))!=null && !rowJSON.get(colName.get(CommonConstants.CONTACTNAME)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.CONTACTNAME)).toString().split(" ")[1] : null;
						 			contactDO.setFirstname(fname);
						 			contactDO.setLastname(lname);
					 			} else if(rowJSON.get(colName.get(CommonConstants.CONTACTNAME)).toString().split(" ").length == 1) {
					 				fname = rowJSON.get(colName.get(CommonConstants.CONTACTNAME))!=null && !rowJSON.get(colName.get(CommonConstants.CONTACTNAME)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.CONTACTNAME)).toString().split(" ")[0] : null;
					 				contactDO.setFirstname(fname);
					 			}
					 			//contactDO.setLeadtype(leadDO.getSupplierId());
					 			List<LeadDO> leadList = leadService.retrieveById(leadDO.getSupplierId());
					 			if(leadList != null && leadList.size() > 0){
					 				contactDO.setLead(leadList.get(0));
					 			}
					 			contactDO.setUpdatedon(new Date());
						 		contactDO.setIsDeleted("n");
				 			
					 			if(colName.get(CommonConstants.MOBILE1) != null && !colName.get(CommonConstants.MOBILE1).toString().isEmpty()){
					 				if(rowJSON.get(colName.get(CommonConstants.MOBILE1))!=null && !rowJSON.get(colName.get(CommonConstants.MOBILE1)).toString().isEmpty()){
					 					contactDO.setMobile1(rowJSON.get(colName.get(CommonConstants.MOBILE1)).toString());
					 				}
					 			}	
					 			if(colName.get(CommonConstants.PRIMARYEMAIL) != null && !colName.get(CommonConstants.PRIMARYEMAIL).toString().isEmpty()){
					 				if(rowJSON.get(colName.get(CommonConstants.PRIMARYEMAIL))!=null && !rowJSON.get(colName.get(CommonConstants.PRIMARYEMAIL)).toString().isEmpty()){
					 					contactDO.setPrimaryemail(rowJSON.get(colName.get(CommonConstants.PRIMARYEMAIL)).toString());
					 				}
					 			}
						 		if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
						 			contactDO.setUpdatedby(updatedBy.toString());
						 			contactDO.setEmpId((updatedBy.toString()));
						 		}
						 		contactService.persist(contactDO);
				 			}
				 		}*/
			 		}
				}
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Supplier Created");
				//leadService.persistList(leadDOlist);
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}	
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}

	
}
