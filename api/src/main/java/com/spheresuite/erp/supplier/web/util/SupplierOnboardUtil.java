package com.spheresuite.erp.supplier.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.SupplierOnboardDO;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
@Component
public class SupplierOnboardUtil {
	
	private SupplierOnboardUtil() {}
	
	public static JSONObject getSupplierOnboardList(List<SupplierOnboardDO> supplierOnboardList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (SupplierOnboardDO supplier : supplierOnboardList) {
				resultJSONArray.put(getSupplierOnboardDetailObject(supplier));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getSupplierOnboardDetailObject(SupplierOnboardDO supplier)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(supplier.getSupplierOnboardId() ));
		result.put(CommonConstants.NAME, String.valueOf(supplier.getName()));
		if(supplier.getRegisteredAddr() !=null){
			result.put(CommonConstants.REGISTERED_ADDRESS, String.valueOf(supplier.getRegisteredAddr()));
		}else{
			result.put(CommonConstants.REGISTERED_ADDRESS, "");
		}
		
		if(supplier.getCommunicationAddr() !=null){
			result.put(CommonConstants.COMMUNICATION_ADDRESS, String.valueOf(supplier.getCommunicationAddr()));
		}else{
			result.put(CommonConstants.COMMUNICATION_ADDRESS, "");
		}
		
		if(supplier.getEntityStatus() !=null){
			result.put(CommonConstants.ENTITY_OF_STATUS, String.valueOf(supplier.getEntityStatus()));
		}else{
			result.put(CommonConstants.ENTITY_OF_STATUS, "");
		}
		
		if(supplier.getNameOfDirectors() !=null){
			result.put(CommonConstants.NAME_OF_DIRECTORS, String.valueOf(supplier.getNameOfDirectors()));
		}else{
			result.put(CommonConstants.NAME_OF_DIRECTORS, "");
		}
		if(supplier.getPan() !=null){
			result.put(CommonConstants.PANNO, String.valueOf(supplier.getPan()));
		}else{
			result.put(CommonConstants.PANNO, "");
		}
		if(supplier.getGstRegNo() !=null){
			result.put(CommonConstants.GST_REG_NO, String.valueOf(supplier.getGstRegNo()));
		}else{
			result.put(CommonConstants.GST_REG_NO, "");
		}
		if(supplier.getCin() !=null){
			result.put(CommonConstants.CIN, String.valueOf(supplier.getCin()));
		}else{
			result.put(CommonConstants.CIN, "");
		}
		if(supplier.getCstRegNo() !=null){
			result.put(CommonConstants.CST_REG_NO, String.valueOf(supplier.getCstRegNo()));
		}else{
			result.put(CommonConstants.CST_REG_NO, "");
		}
		
		if(supplier.getCentralExciseRegNo() !=null){
			result.put(CommonConstants.CENTRAL_EXCISE_REG_NO, String.valueOf(supplier.getCentralExciseRegNo()));
		}else{
			result.put(CommonConstants.CENTRAL_EXCISE_REG_NO, "");
		}
		if(supplier.getEsiRegNo() != null){
			result.put(CommonConstants.ESI_REG_NO, String.valueOf(supplier.getEsiRegNo()));
		}else{
			result.put(CommonConstants.ESI_REG_NO, "");
		}
		
		if(supplier.getPfRegNo() != null){
			result.put(CommonConstants.PF_REG_NO, String.valueOf(supplier.getPfRegNo()));
		}else{
			result.put(CommonConstants.PF_REG_NO, "");
		}
		
		if(supplier.getBankName() != null){
			result.put(CommonConstants.BANKNAME, String.valueOf(supplier.getBankName()));
		}else{
			result.put(CommonConstants.BANKNAME, "");
		}
		
		if(supplier.getBankBranch() != null){
			result.put(CommonConstants.BRANCHNAME, String.valueOf(supplier.getBankBranch()));
		}else{
			result.put(CommonConstants.BRANCHNAME, "");
		}
		
		if(supplier.getAccountNo() != null){
			result.put(CommonConstants.ACCOUNTNUMBER, String.valueOf(supplier.getAccountNo()));
		}else{
			result.put(CommonConstants.ACCOUNTNUMBER, "");
		}
		
		if(supplier.getIfsCode() != null){
			result.put(CommonConstants.IFSCCODE, String.valueOf(supplier.getIfsCode()));
		}else{
			result.put(CommonConstants.IFSCCODE, "");
		}
		
		if(supplier.getTypeOfAccount() != null){
			result.put(CommonConstants.TYPE_OF_ACCOUNT, String.valueOf(supplier.getTypeOfAccount()));
		}else{
			result.put(CommonConstants.TYPE_OF_ACCOUNT, "");
		}
		
		if(supplier.getListOfProducts() != null){
			result.put(CommonConstants.LIST_OF_PRODUCTS, String.valueOf(supplier.getListOfProducts()));
		}else{
			result.put(CommonConstants.LIST_OF_PRODUCTS, "");
		}
		
		if(supplier.getSpecializedIn() != null){
			result.put(CommonConstants.SPECIALIZED_IN, String.valueOf(supplier.getSpecializedIn()));
		}else{
			result.put(CommonConstants.SPECIALIZED_IN, "");
		}
		
		String name="";
		if(supplier.getSupplierContactId() !=null){
			result.put(CommonConstants.SUPPLIERCONTACT, String.valueOf(supplier.getSupplierContactId()));
		}else{
			result.put(CommonConstants.SUPPLIERCONTACT, "");
		}
		if(supplier.getFname() !=null){
			result.put(CommonConstants.FIRSTNAME, String.valueOf(supplier.getFname()));
			name = name+" "+supplier.getFname();
		}else{
			result.put(CommonConstants.FIRSTNAME, "");
		}
		if(supplier.getMname() !=null){
			result.put(CommonConstants.MIDDLENAME, String.valueOf(supplier.getMname()));
			name = name+" "+supplier.getMname();
		}else{
			result.put(CommonConstants.MIDDLENAME, "");
		}
		if(supplier.getLname() !=null){
			result.put(CommonConstants.LASTNAME, String.valueOf(supplier.getLname()));
			name = name+" "+supplier.getLname();
		}else{
			result.put(CommonConstants.LASTNAME, "");
		}
		result.put(CommonConstants.CONTACTNAME, name);
		
		if(supplier.getEmail() !=null){
			result.put(CommonConstants.EMAIL, String.valueOf(supplier.getEmail()));
		}else{
			result.put(CommonConstants.EMAIL, "");
		}
		
		if(supplier.getDesignation() !=null){
			result.put(CommonConstants.DESIGNATION, String.valueOf(supplier.getDesignation()));
		}else{
			result.put(CommonConstants.DESIGNATION, "");
		}
		
		if(supplier.getMobile() !=null){
			result.put(CommonConstants.MOBILE, String.valueOf(supplier.getMobile()));
		}else{
			result.put(CommonConstants.MOBILE, "");
		}
		
		if(supplier.getUpdatedon() != null) result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(supplier.getUpdatedon())));

		if(supplier.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(supplier.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}	
		result.put(CommonConstants.STATUS,String.valueOf(supplier.getStatus()));
		return result;
	}
	
}
