package com.spheresuite.erp.supplier.web.util;

import java.util.List;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.SuppliersContactDO;
import com.spheresuite.erp.domainobject.SuppliersDO;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.supplier.service.SupplierService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
@Component
public class SupplierContactUtil {
	
	private SupplierContactUtil() {}
	
	@Autowired
	private SupplierService tsupplierService;
	
	@SuppressWarnings("unused")
	private static SupplierService supplierService;
	
	@Autowired
	private EmployeeService temployeeService;
	
	private static EmployeeService employeeService;
	
	@PostConstruct
	public void init() {
		supplierService = tsupplierService;
		employeeService = temployeeService;
	}
	
	
	
	public static JSONObject getContactList(List<SuppliersContactDO> contactList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (SuppliersContactDO contact : contactList) {
				resultJSONArray.put(getContactDetailObject(contact));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getContactDetailObject(SuppliersContactDO contact)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(contact.getSupplierContactId()));
		result.put(CommonConstants.SUPPLIERCONTACT, String.valueOf(contact.getSupplierContactId()));
		if(contact.getSuppliers() !=null){
			result.put(CommonConstants.SUPPLIERID, String.valueOf(contact.getSuppliers().getSupplierId()));
		}else{
			result.put(CommonConstants.SUPPLIERID, "");
		}
		String name="";
		if(contact.getFname() !=null){
			result.put(CommonConstants.FIRSTNAME, String.valueOf(contact.getFname()));
			name = name+" "+contact.getFname();
		}else{
			result.put(CommonConstants.FIRSTNAME, "");
		}
		if(contact.getMname() !=null){
			result.put(CommonConstants.MIDDLENAME, String.valueOf(contact.getMname()));
			name = name+" "+contact.getMname();
		}else{
			result.put(CommonConstants.MIDDLENAME, "");
		}
		if(contact.getLname() !=null){
			result.put(CommonConstants.LASTNAME, String.valueOf(contact.getLname()));
			name = name+" "+contact.getLname();
		}else{
			result.put(CommonConstants.LASTNAME, "");
		}
		result.put(CommonConstants.NAME, name);
		
		if(contact.getEmail() !=null){
			result.put(CommonConstants.EMAIL, String.valueOf(contact.getEmail()));
		}else{
			result.put(CommonConstants.EMAIL, "");
		}
		
		if(contact.getDesignation() !=null){
			result.put(CommonConstants.DESIGNATION, String.valueOf(contact.getDesignation()));
		}else{
			result.put(CommonConstants.DESIGNATION, "");
		}
		
		if(contact.getMobile() !=null){
			result.put(CommonConstants.MOBILE, String.valueOf(contact.getMobile()));
		}else{
			result.put(CommonConstants.MOBILE, "");
		}
		
				
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(contact.getUpdatedon())));
		/*if(contact.getUpdatedby() != null){
			
			List<Supplier_SuppliersDO> supplierList = supplierService.retrieveById(Long.parseLong(contact.getUpdatedby().toString()));
			if(supplierList != null && supplierList.size() > 0){
				if(supplierList.get(0).getName() != null)
					result.put(CommonConstants.UPDATED_BY, String.valueOf(supplierList.get(0).getName()));
				else result.put(CommonConstants.UPDATED_BY, "");
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
			
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}*/	
		if(contact.getUpdatedby() != null){
			
			List<EmployeeDO> empList = employeeService.retrieveEmpId(contact.getUpdatedby().toString());
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
			
			/*List<UserDO> userList = userService.retriveById(Long.parseLong(contact.getUpdatedby()));
			if(userList != null && userList.size() > 0){
				result.put(CommonConstants.UPDATED_BY, String.valueOf(userList.get(0).getName())); 
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}*/
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		if(contact.getSuppliers() !=null){
				result.put(CommonConstants.SUPPLIERNAME, String.valueOf(contact.getSuppliers().getName()));
		}else{
			result.put(CommonConstants.SUPPLIERNAME, "");
		}
		
		return result;
	}
}
