package com.spheresuite.erp.supplier.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.domainobject.SuppliersContactDO;
import com.spheresuite.erp.supplier.dao.SuppliersContactDAO;
@Service
@Transactional
public class SuppliersContactService {
	static Logger logger = Logger.getLogger(SuppliersContactService.class.getName());
	
	@Autowired
	private SuppliersContactDAO contactDAO;
	
	@Transactional
	public SuppliersContactDO persist(SuppliersContactDO contactDO)  {
		return contactDAO.persist(contactDO);
	}

	@Transactional
	public List<SuppliersContactDO> retriveById(Long string)  {
		return contactDAO.retrieveById(string);
	}
	
	@Transactional
	public List<SuppliersContactDO> retrieveByEmail(String email)  {
		return contactDAO.retrieveByEmail(email);
	}

	@Transactional
	public List<SuppliersContactDO> retriveByLeadId(Long id)  {
		return contactDAO.retrieveByLeadId(id);
	}

	/*@Transactional
	public List<Supplier_SuppliersContactDO> retrieve(List<String> ids)  {
		return contactDAO.retrieve(ids);
	}*/

	@Transactional
	public List<SuppliersContactDO> retrieveAll()  {
		return contactDAO.retrieveAll();
	}

	@Transactional
	public SuppliersContactDO update(SuppliersContactDO contactDO)  {
		return contactDAO.update(contactDO);
	}

	@Transactional
	public boolean update(List<SuppliersContactDO> contactDO)  {
		return contactDAO.update(contactDO);
	}

	@Transactional
	public boolean persistList(List<SuppliersContactDO> contactDO)  {
		return contactDAO.persistList(contactDO);
	}
}
