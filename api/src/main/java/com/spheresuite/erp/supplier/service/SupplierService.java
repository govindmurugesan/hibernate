package com.spheresuite.erp.supplier.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.domainobject.SuppliersDO;
import com.spheresuite.erp.supplier.dao.SupplierDAO;
@Service
@Transactional
public class SupplierService {
	static Logger logger = Logger.getLogger(SupplierService.class.getName());
	@Autowired
	private SupplierDAO supplierdao;
	
	@Transactional
	public SuppliersDO persist(SuppliersDO leadDO) {
		return supplierdao.persist(leadDO);
	}

	@Transactional
	public List<SuppliersDO> retrieveById(Long Id) {
		return supplierdao.retrieveById(Id);
	}

	/*@Transactional
	public List<Supplier_SuppliersDO> retrieveBetweenDate(Date fromDate, Date toDate) {
		return supplierdao.retrieveBetweenDate(fromDate, toDate);
	}

	@Transactional
	public List<Supplier_SuppliersDO> retrieveBetweenDateByEmp(Date fromDate, Date toDate, String empId) {
		return supplierdao.retrieveBetweenDateByEmp(fromDate, toDate, empId);
	}

	@Transactional
	public List<Supplier_SuppliersDO> retrieve(String type, List<Long> id) {
		return supplierdao.retrieve(type, id);
	}*/

	@Transactional
	public boolean update(List<SuppliersDO> leadList) {
		return supplierdao.update(leadList);
	}

	@Transactional
	public List<SuppliersDO> retrieveAll() {
		return supplierdao.retrieveAll();
	}

	/*@Transactional
	public Long retrieveAllForChart(String type) {
		return supplierdao.retrieveAllForChart(type);
	}

	@Transactional
	public List<Supplier_SuppliersDO> retrieve(List<Long> id) {
		return supplierdao.retrieve(id);
	}

	@Transactional
	public List<Supplier_SuppliersDO> retrieveAll() {
		return supplierdao.retrieveAll();
	}

	@Transactional
	public List<Long> retrieveForCustomerIds() {
		return supplierdao.retrieveForCustomerIds();
	}*/

	@Transactional
	public SuppliersDO update(SuppliersDO leadDO) {
		return supplierdao.update(leadDO);
	}

	/*@Transactional
	public List<Supplier_SuppliersDO> retrieveCustomerBetweenDate(Date fromDate, Date toDate) {
		return supplierdao.retrieveCustomerBetweenDate(fromDate, toDate);
	}

	@Transactional
	public List<Supplier_SuppliersDO> retrieveCustomerBetweenDateByEmp(Date fromDate, Date toDate, String empId) {
		return supplierdao.retrieveCustomerBetweenDateByEmp(fromDate, toDate, empId);
	}*/
	

	@Transactional
	public boolean persistList(List<SuppliersDO> leadDO) {
		return supplierdao.persistList(leadDO);
	}

	/*@Transactional
	public List<Supplier_SuppliersDO> retrieveLeadByEmp(Long empId, String type) {
		return supplierdao.retrieveLeadByEmp(empId, type);
	}
	
	@Transactional
	public List<Supplier_SuppliersDO> retrieveLeadByPermenantEmp(Long empId, String type) {
		return supplierdao.retrieveLeadByPermenantEmp(empId, type);
	}

	@Transactional
	public List<Supplier_SuppliersDO> retrieveLeadByTransferType(String transferType, String type) {
		return supplierdao.retrieveLeadByTransferType(transferType, type);
	}

	@Transactional
	public List<Supplier_SuppliersDO> retrieveLeadByAllTransferType(String type) {
		return supplierdao.retrieveLeadByAllTransferType(type);
	}

	@Transactional
	public List<Supplier_SuppliersDO> retrieveTransferLeadById(String transferType, String type, Long empID) {
		return supplierdao.retrieveTransferLeadById(transferType, type, empID);
	}

	@Transactional
	public List<Supplier_SuppliersDO> retrieveByEmp(String type, Long empId) {
		return supplierdao.retrieveByEmp(type, empId);
	}*/

}
