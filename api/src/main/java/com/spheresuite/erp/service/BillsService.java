package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.BillsDAO;
import com.spheresuite.erp.domainobject.BillsDO;

@Service
@Transactional
public class BillsService {
	static Logger logger = Logger.getLogger(BillsService.class.getName());
	
	@Autowired
	private BillsDAO billsDAO;
	
	@Transactional
	public BillsDO persist(BillsDO billsDO)  {
		return billsDAO.persist(billsDO);
	}
	@Transactional
	public List<BillsDO> retriveById(Long id)  {
		return billsDAO.retrieveById(id);
	}
	@Transactional
	public List<BillsDO> retrieve()  {
		return billsDAO.retrieve();
	}
	@Transactional
	public List<BillsDO> retrieveBillNumber()  {
		return billsDAO.retrieveBillNumber();
	}
	@Transactional
	public BillsDO update(BillsDO billsDO)  {
		return billsDAO.update(billsDO);
	}
	@Transactional
	public boolean persistList(List<BillsDO> billsDO)  {
		return billsDAO.persistList(billsDO);
	}
}
