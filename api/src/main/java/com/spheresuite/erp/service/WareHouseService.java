package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.WareHouseDAO;
import com.spheresuite.erp.domainobject.WareHouseDO;

@Service
@Transactional
public class WareHouseService {
	static Logger logger = Logger.getLogger(WareHouseService.class.getName());
	
	@Autowired
	private WareHouseDAO wareHouseDAO;
	
	@Transactional
	public WareHouseDO persist(WareHouseDO wareHouseDO) {
		return wareHouseDAO.persist(wareHouseDO);
	}
	
	@Transactional
	public WareHouseDO update(WareHouseDO wareHouseDO) {
		return wareHouseDAO.update(wareHouseDO);
	}

	@Transactional
	public List<WareHouseDO> retrieveById(Long Id) {
		return wareHouseDAO.retrieveById(Id);
	}

	@Transactional
	public List<WareHouseDO> retrieve() {
		return wareHouseDAO.retrieve();
	}
}
