package com.spheresuite.erp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.WorkingDayAllowanceDAO;
import com.spheresuite.erp.domainobject.WorkingDayAllowanceDO;

@Service
@Transactional
public class WorkingDayAllowanceService{
	
	@Autowired
	private WorkingDayAllowanceDAO workingDayAllowanceDAO;

	@Transactional
	public WorkingDayAllowanceDO persist(WorkingDayAllowanceDO workingDayAllowance) {
		return workingDayAllowanceDAO.persist(workingDayAllowance);
	}
	
	@Transactional
	public List<WorkingDayAllowanceDO> retrieve() {
		return workingDayAllowanceDAO.retrieve();
	}

	@Transactional
	public boolean update(WorkingDayAllowanceDO workingDayAllowance) {
		return workingDayAllowanceDAO.update(workingDayAllowance);
	}
	
	@Transactional
	public List<WorkingDayAllowanceDO> retrieveActive(){
		return workingDayAllowanceDAO.retrieveActive();
	}
	
	@Transactional
	public List<WorkingDayAllowanceDO> retrieveById(Long id){
		return workingDayAllowanceDAO.retrieveById(id);
	}
	
}
