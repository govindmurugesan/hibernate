package com.spheresuite.erp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.PayrollGroupDAO;
import com.spheresuite.erp.domainobject.PayrollGroupDO;

@Service
@Transactional
public class PayrollGroupService {

	@Autowired
	private PayrollGroupDAO payrollGroupDAO;
	
	@Transactional
	public boolean persist(PayrollGroupDO payrollGroupDO){
		return payrollGroupDAO.persist(payrollGroupDO);
	}

	@Transactional
	public List<PayrollGroupDO> retrieveActive(){
		return payrollGroupDAO.retrieveActive();
	}
	
	@Transactional
	public List<PayrollGroupDO> retrieveById(Long id){
		return payrollGroupDAO.retrieveById(id);
	}
	
	@Transactional
	public List<PayrollGroupDO> retrieveByName(String name){
		return payrollGroupDAO.retrieveByName(name);
	}
	
	@Transactional
	public List<PayrollGroupDO> retrieve(){
		return payrollGroupDAO.retrieve();
	}
	
	@Transactional
	public boolean persistList(List<PayrollGroupDO> payrollGroupDO) {
		return payrollGroupDAO.persistList(payrollGroupDO);
	}
	
	@Transactional
	public boolean update(PayrollGroupDO payrollGroupDO){
		return payrollGroupDAO.update(payrollGroupDO);
	}
}
