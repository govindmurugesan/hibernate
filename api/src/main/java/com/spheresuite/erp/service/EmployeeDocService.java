package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.EmployeeDocDAO;
import com.spheresuite.erp.domainobject.EmployeeDocDO;
@Service
@Transactional
public class EmployeeDocService {
	static Logger logger = Logger.getLogger(EmployeeDocService.class.getName());
	@Autowired
	private EmployeeDocDAO employeeDocDAO;

	@Transactional
	public EmployeeDocDO persist(EmployeeDocDO employeeDocDO) {
		return employeeDocDAO.persist(employeeDocDO);
	}

	@Transactional
	public List<EmployeeDocDO> retrieveByEmpId(String id) {
		return employeeDocDAO.retrieveByEmpId(id);
	}

	@Transactional
	public boolean delete(Long id) {
		return employeeDocDAO.delete(id);
	}

	@Transactional
	public List<EmployeeDocDO> retrieve() {
		return employeeDocDAO.retrieve();
	}

	@Transactional
	public EmployeeDocDO update(EmployeeDocDO employeeDocDO) {
		return employeeDocDAO.update(employeeDocDO);
	}
}
