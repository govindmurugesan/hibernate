package com.spheresuite.erp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.ExpenseTypeDAO;
import com.spheresuite.erp.domainobject.ExpenseTypeDO;

@Service
@Transactional
public class ExpenseTypeService{
	
	@Autowired
	private ExpenseTypeDAO expenseTypeDAO;

	@Transactional
	public boolean persist(ExpenseTypeDO ExpenseTypeDO) {
		return expenseTypeDAO.persist(ExpenseTypeDO);
	}
	
	@Transactional
	public List<ExpenseTypeDO> retrieve() {
		return expenseTypeDAO.retrieve();
	}

	@Transactional
	public boolean update(ExpenseTypeDO ExpenseTypeDO) {
		return expenseTypeDAO.update(ExpenseTypeDO);
	}
	
	@Transactional
	public List<ExpenseTypeDO> retrieveActive(){
		return expenseTypeDAO.retrieveActive();
	}
	
	@Transactional
	public List<ExpenseTypeDO> retrieveById(Long id){
		return expenseTypeDAO.retrieveById(id);
	}
	
	@Transactional
	public boolean persistList(List<ExpenseTypeDO> ExpenseTypeDO) {
		return expenseTypeDAO.persistList(ExpenseTypeDO);
	}
}
