package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.EmployeeOtherInformationDAO;
import com.spheresuite.erp.domainobject.EmployeeOtherInformationDO;
@Service
@Transactional
public class EmployeeOtherInformationService {
	static Logger logger = Logger.getLogger(EmployeeOtherInformationService.class.getName());
	@Autowired
	private EmployeeOtherInformationDAO employeeOtherInformationDAO;
	
	@Transactional
	public EmployeeOtherInformationDO persist(EmployeeOtherInformationDO employeeOtherInformationDO) {
		return employeeOtherInformationDAO.persist(employeeOtherInformationDO);
	}

	@Transactional
	public List<EmployeeOtherInformationDO> retrieveByEmpId(String empId) {
		return employeeOtherInformationDAO.retrieveByEmpId(empId);
	}

	@Transactional
	public List<EmployeeOtherInformationDO> retrieveById(Long id) {
		return employeeOtherInformationDAO.retrieveById(id);
	}

	@Transactional
	public EmployeeOtherInformationDO update(EmployeeOtherInformationDO employeeOtherInformationDO) {
		return employeeOtherInformationDAO.update(employeeOtherInformationDO);
	}
}
