package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.HrRequestDocDAO;
import com.spheresuite.erp.domainobject.HrRequestDocDO;
@Service
@Transactional
public class HrRequestDocService {
	static Logger logger = Logger.getLogger(HrRequestDocService.class.getName());
	@Autowired
	private HrRequestDocDAO hrRequestDocDAO;
	
	@Transactional
	public HrRequestDocDO persist(HrRequestDocDO hrRequestDoc) {
		return hrRequestDocDAO.persist(hrRequestDoc);
	}

	@Transactional
	public List<HrRequestDocDO> retrieveByOppId(Long id) {
		return hrRequestDocDAO.retrieveByOppId(id);
	}

	@Transactional
	public boolean delete(Long id) {
		return hrRequestDocDAO.delete(id);
	}

	@Transactional
	public List<HrRequestDocDO> retrieve() {
		return hrRequestDocDAO.retrieve();
	}

	@Transactional
	public HrRequestDocDO update(HrRequestDocDO hrRequestDoc) {
		return hrRequestDocDAO.update(hrRequestDoc);
	}
}
