package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.UserDAO;
import com.spheresuite.erp.domainobject.UserDO;

@Service
@Transactional
public class UserService {
	static Logger logger = Logger.getLogger(UserService.class.getName());
	
	@Autowired
	private UserDAO userDAO;
	
	@Transactional
	public Boolean persist(UserDO userDO) {
		return userDAO.persist(userDO);
	}

	@Transactional
	public List<UserDO> retrieveActive() {
		return userDAO.retrieveActive();
	}
	
	@Transactional
	public List<UserDO> retrieveByEmail(String personal, String secondary) {
		return userDAO.retrieveByEmail(personal, secondary);
	}
	
	@Transactional
	public List<UserDO> retriveById(Long id) {
		return userDAO.retrieveById(id);
	}
	
	@Transactional
	public List<UserDO> retrieveByTempPassword(String tempPassword, String empId) {
		return userDAO.retrieveByTempPassword(tempPassword, empId);
	}
	
	@Transactional
	public List<UserDO> retrieveByEmailId(String email) {
		return userDAO.retrieveByEmailId(email);
	}
	
	@Transactional
	public List<UserDO> retrieve() {
		return userDAO.retrieve();
	}
	
	@Transactional
	public Boolean update(UserDO userDO) {
		return userDAO.update(userDO);
	}
	
	@Transactional
	public Boolean updatePassword(UserDO userDO) {
		return userDAO.updatePassword(userDO);
	}
	
	@Transactional
	public List<UserDO> retrieveUserByUnitId(Long unitId) {
		return userDAO.retrieveUserByUnitId(unitId);
	}
	
	@Transactional
	public List<UserDO> retrieveForLogin(String email, String pasword) {
		return userDAO.retrieveForLogin(email, pasword);
	}
	
	@Transactional
	public List<UserDO> retriveByEmpId(String id) {
		return userDAO.retrieveByEmpId(id);
	}
	
	@Transactional
	public List<UserDO> getSuperAdmins() {
		return userDAO.getSuperAdmins();
	}
}
