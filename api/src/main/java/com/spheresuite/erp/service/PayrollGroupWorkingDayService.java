package com.spheresuite.erp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.PayrollGroupWorkingDayAllowanceDAO;
import com.spheresuite.erp.domainobject.PayrollGroupWorkingdaysAllowanceDO;

@Service
@Transactional
public class PayrollGroupWorkingDayService {

	@Autowired
	private PayrollGroupWorkingDayAllowanceDAO payrollGroupWorkingDayAllowanceDAO;
	
	@Transactional
	public boolean persist(PayrollGroupWorkingdaysAllowanceDO payrollGroupDO){
		return payrollGroupWorkingDayAllowanceDAO.persist(payrollGroupDO);
	}
	
	@Transactional
	public List<PayrollGroupWorkingdaysAllowanceDO> retrieveByGroupID(Long id){
		return payrollGroupWorkingDayAllowanceDAO.retrieveByGroupID(id);
	}
	
	@Transactional
	public List<PayrollGroupWorkingdaysAllowanceDO> retrieveByID(Long id){
		return payrollGroupWorkingDayAllowanceDAO.retrieveByID(id);
	}
	
	@Transactional
	public boolean updateList(List<PayrollGroupWorkingdaysAllowanceDO> payrollGroupList){
		return payrollGroupWorkingDayAllowanceDAO.updateList(payrollGroupList);
	}
	/*@Transactional
	public List<PayrollGroupDO> retrieveActive(){
		return payrollGroupDAO.retrieveActive();
	}
	
	
	
	@Transactional
	public List<PayrollGroupDO> retrieve(){
		return payrollGroupDAO.retrieve();
	}*/
	
	/*@Transactional
	public boolean updateList(PayrollGroupDO payrollGroupDO){
		return payrollGroupDAO.update(payrollGroupDO);
	}*/
}
