package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.HrRequestsDAO;
import com.spheresuite.erp.domainobject.HrRequestsDO;
@Service
@Transactional
public class HrRequestsService {
	static Logger logger = Logger.getLogger(HrRequestsService.class.getName());
	@Autowired
	private HrRequestsDAO hRequestsDAO;

	@Transactional
	public HrRequestsDO persist(HrRequestsDO hrRequestDetail) {
		return hRequestsDAO.persist(hrRequestDetail);
	}

	@Transactional
	public List<HrRequestsDO> retrieve() {
		return hRequestsDAO.retrieve();
	}

	@Transactional
	public List<HrRequestsDO> retrieveById(Long Id) {
		return hRequestsDAO.retrieveById(Id);
	}

	@Transactional
	public HrRequestsDO update(HrRequestsDO hrRequest) {
		return hRequestsDAO.update(hrRequest);
	}

	@Transactional
	public List<HrRequestsDO> retriveByEmpId(String id) {
		return hRequestsDAO.retrieveByEmpId(id);
	}
}
