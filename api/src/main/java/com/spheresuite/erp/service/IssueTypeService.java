package com.spheresuite.erp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.IssueTypeDAO;
import com.spheresuite.erp.domainobject.IssueTypeDO;

@Service
@Transactional
public class IssueTypeService{
	
	@Autowired
	private IssueTypeDAO issueTypeDAO;

	@Transactional
	public boolean persist(IssueTypeDO issueTypeDO) {
		return issueTypeDAO.persist(issueTypeDO);
	}
	
	@Transactional
	public List<IssueTypeDO> retrieve() {
		return issueTypeDAO.retrieve();
	}

	@Transactional
	public boolean update(IssueTypeDO issueTypeDO) {
		return issueTypeDAO.update(issueTypeDO);
	}
	
	/*@Transactional
	public List<IssueTypeDO> retrieveActive(){
		return issueTypeDAO.retrieveActive();
	}
	*/
	@Transactional
	public List<IssueTypeDO> retrieveById(Long id){
		return issueTypeDAO.retrieveById(id);
	}
	
	/*@Transactional
	public boolean persistList(List<IssueTypeDO> IssueTypeDO) {
		return issueTypeDAO.persistList(IssueTypeDO);
	}*/
}
