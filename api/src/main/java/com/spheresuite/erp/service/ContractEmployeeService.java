package com.spheresuite.erp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.ContractEmployeeDAO;
import com.spheresuite.erp.domainobject.ContractEmployeeDO;

@Service
@Transactional
public class ContractEmployeeService{
	
	@Autowired
	private ContractEmployeeDAO contractLaboursDAO;

	@Transactional
	public boolean persist(ContractEmployeeDO contractLaboursDO) {
		return contractLaboursDAO.persist(contractLaboursDO);
	}
	
	@Transactional
	public List<ContractEmployeeDO> retrieve() {
		return contractLaboursDAO.retrieve();
	}

	@Transactional
	public boolean update(ContractEmployeeDO contractLaboursDO) {
		return contractLaboursDAO.update(contractLaboursDO);
	}
	/*
	@Transactional
	public List<AddressTypeDO> retrieveActive(){
		return addressTypeDAO.retrieveActive();
	}
	*/
	@Transactional
	public List<ContractEmployeeDO> retrieveById(Long id){
		return contractLaboursDAO.retrieveById(id);
	}
	
/*	@Transactional
	public boolean persistList(List<AddressTypeDO> addressTypeDO) {
		return addressTypeDAO.persistList(addressTypeDO);
	}*/
}
