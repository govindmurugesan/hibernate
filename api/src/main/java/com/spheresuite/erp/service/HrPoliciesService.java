package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.HrPoliciesDAO;
import com.spheresuite.erp.domainobject.HrPoliciesDO;
@Service
@Transactional
public class HrPoliciesService {
	static Logger logger = Logger.getLogger(HrPoliciesService.class.getName());
	@Autowired
	private HrPoliciesDAO hrPoliciesDAO;

	@Transactional
	public HrPoliciesDO persist(HrPoliciesDO hrPoliciesDOList) {
		return hrPoliciesDAO.persist(hrPoliciesDOList);
	}

	@Transactional
	public List<HrPoliciesDO> retrieveById(Long Id) {
		return hrPoliciesDAO.retrieveById(Id);
	}

	@Transactional
	public List<HrPoliciesDO> retrieveByRoleId(Long Id) {
		return hrPoliciesDAO.retrieveByRoleId(Id);
	}

	@Transactional
	public List<HrPoliciesDO> retrieve() {
		return hrPoliciesDAO.retrieve();
	}

	@Transactional
	public HrPoliciesDO update(HrPoliciesDO hrPoliciesDO) {
		return hrPoliciesDAO.update(hrPoliciesDO);
	}
}
