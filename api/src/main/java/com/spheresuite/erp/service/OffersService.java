package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.OffersDAO;
import com.spheresuite.erp.domainobject.OffersDO;
@Service
@Transactional
public class OffersService {
	static Logger logger = Logger.getLogger(OffersService.class.getName());
	@Autowired
	private OffersDAO offersDAO;
	
	@Transactional
	public OffersDO persist(OffersDO offersDO) {
		return offersDAO.persist(offersDO);
	}

	@Transactional
	public OffersDO update(OffersDO offersDO) {
		return offersDAO.update(offersDO);
	}

	@Transactional
	public List<OffersDO> retrieveById(Long Id) {
		return offersDAO.retrieveById(Id);
	}

	@Transactional
	public List<OffersDO> retrieve() {
		return offersDAO.retrieve();
	}

	@Transactional
	public List<OffersDO> retrieveByStatus(char status) {
		return offersDAO.retrieveByStatus(status);
	}

	@Transactional
	public List<OffersDO> retrieveByProjectId(Long id) {
		return offersDAO.retrieveByProjectId(id);
	}
	
	
}
