package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.PaymentModeDAO;
import com.spheresuite.erp.domainobject.PaymentModeDO;
@Service
@Transactional
public class PaymentModeService {
	static Logger logger = Logger.getLogger(HolidaysService.class.getName());
	@Autowired
	private PaymentModeDAO paymentModeDAO;

	@Transactional
	public PaymentModeDO persist(PaymentModeDO paymentMode) {
		return paymentModeDAO.persist(paymentMode);
	}

	@Transactional
	public List<PaymentModeDO> retrieveById(Long Id) {
		return paymentModeDAO.retrieveById(Id);
	}

	@Transactional
	public List<PaymentModeDO> retrieve() {
		return paymentModeDAO.retrieve();
	}

	@Transactional
	public PaymentModeDO update(PaymentModeDO paymentDetail) {
		return paymentModeDAO.update(paymentDetail);
	}
}
