package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.DASettingsDAO;
import com.spheresuite.erp.domainobject.DASettingsDO;
@Service
@Transactional
public class DASettingsService {
	static Logger logger = Logger.getLogger(DASettingsService.class.getName());
	
	@Autowired
	private DASettingsDAO daSettingsDAO;

	@Transactional
	public boolean persist(DASettingsDO daSettingsDO) {
		return daSettingsDAO.persist(daSettingsDO);
	}

	@Transactional
	public List<DASettingsDO> retrieveById(Long id) {
		return daSettingsDAO.retrieveById(id);
	}
	
	@Transactional
	public List<DASettingsDO> retrieve() {
		return daSettingsDAO.retrieve();
	}

	@Transactional
	public boolean update(DASettingsDO desinationDO) {
		return daSettingsDAO.update(desinationDO);
	}
	
	@Transactional
	public List<DASettingsDO> retrieveByMonthAndYear(String month, Long year) {
		return daSettingsDAO.retrieveByMonthAndYear(month, year);
	}
	
	@Transactional
	public List<DASettingsDO> retrieveByMonthAndYearUnit(String month, Long year, Long unitId) {
		return daSettingsDAO.retrieveByMonthAndYearUnit(month, year, unitId);
	}
	
	
}
