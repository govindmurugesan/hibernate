package com.spheresuite.erp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.ESIGroupDAO;
import com.spheresuite.erp.domainobject.ESIGroupDO;

@Service
@Transactional
public class ESIGroupService {

	@Autowired
	private ESIGroupDAO esiGroupDAO;
	
	@Transactional
	public boolean persist(ESIGroupDO esiGroupDO){
		return esiGroupDAO.persist(esiGroupDO);
	}

	@Transactional
	public List<ESIGroupDO> retrieveActive(){
		return esiGroupDAO.retrieveActive();
	}
	
	@Transactional
	public List<ESIGroupDO> retrieveById(Long id){
		return esiGroupDAO.retrieveById(id);
	}
	
	@Transactional
	public List<ESIGroupDO> retrieveByName(String name){
		return esiGroupDAO.retrieveByName(name);
	}
	
	@Transactional
	public List<ESIGroupDO> retrieve(){
		return esiGroupDAO.retrieve();
	}
	
	@Transactional
	public boolean persistList(List<ESIGroupDO> esiGroupDO) {
		return esiGroupDAO.persistList(esiGroupDO);
	}
	
	@Transactional
	public boolean update(ESIGroupDO esiGroupDO) {
		return esiGroupDAO.update(esiGroupDO);
	}
	
	
}
