package com.spheresuite.erp.service;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.ItSavingsDAO;
import com.spheresuite.erp.domainobject.ItSavingsDO;
@Service
@Transactional
public class ItSavingsService {
	static Logger logger = Logger.getLogger(ItSavingsService.class.getName());
	@Autowired
	private ItSavingsDAO itSavingsDAO;

	@Transactional
	public ItSavingsDO persist(ItSavingsDO itSavingsDO) {
		return itSavingsDAO.persist(itSavingsDO);
	}

	@Transactional
	public List<ItSavingsDO> retrieveByEmpId(String empId) {
		return itSavingsDAO.retrieveByEmpId(empId);
	}

	@Transactional
	public List<ItSavingsDO> retrieveById(Long id) {
		return itSavingsDAO.retrieveById(id);
	}

	@Transactional
	public List<ItSavingsDO> retrieve() {
		return itSavingsDAO.retrieve();
	}

	@Transactional
	public List<ItSavingsDO> retrieveByDate(Date fromDate, Date toDate) {
		return itSavingsDAO.retrieveByDate(fromDate, toDate);
	}
	
	@Transactional
	public List<ItSavingsDO> retrieveByEmpIdSectionIdAndDate(Date date, String empId, long sectionId) {
		return itSavingsDAO.retrieveByEmpIdSectionIdAndDate(date, empId, sectionId);
	}

	@Transactional
	public ItSavingsDO update(ItSavingsDO itSavingsDO) {
		return itSavingsDAO.update(itSavingsDO);
	}

	
}
