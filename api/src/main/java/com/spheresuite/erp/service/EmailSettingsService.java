package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.EmailSettingsDAO;
import com.spheresuite.erp.domainobject.EmailSettingsDO;

@Service
@Transactional
public class EmailSettingsService {
	static Logger logger = Logger.getLogger(EmailSettingsService.class.getName());
	@Autowired
	private EmailSettingsDAO emailSettingsDAO;

	@Transactional
	public boolean persist(EmailSettingsDO emailSettingsDO)  {
		return emailSettingsDAO.persist(emailSettingsDO);
	}

	@Transactional
	public List<EmailSettingsDO> retrieveById(Long Id)  {
		return emailSettingsDAO.retrieveById(Id);
	}

	@Transactional
	public List<EmailSettingsDO> retrieveByMailType(String mailType)  {
		return emailSettingsDAO.retrieveByMailType(mailType);
	}

	@Transactional
	public List<EmailSettingsDO> retrieve()  {
		return emailSettingsDAO.retrieve();
	}
	
	@Transactional
	public List<EmailSettingsDO> retrieveActive()  {
		return emailSettingsDAO.retrieveActive();
	}

	@Transactional
	public boolean update(EmailSettingsDO emailSettingsDO)  {
		return emailSettingsDAO.update(emailSettingsDO);
	}
	
	@Transactional
	public boolean persistList(List<EmailSettingsDO> emailSettingsDO) {
		return emailSettingsDAO.persistList(emailSettingsDO);
	}
}
