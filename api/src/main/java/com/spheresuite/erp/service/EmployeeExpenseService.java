package com.spheresuite.erp.service;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.EmployeeExpenseDAO;
import com.spheresuite.erp.domainobject.EmployeeExpenseDO;
@Service
@Transactional
public class EmployeeExpenseService {
	static Logger logger = Logger.getLogger(EmployeeExpenseService.class.getName());

	@Autowired
	private EmployeeExpenseDAO employeeExpenseDAO;
	
	@Transactional
	public EmployeeExpenseDO persist(EmployeeExpenseDO employeeExpenseDO)  {
		return employeeExpenseDAO.persist(employeeExpenseDO);
	}

	@Transactional
	public List<EmployeeExpenseDO> retrieveByEmpId(String empId)  {
		return employeeExpenseDAO.retrieveByEmpId(empId);
	}

	@Transactional
	public List<EmployeeExpenseDO> retrieveById(Long id)  {
		return employeeExpenseDAO.retrieveById(id);
	}
	
	@Transactional
	public List<EmployeeExpenseDO> retrieve()  {
		return employeeExpenseDAO.retrieve();
	}
	
	@Transactional
	public List<EmployeeExpenseDO> retrieveByMonthAndEmpId(String empId, Date fromDate, Date ToDate, String status)  {
		return employeeExpenseDAO.retrieveByMonthAndEmpId(empId,fromDate,ToDate,status);
	}

	@Transactional
	public EmployeeExpenseDO update(EmployeeExpenseDO employeeExpenseDO)  {
		return employeeExpenseDAO.update(employeeExpenseDO);
	}
	
	@Transactional
	public List<EmployeeExpenseDO> retrieveByReportingId(String empId){
		return employeeExpenseDAO.retrieveByReportingId(empId);
	}
	
}
