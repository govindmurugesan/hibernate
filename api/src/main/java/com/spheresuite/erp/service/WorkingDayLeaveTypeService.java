package com.spheresuite.erp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.WorkingDayLeavetypeDAO;
import com.spheresuite.erp.domainobject.WorkingDayLeavetypeDO;

@Service
@Transactional
public class WorkingDayLeaveTypeService{
	
	@Autowired
	private WorkingDayLeavetypeDAO workingDayLeavetypeDAO;

	@Transactional
	public boolean persist(WorkingDayLeavetypeDO workingDayAllowance) {
		return workingDayLeavetypeDAO.persist(workingDayAllowance);
	}
	
	@Transactional
	public List<WorkingDayLeavetypeDO> persistList(List<WorkingDayLeavetypeDO> workingDayAllowance) {
		return workingDayLeavetypeDAO.persistList(workingDayAllowance);
	}
	
	@Transactional
	public List<WorkingDayLeavetypeDO> retrieveByWorkingDayId(Long id){
		return workingDayLeavetypeDAO.retrieveByWorkingDayId(id);
	}
	
	/*@Transactional
	public List<WorkingDayLeavetypeDO> retrieve() {
		return workingDayLeavetypeDAO.retrieve();
	}

	@Transactional
	public boolean update(WorkingDayLeavetypeDO workingDayAllowance) {
		return workingDayLeavetypeDAO.update(workingDayAllowance);
	}
	
	@Transactional
	public List<WorkingDayLeavetypeDO> retrieveActive(){
		return workingDayLeavetypeDAO.retrieveActive();
	}
	
	*/
	
}
