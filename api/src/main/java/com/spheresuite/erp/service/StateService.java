package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.StateDAO;
import com.spheresuite.erp.domainobject.StateDO;

@Service
@Transactional
public class StateService {
	static Logger logger = Logger.getLogger(StateService.class.getName());
	
	@Autowired
	private StateDAO stateDAO;
	
	@Transactional
	public boolean persist(StateDO stateDO) {
		return stateDAO.persist(stateDO);
	}

	@Transactional
	public List<StateDO> retrieveActive() {
		return stateDAO.retrieveActive();
	}
	
	@Transactional
	public List<StateDO> retrieve() {
		return stateDAO.retrieve();
	}
	
	@Transactional
	public List<StateDO> retrieveById(Long Id) {
		return stateDAO.retrieveById(Id);
	}
	
	@Transactional
	public List<StateDO> retrieveByName(String name) {
		return stateDAO.retrieveByName(name);
	}
	
	@Transactional
	public List<StateDO> retrieveByCountryId(Long Id) {
		return stateDAO.retrieveByCountryId(Id);
	}
	
	@Transactional
	public boolean update(StateDO stateDO) {
		return stateDAO.update(stateDO);
	}
	
	@Transactional
	public List<StateDO> retrieveByActiveName(String name) {
		return stateDAO.retrieveByActiveName(name);
	}
	
	@Transactional
	public boolean persistList(List<StateDO> sateDO) {
		return stateDAO.persistList(sateDO);
	}
}
