package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.WorkLocationDAO;
import com.spheresuite.erp.domainobject.WorkLocationDO;

@Service
@Transactional
public class WorkLocationService {
	static Logger logger = Logger.getLogger(WorkLocationService.class.getName());
	
	@Autowired
	private WorkLocationDAO workLocationDAO;
	
	@Transactional
	public boolean persist(WorkLocationDO workLocationDO) {
		return workLocationDAO.persist(workLocationDO);
	}

	@Transactional
	public List<WorkLocationDO> retrieveActive() {
		return workLocationDAO.retrieveActive();
	}

	@Transactional
	public List<WorkLocationDO> retrieveById(Long Id) {
		return workLocationDAO.retrieveById(Id);
	}
	
	@Transactional
	public List<WorkLocationDO> retrieveByStateId(Long Id) {
		return workLocationDAO.retrieveByStateId(Id);
	}
	
	/*@Transactional
	public List<WorkLocationDO> retrieveByCityAll(String city) {
		return workLocationDAO.retrieveByCityAll(city);
	}*/
	
	@Transactional
	public List<WorkLocationDO> retrieveByName(String name) {
		return workLocationDAO.retrieveByName(name);
	}

	@Transactional
	public List<WorkLocationDO> retrieve() {
		return workLocationDAO.retrieve();
	}

	@Transactional
	public boolean update(WorkLocationDO WorkLocationDO) {
		return workLocationDAO.update(WorkLocationDO);
	}
	
	@Transactional
	public boolean persistList(List<WorkLocationDO> workLocationDO) {
		return workLocationDAO.persistList(workLocationDO);
	}
}
