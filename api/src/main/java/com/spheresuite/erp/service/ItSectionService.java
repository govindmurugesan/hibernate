package com.spheresuite.erp.service;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.ItSectionDAO;
import com.spheresuite.erp.domainobject.ItSectionDO;
@Service
@Transactional
public class ItSectionService {
	static Logger logger = Logger.getLogger(ItSectionService.class.getName());
	@Autowired
	private ItSectionDAO itSectionDAO;
	
	@Transactional
	public boolean persist(ItSectionDO itSavingsDO) {
		return itSectionDAO.persist(itSavingsDO);
	}

	@Transactional
	public List<ItSectionDO> retrieveActive() {
		return itSectionDAO.retrieveActive();
	}

	@Transactional
	public List<ItSectionDO> retrieveById(Long id) {
		return itSectionDAO.retrieveById(id);
	}

	@Transactional
	public List<ItSectionDO> retrieve() {
		return itSectionDAO.retrieve();
	}

	/*@Transactional
	public List<ItSectionDO> retrieveByDate(String fromDate, String toDate) {
		return itSectionDAO.retrieveByDate(fromDate, toDate);
	}*/
	
	@Transactional
	public List<ItSectionDO> retrieveByCurrentDate(Date date) {
		return itSectionDAO.retrieveByCurrentDate(date);
	}
	
	@Transactional
	public boolean update(ItSectionDO itSavingsDO) {
		return itSectionDAO.update(itSavingsDO);
	}
	
	@Transactional
	public boolean persistList(List<ItSectionDO> ItSection) {
		return itSectionDAO.persistList(ItSection);
	}
	
	@Transactional
	public List<ItSectionDO> retrieveBySectionName(String name) {
		return itSectionDAO.retrieveBySectionName(name);
	}
}
