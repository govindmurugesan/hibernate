package com.spheresuite.erp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.PayrollGroupDeductionDAO;
import com.spheresuite.erp.domainobject.PayrollGroupDeductionDO;

@Service
@Transactional
public class PayrollGroupDeductionService {

	@Autowired
	private PayrollGroupDeductionDAO payrollGroupDeductionDAO;
	
	@Transactional
	public boolean persist(PayrollGroupDeductionDO payrollGroupDeductionDO){
		return payrollGroupDeductionDAO.persist(payrollGroupDeductionDO);
	}
	
	@Transactional
	public List<PayrollGroupDeductionDO> retrieveByGroupID(Long id){
		return payrollGroupDeductionDAO.retrieveByGroupID(id);
	}
	
	@Transactional
	public List<PayrollGroupDeductionDO> retrieveByID(Long id){
		return payrollGroupDeductionDAO.retrieveByID(id);
	}
	
	@Transactional
	public boolean updateList(List<PayrollGroupDeductionDO> payrollGroupDeductionList){
		return payrollGroupDeductionDAO.updateList(payrollGroupDeductionList);
	}
	
	/*@Transactional
	public List<PayrollGroupDO> retrieveActive(){
		return payrollGroupDAO.retrieveActive();
	}
	
	@Transactional
	public List<PayrollGroupDO> retrieveById(Long id){
		return payrollGroupDAO.retrieveById(id);
	}
	
	@Transactional
	public List<PayrollGroupDO> retrieve(){
		return payrollGroupDAO.retrieve();
	}*/
	
	/*@Transactional
	public boolean update(PayrollGroupDO payrollGroupDO){
		return payrollGroupDAO.update(payrollGroupDO);
	}*/
}
