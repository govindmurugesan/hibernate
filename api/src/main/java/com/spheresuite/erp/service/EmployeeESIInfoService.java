package com.spheresuite.erp.service;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.EmployeeESIInfoDAO;
import com.spheresuite.erp.domainobject.EmployeeESIInfoDO;
import com.spheresuite.erp.domainobject.EmployeePFInfoDO;
@Service
@Transactional
public class EmployeeESIInfoService {
	static Logger logger = Logger.getLogger(EmployeeESIInfoService.class.getName());
	
	@Autowired
	private EmployeeESIInfoDAO employeeesiInfoDAO;

	@Transactional
	public Boolean persist(EmployeeESIInfoDO employeePF_ESIInfoDO)  {
		return employeeesiInfoDAO.persist(employeePF_ESIInfoDO);
	}

	@Transactional
	public List<EmployeeESIInfoDO> retrieveByEmpId(String empId)  {
		return employeeesiInfoDAO.retrieveByEmpId(empId);
	}
	
	@Transactional
	public List<EmployeeESIInfoDO> retrieveByEmpIdAndDate(String empId, Date fromDate, Date toDate) {
		return employeeesiInfoDAO.retrieveByEmpIdAndDate(empId,fromDate,toDate);
	} 
	@Transactional
	public List<EmployeeESIInfoDO> retrieveById(Long id)  {
		return employeeesiInfoDAO.retrieveById(id);
	}

	@Transactional
	public EmployeeESIInfoDO update(EmployeeESIInfoDO employeePF_ESIInfoDO)  {
		return employeeesiInfoDAO.update(employeePF_ESIInfoDO);
	}
	
	@Transactional
	public boolean persistList(List<EmployeeESIInfoDO> empPFDO) {
		return employeeesiInfoDAO.persistList(empPFDO);
	}
}
