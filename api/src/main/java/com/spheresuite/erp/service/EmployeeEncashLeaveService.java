package com.spheresuite.erp.service;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.EmployeeEncashLeaveDAO;
import com.spheresuite.erp.domainobject.EmployeeEncashLeaveDO;
@Service
@Transactional
public class EmployeeEncashLeaveService {
	static Logger logger = Logger.getLogger(EmployeeEncashLeaveService.class.getName());
	
	@Autowired
	private EmployeeEncashLeaveDAO employeeEncashLeaveDAO;

	@Transactional
	public Boolean persist(EmployeeEncashLeaveDO employeeLeavegrp)  {
		return employeeEncashLeaveDAO.persist(employeeLeavegrp);
	}

	@Transactional
	public List<EmployeeEncashLeaveDO> retrieveByEmpId(String empId)  {
		return employeeEncashLeaveDAO.retrieveByEmpId(empId);
	}
	
	@Transactional
	public List<Object[]> retrieveAvailableLeavesByEmpId(String id, Date leaveTypeFromDate, Date leaveTypeToDate) {
		return employeeEncashLeaveDAO.retrieveAvailableLeavesByEmpId(id, leaveTypeFromDate, leaveTypeToDate);
	}
}
