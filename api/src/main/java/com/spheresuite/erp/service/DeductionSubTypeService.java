package com.spheresuite.erp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.DeductionSubTypeDAO;
import com.spheresuite.erp.domainobject.DeductionSubTypeDO;

@Service
@Transactional
public class DeductionSubTypeService{
	
	@Autowired
	private DeductionSubTypeDAO loanTypeDAO;

	@Transactional
	public boolean persist(DeductionSubTypeDO loanTypeDO) {
		return loanTypeDAO.persist(loanTypeDO);
	}
	
	@Transactional
	public List<DeductionSubTypeDO> retrieve() {
		return loanTypeDAO.retrieve();
	}

	@Transactional
	public boolean update(DeductionSubTypeDO loanTypeDO) {
		return loanTypeDAO.update(loanTypeDO);
	}
	
	/*@Transactional
	public List<LoanTypeDO> retrieveActive(){
		return advanceTypeDAO.retrieveActive();
	}
	*/
	@Transactional
	public List<DeductionSubTypeDO> retrieveById(Long id){
		return loanTypeDAO.retrieveById(id);
	}
	
	@Transactional
	public List<DeductionSubTypeDO> retrieveByName(String name){
		return loanTypeDAO.retrieveByName(name);
	}
	
	@Transactional
	public List<DeductionSubTypeDO> retrieveByDeductionTypeId(Long id){
		return loanTypeDAO.retrieveByDeductionTypeId(id);
	}
	
	/*@Transactional
	public boolean persistList(List<LoanTypeDO> LoanTypeDO) {
		return advanceTypeDAO.persistList(LoanTypeDO);
	}*/
}
