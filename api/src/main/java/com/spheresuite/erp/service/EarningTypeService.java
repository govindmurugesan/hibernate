package com.spheresuite.erp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.EarningTypeDAO;
import com.spheresuite.erp.domainobject.EarningTypeDO;

@Service
@Transactional
public class EarningTypeService {

	@Autowired
	private EarningTypeDAO earningTypeDAO;
	
	@Transactional
	public boolean persist(EarningTypeDO earningTypeDO){
		return earningTypeDAO.persist(earningTypeDO);
	}

	@Transactional
	public List<EarningTypeDO> retrieveActive(){
		return earningTypeDAO.retrieveActive();
	}
	
	@Transactional
	public List<EarningTypeDO> retrieveById(Long id){
		return earningTypeDAO.retrieveById(id);
	}
	
	@Transactional
	public List<EarningTypeDO> retrieveByName(String name){
		return earningTypeDAO.retrieveByName(name);
	}
	
	@Transactional
	public List<EarningTypeDO> retrieve(){
		return earningTypeDAO.retrieve();
	}
	
	@Transactional
	public boolean update(EarningTypeDO earningTypeDO){
		return earningTypeDAO.update(earningTypeDO);
	}
	
	@Transactional
	public boolean persistList(List<EarningTypeDO> earningTypeDO) {
		return earningTypeDAO.persistList(earningTypeDO);
	}
}
