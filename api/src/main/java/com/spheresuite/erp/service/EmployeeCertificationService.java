package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.EmployeeCertificationDAO;
import com.spheresuite.erp.domainobject.EmployeeCertificationDO;
@Service
@Transactional
public class EmployeeCertificationService {
	static Logger logger = Logger.getLogger(EmployeeCertificationService.class.getName());
	
	@Autowired
	private EmployeeCertificationDAO employeeCertificationDAO;

	@Transactional
	public EmployeeCertificationDO persist(EmployeeCertificationDO employeeCertificationDO) {
		return employeeCertificationDAO.persist(employeeCertificationDO);
	}

	@Transactional
	public List<EmployeeCertificationDO> retrieveByEmpId(String empId) {
		return employeeCertificationDAO.retrieveByEmpId(empId);
	}

	@Transactional
	public List<EmployeeCertificationDO> retrieveById(Long id) {
		return employeeCertificationDAO.retrieveById(id);
	}

	@Transactional
	public EmployeeCertificationDO update(EmployeeCertificationDO employeeCertificationDO) {
		return employeeCertificationDAO.update(employeeCertificationDO);
	}
}
