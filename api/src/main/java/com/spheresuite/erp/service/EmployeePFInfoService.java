package com.spheresuite.erp.service;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.EmployeePFInfoDAO;
import com.spheresuite.erp.domainobject.EmployeePFInfoDO;
@Service
@Transactional
public class EmployeePFInfoService {
	static Logger logger = Logger.getLogger(EmployeePFInfoService.class.getName());
	
	@Autowired
	private EmployeePFInfoDAO employeePFInfoDAO;

	@Transactional
	public Boolean persist(EmployeePFInfoDO employeePF_ESIInfoDO)  {
		return employeePFInfoDAO.persist(employeePF_ESIInfoDO);
	}

	@Transactional
	public List<EmployeePFInfoDO> retrieveByEmpId(String empId)  {
		return employeePFInfoDAO.retrieveByEmpId(empId);
	}

	@Transactional
	public List<EmployeePFInfoDO> retrieveById(Long id)  {
		return employeePFInfoDAO.retrieveById(id);
	}
	
	@Transactional
	public List<EmployeePFInfoDO> retrieveByEmpIdAndDate(String empId, Date fromDate, Date toDate) {
		return employeePFInfoDAO.retrieveByEmpIdAndDate(empId,fromDate,toDate);
	}

	@Transactional
	public EmployeePFInfoDO update(EmployeePFInfoDO employeePF_ESIInfoDO)  {
		return employeePFInfoDAO.update(employeePF_ESIInfoDO);
	}
	
	@Transactional
	public boolean persistList(List<EmployeePFInfoDO> empPFDO) {
		return employeePFInfoDAO.persistList(empPFDO);
	}
}
