package com.spheresuite.erp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.ESIGroupEarningsDAO;
import com.spheresuite.erp.domainobject.ESIGroupEarningsDO;

@Service
@Transactional
public class ESIGroupEarningService {

	@Autowired
	private ESIGroupEarningsDAO esiGroupEarningDAO;
	
	@Transactional
	public boolean persist(ESIGroupEarningsDO esiGroupEarningDO){
		return esiGroupEarningDAO.persist(esiGroupEarningDO);
	}
	
	@Transactional
	public List<ESIGroupEarningsDO> retrieveByGroupID(Long id){
		return esiGroupEarningDAO.retrieveByGroupID(id);
	}
	
	@Transactional
	public List<ESIGroupEarningsDO> retrieveByID(Long id){
		return esiGroupEarningDAO.retrieveByID(id);
	}
	
	@Transactional
	public boolean updateList(List<ESIGroupEarningsDO> esiGroupEarningDO){
		return esiGroupEarningDAO.updateList(esiGroupEarningDO);
	}
}
