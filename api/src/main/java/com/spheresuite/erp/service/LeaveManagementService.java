package com.spheresuite.erp.service;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.LeaveManagementDAO;
import com.spheresuite.erp.domainobject.LeaveManagementDO;
@Service
@Transactional
public class LeaveManagementService {
	static Logger logger = Logger.getLogger(LeaveManagementService.class.getName());
	@Autowired
	private LeaveManagementDAO leaveManagementDAO;

	@Transactional
	public Boolean persist(LeaveManagementDO leaveDetail) {
		return leaveManagementDAO.persist(leaveDetail);
	}

	@Transactional
	public List<LeaveManagementDO> retrieve() {
		return leaveManagementDAO.retrieve();
	}

	/*@Transactional
	public List<LeaveManagementDO> retrieveByDept(List<Long> ids) {
		return leaveManagementDAO.retrieveByDept(ids);
	}*/

	@Transactional
	public List<LeaveManagementDO> retrieveById(Long Id) {
		return leaveManagementDAO.retrieveById(Id);
	}

	@Transactional
	public Boolean update(LeaveManagementDO leaveDetail) {
		return leaveManagementDAO.update(leaveDetail);
	}
	
	/*public List<LeaveManagementDO> retriveByEmpId(Long id) {
		return leaveManagementDAO.retrieveByEmpId(id);
	}*/

	@Transactional
	public List<LeaveManagementDO> retriveActiveById(Long id) {
		return leaveManagementDAO.retriveActiveById(id);
	}

	@Transactional
	public List<LeaveManagementDO> retrieveCountByType(Long type) {
		return leaveManagementDAO.retrieveCountByType(type);
	}

	@Transactional
	public List<LeaveManagementDO> retrieveAllActive() {
		return leaveManagementDAO.retrieveAllActive();
	}
	
	@Transactional
	public List<LeaveManagementDO> retrieveCountByYear(Date fromDate, Date toDate,Long leaveTypeId) {
		return leaveManagementDAO.retrieveCountByYear(fromDate, toDate, leaveTypeId);
	}
	
	/*@Transactional
	public List<LeaveManagementDO> retrieveByTypeName(String name) {
		return leaveManagementDAO.retrieveByTypeName(name);
	}*/
	
	@Transactional
	public boolean persistList(List<LeaveManagementDO> leaveManagementDO) {
		return leaveManagementDAO.persistList(leaveManagementDO);
	}
}
