package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.CountryDAO;
import com.spheresuite.erp.domainobject.CountryDO;
@Service
@Transactional
public class CountryService {
	static Logger logger = Logger.getLogger(CountryService.class.getName());

	@Autowired
	private CountryDAO countryDAO;

	@Transactional
	public boolean persist(CountryDO countryDO) {
		return countryDAO.persist(countryDO);
	}

	@Transactional
	public List<CountryDO> retrieveActive() {
		return countryDAO.retrieveActive();
	}

	@Transactional
	public List<CountryDO> retrieve() {
		return countryDAO.retrieve();
	}

	@Transactional
	public List<CountryDO> retrieveById(Long Id) {
		return countryDAO.retrieveById(Id);
	}
	
	@Transactional
	public List<CountryDO> retrieveByName(String name) {
		return countryDAO.retrieveByName(name);
	}

	@Transactional
	public boolean update(CountryDO countryDO) {
		return countryDAO.update(countryDO);
	}
	
	@Transactional
	public boolean persistList(List<CountryDO> countryDO) {
		return countryDAO.persistList(countryDO);
	}
}
