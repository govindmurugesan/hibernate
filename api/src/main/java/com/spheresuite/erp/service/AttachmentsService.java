package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.AttachmentsDAO;
import com.spheresuite.erp.domainobject.AttachmentsDO;
@Service
@Transactional
public class AttachmentsService {
	static Logger logger = Logger.getLogger(AttachmentsService.class.getName());
	
	@Autowired
	private AttachmentsDAO attachmentsDAO;
	@Transactional
	public AttachmentsDO persist(AttachmentsDO attachmentsDO)  {
		return attachmentsDAO.persist(attachmentsDO);
	}
	@Transactional
	public boolean delete()  {
		return attachmentsDAO.delete();
	}
	@Transactional
	public List<AttachmentsDO> retrieve()  {
		return attachmentsDAO.retrieve();
	}
}
