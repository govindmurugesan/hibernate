package com.spheresuite.erp.service;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.EmployeeCompensationDAO;
import com.spheresuite.erp.domainobject.EmployeeCompensationDO;
@Service
@Transactional
public class EmployeeCompensationService {
	static Logger logger = Logger.getLogger(EmployeeCompensationService.class.getName());
	@Autowired
	private EmployeeCompensationDAO employeeCompensationDAO;
	

	@Transactional
	public EmployeeCompensationDO persist(EmployeeCompensationDO empCompensationDO) {
		return employeeCompensationDAO.persist(empCompensationDO);
	}
	
	@Transactional
	public List<EmployeeCompensationDO> retrieveByEmpIdAndStatus(String empId) {
		return employeeCompensationDAO.retrieveByEmpIdAndStatus(empId);
	}
	
	@Transactional
	public EmployeeCompensationDO update(EmployeeCompensationDO empCompensationDO) {
		return employeeCompensationDAO.update(empCompensationDO);
	}
	
	@Transactional
	public List<EmployeeCompensationDO> retrieveActive() {
		return employeeCompensationDAO.retrieveActive();
	}
	
	@Transactional
	public List<EmployeeCompensationDO> retrieveById(Long id) {
		return employeeCompensationDAO.retrieveById(id);
	}
	
	/*@Transactional
	public List<EmployeeCompensationDO> retrieveWithComponsesationDateAndPayrollBatch(Long BatchId, Date month) {
		return employeeCompensationDAO.retrieveWithComponsesationDateAndPayrollBatch(BatchId, month);
	}*/
	
	
	@Transactional
	public List<EmployeeCompensationDO> retrieveWithComponsesationDateAndEmpID(String id, Date month) {
		return employeeCompensationDAO.retrieveWithComponsesationDateAndEmpID(id, month);
	}
	
	
	@Transactional
	public boolean persistList(List<EmployeeCompensationDO> empCompensationList) {
		return employeeCompensationDAO.persistList(empCompensationList);
	}
	
	
	/*@Transactional
	public List<EmployeeCompensationDO> retrieveActive(Long empId) {
		return employeeCompensationDAO.retrieveActive(empId);
	}

	@Transactional
	public List<EmployeeCompensationDO> retrieveById(Long id) {
		return employeeCompensationDAO.retrieveById(id);
	}

	@Transactional
	public List<EmployeeCompensationDO> retrieveByEmpId(String empId) {
		return employeeCompensationDAO.retrieveByEmpId(empId);
	}

	@Transactional
	public List<EmployeeCompensationDO> retrieve() {
		return employeeCompensationDAO.retrieve();
	}

	*/
}
