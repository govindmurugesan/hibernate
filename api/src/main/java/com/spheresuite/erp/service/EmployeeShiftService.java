package com.spheresuite.erp.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.EmployeeShiftDAO;
import com.spheresuite.erp.domainobject.EmployeeShiftDO;

@Service
@Transactional
public class EmployeeShiftService{
	
	@Autowired
	private EmployeeShiftDAO shiftDAO;

	@Transactional
	public boolean persist(EmployeeShiftDO EmployeeShiftDO) {
		return shiftDAO.persist(EmployeeShiftDO);
	}
	
	@Transactional
	public List<EmployeeShiftDO> retrieve() {
		return shiftDAO.retrieve();
	}

	@Transactional
	public boolean update(EmployeeShiftDO EmployeeShiftDO) {
		return shiftDAO.update(EmployeeShiftDO);
	}
	
	@Transactional
	public List<EmployeeShiftDO> retrieveByEmpId(String string){
		return shiftDAO.retrieveByEmpId(string);
	}
	
	@Transactional
	public List<EmployeeShiftDO> retrieveById(Long id){
		return shiftDAO.retrieveById(id);
	}
	
	@Transactional
	public List<EmployeeShiftDO> retrieveByDate(Date date) {
		return shiftDAO.retrieveByDate(date);
	}
	
	@Transactional
	public List<EmployeeShiftDO> retrieveByEmpDate(String id, Date date) {
		return shiftDAO.retrieveByEmpDate(id,date);
	}
	
	@Transactional
	public List<EmployeeShiftDO> retrieveByDateValidate(Long id) {
		return shiftDAO.retrieveByDateValidate(id);
	}
	
	@Transactional
	public boolean persistList(List<EmployeeShiftDO> EmployeeShiftDO) {
		return shiftDAO.persistList(EmployeeShiftDO);
	}
}
