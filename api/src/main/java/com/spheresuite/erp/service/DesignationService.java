package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.DesignationDAO;
import com.spheresuite.erp.domainobject.DesignationDO;
@Service
@Transactional
public class DesignationService {
	static Logger logger = Logger.getLogger(DesignationService.class.getName());
	
	@Autowired
	private DesignationDAO designationDAO;

	@Transactional
	public boolean persist(DesignationDO desinationDO) {
		return designationDAO.persist(desinationDO);
	}

	@Transactional
	public List<DesignationDO> retrieveActive() {
		return designationDAO.retrieveActive();
	}

	@Transactional
	public List<DesignationDO> retrieveById(Long id) {
		return designationDAO.retrieveById(id);
	}
	
	@Transactional
	public List<DesignationDO> retrieveByName(String name) {
		return designationDAO.retrieveByName(name);
	}

	@Transactional
	public List<DesignationDO> retrieve() {
		return designationDAO.retrieve();
	}

	@Transactional
	public boolean update(DesignationDO desinationDO) {
		return designationDAO.update(desinationDO);
	}
	
	@Transactional
	public boolean persistList(List<DesignationDO> designationDO) {
		return designationDAO.persistList(designationDO);
	}
}
