package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.FyDAO;
import com.spheresuite.erp.domainobject.FyDO;
@Service
@Transactional
public class FyService {
	static Logger logger = Logger.getLogger(FyService.class.getName());
	@Autowired
	private FyDAO fydo;

	@Transactional
	public boolean persist(FyDO fyDO) {
		return fydo.persist(fyDO);
	}

	@Transactional
	public List<FyDO> retrieveActive() {
		return fydo.retrieveActive();
	}

	@Transactional
	public List<FyDO> retrieveById(Long Id) {
		return fydo.retrieveById(Id);
	}

	@Transactional
	public List<FyDO> retrieve() {
		return fydo.retrieve();
	}

	@Transactional
	public boolean update(FyDO fyDO) {
		return fydo.update(fyDO);
	}
	
	@Transactional
	public boolean persistList(List<FyDO> fydoList) {
		return fydo.persistList(fydoList);
	}
}
