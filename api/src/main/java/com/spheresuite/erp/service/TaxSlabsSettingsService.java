package com.spheresuite.erp.service;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.TaxSlabsSettingsDAO;
import com.spheresuite.erp.domainobject.TaxSlabsSettingsDO;

@Service
@Transactional
public class TaxSlabsSettingsService {
	static Logger logger = Logger.getLogger(TaxSlabsSettingsService.class.getName());
	
	@Autowired
	private TaxSlabsSettingsDAO taxSlabsSettingsDAO;
	
	@Transactional
	public boolean persist(TaxSlabsSettingsDO taxSettingDO) {
		return taxSlabsSettingsDAO.persist(taxSettingDO);
	}
	
	@Transactional
	public boolean update(TaxSlabsSettingsDO taxSettingDO) {
		return taxSlabsSettingsDAO.update(taxSettingDO);
	}

	@Transactional
	public List<TaxSlabsSettingsDO> retrieveById(Long Id) {
		return taxSlabsSettingsDAO.retrieveById(Id);
	}
	
	@Transactional
	public List<TaxSlabsSettingsDO> retrieveByAgeDate(Long age, Date currentDate) {
		return taxSlabsSettingsDAO.retrieveByAgeDate(age, currentDate);
	}
	
	@Transactional
	public boolean persistList(List<TaxSlabsSettingsDO> taxSlabSettings) {
		return taxSlabsSettingsDAO.persistList(taxSlabSettings);
	}
	
	@Transactional
	public List<TaxSlabsSettingsDO> retrieve() {
		return taxSlabsSettingsDAO.retrieve();
	}
	
}
