package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.InsurancePaymentDetailDAO;
import com.spheresuite.erp.domainobject.InsurancePaymentDetailDO;
@Service
@Transactional
public class InsurancePaymentDetailService {
	static Logger logger = Logger.getLogger(InsurancePaymentDetailService.class.getName());

	@Autowired
	private InsurancePaymentDetailDAO insurancePaymentDetailDAO;
	
	@Transactional
	public InsurancePaymentDetailDO persist(InsurancePaymentDetailDO insurancePaymentDetailDO) {
		return insurancePaymentDetailDAO.persist(insurancePaymentDetailDO);
	}
	
	@Transactional
	public List<InsurancePaymentDetailDO> getByAdvanceId(Long id) {
		return insurancePaymentDetailDAO.getByAdvanceId(id);
	}
	
	@Transactional
	public List<InsurancePaymentDetailDO> getById(Long id) {
		return insurancePaymentDetailDAO.getById(id);
	}
	
	@Transactional
	public List<InsurancePaymentDetailDO> getByMonthAndEmpId(String empId, String month) {
		return insurancePaymentDetailDAO.getByMonthAndEmpId(empId, month);
	}
	
	@Transactional
	public List<InsurancePaymentDetailDO> getByStatusAndEmpId(String empId, String month) {
		return insurancePaymentDetailDAO.getByStatusAndEmpId(empId, month);
	}
	
	
	@Transactional
	public InsurancePaymentDetailDO update(InsurancePaymentDetailDO insurancePayment) {
		return insurancePaymentDetailDAO.update(insurancePayment);
	}
	
}
