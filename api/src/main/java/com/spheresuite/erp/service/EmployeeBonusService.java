package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.EmployeeBonusDAO;
import com.spheresuite.erp.domainobject.EmployeeBonusDO;
@Service
@Transactional
public class EmployeeBonusService {
	static Logger logger = Logger.getLogger(EmployeeBonusService.class.getName());

	@Autowired
	private EmployeeBonusDAO employeeBonusDAO;
	
	@Transactional
	public EmployeeBonusDO persist(EmployeeBonusDO employeeBonusDO) {
		return employeeBonusDAO.persist(employeeBonusDO);
	}

	@Transactional
	public List<EmployeeBonusDO> retrieveByEmpId(String empId) {
		return employeeBonusDAO.retrieveByEmpId(empId);
	}

	@Transactional
	public List<EmployeeBonusDO> retrieveByEmpIdAndMonth(String empId, String month) {
		return employeeBonusDAO.retrieveByEmpIdAndMonth(empId,month);
	}

	@Transactional
	public Long retrieveByEmpIdBetweenMonth(String empId, String fromMonth, String toMonth){
		return employeeBonusDAO.retrieveByEmpIdBetweenMonth(empId,fromMonth, toMonth);
	}

	@Transactional
	public List<EmployeeBonusDO> retrieve() {
		return employeeBonusDAO.retrieve();
	}

	@Transactional
	public List<EmployeeBonusDO> retrieveById(Long id) {
		return employeeBonusDAO.retrieveById(id);
	}

	@Transactional
	public List<EmployeeBonusDO> retrieveByEmpIdWithDate(List<Long> id, String bonusMonth) {
		return employeeBonusDAO.retrieveByEmpIdWithDate(id, bonusMonth);
	}

	@Transactional
	public List<EmployeeBonusDO> retrieveByEmpIdBetweenDate(List<Long> id, String fromMonth, String toMonth) {
		return employeeBonusDAO.retrieveByEmpIdBetweenDate(id, fromMonth, toMonth);
	}

	@Transactional
	public EmployeeBonusDO update(EmployeeBonusDO employeeBonusDO) {
		return employeeBonusDAO.update(employeeBonusDO);
	}
	
	@Transactional
	public boolean delete(Long id) {
		return employeeBonusDAO.delete(id);
	}
}
