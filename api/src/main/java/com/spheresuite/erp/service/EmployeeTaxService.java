package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.EmployeeTaxDAO;
import com.spheresuite.erp.domainobject.EmployeeTaxDO;
@Service
@Transactional
public class EmployeeTaxService {
	static Logger logger = Logger.getLogger(EmployeeTaxService.class.getName());
	@Autowired
	private EmployeeTaxDAO employeeTaxDAO;

	@Transactional
	public List<EmployeeTaxDO> persist(List<EmployeeTaxDO> employeetaxDO) {
		return employeeTaxDAO.persist(employeetaxDO);
	}

	@Transactional
	public List<EmployeeTaxDO> retrieveByEmpCompensationId(Long id) {
		return employeeTaxDAO.retrieveByEmpCompensationId(id);
	}

	@Transactional
	public List<EmployeeTaxDO> retrieve() {
		return employeeTaxDAO.retrieve();
	}

	@Transactional
	public EmployeeTaxDO update(EmployeeTaxDO employeetaxDO) {
		return employeeTaxDAO.update(employeetaxDO);
	}
}
