package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.EmployeeEducationDAO;
import com.spheresuite.erp.domainobject.EmployeeEducationDO;
@Service
@Transactional
public class EmployeeEducationService {
	static Logger logger = Logger.getLogger(EmployeeEducationService.class.getName());
	@Autowired
	private EmployeeEducationDAO employeeEducationDAO;
	
	@Transactional
	public EmployeeEducationDO persist(EmployeeEducationDO employeeEducationDO) {
		return employeeEducationDAO.persist(employeeEducationDO);
	}

	@Transactional
	public List<EmployeeEducationDO> retrieveByEmpId(String empId) {
		return employeeEducationDAO.retrieveByEmpId(empId);
	}

	@Transactional
	public List<EmployeeEducationDO> retrieveById(Long id) {
		return employeeEducationDAO.retrieveById(id);
	}

	@Transactional
	public EmployeeEducationDO update(EmployeeEducationDO employeeEducationDO) {
		return employeeEducationDAO.update(employeeEducationDO);
	}
}
