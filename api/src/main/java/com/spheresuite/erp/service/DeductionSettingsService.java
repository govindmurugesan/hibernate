package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.DeductionSettingsDAO;
import com.spheresuite.erp.domainobject.DeductionSettingDO;
@Service
@Transactional
public class DeductionSettingsService {
	static Logger logger = Logger.getLogger(DeductionSettingsService.class.getName());

	@Autowired
	private DeductionSettingsDAO deductionSettingsDAO;

	@Transactional
	public DeductionSettingDO persist(DeductionSettingDO deductionSettingDO)  {
		return deductionSettingsDAO.persist(deductionSettingDO);
	}

	@Transactional
	public DeductionSettingDO update(DeductionSettingDO deductionSettingDO)  {
		return deductionSettingsDAO.update(deductionSettingDO);
	}

	@Transactional
	public List<DeductionSettingDO> retrieveById(Long Id)  {
		return deductionSettingsDAO.retrieveById(Id);
	}

	@Transactional
	public List<DeductionSettingDO> retrieve()  {
		return deductionSettingsDAO.retrieve();
	}

	@Transactional
	public List<DeductionSettingDO> retrieveActive(String name)  {
		return deductionSettingsDAO.retrieveActive(name);
	}
}
