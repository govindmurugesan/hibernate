package com.spheresuite.erp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.PayrollGroupAttendanceAllowanceDAO;
import com.spheresuite.erp.domainobject.PayrollGroupAttendanceAllowanceDO;

@Service
@Transactional
public class PayrollGroupAttendanceAllowanceService {

	@Autowired
	private PayrollGroupAttendanceAllowanceDAO payrollGroupAttendanceAllowanceDAO;
	
	@Transactional
	public boolean persist(PayrollGroupAttendanceAllowanceDO payrollGroupDO){
		return payrollGroupAttendanceAllowanceDAO.persist(payrollGroupDO);
	}
	
	@Transactional
	public List<PayrollGroupAttendanceAllowanceDO> retrieveByGroupID(Long id){
		return payrollGroupAttendanceAllowanceDAO.retrieveByGroupID(id);
	}
	
	@Transactional
	public List<PayrollGroupAttendanceAllowanceDO> retrieveByID(Long id){
		return payrollGroupAttendanceAllowanceDAO.retrieveByID(id);
	}
	
	@Transactional
	public boolean updateList(List<PayrollGroupAttendanceAllowanceDO> payrollGroupList){
		return payrollGroupAttendanceAllowanceDAO.updateList(payrollGroupList);
	}
	/*@Transactional
	public List<PayrollGroupDO> retrieveActive(){
		return payrollGroupDAO.retrieveActive();
	}
	
	
	
	@Transactional
	public List<PayrollGroupDO> retrieve(){
		return payrollGroupDAO.retrieve();
	}*/
	
	/*@Transactional
	public boolean updateList(PayrollGroupDO payrollGroupDO){
		return payrollGroupDAO.update(payrollGroupDO);
	}*/
}
