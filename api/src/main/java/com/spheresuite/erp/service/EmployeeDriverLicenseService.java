package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.EmployeeDriverLicenseDAO;
import com.spheresuite.erp.domainobject.EmployeeDriverLicenseDO;
@Service
@Transactional
public class EmployeeDriverLicenseService {
	static Logger logger = Logger.getLogger(EmployeeDriverLicenseService.class.getName());
	@Autowired
	private EmployeeDriverLicenseDAO employeeDriverLicenseDAO;
	
	@Transactional
	public EmployeeDriverLicenseDO persist(EmployeeDriverLicenseDO employeeDriverLicenseDO) {
		return employeeDriverLicenseDAO.persist(employeeDriverLicenseDO);
	}

	@Transactional
	public List<EmployeeDriverLicenseDO> retrieveByEmpId(String empId) {
		return employeeDriverLicenseDAO.retrieveByEmpId(empId);
	}

	@Transactional
	public List<EmployeeDriverLicenseDO> retrieveById(Long id) {
		return employeeDriverLicenseDAO.retrieveById(id);
	}

	@Transactional
	public EmployeeDriverLicenseDO update(EmployeeDriverLicenseDO employeeDriverLicenseDO) {
		return employeeDriverLicenseDAO.update(employeeDriverLicenseDO);
	}
}
