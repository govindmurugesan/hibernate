package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.ItSavingDocDAO;
import com.spheresuite.erp.domainobject.ItSavingsDocDO;
@Service
@Transactional
public class ItSavingDocService {
	static Logger logger = Logger.getLogger(ItSavingDocService.class.getName());
	@Autowired
	private ItSavingDocDAO itSavingDocDAO;
	
	@Transactional
	public ItSavingsDocDO persist(ItSavingsDocDO itsavingdoc) {
		return itSavingDocDAO.persist(itsavingdoc);
	}

	@Transactional
	public List<ItSavingsDocDO> retrieveByOppId(Long id) {
		return itSavingDocDAO.retrieveByOppId(id);
	}

	@Transactional
	public boolean delete(Long id) {
		return itSavingDocDAO.delete(id);
	}

	@Transactional
	public List<ItSavingsDocDO> retrieve() {
		return itSavingDocDAO.retrieve();
	}

	@Transactional
	public ItSavingsDocDO update(ItSavingsDocDO itsavingdoc) {
		return itSavingDocDAO.update(itsavingdoc);
	}
}
