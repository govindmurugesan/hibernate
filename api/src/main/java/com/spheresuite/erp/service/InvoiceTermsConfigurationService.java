package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.crm.service.LeadTypeService;
import com.spheresuite.erp.dao.InvoiceTermsConfigurationDAO;
import com.spheresuite.erp.domainobject.InvoiceTermsConfigurationDO;
@Service
@Transactional
public class InvoiceTermsConfigurationService {
	static Logger logger = Logger.getLogger(LeadTypeService.class.getName());
	@Autowired
	private InvoiceTermsConfigurationDAO invoiceTermsConfigurationDAO;
	
	@Transactional
	public InvoiceTermsConfigurationDO persist(InvoiceTermsConfigurationDO invoiceTermsConfigurationDO) {
		return invoiceTermsConfigurationDAO.persist(invoiceTermsConfigurationDO);
	}

	@Transactional
	public List<InvoiceTermsConfigurationDO> retrieveActive() {
		return invoiceTermsConfigurationDAO.retrieveActive();
	}

	@Transactional
	public List<InvoiceTermsConfigurationDO> retrieveById(Long Id) {
		return invoiceTermsConfigurationDAO.retrieveById(Id);
	}

	@Transactional
	public List<InvoiceTermsConfigurationDO> retrieve() {
		return invoiceTermsConfigurationDAO.retrieve();
	}

	@Transactional
	public InvoiceTermsConfigurationDO update(InvoiceTermsConfigurationDO invoiceTermsConfigurationDO) {
		return invoiceTermsConfigurationDAO.update(invoiceTermsConfigurationDO);
	}
}
