package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.PFDAO;
import com.spheresuite.erp.domainobject.PFDO;

@Service
@Transactional
public class PFService {
	static Logger logger = Logger.getLogger(PFService.class.getName());
	
	@Autowired
	private PFDAO pfDAO;
	
	@Transactional
	public boolean persist(PFDO pfDO) {
		return pfDAO.persist(pfDO);
	}
	
	@Transactional
	public List<PFDO> retrieveById(Long Id) {
		return pfDAO.retrieveById(Id);
	}
	
	@Transactional
	public List<PFDO> retrieve() {
		return pfDAO.retrieve();
	}
	
	@Transactional
	public List<PFDO> retrieveActive() {
		return pfDAO.retrieveActive();
	}
	
	
	
	/*@Transactional
	public List<EducationCessDO> retrieveByCurrentDate(Date currentDate) {
		return educationCessDAO.retrieveByCurrentDate(currentDate);
	}*/
	
	@Transactional
	public boolean persistList(List<PFDO> pfDO) {
		return pfDAO.persistList(pfDO);
	}
	
	@Transactional
	public boolean update(PFDO pfDO) {
		return pfDAO.update(pfDO);
	}
}
