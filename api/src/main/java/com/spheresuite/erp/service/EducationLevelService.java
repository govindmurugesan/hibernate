package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.EducationLevelDAO;
import com.spheresuite.erp.domainobject.EducationLevelDO;

@Service
@Transactional
public class EducationLevelService {
	static Logger logger = Logger.getLogger(EducationLevelService.class.getName());
	@Autowired
	private EducationLevelDAO educationLevelDAO;
	
	@Transactional
	public boolean persist(EducationLevelDO EducationLevelDO)  {
		return educationLevelDAO.persist(EducationLevelDO);
	}

	@Transactional
	public List<EducationLevelDO> retrieveActive()  {
		return educationLevelDAO.retrieveActive();
	}

	@Transactional
	public List<EducationLevelDO> retrieveById(Long id)  {
		return educationLevelDAO.retrieveById(id);
	}

	@Transactional
	public List<EducationLevelDO> retrieve()  {
		return educationLevelDAO.retrieve();
	}

	@Transactional
	public boolean update(EducationLevelDO EducationLevelDO)  {
		return educationLevelDAO.update(EducationLevelDO);
	}
	
	@Transactional
	public boolean persistList(List<EducationLevelDO> educationLevelDO) {
		return educationLevelDAO.persistList(educationLevelDO);
	}
}
