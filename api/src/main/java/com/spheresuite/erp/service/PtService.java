package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.PtDAO;
import com.spheresuite.erp.domainobject.PtDO;

@Service
@Transactional
public class PtService {
	static Logger logger = Logger.getLogger(PtService.class.getName());
	
	@Autowired
	private PtDAO ptDAO;
	
	@Transactional
	public boolean persist(PtDO ptDO) {
		return ptDAO.persist(ptDO);
	}

	@Transactional
	public List<PtDO> retrieveActive() {
		return ptDAO.retrieveActive();
	}
	
	@Transactional
	public List<PtDO> retrieveActiveById(Long Id) {
		return ptDAO.retrieveActiveById(Id);
	}
	
	@Transactional
	public List<PtDO> retrieveByState(Long stateId) {
		return ptDAO.retrieveByState(stateId);
	}
	
	@Transactional
	public List<PtDO> retrieveByCtcAndState(Long ctc ,Long stateId) {
		return ptDAO.retrieveByCtcAndState(ctc,stateId);
	}
	
	@Transactional
	public List<PtDO> retrieve() {
		return ptDAO.retrieve();
	}
	
	@Transactional
	public boolean update(PtDO ptDO) {
		return ptDAO.update(ptDO);
	}
	
	@Transactional
	public boolean persistList(List<PtDO> ptDO) {
		return ptDAO.persistList(ptDO);
	}
}
