package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.PayrollTaxDetailsDAO;
import com.spheresuite.erp.domainobject.PayrollTaxDetailsDO;

@Service
@Transactional
public class PayrollTaxDetailsService {
	static Logger logger = Logger.getLogger(PayrollTaxDetailsService.class.getName());
	
	@Autowired
	private PayrollTaxDetailsDAO payrollTaxDetailsDAO;
	
	@Transactional
	public List<PayrollTaxDetailsDO> persist(List<PayrollTaxDetailsDO> payrollTaxDetailsDO) {
		return payrollTaxDetailsDAO.persist(payrollTaxDetailsDO);
	}

	@Transactional
	public List<PayrollTaxDetailsDO> retrieveByEmpCompensationId(Long id) {
		return payrollTaxDetailsDAO.retrieveByEmpCompensationId(id);
	}
	
	@Transactional
	public List<PayrollTaxDetailsDO> retrieve() {
		return payrollTaxDetailsDAO.retrieve();
	}
	
	@Transactional
	public PayrollTaxDetailsDO update(PayrollTaxDetailsDO payrollTaxDetailsDO) {
		return payrollTaxDetailsDAO.update(payrollTaxDetailsDO);
	}
	
	@Transactional
	public List<PayrollTaxDetailsDO> retrieveByEmpCompensationIds(List<Long> id) {
		return payrollTaxDetailsDAO.retrieveByEmpCompensationIds(id);
	}
	
	@Transactional
	public boolean deletePayrollById(Long payrollId) {
		return payrollTaxDetailsDAO.deletePayrollById(payrollId);
	}
}
