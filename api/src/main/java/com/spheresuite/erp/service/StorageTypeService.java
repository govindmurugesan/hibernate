package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.StorageTypeDAO;
import com.spheresuite.erp.domainobject.StorageTypeDO;

@Service
@Transactional
public class StorageTypeService {
	static Logger logger = Logger.getLogger(StorageTypeService.class.getName());
	
	@Autowired
	private StorageTypeDAO storageTypeDAO;
	
	@Transactional
	public StorageTypeDO persist(StorageTypeDO storageTypeDO) {
		return storageTypeDAO.persist(storageTypeDO);
	}

	@Transactional
	public List<StorageTypeDO> retrieveActive() {
		return storageTypeDAO.retrieveActive();
	}
	
	@Transactional
	public List<StorageTypeDO> retrieveById(Long id) {
		return storageTypeDAO.retrieveById(id);
	}
	
	@Transactional
	public List<StorageTypeDO> retrieve() {
		return storageTypeDAO.retrieve();
	}
	
	@Transactional
	public StorageTypeDO update(StorageTypeDO storageTypeDO) {
		return storageTypeDAO.update(storageTypeDO);
	}
}
