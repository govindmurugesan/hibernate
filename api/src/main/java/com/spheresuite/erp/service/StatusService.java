package com.spheresuite.erp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.StatusDAO;
import com.spheresuite.erp.domainobject.StatusDO;

@Service
@Transactional
public class StatusService{
	
	@Autowired
	private StatusDAO statusDAO;

	@Transactional
	public boolean persist(StatusDO statusDO) {
		return statusDAO.persist(statusDO);
	}
	
	@Transactional
	public List<StatusDO> retrieve() {
		return statusDAO.retrieve();
	}

	@Transactional
	public boolean update(StatusDO statusDO) {
		return statusDAO.update(statusDO);
	}
	
	/*@Transactional
	public List<StatusDO> retrieveActive(){
		return statusDAO.retrieveActive();
	}
	*/
	@Transactional
	public List<StatusDO> retrieveById(Long id){
		return statusDAO.retrieveById(id);
	}
	
	/*@Transactional
	public boolean persistList(List<StatusDO> StatusDO) {
		return statusDAO.persistList(StatusDO);
	}*/
}
