package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.SupportDocDAO;
import com.spheresuite.erp.domainobject.SupportDocDO;
@Service
@Transactional
public class SupportDocService {
	static Logger logger = Logger.getLogger(SupportDocService.class.getName());
	@Autowired
	private SupportDocDAO supportDocDAO;

	@Transactional
	public SupportDocDO persist(SupportDocDO supportDocDO) {
		return supportDocDAO.persist(supportDocDO);
	}

	@Transactional
	public List<SupportDocDO> retrieveBySupportId(Long id) {
		return supportDocDAO.retrieveBySupportId(id);
	}

	@Transactional
	public boolean delete(Long id) {
		return supportDocDAO.delete(id);
	}

	@Transactional
	public List<SupportDocDO> retrieve() {
		return supportDocDAO.retrieve();
	}

	@Transactional
	public SupportDocDO update(SupportDocDO supportDocDO) {
		return supportDocDAO.update(supportDocDO);
	}
}
