package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.CompanyCalenderDAO;
import com.spheresuite.erp.domainobject.CompanyCalenderDO;

@Service
@Transactional
public class CompanyCalenderService {
	static Logger logger = Logger.getLogger(CompanyCalenderService.class.getName());
	
	@Autowired
	private CompanyCalenderDAO companyCalenderDetailsDAO;
	
	@Transactional
	public List<CompanyCalenderDO> persist(List<CompanyCalenderDO> companyCalenderDO) {
		return companyCalenderDetailsDAO.persist(companyCalenderDO);
	}
	
	@Transactional
	public Boolean deleteByMonth(String month, Long batchId) {
		return companyCalenderDetailsDAO.deleteByMonth(month, batchId);
	}
	
	@Transactional
	public List<CompanyCalenderDO> retrieveByMonth(String month, Long batchId) {
		return companyCalenderDetailsDAO.retrieveByMonth(month, batchId);
	}
	
	/*@Transactional
	public List<PayrollEarningsDetailsDO> retrieveByEmpCompensationId(Long id) {
		return payrollEarningsDetailsDAO.retrieveByEmpCompensationId(id);
	}
	
	@Transactional
	public List<PayrollEarningsDetailsDO> retrieve() {
		return payrollEarningsDetailsDAO.retrieve();
	}
	
	@Transactional
	public PayrollEarningsDetailsDO update(PayrollEarningsDetailsDO employeeEarningsDO) {
		return payrollEarningsDetailsDAO.update(employeeEarningsDO);
	}
	
	@Transactional
	public List<PayrollEarningsDetailsDO> retrieveByEmpCompensationIds(List<Long> id) {
		return payrollEarningsDetailsDAO.retrieveByEmpCompensationIds(id);
	}*/
}
