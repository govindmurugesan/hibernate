package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.TimesheetDAO;
import com.spheresuite.erp.domainobject.TimesheetDO;

@Service
@Transactional
public class TimesheetService {
	static Logger logger = Logger.getLogger(TimesheetService.class.getName());
	
	@Autowired
	private TimesheetDAO timesheetDAO;
	
	@Transactional
	public boolean persist(TimesheetDO timesheet) {
		return timesheetDAO.persist(timesheet);
	}

	@Transactional
	public List<TimesheetDO> retrieve() {
		return timesheetDAO.retrieve();
	}
	
	@Transactional
	public List<TimesheetDO> retrieveById(Long Id) {
		return timesheetDAO.retrieveById(Id);
	}
	
	@Transactional
	public List<TimesheetDO> retrieveByWeekendDate(String date, String empId) {
		return timesheetDAO.retrieveByWeekendDate(date, empId);
	}
	
	@Transactional	
	public boolean update(TimesheetDO timesheet) {
		return timesheetDAO.update(timesheet);
	}
}
