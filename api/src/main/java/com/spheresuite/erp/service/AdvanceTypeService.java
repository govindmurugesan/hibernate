package com.spheresuite.erp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.AdvanceTypeDAO;
import com.spheresuite.erp.domainobject.AdvanceTypeDO;

@Service
@Transactional
public class AdvanceTypeService{
	
	@Autowired
	private AdvanceTypeDAO advanceTypeDAO;

	@Transactional
	public boolean persist(AdvanceTypeDO AdvanceTypeDO) {
		return advanceTypeDAO.persist(AdvanceTypeDO);
	}
	
	@Transactional
	public List<AdvanceTypeDO> retrieve() {
		return advanceTypeDAO.retrieve();
	}

	@Transactional
	public boolean update(AdvanceTypeDO AdvanceTypeDO) {
		return advanceTypeDAO.update(AdvanceTypeDO);
	}
	
	/*@Transactional
	public List<AdvanceTypeDO> retrieveActive(){
		return advanceTypeDAO.retrieveActive();
	}
	*/
	@Transactional
	public List<AdvanceTypeDO> retrieveById(Long id){
		return advanceTypeDAO.retrieveById(id);
	}
	
	/*@Transactional
	public boolean persistList(List<AdvanceTypeDO> AdvanceTypeDO) {
		return advanceTypeDAO.persistList(AdvanceTypeDO);
	}*/
}
