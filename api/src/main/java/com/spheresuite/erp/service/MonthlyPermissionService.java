package com.spheresuite.erp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.MonthlyPermissionDAO;
import com.spheresuite.erp.domainobject.MonthlyPermissionDO;

@Service
@Transactional
public class MonthlyPermissionService {

	@Autowired
	private MonthlyPermissionDAO monthlyPermissionDAO;
	
	@Transactional
	public boolean persist(MonthlyPermissionDO monthlyPermissionDO){
		return monthlyPermissionDAO.persist(monthlyPermissionDO);
	}

	@Transactional
	public List<MonthlyPermissionDO> retrieveActive(){
		return monthlyPermissionDAO.retrieveActive();
	}
	
	@Transactional
	public List<MonthlyPermissionDO> retrieveById(Long id){
		return monthlyPermissionDAO.retrieveById(id);
	}
	
	@Transactional
	public boolean update(MonthlyPermissionDO monthlyPermissionDO){
		return monthlyPermissionDAO.update(monthlyPermissionDO);
	}
}
