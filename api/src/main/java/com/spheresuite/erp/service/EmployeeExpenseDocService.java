package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.EmployeeExpenseDocDAO;
import com.spheresuite.erp.domainobject.EmployeeExpenseDocDO;
@Service
@Transactional
public class EmployeeExpenseDocService {
	static Logger logger = Logger.getLogger(EmployeeExpenseDocService.class.getName());
	@Autowired
	private EmployeeExpenseDocDAO empExpenseDocDAO;

	@Transactional
	public EmployeeExpenseDocDO persist(EmployeeExpenseDocDO empExpenseDocDO) {
		return empExpenseDocDAO.persist(empExpenseDocDO);
	}

	@Transactional
	public List<EmployeeExpenseDocDO> retrieveByExpenseId(Long id) {
		return empExpenseDocDAO.retrieveByExpenseId(id);
	}

	@Transactional
	public boolean delete(Long id) {
		return empExpenseDocDAO.delete(id);
	}

	@Transactional
	public List<EmployeeExpenseDocDO> retrieve() {
		return empExpenseDocDAO.retrieve();
	}

	@Transactional
	public EmployeeExpenseDocDO update(EmployeeExpenseDocDO empExpenseDocDO) {
		return empExpenseDocDAO.update(empExpenseDocDO);
	}
}
