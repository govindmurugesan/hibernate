package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.UnitOrBranchDAO;
import com.spheresuite.erp.domainobject.UnitOrBranchDO;

@Service
@Transactional
public class UnitOrBranchService {
	static Logger logger = Logger.getLogger(UnitOrBranchService.class.getName());
	
	@Autowired
	private UnitOrBranchDAO unitOrBranchDAO;
	
	@Transactional
	public boolean persist(UnitOrBranchDO unitOrBranchDO) {
		return unitOrBranchDAO.persist(unitOrBranchDO);
	}

	@Transactional
	public List<UnitOrBranchDO> retrieveActive() {
		return unitOrBranchDAO.retrieveActive();
	}

	@Transactional
	public List<UnitOrBranchDO> retrieveActiveById(Long Id) {
		return unitOrBranchDAO.retrieveActiveById(Id);
	}
	
	@Transactional
	public List<UnitOrBranchDO> retrieveByCityAll(String city) {
		return unitOrBranchDAO.retrieveByCityAll(city);
	}
	
	@Transactional
	public List<UnitOrBranchDO> retrieveActiveByName(String city) {
		return unitOrBranchDAO.retrieveActiveByName(city);
	}

	@Transactional
	public List<UnitOrBranchDO> retrieve() {
		return unitOrBranchDAO.retrieve();
	}

	@Transactional
	public boolean update(UnitOrBranchDO unitOrBranchDO) {
		return unitOrBranchDAO.update(unitOrBranchDO);
	}
	
	@Transactional
	public boolean persistList(List<UnitOrBranchDO> unitBranchList) {
		return unitOrBranchDAO.persistList(unitBranchList);
	}
}
