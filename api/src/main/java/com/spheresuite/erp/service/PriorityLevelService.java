package com.spheresuite.erp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.PriorityLevelDAO;
import com.spheresuite.erp.domainobject.PriorityLevelDO;

@Service
@Transactional
public class PriorityLevelService{
	
	@Autowired
	private PriorityLevelDAO priorityLevelDAO;

	@Transactional
	public boolean persist(PriorityLevelDO priorityLevelDO) {
		return priorityLevelDAO.persist(priorityLevelDO);
	}
	
	@Transactional
	public List<PriorityLevelDO> retrieve() {
		return priorityLevelDAO.retrieve();
	}

	@Transactional
	public boolean update(PriorityLevelDO priorityLevelDO) {
		return priorityLevelDAO.update(priorityLevelDO);
	}
	
	/*@Transactional
	public List<PriorityLevelDO> retrieveActive(){
		return advanceTypeDAO.retrieveActive();
	}
	*/
	@Transactional
	public List<PriorityLevelDO> retrieveById(Long id){
		return priorityLevelDAO.retrieveById(id);
	}
	
	/*@Transactional
	public boolean persistList(List<PriorityLevelDO> PriorityLevelDO) {
		return advanceTypeDAO.persistList(PriorityLevelDO);
	}*/
}
