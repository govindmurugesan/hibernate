package com.spheresuite.erp.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.LeaveGroupSettingsDAO;
import com.spheresuite.erp.domainobject.LeaveGroupSettingsDO;

@Service
@Transactional
public class LeaveGroupSettingsService {

	@Autowired
	private LeaveGroupSettingsDAO leaveGroupSettingsDAO;
	
	@Transactional
	public boolean persist(LeaveGroupSettingsDO leaveGroupSettingsDO){
		return leaveGroupSettingsDAO.persist(leaveGroupSettingsDO);
	}
	
	@Transactional
	public List<LeaveGroupSettingsDO> retrieveByGroupIdWithStatus(Long id){
		return leaveGroupSettingsDAO.retrieveByGroupIdWithStatus(id);
	}
	
	@Transactional
	public List<LeaveGroupSettingsDO> retrieveByGroupID(Long id){
		return leaveGroupSettingsDAO.retrieveByGroupID(id);
	}
	
	
	
	@Transactional
	public List<LeaveGroupSettingsDO> retrieveByID(Long id){
		return leaveGroupSettingsDAO.retrieveByID(id);
	}
	
	@Transactional
	public boolean updateList(List<LeaveGroupSettingsDO> leaveGroupSettingsList){
		return leaveGroupSettingsDAO.updateList(leaveGroupSettingsList);
	}
	
	@Transactional
	public List<LeaveGroupSettingsDO> retrieveAvailableLeavesByGrpId(Long grpId, Date currentDate ){
		return leaveGroupSettingsDAO.retrieveAvailableLeavesByGrpId(grpId, currentDate);
	}
	
	@Transactional
	public List<LeaveGroupSettingsDO> retrieveAvailableLeavesByGrpIdAndDate(Long grpId, Date fromDate,  Date toDate){
		return leaveGroupSettingsDAO.retrieveAvailableLeavesByGrpIdAndDate(grpId, fromDate, toDate);
	}
	
	@Transactional
	public List<LeaveGroupSettingsDO> retrieveByGrpAndLeaveId(Long grpId, Long leaveId){
		return leaveGroupSettingsDAO.retrieveByGrpAndLeaveId(grpId, leaveId);
	}
	
	@Transactional
	public List<LeaveGroupSettingsDO> retrieveByGrpAndCompensation(Long grpId, String type){
		return leaveGroupSettingsDAO.retrieveByGrpAndCompensation(grpId, type);
	}
	
	@Transactional
	public List<LeaveGroupSettingsDO> retrieveByGrpAndPermission(Long grpId, String type){
		return leaveGroupSettingsDAO.retrieveByGrpAndCompensation(grpId, type);
	}
	
	@Transactional
	public boolean update(LeaveGroupSettingsDO leaveGroupSettingsList){
		return leaveGroupSettingsDAO.update(leaveGroupSettingsList);
	}
	
	
	
	/*@Transactional
	public List<PayrollGroupDO> retrieveActive(){
		return payrollGroupDAO.retrieveActive();
	}
	
	
	
	@Transactional
	public List<PayrollGroupDO> retrieve(){
		return payrollGroupDAO.retrieve();
	}*/
	
	/*@Transactional
	public boolean updateList(PayrollGroupDO payrollGroupDO){
		return payrollGroupDAO.update(payrollGroupDO);
	}*/
}
