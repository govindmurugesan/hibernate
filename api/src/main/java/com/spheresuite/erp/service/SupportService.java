package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.SupportDAO;
import com.spheresuite.erp.domainobject.SupportDO;
@Service
@Transactional
public class SupportService {
	static Logger logger = Logger.getLogger(SupportService.class.getName());
	
	@Autowired
	private SupportDAO supportDAO;
	
	@Transactional
	public SupportDO persist(SupportDO supportDO)  {
		return supportDAO.persist(supportDO);
	}

	@Transactional
	public List<SupportDO> retriveById(Long id)  {
		return supportDAO.retrieveById(id);
	}
	
	@Transactional
	public List<SupportDO> retriveByEmailId(String email)  {
		return supportDAO.retriveByEmailId(email);
	}

	@Transactional
	public List<SupportDO> retriveByLeadId(Long id)  {
		return supportDAO.retrieveByLeadId(id);
	}

	@Transactional
	public List<SupportDO> retrieve(List<String> ids)  {
		return supportDAO.retrieve(ids);
	}

	@Transactional
	public List<SupportDO> retrieveAll()  {
		return supportDAO.retrieveAll();
	}

	@Transactional
	public SupportDO update(SupportDO supportDO)  {
		return supportDAO.update(supportDO);
	}

	@Transactional
	public boolean update(List<SupportDO> supportDO)  {
		return supportDAO.update(supportDO);
	}

	@Transactional
	public boolean persistList(List<SupportDO> supportDO)  {
		return supportDAO.persistList(supportDO);
	}
}
