package com.spheresuite.erp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.PayrollGroupTaxDAO;
import com.spheresuite.erp.domainobject.PayrollGroupTaxDO;

@Service
@Transactional
public class PayrollGroupTaxService {

	@Autowired
	private PayrollGroupTaxDAO payrollGroupTaxDAO;
	
	@Transactional
	public boolean persist(PayrollGroupTaxDO payrollGroupTaxDO){
		return payrollGroupTaxDAO.persist(payrollGroupTaxDO);
	}
	
	@Transactional
	public boolean persistWithoutDuplicateCheck(PayrollGroupTaxDO payrollGroupTaxDO){
		return payrollGroupTaxDAO.persistWithoutDuplicateCheck(payrollGroupTaxDO);
	}
	
	
	
	@Transactional
	public List<PayrollGroupTaxDO> retrieveByGroupID(Long id){
		return payrollGroupTaxDAO.retrieveByGroupID(id);
	}
	
	@Transactional
	public List<PayrollGroupTaxDO> retrieveByID(Long id){
		return payrollGroupTaxDAO.retrieveByID(id);
	}
	
	@Transactional
	public List<PayrollGroupTaxDO> retrieveByTDS(Long grpId){
		return payrollGroupTaxDAO.retrieveByTDS(grpId);
	}
	
	@Transactional
	public List<PayrollGroupTaxDO> retrieveByPt(Long grpId){
		return payrollGroupTaxDAO.retrieveByPt(grpId);
	}
	
	@Transactional
	public List<PayrollGroupTaxDO> retrieveByEducationCess(Long grpId){
		return payrollGroupTaxDAO.retrieveByEducationCess(grpId);
	}
	
	
	
	@Transactional
	public boolean updateList(List<PayrollGroupTaxDO> payrollGroupTaxList){
		return payrollGroupTaxDAO.updateList(payrollGroupTaxList);
	}
	
	@Transactional
	public boolean update(PayrollGroupTaxDO payrollGroupDO){
		return payrollGroupTaxDAO.update(payrollGroupDO);
	}

	/*@Transactional
	public List<PayrollGroupDO> retrieveActive(){
		return payrollGroupDAO.retrieveActive();
	}
	
	@Transactional
	public List<PayrollGroupDO> retrieveById(Long id){
		return payrollGroupDAO.retrieveById(id);
	}
	
	@Transactional
	public List<PayrollGroupDO> retrieve(){
		return payrollGroupDAO.retrieve();
	}*/
	
	/*@Transactional
	public boolean update(PayrollGroupDO payrollGroupDO){
		return payrollGroupDAO.update(payrollGroupDO);
	}*/
}
