package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.AdvancePaymentDAO;
import com.spheresuite.erp.domainobject.AdvancePaymentDO;
@Service
@Transactional
public class AdvancePaymentService {
	static Logger logger = Logger.getLogger(AdvancePaymentService.class.getName());

	@Autowired
	private AdvancePaymentDAO advancePaymentDAO;
	
	@Transactional
	public AdvancePaymentDO persist(AdvancePaymentDO AdvancePaymentDO) {
		return advancePaymentDAO.persist(AdvancePaymentDO);
	}

	@Transactional
	public List<AdvancePaymentDO> retrieveActiveByEmpId(String empId) {
		return advancePaymentDAO.retrieveActiveByEmpId(empId);
	}

	/*@Transactional
	public List<AdvancePaymentDO> retrieveByEmpIdAndMonth(String empId, String month) {
		return advancePaymentDAO.retrieveByEmpIdAndMonth(empId,month);
	}

	@Transactional
	public Long retrieveByEmpIdBetweenMonth(String empId, String fromMonth, String toMonth){
		return advancePaymentDAO.retrieveByEmpIdBetweenMonth(empId,fromMonth, toMonth);
	}*/

	@Transactional
	public List<AdvancePaymentDO> retrieve() {
		return advancePaymentDAO.retrieve();
	}

	@Transactional
	public List<AdvancePaymentDO> retrieveById(Long id) {
		return advancePaymentDAO.retrieveById(id);
	}

	/*@Transactional
	public List<AdvancePaymentDO> retrieveByEmpIdWithDate(List<Long> id, String bonusMonth) {
		return advancePaymentDAO.retrieveByEmpIdWithDate(id, bonusMonth);
	}

	@Transactional
	public List<AdvancePaymentDO> retrieveByEmpIdBetweenDate(List<Long> id, String fromMonth, String toMonth) {
		return advancePaymentDAO.retrieveByEmpIdBetweenDate(id, fromMonth, toMonth);
	}*/

	@Transactional
	public AdvancePaymentDO update(AdvancePaymentDO AdvancePaymentDO) {
		return advancePaymentDAO.update(AdvancePaymentDO);
	}
	
	/*@Transactional
	public boolean delete(Long id) {
		return advancePaymentDAO.delete(id);
	}*/
}
