package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.AdvancePaymentDetailDAO;
import com.spheresuite.erp.domainobject.AdvancePaymentDetailDO;
@Service
@Transactional
public class AdvancePaymentDetailService {
	static Logger logger = Logger.getLogger(AdvancePaymentDetailService.class.getName());

	@Autowired
	private AdvancePaymentDetailDAO advancePaymentDetailDAO;
	
	@Transactional
	public AdvancePaymentDetailDO persist(AdvancePaymentDetailDO advancePaymentDetailDO) {
		return advancePaymentDetailDAO.persist(advancePaymentDetailDO);
	}
	
	@Transactional
	public List<AdvancePaymentDetailDO> getByAdvanceId(Long id) {
		return advancePaymentDetailDAO.getByAdvanceId(id);
	}
	
	@Transactional
	public List<AdvancePaymentDetailDO> getById(Long id) {
		return advancePaymentDetailDAO.getById(id);
	}
	
	@Transactional
	public List<AdvancePaymentDetailDO> getByMonthAndEmpId(String empId, String month) {
		return advancePaymentDetailDAO.getByMonthAndEmpId(empId, month);
	}
	
	@Transactional
	public List<AdvancePaymentDetailDO> getByStatusAndEmpId(String empId, String month) {
		return advancePaymentDetailDAO.getByStatusAndEmpId(empId, month);
	}
	
	
	@Transactional
	public AdvancePaymentDetailDO update(AdvancePaymentDetailDO advancePayment) {
		return advancePaymentDetailDAO.update(advancePayment);
	}
	
	/*@Transactional
	public List<AdvancePaymentDO> retrieveActiveByEmpId(String empId) {
		return advancePaymentDAO.retrieveActiveByEmpId(empId);
	}*/

	/*@Transactional
	public List<AdvancePaymentDO> retrieveByEmpIdAndMonth(String empId, String month) {
		return advancePaymentDAO.retrieveByEmpIdAndMonth(empId,month);
	}

	@Transactional
	public Long retrieveByEmpIdBetweenMonth(String empId, String fromMonth, String toMonth){
		return advancePaymentDAO.retrieveByEmpIdBetweenMonth(empId,fromMonth, toMonth);
	}*/

	/*@Transactional
	public List<AdvancePaymentDO> retrieve() {
		return advancePaymentDAO.retrieve();
	}

	@Transactional
	public List<AdvancePaymentDO> retrieveById(Long id) {
		return advancePaymentDAO.retrieveById(id);
	}*/

	/*@Transactional
	public List<AdvancePaymentDO> retrieveByEmpIdWithDate(List<Long> id, String bonusMonth) {
		return advancePaymentDAO.retrieveByEmpIdWithDate(id, bonusMonth);
	}

	@Transactional
	public List<AdvancePaymentDO> retrieveByEmpIdBetweenDate(List<Long> id, String fromMonth, String toMonth) {
		return advancePaymentDAO.retrieveByEmpIdBetweenDate(id, fromMonth, toMonth);
	}*/

	/*@Transactional
	public AdvancePaymentDO update(AdvancePaymentDO AdvancePaymentDO) {
		return advancePaymentDAO.update(AdvancePaymentDO);
	}*/
	
	/*@Transactional
	public boolean delete(Long id) {
		return advancePaymentDAO.delete(id);
	}*/
}
