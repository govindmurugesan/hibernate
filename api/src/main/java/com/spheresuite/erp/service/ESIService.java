package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.ESIDAO;
import com.spheresuite.erp.domainobject.ESIDO;

@Service
@Transactional
public class ESIService {
	static Logger logger = Logger.getLogger(ESIService.class.getName());
	
	@Autowired
	private ESIDAO esiDAO;
	
	@Transactional
	public boolean persist(ESIDO esiDO) {
		return esiDAO.persist(esiDO);
	}
	
	@Transactional
	public List<ESIDO> retrieveById(Long Id) {
		return esiDAO.retrieveById(Id);
	}
	
	@Transactional
	public List<ESIDO> retrieveActive() {
		return esiDAO.retrieveActive();
	}
	
	@Transactional
	public List<ESIDO> retrieve() {
		return esiDAO.retrieve();
	}
	
	/*@Transactional
	public List<EducationCessDO> retrieveByCurrentDate(Date currentDate) {
		return educationCessDAO.retrieveByCurrentDate(currentDate);
	}*/
	
	@Transactional
	public boolean persistList(List<ESIDO> esiDO) {
		return esiDAO.persistList(esiDO);
	}
	
	@Transactional
	public boolean update(ESIDO esiDO) {
		return esiDAO.update(esiDO);
	}
}
