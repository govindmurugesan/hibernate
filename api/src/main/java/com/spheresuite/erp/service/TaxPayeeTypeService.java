package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.TaxPayeeTypeDAO;
import com.spheresuite.erp.domainobject.TaxPayeeTypeDO;

@Service
@Transactional
public class TaxPayeeTypeService {
	static Logger logger = Logger.getLogger(TaxPayeeTypeService.class.getName());
	
	@Autowired
	private TaxPayeeTypeDAO taxPayeeTypeDAO;
	
	@Transactional
	public boolean persist(TaxPayeeTypeDO taxPayeeType) {
		return taxPayeeTypeDAO.persist(taxPayeeType);
	}

	@Transactional
	public List<TaxPayeeTypeDO> retrieve() {
		return taxPayeeTypeDAO.retrieve();
	}
	
	@Transactional
	public List<TaxPayeeTypeDO> retrieveById(Long Id) {
		return taxPayeeTypeDAO.retrieveById(Id);
	}
	
	@Transactional
	public List<TaxPayeeTypeDO> retrieveActive(char status) {
		return taxPayeeTypeDAO.retrieveActive(status);
	}
	
	@Transactional
	public boolean update(TaxPayeeTypeDO taxPayeeType) {
		return taxPayeeTypeDAO.update(taxPayeeType);
	}
}
