package com.spheresuite.erp.service;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.ItSlabEffectiveDAO;
import com.spheresuite.erp.domainobject.ItSlabEffectiveDO;
@Service
@Transactional
public class ItSlabEffectiveService {
	static Logger logger = Logger.getLogger(ItSlabEffectiveService.class.getName());
	@Autowired
	private ItSlabEffectiveDAO itSlabEffectiveDAO;
	
	@Transactional
	public boolean persist(ItSlabEffectiveDO itSavingsDO) {
		return itSlabEffectiveDAO.persist(itSavingsDO);
	}
/*
	@Transactional
	public List<ItSlabEffectiveDO> retrieveActive() {
		return itSlabEffectiveDAO.retrieveActive();
	}*/

	@Transactional
	public List<ItSlabEffectiveDO> retrieveById(Long id) {
		return itSlabEffectiveDAO.retrieveById(id);
	}

	@Transactional
	public List<ItSlabEffectiveDO> retrieve() {
		return itSlabEffectiveDAO.retrieve();
	}

	@Transactional
	public List<ItSlabEffectiveDO> retrieveByDate(Date fromDate, Date toDate) {
		return itSlabEffectiveDAO.retrieveByDate(fromDate, toDate);
	}
	
	/*@Transactional
	public List<ItSlabEffectiveDO> retrieveByCurrentDate(Date date) {
		return itSlabEffectiveDAO.retrieveByCurrentDate(date);
	}*/
	
	@Transactional
	public boolean update(ItSlabEffectiveDO itSavingsDO) {
		return itSlabEffectiveDAO.update(itSavingsDO);
	}
	
}
