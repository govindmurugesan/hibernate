package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.CategoryDAO;
import com.spheresuite.erp.domainobject.CategoryDO;
@Service
@Transactional
public class CategoryService {
	static Logger logger = Logger.getLogger(CategoryService.class.getName());
	
	@Autowired
	private CategoryDAO categoryDAO;
	
	@Transactional
	public boolean persist(CategoryDO countryDO)  {
		return categoryDAO.persist(countryDO);
	}

	@Transactional
	public List<CategoryDO> retrieveActive()  {
		return categoryDAO.retrieveActive();
	}
	
	@Transactional
	public List<CategoryDO> retrieveById(Long id)  {
		return categoryDAO.retrieveById(id);
	}
	
	@Transactional
	public List<CategoryDO> retrieve()  {
		return categoryDAO.retrieve();
	}
	
	@Transactional
	public boolean update(CategoryDO countryDO)  {
		return categoryDAO.update(countryDO);
	}
	
	@Transactional
	public boolean persistList(List<CategoryDO> categoryDO) {
		return categoryDAO.persistList(categoryDO);
	}
}
