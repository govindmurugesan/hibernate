package com.spheresuite.erp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.InsuranceTypeDAO;
import com.spheresuite.erp.domainobject.InsuranceTypeDO;

@Service
@Transactional
public class InsuranceTypeService{
	
	@Autowired
	private InsuranceTypeDAO insuranceTypeDAO;

	@Transactional
	public boolean persist(InsuranceTypeDO insuranceTypeDO) {
		return insuranceTypeDAO.persist(insuranceTypeDO);
	}
	
	@Transactional
	public List<InsuranceTypeDO> retrieve() {
		return insuranceTypeDAO.retrieve();
	}

	@Transactional
	public boolean update(InsuranceTypeDO insuranceTypeDO) {
		return insuranceTypeDAO.update(insuranceTypeDO);
	}
	
	/*@Transactional
	public List<InsuranceTypeDO> retrieveActive(){
		return advanceTypeDAO.retrieveActive();
	}
	*/
	@Transactional
	public List<InsuranceTypeDO> retrieveById(Long id){
		return insuranceTypeDAO.retrieveById(id);
	}
	
	/*@Transactional
	public boolean persistList(List<InsuranceTypeDO> InsuranceTypeDO) {
		return advanceTypeDAO.persistList(InsuranceTypeDO);
	}*/
}
