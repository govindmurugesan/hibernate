package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.HolidaysDAO;
import com.spheresuite.erp.domainobject.HolidaysDO;
@Service
@Transactional
public class HolidaysService {
	static Logger logger = Logger.getLogger(HolidaysService.class.getName());
	@Autowired
	private HolidaysDAO holidaysDAO;

	@Transactional
	public HolidaysDO persist(HolidaysDO holidayDetail) {
		return holidaysDAO.persist(holidayDetail);
	}

	@Transactional
	public List<HolidaysDO> retrieveById(Long Id) {
		return holidaysDAO.retrieveById(Id);
	}

	@Transactional
	public List<HolidaysDO> retrieveByHoliday(String year, String date) {
		return holidaysDAO.retrieveByHoliday( year, date);
	}

	@Transactional
	public List<HolidaysDO> retrieveByHolidayForUpdate(Long id, String year, String date) {
		return holidaysDAO.retrieveByHolidayForUpdate(id, year, date);
	}

	@Transactional
	public List<HolidaysDO> retrieve() {
		return holidaysDAO.retrieve();
	}

	@Transactional
	public HolidaysDO update(HolidaysDO holidayDetail) {
		return holidaysDAO.update(holidayDetail);
	}
	
	
	@Transactional
	public List<HolidaysDO> retrieveByDate(String year, String date) {
		return holidaysDAO.retrieveByDate(year, date);
	}
	
	/*@Transactional
	public List<HolidaysDO> retrieveByDateAndWorkLocation(String year, String date, Long id) {
		return holidaysDAO.retrieveByDateAndWorkLocation(year, date, id);
	}*/
	
	

}
