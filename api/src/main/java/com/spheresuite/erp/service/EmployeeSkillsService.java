package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.EmployeeSkillsDAO;
import com.spheresuite.erp.domainobject.EmployeeSkillsDO;
@Service
@Transactional
public class EmployeeSkillsService {
	static Logger logger = Logger.getLogger(EmployeeSkillsService.class.getName());
	@Autowired
	private EmployeeSkillsDAO employeeSkillsDAO;

	@Transactional
	public EmployeeSkillsDO persist(EmployeeSkillsDO employeeSkillsDO) {
		return employeeSkillsDAO.persist(employeeSkillsDO);
	}

	@Transactional
	public List<EmployeeSkillsDO> retrieveByEmpId(String empId) {
		return employeeSkillsDAO.retrieveByEmpId(empId);
	}

	@Transactional
	public List<EmployeeSkillsDO> retrieveById(Long id) {
		return employeeSkillsDAO.retrieveById(id);
	}

	@Transactional
	public EmployeeSkillsDO update(EmployeeSkillsDO employeeSkillsDO) {
		return employeeSkillsDAO.update(employeeSkillsDO);
	}
}
