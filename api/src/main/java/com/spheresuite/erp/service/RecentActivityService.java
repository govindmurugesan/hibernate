package com.spheresuite.erp.service;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.RecentActivityDAO;
import com.spheresuite.erp.domainobject.RecentActivityDO;

@Service
@Transactional
public class RecentActivityService {
	static Logger logger = Logger.getLogger(RecentActivityService.class.getName());
	
	@Autowired
	private RecentActivityDAO recentActivityDAO;
	
	@Transactional
	public RecentActivityDO persist(RecentActivityDO recentActivityDO) {
		return recentActivityDAO.persist(recentActivityDO);
	}

	@Transactional
	public List<RecentActivityDO> retrieveByEmpId(String id) {
		return recentActivityDAO.retrieveByEmpId(id);
	}
	
	@Transactional
	public List<RecentActivityDO> retrieveByEmp(String id) {
		return recentActivityDAO.retrieveByEmp(id);
	}
	
	@Transactional
	public List<RecentActivityDO> retrieveByEmpDate(String id, Date date) {
		return recentActivityDAO.retrieveByEmpDate(id, date);
	}
	
	@Transactional
	public List<RecentActivityDO> retrieve() {
		return recentActivityDAO.retrieve();
	}
	
	@Transactional
	public RecentActivityDO update(RecentActivityDO recentActivityDO) {
		return recentActivityDAO.update(recentActivityDO);
	}
}
