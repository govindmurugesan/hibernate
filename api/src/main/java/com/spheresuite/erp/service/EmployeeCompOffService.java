package com.spheresuite.erp.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.EmployeeCompOffDAO;
import com.spheresuite.erp.domainobject.EmployeeCompOffDO;

@Service
@Transactional
public class EmployeeCompOffService {

	@Autowired
	private EmployeeCompOffDAO employeeCompOffDAO;
	
	@Transactional
	public boolean persist(EmployeeCompOffDO employeeCompOffDO){
		return employeeCompOffDAO.persist(employeeCompOffDO);
	}
	
	@Transactional
	public boolean persist(List<EmployeeCompOffDO> employeeCompOffDO){
		return employeeCompOffDAO.persist(employeeCompOffDO);
	}

	@Transactional
	public List<EmployeeCompOffDO> retrieveByEmpId(String empId){
		return employeeCompOffDAO.retrieveByEmpId(empId);
	}
	
	
	@Transactional
	public List<EmployeeCompOffDO> retrieveByReportingId(String empId){
		return employeeCompOffDAO.retrieveByReportingId(empId);
	}
	
	@Transactional
	public List<EmployeeCompOffDO> retrieveById(Long id){
		return employeeCompOffDAO.retrieveById(id);
	}
	
	@Transactional
	public List<EmployeeCompOffDO> retrieve(){
		return employeeCompOffDAO.retrieve();
	}
	
	@Transactional
	public boolean update(EmployeeCompOffDO employeeCompOffDO){
		return employeeCompOffDAO.update(employeeCompOffDO);
	}
	
	public List<EmployeeCompOffDO> retrieveByEmpIdValidCompOff(String empId, Date date) {
		return employeeCompOffDAO.retrieveByEmpIdValidCompOff(empId, date);
	}
	
	public List<EmployeeCompOffDO> retrieveByEmpIdValidCompOffWithType(String empId, Date date, char type) {
		return employeeCompOffDAO.retrieveByEmpIdValidCompOffWithType(empId, date, type);
	}
	
	public List<EmployeeCompOffDO> retrieveByEmpCompOffWithTypeExpiry(String empId, Date date, char type) {
		return employeeCompOffDAO.retrieveByEmpCompOffWithTypeExpiry(empId, date, type);
	}
	
	
}
