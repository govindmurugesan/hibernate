package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.EmployeeDeductionDAO;
import com.spheresuite.erp.domainobject.EmployeeDeductionDO;
@Service
@Transactional
public class EmployeeDeductionService {
	static Logger logger = Logger.getLogger(EmployeeDeductionService.class.getName());
	@Autowired
	private EmployeeDeductionDAO employeeDeductionDAO;
	
	@Transactional
	public List<EmployeeDeductionDO> persist(List<EmployeeDeductionDO> employeeDeductionDO) {
		return employeeDeductionDAO.persist(employeeDeductionDO);
	}

	@Transactional
	public List<EmployeeDeductionDO> retrieveByEmpCompensationId(Long id) {
		return employeeDeductionDAO.retrieveByEmpCompensationId(id);
	}

	@Transactional
	public List<EmployeeDeductionDO> retrieve() {
		return employeeDeductionDAO.retrieve();
	}

	@Transactional
	public EmployeeDeductionDO update(EmployeeDeductionDO employeeDeductionDO) {
		return employeeDeductionDAO.update(employeeDeductionDO);
	}
}
