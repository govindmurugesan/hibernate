package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.EmployeeAddressDAO;
import com.spheresuite.erp.domainobject.EmployeeAddressDO;
@Service
@Transactional
public class EmployeeAddressService {
	static Logger logger = Logger.getLogger(EmployeeAddressService.class.getName());

	@Autowired
	private EmployeeAddressDAO employeeAddressDAO;
	
	@Transactional
	public EmployeeAddressDO persist(EmployeeAddressDO employeeAddressDO)  {
		return employeeAddressDAO.persist(employeeAddressDO);
	}

	@Transactional
	public List<EmployeeAddressDO> retrieveByEmpId(String empId)  {
		return employeeAddressDAO.retrieveByEmpId(empId);
	}

	@Transactional
	public List<EmployeeAddressDO> retrieveById(Long id)  {
		return employeeAddressDAO.retrieveById(id);
	}

	@Transactional
	public EmployeeAddressDO update(EmployeeAddressDO employeeAddressDO)  {
		return employeeAddressDAO.update(employeeAddressDO);
	}
}
