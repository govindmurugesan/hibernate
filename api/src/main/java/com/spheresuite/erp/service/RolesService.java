package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.RolesDAO;
import com.spheresuite.erp.domainobject.RolesDO;

@Service
@Transactional
public class RolesService {
	static Logger logger = Logger.getLogger(RolesService.class.getName());
	
	@Autowired
	private RolesDAO rolesDao;
	
	@Transactional
	public Boolean persist(RolesDO rolesDO) {
		return rolesDao.persist(rolesDO);
	}

	@Transactional
	public List<RolesDO> retrieve() {
		return rolesDao.retrieve();
	}

	@Transactional
	public List<RolesDO> retriveById(long id) {
		return rolesDao.retriveById(id);
	}

	@Transactional
	public boolean delete(RolesDO rolesDO) {
		return rolesDao.delete(rolesDO);
	}

	@Transactional
	public Boolean update(RolesDO rolesDO) {
		return rolesDao.update(rolesDO);
	}
	
	@Transactional
	public List<RolesDO> retriveByName(String name) {
		return rolesDao.retriveByName(name);
	}

}
