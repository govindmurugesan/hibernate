package com.spheresuite.erp.service;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.LeaveRequestsDAO;
import com.spheresuite.erp.domainobject.LeaveRequestsDO;
@Service
@Transactional
public class LeaveRequestsService {
	static Logger logger = Logger.getLogger(LeaveRequestsService.class.getName());
	@Autowired
	private LeaveRequestsDAO leaveRequestsDAO;

	@Transactional
	public LeaveRequestsDO persist(LeaveRequestsDO leaveRequestDetail) {
		return leaveRequestsDAO.persist(leaveRequestDetail);
	}

	@Transactional
	public List<LeaveRequestsDO> retrieve() {
		return leaveRequestsDAO.retrieve();
	}

	@Transactional
	public List<LeaveRequestsDO> retrieveAllPermissionForEmpForMonth(String empId, int year, int month) {
		return leaveRequestsDAO.retrieveAllPermissionForEmpForMonth(empId, year, month);
	}
	
	@Transactional
	public List<LeaveRequestsDO> retrieveById(Long Id) {
		return leaveRequestsDAO.retrieveById(Id);
	}
	
	@Transactional
	public List<LeaveRequestsDO> retrieveByReportingTo(String Id) {
		return leaveRequestsDAO.retrieveByReportingTo(Id);
	}

	@Transactional
	public LeaveRequestsDO update(LeaveRequestsDO leaveRequest) {
		return leaveRequestsDAO.update(leaveRequest);
	}

	@Transactional
	public List<LeaveRequestsDO> retriveByEmpId(String id) {
		return leaveRequestsDAO.retrieveByEmpId(id);
	}
	
	@Transactional
	public List<LeaveRequestsDO> retriveByEmpIdStatus(String id) {
		return leaveRequestsDAO.retriveByEmpIdStatus(id);
	}
	
	@Transactional
	public List<LeaveRequestsDO> retriveByEmpIdStatusType(String id,Long type) {
		return leaveRequestsDAO.retriveByEmpIdStatusType(id,type);
	}
	

	@Transactional
	public List<LeaveRequestsDO> retrieveAvailableLeavesByEmpId(String id, Date leaveTypeFromDate, Date leaveTypeToDate, Long type) {
		return leaveRequestsDAO.retrieveAvailableLeavesByEmpId(id, leaveTypeFromDate, leaveTypeToDate, type);
	}
	
	@Transactional
	public List<LeaveRequestsDO> retrieveLeavesByYear(String empId, Date fromDate, Date toDate,Long leaveTypeId) {
		return leaveRequestsDAO.retrieveLeavesByYear(empId,fromDate, toDate, leaveTypeId);
	}
	
	@Transactional
	public List<LeaveRequestsDO> retrieveLeavesByFromDate(String empId, Date fromDate) {
		return leaveRequestsDAO.retrieveLeavesByFromDate(empId,fromDate);
	}
	
	@Transactional
	public List<LeaveRequestsDO> retrieveLeavesByFromDateWithType(String empId, Date fromDate, Long type) {
		return leaveRequestsDAO.retrieveLeavesByFromDateWithType(empId,fromDate, type);
	}
	
	@Transactional
	public List<LeaveRequestsDO> retrieveLeavesByFromToDateType(String empId, Date fromDate, Date toDate, Long type) {
		return leaveRequestsDAO.retrieveLeavesByFromToDateType(empId,fromDate,toDate,type);
	}
	
	@Transactional
	public List<LeaveRequestsDO> retrieveLeavesByFromToDate(String empId, Date fromDate, Date toDate) {
		return leaveRequestsDAO.retrieveLeavesByFromToDate(empId,fromDate,toDate);
	}
	
	
	
	@Transactional
	public List<LeaveRequestsDO> retrieveLeavesForPayroll(String empId, Date fromDate, Date toDate) {
		return leaveRequestsDAO.retrieveLeavesForPayroll(empId,fromDate,toDate);
	}
	
	@Transactional
	public List<LeaveRequestsDO> retrieveReportByEmpId(String empId, Long leaveTypeId) {
		return leaveRequestsDAO.retrieveReportByEmpId(empId, leaveTypeId);
	}
	
	@Transactional
	public List<LeaveRequestsDO> retrievePermissionByYear(String empId, Date fromDate, Date toDate,String leaveTypeId) {
		return leaveRequestsDAO.retrievePermissionByYear(empId,fromDate, toDate, leaveTypeId);
	}
	
	
	
	@Transactional
	public List<LeaveRequestsDO> retrievePermissionByMonth(String empId, Date fromDate, Date toDate,String leaveTypeId) {
		return leaveRequestsDAO.retrievePermissionByMonth(empId,fromDate, toDate, leaveTypeId);
	}
	
	@Transactional
	public List<LeaveRequestsDO> retrievePermissionByEmpID(String empId, String leaveTypeId) {
		return leaveRequestsDAO.retrievePermissionByEmpID(empId,leaveTypeId);
	}
	
	@Transactional
	public List<LeaveRequestsDO> retrivebyEmpLeaveperiod(String empId, Date fromDate, Date toDate,Date leaveTypeFrom, Date leaveTypeTo, long grpId, long leaveType) {
		return leaveRequestsDAO.retrivebyEmpLeaveperiod(empId,fromDate, toDate, leaveTypeFrom,leaveTypeTo,grpId,leaveType);
	}
	
	@Transactional
	public List<LeaveRequestsDO> empCompoffValidity(String id, Date leaveFromDate, Date leaveToDate, String compOffType) {
		return leaveRequestsDAO.empCompoffValidity(id, leaveFromDate, leaveToDate, compOffType);
	}
	
	@Transactional
	public List<LeaveRequestsDO> empCompoffValidityWithoutExpiry(String id, Date leaveFromDate,String compOffType) {
		return leaveRequestsDAO.empCompoffValidityWithoutExpiry(id, leaveFromDate, compOffType);
	}
	
	@Transactional
	public boolean deleteLeaveRequestByEmpId(Date fromDate, Date toDate, String empId) {
		return leaveRequestsDAO.deleteLeaveRequestByEmpId(fromDate, toDate, empId);
	}
	/*public List<LeaveRequestsDO> retrieveActive() {
		return leaveRequestsDAO.retrieveActive();
	}
	*/
}
