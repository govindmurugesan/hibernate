package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.EmployeeOnboardDAO;
import com.spheresuite.erp.domainobject.EmployeeOnboardDO;
@Service
@Transactional
public class EmployeeOnboardService {
	static Logger logger = Logger.getLogger(EmployeeOnboardService.class.getName());
	@Autowired
	private EmployeeOnboardDAO employeeOnboardDAO;

	@Transactional
	public Boolean persist(EmployeeOnboardDO employeeOnboardDO) {
		return employeeOnboardDAO.persist(employeeOnboardDO);
	}
	
	@Transactional
	public boolean delete(Long id) {
		return employeeOnboardDAO.delete(id);
	}

	@Transactional
	public List<EmployeeOnboardDO> retriveById(Long id) {
		return employeeOnboardDAO.retrieveById(id);
	}

	@Transactional
	public List<EmployeeOnboardDO> retrieve() {
		return employeeOnboardDAO.retrieve();
	}

	@Transactional
	public List<EmployeeOnboardDO> retrieveByEmail(String personalEmail) {
		return employeeOnboardDAO.retrieveByEmail(personalEmail);
	}
	
	/*@Transactional
	public List<EmployeeOnboardDO> retrieveByEmailWithoutStatus(String personalEmail) {
		return employeeOnboardDAO.retrieveByEmailWithoutStatus(personalEmail);
	}*/
	
	
	@Transactional
	public Boolean update(EmployeeOnboardDO employeeOnboardDO) {
		return employeeOnboardDAO.update(employeeOnboardDO);
	}

}
