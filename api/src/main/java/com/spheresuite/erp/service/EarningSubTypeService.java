package com.spheresuite.erp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.EarningSubTypeDAO;
import com.spheresuite.erp.domainobject.EarningSubTypeDO;

@Service
@Transactional
public class EarningSubTypeService {

	@Autowired
	private EarningSubTypeDAO earningSubTypeDAO;
	
	@Transactional
	public boolean persist(EarningSubTypeDO EarningSubTypeDO){
		return earningSubTypeDAO.persist(EarningSubTypeDO);
	}

	@Transactional
	public List<EarningSubTypeDO> retrieveActive(){
		return earningSubTypeDAO.retrieveActive();
	}
	
	@Transactional
	public List<EarningSubTypeDO> retrieveById(Long id){
		return earningSubTypeDAO.retrieveById(id);
	}
	
	@Transactional
	public List<EarningSubTypeDO> retrieveByName(String name){
		return earningSubTypeDAO.retrieveByName(name);
	}
	
	@Transactional
	public List<EarningSubTypeDO> retrieveByEarningId(Long id){
		return earningSubTypeDAO.retrieveByEarningId(id);
	}
	
	
	@Transactional
	public List<EarningSubTypeDO> retrieve(){
		return earningSubTypeDAO.retrieve();
	}
	
	@Transactional
	public boolean update(EarningSubTypeDO EarningSubTypeDO){
		return earningSubTypeDAO.update(EarningSubTypeDO);
	}
	
	@Transactional
	public boolean persistList(List<EarningSubTypeDO> EarningSubTypeDO) {
		return earningSubTypeDAO.persistList(EarningSubTypeDO);
	}
}
