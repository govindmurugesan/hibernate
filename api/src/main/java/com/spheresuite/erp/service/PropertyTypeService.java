package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.PropertyTypeDAO;
import com.spheresuite.erp.domainobject.PropertyTypeDO;

@Service
@Transactional
public class PropertyTypeService {
	static Logger logger = Logger.getLogger(PropertyTypeService.class.getName());
	
	@Autowired
	private PropertyTypeDAO propertyTypeDAO;
	
	@Transactional
	public PropertyTypeDO persist(PropertyTypeDO propertyTypeDO) {
		return propertyTypeDAO.persist(propertyTypeDO);
	}

	@Transactional
	public List<PropertyTypeDO> retrieveActive() {
		return propertyTypeDAO.retrieveActive();
	}
	
	@Transactional
	public List<PropertyTypeDO> retrieveById(Long id) {
		return propertyTypeDAO.retrieveById(id);
	}
	
	@Transactional
	public List<PropertyTypeDO> retrieve() {
		return propertyTypeDAO.retrieve();
	}
	
	@Transactional
	public PropertyTypeDO update(PropertyTypeDO propertyTypeDO) {
		return propertyTypeDAO.update(propertyTypeDO);
	}
}
