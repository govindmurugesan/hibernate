package com.spheresuite.erp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.ShiftDAO;
import com.spheresuite.erp.domainobject.ShiftDO;

@Service
@Transactional
public class ShiftService{
	
	@Autowired
	private ShiftDAO shiftDAO;

	@Transactional
	public boolean persist(ShiftDO ShiftDO) {
		return shiftDAO.persist(ShiftDO);
	}
	
	@Transactional
	public List<ShiftDO> retrieve() {
		return shiftDAO.retrieve();
	}

	@Transactional
	public boolean update(ShiftDO ShiftDO) {
		return shiftDAO.update(ShiftDO);
	}
	
	@Transactional
	public List<ShiftDO> retrieveActive(){
		return shiftDAO.retrieveActive();
	}
	
	@Transactional
	public List<ShiftDO> retrieveById(Long id){
		return shiftDAO.retrieveById(id);
	}
	
	@Transactional
	public List<ShiftDO> retrieveByName(String name){
		return shiftDAO.retrieveByName(name);
	}
	
	@Transactional
	public boolean persistList(List<ShiftDO> ShiftDO) {
		return shiftDAO.persistList(ShiftDO);
	}
}
