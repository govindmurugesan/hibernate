package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.EmployeeDAO;
import com.spheresuite.erp.domainobject.EmployeeDO;
@Service
@Transactional
public class EmployeeService {
	static Logger logger = Logger.getLogger(EmployeeService.class.getName());
	@Autowired
	private EmployeeDAO employeeDAO;
	
	@Transactional
	public Boolean persist(EmployeeDO employeeDO) {
		return employeeDAO.persist(employeeDO);
	}

	/*@Transactional
	public List<EmployeeDO> retriveById(String id) {
		return employeeDAO.retrieveById(id);
	}
*/
	@Transactional
	public List<EmployeeDO> retrieve() {
		return employeeDAO.retrieve();
	}
	
	@Transactional
	public List<Object []> retrieveAll() {
		return employeeDAO.retrieveAll();
	}
	
	@Transactional
	public List<Object []> retrieveNameId() {
		return employeeDAO.retrieveNameId();
	}
	
	@Transactional
	public List<Object []> retrieveByUnit(Long unitId) {
		return employeeDAO.retrieveByUnit(unitId);
	}
	
	@Transactional
	public List<EmployeeDO> retrieveForId() {
		return employeeDAO.retrieveForId();
	}
	
	@Transactional
	public List<EmployeeDO> retrieveReportTo(String empId) {
		return employeeDAO.retrieveReportTo(empId);
	}

	@Transactional
	public List<EmployeeDO> retrieveActiveNoCtc(List<String> empIds) {
		return employeeDAO.retrieveActiveNoCtc(empIds);
	}

	@Transactional
	public List<EmployeeDO> retrieveActive() {
		return employeeDAO.retrieveActive();
	}

	@Transactional
	public List<EmployeeDO> isEmpActive(String id) {
		return employeeDAO.isEmpActive(id);
	}

	@Transactional
	public List<EmployeeDO> retrieveByEmail(String email) {
		return employeeDAO.retrieveByEmail(email);
	}

	@Transactional
	public Boolean update(EmployeeDO employeeDO) {
		return employeeDAO.update(employeeDO);
	}

	@Transactional
	public List<EmployeeDO> retriveByEmpId(String id) {
		return employeeDAO.retrieveByEmpId(id);
	}
	
	@Transactional
	public List<EmployeeDO> retrieveEmpId(String id) {
		return employeeDAO.retrieveByEmpId(id);
	}
	
	@Transactional
	public List<Object[]> retrieveByEmpIdOnlyNames(String id) {
		return employeeDAO.retrieveByEmpIdOnlyNames(id);
	}
	
	@Transactional
	public String checkDuplicate(String empId, String companyEmail){
		return employeeDAO.checkDuplicate(empId, companyEmail);
	}

	@Transactional
	public String checkDuplicateUpdate(String empId, String companyEmail, Long Id){
		return employeeDAO.checkDuplicateUpdate(empId, companyEmail, Id);
	}

	@Transactional
	public List<String> retrieveByDeptIds(List<Long> id) {
		return employeeDAO.retrieveByDeptIds(id);
	}

	@Transactional
	public boolean persistList(List<EmployeeDO> empDO) {
		return employeeDAO.persistList(empDO);
	}
	
	/*@Transactional
	public List<EmployeeDO> retrieveEmpName(String fname,String mname,String lname) {
		return employeeDAO.retrieveEmpName(fname,mname,lname);
	}*/

}
