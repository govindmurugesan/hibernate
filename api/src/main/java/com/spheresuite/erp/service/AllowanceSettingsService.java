package com.spheresuite.erp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.AllowanceSettingsDAO;
import com.spheresuite.erp.domainobject.AllowanceSettingDO;

@Service
@Transactional
public class AllowanceSettingsService{

	@Autowired
	private AllowanceSettingsDAO allowanceSettingsDAO;
	
	@Transactional
	public void persist(AllowanceSettingDO allowanceSettingDO) {
		allowanceSettingsDAO.persist(allowanceSettingDO);
	}
	
	@Transactional
	public void update(AllowanceSettingDO allowanceSettingDO) {
		allowanceSettingsDAO.update(allowanceSettingDO);
	}
	
	@Transactional
	public List<AllowanceSettingDO> retrieveById(Long id){
		return allowanceSettingsDAO.retrieveById(id);
	}
	
	@Transactional
	public List<AllowanceSettingDO> retrieveActive(String name){
		return allowanceSettingsDAO.retrieveActive(name);
	}

	@Transactional
	public List<AllowanceSettingDO> retrieve() {
		return allowanceSettingsDAO.retrieve();
	}
}
