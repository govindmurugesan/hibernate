package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.PayrollDeductionDetailsDAO;
import com.spheresuite.erp.domainobject.PayrollDeductionDetailsDO;

@Service
@Transactional
public class PayrollDeductionDetailsService {
	static Logger logger = Logger.getLogger(PayrollDeductionDetailsService.class.getName());
	
	@Autowired
	private PayrollDeductionDetailsDAO payrollDeductionDetailsDAO;
	
	@Transactional
	public List<PayrollDeductionDetailsDO> persist(List<PayrollDeductionDetailsDO> payrollDeductionDetailsDO) {
		return payrollDeductionDetailsDAO.persist(payrollDeductionDetailsDO);
	}

	@Transactional
	public List<PayrollDeductionDetailsDO> retrieveByEmpCompensationId(Long id) {
		return payrollDeductionDetailsDAO.retrieveByEmpCompensationId(id);
	}
	
	@Transactional
	public List<PayrollDeductionDetailsDO> retrieve() {
		return payrollDeductionDetailsDAO.retrieve();
	}
	
	@Transactional
	public PayrollDeductionDetailsDO update(PayrollDeductionDetailsDO payrollDeductionDetailsDO) {
		return payrollDeductionDetailsDAO.update(payrollDeductionDetailsDO);
	}
	
	@Transactional
	public List<PayrollDeductionDetailsDO> retrieveByEmpCompensationIds(List<Long> id) {
		return payrollDeductionDetailsDAO.retrieveByEmpCompensationIds(id);
	}
	
	@Transactional
	public Double retrieveAllDeduction(Long id) {
		return payrollDeductionDetailsDAO.retrieveAllDeduction(id);
	}
	
	@Transactional
	public boolean deletePayrollById(Long payrollId) {
		return payrollDeductionDetailsDAO.deletePayrollById(payrollId);
	}
}
