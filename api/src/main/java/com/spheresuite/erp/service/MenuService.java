package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.MenusDAO;
import com.spheresuite.erp.domainobject.MenuDO;
@Service
@Transactional
public class MenuService {
	static Logger logger = Logger.getLogger(MenuService.class.getName());
	@Autowired
	private MenusDAO menusDAO;

	@Transactional
	public List<MenuDO> retrieveMainMenu() {
		return menusDAO.retrieveMainMenu();
	}
	
	@Transactional
	public List<MenuDO> retrieveSuperMenu(Long menuId) {
		return menusDAO.retrieveSuperMenu(menuId);
	}
	
	@Transactional
	public List<MenuDO> retrieveByProductId(Long productId) {
		return menusDAO.retrieveByProductId(productId);
	}
	
	
	@Transactional
	public List<MenuDO> retrieveById(Long id) {
		return menusDAO.retrieveById(id);
	}
}
