package com.spheresuite.erp.service;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.EducationCessDAO;
import com.spheresuite.erp.domainobject.EducationCessDO;

@Service
@Transactional
public class EducationCessService {
	static Logger logger = Logger.getLogger(EducationCessService.class.getName());
	
	@Autowired
	private EducationCessDAO educationCessDAO;
	
	@Transactional
	public boolean persist(EducationCessDO educationCessDO) {
		return educationCessDAO.persist(educationCessDO);
	}
	
	@Transactional
	public List<EducationCessDO> retrieveById(Long Id) {
		return educationCessDAO.retrieveById(Id);
	}
	
	@Transactional
	public List<EducationCessDO> retrieve() {
		return educationCessDAO.retrieve();
	}
	
	@Transactional
	public List<EducationCessDO> retrieveByCurrentDate(Date currentDate) {
		return educationCessDAO.retrieveByCurrentDate(currentDate);
	}
	
	@Transactional
	public boolean persistList(List<EducationCessDO> educationCessDO) {
		return educationCessDAO.persistList(educationCessDO);
	}
	
	@Transactional
	public boolean update(EducationCessDO ptDO) {
		return educationCessDAO.update(ptDO);
	}
}
