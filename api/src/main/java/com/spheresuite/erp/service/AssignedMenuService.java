package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.AssignedMenuDAO;
import com.spheresuite.erp.domainobject.AssignedMenuDO;

@Service
@Transactional
public class AssignedMenuService {
	static Logger logger = Logger.getLogger(AssignedMenuService.class.getName());
	
	@Autowired
	private AssignedMenuDAO assignedMenuDAO;
	
	@Transactional
	public List<AssignedMenuDO> persist(List<AssignedMenuDO> assignedMenuDO){
		return assignedMenuDAO.update(assignedMenuDO);
	}

	@Transactional
	public List<AssignedMenuDO> retrieveByRoleId(Long id){
		return assignedMenuDAO.retrieveByRoleId(id);
	}
	
	@Transactional
	public List<AssignedMenuDO> retrieveProductByRoleId(Long id){
		return assignedMenuDAO.retrieveProductByRoleId(id);
	}
	
	@Transactional
	public List<AssignedMenuDO> retrieveMenuByRoleId(Long roleId, Long productId){
		return assignedMenuDAO.retrieveMenuByRoleId(roleId, productId);
	}
	
	@Transactional
	public List<AssignedMenuDO> retrieveMenuBySupermenuRoleId(Long roleId, Long productId){
		return assignedMenuDAO.retrieveMenuBySupermenuRoleId(roleId, productId);
	}
	
	
	
	
	/*@Transactional
	public List<AssignedMenuDO> update(List<AssignedMenuDO> assignedMenuDO){
		return assignedMenuDAO.update(assignedMenuDO);
	}*/
}
