package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.HrRequestTypeDAO;
import com.spheresuite.erp.domainobject.HrRequestTypeDO;
@Service
@Transactional
public class HrRequestTypeService {
	static Logger logger = Logger.getLogger(HrRequestTypeService.class.getName());
	@Autowired
	private HrRequestTypeDAO hrRequestTypeDAO;
	
	@Transactional
	public boolean persist(HrRequestTypeDO requestType) {
		return hrRequestTypeDAO.persist(requestType);
	}

	@Transactional
	public List<HrRequestTypeDO> retrieve() {
		return hrRequestTypeDAO.retrieve();
	}

	@Transactional
	public List<HrRequestTypeDO> retrieveById(Long Id) {
		return hrRequestTypeDAO.retrieveById(Id);
	}

	@Transactional
	public boolean update(HrRequestTypeDO requestType) {
		return hrRequestTypeDAO.update(requestType);
	}
	
	@Transactional
	public boolean persistList(List<HrRequestTypeDO> hrRequestTypeDO) {
		return hrRequestTypeDAO.persistList(hrRequestTypeDO);
	}
}
