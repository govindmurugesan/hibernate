package com.spheresuite.erp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.AttendanceAllowanceDAO;
import com.spheresuite.erp.domainobject.AttendanceAllowanceDO;

@Service
@Transactional
public class AttendanceAllowanceService{
	
	@Autowired
	private AttendanceAllowanceDAO attendanceAllowanceDAO;

	@Transactional
	public boolean persist(AttendanceAllowanceDO AttendanceAllowanceDO) {
		return attendanceAllowanceDAO.persist(AttendanceAllowanceDO);
	}
	
	@Transactional
	public List<AttendanceAllowanceDO> retrieve() {
		return attendanceAllowanceDAO.retrieve();
	}

	@Transactional
	public boolean update(AttendanceAllowanceDO AttendanceAllowanceDO) {
		return attendanceAllowanceDAO.update(AttendanceAllowanceDO);
	}
	
	/*@Transactional
	public List<AttendanceAllowanceDO> retrieveActive(){
		return attendanceAllowanceDAO.retrieveActive();
	}
	*/
	@Transactional
	public List<AttendanceAllowanceDO> retrieveById(Long id){
		return attendanceAllowanceDAO.retrieveById(id);
	}
	
	/*@Transactional
	public boolean persistList(List<AttendanceAllowanceDO> AttendanceAllowanceDO) {
		return attendanceAllowanceDAO.persistList(AttendanceAllowanceDO);
	}*/
}
