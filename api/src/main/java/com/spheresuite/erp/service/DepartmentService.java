package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.DepartmentDAO;
import com.spheresuite.erp.domainobject.DepartmentDO;
@Service
@Transactional
public class DepartmentService {
	static Logger logger = Logger.getLogger(DepartmentService.class.getName());
	
	@Autowired
	private DepartmentDAO departmentDAO;

	@Transactional
	public boolean persist(DepartmentDO departmentDO) {
		return departmentDAO.persist(departmentDO);
	}

	@Transactional
	public List<DepartmentDO> retrieveActive() {
		return departmentDAO.retrieveActive();
	}

	@Transactional
	public List<DepartmentDO> retrieveById(Long Id) {
		return departmentDAO.retrieveById(Id);
	}

	@Transactional
	public List<DepartmentDO> retrieve() {
		return departmentDAO.retrieve();
	}

	@Transactional
	public boolean update(DepartmentDO departmentDO) {
		return departmentDAO.update(departmentDO);
	}

	@Transactional
	public List<Long> retrieveisManager(String Id) {
		return departmentDAO.retrieveisManager(Id);
	}
	
	@Transactional
	public List<DepartmentDO> retrieveByName(String name) {
		return departmentDAO.retrieveByName(name);
	}
	
	@Transactional
	public boolean persistList(List<DepartmentDO> departmentDO) {
		return departmentDAO.persistList(departmentDO);
	}
}
