package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.HrPoliciesDocDAO;
import com.spheresuite.erp.domainobject.HrPoliciesDocDO;
@Service
@Transactional
public class HrPoliciesDocService {
	static Logger logger = Logger.getLogger(HrPoliciesDocService.class.getName());
	@Autowired
	private HrPoliciesDocDAO hrPoliciesDocDAO;
	
	@Transactional
	public HrPoliciesDocDO persist(HrPoliciesDocDO hrpoliciesdoc) {
		return hrPoliciesDocDAO.persist(hrpoliciesdoc);
	}

	@Transactional
	public List<HrPoliciesDocDO> retrieveByPoliciesId(Long id) {
		return hrPoliciesDocDAO.retrieveByPoliciesId(id);
	}

	@Transactional
	public boolean delete(Long id) {
		return hrPoliciesDocDAO.delete(id);
	}

	@Transactional
	public List<HrPoliciesDocDO> retrieve() {
		return hrPoliciesDocDAO.retrieve();
	}

	@Transactional
	public HrPoliciesDocDO update(HrPoliciesDocDO hrpoliciesdoc) {
		return hrPoliciesDocDAO.update(hrpoliciesdoc);
	}
}
