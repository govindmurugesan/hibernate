package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.PayrollEarningsDetailsDAO;
import com.spheresuite.erp.domainobject.PayrollEarningsDetailsDO;

@Service
@Transactional
public class PayrollEarningsDetailsService {
	static Logger logger = Logger.getLogger(PayrollEarningsDetailsService.class.getName());
	
	@Autowired
	private PayrollEarningsDetailsDAO payrollEarningsDetailsDAO;
	
	@Transactional
	public List<PayrollEarningsDetailsDO> persist(List<PayrollEarningsDetailsDO> employeeEarningsDO) {
		return payrollEarningsDetailsDAO.persist(employeeEarningsDO);
	}

	@Transactional
	public List<PayrollEarningsDetailsDO> retrieveByEmpCompensationId(Long id) {
		return payrollEarningsDetailsDAO.retrieveByEmpCompensationId(id);
	}
	
	@Transactional
	public List<PayrollEarningsDetailsDO> retrieve() {
		return payrollEarningsDetailsDAO.retrieve();
	}
	
	@Transactional
	public PayrollEarningsDetailsDO update(PayrollEarningsDetailsDO employeeEarningsDO) {
		return payrollEarningsDetailsDAO.update(employeeEarningsDO);
	}
	
	@Transactional
	public List<PayrollEarningsDetailsDO> retrieveByEmpCompensationIds(List<Long> id) {
		return payrollEarningsDetailsDAO.retrieveByEmpCompensationIds(id);
	}
	
	@Transactional
	public boolean deletePayrollById(Long payrollId) {
		return payrollEarningsDetailsDAO.deletePayrollById(payrollId);
	}

}
