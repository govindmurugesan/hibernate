package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.InsurancePaymentDAO;
import com.spheresuite.erp.domainobject.InsurancePaymentDO;
@Service
@Transactional
public class InsurancePaymentService {
	static Logger logger = Logger.getLogger(InsurancePaymentService.class.getName());

	@Autowired
	private InsurancePaymentDAO insurancePaymentDAO;
	
	@Transactional
	public InsurancePaymentDO persist(InsurancePaymentDO insurancePaymentDO) {
		return insurancePaymentDAO.persist(insurancePaymentDO);
	}

	@Transactional
	public List<InsurancePaymentDO> retrieveActiveByEmpId(String empId) {
		return insurancePaymentDAO.retrieveActiveByEmpId(empId);
	}

	/*@Transactional
	public List<InsurancePaymentDO> retrieveByEmpIdAndMonth(String empId, String month) {
		return advancePaymentDAO.retrieveByEmpIdAndMonth(empId,month);
	}

	@Transactional
	public Long retrieveByEmpIdBetweenMonth(String empId, String fromMonth, String toMonth){
		return advancePaymentDAO.retrieveByEmpIdBetweenMonth(empId,fromMonth, toMonth);
	}*/

	@Transactional
	public List<InsurancePaymentDO> retrieve() {
		return insurancePaymentDAO.retrieve();
	}

	@Transactional
	public List<InsurancePaymentDO> retrieveById(Long id) {
		return insurancePaymentDAO.retrieveById(id);
	}

	/*@Transactional
	public List<InsurancePaymentDO> retrieveByEmpIdWithDate(List<Long> id, String bonusMonth) {
		return advancePaymentDAO.retrieveByEmpIdWithDate(id, bonusMonth);
	}

	@Transactional
	public List<InsurancePaymentDO> retrieveByEmpIdBetweenDate(List<Long> id, String fromMonth, String toMonth) {
		return advancePaymentDAO.retrieveByEmpIdBetweenDate(id, fromMonth, toMonth);
	}*/

	@Transactional
	public InsurancePaymentDO update(InsurancePaymentDO insurancePaymentDO) {
		return insurancePaymentDAO.update(insurancePaymentDO);
	}
	
	/*@Transactional
	public boolean delete(Long id) {
		return advancePaymentDAO.delete(id);
	}*/
}
