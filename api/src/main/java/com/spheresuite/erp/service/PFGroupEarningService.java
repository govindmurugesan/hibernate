package com.spheresuite.erp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.PFGroupEarningsDAO;
import com.spheresuite.erp.domainobject.PFGroupEarningsDO;

@Service
@Transactional
public class PFGroupEarningService {

	@Autowired
	private PFGroupEarningsDAO pfGroupEarningDAO;
	
	@Transactional
	public boolean persist(PFGroupEarningsDO pfGroupEarningDO){
		return pfGroupEarningDAO.persist(pfGroupEarningDO);
	}
	
	@Transactional
	public List<PFGroupEarningsDO> retrieveByGroupID(Long id){
		return pfGroupEarningDAO.retrieveByGroupID(id);
	}
	
	@Transactional
	public List<PFGroupEarningsDO> retrieveByID(Long id){
		return pfGroupEarningDAO.retrieveByID(id);
	}
	
	@Transactional
	public boolean updateList(List<PFGroupEarningsDO> pfGroupEarningList){
		return pfGroupEarningDAO.updateList(pfGroupEarningList);
	}
}
