package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.GeneralEarningDAO;
import com.spheresuite.erp.domainobject.GeneralEarningDO;
@Service
@Transactional
public class GeneralEarningService {
	static Logger logger = Logger.getLogger(GeneralEarningService.class.getName());

	@Autowired
	private GeneralEarningDAO loanPaymentDAO;
	
	@Transactional
	public Boolean persist(GeneralEarningDO loanPaymentDO) {
		return loanPaymentDAO.persist(loanPaymentDO);
	}

	@Transactional
	public List<GeneralEarningDO> retrieveActiveByEmpId(String empId) {
		return loanPaymentDAO.retrieveActiveByEmpId(empId);
	}
	
	@Transactional
	public List<GeneralEarningDO> retrieveActiveByEmpIdAndMonth(String empId, String month) {
		return loanPaymentDAO.retrieveActiveByEmpIdAndMonth(empId, month);
	}
	
	

	/*@Transactional
	public List<LoanPaymentDO> retrieveByEmpIdAndMonth(String empId, String month) {
		return advancePaymentDAO.retrieveByEmpIdAndMonth(empId,month);
	}

	@Transactional
	public Long retrieveByEmpIdBetweenMonth(String empId, String fromMonth, String toMonth){
		return advancePaymentDAO.retrieveByEmpIdBetweenMonth(empId,fromMonth, toMonth);
	}*/

	@Transactional
	public List<GeneralEarningDO> retrieve() {
		return loanPaymentDAO.retrieve();
	}

	@Transactional
	public List<GeneralEarningDO> retrieveById(Long id) {
		return loanPaymentDAO.retrieveById(id);
	}

	/*@Transactional
	public List<LoanPaymentDO> retrieveByEmpIdWithDate(List<Long> id, String bonusMonth) {
		return advancePaymentDAO.retrieveByEmpIdWithDate(id, bonusMonth);
	}

	@Transactional
	public List<LoanPaymentDO> retrieveByEmpIdBetweenDate(List<Long> id, String fromMonth, String toMonth) {
		return advancePaymentDAO.retrieveByEmpIdBetweenDate(id, fromMonth, toMonth);
	}*/

	@Transactional
	public Boolean update(GeneralEarningDO loanPaymentDO) {
		return loanPaymentDAO.update(loanPaymentDO);
	}
	
	@Transactional
	public boolean persistList(List<GeneralEarningDO> earningList) {
		return loanPaymentDAO.persistList(earningList);
	}
	
	/*@Transactional
	public boolean delete(Long id) {
		return advancePaymentDAO.delete(id);
	}*/
}
