package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.TimesheetTypeDAO;
import com.spheresuite.erp.domainobject.TimesheetTypeDO;

@Service
@Transactional
public class TimesheetTypeService {
	static Logger logger = Logger.getLogger(TimesheetTypeService.class.getName());
	
	@Autowired
	private TimesheetTypeDAO timesheetTypeDAO;
	
	@Transactional
	public boolean persist(TimesheetTypeDO  timesheetTypeDO) {
		return timesheetTypeDAO.persist(timesheetTypeDO);
	}
	
	@Transactional
	public List<TimesheetTypeDO> retrieveById(Long Id) {
		return timesheetTypeDAO.retrieveById(Id);
	}
	
	@Transactional
	public List<TimesheetTypeDO> retrieve() {
		return timesheetTypeDAO.retrieve();
	}
	
	@Transactional
	public boolean update(TimesheetTypeDO timesheetTypeDO) {
		return timesheetTypeDAO.update(timesheetTypeDO);
	}
	
	@Transactional
	public boolean persistList(List<TimesheetTypeDO> timeSheetType) {
		return timesheetTypeDAO.persistList(timeSheetType);
	}
}
