package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.EmployeeLeavegroupDAO;
import com.spheresuite.erp.domainobject.EmployeeLeavegroupDO;
@Service
@Transactional
public class EmployeeLeavegroupService {
	static Logger logger = Logger.getLogger(EmployeeLeavegroupService.class.getName());
	
	@Autowired
	private EmployeeLeavegroupDAO employeeLeavegrpDAO;

	@Transactional
	public Boolean persist(EmployeeLeavegroupDO employeeLeavegrp)  {
		return employeeLeavegrpDAO.persist(employeeLeavegrp);
	}

	@Transactional
	public List<EmployeeLeavegroupDO> retrieveByEmpId(String empId)  {
		return employeeLeavegrpDAO.retrieveByEmpId(empId);
	}

	@Transactional
	public List<EmployeeLeavegroupDO> retrieveById(Long id)  {
		return employeeLeavegrpDAO.retrieveById(id);
	}

	@Transactional
	public EmployeeLeavegroupDO update(EmployeeLeavegroupDO employeeLeavegrp)  {
		return employeeLeavegrpDAO.update(employeeLeavegrp);
	}

	@Transactional
	public boolean persistList(List<EmployeeLeavegroupDO> employeeLeavegroupDO) {
		return employeeLeavegrpDAO.persistList(employeeLeavegroupDO);
	}
}
