package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.TerminateReasonTypeDAO;
import com.spheresuite.erp.domainobject.TerminateReasonTypeDO;

@Service
@Transactional
public class TerminateReasonTypeService {
	static Logger logger = Logger.getLogger(TerminateReasonTypeService.class.getName());
	
	@Autowired
	private TerminateReasonTypeDAO terminateReasonTypeDAO;
	
	@Transactional
	public boolean persist(TerminateReasonTypeDO terminateReasonTypeDO) {
		return terminateReasonTypeDAO.persist(terminateReasonTypeDO);
	}

	@Transactional
	public List<TerminateReasonTypeDO> retrieveActive() {
		return terminateReasonTypeDAO.retrieveActive();
	}
	
	@Transactional
	public List<TerminateReasonTypeDO> retrieveById(Long id) {
		return terminateReasonTypeDAO.retrieveById(id);
	}
	
	@Transactional
	public List<TerminateReasonTypeDO> retrieve() {
		return terminateReasonTypeDAO.retrieve();
	}
	
	@Transactional
	public boolean update(TerminateReasonTypeDO terminateReasonTypeDO) {
		return terminateReasonTypeDAO.update(terminateReasonTypeDO);
	}
	
	@Transactional
	public boolean persistList(List<TerminateReasonTypeDO> terminateReasonTypeDO) {
		return terminateReasonTypeDAO.persistList(terminateReasonTypeDO);
	}
}
