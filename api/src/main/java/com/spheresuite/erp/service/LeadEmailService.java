package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.LeadEmailDAO;
import com.spheresuite.erp.domainobject.LeadEmailDO;
@Service
@Transactional
public class LeadEmailService {
	static Logger logger = Logger.getLogger(LeadEmailService.class.getName());
	@Autowired
	private LeadEmailDAO leadEmailDAO;

	@Transactional
	public LeadEmailDO persist(LeadEmailDO leadEmailDO) {
		return leadEmailDAO.persist(leadEmailDO);
	}

	@Transactional
	public boolean persistList(List<LeadEmailDO> leadEmailDO) {
		return leadEmailDAO.persistList(leadEmailDO);
	}

	@Transactional
	public List<LeadEmailDO> retrieveByLead(Long leadId) {
		return leadEmailDAO.retrieveByLead(leadId);
	}

	@Transactional
	public List<LeadEmailDO> retrieveById(Long Id) {
		return leadEmailDAO.retrieveById(Id);
	}

	@Transactional
	public List<LeadEmailDO> retrieveLast(Long leadId) {
		return leadEmailDAO.retrieveByLead(leadId);
	}
	

	@Transactional
	public List<LeadEmailDO> retrieveAllMail() {
		return leadEmailDAO.retrieveAllMail();
	}

	@Transactional
	public boolean delete(String string) {
		return leadEmailDAO.delete(string);
	}
	
	
}
