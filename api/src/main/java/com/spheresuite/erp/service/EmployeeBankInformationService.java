package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.EmployeeBankInformationDAO;
import com.spheresuite.erp.domainobject.EmployeeBankInformationDO;
@Service
@Transactional
public class EmployeeBankInformationService {
	static Logger logger = Logger.getLogger(EmployeeBankInformationService.class.getName());
	
	@Autowired
	private EmployeeBankInformationDAO employeeBankInformationDAO;

	@Transactional
	public EmployeeBankInformationDO persist(EmployeeBankInformationDO employeeBankInformationDO)  {
		return employeeBankInformationDAO.persist(employeeBankInformationDO);
	}

	@Transactional
	public List<EmployeeBankInformationDO> retrieveByEmpId(String empId)  {
		return employeeBankInformationDAO.retrieveByEmpId(empId);
	}

	@Transactional
	public List<EmployeeBankInformationDO> retrieveById(Long id)  {
		return employeeBankInformationDAO.retrieveById(id);
	}

	@Transactional
	public EmployeeBankInformationDO update(EmployeeBankInformationDO employeeBankInformationDO)  {
		return employeeBankInformationDAO.update(employeeBankInformationDO);
	}
	
	@Transactional
	public boolean persistList(List<EmployeeBankInformationDO> employeeBankInfoDO) {
		return employeeBankInformationDAO.persistList(employeeBankInfoDO);
	}
}
