package com.spheresuite.erp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.EmployeeTravelDAO;
import com.spheresuite.erp.domainobject.EmployeeTravelDO;

@Service
@Transactional
public class EmployeeTravelService{
	
	@Autowired
	private EmployeeTravelDAO employeeTravelDAO;

	@Transactional
	public boolean persist(EmployeeTravelDO empTravellDO) {
		return employeeTravelDAO.persist(empTravellDO);
	}
	
	/*@Transactional
	public List<EmployeeTravelDO> retrieve() {
		return employeeTravelDAO.retrieve();
	}*/

	@Transactional
	public boolean update(EmployeeTravelDO empTravellDO) {
		return employeeTravelDAO.update(empTravellDO);
	}
	
	/*@Transactional
	public List<AddressTypeDO> retrieveActive(){
		return employeeTravelDAO.retrieveActive();
	}*/
	
	@Transactional
	public List<EmployeeTravelDO> retrieveById(Long id){
		return employeeTravelDAO.retrieveById(id);
	}
	
	@Transactional
	public List<EmployeeTravelDO> retrieveByReportingId(String id){
		return employeeTravelDAO.retrieveByReportingId(id);
	}
	
	@Transactional
	public List<EmployeeTravelDO> retrieveByEmpId(String id){
		return employeeTravelDAO.retrieveByEmpId(id);
	}
	
	
	/*@Transactional
	public boolean persistList(List<AddressTypeDO> addressTypeDO) {
		return addressTypeDAO.persistList(addressTypeDO);
	}*/
}
