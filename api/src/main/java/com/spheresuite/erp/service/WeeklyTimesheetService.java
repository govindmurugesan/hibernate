package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.WeeklyTimesheetDAO;
import com.spheresuite.erp.domainobject.WeeklyTimesheetDO;

@Service
@Transactional
public class WeeklyTimesheetService {
	static Logger logger = Logger.getLogger(WeeklyTimesheetService.class.getName());
	
	@Autowired
	private WeeklyTimesheetDAO weeklyTimesheetDAO;
	
	@Transactional
	public WeeklyTimesheetDO persist(WeeklyTimesheetDO timesheet) {
		return weeklyTimesheetDAO.persist(timesheet);
	}

	@Transactional
	public List<WeeklyTimesheetDO> retrieve() {
		return weeklyTimesheetDAO.retrieve();
	}
	
	@Transactional
	public List<WeeklyTimesheetDO> retrieveByDept(List<Long> ids) {
		return weeklyTimesheetDAO.retrieveByDept(ids);
	}
	
	@Transactional
	public List<WeeklyTimesheetDO> retrieveById(Long Id) {
		return weeklyTimesheetDAO.retrieveById(Id);
	}
	
	@Transactional
	public List<WeeklyTimesheetDO> retrieveByDate(String date, long empid) {
		return weeklyTimesheetDAO.retrieveByDate(date, empid);
	}
	
	@Transactional
	public List<WeeklyTimesheetDO> retrieveByEmpId(String empid) {
		return weeklyTimesheetDAO.retrieveByEmpId(empid);
	}
	
	@Transactional
	public List<WeeklyTimesheetDO> retrieveSubmittedTimesheet() {
		return weeklyTimesheetDAO.retrieveSubmittedTimesheet();
	}
	
	@Transactional
	public WeeklyTimesheetDO update(WeeklyTimesheetDO timesheet) {
		return weeklyTimesheetDAO.update(timesheet);
	}
}
