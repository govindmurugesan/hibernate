package com.spheresuite.erp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.AddressTypeDAO;
import com.spheresuite.erp.domainobject.AddressTypeDO;

@Service
@Transactional
public class AddressTypeService{
	
	@Autowired
	private AddressTypeDAO addressTypeDAO;

	@Transactional
	public boolean persist(AddressTypeDO addressTypeDO) {
		return addressTypeDAO.persist(addressTypeDO);
	}
	
	@Transactional
	public List<AddressTypeDO> retrieve() {
		return addressTypeDAO.retrieve();
	}

	@Transactional
	public boolean update(AddressTypeDO addressTypeDO) {
		return addressTypeDAO.update(addressTypeDO);
	}
	
	@Transactional
	public List<AddressTypeDO> retrieveActive(){
		return addressTypeDAO.retrieveActive();
	}
	
	@Transactional
	public List<AddressTypeDO> retrieveById(Long id){
		return addressTypeDAO.retrieveById(id);
	}
	
	@Transactional
	public boolean persistList(List<AddressTypeDO> addressTypeDO) {
		return addressTypeDAO.persistList(addressTypeDO);
	}
}
