package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.DeductionTypeDAO;
import com.spheresuite.erp.domainobject.DeductionTypeDO;
@Service
@Transactional
public class DeductionTypeService {
	static Logger logger = Logger.getLogger(DeductionTypeService.class.getName());
	@Autowired
	private DeductionTypeDAO deductionTypeDAO;
	
	@Transactional
	public boolean persist(DeductionTypeDO deductionTypeDO)  {
		return deductionTypeDAO.persist(deductionTypeDO);
	}

	@Transactional
	public List<DeductionTypeDO> retrieveActive()  {
		return deductionTypeDAO.retrieveActive();
	}

	@Transactional
	public List<DeductionTypeDO> retrieveById(Long id)  {
		return deductionTypeDAO.retrieveById(id);
	}
	
	@Transactional
	public List<DeductionTypeDO> retrieveByName(String name)  {
		return deductionTypeDAO.retrieveByName(name);
	}

	@Transactional
	public List<DeductionTypeDO> retrieve()  {
		return deductionTypeDAO.retrieve();
	}

	@Transactional
	public boolean update(DeductionTypeDO deductionTypeDO)  {
		return deductionTypeDAO.update(deductionTypeDO);
	}
	
	@Transactional
	public boolean persistList(List<DeductionTypeDO> deducationType) {
		return deductionTypeDAO.persistList(deducationType);
	}
}
