package com.spheresuite.erp.service;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.EmployeePayrollDAO;
import com.spheresuite.erp.domainobject.EmployeePayrollDO;
@Service
@Transactional
public class EmployeePayrollService {
	static Logger logger = Logger.getLogger(EmployeePayrollService.class.getName());
	@Autowired
	private EmployeePayrollDAO employeePayrollDAO;
	
	@Transactional
	public EmployeePayrollDO persist(EmployeePayrollDO employeePayrollDO) {
		return employeePayrollDAO.persist(employeePayrollDO);
	}
	
	@Transactional
	public List<EmployeePayrollDO> retriveByMonthAndEmp(String month, String empId) {
		return employeePayrollDAO.retriveByMonthAndEmp(month, empId);
	}
	
	@Transactional
	public List<EmployeePayrollDO> retriveByWeekAndEmp(String fromDate, String toDate, String empId) {
		return employeePayrollDAO.retriveByWeekAndEmp(fromDate, toDate, empId);
	}
	
	@Transactional
	public Object[] retrieveByEmpIdBetweenDateSum(String empId, String fromMnth, String toMonth) {
		return employeePayrollDAO.retrieveByEmpIdBetweenDateSum(empId, fromMnth, toMonth);
	}
	
	@Transactional
	public List<EmployeePayrollDO> retrieveByEmpIdBetweenDate(String empId, String fromMnth, String toMonth) {
		return employeePayrollDAO.retrieveByEmpIdBetweenDate(empId, fromMnth, toMonth);
	}
	
	@Transactional
	public Object[] retrieveByEmpIdForMonthSum(String empId, String fromMnth) {
		return employeePayrollDAO.retrieveByEmpIdForMonthSum(empId, fromMnth);
	}
	
	@Transactional
	public List<EmployeePayrollDO> retriveByMonth(String month, Long payrollBatchId) {
		return employeePayrollDAO.retriveByMonthAndBatch(month, payrollBatchId);
	}
	
	@Transactional
	public List<EmployeePayrollDO> retriveForProcessPayroll(String month, Long payrollBatchId) {
		return employeePayrollDAO.retriveForProcessPayroll(month, payrollBatchId);
	}
	
	@Transactional
	public List<EmployeePayrollDO> retriveForSubmitApprovel(String month, Long payrollBatchId) {
		return employeePayrollDAO.retriveForSubmitApprovel(month, payrollBatchId);
	}
	
	@Transactional
	public List<EmployeePayrollDO> retriveWeelyPayrollForSubmitApprovel(String fromDate, String toDate, Long payrollBatchId) {
		return employeePayrollDAO.retriveWeelyPayrollForSubmitApprovel(fromDate,toDate, payrollBatchId);
	}
	
	@Transactional
	public List<EmployeePayrollDO> retriveDailyPayrollForSubmitApprovel(String date, Long payrollBatchId) {
		return employeePayrollDAO.retriveDailyPayrollForSubmitApprovel(date, payrollBatchId);
	}
	
	@Transactional
	public List<EmployeePayrollDO> retriveDailyPayrollForProcess(String date,Long payrollBatchId) {
		return employeePayrollDAO.retriveDailyPayrollForProcess(date, payrollBatchId);
	}
	
	
	@Transactional
	public List<EmployeePayrollDO> retriveWeelyPayrollForProcess(String fromDate, String toDate, Long payrollBatchId) {
		return employeePayrollDAO.retriveWeelyPayrollForProcess(fromDate,toDate, payrollBatchId);
	}
	@Transactional
	public List<EmployeePayrollDO> retriveWeelyPayrollForApprovel(String fromDate, String toDate, Long payrollBatchId) {
		return employeePayrollDAO.retriveWeelyPayrollForApprovel(fromDate,toDate, payrollBatchId);
	}
	
	@Transactional
	public EmployeePayrollDO update(EmployeePayrollDO employeePayrollDO) {
		return employeePayrollDAO.update(employeePayrollDO);
	}

	@Transactional
	public List<EmployeePayrollDO> retrieveById(Long id) {
		return employeePayrollDAO.retrieveById(id);
	}
	
	@Transactional
	public List<Object[]> retrieveForSummary() {
		return employeePayrollDAO.retrieveForSummary();
	}
	
	@Transactional
	public List<Object[]> retrieveForSummaryByBatchId(String month) {
		return employeePayrollDAO.retrieveForSummaryByBatchId(month);
	}
	
	@Transactional
	public List<Object[]> retrieveForWeeklySummaryByBatchId(String month) {
		return employeePayrollDAO.retrieveForWeeklySummaryByBatchId(month);
	}
	
	
	@Transactional
	public List<EmployeePayrollDO> retrieveByAllEmp(String payrollMonth, Long payrollbatchId, char status) {
		return employeePayrollDAO.retrieveByAllEmp(payrollMonth, payrollbatchId, status);
	}
	
	@Transactional
	public List<EmployeePayrollDO> retrieveByEmpIdAndStatus(String empId) {
		return employeePayrollDAO.retrieveByEmpIdAndStatus(empId);
	}
	
	@Transactional
	public boolean deletePayrollById(Long payrollId) {
		return employeePayrollDAO.deletePayrollById(payrollId);
	}

	@Transactional
	public List<EmployeePayrollDO> retriveDailyEmpPayroll(String empId, String date, Long payrollbatchId) {
		return employeePayrollDAO.retriveDailyEmpPayroll(empId, date, payrollbatchId);
	}
	
	@Transactional
	public List<EmployeePayrollDO> retrieveByPayrollMonth(String payrollMonth, char status) {
		return employeePayrollDAO.retrieveByPayrollMonth(payrollMonth, status);
	}
	
	@Transactional
	public List<EmployeePayrollDO> retrieveByPayrollMonthByEmpID(String payrollMonth, char status, String empid) {
		return employeePayrollDAO.retrieveByPayrollMonthByEmpID(payrollMonth, status,empid);
	}
	
	
	
	
	/*@Transactional
	public EmployeePayrollDO persist(EmployeePayrollDO employeePayrollDO) {
		return employeePayrollDAO.persist(employeePayrollDO);
	}

	@Transactional
	public List<EmployeePayrollDO> retrieveActive(Long empId) {
		return employeePayrollDAO.retrieveActive(empId);
	}

	@Transactional
	public List<EmployeePayrollDO> retrieveById(Long id) {
		return employeePayrollDAO.retrieveById(id);
	}

	@Transactional
	public List<EmployeePayrollDO> retrieveByEmpId(Long empId) {
		return employeePayrollDAO.retrieveByEmpId(empId);
	}

	

	

	
	@Transactional
	public List<EmployeePayrollDO> retrieveByAllEmp(String payrollMonth, Long payrollbatchId, char status) {
		return employeePayrollDAO.retrieveByAllEmp(payrollMonth, payrollbatchId, status);
	}

	@Transactional
	public List<EmployeePayrollDO> retriveByMonth(String month, Long payrollBatchId) {
		return employeePayrollDAO.retriveByMonth(month, payrollBatchId);
	}

	@Transactional
	public List<EmployeePayrollDO> retriveByStatus(String month, Long payrollBatchId) {
		return employeePayrollDAO.retriveByStatus(month, payrollBatchId);
	}

	@Transactional
	public List<EmployeePayrollDO> retriveByMonthAndEmp(String month, Long empId) {
		return employeePayrollDAO.retriveByMonthAndEmp(month, empId);
	}

	@Transactional
	public List<EmployeePayrollDO> retrieve() {
		return employeePayrollDAO.retrieve();
	}

	@Transactional
	public EmployeePayrollDO update(EmployeePayrollDO employeePayrollDO) {
		return employeePayrollDAO.update(employeePayrollDO);
	}

	@Transactional
	public List<Object[]> retrieveForSummary() {
		return employeePayrollDAO.retrieveForSummary();
	}*/
	
	/*@Transactional
	public List<Object[]> retrieveForSummaryByBatchId(String month, char status) {
		return employeePayrollDAO.retrieveForSummaryByBatchId(month,status);
	}*/
	
	/*@Transactional
	public List<Object[]> retrieveForSummaryByBatchId(String month) {
		return employeePayrollDAO.retrieveForSummaryByBatchId(month);
	}

	@Transactional
	public List<EmployeePayrollDO> retriveForReportMonth(String month, Long batchId) {
		return employeePayrollDAO.retriveForReportMonth(month, batchId);
	}

	@Transactional
	public List<EmployeePayrollDO> retriveForReportQuarter(String fromMonth, String toMonth, Long batchId) {
		return employeePayrollDAO.retriveForReportQuarter(fromMonth, toMonth, batchId);
	}*/
}
