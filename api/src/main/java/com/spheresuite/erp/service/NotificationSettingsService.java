package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.NotificationSettingsDAO;
import com.spheresuite.erp.domainobject.NotificationSettingsDO;
@Service
@Transactional
public class NotificationSettingsService {
	static Logger logger = Logger.getLogger(NotificationSettingsService.class.getName());
	@Autowired
	private NotificationSettingsDAO notificationSettingsDAO;

	@Transactional
	public NotificationSettingsDO persist(NotificationSettingsDO notificationSettingsDO) {
		return notificationSettingsDAO.persist(notificationSettingsDO);
	}

	@Transactional
	public NotificationSettingsDO update(NotificationSettingsDO notificationSettingsDO) {
		return notificationSettingsDAO.update(notificationSettingsDO);
	}

	@Transactional
	public List<NotificationSettingsDO> retrieveById(Long Id) {
		return notificationSettingsDAO.retrieveById(Id);
	}

	@Transactional
	public List<NotificationSettingsDO> retrieve() {
		return notificationSettingsDAO.retrieve();
	}
}
