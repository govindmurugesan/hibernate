package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.EmployeeTypeDAO;
import com.spheresuite.erp.domainobject.EmployeeTypeDO;
@Service
@Transactional
public class EmployeeTypeService {
	static Logger logger = Logger.getLogger(EmployeeTypeService.class.getName());
	
	@Autowired
	private EmployeeTypeDAO employeeTypeDAO;

	@Transactional
	public boolean persist(EmployeeTypeDO empTypeDO) {
		return employeeTypeDAO.persist(empTypeDO);
	}

	@Transactional
	public List<EmployeeTypeDO> retrieveActive() {
		return employeeTypeDAO.retrieveActive();
	}

	@Transactional
	public List<EmployeeTypeDO> retrieveById(Long id) {
		return employeeTypeDAO.retrieveById(id);
	}
	
	@Transactional
	public List<EmployeeTypeDO> retrieveByName(String name) {
		return employeeTypeDAO.retrieveByName(name);
	}

	@Transactional
	public List<EmployeeTypeDO> retrieve() {
		return employeeTypeDAO.retrieve();
	}

	@Transactional
	public boolean update(EmployeeTypeDO empTypeDO) {
		return employeeTypeDAO.update(empTypeDO);
	}
	
	@Transactional
	public boolean persistList(List<EmployeeTypeDO> employeeTypeDO) {
		return employeeTypeDAO.persistList(employeeTypeDO);
	}
}
