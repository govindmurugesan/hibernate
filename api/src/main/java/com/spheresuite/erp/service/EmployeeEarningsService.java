package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.EmployeeEarningsDAO;
import com.spheresuite.erp.domainobject.EmployeeEarningsDO;
@Service
@Transactional
public class EmployeeEarningsService {
	static Logger logger = Logger.getLogger(EmployeeEarningsService.class.getName());
	@Autowired
	private EmployeeEarningsDAO employeeEarningsDAO;

	@Transactional
	public List<EmployeeEarningsDO> persist(List<EmployeeEarningsDO> employeeEarningsDO) {
		return employeeEarningsDAO.persist(employeeEarningsDO);
	}

	@Transactional
	public List<EmployeeEarningsDO> retrieveByEmpCompensationId(Long id) {
		return employeeEarningsDAO.retrieveByEmpCompensationId(id);
	}

	@Transactional
	public List<EmployeeEarningsDO> retrieve() {
		return employeeEarningsDAO.retrieve();
	}

	@Transactional
	public EmployeeEarningsDO update(EmployeeEarningsDO employeeEarningsDO) {
		return employeeEarningsDAO.update(employeeEarningsDO);
	}
}
