package com.spheresuite.erp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.PayrollGroupEarningsDAO;
import com.spheresuite.erp.domainobject.PayrollGroupEarningsDO;

@Service
@Transactional
public class PayrollGroupEarningService {

	@Autowired
	private PayrollGroupEarningsDAO payrollGroupEarningDAO;
	
	@Transactional
	public boolean persist(PayrollGroupEarningsDO payrollGroupEarningDO){
		return payrollGroupEarningDAO.persist(payrollGroupEarningDO);
	}
	
	@Transactional
	public List<PayrollGroupEarningsDO> retrieveByGroupID(Long id){
		return payrollGroupEarningDAO.retrieveByGroupID(id);
	}
	
	@Transactional
	public List<PayrollGroupEarningsDO> retrieveByID(Long id){
		return payrollGroupEarningDAO.retrieveByID(id);
	}
	
	@Transactional
	public boolean updateList(List<PayrollGroupEarningsDO> payrollGroupEarningList){
		return payrollGroupEarningDAO.updateList(payrollGroupEarningList);
	}
	/*@Transactional
	public List<PayrollGroupDO> retrieveActive(){
		return payrollGroupDAO.retrieveActive();
	}
	
	
	
	@Transactional
	public List<PayrollGroupDO> retrieve(){
		return payrollGroupDAO.retrieve();
	}*/
	
	/*@Transactional
	public boolean updateList(PayrollGroupDO payrollGroupDO){
		return payrollGroupDAO.update(payrollGroupDO);
	}*/
}
