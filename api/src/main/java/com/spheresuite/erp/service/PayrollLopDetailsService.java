package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.PayrollLopDetailsDAO;
import com.spheresuite.erp.domainobject.PayrollLopDetailsDO;

@Service
@Transactional
public class PayrollLopDetailsService {
	static Logger logger = Logger.getLogger(PayrollLopDetailsService.class.getName());
	
	@Autowired
	private PayrollLopDetailsDAO payrollLopDetailsDAO;
	
	@Transactional
	public PayrollLopDetailsDO persist(PayrollLopDetailsDO employeeEarningsDO) {
		return payrollLopDetailsDAO.persist(employeeEarningsDO);
	}

	@Transactional
	public List<PayrollLopDetailsDO> retrieveByEmpId(Long id, String monthly) {
		return payrollLopDetailsDAO.retrieveByEmpId(id, monthly);
	}
	
	@Transactional
	public List<PayrollLopDetailsDO> retrieve() {
		return payrollLopDetailsDAO.retrieve();
	}
	
	@Transactional
	public List<PayrollLopDetailsDO> retrieveByBetweenMonth(Long id, String fromMonth, String toMonth) {
		return payrollLopDetailsDAO.retrieveByBetweenMonth( id, fromMonth, toMonth);
	}
}
