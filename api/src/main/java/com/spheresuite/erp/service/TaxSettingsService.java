package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.TaxSettingsDAO;
import com.spheresuite.erp.domainobject.TaxSettingsDO;

@Service
@Transactional
public class TaxSettingsService {
	static Logger logger = Logger.getLogger(TaxSettingsService.class.getName());
	
	@Autowired
	private TaxSettingsDAO taxSettingsDAO;
	
	@Transactional
	public TaxSettingsDO persist(TaxSettingsDO taxSettingDO) {
		return taxSettingsDAO.persist(taxSettingDO);
	}
	
	@Transactional
	public TaxSettingsDO update(TaxSettingsDO taxSettingDO) {
		return taxSettingsDAO.update(taxSettingDO);
	}

	@Transactional
	public List<TaxSettingsDO> retrieveById(Long Id) {
		return taxSettingsDAO.retrieveById(Id);
	}

	@Transactional
	public List<TaxSettingsDO> retrieve() {
		return taxSettingsDAO.retrieve();
	}
	
	@Transactional
	public List<TaxSettingsDO> retrieveActive(String name) {
		return taxSettingsDAO.retrieveActive(name);
	}
}
