package com.spheresuite.erp.service;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.ItSavingsSettingsDAO;
import com.spheresuite.erp.domainobject.ItSavingsSettingsDO;
@Service
@Transactional
public class ItSavingsSettingsService {
	static Logger logger = Logger.getLogger(ItSavingsSettingsService.class.getName());
	@Autowired
	private ItSavingsSettingsDAO itSavingsSettingsDAO;
	
	@Transactional
	public boolean persist(ItSavingsSettingsDO itSavingsSettingsDO) {
		return itSavingsSettingsDAO.persist(itSavingsSettingsDO);
	}

	@Transactional
	public List<ItSavingsSettingsDO> retrieveActive() {
		return itSavingsSettingsDAO.retrieveActive();
	}

	@Transactional
	public List<ItSavingsSettingsDO> retrieveById(Long id) {
		return itSavingsSettingsDAO.retrieveById(id);
	}

	@Transactional
	public List<ItSavingsSettingsDO> retrieve() {
		return itSavingsSettingsDAO.retrieve();
	}

	@Transactional
	public List<ItSavingsSettingsDO> retrieveByDate(Date fromDate, Date toDate) {
		return itSavingsSettingsDAO.retrieveByDate(fromDate, toDate);
	}
	
	@Transactional
	public boolean persistList(List<ItSavingsSettingsDO> itSavingsSettings) {
		return itSavingsSettingsDAO.persistList(itSavingsSettings);
	}

	@Transactional
	public boolean update(ItSavingsSettingsDO itSavingsSettingsDO) {
		return itSavingsSettingsDAO.update(itSavingsSettingsDO);
	}
}
