package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.EmployeePassportDetailsDAO;
import com.spheresuite.erp.domainobject.EmployeePassportDetailsDO;
@Service
@Transactional
public class EmployeePassportDetailsService {
	static Logger logger = Logger.getLogger(EmployeePassportDetailsService.class.getName());
	@Autowired
	private EmployeePassportDetailsDAO employeePassportDetailsDAO;
	

	@Transactional
	public EmployeePassportDetailsDO persist(EmployeePassportDetailsDO employeePassportInfoDO) {
		return employeePassportDetailsDAO.persist(employeePassportInfoDO);
	}

	@Transactional
	public List<EmployeePassportDetailsDO> retrieveByEmpId(String empId) {
		return employeePassportDetailsDAO.retrieveByEmpId(empId);
	}

	@Transactional
	public List<EmployeePassportDetailsDO> retrieveById(Long id) {
		return employeePassportDetailsDAO.retrieveById(id);
	}

	@Transactional
	public EmployeePassportDetailsDO update(EmployeePassportDetailsDO employeePassportInfoDO) {
		return employeePassportDetailsDAO.update(employeePassportInfoDO);
	}
}
