package com.spheresuite.erp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.PFGroupDAO;
import com.spheresuite.erp.domainobject.PFGroupDO;

@Service
@Transactional
public class PFGroupService {

	@Autowired
	private PFGroupDAO pfGroupDAO;
	
	@Transactional
	public boolean persist(PFGroupDO pfGroupDO){
		return pfGroupDAO.persist(pfGroupDO);
	}

	@Transactional
	public List<PFGroupDO> retrieveActive(){
		return pfGroupDAO.retrieveActive();
	}
	
	@Transactional
	public List<PFGroupDO> retrieveById(Long id){
		return pfGroupDAO.retrieveById(id);
	}
	
	@Transactional
	public List<PFGroupDO> retrieve(){
		return pfGroupDAO.retrieve();
	}
	
	@Transactional
	public List<PFGroupDO> retrieveByName(String name){
		return pfGroupDAO.retrieveByName(name);
	}
	
	@Transactional
	public boolean persistList(List<PFGroupDO> pfGroupList) {
		return pfGroupDAO.persistList(pfGroupList);
	}
	
	@Transactional
	public boolean update(PFGroupDO pfGroupDO){
		return pfGroupDAO.update(pfGroupDO);
	}
}
