package com.spheresuite.erp.service;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.AttendanceDAO;
import com.spheresuite.erp.domainobject.AttendanceDO;
@Service
@Transactional
public class AttendanceService {
	static Logger logger = Logger.getLogger(AttendanceService.class.getName());
	
	@Autowired
	private AttendanceDAO attendaceDAO;

	@Transactional
	public boolean persist(AttendanceDO attendanceDO) {
		return attendaceDAO.persist(attendanceDO);
	}
	
	@Transactional
	public boolean update(AttendanceDO attendanceDO) {
		return attendaceDAO.update(attendanceDO);
	}
	
	@Transactional
	public List<AttendanceDO> retrieve() {
		return attendaceDAO.retrieve();
	}
	
	@Transactional
	public List<Object[]> retrieveSpeceficField() {
		return attendaceDAO.retrieveSpeceficField();
	}

	@Transactional
	public boolean persistList(List<AttendanceDO> attendanceDO) {
		return attendaceDAO.persistList(attendanceDO);
	}
	
	@Transactional
	public List<AttendanceDO> retrieveByEmpId(String empId) {
		return attendaceDAO.retrieveByEmpId(empId);
	}
	
	@Transactional
	public List<AttendanceDO> retrieveById(Long id) {
		return attendaceDAO.retrieveById(id);
	}
	
	@Transactional
	public List<AttendanceDO> retrieveByEmpIdAndDate(String empId,Date fromDate, Date endDate) {
		return attendaceDAO.retrieveByEmpIdAndDate(empId, fromDate ,endDate);
	}
	
	@Transactional
	public List<AttendanceDO> retrieveByEmpId_Date(String empId,Date fromDate, Date endDate) {
		return attendaceDAO.retrieveByEmpId_Date(empId, fromDate ,endDate);
	}
	
	@Transactional
	public List<AttendanceDO> retrieveByTeamEmp(List<String> empIds) {
		return attendaceDAO.retrieveByTeamEmp(empIds);
	}

	/*@Transactional
	public List<DesignationDO> retrieveActive() {
		return designationDAO.retrieveActive();
	}

	@Transactional
	public List<DesignationDO> retrieveById(Long id) {
		return designationDAO.retrieveById(id);
	}
	
	@Transactional
	public List<DesignationDO> retrieveByName(String name) {
		return designationDAO.retrieveByName(name);
	}*/

	
}
