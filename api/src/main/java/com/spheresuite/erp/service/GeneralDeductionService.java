package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.GeneralDeductionDAO;
import com.spheresuite.erp.domainobject.GeneralDeductionDO;
@Service
@Transactional
public class GeneralDeductionService {
	static Logger logger = Logger.getLogger(GeneralDeductionService.class.getName());

	@Autowired
	private GeneralDeductionDAO loanPaymentDAO;
	
	@Transactional
	public GeneralDeductionDO persist(GeneralDeductionDO loanPaymentDO) {
		return loanPaymentDAO.persist(loanPaymentDO);
	}

	@Transactional
	public List<GeneralDeductionDO> retrieveActiveByEmpId(String empId) {
		return loanPaymentDAO.retrieveActiveByEmpId(empId);
	}

	/*@Transactional
	public List<LoanPaymentDO> retrieveByEmpIdAndMonth(String empId, String month) {
		return advancePaymentDAO.retrieveByEmpIdAndMonth(empId,month);
	}

	@Transactional
	public Long retrieveByEmpIdBetweenMonth(String empId, String fromMonth, String toMonth){
		return advancePaymentDAO.retrieveByEmpIdBetweenMonth(empId,fromMonth, toMonth);
	}*/

	@Transactional
	public List<GeneralDeductionDO> retrieve() {
		return loanPaymentDAO.retrieve();
	}

	@Transactional
	public List<GeneralDeductionDO> retrieveById(Long id) {
		return loanPaymentDAO.retrieveById(id);
	}

	/*@Transactional
	public List<LoanPaymentDO> retrieveByEmpIdWithDate(List<Long> id, String bonusMonth) {
		return advancePaymentDAO.retrieveByEmpIdWithDate(id, bonusMonth);
	}

	@Transactional
	public List<LoanPaymentDO> retrieveByEmpIdBetweenDate(List<Long> id, String fromMonth, String toMonth) {
		return advancePaymentDAO.retrieveByEmpIdBetweenDate(id, fromMonth, toMonth);
	}*/

	@Transactional
	public GeneralDeductionDO update(GeneralDeductionDO loanPaymentDO) {
		return loanPaymentDAO.update(loanPaymentDO);
	}
	
	/*@Transactional
	public boolean delete(Long id) {
		return advancePaymentDAO.delete(id);
	}*/
}
