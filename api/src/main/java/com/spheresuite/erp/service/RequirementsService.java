package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.RequirementsDAO;
import com.spheresuite.erp.domainobject.RequirementsDO;

@Service
@Transactional
public class RequirementsService {
	static Logger logger = Logger.getLogger(RequirementsService.class.getName());
	
	@Autowired
	private RequirementsDAO requirementsDAO;
	
	@Transactional
	public RequirementsDO persist(RequirementsDO requirementsDO) {
		return requirementsDAO.persist(requirementsDO);
	}
	
	@Transactional
	public RequirementsDO update(RequirementsDO requirementsDO) {
		return requirementsDAO.update(requirementsDO);
	}

	@Transactional
	public List<RequirementsDO> retrieveById(Long Id) {
		return requirementsDAO.retrieveById(Id);
	}

	@Transactional
	public List<RequirementsDO> retrieve() {
		return requirementsDAO.retrieve();
	}
	
	@Transactional
	public List<RequirementsDO> retrieveByStatus(char status) {
		return requirementsDAO.retrieveByStatus(status);
	}
	
	@Transactional
	public List<RequirementsDO> retrieveByProjectId(Long id) {
		return requirementsDAO.retrieveByProjectId(id);
	}
	
	@Transactional
	public List<RequirementsDO> retrieveByCustomerId(Long id) {
		return requirementsDAO.retrieveByCustomerId(id);
	}
}
