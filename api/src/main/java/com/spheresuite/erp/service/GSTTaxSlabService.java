package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.GSTTaxSlabDAO;
import com.spheresuite.erp.domainobject.GSTTaxSlabDO;
@Service
@Transactional
public class GSTTaxSlabService {
	static Logger logger = Logger.getLogger(GSTTaxSlabService.class.getName());
	@Autowired
	private GSTTaxSlabDAO gSTTaxSlabDAO;
	
	@Transactional
	public boolean persist(GSTTaxSlabDO gstTaxDO) {
		return gSTTaxSlabDAO.persist(gstTaxDO);
	}

	@Transactional
	public List<GSTTaxSlabDO> retrieveActive() {
		return gSTTaxSlabDAO.retrieveActive();
	}

	@Transactional
	public List<GSTTaxSlabDO> retrieveById(Long Id) {
		return gSTTaxSlabDAO.retrieveById(Id);
	}

	@Transactional
	public List<GSTTaxSlabDO> retrieve() {
		return gSTTaxSlabDAO.retrieve();
	}

	@Transactional
	public boolean update(GSTTaxSlabDO gstTaxDO) {
		return gSTTaxSlabDAO.update(gstTaxDO);
	}
	
	@Transactional
	public boolean persistList(List<GSTTaxSlabDO> gstTaxDO) {
		return gSTTaxSlabDAO.persistList(gstTaxDO);
	}
}
