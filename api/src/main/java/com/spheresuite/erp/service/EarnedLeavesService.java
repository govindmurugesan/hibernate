package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.EarnedLeavesDAO;
import com.spheresuite.erp.domainobject.EarnedLeavesDO;
@Service
@Transactional
public class EarnedLeavesService {
	static Logger logger = Logger.getLogger(EarnedLeavesService.class.getName());
	
	@Autowired
	private EarnedLeavesDAO earnedLeavesDAO;
	
	@Transactional
	public boolean persistList(List<EarnedLeavesDO> earnedLeaveDO) {
		return earnedLeavesDAO.persistList(earnedLeaveDO);
	}
	
	@Transactional
	public List<EarnedLeavesDO> retrieve() {
		return earnedLeavesDAO.retrieve();
	}
	
	@Transactional
	public List<EarnedLeavesDO> retrieveByEmpId(String empId)  {
		return earnedLeavesDAO.retrieveByEmpId(empId);
	}


	/*@Transactional
	public Boolean persist(EmployeePFInfoDO employeePF_ESIInfoDO)  {
		return employeePFInfoDAO.persist(employeePF_ESIInfoDO);
	}


	@Transactional
	public List<EmployeePFInfoDO> retrieveById(Long id)  {
		return employeePFInfoDAO.retrieveById(id);
	}

	@Transactional
	public EmployeePFInfoDO update(EmployeePFInfoDO employeePF_ESIInfoDO)  {
		return employeePFInfoDAO.update(employeePF_ESIInfoDO);
	}*/
	
	
}
