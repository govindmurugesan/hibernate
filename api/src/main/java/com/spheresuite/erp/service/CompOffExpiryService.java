package com.spheresuite.erp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.CompOffExpiryDAO;
import com.spheresuite.erp.domainobject.CompOffExpiryDO;

@Service
@Transactional
public class CompOffExpiryService {

	@Autowired
	private CompOffExpiryDAO compOffExpiryDAO;
	
	@Transactional
	public boolean persist(CompOffExpiryDO compOffExpiryDO){
		return compOffExpiryDAO.persist(compOffExpiryDO);
	}
	
	@Transactional
	public boolean update(CompOffExpiryDO compOffExpiryDO){
		return compOffExpiryDAO.update(compOffExpiryDO);
	}

	@Transactional
	public List<CompOffExpiryDO> retrieveById(Long id){
		return compOffExpiryDAO.retrieveById(id);
	}
	
	@Transactional
	public List<CompOffExpiryDO> retrieve(){
		return compOffExpiryDAO.retrieve();
	}
}
