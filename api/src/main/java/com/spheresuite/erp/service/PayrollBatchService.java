package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.PayrollBatchDAO;
import com.spheresuite.erp.domainobject.PayrollBatchDO;

@Service
@Transactional
public class PayrollBatchService {
	static Logger logger = Logger.getLogger(PayrollBatchService.class.getName());
	
	@Autowired
	private PayrollBatchDAO payrollBatchDAO;
	
	@Transactional
	public boolean persist(PayrollBatchDO batchDO) {
		return payrollBatchDAO.persist(batchDO);
	}

	@Transactional
	public List<PayrollBatchDO> retrieveActive() {
		return payrollBatchDAO.retrieveActive();
	}
	
	@Transactional
	public List<PayrollBatchDO> retrieveById(Long id) {
		return payrollBatchDAO.retrieveById(id);
	}
	
	@Transactional
	public List<PayrollBatchDO> retrieveByUnitId(Long id) {
		return payrollBatchDAO.retrieveByUnitId(id);
	}
	
	@Transactional
	public List<PayrollBatchDO> retrieve() {
		return payrollBatchDAO.retrieve();
	}
	
	@Transactional
	public boolean update(PayrollBatchDO batchDO) {
		return payrollBatchDAO.update(batchDO);
	}
	
	@Transactional
	public List<PayrollBatchDO> retrieveByName(String name) {
		return payrollBatchDAO.retrieveByName(name);
	}
	
	@Transactional
	public List<PayrollBatchDO> retrieveByPayrollType(String type) {
		return payrollBatchDAO.retrieveByPayrollType(type);
	}
	
	
	
	@Transactional
	public boolean persistList(List<PayrollBatchDO> batchDO) {
		return payrollBatchDAO.persistList(batchDO);
	}
	
}
