package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.EmployeeTerminateDAO;
import com.spheresuite.erp.domainobject.EmployeeTerminateDO;
@Service
@Transactional
public class EmployeeTerminateService {
	static Logger logger = Logger.getLogger(EmployeeTerminateService.class.getName());
	@Autowired
	private EmployeeTerminateDAO employeeTerminateDAO;
	
	@Transactional
	public EmployeeTerminateDO persist(EmployeeTerminateDO employeeTerminateDO) {
		return employeeTerminateDAO.persist(employeeTerminateDO);
	}

	@Transactional
	public List<EmployeeTerminateDO> retrieveByEmpId(String empId) {
		return employeeTerminateDAO.retrieveByEmpId(empId);
	}

	@Transactional
	public List<EmployeeTerminateDO> retrieveById(Long id) {
		return employeeTerminateDAO.retrieveById(id);
	}

	@Transactional
	public List<EmployeeTerminateDO> delete(Long id) {
		return employeeTerminateDAO.delete(id);
	}

	@Transactional
	public List<EmployeeTerminateDO> retrieve() {
		return employeeTerminateDAO.retrieve();
	}

	@Transactional
	public EmployeeTerminateDO update(EmployeeTerminateDO EmployeeTerminateDO) {
		return employeeTerminateDAO.update(EmployeeTerminateDO);
	}
}
