package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.LeaveGroupDAO;
import com.spheresuite.erp.domainobject.LeaveGroupDO;
@Service
@Transactional
public class LeaveGroupService {
	static Logger logger = Logger.getLogger(HrRequestTypeService.class.getName());
	@Autowired
	private LeaveGroupDAO leaveGroupDAO;

	@Transactional
	public boolean persist(LeaveGroupDO leaveGroup) {
		return leaveGroupDAO.persist(leaveGroup);
	}

	@Transactional
	public List<LeaveGroupDO> retrieve() {
		return leaveGroupDAO.retrieve();
	}

	/*@Transactional
	public List<LeaveGroupDO> retrieveByType(String type) {
		return leaveTypeDAO.retrieveByType(type);
	}*/

	/*@Transactional
	public List<LeaveGroupDO> retrieveByTypeUpdate(String type, Long id) {
		return leaveTypeDAO.retrieveByTypeUpdate(type, id);
	}
*/
	
	
	
	@Transactional
	public List<LeaveGroupDO> retrieveByName(String name) {
		return leaveGroupDAO.retrieveByName(name);
	}
	
	@Transactional
	public List<LeaveGroupDO> retrieveById(Long Id) {
		return leaveGroupDAO.retrieveById(Id);
	}
	
	@Transactional
	public List<LeaveGroupDO> retrieveActive() {
		return leaveGroupDAO.retrieveActive();
	}
	
	@Transactional
	public boolean update(LeaveGroupDO leaveGroup) {
		return leaveGroupDAO.update(leaveGroup);
	}
	
	@Transactional
	public boolean persistList(List<LeaveGroupDO> leaveGroup) {
		return leaveGroupDAO.persistList(leaveGroup);
	}

}
