package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.EmployeeContactInfoDAO;
import com.spheresuite.erp.domainobject.EmployeeContactInfoDO;
@Service
@Transactional
public class EmployeeContactInfoService {
	static Logger logger = Logger.getLogger(EmployeeContactInfoService.class.getName());
	@Autowired
	private EmployeeContactInfoDAO employeeContactInfoDAO;
	

	@Transactional
	public EmployeeContactInfoDO persist(EmployeeContactInfoDO employeeContactInfoDO) {
		return employeeContactInfoDAO.persist(employeeContactInfoDO);
	}

	@Transactional
	public List<EmployeeContactInfoDO> retrieveByEmpId(String empId) {
		return employeeContactInfoDAO.retrieveByEmpId(empId);
	}

	@Transactional
	public List<EmployeeContactInfoDO> retrieveById(Long id) {
		return employeeContactInfoDAO.retrieveById(id);
	}

	@Transactional
	public EmployeeContactInfoDO update(EmployeeContactInfoDO employeeContactInfoDO) {
		return employeeContactInfoDAO.update(employeeContactInfoDO);
	}
	
	@Transactional
	public boolean persistList(List<EmployeeContactInfoDO> employeeContactInfoDO) {
		return employeeContactInfoDAO.persistList(employeeContactInfoDO);
	}
}
