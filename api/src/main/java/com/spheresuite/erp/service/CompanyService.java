package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.CompanyDAO;
import com.spheresuite.erp.domainobject.CompanyDO;

@Service
@Transactional
public class CompanyService {
	static Logger logger = Logger.getLogger(CompanyService.class.getName());
	
	@Autowired
	private CompanyDAO companyDAO;
	
	@Transactional
	public CompanyDO persist(CompanyDO countryDO)  {
		return companyDAO.persist(countryDO);
	}
	
	@Transactional
	public List<CompanyDO> retrieveById(Long Id)  {
		return companyDAO.retrieveById(Id);
	}
	
	@Transactional
	public List<CompanyDO> retrieve()  {
		return companyDAO.retrieve();
	}
	
	@Transactional
	public CompanyDO update(CompanyDO countryDO)  {
		return companyDAO.update(countryDO);
	}
}
