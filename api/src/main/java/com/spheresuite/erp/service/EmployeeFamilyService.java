package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.EmployeeFamilyDAO;
import com.spheresuite.erp.domainobject.EmployeeFamilyDO;
@Service
@Transactional
public class EmployeeFamilyService {
	static Logger logger = Logger.getLogger(EmployeeFamilyService.class.getName());
	@Autowired
	private EmployeeFamilyDAO employeeFamilyDAO;

	@Transactional
	public EmployeeFamilyDO persist(EmployeeFamilyDO employeeFamilyDO) {
		return employeeFamilyDAO.persist(employeeFamilyDO);
	}

	@Transactional
	public List<EmployeeFamilyDO> retrieveByEmpId(String empId) {
		return employeeFamilyDAO.retrieveByEmpId(empId);
	}

	@Transactional
	public List<EmployeeFamilyDO> retrieveById(Long id) {
		return employeeFamilyDAO.retrieveById(id);
	}

	@Transactional
	public EmployeeFamilyDO update(EmployeeFamilyDO employeeFamilyDO) {
		return employeeFamilyDAO.update(employeeFamilyDO);
	}
}
