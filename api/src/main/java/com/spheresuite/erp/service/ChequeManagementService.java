package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.ChequeManagementDAO;
import com.spheresuite.erp.domainobject.ChequeManagementDO;

@Service
@Transactional
public class ChequeManagementService {
	static Logger logger = Logger.getLogger(ChequeManagementService.class.getName());
	
	@Autowired
	private ChequeManagementDAO chequeManagementDAO;
	
	@Transactional
	public ChequeManagementDO persist(ChequeManagementDO chequeManagementDO)  {
		return chequeManagementDAO.persist(chequeManagementDO);
	}
	
	@Transactional
	public List<ChequeManagementDO> retriveById(Long id)  {
		return chequeManagementDAO.retrieveById(id);
	}
	
	@Transactional
	public List<ChequeManagementDO> retrieveActive()  {
		return chequeManagementDAO.retrieveActive();
	}
	
	@Transactional
	public List<ChequeManagementDO> retrieve()  {
		return chequeManagementDAO.retrieve();
	}
	
	@Transactional
	public ChequeManagementDO update(ChequeManagementDO chequeManagementDO)  {
		return chequeManagementDAO.update(chequeManagementDO);
	}
	
	
}
