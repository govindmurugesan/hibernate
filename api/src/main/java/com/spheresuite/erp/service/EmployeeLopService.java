package com.spheresuite.erp.service;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.EmployeeLopDAO;
import com.spheresuite.erp.domainobject.EmployeeLopDO;
@Service
@Transactional
public class EmployeeLopService {
	static Logger logger = Logger.getLogger(EmployeeLopService.class.getName());
	@Autowired
	private EmployeeLopDAO employeeLopDAO;
	
	@Transactional
	public EmployeeLopDO persist(EmployeeLopDO employeeLopDO) {
		return employeeLopDAO.persist(employeeLopDO);
	}

	@Transactional
	public List<EmployeeLopDO> retrieveByEmpId(String empId) {
		return employeeLopDAO.retrieveByEmpId(empId);
	}

	@Transactional
	public List<EmployeeLopDO> retrieve() {
		return employeeLopDAO.retrieve();
	}

	@Transactional
	public List<EmployeeLopDO> retrieveById(Long id) {
		return employeeLopDAO.retrieveById(id);
	}

	@Transactional
	public EmployeeLopDO update(EmployeeLopDO employeeLopDO) {
		return employeeLopDAO.update(employeeLopDO);
	}
	
	@Transactional
	public boolean delete(Long id) {
		return employeeLopDAO.delete(id);
	}
	
	@Transactional
	public boolean deleteByDate(String empId, Date startDate, Date endDate) {
		return employeeLopDAO.deleteByDate(empId, startDate, endDate);
	}

	@Transactional
	public List<EmployeeLopDO> retrieveByEmpWithDate(List<String> empIds, String fromMonth){
		return employeeLopDAO.retrieveByEmpWithDate(empIds, fromMonth);
	}
	
	@Transactional
	public List<EmployeeLopDO> retrieveByEmpDate(String empId, Date startDate, Date endDate) {
		return employeeLopDAO.retrieveByEmpDate(empId, startDate, endDate);
	}
	
	@Transactional
	public List<EmployeeLopDO> retrieveByEmpIdDateStatus(String empId, Date startDate, Date endDate) {
		return employeeLopDAO.retrieveByEmpIdDateStatus(empId, startDate, endDate);
	}
	
	
	@Transactional
	public List<EmployeeLopDO> retrieveByEmpDateForUpdate(Long id,String empId, Date startDate, Date endDate) {
		return employeeLopDAO.retrieveByEmpDateForUpdate(id,empId, startDate, endDate);
	}

	@Transactional
	public List<EmployeeLopDO> retrieveByEmpIdBetweenDate(List<String> empIds, String fromMonth, String toMonth) {
		return employeeLopDAO.retrieveByEmpIdBetweenDate(empIds, fromMonth, toMonth);
	}
}
