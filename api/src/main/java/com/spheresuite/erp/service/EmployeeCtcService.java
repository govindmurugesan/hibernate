package com.spheresuite.erp.service;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.EmployeeCtcDAO;
import com.spheresuite.erp.domainobject.EmployeeCtcDO;
@Service
@Transactional
public class EmployeeCtcService {
	static Logger logger = Logger.getLogger(EmployeeCtcService.class.getName());
	
	@Autowired
	private EmployeeCtcDAO employeeCtcDAO;
	

	@Transactional
	public EmployeeCtcDO persist(EmployeeCtcDO empCompensationDO) {
		return employeeCtcDAO.persist(empCompensationDO);
	}

	@Transactional
	public List<EmployeeCtcDO> retrieveActive(String empId) {
		return employeeCtcDAO.retrieveActive(empId);
	}

	@Transactional
	public List<EmployeeCtcDO> retrieveEmpIdeWithDate(String empId, Date date) {
		return employeeCtcDAO.retrieveEmpIdeWithDate(empId, date);
	}
	
	@Transactional
	public List<EmployeeCtcDO> retrieveByEmpCtc(Long ctc,String empId) {
		return employeeCtcDAO.retrieveByEmpCtc(ctc, empId);
	}

	@Transactional
	public List<EmployeeCtcDO> retrieveById(Long id) {
		return employeeCtcDAO.retrieveById(id);
	}

	@Transactional
	public List<Long> retrieveByBatchId(Long id) {
		return employeeCtcDAO.retrieveByBatchId(id);
	}

	@Transactional
	public List<EmployeeCtcDO> retrieveByEmpId(String empId) {
		return employeeCtcDAO.retrieveByEmpId(empId);
	}

	@Transactional
	public List<EmployeeCtcDO> retrieve() {
		return employeeCtcDAO.retrieve();
	}
	
	@Transactional
	public List<EmployeeCtcDO> retrieveAll() {
		return employeeCtcDAO.retrieveAll();
	}

	@Transactional
	public EmployeeCtcDO update(EmployeeCtcDO empCompensationDO) {
		return employeeCtcDAO.update(empCompensationDO);
	}
	
	@Transactional
	public boolean persistList(List<EmployeeCtcDO> empCompensationDO) {
		return employeeCtcDAO.persistList(empCompensationDO);
	}
	
	@Transactional
	public List<EmployeeCtcDO> retrieveBatchAndMonth(Long batchId, Date date) {
		return employeeCtcDAO.retrieveBatchAndMonth(batchId, date);
	}
	
	@Transactional
	public List<EmployeeCtcDO> retrieveWeeklyBatchWithDate(Long batchId, Date date) {
		return employeeCtcDAO.retrieveWeeklyBatchWithDate(batchId, date);
	}
	
	@Transactional
	public List<EmployeeCtcDO> retrieveDailyBatchWithDate(Long batchId, Date date) {
		return employeeCtcDAO.retrieveDailyBatchWithDate(batchId, date);
	}

	
}
