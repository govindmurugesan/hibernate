package com.spheresuite.erp.ams.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.AMSOfficeDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class AMSOfficeDAO {
	static Logger logger = Logger.getLogger(AMSOfficeDAO.class.getName());
	@Autowired
	private GenericDAOImpl<AMSOfficeDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(AMSOfficeDO officeDO) {
		boolean persistStatus = true;
		try {
			List<AMSOfficeDO> officeDOList = null;
			officeDOList = this.sessionFactory.getCurrentSession().getNamedQuery(AMSOfficeDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, officeDO.getOffice_name())
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
					if(officeDOList != null && officeDOList.size() > 0){
						persistStatus = false;
					}else{
						genericObject.persist(officeDO);
					}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	
	public List<AMSOfficeDO> retrieve() {
		List<AMSOfficeDO> officeDOList = null;
		try {
			officeDOList = genericObject.retrieve(AMSOfficeDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return officeDOList;
	}
	
	public List<AMSOfficeDO> retrieveActive() {
		List<AMSOfficeDO> officeDOList = null;
		try {
			return genericObject.retrieveActive(CommonConstants.ACTIVE, 
					AMSOfficeDO.FIND_BY_STATUS, AMSOfficeDO.class);
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return officeDOList;
	}
	
	@SuppressWarnings("unchecked")
	public List<AMSOfficeDO> retrieveById(Long Id) {
		List<AMSOfficeDO> officeDOList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(AMSOfficeDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, Id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return officeDOList;
	}

	
	@SuppressWarnings("unchecked")
	public boolean update(AMSOfficeDO domainDO) {
		boolean updateStatus = true;
		try {
			List<AMSOfficeDO> officeDOList = null;
			officeDOList = this.sessionFactory.getCurrentSession().getNamedQuery(AMSOfficeDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, domainDO.getOffice_name())
					.setParameter(CommonConstants.ID, domainDO.getOfficeId())
					.setParameter(CommonConstants.STATUS, domainDO.getStatus())
					.list();
			if(officeDOList != null && officeDOList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(domainDO);
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return updateStatus;
	}

}
