package com.spheresuite.erp.ams.rs;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.ams.service.AMSTypeService;
import com.spheresuite.erp.ams.web.util.AMSTypeUtil;
import com.spheresuite.erp.domainobject.AMSTypeDO;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/ams_type")
public class AMSTypeRS {

	String validation = null;
	static Logger logger = Logger.getLogger(AMSTypeRS.class.getName());

	@Autowired
	private AMSTypeService amsTypeService;
	
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) { 
			
		try {
			if (WebManager.authenticateSession(request)) {
				AMSTypeDO amsTypeDO = new AMSTypeDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
			 			amsTypeDO.setTypeName(inputJSON.get(CommonConstants.NAME).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.COMMENT) != null && !inputJSON.get(CommonConstants.COMMENT).toString().isEmpty()){
			 			amsTypeDO.setTypeComments(inputJSON.get(CommonConstants.COMMENT).toString());
			 		}
			 		amsTypeDO.setUpdatedon(new Date());
			 		amsTypeDO.setCreatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY)!= null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			amsTypeDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			amsTypeDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		amsTypeDO.setStatus(CommonConstants.ACTIVE);
			 	}
			 	if(!amsTypeService.persist(amsTypeDO)){
					return CommonWebUtil.buildErrorResponse("Type Already Added").toString();
				}
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Type Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retrieveActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<AMSTypeDO> typeList = amsTypeService.retrieveActive();
				respJSON = AMSTypeUtil.getTypeList(typeList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<AMSTypeDO> typeList = amsTypeService.retrieve();
				respJSON = AMSTypeUtil.getTypeList(typeList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				AMSTypeDO typeDO = new AMSTypeDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<AMSTypeDO> unitList = amsTypeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		typeDO = unitList.get(0);
			 		if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
			 			typeDO.setTypeName(inputJSON.get(CommonConstants.NAME).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.COMMENT) != null && !inputJSON.get(CommonConstants.COMMENT).toString().isEmpty()){
			 			typeDO.setTypeComments(inputJSON.get(CommonConstants.COMMENT).toString());
			 		}
			 		typeDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY)!= null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			typeDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		typeDO.setStatus((char) (inputJSON != null ? inputJSON.get(CommonConstants.STATUS).toString().charAt(0) : ""));
			 		if(!amsTypeService.update(typeDO)){
						return CommonWebUtil.buildErrorResponse("Type Already Added").toString();
					}
			 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Type Updated");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
}
