package com.spheresuite.erp.ams.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.AMSStockDO;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;

@Component
public class AMSStockUtil {

	private AMSStockUtil() {
	}

	public static JSONObject getStockList(List<AMSStockDO> stockListDO) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (AMSStockDO stockDO : stockListDO) {
				resultJSONArray.put(getMakeObject(stockDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getMakeObject(AMSStockDO stockDO)
			throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(stockDO.getStockId()));
		result.put(CommonConstants.NAME, String.valueOf(stockDO.getStock_name()));
		result.put(CommonConstants.STATUS,
				String.valueOf(stockDO.getStatus()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil
				.convertDateToStringWithtime(stockDO.getUpdatedon())));
		

		if (stockDO.getStock_comments() != null) {
			result.put(CommonConstants.COMMENT,
					String.valueOf(stockDO.getStock_comments()));
		} else {
			result.put(CommonConstants.COMMENT, "");
		}
		return result;
	}
}
