package com.spheresuite.erp.ams.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.AMSTypeDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class AMSTypeDAO {
	static Logger logger = Logger.getLogger(AMSTypeDAO.class.getName());
	@Autowired
	private GenericDAOImpl<AMSTypeDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(AMSTypeDO typeDO) {
		boolean persistStatus = true;
		try {
			List<AMSTypeDO> typeDOList = null;
			typeDOList = this.sessionFactory.getCurrentSession().getNamedQuery(AMSTypeDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, typeDO.getTypeName())
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
					if(typeDOList != null && typeDOList.size() > 0){
						persistStatus = false;
					}else{
						genericObject.persist(typeDO);
					}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	
	public List<AMSTypeDO> retrieve() {
		List<AMSTypeDO> typeDOList = null;
		try {
			typeDOList = genericObject.retrieve(AMSTypeDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return typeDOList;
	}
	
	public List<AMSTypeDO> retrieveActive() {
		List<AMSTypeDO> typeDOList = null;
		try {
			return genericObject.retrieveActive(CommonConstants.ACTIVE, 
					AMSTypeDO.FIND_BY_STATUS, AMSTypeDO.class);
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return typeDOList;
	}
	
	@SuppressWarnings("unchecked")
	public List<AMSTypeDO> retrieveById(Long Id) {
		List<AMSTypeDO> typeDOList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(AMSTypeDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, Id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return typeDOList;
	}

	
	@SuppressWarnings("unchecked")
	public boolean update(AMSTypeDO domainDO) {
		boolean updateStatus = true;
		try {
			List<AMSTypeDO> typeDOList = null;
			typeDOList = this.sessionFactory.getCurrentSession().getNamedQuery(AMSTypeDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, domainDO.getTypeName())
					.setParameter(CommonConstants.ID, domainDO.getTypeId())
					.setParameter(CommonConstants.STATUS, domainDO.getStatus())
					.list();
			if(typeDOList != null && typeDOList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(domainDO);
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return updateStatus;
	}

}
