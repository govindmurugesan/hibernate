package com.spheresuite.erp.ams.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.AMSMakeDO;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;

@Component
public class AMSMakeUtil {

	private AMSMakeUtil() {
	}


	public static JSONObject getMakeList(List<AMSMakeDO> makeList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (AMSMakeDO makeDO : makeList) {
				resultJSONArray.put(getMakeObject(makeDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getMakeObject(AMSMakeDO makeDO)
			throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(makeDO.getMakeId()));
		result.put(CommonConstants.NAME, String.valueOf(makeDO.getMake_name()));
		result.put(CommonConstants.STATUS,
				String.valueOf(makeDO.getStatus()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil
				.convertDateToStringWithtime(makeDO.getUpdatedon())));

		if (makeDO.getMake_comments() != null) {
			result.put(CommonConstants.COMMENT,
					String.valueOf(makeDO.getMake_comments()));
		} else {
			result.put(CommonConstants.COMMENT, "");
		}
		return result;
	}
}
