package com.spheresuite.erp.ams.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.ams.dao.AMSOSDAO;
import com.spheresuite.erp.domainobject.AMSOSDO;
@Service
@Transactional
public class AMSOSService {
	static Logger logger = Logger.getLogger(AMSOSService.class.getName());
	@Autowired
	private AMSOSDAO amsOsDAO;

	@Transactional
	public boolean persist(AMSOSDO osDO) {
		return amsOsDAO.persist(osDO);
	}

	@Transactional
	public List<AMSOSDO> retrieveActive() {
		return amsOsDAO.retrieveActive();
	}

	@Transactional
	public List<AMSOSDO> retrieveById(Long Id) {
		return amsOsDAO.retrieveById(Id);
	}

	@Transactional
	public List<AMSOSDO> retrieve() {
		return amsOsDAO.retrieve();
	}

	@Transactional
	public boolean update(AMSOSDO osDO) {
		return amsOsDAO.update(osDO);
	}

}
