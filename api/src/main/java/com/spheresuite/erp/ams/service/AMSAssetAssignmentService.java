package com.spheresuite.erp.ams.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.ams.dao.AMSAssetAssignmentDAO;
import com.spheresuite.erp.domainobject.AMSAssetAssignmentDO;
@Service
@Transactional
public class AMSAssetAssignmentService {
	static Logger logger = Logger.getLogger(AMSAssetAssignmentService.class.getName());
	@Autowired
	private AMSAssetAssignmentDAO amsAssetAssignmentDAO;

	@Transactional
	public boolean persist(AMSAssetAssignmentDO assetDetails) {
		return amsAssetAssignmentDAO.persist(assetDetails);
	}

	/*@Transactional
	public List<AMSAssetDO> retrieveActive() {
		return amsAssetDAO.retrieveActive();
	}*/

	@Transactional
	public List<AMSAssetAssignmentDO> retrieveById(Long Id) {
		return amsAssetAssignmentDAO.retrieveById(Id);
	}
	
	@Transactional
	public List<AMSAssetAssignmentDO> retrieveByEmpId(String Id) {
		return amsAssetAssignmentDAO.retrieveByEmpId(Id);
	}
	
	@Transactional
	public List<AMSAssetAssignmentDO> retrieveByUnit(Long Id) {
		return amsAssetAssignmentDAO.retrieveByUnit(Id);
	}

	@Transactional
	public List<AMSAssetAssignmentDO> retrieve() {
		return amsAssetAssignmentDAO.retrieve();
	}

	@Transactional
	public boolean update(AMSAssetAssignmentDO assetDetails) {
		return amsAssetAssignmentDAO.update(assetDetails);
	}
	
	@Transactional
	public List<AMSAssetAssignmentDO> retrieveByIdAndStatus(Long Id) {
		return amsAssetAssignmentDAO.retrieveByIdAndStatus(Id);
	}

}
