package com.spheresuite.erp.ams.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.ams.dao.AMSTypeDAO;
import com.spheresuite.erp.domainobject.AMSTypeDO;
@Service
@Transactional
public class AMSTypeService {
	static Logger logger = Logger.getLogger(AMSTypeService.class.getName());
	@Autowired
	private AMSTypeDAO amsTypeDAO;

	@Transactional
	public boolean persist(AMSTypeDO typeDO) {
		return amsTypeDAO.persist(typeDO);
	}

	@Transactional
	public List<AMSTypeDO> retrieveActive() {
		return amsTypeDAO.retrieveActive();
	}

	@Transactional
	public List<AMSTypeDO> retrieveById(Long Id) {
		return amsTypeDAO.retrieveById(Id);
	}

	@Transactional
	public List<AMSTypeDO> retrieve() {
		return amsTypeDAO.retrieve();
	}

	@Transactional
	public boolean update(AMSTypeDO typeDO) {
		return amsTypeDAO.update(typeDO);
	}

}
