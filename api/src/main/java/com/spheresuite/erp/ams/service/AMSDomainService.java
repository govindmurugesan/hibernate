package com.spheresuite.erp.ams.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.ams.dao.AMSDomainDAO;
import com.spheresuite.erp.domainobject.AMSDomainDO;
@Service
@Transactional
public class AMSDomainService {
	static Logger logger = Logger.getLogger(AMSDomainService.class.getName());
	@Autowired
	private AMSDomainDAO amsDoaminDAO;

	@Transactional
	public boolean persist(AMSDomainDO domainDO) {
		return amsDoaminDAO.persist(domainDO);
	}

	@Transactional
	public List<AMSDomainDO> retrieveActive() {
		return amsDoaminDAO.retrieveActive();
	}

	@Transactional
	public List<AMSDomainDO> retrieveById(Long Id) {
		return amsDoaminDAO.retrieveById(Id);
	}

	@Transactional
	public List<AMSDomainDO> retrieve() {
		return amsDoaminDAO.retrieve();
	}

	@Transactional
	public boolean update(AMSDomainDO domainDO) {
		return amsDoaminDAO.update(domainDO);
	}

}
