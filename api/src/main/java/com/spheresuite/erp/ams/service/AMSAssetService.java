package com.spheresuite.erp.ams.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.ams.dao.AMSAssetDAO;
import com.spheresuite.erp.domainobject.AMSAssetDO;
@Service
@Transactional
public class AMSAssetService {
	static Logger logger = Logger.getLogger(AMSAssetService.class.getName());
	@Autowired
	private AMSAssetDAO amsAssetDAO;

	@Transactional
	public boolean persist(AMSAssetDO assetDO) {
		return amsAssetDAO.persist(assetDO);
	}

	/*@Transactional
	public List<AMSAssetDO> retrieveActive() {
		return amsAssetDAO.retrieveActive();
	}*/

	@Transactional
	public List<AMSAssetDO> retrieveById(Long Id) {
		return amsAssetDAO.retrieveById(Id);
	}
	@Transactional
	public List<AMSAssetDO> retrieveForId() {
		return amsAssetDAO.retrieveForId();
	}
	
	@Transactional
	public List<AMSAssetDO> retrieveByUnit(Long Id) {
		return amsAssetDAO.retrieveByUnit(Id);
	}

	@Transactional
	public List<AMSAssetDO> retrieve() {
		return amsAssetDAO.retrieve();
	}

	@Transactional
	public boolean update(AMSAssetDO assetDO) {
		return amsAssetDAO.update(assetDO);
	}

}
