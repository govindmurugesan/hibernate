package com.spheresuite.erp.ams.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.ams.dao.AMSStockDAO;
import com.spheresuite.erp.domainobject.AMSStockDO;
@Service
@Transactional
public class AMSStockService {
	static Logger logger = Logger.getLogger(AMSStockService.class.getName());
	@Autowired
	private AMSStockDAO amsStockDAO;

	@Transactional
	public boolean persist(AMSStockDO amsStockDO) {
		return amsStockDAO.persist(amsStockDO);
	}

	@Transactional
	public List<AMSStockDO> retrieveActive() {
		return amsStockDAO.retrieveActive();
	}

	@Transactional
	public List<AMSStockDO> retrieveById(Long Id) {
		return amsStockDAO.retrieveById(Id);
	}

	@Transactional
	public List<AMSStockDO> retrieve() {
		return amsStockDAO.retrieve();
	}

	@Transactional
	public boolean update(AMSStockDO amsStockDO) {
		return amsStockDAO.update(amsStockDO);
	}

}
