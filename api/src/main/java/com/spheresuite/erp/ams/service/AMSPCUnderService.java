package com.spheresuite.erp.ams.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.ams.dao.AMSPCUnderDAO;
import com.spheresuite.erp.domainobject.AMSPCUnderDO;
@Service
@Transactional
public class AMSPCUnderService {
	static Logger logger = Logger.getLogger(AMSPCUnderService.class.getName());
	@Autowired
	private AMSPCUnderDAO amspcUnderDAO;

	@Transactional
	public boolean persist(AMSPCUnderDO pcUnderDO) {
		return amspcUnderDAO.persist(pcUnderDO);
	}

	@Transactional
	public List<AMSPCUnderDO> retrieveActive() {
		return amspcUnderDAO.retrieveActive();
	}

	@Transactional
	public List<AMSPCUnderDO> retrieveById(Long Id) {
		return amspcUnderDAO.retrieveById(Id);
	}

	@Transactional
	public List<AMSPCUnderDO> retrieve() {
		return amspcUnderDAO.retrieve();
	}

	@Transactional
	public boolean update(AMSPCUnderDO pcUnderDO) {
		return amspcUnderDAO.update(pcUnderDO);
	}

}
