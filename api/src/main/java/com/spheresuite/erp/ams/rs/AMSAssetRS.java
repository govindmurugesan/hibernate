package com.spheresuite.erp.ams.rs;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.ams.service.AMSAssetService;
import com.spheresuite.erp.ams.service.AMSCD_DVDService;
import com.spheresuite.erp.ams.service.AMSCPUSpeedService;
import com.spheresuite.erp.ams.service.AMSCPUTypeService;
import com.spheresuite.erp.ams.service.AMSDepartmentService;
import com.spheresuite.erp.ams.service.AMSDomainService;
import com.spheresuite.erp.ams.service.AMSFloorService;
import com.spheresuite.erp.ams.service.AMSHddService;
import com.spheresuite.erp.ams.service.AMSMailService;
import com.spheresuite.erp.ams.service.AMSMakeService;
import com.spheresuite.erp.ams.service.AMSOSService;
import com.spheresuite.erp.ams.service.AMSOfficeService;
import com.spheresuite.erp.ams.service.AMSPCUnderService;
import com.spheresuite.erp.ams.service.AMSRamService;
import com.spheresuite.erp.ams.service.AMSTypeService;
import com.spheresuite.erp.ams.service.AMSUnitService;
import com.spheresuite.erp.ams.web.util.AMSAssetUtil;
import com.spheresuite.erp.domainobject.AMSAssetDO;
import com.spheresuite.erp.domainobject.AMSCD_DVDDO;
import com.spheresuite.erp.domainobject.AMSCPUSpeedDO;
import com.spheresuite.erp.domainobject.AMSCPUTypeDO;
import com.spheresuite.erp.domainobject.AMSDepartmentDO;
import com.spheresuite.erp.domainobject.AMSDomainDO;
import com.spheresuite.erp.domainobject.AMSFloorDO;
import com.spheresuite.erp.domainobject.AMSHddDO;
import com.spheresuite.erp.domainobject.AMSMailDO;
import com.spheresuite.erp.domainobject.AMSMakeDO;
import com.spheresuite.erp.domainobject.AMSOSDO;
import com.spheresuite.erp.domainobject.AMSOfficeDO;
import com.spheresuite.erp.domainobject.AMSPCUnderDO;
import com.spheresuite.erp.domainobject.AMSRamDO;
import com.spheresuite.erp.domainobject.AMSTypeDO;
import com.spheresuite.erp.domainobject.AMSUnitDO;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/ams_asset")
public class AMSAssetRS {

	String validation = null;
	static Logger logger = Logger.getLogger(AMSAssetRS.class.getName());

	@Autowired
	private AMSAssetService assetService;
	
	@Autowired
	private AMSUnitService amsUnitService;
	
	@Autowired
	private AMSDepartmentService amsdeptService;
	
	@Autowired
	private AMSFloorService amsFloorService;
	
	@Autowired
	private AMSDomainService amsDomainService;
	
	@Autowired
	private AMSPCUnderService amsPCUnderService;
	
	@Autowired
	private AMSTypeService amsTypeService;
	
	@Autowired
	private AMSMakeService amsMakeService;
	
	@Autowired
	private AMSCPUTypeService amsCpuTypeService;
	
	@Autowired
	private AMSCPUSpeedService amsCpuSpeedService;
	
	@Autowired
	private AMSRamService amsRamService;
	
	@Autowired
	private AMSHddService amsHddService;
	
	@Autowired
	private AMSCD_DVDService amsCDTypeService;
	
	@Autowired
	private AMSOfficeService amsOfficeService;
	
	@Autowired
	private AMSMailService amsMailService;
	
	@Autowired
	private AMSOSService amsOSService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) { 
		try {
			if (WebManager.authenticateSession(request)) {
				AMSAssetDO assetDO = new AMSAssetDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.UNIT) != null && !inputJSON.get(CommonConstants.UNIT).toString().isEmpty()){
			 			List<AMSUnitDO> unitList = amsUnitService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.UNIT).toString()));
			 			if(unitList != null && unitList.size() > 0){
			 				assetDO.setUnit(unitList.get(0));
			 			}
			 		}
			 		if(inputJSON.get(CommonConstants.ASSETNUMBER) != null && !inputJSON.get(CommonConstants.ASSETNUMBER).toString().isEmpty()){
			 			assetDO.setAsset_number( inputJSON.get(CommonConstants.ASSETNUMBER).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
			 			assetDO.setAsset_name(inputJSON.get(CommonConstants.NAME).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.DEPT) != null && !inputJSON.get(CommonConstants.DEPT).toString().isEmpty()){
			 			List<AMSDepartmentDO> deptDOList = amsdeptService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.DEPT).toString()));
			 			if(deptDOList != null && deptDOList.size() > 0){
			 				assetDO.setDepartment(deptDOList.get(0));
			 			}
			 		}
			 		if(inputJSON.get(CommonConstants.FLOOR) != null && !inputJSON.get(CommonConstants.FLOOR).toString().isEmpty()){
			 			List<AMSFloorDO> floorList = amsFloorService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.FLOOR).toString()));
			 			if(floorList != null && floorList.size() > 0){
			 				assetDO.setFloor(floorList.get(0));
			 			}
			 		}
			 		if(inputJSON.get(CommonConstants.DOMAIN) != null && !inputJSON.get(CommonConstants.DOMAIN).toString().isEmpty()){
			 			List<AMSDomainDO> domainList = amsDomainService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.DOMAIN).toString()));
			 			if(domainList != null && domainList.size() > 0){
			 				assetDO.setDomain(domainList.get(0));
			 			}
			 		}
			 		if(inputJSON.get(CommonConstants.PCUNDER) != null && !inputJSON.get(CommonConstants.PCUNDER).toString().isEmpty()){
			 			List<AMSPCUnderDO> pcUnderList = amsPCUnderService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.PCUNDER).toString()));
			 			if(pcUnderList != null && pcUnderList.size() > 0){
			 				assetDO.setPcUnder(pcUnderList.get(0));
			 			}
			 		}
			 		if(inputJSON.get(CommonConstants.PURCHASEDATE) != null && !inputJSON.get(CommonConstants.PURCHASEDATE).toString().isEmpty()){
			 			assetDO.setPurchaseDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.PURCHASEDATE).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.EXPIRYDATE) != null && !inputJSON.get(CommonConstants.EXPIRYDATE).toString().isEmpty()){
			 			assetDO.setExpiry(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.EXPIRYDATE).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.TYPE) != null && !inputJSON.get(CommonConstants.TYPE).toString().isEmpty()){
			 			List<AMSTypeDO> typeList = amsTypeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.TYPE).toString()));
			 			if(typeList != null && typeList.size() > 0){
			 				assetDO.setType(typeList.get(0));
			 			}
			 		}
			 		if(inputJSON.get(CommonConstants.MAKE) != null && !inputJSON.get(CommonConstants.MAKE).toString().isEmpty()){
			 			List<AMSMakeDO> makeList = amsMakeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.MAKE).toString()));
			 			if(makeList != null && makeList.size() > 0){
			 				assetDO.setMake(makeList.get(0));
			 			}
			 		}
			 		if(inputJSON.get(CommonConstants.MONITOR) != null && !inputJSON.get(CommonConstants.MONITOR).toString().isEmpty()){
			 			assetDO.setMonitor(inputJSON.get(CommonConstants.MONITOR).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.CPUTYPE) != null && !inputJSON.get(CommonConstants.CPUTYPE).toString().isEmpty()){
			 			List<AMSCPUTypeDO> CPUTypeList = amsCpuTypeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.CPUTYPE).toString()));
			 			if(CPUTypeList != null && CPUTypeList.size() > 0){
			 				assetDO.setCpuType(CPUTypeList.get(0));
			 			}
			 		}
			 		if(inputJSON.get(CommonConstants.CPUSPEED) != null && !inputJSON.get(CommonConstants.CPUSPEED).toString().isEmpty()){
			 			List<AMSCPUSpeedDO> amsCPUSpeedList = amsCpuSpeedService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.CPUSPEED).toString()));
			 			if(amsCPUSpeedList != null && amsCPUSpeedList.size() > 0){
			 				assetDO.setCpuSpeed(amsCPUSpeedList.get(0));
			 			}
			 		}
			 		if(inputJSON.get(CommonConstants.FDD) != null && !inputJSON.get(CommonConstants.FDD).toString().isEmpty()){
			 			assetDO.setFdd(inputJSON.get(CommonConstants.FDD).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.RAM) != null && !inputJSON.get(CommonConstants.RAM).toString().isEmpty()){
			 			List<AMSRamDO> ramList = amsRamService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.RAM).toString()));
			 			if(ramList != null && ramList.size() > 0){
			 				assetDO.setRam(ramList.get(0));
			 			}
			 		}
			 		if(inputJSON.get(CommonConstants.HDD) != null && !inputJSON.get(CommonConstants.HDD).toString().isEmpty()){
			 			List<AMSHddDO> hddList = amsHddService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.HDD).toString()));
			 			if(hddList != null && hddList.size() > 0){
			 				assetDO.setHdd(hddList.get(0));
			 			}
			 		}
			 		if(inputJSON.get(CommonConstants.CDD) != null && !inputJSON.get(CommonConstants.CDD).toString().isEmpty()){
			 			List<AMSCD_DVDDO> CDTypeList = amsCDTypeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.CDD).toString()));
			 			if(CDTypeList != null && CDTypeList.size() > 0){
			 				assetDO.setCdd_dvd(CDTypeList.get(0));
			 			}
			 			
			 		}
			 		if(inputJSON.get(CommonConstants.MSOFFICE) != null && !inputJSON.get(CommonConstants.MSOFFICE).toString().isEmpty()){
			 			List<AMSOfficeDO> amsOfficeList = amsOfficeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.MSOFFICE).toString()));
			 			if(amsOfficeList != null && amsOfficeList.size() > 0){
			 				assetDO.setOffice(amsOfficeList.get(0));
			 			}
			 		}
			 		if(inputJSON.get(CommonConstants.MAILID) != null && !inputJSON.get(CommonConstants.MAILID).toString().isEmpty()){
			 			List<AMSMailDO> mailList = amsMailService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.MAILID).toString()));
			 			if(mailList != null && mailList.size() >0){
			 				assetDO.setMail(mailList.get(0));
			 			}
			 		}
			 		if(inputJSON.get(CommonConstants.OSMODEL) != null && !inputJSON.get(CommonConstants.OSMODEL).toString().isEmpty()){
			 			List<AMSOSDO> osList = amsOSService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.OSMODEL).toString()));
			 			if(osList != null && osList.size() > 0){
			 				assetDO.setOSModel(osList.get(0));
			 			}
			 		}
			 		if(inputJSON.get(CommonConstants.INTERNET) != null && !inputJSON.get(CommonConstants.INTERNET).toString().isEmpty()){
			 			assetDO.setInternet(inputJSON.get(CommonConstants.INTERNET).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.USB) != null && !inputJSON.get(CommonConstants.USB).toString().isEmpty()){
			 			assetDO.setUsb(inputJSON.get(CommonConstants.USB).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.IPADDRESS) != null && !inputJSON.get(CommonConstants.IPADDRESS).toString().isEmpty()){
			 			assetDO.setIpAddress(inputJSON.get(CommonConstants.IPADDRESS).toString());
			 		}
			 		assetDO.setStatus(CommonConstants.INACTIVE);
			 		assetDO.setUpdatedon(new Date());
			 		assetDO.setCreatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			assetDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			assetDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 	}
			 	if(!assetService.persist(assetDO)){
					return CommonWebUtil.buildErrorResponse("Asset Already Added").toString();
				}
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Asset Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<AMSAssetDO> assetList = assetService.retrieve();
				if(assetList != null && assetList.size() >0){
					respJSON = AMSAssetUtil.getAssetList(assetList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveLastId", method = RequestMethod.GET)
	public @ResponseBody String retrieveLastId(Model model, HttpServletRequest request) {
		String assetNumber = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<AMSAssetDO> assetList = assetService.retrieveForId();
				if(assetList != null && assetList.size() >0){
					assetNumber = assetList.get(0).getAsset_number();
				}else{
					return CommonWebUtil.buildSuccessResponseId(assetNumber).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponseId(assetNumber).toString();
	}
	
	@RequestMapping(value = "/retriveById/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveById(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<AMSAssetDO> assetList = assetService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = AMSAssetUtil.getAssetList(assetList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	/*@RequestMapping(value = "/assignAsset/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String assignAsset(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<AMSAssetDO> remainderList = assetService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = AMSAssetUtil.getassetList(remainderList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	*/
	
	@RequestMapping(value = "/retriveByUnitId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveByUnitId(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<AMSUnitDO> unitList = amsUnitService.retrieveByUnitHead(inputJSON.get(CommonConstants.ID).toString());
					if(unitList != null && unitList.size() > 0){
						List<AMSAssetDO> assetList = assetService.retrieveByUnit(unitList.get(0).getUnitId());
						respJSON = AMSAssetUtil.getUnitAssetList(assetList).toString();
					}else{
						return CommonWebUtil.buildErrorResponse("Your Not An Unit Head").toString();
					}
					
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				AMSAssetDO assetDO = new AMSAssetDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<AMSAssetDO> assetList = assetService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		assetDO = assetList.get(0);
			 		if(inputJSON.get(CommonConstants.ASSETNUMBER) != null && !inputJSON.get(CommonConstants.ASSETNUMBER).toString().isEmpty()){
			 			assetDO.setAsset_number( inputJSON.get(CommonConstants.ASSETNUMBER).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
			 			assetDO.setAsset_name(inputJSON.get(CommonConstants.NAME).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.DEPT) != null && !inputJSON.get(CommonConstants.DEPT).toString().isEmpty()){
			 			List<AMSDepartmentDO> deptDOList = amsdeptService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.DEPT).toString()));
			 			if(deptDOList != null && deptDOList.size() > 0){
			 				assetDO.setDepartment(deptDOList.get(0));
			 			}
			 		}
			 		if(inputJSON.get(CommonConstants.FLOOR) != null && !inputJSON.get(CommonConstants.FLOOR).toString().isEmpty()){
			 			List<AMSFloorDO> floorList = amsFloorService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.FLOOR).toString()));
			 			if(floorList != null && floorList.size() > 0){
			 				assetDO.setFloor(floorList.get(0));
			 			}
			 		}
			 		if(inputJSON.get(CommonConstants.DOMAIN) != null && !inputJSON.get(CommonConstants.DOMAIN).toString().isEmpty()){
			 			List<AMSDomainDO> domainList = amsDomainService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.DOMAIN).toString()));
			 			if(domainList != null && domainList.size() > 0){
			 				assetDO.setDomain(domainList.get(0));
			 			}
			 		}
			 		if(inputJSON.get(CommonConstants.PCUNDERID) != null && !inputJSON.get(CommonConstants.PCUNDERID).toString().isEmpty()){
			 			List<AMSPCUnderDO> pcUnderList = amsPCUnderService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.PCUNDERID).toString()));
			 			if(pcUnderList != null && pcUnderList.size() > 0){
			 				assetDO.setPcUnder(pcUnderList.get(0));
			 			}
			 		}
			 		if(inputJSON.get(CommonConstants.PURCHASEDATE) != null && !inputJSON.get(CommonConstants.PURCHASEDATE).toString().isEmpty()){
			 			assetDO.setPurchaseDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.PURCHASEDATE).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.EXPIRYDATE) != null && !inputJSON.get(CommonConstants.EXPIRYDATE).toString().isEmpty()){
			 			assetDO.setExpiry(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.EXPIRYDATE).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.TYPE) != null && !inputJSON.get(CommonConstants.TYPE).toString().isEmpty()){
			 			List<AMSTypeDO> typeList = amsTypeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.TYPE).toString()));
			 			if(typeList != null && typeList.size() > 0){
			 				assetDO.setType(typeList.get(0));
			 			}
			 		}
			 		if(inputJSON.get(CommonConstants.MAKE) != null && !inputJSON.get(CommonConstants.MAKE).toString().isEmpty()){
			 			List<AMSMakeDO> makeList = amsMakeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.MAKE).toString()));
			 			if(makeList != null && makeList.size() > 0){
			 				assetDO.setMake(makeList.get(0));
			 			}
			 		}
			 		if(inputJSON.get(CommonConstants.MONITOR) != null && !inputJSON.get(CommonConstants.MONITOR).toString().isEmpty()){
			 			assetDO.setMonitor(inputJSON.get(CommonConstants.MONITOR).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.CPUTYPE) != null && !inputJSON.get(CommonConstants.CPUTYPE).toString().isEmpty()){
			 			List<AMSCPUTypeDO> CPUTypeList = amsCpuTypeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.CPUTYPE).toString()));
			 			if(CPUTypeList != null && CPUTypeList.size() > 0){
			 				assetDO.setCpuType(CPUTypeList.get(0));
			 			}
			 		}
			 		if(inputJSON.get(CommonConstants.CPUSPEED) != null && !inputJSON.get(CommonConstants.CPUSPEED).toString().isEmpty()){
			 			List<AMSCPUSpeedDO> amsCPUSpeedList = amsCpuSpeedService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.CPUSPEED).toString()));
			 			if(amsCPUSpeedList != null && amsCPUSpeedList.size() > 0){
			 				assetDO.setCpuSpeed(amsCPUSpeedList.get(0));
			 			}
			 		}
			 		if(inputJSON.get(CommonConstants.FDD) != null && !inputJSON.get(CommonConstants.FDD).toString().isEmpty()){
			 			assetDO.setFdd(inputJSON.get(CommonConstants.FDD).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.RAM) != null && !inputJSON.get(CommonConstants.RAM).toString().isEmpty()){
			 			List<AMSRamDO> ramList = amsRamService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.RAM).toString()));
			 			if(ramList != null && ramList.size() > 0){
			 				assetDO.setRam(ramList.get(0));
			 			}
			 		}
			 		if(inputJSON.get(CommonConstants.HDD) != null && !inputJSON.get(CommonConstants.HDD).toString().isEmpty()){
			 			List<AMSHddDO> hddList = amsHddService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.HDD).toString()));
			 			if(hddList != null && hddList.size() > 0){
			 				assetDO.setHdd(hddList.get(0));
			 			}
			 		}
			 		if(inputJSON.get(CommonConstants.CDD_DVDID) != null && !inputJSON.get(CommonConstants.CDD_DVDID).toString().isEmpty()){
			 			List<AMSCD_DVDDO> CDTypeList = amsCDTypeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.CDD_DVDID).toString()));
			 			if(CDTypeList != null && CDTypeList.size() > 0){
			 				assetDO.setCdd_dvd(CDTypeList.get(0));
			 			}
			 			
			 		}
			 		if(inputJSON.get(CommonConstants.MSOFFICE) != null && !inputJSON.get(CommonConstants.MSOFFICE).toString().isEmpty()){
			 			List<AMSOfficeDO> amsOfficeList = amsOfficeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.MSOFFICE).toString()));
			 			if(amsOfficeList != null && amsOfficeList.size() > 0){
			 				assetDO.setOffice(amsOfficeList.get(0));
			 			}
			 		}
			 		if(inputJSON.get(CommonConstants.MAILID) != null && !inputJSON.get(CommonConstants.MAILID).toString().isEmpty()){
			 			List<AMSMailDO> mailList = amsMailService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.MAILID).toString()));
			 			if(mailList != null && mailList.size() >0){
			 				assetDO.setMail(mailList.get(0));
			 			}
			 		}
			 		if(inputJSON.get(CommonConstants.OSMODEL) != null && !inputJSON.get(CommonConstants.OSMODEL).toString().isEmpty()){
			 			List<AMSOSDO> osList = amsOSService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.OSMODEL).toString()));
			 			if(osList != null && osList.size() > 0){
			 				assetDO.setOSModel(osList.get(0));
			 			}
			 		}
			 		if(inputJSON.get(CommonConstants.INTERNET) != null && !inputJSON.get(CommonConstants.INTERNET).toString().isEmpty()){
			 			assetDO.setInternet(inputJSON.get(CommonConstants.INTERNET).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.USB) != null && !inputJSON.get(CommonConstants.USB).toString().isEmpty()){
			 			assetDO.setUsb(inputJSON.get(CommonConstants.USB).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.IPADDRESS) != null && !inputJSON.get(CommonConstants.IPADDRESS).toString().isEmpty()){
			 			assetDO.setIpAddress(inputJSON.get(CommonConstants.IPADDRESS).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
			 			assetDO.setStatus((char) (inputJSON.get(CommonConstants.STATUS).toString().charAt(0)));
			 		}
			 		assetDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			assetDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		if(!assetService.update(assetDO)){
						return CommonWebUtil.buildErrorResponse("Asset Already Added").toString();
					}
			 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Asset Updated");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	
	
}
