package com.spheresuite.erp.ams.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.AMSHddDO;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;

@Component
public class AMSHddUtil {

	private AMSHddUtil() {
	}

	public static JSONObject getHddList(List<AMSHddDO> amsHddDOList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (AMSHddDO amsHddDO : amsHddDOList) {
				resultJSONArray.put(getHDDObject(amsHddDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getHDDObject(AMSHddDO amsHddDO)
			throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(amsHddDO.getHddId()));
		result.put(CommonConstants.NAME, String.valueOf(amsHddDO.getHdd()));
		result.put(CommonConstants.STATUS,
				String.valueOf(amsHddDO.getStatus()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil
				.convertDateToStringWithtime(amsHddDO.getUpdatedon())));

		if (amsHddDO.getHdd_comments() != null) {
			result.put(CommonConstants.COMMENT,
					String.valueOf(amsHddDO.getHdd_comments()));
		} else {
			result.put(CommonConstants.COMMENT, "");
		}
		return result;
	}
}
