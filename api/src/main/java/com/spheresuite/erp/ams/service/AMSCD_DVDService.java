package com.spheresuite.erp.ams.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.ams.dao.AMSCD_DVDDAO;
import com.spheresuite.erp.domainobject.AMSCD_DVDDO;
@Service
@Transactional
public class AMSCD_DVDService {
	static Logger logger = Logger.getLogger(AMSCD_DVDService.class.getName());
	@Autowired
	private AMSCD_DVDDAO amsCDTypeDAO;

	@Transactional
	public boolean persist(AMSCD_DVDDO makeDO) {
		return amsCDTypeDAO.persist(makeDO);
	}

	@Transactional
	public List<AMSCD_DVDDO> retrieveActive() {
		return amsCDTypeDAO.retrieveActive();
	}

	@Transactional
	public List<AMSCD_DVDDO> retrieveById(Long Id) {
		return amsCDTypeDAO.retrieveById(Id);
	}

	@Transactional
	public List<AMSCD_DVDDO> retrieve() {
		return amsCDTypeDAO.retrieve();
	}

	@Transactional
	public boolean update(AMSCD_DVDDO makeDO) {
		return amsCDTypeDAO.update(makeDO);
	}

}
