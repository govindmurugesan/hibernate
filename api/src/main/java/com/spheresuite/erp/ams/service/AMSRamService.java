package com.spheresuite.erp.ams.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.ams.dao.AMSRamDAO;
import com.spheresuite.erp.domainobject.AMSRamDO;
@Service
@Transactional
public class AMSRamService {
	static Logger logger = Logger.getLogger(AMSRamService.class.getName());
	@Autowired
	private AMSRamDAO amsRamDAO;

	@Transactional
	public boolean persist(AMSRamDO ramDO) {
		return amsRamDAO.persist(ramDO);
	}

	@Transactional
	public List<AMSRamDO> retrieveActive() {
		return amsRamDAO.retrieveActive();
	}

	@Transactional
	public List<AMSRamDO> retrieveById(Long Id) {
		return amsRamDAO.retrieveById(Id);
	}

	@Transactional
	public List<AMSRamDO> retrieve() {
		return amsRamDAO.retrieve();
	}

	@Transactional
	public boolean update(AMSRamDO ramDO) {
		return amsRamDAO.update(ramDO);
	}

	/*@Transactional
	public List<AMSFloorDO> retrieveByName(String name) {
		return frequencyDAO.retrieveByName(name);
	}*/
}
