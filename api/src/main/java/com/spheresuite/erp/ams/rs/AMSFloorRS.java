package com.spheresuite.erp.ams.rs;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.ams.service.AMSFloorService;
import com.spheresuite.erp.ams.web.util.AMSFloorUtil;
import com.spheresuite.erp.domainobject.AMSFloorDO;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/ams_floor")
public class AMSFloorRS {

	String validation = null;
	static Logger logger = Logger.getLogger(AMSFloorRS.class.getName());

	@Autowired
	private AMSFloorService amsFloorService;

	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) { 
		try {
			if (WebManager.authenticateSession(request)) {
				AMSFloorDO amsFloorDO = new AMSFloorDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.NAME) != null &&  !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
			 			amsFloorDO.setFloorName(inputJSON.get(CommonConstants.NAME).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.COMMENT) != null  && !inputJSON.get(CommonConstants.COMMENT).toString().isEmpty() ){
			 			amsFloorDO.setFloorComments(inputJSON.get(CommonConstants.COMMENT).toString());
			 		}
			 		amsFloorDO.setUpdatedon(new Date());
			 		amsFloorDO.setCreatedon(new Date());
			 		
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			amsFloorDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			amsFloorDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		amsFloorDO.setStatus(CommonConstants.ACTIVE);
			 	}
			 	if(!amsFloorService.persist(amsFloorDO)){
					return CommonWebUtil.buildErrorResponse("Floor Already Added").toString();
				}
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Floor Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retrieveActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<AMSFloorDO> floorList = amsFloorService.retrieveActive();
				respJSON = AMSFloorUtil.getFloorList(floorList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<AMSFloorDO> floorList = amsFloorService.retrieve();
				respJSON = AMSFloorUtil.getFloorList(floorList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				AMSFloorDO floorDO = new AMSFloorDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<AMSFloorDO> floorList = amsFloorService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		floorDO = floorList.get(0);
			 		if(inputJSON.get(CommonConstants.NAME) != null &&  !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
			 			floorDO.setFloorName(inputJSON.get(CommonConstants.NAME).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.COMMENT) != null  && !inputJSON.get(CommonConstants.COMMENT).toString().isEmpty() ){
			 			floorDO.setFloorComments(inputJSON.get(CommonConstants.COMMENT).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			floorDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		floorDO.setUpdatedon(new Date());
			 		floorDO.setStatus((char) (inputJSON != null ? inputJSON.get(CommonConstants.STATUS).toString().charAt(0) : ""));
			 		if(!amsFloorService.update(floorDO)){
						return CommonWebUtil.buildErrorResponse("Frequency Already Added").toString();
					}
			 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Frequency Updated");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
}
