package com.spheresuite.erp.ams.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.AMSMakeDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class AMSMakeDAO {
	static Logger logger = Logger.getLogger(AMSMakeDAO.class.getName());
	@Autowired
	private GenericDAOImpl<AMSMakeDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(AMSMakeDO makeDO) {
		boolean persistStatus = true;
		try {
			List<AMSMakeDO> makeDOList = null;
			makeDOList = this.sessionFactory.getCurrentSession().getNamedQuery(AMSMakeDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, makeDO.getMake_name())
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
					if(makeDOList != null && makeDOList.size() > 0){
						persistStatus = false;
					}else{
						genericObject.persist(makeDO);
					}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	
	public List<AMSMakeDO> retrieve() {
		List<AMSMakeDO> makeDOList = null;
		try {
			makeDOList = genericObject.retrieve(AMSMakeDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return makeDOList;
	}
	
	public List<AMSMakeDO> retrieveActive() {
		List<AMSMakeDO> makeDOList = null;
		try {
			return genericObject.retrieveActive(CommonConstants.ACTIVE, 
					AMSMakeDO.FIND_BY_STATUS, AMSMakeDO.class);
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return makeDOList;
	}
	
	@SuppressWarnings("unchecked")
	public List<AMSMakeDO> retrieveById(Long Id) {
		List<AMSMakeDO> makeDOList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(AMSMakeDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, Id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return makeDOList;
	}

	
	@SuppressWarnings("unchecked")
	public boolean update(AMSMakeDO makeDO) {
		boolean updateStatus = true;
		try {
			List<AMSMakeDO> makeDOList = null;
			makeDOList = this.sessionFactory.getCurrentSession().getNamedQuery(AMSMakeDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, makeDO.getMake_name())
					.setParameter(CommonConstants.ID, makeDO.getMakeId())
					.setParameter(CommonConstants.STATUS, makeDO.getStatus())
					.list();
			if(makeDOList != null && makeDOList.size() > 0){
				updateStatus= false;
			}else{
				genericObject.merge(makeDO);
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return updateStatus;
	}

}
