package com.spheresuite.erp.ams.rs;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.ams.service.AMSUnitService;
import com.spheresuite.erp.ams.web.util.AMSUnitUtil;
import com.spheresuite.erp.domainobject.AMSUnitDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/ams_unit")
public class AMSUnitRS {

	String validation = null;
	static Logger logger = Logger.getLogger(AMSUnitRS.class.getName());

	@Autowired
	private AMSUnitService amsUnitService;
	
	@Autowired
	private EmployeeService employeeService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) { 
			
		try {
			if (WebManager.authenticateSession(request)) {
				AMSUnitDO amsUnitDO = new AMSUnitDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
			 			amsUnitDO.setUnitName(inputJSON.get(CommonConstants.NAME).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.EMPID) != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
				 		if(employeeList != null && employeeList.size() > 0){
				 			amsUnitDO.setUnitHead(employeeList.get(0));
				 		}
			 		}
			 		if(inputJSON.get(CommonConstants.COMMENT) != null && !inputJSON.get(CommonConstants.COMMENT).toString().isEmpty()){
			 			amsUnitDO.setUnitComments(inputJSON.get(CommonConstants.COMMENT).toString());
			 		}
			 		amsUnitDO.setUpdatedon(new Date());
			 		amsUnitDO.setCreatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			amsUnitDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			amsUnitDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		amsUnitDO.setStatus(CommonConstants.ACTIVE);
			 	}
			 	if(!amsUnitService.persist(amsUnitDO)){
					return CommonWebUtil.buildErrorResponse("Unit Already Added").toString();
				}
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Unit Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retrieveActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<AMSUnitDO> unitList = amsUnitService.retrieveActive();
				respJSON = AMSUnitUtil.getUnitList(unitList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<AMSUnitDO> unitList = amsUnitService.retrieve();
				respJSON = AMSUnitUtil.getUnitList(unitList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				AMSUnitDO unitDO = new AMSUnitDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<AMSUnitDO> unitList = amsUnitService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		unitDO = unitList.get(0);
			 		if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
			 			unitDO.setUnitName(inputJSON.get(CommonConstants.NAME).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.COMMENT) != null && !inputJSON.get(CommonConstants.COMMENT).toString().isEmpty()){
			 			unitDO.setUnitComments(inputJSON.get(CommonConstants.COMMENT).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.EMPID) != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
				 		if(employeeList != null && employeeList.size() > 0){
				 			unitDO.setUnitHead(employeeList.get(0));
				 		}
			 		}
			 		unitDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			unitDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		unitDO.setStatus((char) (inputJSON != null ? inputJSON.get(CommonConstants.STATUS).toString().charAt(0) : ""));
			 		if(!amsUnitService.update(unitDO)){
						return CommonWebUtil.buildErrorResponse("Unit Already Added").toString();
					}
			 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Unit Updated");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
}
