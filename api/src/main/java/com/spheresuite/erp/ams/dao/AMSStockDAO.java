package com.spheresuite.erp.ams.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.AMSStockDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class AMSStockDAO {
	static Logger logger = Logger.getLogger(AMSStockDAO.class.getName());
	@Autowired
	private GenericDAOImpl<AMSStockDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(AMSStockDO stockDO) {
		boolean persistStatus = true;
		try {
			List<AMSStockDO> stockDOList = null;
			stockDOList = this.sessionFactory.getCurrentSession().getNamedQuery(AMSStockDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, stockDO.getStock_name())
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
					if(stockDOList != null && stockDOList.size() > 0){
						persistStatus = false;
					}else{
						genericObject.persist(stockDO);
					}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	
	public List<AMSStockDO> retrieve() {
		List<AMSStockDO> stockDOList = null;
		try {
			stockDOList = genericObject.retrieve(AMSStockDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return stockDOList;
	}
	
	public List<AMSStockDO> retrieveActive() {
		List<AMSStockDO> stockDOList = null;
		try {
			return genericObject.retrieveActive(CommonConstants.ACTIVE, 
					AMSStockDO.FIND_BY_STATUS, AMSStockDO.class);
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return stockDOList;
	}
	
	@SuppressWarnings("unchecked")
	public List<AMSStockDO> retrieveById(Long Id) {
		List<AMSStockDO> stockDOList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(AMSStockDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, Id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return stockDOList;
	}

	
	@SuppressWarnings("unchecked")
	public boolean update(AMSStockDO stockDO) {
		boolean updateStatus = true;
		try {
			List<AMSStockDO> stockDOList = null;
			stockDOList = this.sessionFactory.getCurrentSession().getNamedQuery(AMSStockDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, stockDO.getStock_name())
					.setParameter(CommonConstants.ID, stockDO.getStockId())
					.setParameter(CommonConstants.STATUS, stockDO.getStatus())
					.list();
			if(stockDOList != null && stockDOList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(stockDO);
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return updateStatus;
	}

}
