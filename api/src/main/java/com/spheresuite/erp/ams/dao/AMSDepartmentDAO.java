package com.spheresuite.erp.ams.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.AMSDepartmentDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class AMSDepartmentDAO {
	static Logger logger = Logger.getLogger(AMSDepartmentDAO.class.getName());
	@Autowired
	private GenericDAOImpl<AMSDepartmentDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(AMSDepartmentDO deptDO) {
		boolean persistStatus = true;
		try {
			List<AMSDepartmentDO> deptList = null;
			deptList = this.sessionFactory.getCurrentSession().getNamedQuery(AMSDepartmentDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, deptDO.getDepartmentName())
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
					if(deptList != null && deptList.size() > 0){
						persistStatus = false;
					}else{
						genericObject.persist(deptDO);
					}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	
	public List<AMSDepartmentDO> retrieve() {
		List<AMSDepartmentDO> deptList = null;
		try {
			deptList = genericObject.retrieve(AMSDepartmentDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return deptList;
	}
	
	public List<AMSDepartmentDO> retrieveActive() {
		List<AMSDepartmentDO> deptList = null;
		try {
			return genericObject.retrieveActive(CommonConstants.ACTIVE, 
					AMSDepartmentDO.FIND_BY_STATUS, AMSDepartmentDO.class);
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return deptList;
	}
	
	@SuppressWarnings("unchecked")
	public List<AMSDepartmentDO> retrieveById(Long Id) {
		List<AMSDepartmentDO> deptList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(AMSDepartmentDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, Id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return deptList;
	}
	
	@SuppressWarnings("unchecked")
	public boolean update(AMSDepartmentDO deptDO) {
		boolean updateStatus = true;
		try {
			List<AMSDepartmentDO> deptDoList = null;
			deptDoList = this.sessionFactory.getCurrentSession().getNamedQuery(AMSDepartmentDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, deptDO.getDepartmentName())
					.setParameter(CommonConstants.ID, deptDO.getDepartmentId())
					.setParameter(CommonConstants.STATUS, deptDO.getStatus())
					.list();
			if(deptDoList != null && deptDoList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(deptDO);
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return updateStatus;
	}

}
