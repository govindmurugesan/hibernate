package com.spheresuite.erp.ams.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.ams.dao.AMSOfficeDAO;
import com.spheresuite.erp.domainobject.AMSOfficeDO;
@Service
@Transactional
public class AMSOfficeService {
	static Logger logger = Logger.getLogger(AMSOfficeService.class.getName());
	@Autowired
	private AMSOfficeDAO amsOfficeDAO;

	@Transactional
	public boolean persist(AMSOfficeDO officeDO) {
		return amsOfficeDAO.persist(officeDO);
	}

	@Transactional
	public List<AMSOfficeDO> retrieveActive() {
		return amsOfficeDAO.retrieveActive();
	}

	@Transactional
	public List<AMSOfficeDO> retrieveById(Long Id) {
		return amsOfficeDAO.retrieveById(Id);
	}

	@Transactional
	public List<AMSOfficeDO> retrieve() {
		return amsOfficeDAO.retrieve();
	}

	@Transactional
	public boolean update(AMSOfficeDO officeDO) {
		return amsOfficeDAO.update(officeDO);
	}

}
