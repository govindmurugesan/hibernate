package com.spheresuite.erp.ams.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.AMSInstalledProgramsDO;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;

@Component
public class AMSInstalledProgramsUtil {

	private AMSInstalledProgramsUtil() {
	}

	public static JSONObject getInstlledPrgrmList(List<AMSInstalledProgramsDO> prgrmList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (AMSInstalledProgramsDO prgrmDO : prgrmList) {
				resultJSONArray.put(getHDDObject(prgrmDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getHDDObject(AMSInstalledProgramsDO prgrmDO)
			throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(prgrmDO.getInstalledProgramId()));
		result.put(CommonConstants.NAME, String.valueOf(prgrmDO.getProgram_name()));
		result.put(CommonConstants.STATUS,
				String.valueOf(prgrmDO.getStatus()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil
				.convertDateToStringWithtime(prgrmDO.getUpdatedon())));
		if (prgrmDO.getProgram_comments() != null) {
			result.put(CommonConstants.COMMENT,
					String.valueOf(prgrmDO.getProgram_comments()));
		} else {
			result.put(CommonConstants.COMMENT, "");
		}

		return result;
	}
}
