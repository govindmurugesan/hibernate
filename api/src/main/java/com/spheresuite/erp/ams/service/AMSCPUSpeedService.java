package com.spheresuite.erp.ams.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.ams.dao.AMSCPUSpeedDAO;
import com.spheresuite.erp.domainobject.AMSCPUSpeedDO;
@Service
@Transactional
public class AMSCPUSpeedService {
	static Logger logger = Logger.getLogger(AMSCPUSpeedService.class.getName());
	@Autowired
	private AMSCPUSpeedDAO amsCpuSpeedDAO;

	@Transactional
	public boolean persist(AMSCPUSpeedDO amsCPUSpeedDO) {
		return amsCpuSpeedDAO.persist(amsCPUSpeedDO);
	}

	@Transactional
	public List<AMSCPUSpeedDO> retrieveActive() {
		return amsCpuSpeedDAO.retrieveActive();
	}

	@Transactional
	public List<AMSCPUSpeedDO> retrieveById(Long Id) {
		return amsCpuSpeedDAO.retrieveById(Id);
	}

	@Transactional
	public List<AMSCPUSpeedDO> retrieve() {
		return amsCpuSpeedDAO.retrieve();
	}

	@Transactional
	public boolean update(AMSCPUSpeedDO amsCPUSpeedDO) {
		return amsCpuSpeedDAO.update(amsCPUSpeedDO);
	}

}
