package com.spheresuite.erp.ams.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.ams.dao.AMSHddDAO;
import com.spheresuite.erp.domainobject.AMSHddDO;
@Service
@Transactional
public class AMSHddService {
	static Logger logger = Logger.getLogger(AMSHddService.class.getName());
	@Autowired
	private AMSHddDAO amsHddDAO;

	@Transactional
	public boolean persist(AMSHddDO hddDO) {
		return amsHddDAO.persist(hddDO);
	}

	@Transactional
	public List<AMSHddDO> retrieveActive() {
		return amsHddDAO.retrieveActive();
	}

	@Transactional
	public List<AMSHddDO> retrieveById(Long Id) {
		return amsHddDAO.retrieveById(Id);
	}

	@Transactional
	public List<AMSHddDO> retrieve() {
		return amsHddDAO.retrieve();
	}

	@Transactional
	public boolean update(AMSHddDO hddDO) {
		return amsHddDAO.update(hddDO);
	}

}
