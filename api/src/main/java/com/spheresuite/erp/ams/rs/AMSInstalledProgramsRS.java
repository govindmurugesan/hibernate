package com.spheresuite.erp.ams.rs;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.ams.service.AMSInstalledProgramService;
import com.spheresuite.erp.ams.web.util.AMSInstalledProgramsUtil;
import com.spheresuite.erp.domainobject.AMSInstalledProgramsDO;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/ams_installedPrograms")
public class AMSInstalledProgramsRS {

	String validation = null;
	static Logger logger = Logger.getLogger(AMSInstalledProgramsRS.class.getName());

	@Autowired
	private AMSInstalledProgramService instlledPrgrmService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) { 
			
		try {
			if (WebManager.authenticateSession(request)) {
				AMSInstalledProgramsDO instlledPrgrmDo = new AMSInstalledProgramsDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
			 			instlledPrgrmDo.setProgram_name(inputJSON.get(CommonConstants.NAME).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.COMMENT) != null && !inputJSON.get(CommonConstants.COMMENT).toString().isEmpty()){
			 			instlledPrgrmDo.setProgram_comments(inputJSON.get(CommonConstants.COMMENT).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			instlledPrgrmDo.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			instlledPrgrmDo.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		instlledPrgrmDo.setUpdatedon(new Date());
			 		instlledPrgrmDo.setStatus(CommonConstants.ACTIVE);
			 	}
			 	if(!instlledPrgrmService.persist(instlledPrgrmDo)){
					return CommonWebUtil.buildErrorResponse("Installed Program Already Added").toString();
				}
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Installed Program Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retrieveActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<AMSInstalledProgramsDO> prgrmList = instlledPrgrmService.retrieveActive();
				respJSON = AMSInstalledProgramsUtil.getInstlledPrgrmList(prgrmList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<AMSInstalledProgramsDO> prgrmList = instlledPrgrmService.retrieve();
				respJSON = AMSInstalledProgramsUtil.getInstlledPrgrmList(prgrmList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				AMSInstalledProgramsDO installPrgrmDO = new AMSInstalledProgramsDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<AMSInstalledProgramsDO> prgrmList = instlledPrgrmService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		installPrgrmDO = prgrmList.get(0);
			 		if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
			 			installPrgrmDO.setProgram_name(inputJSON.get(CommonConstants.NAME).toString());
			 		}
			 		installPrgrmDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			installPrgrmDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.COMMENT)!= null && !inputJSON.get(CommonConstants.COMMENT).toString().isEmpty()){
			 			installPrgrmDO.setProgram_comments(inputJSON.get(CommonConstants.COMMENT).toString());
			 		}
			 		installPrgrmDO.setStatus((char) (inputJSON != null ? inputJSON.get(CommonConstants.STATUS).toString().charAt(0) : ""));
			 		if(!instlledPrgrmService.update(installPrgrmDO)){
						return CommonWebUtil.buildErrorResponse("Data Already Added").toString();
					}
			 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Data Updated");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
}
