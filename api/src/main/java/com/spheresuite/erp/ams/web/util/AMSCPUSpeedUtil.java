package com.spheresuite.erp.ams.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.AMSCPUSpeedDO;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;

@Component
public class AMSCPUSpeedUtil {

	private AMSCPUSpeedUtil() {
	}


	public static JSONObject getCPUSpeedList(List<AMSCPUSpeedDO> cpuTypeList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (AMSCPUSpeedDO cpuTypeDO : cpuTypeList) {
				resultJSONArray.put(getCPUTypeObject(cpuTypeDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getCPUTypeObject(AMSCPUSpeedDO cpuSpeedDO)
			throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(cpuSpeedDO.getCpuSpeedId()));
		result.put(CommonConstants.NAME, String.valueOf(cpuSpeedDO.getCpuspeed_name()));
		result.put(CommonConstants.STATUS,
				String.valueOf(cpuSpeedDO.getStatus()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil
				.convertDateToStringWithtime(cpuSpeedDO.getUpdatedon())));
		

		if (cpuSpeedDO.getCpuspeed_comments() != null) {
			result.put(CommonConstants.COMMENT,
					String.valueOf(cpuSpeedDO.getCpuspeed_comments()));
		} else {
			result.put(CommonConstants.COMMENT, "");
		}
		return result;
	}
}
