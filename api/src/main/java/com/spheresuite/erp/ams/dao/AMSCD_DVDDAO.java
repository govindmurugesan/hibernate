package com.spheresuite.erp.ams.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.AMSCD_DVDDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class AMSCD_DVDDAO {
	static Logger logger = Logger.getLogger(AMSCD_DVDDAO.class.getName());
	@Autowired
	private GenericDAOImpl<AMSCD_DVDDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(AMSCD_DVDDO amsCDTypeDO) {
		boolean persistStatus = true;
		try {
			List<AMSCD_DVDDO> amsCDTypeDOList = null;
			amsCDTypeDOList = this.sessionFactory.getCurrentSession().getNamedQuery(AMSCD_DVDDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, amsCDTypeDO.getCd_dvdname())
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
					if(amsCDTypeDOList != null && amsCDTypeDOList.size() > 0){
						persistStatus = false;
					}else{
						genericObject.persist(amsCDTypeDO);
					}
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
		}
		return persistStatus;
	}
	
	
	public List<AMSCD_DVDDO> retrieve() {
		List<AMSCD_DVDDO> amsCDTypeDOList = null;
		try {
			amsCDTypeDOList = genericObject.retrieve(AMSCD_DVDDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return amsCDTypeDOList;
	}
	
	public List<AMSCD_DVDDO> retrieveActive() {
		List<AMSCD_DVDDO> amsCDTypeDOList = null;
		try {
			return genericObject.retrieveActive(CommonConstants.ACTIVE, 
					AMSCD_DVDDO.FIND_BY_STATUS, AMSCD_DVDDO.class);
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return amsCDTypeDOList;
	}
	
	@SuppressWarnings("unchecked")
	public List<AMSCD_DVDDO> retrieveById(Long Id) {
		List<AMSCD_DVDDO> amsCDTypeDOList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(AMSCD_DVDDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, Id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return amsCDTypeDOList;
	}

	
	@SuppressWarnings("unchecked")
	public boolean update(AMSCD_DVDDO makeDO) {
		boolean updateStatus = true;
		try {
			List<AMSCD_DVDDO> amsCDTypeDOList = null;
			amsCDTypeDOList = this.sessionFactory.getCurrentSession().getNamedQuery(AMSCD_DVDDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, makeDO.getCd_dvdname())
					.setParameter(CommonConstants.ID, makeDO.getCd_dvdId())
					.setParameter(CommonConstants.STATUS, makeDO.getStatus())
					.list();
			if(amsCDTypeDOList != null && amsCDTypeDOList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(makeDO);
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return updateStatus;
	}

}
