package com.spheresuite.erp.ams.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.AMSAssetDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class AMSAssetDAO {
	static Logger logger = Logger.getLogger(AMSAssetDAO.class.getName());
	@Autowired
	private GenericDAOImpl<AMSAssetDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public boolean persist(AMSAssetDO assetDO) {
		boolean persistStatus = true;
		try {
			genericObject.persist(assetDO);
			persistStatus = true;
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	
	public List<AMSAssetDO> retrieve() {
		List<AMSAssetDO> assetDOList = null;
		try {
			assetDOList = genericObject.retrieve(AMSAssetDO.class);
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
		}
		return assetDOList;
	}
	
	/*public List<AMSAssetDO> retrieveActive() {
		List<AMSAssetDO> assetDOList = null;
		try {
			return genericObject.retrieveActive(CommonConstants.ACTIVE, 
					AMSAssetDO.FIND_BY_STATUS, AMSAssetDO.class);
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return assetDOList;
	}*/
	
	@SuppressWarnings("unchecked")
	public List<AMSAssetDO> retrieveById(Long Id) {
		List<AMSAssetDO> assetDOList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(AMSAssetDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, Id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return assetDOList;
	}
	
	@SuppressWarnings("unchecked")
	public List<AMSAssetDO> retrieveByUnit(Long Id) {
		List<AMSAssetDO> assetDOList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(AMSAssetDO.FIND_BY_UNIT)
					.setParameter(CommonConstants.ID, Id)
					.setParameter(CommonConstants.STATUS, CommonConstants.INACTIVE)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return assetDOList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<AMSAssetDO> retrieveForId() {
		List<AMSAssetDO> assetDOList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(AMSAssetDO.FIND_ALL)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return assetDOList;
	}
	
	public boolean update(AMSAssetDO assetDO) {
		try {
			genericObject.merge(assetDO);
			return true;
		} catch (Exception eException) {
			eException.printStackTrace();
			logger.info(eException.getMessage());
			return true;
		} finally {
		}
	}

}
