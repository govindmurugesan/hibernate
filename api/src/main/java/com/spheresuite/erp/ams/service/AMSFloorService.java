package com.spheresuite.erp.ams.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.ams.dao.AMSFloorDAO;
import com.spheresuite.erp.domainobject.AMSFloorDO;
@Service
@Transactional
public class AMSFloorService {
	static Logger logger = Logger.getLogger(AMSFloorService.class.getName());
	@Autowired
	private AMSFloorDAO amsFloorDAO;

	@Transactional
	public boolean persist(AMSFloorDO floorDO) {
		return amsFloorDAO.persist(floorDO);
	}

	@Transactional
	public List<AMSFloorDO> retrieveActive() {
		return amsFloorDAO.retrieveActive();
	}

	@Transactional
	public List<AMSFloorDO> retrieveById(Long Id) {
		return amsFloorDAO.retrieveById(Id);
	}

	@Transactional
	public List<AMSFloorDO> retrieve() {
		return amsFloorDAO.retrieve();
	}

	@Transactional
	public boolean update(AMSFloorDO floorDO) {
		return amsFloorDAO.update(floorDO);
	}
}
