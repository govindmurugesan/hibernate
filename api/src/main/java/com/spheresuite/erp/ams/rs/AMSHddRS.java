package com.spheresuite.erp.ams.rs;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.ams.service.AMSHddService;
import com.spheresuite.erp.ams.web.util.AMSHddUtil;
import com.spheresuite.erp.domainobject.AMSHddDO;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/ams_hdd")
public class AMSHddRS {

	String validation = null;
	static Logger logger = Logger.getLogger(AMSHddRS.class.getName());

	@Autowired
	private AMSHddService amsHddService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) { 
			
		try {
			if (WebManager.authenticateSession(request)) {
				AMSHddDO amsHddDO = new AMSHddDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
			 			amsHddDO.setHdd(inputJSON.get(CommonConstants.NAME).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.COMMENT) != null && !inputJSON.get(CommonConstants.COMMENT).toString().isEmpty()){
			 			amsHddDO.setHdd_comments(inputJSON.get(CommonConstants.COMMENT).toString());
			 		}
			 		amsHddDO.setUpdatedon(new Date());
			 		amsHddDO.setCreatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			amsHddDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			amsHddDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		amsHddDO.setStatus(CommonConstants.ACTIVE);
			 	}
			 	if(!amsHddService.persist(amsHddDO)){
					return CommonWebUtil.buildErrorResponse("HDD Already Added").toString();
				}
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New HDD Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retrieveActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<AMSHddDO> hddList = amsHddService.retrieveActive();
				respJSON = AMSHddUtil.getHddList(hddList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<AMSHddDO> hddList = amsHddService.retrieve();
				respJSON = AMSHddUtil.getHddList(hddList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				AMSHddDO hddDO = new AMSHddDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<AMSHddDO> hddList = amsHddService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		hddDO = hddList.get(0);
			 		if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
			 			hddDO.setHdd(inputJSON.get(CommonConstants.NAME).toString());
			 		}
			 		hddDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			hddDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.COMMENT) != null && !inputJSON.get(CommonConstants.COMMENT).toString().isEmpty()){
			 			hddDO.setHdd_comments(inputJSON.get(CommonConstants.COMMENT).toString());
			 		}
			 		hddDO.setStatus((char) (inputJSON != null ? inputJSON.get(CommonConstants.STATUS).toString().charAt(0) : ""));
			 		if(!amsHddService.update(hddDO)){
						return CommonWebUtil.buildErrorResponse("Hdd Already Added").toString();
					}
			 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Hdd Updated");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
}
