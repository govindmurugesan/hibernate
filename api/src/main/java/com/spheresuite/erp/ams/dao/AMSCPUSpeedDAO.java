package com.spheresuite.erp.ams.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.AMSCPUSpeedDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class AMSCPUSpeedDAO {
	static Logger logger = Logger.getLogger(AMSCPUSpeedDAO.class.getName());
	@Autowired
	private GenericDAOImpl<AMSCPUSpeedDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(AMSCPUSpeedDO amsCPUSpeedDO) {
		boolean persistStatus = true;
		try {
			List<AMSCPUSpeedDO> amsCPUSpeedDOList = null;
			amsCPUSpeedDOList = this.sessionFactory.getCurrentSession().getNamedQuery(AMSCPUSpeedDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, amsCPUSpeedDO.getCpuspeed_name())
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
					if(amsCPUSpeedDOList != null && amsCPUSpeedDOList.size() > 0){
						persistStatus = false;
					}else{
						genericObject.persist(amsCPUSpeedDO);
					}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	
	public List<AMSCPUSpeedDO> retrieve() {
		List<AMSCPUSpeedDO> amsCPUSpeedDOList = null;
		try {
			amsCPUSpeedDOList = genericObject.retrieve(AMSCPUSpeedDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return amsCPUSpeedDOList;
	}
	
	public List<AMSCPUSpeedDO> retrieveActive() {
		List<AMSCPUSpeedDO> amsCPUSpeedDOList = null;
		try {
			return genericObject.retrieveActive(CommonConstants.ACTIVE, 
					AMSCPUSpeedDO.FIND_BY_STATUS, AMSCPUSpeedDO.class);
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return amsCPUSpeedDOList;
	}
	
	@SuppressWarnings("unchecked")
	public List<AMSCPUSpeedDO> retrieveById(Long Id) {
		List<AMSCPUSpeedDO> amsCPUSpeedDOList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(AMSCPUSpeedDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, Id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return amsCPUSpeedDOList;
	}

	@SuppressWarnings("unchecked")
	public boolean update(AMSCPUSpeedDO amsCpuDO) {
		boolean updateStatus = true;
		try {
			List<AMSCPUSpeedDO> amsCPUSpeedDOList = null;
			amsCPUSpeedDOList = this.sessionFactory.getCurrentSession().getNamedQuery(AMSCPUSpeedDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, amsCpuDO.getCpuspeed_name())
					.setParameter(CommonConstants.ID, amsCpuDO.getCpuSpeedId())
					.setParameter(CommonConstants.STATUS, amsCpuDO.getStatus())
					.list();
			if(amsCPUSpeedDOList != null && amsCPUSpeedDOList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(amsCpuDO);
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return updateStatus;
	}

}
