package com.spheresuite.erp.ams.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.AMSAssetAssignmentDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class AMSAssetAssignmentDAO {
	static Logger logger = Logger.getLogger(AMSAssetAssignmentDAO.class.getName());
	@Autowired
	private GenericDAOImpl<AMSAssetAssignmentDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(AMSAssetAssignmentDO assetDetail) {
		boolean persistStatus = true;
		try {
			List<AMSAssetAssignmentDO> assignmentList = null;
			assignmentList = this.sessionFactory.getCurrentSession().getNamedQuery(AMSAssetAssignmentDO.FIND_FOR_ADD)
					.setParameter(CommonConstants.ID, assetDetail.getAsset().getAssetId())
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
					if(assignmentList != null && assignmentList.size() > 0){
						persistStatus = false;
					}else{
						genericObject.persist(assetDetail);
					}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	
	/*public boolean persist(AMSAssetAssignmentDO assetDO) {
		boolean persistStatus = true;
		try {
			genericObject.persist(assetDO);
			persistStatus = true;
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}*/
	
	
	public List<AMSAssetAssignmentDO> retrieve() {
		List<AMSAssetAssignmentDO> assignmentList = null;
		try {
			assignmentList = genericObject.retrieve(AMSAssetAssignmentDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return assignmentList;
	}
	
	/*public List<AMSAssignedAssetDO> retrieveActive() {
		List<AMSAssignedAssetDO> assetDOList = null;
		try {
			return genericObject.retrieveActive(CommonConstants.ACTIVE, 
					AMSAssignedAssetDO.FIND_BY_STATUS, AMSAssignedAssetDO.class);
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return assetDOList;
	}*/
	
	@SuppressWarnings("unchecked")
	public List<AMSAssetAssignmentDO> retrieveById(Long Id) {
		List<AMSAssetAssignmentDO> assignmentList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(AMSAssetAssignmentDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, Id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return assignmentList;
	}
	
	@SuppressWarnings("unchecked")
	public List<AMSAssetAssignmentDO> retrieveByEmpId(String Id) {
		List<AMSAssetAssignmentDO> assignmentList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(AMSAssetAssignmentDO.FIND_BY_EMPID)
					.setParameter(CommonConstants.ID, Id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return assignmentList;
	}
	
	@SuppressWarnings("unchecked")
	public List<AMSAssetAssignmentDO> retrieveByUnit(Long Id) {
		List<AMSAssetAssignmentDO> assetDOList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(AMSAssetAssignmentDO.FIND_BY_UNIT)
					.setParameter(CommonConstants.ID, Id)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return assetDOList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<AMSAssetAssignmentDO> retrieveByIdAndStatus(Long id) {
		List<AMSAssetAssignmentDO> assetDOList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(AMSAssetAssignmentDO.FIND_FOR_ADD)
					.setParameter(CommonConstants.ID,  id)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return assetDOList;
	}
	
	public boolean update(AMSAssetAssignmentDO assetAssignmentDO) {
		try {
			genericObject.merge(assetAssignmentDO);
			return true;
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			return true;
		} finally {
		}
	}

}
