package com.spheresuite.erp.ams.rs;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.ams.service.AMSCD_DVDService;
import com.spheresuite.erp.ams.web.util.AMSCD_DVDUtil;
import com.spheresuite.erp.domainobject.AMSCD_DVDDO;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/ams_cd_dvd")
public class AMSCD_DVDRS {

	String validation = null;
	static Logger logger = Logger.getLogger(AMSCD_DVDRS.class.getName());

	@Autowired
	private AMSCD_DVDService amsCDTypeService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) { 
			
		try {
			if (WebManager.authenticateSession(request)) {
				AMSCD_DVDDO AMSCDTypeDO = new AMSCD_DVDDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
			 			AMSCDTypeDO.setCd_dvdname(inputJSON.get(CommonConstants.NAME).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.COMMENT) != null && !inputJSON.get(CommonConstants.COMMENT).toString().isEmpty()){
			 			AMSCDTypeDO.setComments(inputJSON.get(CommonConstants.COMMENT).toString());
			 		}
			 		AMSCDTypeDO.setUpdatedon(new Date());
			 		AMSCDTypeDO.setCreatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			AMSCDTypeDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			AMSCDTypeDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		AMSCDTypeDO.setStatus(CommonConstants.ACTIVE);
			 	}
			 	if(!amsCDTypeService.persist(AMSCDTypeDO)){
					return CommonWebUtil.buildErrorResponse("Data Already Added").toString();
				}
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Data Type Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retrieveActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<AMSCD_DVDDO> typeList = amsCDTypeService.retrieveActive();
				respJSON = AMSCD_DVDUtil.getCDTypeList(typeList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<AMSCD_DVDDO> typeList = amsCDTypeService.retrieve();
				respJSON = AMSCD_DVDUtil.getCDTypeList(typeList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				AMSCD_DVDDO cdTypeDo = new AMSCD_DVDDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<AMSCD_DVDDO> CDTypeList = amsCDTypeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		cdTypeDo = CDTypeList.get(0);
			 		if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
			 			cdTypeDo.setCd_dvdname(inputJSON.get(CommonConstants.NAME).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.COMMENT) != null && !inputJSON.get(CommonConstants.COMMENT).toString().isEmpty()){
			 			cdTypeDo.setComments(inputJSON.get(CommonConstants.COMMENT).toString());
			 		}
			 		cdTypeDo.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			cdTypeDo.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		cdTypeDo.setStatus((char) (inputJSON != null ? inputJSON.get(CommonConstants.STATUS).toString().charAt(0) : ""));
			 		if(!amsCDTypeService.update(cdTypeDo)){
						return CommonWebUtil.buildErrorResponse("Data Already Added").toString();
					}
			 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Data Updated");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
}
