package com.spheresuite.erp.ams.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.AMSHddDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class AMSHddDAO {
	static Logger logger = Logger.getLogger(AMSHddDAO.class.getName());
	@Autowired
	private GenericDAOImpl<AMSHddDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(AMSHddDO hddDO) {
		boolean persistStatus = true;
		try {
			List<AMSHddDO> ramDOList = null;
			ramDOList = this.sessionFactory.getCurrentSession().getNamedQuery(AMSHddDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, hddDO.getHdd())
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
					if(ramDOList != null && ramDOList.size() > 0){
						persistStatus = false;
					}else{
						genericObject.persist(hddDO);
					}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	
	public List<AMSHddDO> retrieve() {
		List<AMSHddDO> hddList = null;
		try {
			hddList = genericObject.retrieve(AMSHddDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return hddList;
	}
	
	public List<AMSHddDO> retrieveActive() {
		List<AMSHddDO> hddList = null;
		try {
			return genericObject.retrieveActive(CommonConstants.ACTIVE, 
					AMSHddDO.FIND_BY_STATUS, AMSHddDO.class);
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return hddList;
	}
	
	@SuppressWarnings("unchecked")
	public List<AMSHddDO> retrieveById(Long Id) {
		List<AMSHddDO> hddList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(AMSHddDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, Id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return hddList;
	}

	
	@SuppressWarnings("unchecked")
	public boolean update(AMSHddDO hddDO) {
		boolean updateStatus = true;
		try {
			List<AMSHddDO> hddList = null;
			hddList = this.sessionFactory.getCurrentSession().getNamedQuery(AMSHddDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, hddDO.getHdd())
					.setParameter(CommonConstants.ID, hddDO.getHddId())
					.setParameter(CommonConstants.STATUS, hddDO.getStatus())
					.list();
			if(hddList != null && hddList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(hddDO);
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return updateStatus;
	}

}
