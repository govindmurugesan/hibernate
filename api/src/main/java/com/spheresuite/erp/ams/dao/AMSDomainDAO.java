package com.spheresuite.erp.ams.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.AMSDomainDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class AMSDomainDAO {
	static Logger logger = Logger.getLogger(AMSDomainDAO.class.getName());
	@Autowired
	private GenericDAOImpl<AMSDomainDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(AMSDomainDO domainDO) {
		boolean persitStatus = true;
		try {
			List<AMSDomainDO> unitDOList = null;
			unitDOList = this.sessionFactory.getCurrentSession().getNamedQuery(AMSDomainDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, domainDO.getDoaminName())
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
					if(unitDOList != null && unitDOList.size() > 0){
						persitStatus = false;
					}else{
						genericObject.persist(domainDO);
					}
		} catch (Exception eException) {
		} finally {
		}
		return persitStatus;
	}
	
	
	public List<AMSDomainDO> retrieve() {
		List<AMSDomainDO> domainDOList = null;
		try {
			domainDOList = genericObject.retrieve(AMSDomainDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return domainDOList;
	}
	
	public List<AMSDomainDO> retrieveActive() {
		List<AMSDomainDO> domainDOList = null;
		try {
			return genericObject.retrieveActive(CommonConstants.ACTIVE, 
					AMSDomainDO.FIND_BY_STATUS, AMSDomainDO.class);
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return domainDOList;
	}
	
	@SuppressWarnings("unchecked")
	public List<AMSDomainDO> retrieveById(Long Id) {
		List<AMSDomainDO> domainDOList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(AMSDomainDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, Id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return domainDOList;
	}

	
	@SuppressWarnings("unchecked")
	public boolean update(AMSDomainDO domainDO) {
		boolean updateStatus = true;
		try {
			List<AMSDomainDO> domainDOList = null;
			domainDOList = this.sessionFactory.getCurrentSession().getNamedQuery(AMSDomainDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, domainDO.getDoaminName())
					.setParameter(CommonConstants.ID, domainDO.getDomainId())
					.setParameter(CommonConstants.STATUS, domainDO.getStatus())
					.list();
			if(domainDOList != null && domainDOList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(domainDO);
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return updateStatus;
	}

}
