package com.spheresuite.erp.ams.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.AMSMailDO;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;

@Component
public class AMSMailUtil {

	private AMSMailUtil() {
	}

	public static JSONObject getMailList(List<AMSMailDO> mailList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (AMSMailDO mailDO : mailList) {
				resultJSONArray.put(getMakeObject(mailDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getMakeObject(AMSMailDO mailDO)
			throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(mailDO.getMailId()));
		result.put(CommonConstants.NAME, String.valueOf(mailDO.getMail_name()));
		result.put(CommonConstants.STATUS,
				String.valueOf(mailDO.getStatus()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil
				.convertDateToStringWithtime(mailDO.getUpdatedon())));

		if (mailDO.getMail_comments() != null) {
			result.put(CommonConstants.COMMENT,
					String.valueOf(mailDO.getMail_comments()));
		} else {
			result.put(CommonConstants.COMMENT, "");
		}
		return result;
	}
}
