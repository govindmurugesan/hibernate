package com.spheresuite.erp.ams.web.util;

import java.util.List;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.ams.service.AMSAssetAssignmentService;
import com.spheresuite.erp.domainobject.AMSAssetAssignmentDO;
import com.spheresuite.erp.domainobject.AMSAssetDO;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
@Component
public class AMSAssetUtil {
	
	private AMSAssetUtil() {}
	
	@Autowired
	private AMSAssetAssignmentService tassetAssignmentService;
	private static AMSAssetAssignmentService assetAssignmentService;
	
	@PostConstruct
	public void init() {
		assetAssignmentService = tassetAssignmentService;
	}
	
	public static JSONObject getAssetList(List<AMSAssetDO> assetList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (AMSAssetDO assetDO : assetList) {
				resultJSONArray.put(getAssetDetailObject(assetDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getAssetDetailObject(AMSAssetDO assetDO)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(assetDO.getAssetId()));
		
		List<AMSAssetAssignmentDO> assignList = assetAssignmentService.retrieveByIdAndStatus(assetDO.getAssetId());
		if(assignList != null && assignList.size() > 0){
			if(assignList.get(0).getEmployee() != null){
				result.put(CommonConstants.EMPID, String.valueOf(assignList.get(0).getEmployee().getEmpId())); 
				if(assignList.get(0).getEmployee().getMiddlename() != null)result.put(CommonConstants.EMPNAME, String.valueOf(assignList.get(0).getEmployee().getFirstname() + " " +assignList.get(0).getEmployee().getMiddlename() + " " + assignList.get(0).getEmployee().getLastname()));
				else if(assignList.get(0).getEmployee().getMiddlename() != null && assignList.get(0).getEmployee().getLastname() == null )result.put(CommonConstants.EMPNAME, String.valueOf(assignList.get(0).getEmployee().getFirstname() + " " +assignList.get(0).getEmployee().getMiddlename()));
				else if(assignList.get(0).getEmployee().getMiddlename() == null && assignList.get(0).getEmployee().getLastname() == null )result.put(CommonConstants.EMPNAME, String.valueOf(assignList.get(0).getEmployee().getFirstname()));
				else result.put(CommonConstants.EMPNAME, String.valueOf(assignList.get(0).getEmployee().getFirstname() + " " + assignList.get(0).getEmployee().getLastname()));
			}else{
				result.put(CommonConstants.EMPID, ""); 
				result.put(CommonConstants.EMPNAME, ""); 
			}
		}
		
		result.put(CommonConstants.NAME, String.valueOf(assetDO.getAsset_name()));
		result.put(CommonConstants.STATUS, String.valueOf(assetDO.getStatus()));
		if(assetDO.getDepartment() != null){
				result.put(CommonConstants.DEPT, String.valueOf(assetDO.getDepartment().getDepartmentId())); 
				result.put(CommonConstants.DEPT_NAME, String.valueOf(assetDO.getDepartment().getDepartmentName())); 
		}else{
			result.put(CommonConstants.DEPT, ""); 
			result.put(CommonConstants.DEPT_NAME, ""); 
		}
		if(assetDO.getUnit() != null){
				result.put(CommonConstants.UNIT, String.valueOf(assetDO.getUnit().getUnitId())); 
				result.put(CommonConstants.UNITNAME, String.valueOf(assetDO.getUnit().getUnitName())); 
		}else{
			result.put(CommonConstants.UNIT, ""); 
			result.put(CommonConstants.UNITNAME, ""); 
		}
		
		
		if(assetDO.getAsset_number()!= null){
			result.put(CommonConstants.ASSETNUMBER, String.valueOf(assetDO.getAsset_number()));
		}else{
			result.put(CommonConstants.ASSETNUMBER, "");
		}
		if(assetDO.getFloor() != null){
				result.put(CommonConstants.FLOOR, String.valueOf(assetDO.getFloor().getFloorId())); 
				result.put(CommonConstants.FLOOR_NAME, String.valueOf(assetDO.getFloor().getFloorName())); 
		}else{
			result.put(CommonConstants.FLOOR, ""); 
			result.put(CommonConstants.FLOOR_NAME, ""); 
		}
		if(assetDO.getDomain() != null){
			result.put(CommonConstants.DOMAIN, String.valueOf(assetDO.getDomain().getDomainId())); 
			result.put(CommonConstants.DOMAIN_NAME, String.valueOf(assetDO.getDomain().getDoaminName())); 
		}else{
			result.put(CommonConstants.DOMAIN, ""); 
			result.put(CommonConstants.DOMAIN_NAME, ""); 
		}
		if(assetDO.getPcUnder() != null){
			result.put(CommonConstants.PCUNDERID, String.valueOf(assetDO.getPcUnder().getPcUnderId()));
			result.put(CommonConstants.PCUNDER, String.valueOf(assetDO.getPcUnder().getPcunder()));
		}else{
			result.put(CommonConstants.PCUNDERID, "");
			result.put(CommonConstants.PCUNDER, "");
		}
		if(assetDO.getPurchaseDate() != null){
			result.put(CommonConstants.PURCHASEDATE, String.valueOf(CommonUtil.convertDateToYearWithOutTime(assetDO.getPurchaseDate())));
		}else{
			result.put(CommonConstants.PURCHASEDATE, "");
		}
		if(assetDO.getExpiry() != null){
			result.put(CommonConstants.EXPIRYDATE, String.valueOf(CommonUtil.convertDateToYearWithOutTime(assetDO.getExpiry())));
		}else{
			result.put(CommonConstants.EXPIRYDATE, "");
		}
		
		
		if(assetDO.getType() != null){
				result.put(CommonConstants.TYPENAME, String.valueOf((assetDO.getType().getTypeName())));
				result.put(CommonConstants.TYPE, String.valueOf(assetDO.getType().getTypeId()));
		}else{
			result.put(CommonConstants.TYPE, ""); 
			result.put(CommonConstants.TYPENAME, ""); 
		}
		if(assetDO.getMake() != null){
			result.put(CommonConstants.MAKE_NAME, String.valueOf(assetDO.getMake().getMake_name()));
			result.put(CommonConstants.MAKE, String.valueOf(assetDO.getMake().getMakeId()));
		}else{
			result.put(CommonConstants.MAKE, ""); 
			result.put(CommonConstants.MAKE_NAME, ""); 
		}
		
		if(assetDO.getMonitor() != null){
			result.put(CommonConstants.MONITOR, String.valueOf(assetDO.getMonitor()));
		}else{
			result.put(CommonConstants.MONITOR, "");
		}
		
		if(assetDO.getCpuType() != null){
				result.put(CommonConstants.CPUTYPE_NAME, String.valueOf(assetDO.getCpuType().getCputype_name()));
				result.put(CommonConstants.CPUTYPE, String.valueOf(assetDO.getCpuType().getCpuTypeId()));
		}else{
			result.put(CommonConstants.CPUTYPE, ""); 
			result.put(CommonConstants.CPUTYPE_NAME, ""); 
		}
		
		if(assetDO.getCpuSpeed() != null){
			result.put(CommonConstants.CPUSPEED_NAME, String.valueOf(assetDO.getCpuSpeed().getCpuspeed_name()));
			result.put(CommonConstants.CPUSPEED, String.valueOf(assetDO.getCpuSpeed().getCpuSpeedId()));
		}else{
			result.put(CommonConstants.CPUSPEED, ""); 
			result.put(CommonConstants.CPUSPEED_NAME, ""); 
		}
		
		if(assetDO.getFdd() != null){
			result.put(CommonConstants.FDD, String.valueOf(assetDO.getFdd()));
		}else{
			result.put(CommonConstants.FDD, "");
		}
		if(assetDO.getRam() != null){
			result.put(CommonConstants.RAM_NAME, String.valueOf(assetDO.getRam().getRam()));
			result.put(CommonConstants.RAM, String.valueOf(assetDO.getRam().getRamId()));
		}else{
			result.put(CommonConstants.RAM, ""); 
			result.put(CommonConstants.RAM_NAME, ""); 
		}
		
		if(assetDO.getHdd() != null){
			result.put(CommonConstants.HDD_NAME, String.valueOf(assetDO.getHdd().getHdd()));
			result.put(CommonConstants.HDD, String.valueOf(assetDO.getHdd().getHddId()));
		}else{
			result.put(CommonConstants.HDD, ""); 
			result.put(CommonConstants.HDD_NAME, ""); 
		}
		
		if(assetDO.getCdd_dvd() != null){
			result.put(CommonConstants.CDD, String.valueOf(assetDO.getCdd_dvd().getCd_dvdname()));
			result.put(CommonConstants.CDD_DVDID, String.valueOf(assetDO.getCdd_dvd().getCd_dvdId()));
		}else{
			result.put(CommonConstants.CDD, "");
			result.put(CommonConstants.CDD_DVDID, "");
		}
		if(assetDO.getInternet() != null){
			result.put(CommonConstants.INTERNET, String.valueOf(assetDO.getInternet()));
		}else{
			result.put(CommonConstants.INTERNET, "");
		}
		if(assetDO.getUsb() != null){
			result.put(CommonConstants.USB, String.valueOf(assetDO.getUsb()));
		}else{
			result.put(CommonConstants.USB, "");
		}
		
		if(assetDO.getIpAddress() != null){
			result.put(CommonConstants.IPADDRESS, String.valueOf(assetDO.getIpAddress()));
		}else{
			result.put(CommonConstants.IPADDRESS, "");
		}
		
		if(assetDO.getOffice() != null){
			result.put(CommonConstants.MSOFFICE_NAME, String.valueOf(assetDO.getOffice().getOffice_name()));
			result.put(CommonConstants.MSOFFICE, String.valueOf(assetDO.getOffice().getOfficeId()));
		}else{
			result.put(CommonConstants.MSOFFICE, ""); 
			result.put(CommonConstants.MSOFFICE_NAME, ""); 
		}
		
		if(assetDO.getMail() != null){
			result.put(CommonConstants.MAILID_NAME, String.valueOf(assetDO.getMail().getMail_name()));
			result.put(CommonConstants.MAILID, String.valueOf(assetDO.getMail().getMailId()));
		}else{
			result.put(CommonConstants.MAILID_NAME, ""); 
			result.put(CommonConstants.MAILID, ""); 
		}
		
		if(assetDO.getOSModel() != null){
			result.put(CommonConstants.OS_NAME, String.valueOf(assetDO.getOSModel().getOs()));
			result.put(CommonConstants.OSMODEL, String.valueOf(assetDO.getOSModel().getOsId()));
		}else{
			result.put(CommonConstants.OSMODEL, ""); 
			result.put(CommonConstants.OS_NAME, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(assetDO.getUpdatedon())));
		if(assetDO.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(assetDO.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		return result;
	}
	
	public static JSONObject getUnitAssetList(List<AMSAssetDO> assetList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (AMSAssetDO assetDO : assetList) {
				resultJSONArray.put(getUnitAssetDetailObject(assetDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getUnitAssetDetailObject(AMSAssetDO assetDO)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(assetDO.getAssetId()));
		result.put(CommonConstants.NAME, String.valueOf(assetDO.getAsset_name()));
		result.put(CommonConstants.STATUS, String.valueOf(assetDO.getStatus()));
		if(assetDO.getAsset_number()!= null){
			result.put(CommonConstants.ASSETNUMBER, String.valueOf(assetDO.getAsset_number()));
		}else{
			result.put(CommonConstants.ASSETNUMBER, "");
		}
		if(assetDO.getUnit() != null){
			result.put(CommonConstants.UNIT, String.valueOf(assetDO.getUnit().getUnitId())); 
			result.put(CommonConstants.UNITNAME, String.valueOf(assetDO.getUnit().getUnitName())); 
		}else{
			result.put(CommonConstants.UNIT, ""); 
			result.put(CommonConstants.UNITNAME, ""); 
		}
		
		
		
		/*if(assetDO.getDepartment() != null){
				result.put(CommonConstants.DEPT, String.valueOf(assetDO.getDepartment().getDepartmentId())); 
				result.put(CommonConstants.DEPT_NAME, String.valueOf(assetDO.getDepartment().getDepartmentName())); 
		}else{
			result.put(CommonConstants.DEPT, ""); 
			result.put(CommonConstants.DEPT_NAME, ""); 
		}
		if(assetDO.getUnit() != null){
				result.put(CommonConstants.UNIT, String.valueOf(assetDO.getUnit().getUnitId())); 
				result.put(CommonConstants.UNITNAME, String.valueOf(assetDO.getUnit().getUnitName())); 
		}else{
			result.put(CommonConstants.UNIT, ""); 
			result.put(CommonConstants.UNITNAME, ""); 
		}
		*/
		
		/*if(assetDO.getAsset_number()!= null){
			result.put(CommonConstants.ASSETNUMBER, String.valueOf(assetDO.getAsset_number()));
		}else{
			result.put(CommonConstants.ASSETNUMBER, "");
		}
		if(assetDO.getFloor() != null){
				result.put(CommonConstants.FLOOR, String.valueOf(assetDO.getFloor().getFloorId())); 
				result.put(CommonConstants.FLOOR_NAME, String.valueOf(assetDO.getFloor().getFloorName())); 
		}else{
			result.put(CommonConstants.FLOOR, ""); 
			result.put(CommonConstants.FLOOR_NAME, ""); 
		}
		if(assetDO.getDomain() != null){
			result.put(CommonConstants.DOMAIN, String.valueOf(assetDO.getDomain().getDomainId())); 
			result.put(CommonConstants.DOMAIN_NAME, String.valueOf(assetDO.getDomain().getDoaminName())); 
		}else{
			result.put(CommonConstants.DOMAIN, ""); 
			result.put(CommonConstants.DOMAIN_NAME, ""); 
		}
		if(assetDO.getPcUnder() != null){
			result.put(CommonConstants.PCUNDERID, String.valueOf(assetDO.getPcUnder().getPcUnderId()));
			result.put(CommonConstants.PCUNDER, String.valueOf(assetDO.getPcUnder().getPcunder()));
		}else{
			result.put(CommonConstants.PCUNDERID, "");
			result.put(CommonConstants.PCUNDER, "");
		}
		result.put(CommonConstants.PURCHASEDATE, String.valueOf(assetDO.getPurchaseDate() != null ? CommonUtil.convertDateToYearWithOutTime(assetDO.getPurchaseDate()) : ""));
		result.put(CommonConstants.EXPIRYDATE, String.valueOf(assetDO.getExpiry()!= null ? CommonUtil.convertDateToYearWithOutTime(assetDO.getExpiry()) : ""));
		if(assetDO.getType() != null){
				result.put(CommonConstants.TYPENAME, String.valueOf((assetDO.getType().getTypeName())));
				result.put(CommonConstants.TYPE, String.valueOf(assetDO.getType().getTypeId()));
		}else{
			result.put(CommonConstants.TYPE, ""); 
			result.put(CommonConstants.TYPENAME, ""); 
		}
		if(assetDO.getMake() != null){
			result.put(CommonConstants.MAKE_NAME, String.valueOf(assetDO.getMake().getMake_name()));
			result.put(CommonConstants.MAKE, String.valueOf(assetDO.getMake().getMakeId()));
		}else{
			result.put(CommonConstants.MAKE, ""); 
			result.put(CommonConstants.MAKE_NAME, ""); 
		}
		
		if(assetDO.getMonitor() != null){
			result.put(CommonConstants.MONITOR, String.valueOf(assetDO.getMonitor()));
		}else{
			result.put(CommonConstants.MONITOR, "");
		}
		
		if(assetDO.getCpuType() != null){
				result.put(CommonConstants.CPUTYPE_NAME, String.valueOf(assetDO.getCpuType().getCputype_name()));
				result.put(CommonConstants.CPUTYPE, String.valueOf(assetDO.getCpuType().getCpuTypeId()));
		}else{
			result.put(CommonConstants.CPUTYPE, ""); 
			result.put(CommonConstants.CPUTYPE_NAME, ""); 
		}
		
		if(assetDO.getCpuSpeed() != null){
			result.put(CommonConstants.CPUSPEED_NAME, String.valueOf(assetDO.getCpuSpeed().getCpuspeed_name()));
			result.put(CommonConstants.CPUSPEED, String.valueOf(assetDO.getCpuSpeed().getCpuSpeedId()));
		}else{
			result.put(CommonConstants.CPUSPEED, ""); 
			result.put(CommonConstants.CPUSPEED_NAME, ""); 
		}
		
		if(assetDO.getFdd() != null){
			result.put(CommonConstants.FDD, String.valueOf(assetDO.getFdd()));
		}else{
			result.put(CommonConstants.FDD, "");
		}
		if(assetDO.getRam() != null){
			result.put(CommonConstants.RAM_NAME, String.valueOf(assetDO.getRam().getRam()));
			result.put(CommonConstants.RAM, String.valueOf(assetDO.getRam().getRamId()));
		}else{
			result.put(CommonConstants.RAM, ""); 
			result.put(CommonConstants.RAM_NAME, ""); 
		}
		
		if(assetDO.getHdd() != null){
			result.put(CommonConstants.HDD_NAME, String.valueOf(assetDO.getHdd().getHdd()));
			result.put(CommonConstants.HDD, String.valueOf(assetDO.getHdd().getHddId()));
		}else{
			result.put(CommonConstants.HDD, ""); 
			result.put(CommonConstants.HDD_NAME, ""); 
		}
		
		if(assetDO.getCdd_dvd() != null){
			result.put(CommonConstants.CDD, String.valueOf(assetDO.getCdd_dvd().getCd_dvdname()));
			result.put(CommonConstants.CDD_DVDID, String.valueOf(assetDO.getCdd_dvd().getCd_dvdId()));
		}else{
			result.put(CommonConstants.CDD, "");
			result.put(CommonConstants.CDD_DVDID, "");
		}
		if(assetDO.getInternet() != null){
			result.put(CommonConstants.INTERNET, String.valueOf(assetDO.getInternet()));
		}else{
			result.put(CommonConstants.INTERNET, "");
		}
		if(assetDO.getUsb() != null){
			result.put(CommonConstants.USB, String.valueOf(assetDO.getUsb()));
		}else{
			result.put(CommonConstants.USB, "");
		}
		
		if(assetDO.getIpAddress() != null){
			result.put(CommonConstants.IPADDRESS, String.valueOf(assetDO.getIpAddress()));
		}else{
			result.put(CommonConstants.IPADDRESS, "");
		}
		
		if(assetDO.getOffice() != null){
			result.put(CommonConstants.MSOFFICE_NAME, String.valueOf(assetDO.getOffice().getOffice_name()));
			result.put(CommonConstants.MSOFFICE, String.valueOf(assetDO.getOffice().getOfficeId()));
		}else{
			result.put(CommonConstants.MSOFFICE, ""); 
			result.put(CommonConstants.MSOFFICE_NAME, ""); 
		}
		
		if(assetDO.getMail() != null){
			result.put(CommonConstants.MAILID_NAME, String.valueOf(assetDO.getMail().getMail_name()));
			result.put(CommonConstants.MAILID, String.valueOf(assetDO.getMail().getMailId()));
		}else{
			result.put(CommonConstants.MAILID_NAME, ""); 
			result.put(CommonConstants.MAILID, ""); 
		}
		
		if(assetDO.getOSModel() != null){
			result.put(CommonConstants.OS_NAME, String.valueOf(assetDO.getOSModel().getOs()));
			result.put(CommonConstants.OSMODEL, String.valueOf(assetDO.getOSModel().getOsId()));
		}else{
			result.put(CommonConstants.OSMODEL, ""); 
			result.put(CommonConstants.OS_NAME, ""); 
		}*/
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(assetDO.getUpdatedon())));
		if(assetDO.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(assetDO.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		return result;
	}
}
