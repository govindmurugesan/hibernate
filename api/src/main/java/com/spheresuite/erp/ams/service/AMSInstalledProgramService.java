package com.spheresuite.erp.ams.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.ams.dao.AMSInstalledProgramsDAO;
import com.spheresuite.erp.domainobject.AMSInstalledProgramsDO;
@Service
@Transactional
public class AMSInstalledProgramService {
	static Logger logger = Logger.getLogger(AMSInstalledProgramService.class.getName());
	@Autowired
	private AMSInstalledProgramsDAO installedPrgrmsDAO;

	@Transactional
	public boolean persist(AMSInstalledProgramsDO installedPrgrms) {
		return installedPrgrmsDAO.persist(installedPrgrms);
	}

	@Transactional
	public List<AMSInstalledProgramsDO> retrieveActive() {
		return installedPrgrmsDAO.retrieveActive();
	}

	@Transactional
	public List<AMSInstalledProgramsDO> retrieveById(Long Id) {
		return installedPrgrmsDAO.retrieveById(Id);
	}

	@Transactional
	public List<AMSInstalledProgramsDO> retrieve() {
		return installedPrgrmsDAO.retrieve();
	}

	@Transactional
	public boolean update(AMSInstalledProgramsDO installedPrgrms) {
		return installedPrgrmsDAO.update(installedPrgrms);
	}

}
