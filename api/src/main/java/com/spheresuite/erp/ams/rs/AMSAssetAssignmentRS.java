package com.spheresuite.erp.ams.rs;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.ams.service.AMSAssetAssignmentService;
import com.spheresuite.erp.ams.service.AMSAssetService;
import com.spheresuite.erp.ams.service.AMSUnitService;
import com.spheresuite.erp.ams.web.util.AMSAssetAssignmenttUtil;
import com.spheresuite.erp.domainobject.AMSAssetAssignmentDO;
import com.spheresuite.erp.domainobject.AMSAssetDO;
import com.spheresuite.erp.domainobject.AMSUnitDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/amsAssignAsset")
public class AMSAssetAssignmentRS {

	String validation = null;
	static Logger logger = Logger.getLogger(AMSAssetAssignmentRS.class.getName());

	@Autowired
	private AMSAssetService assetService;
	
	@Autowired
	private AMSAssetAssignmentService assetAssignmentService;
	
	@Autowired
	private AMSUnitService amsUnitService;
	
	@Autowired
	private EmployeeService employeeService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) { 
			
		try {
			if (WebManager.authenticateSession(request)) {
				AMSAssetAssignmentDO assetAssignmentDO = new AMSAssetAssignmentDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.ASSETNUMBER) != null && !inputJSON.get(CommonConstants.ASSETNUMBER).toString().isEmpty()){
			 			List<AMSAssetDO> assetList = assetService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ASSETNUMBER).toString()));
			 			if(assetList != null && assetList.size() > 0){
			 				AMSAssetDO assetAssetDO = new AMSAssetDO();
			 				assetAssetDO = assetList.get(0);
			 				assetAssetDO.setStatus(CommonConstants.ACTIVE);
			 				if(assetService.update(assetAssetDO)){
			 					assetAssignmentDO.setAsset(assetList.get(0));
			 				
						 		if(inputJSON.get(CommonConstants.EMPID) != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
						 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
							 		if(employeeList != null && employeeList.size() > 0){
							 			assetAssignmentDO.setEmployee(employeeList.get(0));
							 		}
						 		}
			 		
						 		if(inputJSON.get(CommonConstants.UNIT) != null && !inputJSON.get(CommonConstants.UNIT).toString().isEmpty()){
						 			List<AMSUnitDO> unitList = amsUnitService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.UNIT).toString()));
						 			if(unitList != null && unitList.size() > 0){
						 				assetAssignmentDO.setUnit(unitList.get(0));
						 			}
						 		}
						 		assetAssignmentDO.setAssignedDate(new Date());
						 		assetAssignmentDO.setUpdatedon(new Date());
						 		assetAssignmentDO.setCreatedon(new Date());
						 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
						 			assetAssignmentDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
						 			assetAssignmentDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
						 		}
						 		assetAssignmentDO.setStatus(CommonConstants.ACTIVE);
						 		if(!assetAssignmentService.persist(assetAssignmentDO)){
									return CommonWebUtil.buildErrorResponse("Asset Already Assigned").toString();
								}
								CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Asset Assigned");
			 				}
			 			}
		 			}
		 		}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retriveAssignedAsset/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveAssignedAsset(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<AMSUnitDO> unitList = amsUnitService.retrieveByUnitHead(inputJSON.get(CommonConstants.ID).toString());
					if(unitList != null && unitList.size() > 0){
						List<AMSAssetAssignmentDO> assetList = assetAssignmentService.retrieveByUnit(unitList.get(0).getUnitId());
						respJSON = AMSAssetAssignmenttUtil.getAssetList(assetList).toString();
					}else{
						return CommonWebUtil.buildErrorResponse("Your Not An Unit Head").toString();
					}
					
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retriveByEmpId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveByEmpId(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<AMSAssetAssignmentDO> assetList = assetAssignmentService.retrieveByEmpId(inputJSON.get(CommonConstants.ID).toString());
					respJSON = AMSAssetAssignmenttUtil.getAssetList(assetList).toString();
					
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				AMSAssetAssignmentDO assetAssignmnetDO = new AMSAssetAssignmentDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<AMSAssetAssignmentDO> assignList = assetAssignmentService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		assetAssignmnetDO = assignList.get(0);
			 		if(inputJSON.get(CommonConstants.ASSETID) != null && !inputJSON.get(CommonConstants.ASSETID).toString().isEmpty()){
			 			List<AMSAssetDO> assetList = assetService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ASSETID).toString()));
			 			if(assetList != null && assetList.size() > 0){
			 				AMSAssetDO assetAssetDO = new AMSAssetDO();
			 				assetAssetDO = assetList.get(0);
			 				assetAssetDO.setStatus(CommonConstants.INACTIVE);
			 				if(assetService.update(assetAssetDO)){
			 					assetAssignmnetDO.setReleasedDate(new Date());
						 		assetAssignmnetDO.setUpdatedon(new Date());
						 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
						 			assetAssignmnetDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
						 		}
						 		if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
						 			assetAssignmnetDO.setStatus((char) (inputJSON.get(CommonConstants.STATUS).toString().charAt(0)));
						 		}
						 		if(!assetAssignmentService.update(assetAssignmnetDO)){
									return CommonWebUtil.buildErrorResponse("Asset Already released").toString();
								}
						 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Asset released");
			 				}
			 			}
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
}
