package com.spheresuite.erp.ams.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.ams.dao.AMSUnitDAO;
import com.spheresuite.erp.domainobject.AMSUnitDO;
@Service
@Transactional
public class AMSUnitService {
	static Logger logger = Logger.getLogger(AMSUnitService.class.getName());
	@Autowired
	private AMSUnitDAO amsUnitDAO;

	@Transactional
	public boolean persist(AMSUnitDO unitDO) {
		return amsUnitDAO.persist(unitDO);
	}

	@Transactional
	public List<AMSUnitDO> retrieveActive() {
		return amsUnitDAO.retrieveActive();
	}

	@Transactional
	public List<AMSUnitDO> retrieveById(Long Id) {
		return amsUnitDAO.retrieveById(Id);
	}
	
	@Transactional
	public List<AMSUnitDO> retrieveByUnitHead(String Id) {
		return amsUnitDAO.retrieveByUnitHead(Id);
	}

	@Transactional
	public List<AMSUnitDO> retrieve() {
		return amsUnitDAO.retrieve();
	}

	@Transactional
	public boolean update(AMSUnitDO unitDO) {
		return amsUnitDAO.update(unitDO);
	}

}
