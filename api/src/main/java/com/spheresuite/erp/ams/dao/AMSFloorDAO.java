package com.spheresuite.erp.ams.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.AMSFloorDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class AMSFloorDAO {
	static Logger logger = Logger.getLogger(AMSFloorDAO.class.getName());
	@Autowired
	private GenericDAOImpl<AMSFloorDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(AMSFloorDO floorDO) {
		boolean persistStatus = true;
		try {
			List<AMSFloorDO> floorDOList = null;
			floorDOList = this.sessionFactory.getCurrentSession().getNamedQuery(AMSFloorDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, floorDO.getFloorName())
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
					if(floorDOList != null && floorDOList.size() > 0){
						persistStatus = false;
					}else{
						genericObject.persist(floorDO);
					}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	
	public List<AMSFloorDO> retrieve() {
		List<AMSFloorDO> frequencyList = null;
		try {
			frequencyList = genericObject.retrieve(AMSFloorDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return frequencyList;
	}
	
	public List<AMSFloorDO> retrieveActive() {
		List<AMSFloorDO> floorList = null;
		try {
			return genericObject.retrieveActive(CommonConstants.ACTIVE, 
					AMSFloorDO.FIND_BY_STATUS, AMSFloorDO.class);
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return floorList;
	}
	
	@SuppressWarnings("unchecked")
	public List<AMSFloorDO> retrieveById(Long Id) {
		List<AMSFloorDO> floorList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(AMSFloorDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, Id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return floorList;
	}
	
	
/*	@SuppressWarnings("unchecked")
	public List<CMSFrequencyDO> retrieveByName(String name) {
		List<CMSFrequencyDO> frequencyList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(CMSFrequencyDO.FIND_BY_NAME_ALL)
					.setParameter(CommonConstants.NAME, name)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return frequencyList;
	}
	*/
	
	@SuppressWarnings("unchecked")
	public boolean update(AMSFloorDO floorDO) {
		boolean updateStatus = true;
		try {
			List<AMSFloorDO> floorDoList = null;
			floorDoList = this.sessionFactory.getCurrentSession().getNamedQuery(AMSFloorDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, floorDO.getFloorName())
					.setParameter(CommonConstants.ID, floorDO.getFloorId())
					.setParameter(CommonConstants.STATUS, floorDO.getStatus())
					.list();
			if(floorDoList != null && floorDoList.size() > 0){
				updateStatus =  false;
			}else{
				genericObject.merge(floorDO);
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return updateStatus;
	}

}
