package com.spheresuite.erp.ams.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.ams.dao.AMSCPUTypeDAO;
import com.spheresuite.erp.domainobject.AMSCPUTypeDO;
@Service
@Transactional
public class AMSCPUTypeService {
	static Logger logger = Logger.getLogger(AMSCPUTypeService.class.getName());
	@Autowired
	private AMSCPUTypeDAO amsCpuTypeDAO;

	@Transactional
	public boolean persist(AMSCPUTypeDO amsCPUTypeDO) {
		return amsCpuTypeDAO.persist(amsCPUTypeDO);
	}

	@Transactional
	public List<AMSCPUTypeDO> retrieveActive() {
		return amsCpuTypeDAO.retrieveActive();
	}

	@Transactional
	public List<AMSCPUTypeDO> retrieveById(Long Id) {
		return amsCpuTypeDAO.retrieveById(Id);
	}

	@Transactional
	public List<AMSCPUTypeDO> retrieve() {
		return amsCpuTypeDAO.retrieve();
	}

	@Transactional
	public boolean update(AMSCPUTypeDO amsCPUTypeDO) {
		return amsCpuTypeDAO.update(amsCPUTypeDO);
	}

}
