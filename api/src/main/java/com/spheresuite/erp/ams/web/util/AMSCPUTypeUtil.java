package com.spheresuite.erp.ams.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.AMSCPUTypeDO;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;

@Component
public class AMSCPUTypeUtil {

	private AMSCPUTypeUtil() {
	}

	public static JSONObject getCPUTypeList(List<AMSCPUTypeDO> cpuTypeList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (AMSCPUTypeDO cpuTypeDO : cpuTypeList) {
				resultJSONArray.put(getCPUTypeObject(cpuTypeDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getCPUTypeObject(AMSCPUTypeDO cpuTypeDO)
			throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(cpuTypeDO.getCpuTypeId()));
		result.put(CommonConstants.NAME, String.valueOf(cpuTypeDO.getCputype_name()));
		result.put(CommonConstants.STATUS,
				String.valueOf(cpuTypeDO.getStatus()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil
				.convertDateToStringWithtime(cpuTypeDO.getUpdatedon())));
		if (cpuTypeDO.getCputype_comments() != null) {
			result.put(CommonConstants.COMMENT,
					String.valueOf(cpuTypeDO.getCputype_comments()));
		} else {
			result.put(CommonConstants.COMMENT, "");
		}

		return result;
	}
}
