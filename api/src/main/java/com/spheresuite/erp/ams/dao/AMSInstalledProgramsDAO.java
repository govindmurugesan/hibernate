package com.spheresuite.erp.ams.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.AMSInstalledProgramsDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class AMSInstalledProgramsDAO {
	static Logger logger = Logger.getLogger(AMSInstalledProgramsDAO.class.getName());
	@Autowired
	private GenericDAOImpl<AMSInstalledProgramsDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(AMSInstalledProgramsDO installedProgramsDO) {
		boolean persistStatus = true;
		try {
			List<AMSInstalledProgramsDO> prgrmList = null;
			prgrmList = this.sessionFactory.getCurrentSession().getNamedQuery(AMSInstalledProgramsDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, installedProgramsDO.getProgram_name())
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
					if(prgrmList != null && prgrmList.size() > 0){
						persistStatus = false;
					}else{
						genericObject.persist(installedProgramsDO);
					}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	
	public List<AMSInstalledProgramsDO> retrieve() {
		List<AMSInstalledProgramsDO> prgrmList = null;
		try {
			prgrmList = genericObject.retrieve(AMSInstalledProgramsDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return prgrmList;
	}
	
	public List<AMSInstalledProgramsDO> retrieveActive() {
		List<AMSInstalledProgramsDO> prgrmList = null;
		try {
			return genericObject.retrieveActive(CommonConstants.ACTIVE, 
					AMSInstalledProgramsDO.FIND_BY_STATUS, AMSInstalledProgramsDO.class);
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return prgrmList;
	}
	
	@SuppressWarnings("unchecked")
	public List<AMSInstalledProgramsDO> retrieveById(Long Id) {
		List<AMSInstalledProgramsDO> prgrmList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(AMSInstalledProgramsDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, Id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return prgrmList;
	}

	
	@SuppressWarnings("unchecked")
	public boolean update(AMSInstalledProgramsDO instlledPrgrmDO) {
		boolean updateStatus = true;
		try {
			List<AMSInstalledProgramsDO> prgrmList = null;
			prgrmList = this.sessionFactory.getCurrentSession().getNamedQuery(AMSInstalledProgramsDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, instlledPrgrmDO.getProgram_name())
					.setParameter(CommonConstants.ID, instlledPrgrmDO.getInstalledProgramId())
					.setParameter(CommonConstants.STATUS, instlledPrgrmDO.getStatus())
					.list();
			if(prgrmList != null && prgrmList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(instlledPrgrmDO);
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return updateStatus;
	}

}
