package com.spheresuite.erp.ams.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.ams.dao.AMSMailDAO;
import com.spheresuite.erp.domainobject.AMSMailDO;
@Service
@Transactional
public class AMSMailService {
	static Logger logger = Logger.getLogger(AMSMailService.class.getName());
	@Autowired
	private AMSMailDAO amsMailDAO;

	@Transactional
	public boolean persist(AMSMailDO mailDO) {
		return amsMailDAO.persist(mailDO);
	}

	@Transactional
	public List<AMSMailDO> retrieveActive() {
		return amsMailDAO.retrieveActive();
	}

	@Transactional
	public List<AMSMailDO> retrieveById(Long Id) {
		return amsMailDAO.retrieveById(Id);
	}

	@Transactional
	public List<AMSMailDO> retrieve() {
		return amsMailDAO.retrieve();
	}

	@Transactional
	public boolean update(AMSMailDO mailDO) {
		return amsMailDAO.update(mailDO);
	}

}
