package com.spheresuite.erp.ams.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.AMSRamDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class AMSRamDAO {
	static Logger logger = Logger.getLogger(AMSRamDAO.class.getName());
	@Autowired
	private GenericDAOImpl<AMSRamDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(AMSRamDO ramDO) {
		boolean persistStatus = true;
		try {
			List<AMSRamDO> ramDOList = null;
			ramDOList = this.sessionFactory.getCurrentSession().getNamedQuery(AMSRamDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, ramDO.getRam())
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
					if(ramDOList != null && ramDOList.size() > 0){
						persistStatus = false;
					}else{
						genericObject.persist(ramDO);
					}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	
	public List<AMSRamDO> retrieve() {
		List<AMSRamDO> ramDOList = null;
		try {
			ramDOList = genericObject.retrieve(AMSRamDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return ramDOList;
	}
	
	public List<AMSRamDO> retrieveActive() {
		List<AMSRamDO> ramDOList = null;
		try {
			return genericObject.retrieveActive(CommonConstants.ACTIVE, 
					AMSRamDO.FIND_BY_STATUS, AMSRamDO.class);
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return ramDOList;
	}
	
	@SuppressWarnings("unchecked")
	public List<AMSRamDO> retrieveById(Long Id) {
		List<AMSRamDO> ramDOList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(AMSRamDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, Id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return ramDOList;
	}

	
	@SuppressWarnings("unchecked")
	public boolean update(AMSRamDO ramDO) {
		boolean updateStatus = true;
		try {
			List<AMSRamDO> ramDOList = null;
			ramDOList = this.sessionFactory.getCurrentSession().getNamedQuery(AMSRamDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, ramDO.getRam())
					.setParameter(CommonConstants.ID, ramDO.getRamId())
					.setParameter(CommonConstants.STATUS, ramDO.getStatus())
					.list();
			if(ramDOList != null && ramDOList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(ramDO);
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return updateStatus;
	}

}
