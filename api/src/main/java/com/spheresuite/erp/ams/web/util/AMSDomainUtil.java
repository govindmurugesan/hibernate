package com.spheresuite.erp.ams.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.AMSDomainDO;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;

@Component
public class AMSDomainUtil {

	private AMSDomainUtil() {
	}

	public static JSONObject getDomainList(List<AMSDomainDO> domainList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (AMSDomainDO domain : domainList) {
				resultJSONArray.put(getDomainObject(domain));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getDomainObject(AMSDomainDO domain)
			throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(domain.getDomainId()));
		result.put(CommonConstants.NAME, String.valueOf(domain.getDoaminName()));
		result.put(CommonConstants.STATUS,
				String.valueOf(domain.getStatus()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil
				.convertDateToStringWithtime(domain.getUpdatedon())));

		if (domain.getDoaminComments() != null) {
			result.put(CommonConstants.COMMENT,
					String.valueOf(domain.getDoaminComments()));
		} else {
			result.put(CommonConstants.COMMENT, "");
		}
		return result;
	}
}
