package com.spheresuite.erp.ams.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.AMSMailDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class AMSMailDAO {
	static Logger logger = Logger.getLogger(AMSMailDAO.class.getName());
	@Autowired
	private GenericDAOImpl<AMSMailDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(AMSMailDO mailDO) {
		boolean persistStatus = true;
		try {
			List<AMSMailDO> mailDOList = null;
			mailDOList = this.sessionFactory.getCurrentSession().getNamedQuery(AMSMailDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, mailDO.getMail_name())
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
					if(mailDOList != null && mailDOList.size() > 0){
						persistStatus = false;
					}else{
						genericObject.persist(mailDO);
					}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	
	public List<AMSMailDO> retrieve() {
		List<AMSMailDO> mailDOList = null;
		try {
			mailDOList = genericObject.retrieve(AMSMailDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return mailDOList;
	}
	
	public List<AMSMailDO> retrieveActive() {
		List<AMSMailDO> mailDOList = null;
		try {
			return genericObject.retrieveActive(CommonConstants.ACTIVE, 
					AMSMailDO.FIND_BY_STATUS, AMSMailDO.class);
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return mailDOList;
	}
	
	@SuppressWarnings("unchecked")
	public List<AMSMailDO> retrieveById(Long Id) {
		List<AMSMailDO> mailDOList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(AMSMailDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, Id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return mailDOList;
	}

	
	@SuppressWarnings("unchecked")
	public boolean update(AMSMailDO mailDO) {
		boolean updateStatus = true;
		try {
			List<AMSMailDO> mailDOList = null;
			mailDOList = this.sessionFactory.getCurrentSession().getNamedQuery(AMSMailDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, mailDO.getMail_name())
					.setParameter(CommonConstants.ID, mailDO.getMailId())
					.setParameter(CommonConstants.STATUS, mailDO.getStatus())
					.list();
			if(mailDOList != null && mailDOList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(mailDO);
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return updateStatus;
	}

}
