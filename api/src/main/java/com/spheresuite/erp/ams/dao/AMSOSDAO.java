package com.spheresuite.erp.ams.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.AMSOSDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class AMSOSDAO {
	static Logger logger = Logger.getLogger(AMSOSDAO.class.getName());
	@Autowired
	private GenericDAOImpl<AMSOSDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(AMSOSDO osDO) {
		boolean persistStatus = true;
		try {
			List<AMSOSDO> OSDOList = null;
			OSDOList = this.sessionFactory.getCurrentSession().getNamedQuery(AMSOSDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, osDO.getOs())
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
					if(OSDOList != null && OSDOList.size() > 0){
						persistStatus = false;
					}else{
						genericObject.persist(osDO);
					}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	
	public List<AMSOSDO> retrieve() {
		List<AMSOSDO> OSDOList = null;
		try {
			OSDOList = genericObject.retrieve(AMSOSDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return OSDOList;
	}
	
	public List<AMSOSDO> retrieveActive() {
		List<AMSOSDO> OSDOList = null;
		try {
			return genericObject.retrieveActive(CommonConstants.ACTIVE, 
					AMSOSDO.FIND_BY_STATUS, AMSOSDO.class);
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return OSDOList;
	}
	
	@SuppressWarnings("unchecked")
	public List<AMSOSDO> retrieveById(Long Id) {
		List<AMSOSDO> OSDOList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(AMSOSDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, Id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return OSDOList;
	}

	
	@SuppressWarnings("unchecked")
	public boolean update(AMSOSDO OSDO) {
		boolean updateStatus = true;
		try {
			List<AMSOSDO> OSDOList = null;
			OSDOList = this.sessionFactory.getCurrentSession().getNamedQuery(AMSOSDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, OSDO.getOs())
					.setParameter(CommonConstants.ID, OSDO.getOsId())
					.setParameter(CommonConstants.STATUS, OSDO.getStatus())
					.list();
			if(OSDOList != null && OSDOList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(OSDO);
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return updateStatus;
	}

}
