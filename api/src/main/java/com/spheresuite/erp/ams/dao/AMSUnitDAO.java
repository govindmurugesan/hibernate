package com.spheresuite.erp.ams.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.AMSUnitDO;
import com.spheresuite.erp.util.CommonConstants;

@Repository
public class AMSUnitDAO {
	static Logger logger = Logger.getLogger(AMSUnitDAO.class.getName());
	@Autowired
	private GenericDAOImpl<AMSUnitDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(AMSUnitDO unitDO) {
		boolean persistStatus = true;
		try {
			List<AMSUnitDO> unitDOList = null;
			unitDOList = this.sessionFactory.getCurrentSession().getNamedQuery(AMSUnitDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, unitDO.getUnitName())
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
					if(unitDOList != null && unitDOList.size() > 0){
						persistStatus = false;
					}else{
						genericObject.persist(unitDO);
					}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	
	public List<AMSUnitDO> retrieve() {
		List<AMSUnitDO> unitList = null;
		try {
			unitList = genericObject.retrieve(AMSUnitDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return unitList;
	}
	
	public List<AMSUnitDO> retrieveActive() {
		List<AMSUnitDO> unitList = null;
		try {
			return genericObject.retrieveActive(CommonConstants.ACTIVE, 
					AMSUnitDO.FIND_BY_STATUS, AMSUnitDO.class);
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return unitList;
	}
	
	@SuppressWarnings("unchecked")
	public List<AMSUnitDO> retrieveById(Long Id) {
		List<AMSUnitDO> unitList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(AMSUnitDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, Id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return unitList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<AMSUnitDO> retrieveByUnitHead(String empId) {
		List<AMSUnitDO> unitList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(AMSUnitDO.FIND_BY_UNIIHEAD)
					.setParameter(CommonConstants.ID, empId)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return unitList;
	}
	
	@SuppressWarnings("unchecked")
	public boolean update(AMSUnitDO unitDO) {
		boolean updateStatus = true;
		try {
			List<AMSUnitDO> unitList = null;
			unitList = this.sessionFactory.getCurrentSession().getNamedQuery(AMSUnitDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, unitDO.getUnitName())
					.setParameter(CommonConstants.ID, unitDO.getUnitId())
					.setParameter(CommonConstants.STATUS, unitDO.getStatus())
					.list();
			if(unitList != null && unitList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(unitDO);
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return updateStatus;
	}

}
