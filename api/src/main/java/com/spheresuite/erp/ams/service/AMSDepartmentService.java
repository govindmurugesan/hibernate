package com.spheresuite.erp.ams.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.ams.dao.AMSDepartmentDAO;
import com.spheresuite.erp.domainobject.AMSDepartmentDO;
@Service
@Transactional
public class AMSDepartmentService {
	static Logger logger = Logger.getLogger(AMSDepartmentService.class.getName());
	@Autowired
	private AMSDepartmentDAO amsDepartmentDAO;

	@Transactional
	public boolean persist(AMSDepartmentDO floorDO) {
		return amsDepartmentDAO.persist(floorDO);
	}

	@Transactional
	public List<AMSDepartmentDO> retrieveActive() {
		return amsDepartmentDAO.retrieveActive();
	}

	@Transactional
	public List<AMSDepartmentDO> retrieveById(Long Id) {
		return amsDepartmentDAO.retrieveById(Id);
	}

	@Transactional
	public List<AMSDepartmentDO> retrieve() {
		return amsDepartmentDAO.retrieve();
	}

	@Transactional
	public boolean update(AMSDepartmentDO floorDO) {
		return amsDepartmentDAO.update(floorDO);
	}
}
