package com.spheresuite.erp.ams.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.AMSTypeDO;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;

@Component
public class AMSTypeUtil {

	private AMSTypeUtil() {
	}

	public static JSONObject getTypeList(List<AMSTypeDO> typeList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (AMSTypeDO typeDO : typeList) {
				resultJSONArray.put(getTypeObject(typeDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getTypeObject(AMSTypeDO typeDO)
			throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(typeDO.getTypeId()));
		result.put(CommonConstants.NAME, String.valueOf(typeDO.getTypeName()));
		result.put(CommonConstants.STATUS,
				String.valueOf(typeDO.getStatus()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil
				.convertDateToStringWithtime(typeDO.getUpdatedon())));

		if (typeDO.getTypeComments() != null) {
			result.put(CommonConstants.COMMENT,
					String.valueOf(typeDO.getTypeComments()));
		} else {
			result.put(CommonConstants.COMMENT, "");
		}
		return result;
	}
}
