package com.spheresuite.erp.ams.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.ams.dao.AMSMakeDAO;
import com.spheresuite.erp.domainobject.AMSMakeDO;
@Service
@Transactional
public class AMSMakeService {
	static Logger logger = Logger.getLogger(AMSMakeService.class.getName());
	@Autowired
	private AMSMakeDAO amsMakeDAO;

	@Transactional
	public boolean persist(AMSMakeDO makeDO) {
		return amsMakeDAO.persist(makeDO);
	}

	@Transactional
	public List<AMSMakeDO> retrieveActive() {
		return amsMakeDAO.retrieveActive();
	}

	@Transactional
	public List<AMSMakeDO> retrieveById(Long Id) {
		return amsMakeDAO.retrieveById(Id);
	}

	@Transactional
	public List<AMSMakeDO> retrieve() {
		return amsMakeDAO.retrieve();
	}

	@Transactional
	public boolean update(AMSMakeDO makeDO) {
		return amsMakeDAO.update(makeDO);
	}

}
