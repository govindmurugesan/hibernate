package com.spheresuite.erp.ams.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.AMSPCUnderDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class AMSPCUnderDAO {
	static Logger logger = Logger.getLogger(AMSPCUnderDAO.class.getName());
	@Autowired
	private GenericDAOImpl<AMSPCUnderDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(AMSPCUnderDO amspuUnderDO) {
		boolean persistStatus = true;
		try {
			List<AMSPCUnderDO> amsPCunderDOList = null;
			amsPCunderDOList = this.sessionFactory.getCurrentSession().getNamedQuery(AMSPCUnderDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, amspuUnderDO.getPcunder())
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
					if(amsPCunderDOList != null && amsPCunderDOList.size() > 0){
						persistStatus = false;
					}else{
						genericObject.persist(amspuUnderDO);
					}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	
	public List<AMSPCUnderDO> retrieve() {
		List<AMSPCUnderDO> amsPCunderDOList = null;
		try {
			amsPCunderDOList = genericObject.retrieve(AMSPCUnderDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return amsPCunderDOList;
	}
	
	public List<AMSPCUnderDO> retrieveActive() {
		List<AMSPCUnderDO> amsPCunderDOList = null;
		try {
			return genericObject.retrieveActive(CommonConstants.ACTIVE, 
					AMSPCUnderDO.FIND_BY_STATUS, AMSPCUnderDO.class);
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return amsPCunderDOList;
	}
	
	@SuppressWarnings("unchecked")
	public List<AMSPCUnderDO> retrieveById(Long Id) {
		List<AMSPCUnderDO> amsPCunderDOList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(AMSPCUnderDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, Id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return amsPCunderDOList;
	}

	
	@SuppressWarnings("unchecked")
	public boolean update(AMSPCUnderDO pcUnderDO) {
		boolean updateStatus = true;
		try {
			List<AMSPCUnderDO> amsCDTypeDOList = null;
			amsCDTypeDOList = this.sessionFactory.getCurrentSession().getNamedQuery(AMSPCUnderDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, pcUnderDO.getPcunder())
					.setParameter(CommonConstants.ID, pcUnderDO.getPcUnderId())
					.setParameter(CommonConstants.STATUS, pcUnderDO.getStatus())
					.list();
			if(amsCDTypeDOList != null && amsCDTypeDOList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(pcUnderDO);
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return updateStatus;
	}

}
