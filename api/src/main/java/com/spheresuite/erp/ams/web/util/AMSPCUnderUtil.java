package com.spheresuite.erp.ams.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.AMSPCUnderDO;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;

@Component
public class AMSPCUnderUtil {

	private AMSPCUnderUtil() {
	}


	public static JSONObject getPCUnderList(List<AMSPCUnderDO> pcUnderList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (AMSPCUnderDO pcunderDO : pcUnderList) {
				resultJSONArray.put(getMakeObject(pcunderDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getMakeObject(AMSPCUnderDO pcunderDO)
			throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(pcunderDO.getPcUnderId()));
		result.put(CommonConstants.NAME, String.valueOf(pcunderDO.getPcunder()));
		result.put(CommonConstants.STATUS,
				String.valueOf(pcunderDO.getStatus()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil
				.convertDateToStringWithtime(pcunderDO.getUpdatedon())));
		

		if (pcunderDO.getComments() != null) {
			result.put(CommonConstants.COMMENT,
					String.valueOf(pcunderDO.getComments()));
		} else {
			result.put(CommonConstants.COMMENT, "");
		}
		return result;
	}
}
