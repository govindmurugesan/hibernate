package com.spheresuite.erp.ams.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.AMSUnitDO;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;

@Component
public class AMSUnitUtil {

	private AMSUnitUtil() {
	}

	public static JSONObject getUnitList(List<AMSUnitDO> unitList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (AMSUnitDO unit : unitList) {
				resultJSONArray.put(getUnitObject(unit));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getUnitObject(AMSUnitDO unit)
			throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(unit.getUnitId()));
		result.put(CommonConstants.NAME, String.valueOf(unit.getUnitName()));
		if(unit.getUnitHead() != null){
			result.put(CommonConstants.EMPID, String.valueOf(unit.getUnitHead().getEmpId()));
			if(unit.getUnitHead().getMiddlename() != null){
				result.put(CommonConstants.MIDDLENAME, String.valueOf(unit.getUnitHead().getMiddlename()));
			}else{
				result.put(CommonConstants.MIDDLENAME, String.valueOf(""));
			}
			result.put(CommonConstants.FIRSTNAME, String.valueOf(unit.getUnitHead().getFirstname()));
			if(unit.getUnitHead().getLastname() != null){
				result.put(CommonConstants.LASTNAME, String.valueOf(unit.getUnitHead().getLastname()));
			}else{
				result.put(CommonConstants.LASTNAME, String.valueOf(""));
			}
		}
		result.put(CommonConstants.STATUS,
				String.valueOf(unit.getStatus()));
		
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil
				.convertDateToStringWithtime(unit.getUpdatedon())));
		if (unit.getUnitComments() != null) {
			result.put(CommonConstants.COMMENT,
					String.valueOf(unit.getUnitComments()));
		} else {
			result.put(CommonConstants.COMMENT, "");
		}
		return result;
	}
}
