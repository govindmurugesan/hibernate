package com.spheresuite.erp.ams.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.AMSRamDO;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;

@Component
public class AMSRamUtil {

	private AMSRamUtil() {
	}


	public static JSONObject getRamList(List<AMSRamDO> ramList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (AMSRamDO ramDO : ramList) {
				resultJSONArray.put(getDomainObject(ramDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getDomainObject(AMSRamDO ramDO)
			throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(ramDO.getRamId()));
		result.put(CommonConstants.NAME, String.valueOf(ramDO.getRam()));
		result.put(CommonConstants.STATUS,
				String.valueOf(ramDO.getStatus()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil
				.convertDateToStringWithtime(ramDO.getUpdatedon())));

		if (ramDO.getRam_comments() != null) {
			result.put(CommonConstants.COMMENT,
					String.valueOf(ramDO.getRam_comments()));
		} else {
			result.put(CommonConstants.COMMENT, "");
		}
		return result;
	}
}
