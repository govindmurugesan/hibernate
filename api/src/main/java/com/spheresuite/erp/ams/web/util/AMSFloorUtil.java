package com.spheresuite.erp.ams.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.AMSFloorDO;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;

@Component
public class AMSFloorUtil {

	private AMSFloorUtil() {
	}

	public static JSONObject getFloorList(List<AMSFloorDO> floorList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (AMSFloorDO floor : floorList) {
				resultJSONArray.put(getFloorObject(floor));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getFloorObject(AMSFloorDO floorDO)
			throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(floorDO.getFloorId()));
		result.put(CommonConstants.NAME, String.valueOf(floorDO.getFloorName()));
		result.put(CommonConstants.STATUS,
				String.valueOf(floorDO.getStatus()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil
				.convertDateToStringWithtime(floorDO.getUpdatedon())));

		if (floorDO.getFloorComments() != null) {
			result.put(CommonConstants.COMMENT,
					String.valueOf(floorDO.getFloorComments()));
		} else {
			result.put(CommonConstants.COMMENT, "");
		}
		return result;
	}
}
