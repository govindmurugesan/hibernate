package com.spheresuite.erp.ams.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.AMSDepartmentDO;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;

@Component
public class AMSDepartmentUtil {

	private AMSDepartmentUtil() {
	}

	public static JSONObject getDepartmentDetails(List<AMSDepartmentDO> deptList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (AMSDepartmentDO dept : deptList) {
				resultJSONArray.put(getDepartmentObject(dept));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getDepartmentObject(AMSDepartmentDO dept)
			throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(dept.getDepartmentId()));
		result.put(CommonConstants.NAME, String.valueOf(dept.getDepartmentName()));
		result.put(CommonConstants.STATUS,
				String.valueOf(dept.getStatus()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil
				.convertDateToStringWithtime(dept.getUpdatedon())));
		if (dept.getDepartmentComments() != null) {
			result.put(CommonConstants.COMMENT,
					String.valueOf(dept.getDepartmentComments()));
		} else {
			result.put(CommonConstants.COMMENT, "");
		}

	return result;
	}
}
