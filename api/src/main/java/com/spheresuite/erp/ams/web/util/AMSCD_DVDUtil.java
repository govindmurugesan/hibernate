package com.spheresuite.erp.ams.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.AMSCD_DVDDO;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;

@Component
public class AMSCD_DVDUtil {

	private AMSCD_DVDUtil() {
	}

	public static JSONObject getCDTypeList(List<AMSCD_DVDDO> cdTypeList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (AMSCD_DVDDO cdTypeDO : cdTypeList) {
				resultJSONArray.put(getMakeObject(cdTypeDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getMakeObject(AMSCD_DVDDO cdTypeDO)
			throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(cdTypeDO.getCd_dvdId()));
		result.put(CommonConstants.NAME, String.valueOf(cdTypeDO.getCd_dvdname()));
		result.put(CommonConstants.STATUS,
				String.valueOf(cdTypeDO.getStatus()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil
				.convertDateToStringWithtime(cdTypeDO.getUpdatedon())));
		

		if (cdTypeDO.getComments() != null) {
			result.put(CommonConstants.COMMENT,
					String.valueOf(cdTypeDO.getComments()));
		} else {
			result.put(CommonConstants.COMMENT, "");
		}
		return result;
	}
}
