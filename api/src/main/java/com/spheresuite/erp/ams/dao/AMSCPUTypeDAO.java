package com.spheresuite.erp.ams.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.AMSCPUTypeDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class AMSCPUTypeDAO {
	static Logger logger = Logger.getLogger(AMSCPUTypeDAO.class.getName());
	@Autowired
	private GenericDAOImpl<AMSCPUTypeDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(AMSCPUTypeDO amsCpuTypeDO) {
		boolean persistStatus = true;
		try {
			List<AMSCPUTypeDO> AMSCPUTypeDOList = null;
			AMSCPUTypeDOList = this.sessionFactory.getCurrentSession().getNamedQuery(amsCpuTypeDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, amsCpuTypeDO.getCputype_name())
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
					if(AMSCPUTypeDOList != null && AMSCPUTypeDOList.size() > 0){
						persistStatus = false;
					}else{
						genericObject.persist(amsCpuTypeDO);
					}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	
	public List<AMSCPUTypeDO> retrieve() {
		List<AMSCPUTypeDO> amsCpuTypeDOList = null;
		try {
			amsCpuTypeDOList = genericObject.retrieve(AMSCPUTypeDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return amsCpuTypeDOList;
	}
	
	public List<AMSCPUTypeDO> retrieveActive() {
		List<AMSCPUTypeDO> amsCpuTypeDOList = null;
		try {
			return genericObject.retrieveActive(CommonConstants.ACTIVE, 
					AMSCPUTypeDO.FIND_BY_STATUS, AMSCPUTypeDO.class);
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return amsCpuTypeDOList;
	}
	
	@SuppressWarnings("unchecked")
	public List<AMSCPUTypeDO> retrieveById(Long Id) {
		List<AMSCPUTypeDO> amsCpuTypeDOList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(AMSCPUTypeDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, Id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return amsCpuTypeDOList;
	}

	
	@SuppressWarnings("unchecked")
	public boolean update(AMSCPUTypeDO cupType) {
		boolean updateStatus = true;
		try {
			List<AMSCPUTypeDO> amsCpuTypeDOList = null;
			amsCpuTypeDOList = this.sessionFactory.getCurrentSession().getNamedQuery(AMSCPUTypeDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, cupType.getCputype_name())
					.setParameter(CommonConstants.ID, cupType.getCpuTypeId())
					.setParameter(CommonConstants.STATUS, cupType.getStatus())
					.list();
			if(amsCpuTypeDOList != null && amsCpuTypeDOList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(cupType);
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return updateStatus;
	}

}
