package com.spheresuite.erp.ams.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.AMSOfficeDO;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;

@Component
public class AMSOfficeUtil {

	private AMSOfficeUtil() {
	}
	
	public static JSONObject getOfficeList(List<AMSOfficeDO> AMSOfficeDO) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (AMSOfficeDO officeDO : AMSOfficeDO) {
				resultJSONArray.put(getMakeObject(officeDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getMakeObject(AMSOfficeDO officeDO)
			throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(officeDO.getOfficeId()));
		result.put(CommonConstants.NAME, String.valueOf(officeDO.getOffice_name()));
		result.put(CommonConstants.STATUS,
				String.valueOf(officeDO.getStatus()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil
				.convertDateToStringWithtime(officeDO.getUpdatedon())));

		if (officeDO.getOffice_comments() != null) {
			result.put(CommonConstants.COMMENT,
					String.valueOf(officeDO.getOffice_comments()));
		} else {
			result.put(CommonConstants.COMMENT, "");
		}
		return result;
	}
}
