package com.spheresuite.erp.cms.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.CMSRemainderDocDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class CMSRemainderDocDAO {
	static Logger logger = Logger.getLogger(CMSRemainderDocDAO.class.getName());
	@Autowired
	private GenericDAOImpl<CMSRemainderDocDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public boolean delete(Long id)  {
		try {
			org.hibernate.Query query = this.sessionFactory.getCurrentSession().createQuery("delete from CMSRemainderDocDO e where e.cmsremainderId=:id")
					.setParameter(CommonConstants.ID, id);
			query.executeUpdate();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return true;
	}
	
	
	public CMSRemainderDocDO persist(CMSRemainderDocDO cmsRemainderDocDO)  {
		try {
			genericObject.persist(cmsRemainderDocDO);
		} catch (Exception eException) {
		} finally {
		}
		return cmsRemainderDocDO;
	}
	
	
	public List<CMSRemainderDocDO> retrieve()  {
		List<CMSRemainderDocDO> cmsRemainderDocList = null;
		try {
			cmsRemainderDocList = genericObject.retrieve(CMSRemainderDocDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return cmsRemainderDocList;
	}
	
	@SuppressWarnings("unchecked")
	public List<CMSRemainderDocDO> retrieveByCMSId(Long id)  {
		List<CMSRemainderDocDO> cmsRemainderDocList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(CMSRemainderDocDO.FIND_BY_CMSREMAINDER_ID)
			.setParameter(CommonConstants.ID, id)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return cmsRemainderDocList;
	}
	
	public CMSRemainderDocDO update(CMSRemainderDocDO cmsRemainderDocDO)  {
		try {
			genericObject.merge(cmsRemainderDocDO);
		} catch (Exception eException) {
		} finally {
		}
		return cmsRemainderDocDO;
	}

}
