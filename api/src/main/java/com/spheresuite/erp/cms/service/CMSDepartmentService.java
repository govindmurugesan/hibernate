package com.spheresuite.erp.cms.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.cms.dao.CMSDepartmentDAO;
import com.spheresuite.erp.domainobject.CMSDepartmentDO;
@Service
@Transactional
public class CMSDepartmentService {
	static Logger logger = Logger.getLogger(CMSDepartmentService.class.getName());
	@Autowired
	private CMSDepartmentDAO departmentDAO;

	@Transactional
	public boolean persist(CMSDepartmentDO departmentDO) {
		return departmentDAO.persist(departmentDO);
	}

	@Transactional
	public List<CMSDepartmentDO> retrieveActive() {
		return departmentDAO.retrieveActive();
	}

	@Transactional
	public List<CMSDepartmentDO> retrieveById(Long Id) {
		return departmentDAO.retrieveById(Id);
	}

	@Transactional
	public List<CMSDepartmentDO> retrieve() {
		return departmentDAO.retrieve();
	}

	@Transactional
	public boolean update(CMSDepartmentDO departmentDO) {
		return departmentDAO.update(departmentDO);
	}

	/*@Transactional
	public List<Long> retrieveisManager(Long Id) {
		return departmentDAO.retrieveisManager(Id);
	}*/
	
	@Transactional
	public List<CMSDepartmentDO> retrieveByName(String name) {
		return departmentDAO.retrieveByName(name);
	}
}
