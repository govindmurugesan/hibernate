package com.spheresuite.erp.cms.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.cms.dao.CMSSubCategoryDAO;
import com.spheresuite.erp.domainobject.CMSSubCategoryDO;
@Service
@Transactional
public class CMSSubCategoryService {
	static Logger logger = Logger.getLogger(CMSSubCategoryService.class.getName());
	@Autowired
	private CMSSubCategoryDAO subCategoryDAO;

	@Transactional
	public boolean persist(CMSSubCategoryDO subCategoryDO) {
		return subCategoryDAO.persist(subCategoryDO);
	}

	@Transactional
	public List<CMSSubCategoryDO> retrieveActive() {
		return subCategoryDAO.retrieveActive();
	}

	@Transactional
	public List<CMSSubCategoryDO> retrieveById(Long Id) {
		return subCategoryDAO.retrieveById(Id);
	}

	@Transactional
	public List<CMSSubCategoryDO> retrieve() {
		return subCategoryDAO.retrieve();
	}

	@Transactional
	public boolean update(CMSSubCategoryDO subCategoryDO) {
		return subCategoryDAO.update(subCategoryDO);
	}

	@Transactional
	public List<CMSSubCategoryDO> retrieveByName(String name) {
		return subCategoryDAO.retrieveByName(name);
	}
}
