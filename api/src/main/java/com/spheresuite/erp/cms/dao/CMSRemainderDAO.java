package com.spheresuite.erp.cms.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.CMSRemainderDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class CMSRemainderDAO {
	static Logger logger = Logger.getLogger(CMSRemainderDAO.class.getName());
	@Autowired
	private GenericDAOImpl<CMSRemainderDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public boolean persist(CMSRemainderDO remainderDO) {
		try {
			
			genericObject.persist(remainderDO);
			return true;
			/*List<CMSRemainderDO> remainderDOList = null;
			remainderDOList = this.sessionFactory.getCurrentSession().getNamedQuery(DepartmentDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, remainderDO.getName())
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
					if(remainderDOList != null && remainderDOList.size() > 0){
						return false;
					}else{
						genericObject.persist(remainderDO);
						return true;
					}*/
		} catch (Exception eException) {
			eException.printStackTrace();
			return true;
		} finally {
		}
	}
	
	
	public List<CMSRemainderDO> retrieve() {
		List<CMSRemainderDO> remainderDOList = null;
		try {
			remainderDOList = genericObject.retrieve(CMSRemainderDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return remainderDOList;
	}
	
	/*public List<CMSRemainderDO> retrieveActive() {
		List<CMSRemainderDO> remainderDOList = null;
		try {
			return genericObject.retrieveActive(CommonConstants.ACTIVE, 
					CMSRemainderDO.FIND_BY_STATUS, CMSRemainderDO.class);
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return remainderDOList;
	}*/
	
	@SuppressWarnings("unchecked")
	public List<CMSRemainderDO> retrieveById(Long Id) {
		List<CMSRemainderDO> remainderDOList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(CMSRemainderDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, Id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return remainderDOList;
	}
	
	@SuppressWarnings("unchecked")
	public List<CMSRemainderDO> retrieveByTodayDate() {
		List<CMSRemainderDO> remainderDOList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(CMSRemainderDO.FIND_BY_TODAY_DATE)
					//.setParameter(CommonConstants.DATE, dt)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return remainderDOList;
	}
	
	
	/*@SuppressWarnings("unchecked")
	public List<CMSRemainderDO> retrieveByName(String name) {
		List<CMSRemainderDO> remainderDOList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(CMSRemainderDO.FIND_BY_NAME_ALL)
					.setParameter(CommonConstants.NAME, name)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return departmentList;
	}*/
	
	public boolean update(CMSRemainderDO departmentDO) {
		try {
			
			genericObject.merge(departmentDO);
			return true;
			/*List<CMSRemainderDO> remainderDOList = null;
			remainderDOList = this.sessionFactory.getCurrentSession().getNamedQuery(CMSRemainderDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, remainderDOList.getName())
					.setParameter(CommonConstants.ID, remainderDOList.getId())
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
			if(remainderDOList != null && remainderDOList.size() > 0){
				return false;
			}else{
				genericObject.merge(departmentDO);
				return true;
			}*/
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			return true;
		} finally {
		}
	}

}
