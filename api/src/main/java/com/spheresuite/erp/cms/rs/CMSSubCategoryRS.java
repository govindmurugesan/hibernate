package com.spheresuite.erp.cms.rs;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.cms.service.CMSSubCategoryService;
import com.spheresuite.erp.cms.web.util.CMSSubCategoryUtil;
import com.spheresuite.erp.domainobject.CMSSubCategoryDO;
import com.spheresuite.erp.service.UserService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/cms_subCategory")
public class CMSSubCategoryRS {

	String validation = null;
	static Logger logger = Logger.getLogger(CMSSubCategoryRS.class.getName());

	@Autowired
	private CMSSubCategoryService subCategoryService;
	
	@Autowired
	private UserService userService;
	
	/*@Autowired
	private RolesService rolesService;*/
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) { 
			
		try {
			if (WebManager.authenticateSession(request)) {
				CMSSubCategoryDO subCategoryDO = new CMSSubCategoryDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		subCategoryDO.setName(!inputJSON.get(CommonConstants.NAME).toString().isEmpty() ? inputJSON.get(CommonConstants.NAME).toString() : "");
			 		subCategoryDO.setUpdatedon(new Date());
			 		
			 		
			 		if(inputJSON.get(CommonConstants.COMMENT) != null){
			 			subCategoryDO.setCommets(inputJSON.get(CommonConstants.COMMENT).toString());
			 		}else{
			 			subCategoryDO.setCommets("");
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.CATEGORYID).toString() != null && !inputJSON.get(CommonConstants.CATEGORYID).toString().isEmpty()){
			 			subCategoryDO.setCategoryId(Long.parseLong((inputJSON.get(CommonConstants.CATEGORYID).toString())));
			 		}
			 		
			 		
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			subCategoryDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		subCategoryDO.setStatus('a');
			 	}
				//departmentService.persist(departmentDO);
			 	if(!subCategoryService.persist(subCategoryDO)){
					return CommonWebUtil.buildErrorResponse("SubCategory Already Added").toString();
				}
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New SubCategory Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retrieveActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<CMSSubCategoryDO> subCategorytList = subCategoryService.retrieveActive();
				respJSON = CMSSubCategoryUtil.getSubCategoryList(subCategorytList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<CMSSubCategoryDO> subCategoryList = subCategoryService.retrieve();
				respJSON = CMSSubCategoryUtil.getSubCategoryList(subCategoryList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				CMSSubCategoryDO subCategoryDO = new CMSSubCategoryDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<CMSSubCategoryDO> deptList = subCategoryService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		subCategoryDO = deptList.get(0);
			 		subCategoryDO.setName(inputJSON != null ? inputJSON.get(CommonConstants.NAME).toString() : "");
			 		subCategoryDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			subCategoryDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		
			 		/*if(inputJSON.get(CommonConstants.COMMENT).toString() != null && !inputJSON.get(CommonConstants.COMMENT).toString().isEmpty()){
			 			subCategoryDO.setCommets(inputJSON.get(CommonConstants.COMMENT).toString());
			 		}*/
			 		
			 		if(inputJSON.get(CommonConstants.COMMENT) != null){
			 			subCategoryDO.setCommets(inputJSON.get(CommonConstants.COMMENT).toString());
			 		}else{
			 			subCategoryDO.setCommets("");
			 		}
			 		
			 		subCategoryDO.setStatus((char) (inputJSON != null ? inputJSON.get(CommonConstants.STATUS).toString().charAt(0) : ""));
			 		//departmentService.update(departmentDO);
			 		if(!subCategoryService.update(subCategoryDO)){
						return CommonWebUtil.buildErrorResponse("SubCategory Already Added").toString();
					}
			 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "SubCategory Updated");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
}
