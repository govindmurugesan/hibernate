package com.spheresuite.erp.cms.rs;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.cms.service.CMSRemainderDocService;
import com.spheresuite.erp.cms.service.CMSRemainderService;
import com.spheresuite.erp.cms.web.util.CMSRemainderUtil;
import com.spheresuite.erp.domainobject.CMSRemainderDO;
import com.spheresuite.erp.domainobject.CMSRemainderDocDO;
import com.spheresuite.erp.service.UserService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/cms_remainder")
public class CMSRemaindertRS {

	String validation = null;
	static Logger logger = Logger.getLogger(CMSRemaindertRS.class.getName());

	@Autowired
	private CMSRemainderService remainderService;
	
	@Autowired
	private UserService userService;
	
	/*@Autowired
	private RolesService rolesService;*/
	
	@Autowired
	private CMSRemainderDocService cmsRemainderDocService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) { 
		CMSRemainderDO remainderDO = new CMSRemainderDO();
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		
			 		if(inputJSON.get(CommonConstants.CATEGORYID) != null && !inputJSON.get(CommonConstants.CATEGORYID).toString().isEmpty()){
			 			remainderDO.setCategoryId(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.CATEGORYID).toString() : ""));
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.DEPT) != null && !inputJSON.get(CommonConstants.DEPT).toString().isEmpty()){
			 			remainderDO.setDepartmentId(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.DEPT).toString() : ""));
			 		}
			 		
			 		/*if(inputJSON.get(CommonConstants.SUBCATEGORY) != null && !inputJSON.get(CommonConstants.SUBCATEGORY).toString().isEmpty()){
			 			remainderDO.setSubCategoryId(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.SUBCATEGORY).toString() : ""));
			 		}*/
			 		
			 		if(inputJSON.get(CommonConstants.FREQUENCY) != null && !inputJSON.get(CommonConstants.FREQUENCY).toString().isEmpty()){
			 			remainderDO.setFrequencyId(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.FREQUENCY).toString() : ""));
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.ISSUEDBY) != null && !inputJSON.get(CommonConstants.ISSUEDBY).toString().isEmpty()){
			 			remainderDO.setIssuedBy(inputJSON != null ? inputJSON.get(CommonConstants.ISSUEDBY).toString() : "");
			 		}
			 		
			 		
			 		
			 		if(inputJSON.get(CommonConstants.NO_ALERT) != null && !inputJSON.get(CommonConstants.NO_ALERT).toString().isEmpty()){
			 			remainderDO.setNoOfAlerts(inputJSON != null ? inputJSON.get(CommonConstants.NO_ALERT).toString() : "");
			 		}
			 		
			 	/*	if(inputJSON.get(CommonConstants.NEXTRENEWAL) != null && !inputJSON.get(CommonConstants.NEXTRENEWAL).toString().isEmpty()){
			 			remainderDO.setNextRenewal(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.NEXTRENEWAL).toString()));
			 		}*/
			 		
			 		if(inputJSON.get(CommonConstants.LEADTIME) != null && !inputJSON.get(CommonConstants.LEADTIME).toString().isEmpty()){
			 			remainderDO.setLeadTime(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.LEADTIME).toString() : ""));
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.RECURRENCE) != null && !inputJSON.get(CommonConstants.RECURRENCE).toString().isEmpty()){
			 			remainderDO.setRecurrence(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.RECURRENCE).toString() : ""));
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.DATE) != null && !inputJSON.get(CommonConstants.DATE).toString().isEmpty()){
			 			remainderDO.setValidUpto(!inputJSON.get(CommonConstants.DATE).toString().isEmpty() ? CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.DATE).toString()) : null);
			 		}
			 		
			 		//remainderDO.setValidUptoTime(!inputJSON.get(CommonConstants.TIME).toString().isEmpty() ? inputJSON.get(CommonConstants.TIME).toString() : "");
			 		
			 		if(inputJSON.get(CommonConstants.EMAIL) != null && !inputJSON.get(CommonConstants.EMAIL).toString().isEmpty()){
			 			remainderDO.setEmail(!inputJSON.get(CommonConstants.EMAIL).toString().isEmpty() ? inputJSON.get(CommonConstants.EMAIL).toString() : "");
			 		}
			 		
			 		
			 		/*if(inputJSON.get(CommonConstants.ISSUEDBY) != null && !inputJSON.get(CommonConstants.ISSUEDBY).toString().isEmpty()){
			 			remainderDO.set(inputJSON != null ? inputJSON.get(CommonConstants.ISSUEDBY).toString() : null);
			 		}*/
			 		
			 		//empCompensationDO.setStartdate(!inputJSON.get(CommonConstants.STARTDATE).toString().isEmpty() ? CommonUtil.convertStringToSqlDateMonth(inputJSON.get(CommonConstants.STARTDATE).toString()) : null);
			 		
			 		
			 		if(inputJSON.get(CommonConstants.DESCRIPTION) != null && !inputJSON.get(CommonConstants.DESCRIPTION).toString().isEmpty()){
			 			remainderDO.setDescription(inputJSON.get(CommonConstants.DESCRIPTION).toString());
			 		}
			 		
			 		//remainderDO.set(!inputJSON.get(CommonConstants.NAME).toString().isEmpty() ? inputJSON.get(CommonConstants.NAME).toString() : "");
			 		if(inputJSON.get(CommonConstants.COMMENT)  != null && !inputJSON.get(CommonConstants.COMMENT).toString().isEmpty()){
			 			remainderDO.setComments(inputJSON.get(CommonConstants.COMMENT).toString());
			 		}
			 		remainderDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY)  != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			remainderDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 	}
			 	if(!remainderService.persist(remainderDO)){
					return CommonWebUtil.buildErrorResponse("Remainder Already Added").toString();
				}
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Remainder Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponseId(remainderDO.getId().toString()).toString();
	}
	
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<CMSRemainderDO> remainderList = remainderService.retrieve();
				respJSON = CMSRemainderUtil.getremainderList(remainderList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retriveById/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveById(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<CMSRemainderDO> remainderList = remainderService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = CMSRemainderUtil.getremainderList(remainderList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	
	
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				CMSRemainderDO remainderDO = new CMSRemainderDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<CMSRemainderDO> remainderList = remainderService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		remainderDO = remainderList.get(0);
			 		
			 		
			 		if(inputJSON.get(CommonConstants.CATEGORYID) != null && !inputJSON.get(CommonConstants.CATEGORYID).toString().isEmpty()){
			 			remainderDO.setCategoryId(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.CATEGORYID).toString() : ""));
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.DEPT) != null && !inputJSON.get(CommonConstants.DEPT).toString().isEmpty()){
			 			remainderDO.setDepartmentId(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.DEPT).toString() : ""));
			 		}
			 		
			 		/*if(inputJSON.get(CommonConstants.SUBCATEGORY) != null && !inputJSON.get(CommonConstants.SUBCATEGORY).toString().isEmpty()){
			 			remainderDO.setSubCategoryId(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.SUBCATEGORY).toString() : ""));
			 		}*/
			 		
			 		if(inputJSON.get(CommonConstants.FREQUENCY) != null && !inputJSON.get(CommonConstants.FREQUENCY).toString().isEmpty()){
			 			remainderDO.setFrequencyId(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.FREQUENCY).toString() : ""));
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.ISSUEDBY) != null && !inputJSON.get(CommonConstants.ISSUEDBY).toString().isEmpty()){
			 			remainderDO.setIssuedBy(inputJSON != null ? inputJSON.get(CommonConstants.ISSUEDBY).toString() : "");
			 		}
			 		
			 		
			 		
			 		if(inputJSON.get(CommonConstants.NO_ALERT) != null && !inputJSON.get(CommonConstants.NO_ALERT).toString().isEmpty()){
			 			remainderDO.setNoOfAlerts(inputJSON != null ? inputJSON.get(CommonConstants.NO_ALERT).toString() : "");
			 		}
			 		
			 		/*if(inputJSON.get(CommonConstants.NEXTRENEWAL) != null && !inputJSON.get(CommonConstants.NEXTRENEWAL).toString().isEmpty()){
			 			remainderDO.setNextRenewal(!inputJSON.get(CommonConstants.NEXTRENEWAL).toString().isEmpty() ? CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.NEXTRENEWAL).toString()) : null);
			 		}*/
			 		
			 		if(inputJSON.get(CommonConstants.LEADTIME) != null && !inputJSON.get(CommonConstants.LEADTIME).toString().isEmpty()){
			 			remainderDO.setLeadTime(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.LEADTIME).toString() : ""));
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.RECURRENCE) != null && !inputJSON.get(CommonConstants.RECURRENCE).toString().isEmpty()){
			 			remainderDO.setRecurrence(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.RECURRENCE).toString() : ""));
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.DATE) != null && !inputJSON.get(CommonConstants.DATE).toString().isEmpty()){
			 			remainderDO.setValidUpto(!inputJSON.get(CommonConstants.DATE).toString().isEmpty() ? CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.DATE).toString()) : null);
			 		}
			 		//remainderDO.setValidUptoTime(!inputJSON.get(CommonConstants.TIME).toString().isEmpty() ? inputJSON.get(CommonConstants.TIME).toString() : "");
			 		
			 		if(inputJSON.get(CommonConstants.EMAIL) != null && !inputJSON.get(CommonConstants.EMAIL).toString().isEmpty()){
			 			remainderDO.setEmail(!inputJSON.get(CommonConstants.EMAIL).toString().isEmpty() ? inputJSON.get(CommonConstants.EMAIL).toString() : "");
			 		}
			 		
			 		remainderDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			remainderDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.DESCRIPTION).toString() != null && !inputJSON.get(CommonConstants.DESCRIPTION).toString().isEmpty()){
			 			remainderDO.setDescription(inputJSON.get(CommonConstants.DESCRIPTION).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.COMMENT).toString() != null && !inputJSON.get(CommonConstants.COMMENT).toString().isEmpty()){
			 			remainderDO.setComments(inputJSON.get(CommonConstants.COMMENT).toString());
			 		}
			 		
			 		if(!remainderService.update(remainderDO)){
						return CommonWebUtil.buildErrorResponse("Remainder Already Added").toString();
					}
			 		cmsRemainderDocService.delete(remainderDO.getId());
			 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Remainder Updated");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/updatepic", method = RequestMethod.POST)
	public @ResponseBody String updatepic(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				CMSRemainderDocDO cmsRemainderDocDO = new CMSRemainderDocDO();
				/*JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("data").toString());*/
			 	if (request.getParameter("file") != null && request.getParameter("id") != null){
			 		cmsRemainderDocDO.setCmsremainderId(Long.parseLong(request.getParameter("id")));
			 		cmsRemainderDocDO.setPhoto(request.getParameter("file"));
			 		cmsRemainderDocDO.setFileName(request.getParameter("name"));
			 		cmsRemainderDocService.persist(cmsRemainderDocDO);
 			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
}
