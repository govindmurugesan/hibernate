package com.spheresuite.erp.cms.web.util;

import java.util.List;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.cms.service.CMSCategoryService;
import com.spheresuite.erp.domainobject.CMSCategoryDO;
import com.spheresuite.erp.domainobject.CMSSubCategoryDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
@Component
public class CMSSubCategoryUtil {
	
	private CMSSubCategoryUtil() {}

	@Autowired
	private EmployeeService temployeeService;
	
	private static EmployeeService employeeService;
	

	@Autowired
	private CMSCategoryService tcmsCategoryService;
	
	private static CMSCategoryService cmsCategoryService;
	
	@PostConstruct
	public void init() {
		employeeService = temployeeService;
		cmsCategoryService = tcmsCategoryService;
	}
	
	
	public static JSONObject getSubCategoryList(List<CMSSubCategoryDO> subCategoryList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (CMSSubCategoryDO subCategory : subCategoryList) {
				resultJSONArray.put(getDepartmentDetailObject(subCategory));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getDepartmentDetailObject(CMSSubCategoryDO subCategory)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(subCategory.getId()));
		result.put(CommonConstants.NAME, String.valueOf(subCategory.getName()));
		result.put(CommonConstants.STATUS, String.valueOf(subCategory.getStatus()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(subCategory.getUpdatedon())));
		
		
		if(subCategory.getCommets() != null){
			result.put(CommonConstants.COMMENT, String.valueOf(subCategory.getCommets()));
		}else{
			result.put(CommonConstants.COMMENT, "");
		}
		
		
		
		List<CMSCategoryDO> categoryList = cmsCategoryService.retrieveById(Long.parseLong(subCategory.getCategoryId().toString()));
		if(subCategory.getUpdatedBy() != null){
			String empName = CommonUtil.getEmpObject(subCategory.getUpdatedBy());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		
		/*if(subCategory.getUpdatedBy() != null){
			List<EmployeeDO> empList = employeeService.retriveByEmpId(Long.parseLong(subCategory.getUpdatedBy()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}*/
		return result;
	}
}
