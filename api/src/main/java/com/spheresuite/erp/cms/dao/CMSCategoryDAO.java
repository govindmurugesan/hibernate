package com.spheresuite.erp.cms.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.CMSCategoryDO;
import com.spheresuite.erp.domainobject.CategoryDO;
import com.spheresuite.erp.domainobject.OpportunityTypeDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class CMSCategoryDAO {
	static Logger logger = Logger.getLogger(CMSCategoryDAO.class.getName());
	@Autowired
	private GenericDAOImpl<CMSCategoryDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(CMSCategoryDO categoryDO) {
		List<CMSCategoryDO> projectTypeList = null;
		try {
			
			projectTypeList = this.sessionFactory.getCurrentSession().getNamedQuery(CMSCategoryDO.FIND_BY_NAME)
			.setParameter(CommonConstants.NAME, categoryDO.getName())
			.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
			.list();
			if(projectTypeList != null && projectTypeList.size() > 0){
				return false;
			}else{
				genericObject.persist(categoryDO);
				return true;
			}
			
		} catch (Exception eException) {
			return true;
		} finally {
		}
	}
	
	
	public List<CMSCategoryDO> retrieve() {
		List<CMSCategoryDO> categoryList = null;
		
		try {
			categoryList = genericObject.retrieve(CMSCategoryDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return categoryList;
	}
	
	public List<CMSCategoryDO> retrieveActive() {
		List<CMSCategoryDO> categoryList = null;
		try {
			return genericObject.retrieveActive(CommonConstants.ACTIVE,"category.findByStatus", CMSCategoryDO.class);
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return categoryList;
	}
	
	@SuppressWarnings("unchecked")
	public List<CMSCategoryDO> retrieveById(Long id) {
		List<CMSCategoryDO> categoryList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(CMSCategoryDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return categoryList;
	}
	
	@SuppressWarnings("unchecked")
	public boolean update(CMSCategoryDO categoryDO) {
		try {
			List<CMSCategoryDO> categoryList = null;
			categoryList = this.sessionFactory.getCurrentSession().getNamedQuery(CMSCategoryDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, categoryDO.getName())
					.setParameter(CommonConstants.ID, categoryDO.getId())
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
			if(categoryList != null && categoryList.size() > 0){
				return false;
			}else{
				genericObject.merge(categoryDO);
				return true;
			}
			
		} catch (Exception eException) {
			return true;
		} finally {
		}
	}

}
