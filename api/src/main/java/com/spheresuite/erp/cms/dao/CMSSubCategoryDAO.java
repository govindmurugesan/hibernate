package com.spheresuite.erp.cms.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.CMSSubCategoryDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class CMSSubCategoryDAO {
	static Logger logger = Logger.getLogger(CMSSubCategoryDAO.class.getName());
	@Autowired
	private GenericDAOImpl<CMSSubCategoryDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(CMSSubCategoryDO subCategoryDO) {
		try {
			List<CMSSubCategoryDO> subCategoryDOList = null;
			subCategoryDOList = this.sessionFactory.getCurrentSession().getNamedQuery(CMSSubCategoryDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, subCategoryDO.getName())
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
					if(subCategoryDOList != null && subCategoryDOList.size() > 0){
						return false;
					}else{
						genericObject.persist(subCategoryDO);
						return true;
					}
		} catch (Exception eException) {
			return true;
		} finally {
		}
	}
	
	
	public List<CMSSubCategoryDO> retrieve() {
		List<CMSSubCategoryDO> subCategoryDOList = null;
		try {
			subCategoryDOList = genericObject.retrieve(CMSSubCategoryDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return subCategoryDOList;
	}
	
	public List<CMSSubCategoryDO> retrieveActive() {
		List<CMSSubCategoryDO> subCategoryDOList = null;
		try {
			return genericObject.retrieveActive(CommonConstants.ACTIVE, 
					CMSSubCategoryDO.FIND_BY_STATUS, CMSSubCategoryDO.class);
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return subCategoryDOList;
	}
	
	@SuppressWarnings("unchecked")
	public List<CMSSubCategoryDO> retrieveById(Long Id) {
		List<CMSSubCategoryDO> subCategoryDOList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(CMSSubCategoryDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, Id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return subCategoryDOList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<CMSSubCategoryDO> retrieveByName(String name) {
		List<CMSSubCategoryDO> subCategoryDOList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(CMSSubCategoryDO.FIND_BY_NAME_ALL)
					.setParameter(CommonConstants.NAME, name)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return subCategoryDOList;
	}
	
	
	@SuppressWarnings("unchecked")
	public boolean update(CMSSubCategoryDO departmentDO) {
		try {
			List<CMSSubCategoryDO> subCategoryDOList = null;
			subCategoryDOList = this.sessionFactory.getCurrentSession().getNamedQuery(CMSSubCategoryDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, departmentDO.getName())
					.setParameter(CommonConstants.ID, departmentDO.getId())
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
			if(subCategoryDOList != null && subCategoryDOList.size() > 0){
				return false;
			}else{
				genericObject.merge(departmentDO);
				return true;
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			return true;
		} finally {
		}
	}

}
