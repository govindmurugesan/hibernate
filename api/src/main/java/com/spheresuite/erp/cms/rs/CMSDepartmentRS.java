package com.spheresuite.erp.cms.rs;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.cms.service.CMSDepartmentService;
import com.spheresuite.erp.cms.web.util.CMSDepartmentUtil;
import com.spheresuite.erp.domainobject.CMSDepartmentDO;
import com.spheresuite.erp.service.UserService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/cms_department")
public class CMSDepartmentRS {

	String validation = null;
	static Logger logger = Logger.getLogger(CMSDepartmentRS.class.getName());

	@Autowired
	private CMSDepartmentService departmentService;
	
	@Autowired
	private UserService userService;
	
	/*@Autowired
	private RolesService rolesService;*/
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) { 
			
		try {
			if (WebManager.authenticateSession(request)) {
				CMSDepartmentDO departmentDO = new CMSDepartmentDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		departmentDO.setName(!inputJSON.get(CommonConstants.NAME).toString().isEmpty() ? inputJSON.get(CommonConstants.NAME).toString() : "");
			 		departmentDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.COMMENT) != null){
			 			departmentDO.setComments(inputJSON.get(CommonConstants.COMMENT).toString());
			 		}else{
			 			departmentDO.setComments("");
			 		}
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			departmentDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		departmentDO.setStatus('a');
			 	}
				//departmentService.persist(departmentDO);
			 	if(!departmentService.persist(departmentDO)){
					return CommonWebUtil.buildErrorResponse("Department Already Added").toString();
				}
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Department Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retrieveActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<CMSDepartmentDO> departmentList = departmentService.retrieveActive();
				respJSON = CMSDepartmentUtil.getDepartmentList(departmentList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<CMSDepartmentDO> departmentList = departmentService.retrieve();
				respJSON = CMSDepartmentUtil.getDepartmentList(departmentList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				CMSDepartmentDO departmentDO = new CMSDepartmentDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<CMSDepartmentDO> deptList = departmentService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		departmentDO = deptList.get(0);
			 		departmentDO.setName(inputJSON != null ? inputJSON.get(CommonConstants.NAME).toString() : "");
			 		departmentDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			departmentDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.COMMENT) != null ){
			 			departmentDO.setComments(inputJSON.get(CommonConstants.COMMENT).toString());
			 		}else{
			 			departmentDO.setComments("");
			 		}
			 		
			 		departmentDO.setStatus((char) (inputJSON != null ? inputJSON.get(CommonConstants.STATUS).toString().charAt(0) : ""));
			 		//departmentService.update(departmentDO);
			 		if(!departmentService.update(departmentDO)){
						return CommonWebUtil.buildErrorResponse("Department Already Added").toString();
					}
			 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Department Updated");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
}
