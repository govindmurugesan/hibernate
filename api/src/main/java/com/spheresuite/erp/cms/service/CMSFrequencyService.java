package com.spheresuite.erp.cms.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.cms.dao.CMSFrequencyDAO;
import com.spheresuite.erp.domainobject.CMSDepartmentDO;
import com.spheresuite.erp.domainobject.CMSFrequencyDO;
@Service
@Transactional
public class CMSFrequencyService {
	static Logger logger = Logger.getLogger(CMSFrequencyService.class.getName());
	@Autowired
	private CMSFrequencyDAO frequencyDAO;

	@Transactional
	public boolean persist(CMSFrequencyDO frequencyDO) {
		return frequencyDAO.persist(frequencyDO);
	}

	@Transactional
	public List<CMSFrequencyDO> retrieveActive() {
		return frequencyDAO.retrieveActive();
	}

	@Transactional
	public List<CMSFrequencyDO> retrieveById(Long Id) {
		return frequencyDAO.retrieveById(Id);
	}

	@Transactional
	public List<CMSFrequencyDO> retrieve() {
		return frequencyDAO.retrieve();
	}

	@Transactional
	public boolean update(CMSFrequencyDO departmentDO) {
		return frequencyDAO.update(departmentDO);
	}

	
	
	@Transactional
	public List<CMSFrequencyDO> retrieveByName(String name) {
		return frequencyDAO.retrieveByName(name);
	}
}
