package com.spheresuite.erp.cms.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.cms.dao.CMSRemainderDAO;
import com.spheresuite.erp.domainobject.CMSRemainderDO;
@Service
@Transactional
public class CMSRemainderService {
	static Logger logger = Logger.getLogger(CMSRemainderService.class.getName());
	@Autowired
	private CMSRemainderDAO cmsRaminderDAO;

	@Transactional
	public boolean persist(CMSRemainderDO remaindertDO) {
		return cmsRaminderDAO.persist(remaindertDO);
	}

	/*@Transactional
	public List<CMSRemainderDO> retrieveActive() {
		return cmsRaminderDAO.retrieveActive();
	}*/

	@Transactional
	public List<CMSRemainderDO> retrieveById(Long Id) {
		return cmsRaminderDAO.retrieveById(Id);
	}
	
	@Transactional
	public List<CMSRemainderDO> retrieveByTodayDate() {
		return cmsRaminderDAO.retrieveByTodayDate();
	}

	@Transactional
	public List<CMSRemainderDO> retrieve() {
		return cmsRaminderDAO.retrieve();
	}

	@Transactional
	public boolean update(CMSRemainderDO departmentDO) {
		return cmsRaminderDAO.update(departmentDO);
	}

	/*@Transactional
	public List<CMSRemainderDO> retrieveByName(String name) {
		return cmsRaminderDAO.retrieveByName(name);
	}*/
}
