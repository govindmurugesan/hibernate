package com.spheresuite.erp.cms.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.CMSDepartmentDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class CMSDepartmentDAO {
	static Logger logger = Logger.getLogger(CMSDepartmentDAO.class.getName());
	@Autowired
	private GenericDAOImpl<CMSDepartmentDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(CMSDepartmentDO departmentDO) {
		try {
			List<CMSDepartmentDO> departmentDOList = null;
			departmentDOList = this.sessionFactory.getCurrentSession().getNamedQuery(CMSDepartmentDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, departmentDO.getName())
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
					if(departmentDOList != null && departmentDOList.size() > 0){
						return false;
					}else{
						genericObject.persist(departmentDO);
						return true;
					}
		} catch (Exception eException) {
			return true;
		} finally {
		}
	}
	
	
	public List<CMSDepartmentDO> retrieve() {
		List<CMSDepartmentDO> departmentList = null;
		try {
			departmentList = genericObject.retrieve(CMSDepartmentDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return departmentList;
	}
	
	public List<CMSDepartmentDO> retrieveActive() {
		List<CMSDepartmentDO> departmentList = null;
		try {
			return genericObject.retrieveActive(CommonConstants.ACTIVE, 
					CMSDepartmentDO.FIND_BY_STATUS, CMSDepartmentDO.class);
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return departmentList;
	}
	
	@SuppressWarnings("unchecked")
	public List<CMSDepartmentDO> retrieveById(Long Id) {
		List<CMSDepartmentDO> departmentList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(CMSDepartmentDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, Id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return departmentList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<CMSDepartmentDO> retrieveByName(String name) {
		List<CMSDepartmentDO> departmentList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(CMSDepartmentDO.FIND_BY_NAME_ALL)
					.setParameter(CommonConstants.NAME, name)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return departmentList;
	}
	
	/*@SuppressWarnings("unchecked")
	public List<Long> retrieveisManager(Long isManager) {
		List<Long> departmentList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(DepartmentDO.FIND_IS_MANAGER)
					.setParameter("isManager", isManager)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list(); 
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return departmentList;
	}*/
	
	@SuppressWarnings("unchecked")
	public boolean update(CMSDepartmentDO departmentDO) {
		try {
			List<CMSDepartmentDO> departmentDOList = null;
			departmentDOList = this.sessionFactory.getCurrentSession().getNamedQuery(CMSDepartmentDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, departmentDO.getName())
					.setParameter(CommonConstants.ID, departmentDO.getId())
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
			if(departmentDOList != null && departmentDOList.size() > 0){
				return false;
			}else{
				genericObject.merge(departmentDO);
				return true;
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			return true;
		} finally {
		}
	}

}
