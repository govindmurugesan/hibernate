package com.spheresuite.erp.cms.web.util;

import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.cms.service.CMSCategoryService;
import com.spheresuite.erp.cms.service.CMSDepartmentService;
import com.spheresuite.erp.cms.service.CMSFrequencyService;
import com.spheresuite.erp.cms.service.CMSRemainderDocService;
import com.spheresuite.erp.cms.service.CMSSubCategoryService;
import com.spheresuite.erp.domainobject.CMSCategoryDO;
import com.spheresuite.erp.domainobject.CMSDepartmentDO;
import com.spheresuite.erp.domainobject.CMSFrequencyDO;
import com.spheresuite.erp.domainobject.CMSRemainderDO;
import com.spheresuite.erp.domainobject.CMSRemainderDocDO;
//import com.spheresuite.erp.domainobject.CMSSubCategoryDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
@Component
public class CMSRemainderUtil {
	
	private CMSRemainderUtil() {}

	@Autowired
	private EmployeeService temployeeService;
	private static EmployeeService employeeService;
	
	@Autowired
	private CMSCategoryService tcmsCategoryService;
	private static CMSCategoryService cmsCategoryService;
	
	@Autowired
	private CMSDepartmentService tcmsdepartmentService;
	private static CMSDepartmentService cmsdepartmentService;
	
	@Autowired
	private CMSSubCategoryService tcmsSubCategoryService;
	//private static CMSSubCategoryService cmsSubCategoryService;
	
	@Autowired
	private CMSFrequencyService  tcmsFrequencyService;
	private static CMSFrequencyService cmsFrequencyService;
	
	/*@Autowired
	private CMSRemainderService  tcmsRemainderService;
	private static CMSRemainderService cmsRemainderService;*/
	
	@Autowired
	private CMSRemainderDocService tcmsRemainderDocService;
	
	private static CMSRemainderDocService cmsRemainderDocService;
	
	
	
	@PostConstruct
	public void init() {
		employeeService = temployeeService;
		cmsCategoryService = tcmsCategoryService;
		cmsdepartmentService = tcmsdepartmentService;
	//	cmsSubCategoryService = tcmsSubCategoryService ;
		cmsFrequencyService = tcmsFrequencyService;
		cmsRemainderDocService = tcmsRemainderDocService;
	}
	
	public static JSONObject getremainderList(List<CMSRemainderDO> remianderList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (CMSRemainderDO remiander : remianderList) {
				resultJSONArray.put(getDepartmentDetailObject(remiander));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getDepartmentDetailObject(CMSRemainderDO remiander)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(remiander.getId()));
		
		if(remiander.getDepartmentId() != null){
			List<CMSDepartmentDO> cmsdeptList = cmsdepartmentService.retrieveById(Long.parseLong(remiander.getDepartmentId().toString()));
			if(cmsdeptList != null && cmsdeptList.size() > 0){
				if(cmsdeptList.get(0).getName() != null) result.put(CommonConstants.DEPT_NAME, String.valueOf(cmsdeptList.get(0).getName()));
				result.put(CommonConstants.DEPT, String.valueOf(cmsdeptList.get(0).getId()));
			}else{
				result.put(CommonConstants.DEPT, ""); 
				result.put(CommonConstants.DEPT_NAME, ""); 
			}
		}else{
			result.put(CommonConstants.DEPT, ""); 
			result.put(CommonConstants.DEPT_NAME, ""); 
		}
		
		
		/*if(remiander.getSubCategoryId() != null){
			List<CMSSubCategoryDO> cmssubCategoryList = cmsSubCategoryService.retrieveById(Long.parseLong(remiander.getSubCategoryId().toString()));
			if(cmssubCategoryList != null && cmssubCategoryList.size() > 0){
				if(cmssubCategoryList.get(0).getName() != null) result.put(CommonConstants.SUBCATEGORY_NAME, String.valueOf(cmssubCategoryList.get(0).getName()));
				 result.put(CommonConstants.SUBCATEGORY, String.valueOf(cmssubCategoryList.get(0).getId()));
			}else{
				result.put(CommonConstants.SUBCATEGORY, ""); 
				result.put(CommonConstants.SUBCATEGORY_NAME, ""); 
			}
		}else{
			result.put(CommonConstants.SUBCATEGORY, ""); 
			result.put(CommonConstants.SUBCATEGORY_NAME, ""); 
		}*/
		
		if(remiander.getFrequencyId() != null){
			List<CMSFrequencyDO> cmsfrqList = cmsFrequencyService.retrieveById(Long.parseLong(remiander.getFrequencyId().toString()));
			if(cmsfrqList != null && cmsfrqList.size() > 0){
				if(cmsfrqList.get(0).getName() != null) result.put(CommonConstants.FREQUENCY_NAME, String.valueOf(cmsfrqList.get(0).getName()));
				result.put(CommonConstants.FREQUENCY, String.valueOf(cmsfrqList.get(0).getId()));
			}else{
				result.put(CommonConstants.FREQUENCY, ""); 
				result.put(CommonConstants.FREQUENCY_NAME, ""); 
			}
		}else{
			result.put(CommonConstants.FREQUENCY, ""); 
			result.put(CommonConstants.FREQUENCY_NAME, ""); 
		}
		
		if(remiander.getIssuedBy() != null){
			result.put(CommonConstants.ISSUEDBY, String.valueOf(remiander.getIssuedBy()));
		}else{
			result.put(CommonConstants.ISSUEDBY, "");
		}
		
		
		//result.put(CommonConstants.NEXTRENEWAL,String.valueOf(remiander.getNextRenewal() != null ? CommonUtil.convertDateToYearWithOutTime(remiander.getNextRenewal()) : ""));
		
		
		if(remiander.getNoOfAlerts() != null){
			result.put(CommonConstants.NO_ALERT, String.valueOf(remiander.getNoOfAlerts()));
		}else{
			result.put(CommonConstants.NO_ALERT, "");
		}
		
		if(remiander.getRecurrence()!= null){
			result.put(CommonConstants.RECURRENCE, String.valueOf(remiander.getRecurrence()));
		}else{
			result.put(CommonConstants.RECURRENCE, "");
		}
		
		
		if(remiander.getLeadTime()!= null){
			result.put(CommonConstants.LEADTIME, String.valueOf(remiander.getLeadTime()));
		}else{
			result.put(CommonConstants.LEADTIME, "");
		}
		
		if(remiander.getEmail()!= null){
			result.put(CommonConstants.EMAIL, String.valueOf(remiander.getEmail()));
		}else{
			result.put(CommonConstants.EMAIL, "");
		}
		/*if(remiander.getValidUpto() != null){
			List<CMSRemainderDO> cmsRemainderList = cmsRemainderService.retrieveByTodayDate();
			if(cmsRemainderList!= null && cmsRemainderList.size() > 0){
				result.put(CommonConstants.DATE, String.valueOf(remiander.getValidUpto() != null ? CommonUtil.convertDateToYearWithOutTime(remiander.getValidUpto()) : ""));
				result.put(CommonConstants.EXPIRED, "Y");
			} else {
				result.put(CommonConstants.DATE, String.valueOf(remiander.getValidUpto() != null ? CommonUtil.convertDateToYearWithOutTime(remiander.getValidUpto()) : ""));
				result.put(CommonConstants.EXPIRED, "N");
			}
		} else {
			result.put(CommonConstants.DATE, String.valueOf(remiander.getValidUpto() != null ? CommonUtil.convertDateToYearWithOutTime(remiander.getValidUpto()) : ""));
			result.put(CommonConstants.EXPIRED, "N");
		}*/
		if(remiander.getValidUpto() != null){
			boolean flag = CommonUtil.compareDates(CommonUtil.convertDateToYearWithOutTime(remiander.getValidUpto()), CommonUtil.convertDateToYearWithOutTime(new Date()));
			if(flag){
				result.put(CommonConstants.DATE, String.valueOf(remiander.getValidUpto() != null ? CommonUtil.convertDateToYearWithOutTime(remiander.getValidUpto()) : ""));
				result.put(CommonConstants.EXPIRED, "Y");
			} else {
				result.put(CommonConstants.DATE, String.valueOf(remiander.getValidUpto() != null ? CommonUtil.convertDateToYearWithOutTime(remiander.getValidUpto()) : ""));
				result.put(CommonConstants.EXPIRED, "N");
			}
		} else {
			result.put(CommonConstants.DATE, String.valueOf(remiander.getValidUpto() != null ? CommonUtil.convertDateToYearWithOutTime(remiander.getValidUpto()) : ""));
			result.put(CommonConstants.EXPIRED, "N");
		}
		
		
	//	result.put(CommonConstants.TIME, String.valueOf(remiander.getValidUptoTime() != null ? remiander.getValidUptoTime() : ""));
		
		
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(remiander.getUpdatedon())));
		if(remiander.getComments() != null){
			result.put(CommonConstants.COMMENT, String.valueOf(remiander.getComments()));
		}else{
			result.put(CommonConstants.COMMENT, "");
		}
		
		if(remiander.getDescription() != null){
			result.put(CommonConstants.DESCRIPTION, String.valueOf(remiander.getDescription()));
		}else{
			result.put(CommonConstants.DESCRIPTION, "");
		}
		
		if(remiander.getCategoryId() != null){
			List<CMSCategoryDO> cmsCategoryList = cmsCategoryService.retrieveById(Long.parseLong(remiander.getCategoryId().toString()));
			if(cmsCategoryList != null && cmsCategoryList.size() > 0){
				if(cmsCategoryList.get(0).getName() != null) result.put(CommonConstants.CATEGORY_NAME, String.valueOf(cmsCategoryList.get(0).getName()));
				result.put(CommonConstants.CATEGORYID, String.valueOf(cmsCategoryList.get(0).getId()));
			}else{
				result.put(CommonConstants.CATEGORYID, ""); 
				result.put(CommonConstants.CATEGORY_NAME, ""); 
			}
		}else{
			result.put(CommonConstants.CATEGORYID, ""); 
			result.put(CommonConstants.CATEGORY_NAME, "");  
		}
		if(remiander.getUpdatedBy() != null){
			String empName = CommonUtil.getEmpObject(remiander.getUpdatedBy());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		
		/*if(remiander.getUpdatedBy() != null){
			List<EmployeeDO> empList = employeeService.retriveByEmpId(Long.parseLong(remiander.getUpdatedBy()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}*/
		
		JSONArray resultJSONArray = new JSONArray();
		
		List<CMSRemainderDocDO> cmsRemainderDocDOList = cmsRemainderDocService.retrieveByCMSId(remiander.getId());
		if(cmsRemainderDocDOList != null && cmsRemainderDocDOList.size()  > 0){
			for (CMSRemainderDocDO doc : cmsRemainderDocDOList) {
				resultJSONArray.put(getDocDetailObject(doc));
			}
		}
		result.put(CommonConstants.FILE, resultJSONArray);
		
		return result;
	}
	
	public static JSONObject getDocDetailObject(CMSRemainderDocDO doc)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.FILE, String.valueOf(doc.getPhoto()));
		if(doc.getFileName() != null){
			result.put(CommonConstants.NAME, String.valueOf(doc.getFileName()));
		}else{
			result.put(CommonConstants.NAME, "");	
		}
		
		return result;
	}
}
