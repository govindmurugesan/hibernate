package com.spheresuite.erp.cms.web.util;

import java.util.List;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.CMSCategoryDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;

@Component
public class CMSCategoryUtil {
	
	private CMSCategoryUtil() {}
	
	@Autowired
	private EmployeeService temployeeService;
	
	private static EmployeeService employeeService;
	
	@PostConstruct
	public void init() {
		employeeService = temployeeService;
	}
	
	public static JSONObject getCategoryList(List<CMSCategoryDO> categoryList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (CMSCategoryDO category : categoryList) {
				resultJSONArray.put(getCategoryDetailObject(category));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getCategoryDetailObject(CMSCategoryDO category)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(category.getId()));
		result.put(CommonConstants.NAME, String.valueOf(category.getName()));
		if(category.getDescripetion() != null){
			result.put(CommonConstants.DESC, String.valueOf(category.getDescripetion()));
		}else{
			result.put(CommonConstants.DESC, "");
		}
		
		result.put(CommonConstants.STATUS, String.valueOf(category.getStatus()));
		
		if(category.getUpdatedBy() != null){
			String empName = CommonUtil.getEmpObject(category.getUpdatedBy());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		
		/*if(category.getUpdatedBy() != null){
			
			List<EmployeeDO> empList = employeeService.retriveByEmpId(Long.parseLong(category.getUpdatedBy()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
			List<UserDO> userList = userService.retriveById(Long.parseLong(category.getUpdatedBy()));
			if(userList != null && userList.size() > 0){
				result.put(CommonConstants.UPDATED_BY, String.valueOf(userList.get(0).getName())); 
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}*/
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(category.getUpdatedon())));
		return result;
	}
}
