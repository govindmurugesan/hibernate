package com.spheresuite.erp.cms.web.util;

import java.util.List;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.CMSDepartmentDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
@Component
public class CMSDepartmentUtil {
	
	private CMSDepartmentUtil() {}

	@Autowired
	private EmployeeService temployeeService;
	
	private static EmployeeService employeeService;
	
	@PostConstruct
	public void init() {
		employeeService = temployeeService;
	}
	
	public static JSONObject getDepartmentList(List<CMSDepartmentDO> departmentList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (CMSDepartmentDO department : departmentList) {
				resultJSONArray.put(getDepartmentDetailObject(department));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getDepartmentDetailObject(CMSDepartmentDO department)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(department.getId()));
		result.put(CommonConstants.NAME, String.valueOf(department.getName()));
		result.put(CommonConstants.STATUS, String.valueOf(department.getStatus()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(department.getUpdatedon())));
		result.put(CommonConstants.STATUS, String.valueOf(department.getStatus()));
		
		
		if(department.getComments() != null){
			result.put(CommonConstants.COMMENT, String.valueOf(department.getComments()));
		}else{
			result.put(CommonConstants.COMMENT, "");
		}
		
		if(department.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(department.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		/*if(department.getUpdatedby() != null){
			List<EmployeeDO> empList = employeeService.retriveByEmpId(Long.parseLong(department.getUpdatedby()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}*/
		return result;
	}
}
