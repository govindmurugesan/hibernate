package com.spheresuite.erp.cms.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.CMSDepartmentDO;
import com.spheresuite.erp.domainobject.CMSFrequencyDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class CMSFrequencyDAO {
	static Logger logger = Logger.getLogger(CMSFrequencyDAO.class.getName());
	@Autowired
	private GenericDAOImpl<CMSFrequencyDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(CMSFrequencyDO ferquencytDO) {
		try {
			List<CMSFrequencyDO> frequencyDOList = null;
			frequencyDOList = this.sessionFactory.getCurrentSession().getNamedQuery(CMSFrequencyDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, ferquencytDO.getName())
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
					if(frequencyDOList != null && frequencyDOList.size() > 0){
						return false;
					}else{
						genericObject.persist(ferquencytDO);
						return true;
					}
		} catch (Exception eException) {
			return true;
		} finally {
		}
	}
	
	
	public List<CMSFrequencyDO> retrieve() {
		List<CMSFrequencyDO> frequencyList = null;
		try {
			frequencyList = genericObject.retrieve(CMSFrequencyDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return frequencyList;
	}
	
	public List<CMSFrequencyDO> retrieveActive() {
		List<CMSFrequencyDO> frequencyList = null;
		try {
			return genericObject.retrieveActive(CommonConstants.ACTIVE, 
					CMSFrequencyDO.FIND_BY_STATUS, CMSFrequencyDO.class);
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return frequencyList;
	}
	
	@SuppressWarnings("unchecked")
	public List<CMSFrequencyDO> retrieveById(Long Id) {
		List<CMSFrequencyDO> frequencyList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(CMSFrequencyDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, Id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return frequencyList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<CMSFrequencyDO> retrieveByName(String name) {
		List<CMSFrequencyDO> frequencyList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(CMSFrequencyDO.FIND_BY_NAME_ALL)
					.setParameter(CommonConstants.NAME, name)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return frequencyList;
	}
	
	
	@SuppressWarnings("unchecked")
	public boolean update(CMSFrequencyDO frequencyDO) {
		try {
			List<CMSFrequencyDO> frequencyDOList = null;
			frequencyDOList = this.sessionFactory.getCurrentSession().getNamedQuery(CMSDepartmentDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, frequencyDO.getName())
					.setParameter(CommonConstants.ID, frequencyDO.getId())
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
			if(frequencyDOList != null && frequencyDOList.size() > 0){
				return false;
			}else{
				genericObject.merge(frequencyDO);
				return true;
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			return true;
		} finally {
		}
	}

}
