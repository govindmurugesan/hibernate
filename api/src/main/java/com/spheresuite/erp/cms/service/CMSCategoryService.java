package com.spheresuite.erp.cms.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.cms.dao.CMSCategoryDAO;
import com.spheresuite.erp.domainobject.CMSCategoryDO;
@Service
@Transactional
public class CMSCategoryService {
	static Logger logger = Logger.getLogger(CMSCategoryService.class.getName());
	
	@Autowired
	private CMSCategoryDAO cmsCategoryDAO;
	
	@Transactional
	public boolean persist(CMSCategoryDO countryDO)  {
		return cmsCategoryDAO.persist(countryDO);
	}

	@Transactional
	public List<CMSCategoryDO> retrieveActive()  {
		return cmsCategoryDAO.retrieveActive();
	}
	
	@Transactional
	public List<CMSCategoryDO> retrieveById(Long id)  {
		return cmsCategoryDAO.retrieveById(id);
	}
	
	@Transactional
	public List<CMSCategoryDO> retrieve()  {
		return cmsCategoryDAO.retrieve();
	}
	
	@Transactional
	public boolean update(CMSCategoryDO countryDO)  {
		return cmsCategoryDAO.update(countryDO);
	}
}
