package com.spheresuite.erp.cms.rs;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.cms.service.CMSFrequencyService;
import com.spheresuite.erp.cms.web.util.CMSFrequencyUtil;
import com.spheresuite.erp.domainobject.CMSFrequencyDO;
import com.spheresuite.erp.service.UserService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/cms_frequency")
public class CMSFrequencyRS {

	String validation = null;
	static Logger logger = Logger.getLogger(CMSFrequencyRS.class.getName());

	@Autowired
	private CMSFrequencyService frequencyService;
	
	@Autowired
	private UserService userService;
	
	
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) { 
			
		try {
			if (WebManager.authenticateSession(request)) {
				CMSFrequencyDO cmsFrequencyDO = new CMSFrequencyDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		cmsFrequencyDO.setName(!inputJSON.get(CommonConstants.NAME).toString().isEmpty() ? inputJSON.get(CommonConstants.NAME).toString() : "");
			 		cmsFrequencyDO.setUpdatedon(new Date());
			 		
			 		/*
			 		if(inputJSON.get(CommonConstants.COMMENT).toString() != null && !inputJSON.get(CommonConstants.COMMENT).toString().isEmpty()){
			 			cmsFrequencyDO.setComments(inputJSON.get(CommonConstants.COMMENT).toString());
			 		}*/
			 		if(inputJSON.get(CommonConstants.COMMENT) != null){
			 			cmsFrequencyDO.setComments(inputJSON.get(CommonConstants.COMMENT).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			cmsFrequencyDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		cmsFrequencyDO.setStatus('a');
			 	}
			 	if(!frequencyService.persist(cmsFrequencyDO)){
					return CommonWebUtil.buildErrorResponse("Frequency Already Added").toString();
				}
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Frequency Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retrieveActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<CMSFrequencyDO> frequencyList = frequencyService.retrieveActive();
				respJSON = CMSFrequencyUtil.getFrequencyList(frequencyList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<CMSFrequencyDO> frequencyList = frequencyService.retrieve();
				respJSON = CMSFrequencyUtil.getFrequencyList(frequencyList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				CMSFrequencyDO frequencyDO = new CMSFrequencyDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<CMSFrequencyDO> frqList = frequencyService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		frequencyDO = frqList.get(0);
			 		frequencyDO.setName(inputJSON != null ? inputJSON.get(CommonConstants.NAME).toString() : "");
			 		frequencyDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			frequencyDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.COMMENT) != null){
			 			frequencyDO.setComments(inputJSON.get(CommonConstants.COMMENT).toString());
			 		}else{
			 			frequencyDO.setComments("");
			 		}
			 		
			 		frequencyDO.setStatus((char) (inputJSON != null ? inputJSON.get(CommonConstants.STATUS).toString().charAt(0) : ""));
			 		//departmentService.update(departmentDO);
			 		if(!frequencyService.update(frequencyDO)){
						return CommonWebUtil.buildErrorResponse("Frequency Already Added").toString();
					}
			 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Frequency Updated");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
}
