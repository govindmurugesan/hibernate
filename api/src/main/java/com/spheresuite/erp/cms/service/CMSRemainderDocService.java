package com.spheresuite.erp.cms.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.cms.dao.CMSRemainderDocDAO;
import com.spheresuite.erp.domainobject.CMSRemainderDocDO;
@Service
@Transactional
public class CMSRemainderDocService {
	static Logger logger = Logger.getLogger(CMSRemainderDocService.class.getName());
	@Autowired
	private CMSRemainderDocDAO cmdRemainderDocDAO;

	@Transactional
	public CMSRemainderDocDO persist(CMSRemainderDocDO cmsRemainderDocDO) {
		return cmdRemainderDocDAO.persist(cmsRemainderDocDO);
	}

	@Transactional
	public List<CMSRemainderDocDO> retrieveByCMSId(Long id) {
		return cmdRemainderDocDAO.retrieveByCMSId(id);
	}

	@Transactional
	public boolean delete(Long id) {
		return cmdRemainderDocDAO.delete(id);
	}

	@Transactional
	public List<CMSRemainderDocDO> retrieve() {
		return cmdRemainderDocDAO.retrieve();
	}

	@Transactional
	public CMSRemainderDocDO update(CMSRemainderDocDO cmsRemainderDocDO) {
		return cmdRemainderDocDAO.update(cmsRemainderDocDO);
	}
}
