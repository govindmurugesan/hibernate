package com.spheresuite.erp.rs;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.DASettingsDO;
import com.spheresuite.erp.domainobject.UnitOrBranchDO;
import com.spheresuite.erp.service.DASettingsService;
import com.spheresuite.erp.service.UnitOrBranchService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.DASettingsUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/dasettings")
public class DASettingsRS {

	String validation = null;
	static Logger logger = Logger.getLogger(DASettingsRS.class.getName());

	@Autowired
	private DASettingsService daSettingsService;
	
	@Autowired
	private UnitOrBranchService unitOrBranchService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				DASettingsDO DASettingsDO = new DASettingsDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.YEAR) != null && !inputJSON.get(CommonConstants.YEAR).toString().isEmpty()){
			 			DASettingsDO.setYear(Long.parseLong(inputJSON.get(CommonConstants.YEAR).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.MONTHLY) != null && !inputJSON.get(CommonConstants.MONTHLY).toString().isEmpty()){
			 			DASettingsDO.setMonth(inputJSON.get(CommonConstants.MONTHLY).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.DAPOINTS) != null && !inputJSON.get(CommonConstants.DAPOINTS).toString().isEmpty()){
			 			DASettingsDO.setDApoints(Long.parseLong(inputJSON.get(CommonConstants.DAPOINTS).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.RATE) != null && !inputJSON.get(CommonConstants.RATE).toString().isEmpty()){
			 			DASettingsDO.setDArate(Float.parseFloat(inputJSON.get(CommonConstants.RATE).toString()));
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.UNIT) != null && !inputJSON.get(CommonConstants.UNIT).toString().isEmpty()){
			 			List<UnitOrBranchDO> unitList = unitOrBranchService.retrieveActiveById(Long.parseLong(inputJSON.get(CommonConstants.UNIT).toString()));
			 			if(unitList != null && unitList.size() > 0){
			 				DASettingsDO.setUnitOrBranch(unitList.get(0));
			 			}
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.DADEDUCTIONPOINT) != null && !inputJSON.get(CommonConstants.DADEDUCTIONPOINT).toString().isEmpty()){
			 			DASettingsDO.setDAdeductionpoints(Long.parseLong(inputJSON.get(CommonConstants.DADEDUCTIONPOINT).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.ADDDA) != null && !inputJSON.get(CommonConstants.ADDDA).toString().isEmpty()){
			 			DASettingsDO.setAddDA(Long.parseLong(inputJSON.get(CommonConstants.ADDDA).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.SUBDA) != null && !inputJSON.get(CommonConstants.SUBDA).toString().isEmpty()){
			 			DASettingsDO.setSubDA(Long.parseLong(inputJSON.get(CommonConstants.SUBDA).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			DASettingsDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			DASettingsDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		DASettingsDO.setUpdatedon(new Date());
			 		DASettingsDO.setCreatedon(new Date());
			 	}
			 	if(!daSettingsService.persist(DASettingsDO)){
					return CommonWebUtil.buildErrorResponse("Dearness Alloweance Added").toString();
				}
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Dearness Alloweance Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<DASettingsDO> daList = daSettingsService.retrieve();
				respJSON = DASettingsUtil.getDASettingsList(daList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				DASettingsDO daSettingsDO = new DASettingsDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<DASettingsDO> daList = daSettingsService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(daList != null && daList.size() > 0){
			 			daSettingsDO = daList.get(0);
			 			if(inputJSON.get(CommonConstants.YEAR) != null && !inputJSON.get(CommonConstants.YEAR).toString().isEmpty()){
			 				daSettingsDO.setYear(Long.parseLong(inputJSON.get(CommonConstants.YEAR).toString()));
				 		}
				 		if(inputJSON.get(CommonConstants.MONTHLY) != null && !inputJSON.get(CommonConstants.MONTHLY).toString().isEmpty()){
				 			daSettingsDO.setMonth(inputJSON.get(CommonConstants.MONTHLY).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.DAPOINTS) != null && !inputJSON.get(CommonConstants.DAPOINTS).toString().isEmpty()){
				 			daSettingsDO.setDApoints(Long.parseLong(inputJSON.get(CommonConstants.DAPOINTS).toString()));
				 		}
				 		if(inputJSON.get(CommonConstants.RATE) != null && !inputJSON.get(CommonConstants.RATE).toString().isEmpty()){
				 			daSettingsDO.setDArate(Float.parseFloat(inputJSON.get(CommonConstants.RATE).toString()));
				 		}
				 		if(inputJSON.get(CommonConstants.DADEDUCTIONPOINT) != null && !inputJSON.get(CommonConstants.DADEDUCTIONPOINT).toString().isEmpty()){
				 			daSettingsDO.setDAdeductionpoints(Long.parseLong(inputJSON.get(CommonConstants.DADEDUCTIONPOINT).toString()));
				 		}
				 		if(inputJSON.get(CommonConstants.ADDDA) != null && !inputJSON.get(CommonConstants.ADDDA).toString().isEmpty()){
				 			daSettingsDO.setAddDA(Long.parseLong(inputJSON.get(CommonConstants.ADDDA).toString()));
				 		}
				 		if(inputJSON.get(CommonConstants.SUBDA) != null && !inputJSON.get(CommonConstants.SUBDA).toString().isEmpty()){
				 			daSettingsDO.setSubDA(Long.parseLong(inputJSON.get(CommonConstants.SUBDA).toString()));
				 		}
				 		if(inputJSON.get(CommonConstants.UNIT) != null && !inputJSON.get(CommonConstants.UNIT).toString().isEmpty()){
				 			List<UnitOrBranchDO> unitList = unitOrBranchService.retrieveActiveById(Long.parseLong(inputJSON.get(CommonConstants.UNIT).toString()));
					 			if(unitList != null && unitList.size() > 0){
					 				daSettingsDO.setUnitOrBranch(unitList.get(0));
					 			}
			 			}
				 		daSettingsDO.setUpdatedon(new Date());
				 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			daSettingsDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		if(!daSettingsService.update(daSettingsDO)){
							return CommonWebUtil.buildErrorResponse("Dearness Alloewance Already Added For This Month").toString();
						}
				 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Dearness Alloewance Updated");
			 		}
			 		}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
