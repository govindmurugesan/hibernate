package com.spheresuite.erp.rs;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.MonthlyPermissionDO;
import com.spheresuite.erp.service.MonthlyPermissionService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.MonthlyPermissionUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/monthlypermission")
public class MonthlyPermissionRS {

	String validation = null;
	static Logger logger = Logger.getLogger(MonthlyPermissionRS.class.getName());
	@Autowired
	private MonthlyPermissionService monthlyPermissionService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				MonthlyPermissionDO monthlyPermissionDO = new MonthlyPermissionDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.PERMISSIONHOURSPERMONTH) != null && !inputJSON.get(CommonConstants.PERMISSIONHOURSPERMONTH).toString().isEmpty()){
			 			monthlyPermissionDO.setPermissionHoursPerMonth(inputJSON.get(CommonConstants.PERMISSIONHOURSPERMONTH).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.NUMBEROFPERMISSION) != null && !inputJSON.get(CommonConstants.NUMBEROFPERMISSION).toString().isEmpty()){
			 			monthlyPermissionDO.setNumberOfPermissionPerMonth(Double.parseDouble(inputJSON.get(CommonConstants.NUMBEROFPERMISSION).toString()));
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.PERMISSIONHOURSPERDAY) != null && !inputJSON.get(CommonConstants.PERMISSIONHOURSPERDAY).toString().isEmpty()){
			 			monthlyPermissionDO.setPermissionHoursPerDay(inputJSON.get(CommonConstants.PERMISSIONHOURSPERDAY).toString());
			 		}
			 		monthlyPermissionDO.setStatus(CommonConstants.ACTIVE);
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			monthlyPermissionDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			monthlyPermissionDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		monthlyPermissionDO.setUpdatedon(new Date());
			 		monthlyPermissionDO.setCreatedon(new Date());
			 	}
				monthlyPermissionService.persist(monthlyPermissionDO);
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Earning Type Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<MonthlyPermissionDO> cagtegoryList = monthlyPermissionService.retrieveActive();
				respJSON = MonthlyPermissionUtil.getMonthlyPermissionList(cagtegoryList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				MonthlyPermissionDO monthlyPermissionDO = new MonthlyPermissionDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<MonthlyPermissionDO> earningTypeList = monthlyPermissionService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(earningTypeList != null && earningTypeList.size() >0){
				 		monthlyPermissionDO = earningTypeList.get(0);
				 		if(inputJSON.get(CommonConstants.PERMISSIONHOURSPERMONTH) != null && !inputJSON.get(CommonConstants.PERMISSIONHOURSPERMONTH).toString().isEmpty()){
				 			monthlyPermissionDO.setPermissionHoursPerMonth(inputJSON.get(CommonConstants.PERMISSIONHOURSPERMONTH).toString());
				 		}
				 		
				 		if(inputJSON.get(CommonConstants.NUMBEROFPERMISSION) != null && !inputJSON.get(CommonConstants.NUMBEROFPERMISSION).toString().isEmpty()){
				 			monthlyPermissionDO.setNumberOfPermissionPerMonth(Double.parseDouble(inputJSON.get(CommonConstants.NUMBEROFPERMISSION).toString()));
				 		}
				 		
				 		if(inputJSON.get(CommonConstants.PERMISSIONHOURSPERDAY) != null && !inputJSON.get(CommonConstants.PERMISSIONHOURSPERDAY).toString().isEmpty()){
				 			monthlyPermissionDO.setPermissionHoursPerDay(inputJSON.get(CommonConstants.PERMISSIONHOURSPERDAY).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			monthlyPermissionDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		monthlyPermissionDO.setUpdatedon(new Date());
				 		monthlyPermissionService.update(monthlyPermissionDO);
				 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Earning Type Updated");
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
