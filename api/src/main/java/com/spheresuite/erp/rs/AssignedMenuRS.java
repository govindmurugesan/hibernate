package com.spheresuite.erp.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.AssignedMenuDO;
import com.spheresuite.erp.domainobject.MenuDO;
import com.spheresuite.erp.domainobject.RolesDO;
import com.spheresuite.erp.service.AssignedMenuService;
import com.spheresuite.erp.service.MenuService;
import com.spheresuite.erp.service.RolesService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.AssignedMenuUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/assignedmenu")
public class AssignedMenuRS {

	String validation = null;
	static Logger logger = Logger.getLogger(AssignedMenuRS.class.getName());
	
	@Autowired
	private AssignedMenuService assignedMenuService;
	
	@Autowired
	private RolesService rolesService;
	
	@Autowired
	private MenuService menuService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				List<AssignedMenuDO> assignedMenuList = new ArrayList<AssignedMenuDO>();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
				if(inputJSON != null && (inputJSON.get(CommonConstants.ROLE_NAME) != null && !inputJSON.get(CommonConstants.ROLE_NAME).toString().isEmpty()
						|| inputJSON.get(CommonConstants.DESCRIPTION) != null && !inputJSON.get(CommonConstants.DESCRIPTION).toString().isEmpty())){
					RolesDO rolesDO = new RolesDO(); 
					List<RolesDO> rolesList = rolesService.retriveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					rolesDO = rolesList.get(0);
					if(inputJSON.get(CommonConstants.ROLE_NAME) != null && !inputJSON.get(CommonConstants.ROLE_NAME).toString().isEmpty()){
						rolesDO.setName(inputJSON.get(CommonConstants.ROLE_NAME) != null && !inputJSON.get(CommonConstants.ROLE_NAME).toString().isEmpty()?inputJSON.get(CommonConstants.ROLE_NAME).toString():"");
					}
					if(inputJSON.get(CommonConstants.DESCRIPTION) != null && !inputJSON.get(CommonConstants.DESCRIPTION).toString().isEmpty()){
						rolesDO.setDescription(inputJSON.get(CommonConstants.DESCRIPTION) != null && !inputJSON.get(CommonConstants.DESCRIPTION).toString().isEmpty()?inputJSON.get(CommonConstants.DESCRIPTION).toString():"");
					}
					rolesDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			rolesDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			rolesDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		rolesService.update(rolesDO);
				}
			 	if (inputJSON != null && inputJSON.get(CommonConstants.MENU) != null && !inputJSON.get(CommonConstants.MENU).toString().isEmpty()){
			 		JSONArray resultJSONArray = new JSONArray();
			 		resultJSONArray.put(inputJSON.get(CommonConstants.MENU));
			 		String rec = (String) resultJSONArray.get(0).toString();
			 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
			 		for (int i = 0; i < resultJSONArray1.size(); i++) {
			 			AssignedMenuDO assignedMenuDO = new AssignedMenuDO(); 
			 			JSONObject inputJSON1 = (JSONObject) resultJSONArray1.get(i);
			 			if(!inputJSON1.get(CommonConstants.SUB_MENU).toString().isEmpty()){
			 				List<MenuDO> menuList = menuService.retrieveById(Long.parseLong(inputJSON1.get(CommonConstants.SUB_MENU).toString()));
			 				if(menuList != null && menuList.size() > 0){
			 					assignedMenuDO.setMenu(menuList.get(0));
			 					assignedMenuDO.setProductId(menuList.get(0).getProduct());
			 				}
			 				List<RolesDO> rolesList = rolesService.retriveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
							if(rolesList != null && rolesList.size() > 0){
								assignedMenuDO.setRole(rolesList.get(0));
							}
							assignedMenuDO.setUpdatedon(new Date());
							assignedMenuDO.setCreatedon(new Date());
			 				
			 				//assignedMenuDO.setMenuId(Long.parseLong(inputJSON1.get(CommonConstants.SUB_MENU).toString()));
				 			//assignedMenuDO.setRoleId(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
				 			
			 			}
			 			if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 				assignedMenuDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 				assignedMenuDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
			 			assignedMenuList.add(assignedMenuDO);
			 		}
			 		
			 	}
				assignedMenuService.persist(assignedMenuList);
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retrieveProductByRoleId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveProductByRoleId(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<AssignedMenuDO> assignmentList = assignedMenuService.retrieveProductByRoleId(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					if(assignmentList != null && assignmentList.size() > 0 ){
						respJSON = AssignedMenuUtil.getMenuList(assignmentList).toString();
					}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveMenuByRoleId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveMenuByRoleId(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ROLE_ID).toString().isEmpty() && !inputJSON.get(CommonConstants.PRODUCT_ID).toString().isEmpty()){
			 		List<AssignedMenuDO> assignmentList = assignedMenuService.retrieveMenuByRoleId(Long.parseLong(inputJSON.get(CommonConstants.ROLE_ID).toString()) , Long.parseLong(inputJSON.get(CommonConstants.PRODUCT_ID).toString()));
					if(assignmentList != null && assignmentList.size() > 0 ){
						respJSON = AssignedMenuUtil.getMenuList1(assignmentList, Long.parseLong(inputJSON.get(CommonConstants.ROLE_ID).toString())).toString();
					}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	
}
