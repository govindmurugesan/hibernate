package com.spheresuite.erp.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.EducationCessDO;
import com.spheresuite.erp.service.EducationCessService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.EducationCessUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/educationCess")
public class EducationCessRS {

	String validation = null;
	static Logger logger = Logger.getLogger(EducationCessRS.class.getName());

	@Autowired
	private EducationCessService educationCessService;
	
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EducationCessDO educationCessDO = new EducationCessDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.FROMDATE) != null && !inputJSON.get(CommonConstants.FROMDATE).toString().isEmpty()){
			 			educationCessDO.setFromYear(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.FROMDATE).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.TODATE) != null && !inputJSON.get(CommonConstants.TODATE).toString().isEmpty()){
			 			educationCessDO.setToYear(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.TODATE).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.PERCENTAGE) != null && !inputJSON.get(CommonConstants.PERCENTAGE).toString().isEmpty()){
			 			educationCessDO.setPercentage(Double.parseDouble(inputJSON.get(CommonConstants.PERCENTAGE).toString()));
			 		}
			 		educationCessDO.setUpdatedon(new Date());
			 		educationCessDO.setCreatedon(new Date());
			 		educationCessDO.setStatus(CommonConstants.ACTIVE);
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			educationCessDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			educationCessDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 	}
				if(!educationCessService.persist(educationCessDO)){
					return CommonWebUtil.buildErrorResponse("Education Cess Already Added").toString();
				}
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Education Cess Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<EducationCessDO> educationCessList = educationCessService.retrieve();
				respJSON = EducationCessUtil.getEducationCessList(educationCessList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EducationCessDO educationCessDO = new EducationCessDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<EducationCessDO> educationCessList = educationCessService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(educationCessList != null && educationCessList.size() > 0){
			 			educationCessDO = educationCessList.get(0);
				 		if(inputJSON.get(CommonConstants.FROMDATE) != null && !inputJSON.get(CommonConstants.FROMDATE).toString().isEmpty()){
				 			educationCessDO.setFromYear(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.FROMDATE).toString()));
				 		}
				 		if(inputJSON.get(CommonConstants.TODATE) != null && !inputJSON.get(CommonConstants.TODATE).toString().isEmpty()){
				 			educationCessDO.setToYear(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.TODATE).toString()));
				 		}
				 		if(inputJSON.get(CommonConstants.AMOUNT) != null && !inputJSON.get(CommonConstants.AMOUNT).toString().isEmpty()){
				 			educationCessDO.setPercentage(Double.parseDouble(inputJSON.get(CommonConstants.AMOUNT).toString()));
				 		}
				 		if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
				 			educationCessDO.setStatus((char) (inputJSON.get(CommonConstants.STATUS).toString().charAt(0)));
				 		}
				 		educationCessDO.setUpdatedon(new Date());
				 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			educationCessDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		if(!educationCessService.update(educationCessDO)){
							return CommonWebUtil.buildErrorResponse("Education Cess Already Added").toString();
						}
				 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Education Cess Updated");
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/importEducationCess", method = RequestMethod.POST)
	public @ResponseBody String importEducationCess(Model model, HttpServletRequest request) {
		try {
			JSONObject inputJSON =  CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
			JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
			List<EducationCessDO> educationCessList = new ArrayList<EducationCessDO>();
			Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY);
			for (int i=0; i < fileData.size(); i++){
				JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
				EducationCessDO educationCessDO = new EducationCessDO();
				
				if(colName.get(CommonConstants.FROMDATE) != null && !colName.get(CommonConstants.FROMDATE).toString().isEmpty()){
		 			educationCessDO.setFromYear(CommonUtil.convertStringToSqlDate(rowJSON.get(colName.get(CommonConstants.FROMDATE)).toString()));
		 		}
		 		if(colName.get(CommonConstants.TODATE) != null && !colName.get(CommonConstants.TODATE).toString().isEmpty()){
		 			educationCessDO.setToYear(CommonUtil.convertStringToSqlDate(rowJSON.get(colName.get(CommonConstants.TODATE)).toString()));
		 		}
		 		if(colName.get(CommonConstants.PERCENTAGE) != null && !colName.get(CommonConstants.PERCENTAGE).toString().isEmpty()){
		 			educationCessDO.setPercentage(Double.parseDouble(rowJSON.get(colName.get(CommonConstants.PERCENTAGE)).toString()));
		 		}
		 		if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
		 			educationCessDO.setUpdatedby(updatedBy.toString());
		 			educationCessDO.setCreatedby(updatedBy.toString());
		 		}
		 		educationCessDO.setUpdatedon(new Date());
		 		educationCessDO.setCreatedon(new Date());
		 		educationCessDO.setStatus(CommonConstants.ACTIVE);
		 		educationCessList.add(educationCessDO);
			}
			educationCessService.persistList(educationCessList);
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
