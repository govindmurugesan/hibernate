package com.spheresuite.erp.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.crm.rs.LeadTypeRS;
import com.spheresuite.erp.domainobject.GSTTaxSlabDO;
import com.spheresuite.erp.service.GSTTaxSlabService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.GSTTaxSlabUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/gstTaxSlab")
public class GSTTaxSlabRS {

	String validation = null;
	static Logger logger = Logger.getLogger(LeadTypeRS.class.getName());

	@Autowired
	private GSTTaxSlabService gSTTaxSlabService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				GSTTaxSlabDO gstTaxDO = new GSTTaxSlabDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
			 			gstTaxDO.setPercentage(inputJSON.get(CommonConstants.NAME).toString());
			 		}
			 		gstTaxDO.setStatus(CommonConstants.ACTIVE);
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			gstTaxDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			gstTaxDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		gstTaxDO.setUpdatedon(new Date());
			 		gstTaxDO.setCreatedon(new Date());
			 	}
				if(!gSTTaxSlabService.persist(gstTaxDO)){
					return CommonWebUtil.buildErrorResponse("Tax Slab Already Added").toString();
				}
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New GST Tax Slab Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<GSTTaxSlabDO> gstTaxSlabList = gSTTaxSlabService.retrieveActive();
				respJSON = GSTTaxSlabUtil.getTaxSlabList(gstTaxSlabList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<GSTTaxSlabDO> gstTaxSlabList = gSTTaxSlabService.retrieve();
				respJSON = GSTTaxSlabUtil.getTaxSlabList(gstTaxSlabList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				GSTTaxSlabDO gstTaxSlabList = new GSTTaxSlabDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<GSTTaxSlabDO> list = gSTTaxSlabService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(list != null && list.size() > 0){
				 		gstTaxSlabList = list.get(0);
				 		if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
				 			gstTaxSlabList.setPercentage(inputJSON.get(CommonConstants.NAME).toString());
				 		}
				 		gstTaxSlabList.setUpdatedon(new Date());
				 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			gstTaxSlabList.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
				 			gstTaxSlabList.setStatus((char) inputJSON.get(CommonConstants.STATUS).toString().charAt(0));
				 		}
				 		if(!gSTTaxSlabService.update(gstTaxSlabList)){
				 			return CommonWebUtil.buildErrorResponse("GST Tax Slab Already Added").toString();
				 		}
				 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "GST Tax Slab Updated");
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/importGstTaxSlab", method = RequestMethod.POST)
	public @ResponseBody String importGstTaxSlab(Model model, HttpServletRequest request) {
		try {
			JSONObject inputJSON =  CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
			JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
			List<GSTTaxSlabDO> gstTaxSlabList = new ArrayList<GSTTaxSlabDO>();
			Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY);
			for (int i=0; i < fileData.size(); i++){
				JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
				GSTTaxSlabDO gstTaxSlabDO = new GSTTaxSlabDO();
				if (colName.get(CommonConstants.NAME) != null && !colName.get(CommonConstants.NAME).toString().isEmpty()){
					gstTaxSlabDO.setPercentage(rowJSON.get(colName.get(CommonConstants.NAME)).toString());
		 		}
		 		if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
		 			gstTaxSlabDO.setUpdatedby(updatedBy.toString());
		 			gstTaxSlabDO.setCreatedby(updatedBy.toString());
		 		}
		 		gstTaxSlabDO.setUpdatedon(new Date());
		 		gstTaxSlabDO.setCreatedon(new Date());
		 		gstTaxSlabDO.setStatus(CommonConstants.ACTIVE);
		 		gstTaxSlabList.add(gstTaxSlabDO);
			}
			gSTTaxSlabService.persistList(gstTaxSlabList);
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
