package com.spheresuite.erp.rs;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.EmployeeExpenseDO;
import com.spheresuite.erp.domainobject.EmployeeExpenseDocDO;
import com.spheresuite.erp.domainobject.ExpenseTypeDO;
import com.spheresuite.erp.domainobject.RolesDO;
import com.spheresuite.erp.service.EmployeeExpenseDocService;
import com.spheresuite.erp.service.EmployeeExpenseService;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.ExpenseTypeService;
import com.spheresuite.erp.service.RolesService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.EmployeeExpenseUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/employeeexpense")
public class EmployeeExpenseRS {

	String validation = null;
	static Logger logger = Logger.getLogger(EmployeeExpenseRS.class.getName());

	@Autowired
	private EmployeeExpenseService employeeExpenseService;
	
	@Autowired
	private ExpenseTypeService expenseTypeService;
	
	@Autowired
	private EmployeeService employeeService;
	
	@Autowired
	private EmployeeExpenseDocService empExpenseDocService;
	
	@Autowired
	private RolesService rolesService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		EmployeeExpenseDO employeeExpenseDO = new EmployeeExpenseDO();	
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
		 			if(inputJSON.get(CommonConstants.TYPE) != null && !inputJSON.get(CommonConstants.TYPE).toString().isEmpty()){
			 			List<ExpenseTypeDO> expenseTypeList = expenseTypeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.TYPE).toString()));
				 		if(expenseTypeList != null && expenseTypeList.size() > 0){
				 			employeeExpenseDO.setExpenseType(expenseTypeList.get(0));
				 			if(inputJSON.get(CommonConstants.EMPID).toString() != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
				 				List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
				 				if(employeeList != null && employeeList.size() > 0){
				 					employeeExpenseDO.setEmployee(employeeList.get(0));
				 					if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 						employeeExpenseDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 						employeeExpenseDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
							 			if(inputJSON.get(CommonConstants.DESCRIPTION) != null && !inputJSON.get(CommonConstants.DESCRIPTION).toString().isEmpty()){
							 				employeeExpenseDO.setDescription(inputJSON.get(CommonConstants.DESCRIPTION).toString());
								 		}
								 		if(inputJSON.get(CommonConstants.AMOUNT) != null && !inputJSON.get(CommonConstants.AMOUNT).toString().isEmpty()){
								 			employeeExpenseDO.setAmount(Long.parseLong(inputJSON.get(CommonConstants.AMOUNT).toString()));
								 		}
								 		if(inputJSON.get(CommonConstants.FROMDATE) != null && !inputJSON.get(CommonConstants.FROMDATE).toString().isEmpty()){
								 			employeeExpenseDO.setFromDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.FROMDATE).toString()));
								 		}
								 		if(inputJSON.get(CommonConstants.TODATE) != null && !inputJSON.get(CommonConstants.TODATE).toString().isEmpty()){
								 			employeeExpenseDO.setToDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.TODATE).toString()));
								 		}
								 		employeeExpenseDO.setUpdatedon(new Date());
								 		employeeExpenseDO.setCreatedon(new Date());
								 		employeeExpenseDO.setStatus(CommonConstants.SSTATUS);
								 		employeeExpenseService.persist(employeeExpenseDO);
										CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Employee Expense Created");
						 			}
										 			
				 				}
					 		}
				 		}
			 		}
			 	}
				
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponseId(employeeExpenseDO.getEmpExpenseId().toString()).toString();
	}
	
	@RequestMapping(value = "/retrieveByEmpId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByEmpId(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<EmployeeExpenseDO> employeeExpenseList = employeeExpenseService.retrieveByEmpId(inputJSON.get(CommonConstants.ID).toString());
					respJSON = EmployeeExpenseUtil.getEmployeeExpenseList(employeeExpenseList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		EmployeeExpenseDO employeeExpenseDO = new EmployeeExpenseDO();
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<EmployeeExpenseDO> employeeExpenseList = employeeExpenseService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		employeeExpenseDO = employeeExpenseList.get(0);
			 		
			 		if(inputJSON.get(CommonConstants.DESCRIPTION) != null && !inputJSON.get(CommonConstants.DESCRIPTION).toString().isEmpty()){
			 			employeeExpenseDO.setDescription(inputJSON.get(CommonConstants.DESCRIPTION).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.AMOUNT) != null && !inputJSON.get(CommonConstants.AMOUNT).toString().isEmpty()){
			 			employeeExpenseDO.setAmount(Long.parseLong(inputJSON.get(CommonConstants.AMOUNT).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.FROMDATE) != null && !inputJSON.get(CommonConstants.FROMDATE).toString().isEmpty()){
			 			employeeExpenseDO.setFromDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.FROMDATE).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.TODATE) != null && !inputJSON.get(CommonConstants.TODATE).toString().isEmpty()){
			 			employeeExpenseDO.setToDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.TODATE).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.TYPE) != null && !inputJSON.get(CommonConstants.TYPE).toString().isEmpty()){
			 			List<ExpenseTypeDO> expenseTypeList = expenseTypeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.TYPE).toString()));
				 		if(expenseTypeList != null && expenseTypeList.size() > 0){
				 			employeeExpenseDO.setExpenseType(expenseTypeList.get(0));
				 		}
			 		}
			 		if(inputJSON.get(CommonConstants.EMPID).toString() != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
				 		if(employeeList != null && employeeList.size() > 0){
				 			employeeExpenseDO.setEmployee(employeeList.get(0));
				 		}
			 		}
			 		if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
			 			employeeExpenseDO.setStatus(inputJSON.get(CommonConstants.STATUS).toString());
			 		}
			 		employeeExpenseDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			employeeExpenseDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		employeeExpenseService.update(employeeExpenseDO);
			 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Employee Expense Type Updated");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponseId(employeeExpenseDO.getEmpExpenseId().toString()).toString();
	}
	
	
	@RequestMapping(value = "/retrieveByReportingId/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByReportingId(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && inputJSON.get(CommonConstants.EMPID)!= null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()
						&& inputJSON.get(CommonConstants.ROLE_ID)!= null && !inputJSON.get(CommonConstants.ROLE_ID).toString().isEmpty()){
					List<RolesDO> rolesList =  rolesService.retriveById(Long.parseLong(inputJSON.get(CommonConstants.ROLE_ID).toString()));
					if(rolesList != null && rolesList.size() > 0){
						if(rolesList.get(0).getName().equalsIgnoreCase(CommonConstants.SUPERADMIN)){
							List<EmployeeExpenseDO> expenseList = employeeExpenseService.retrieve();
							respJSON = EmployeeExpenseUtil.getEmployeeExpenseList(expenseList).toString();
						}else {
							List<EmployeeExpenseDO> expenseList = employeeExpenseService.retrieveByReportingId(inputJSON.get(CommonConstants.EMPID).toString());
							respJSON = EmployeeExpenseUtil.getEmployeeExpenseList(expenseList).toString();
						}
					}else {
						List<EmployeeExpenseDO> expenseList = employeeExpenseService.retrieveByReportingId(inputJSON.get(CommonConstants.EMPID).toString());
						respJSON = EmployeeExpenseUtil.getEmployeeExpenseList(expenseList).toString();
					}
					
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/updatepic", method = RequestMethod.POST)
	public @ResponseBody String updatepic(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeExpenseDocDO employeeExpenseDocDO = new EmployeeExpenseDocDO();
				/*JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("data").toString());*/
			 	if (request.getParameter("file") != null && request.getParameter("id") != null){
			 		List<EmployeeExpenseDO> empExpenseList = employeeExpenseService.retrieveById(Long.parseLong(request.getParameter("id").toString()));
			 		if(empExpenseList != null && empExpenseList.size() > 0){
			 			employeeExpenseDocDO.setEmpExpense(empExpenseList.get(0));
			 			employeeExpenseDocDO.setPhoto(request.getParameter("file"));
			 			employeeExpenseDocDO.setFileName(request.getParameter("name"));
			 			employeeExpenseDocDO.setUpdatedon(new Date());
			 			employeeExpenseDocDO.setCreatedon(new Date());
				 		if(request.getParameter(CommonConstants.UPDATED_BY) != null && !request.getParameter(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			employeeExpenseDocDO.setUpdatedby(request.getParameter(CommonConstants.UPDATED_BY).toString());
				 			employeeExpenseDocDO.setCreatedby(request.getParameter(CommonConstants.UPDATED_BY).toString());
				 		}
				 		empExpenseDocService.persist(employeeExpenseDocDO);
			 		}else{
			 			return CommonWebUtil.buildErrorResponse("").toString();
			 		}
 			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
