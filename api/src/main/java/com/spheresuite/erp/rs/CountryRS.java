package com.spheresuite.erp.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.CountryDO;
import com.spheresuite.erp.service.CountryService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.CountryUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/country")
public class CountryRS {

	String validation = null;
	static Logger logger = Logger.getLogger(CountryRS.class.getName());

	@Autowired
	private CountryService countryService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				CountryDO countryDO = new CountryDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		
			 		if (!inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
			 			countryDO.setCountryName(inputJSON.get(CommonConstants.NAME).toString());
			 		}
			 		if (!inputJSON.get(CommonConstants.CODE).toString().isEmpty()){
			 			countryDO.setCountryCode(inputJSON.get(CommonConstants.CODE).toString());
			 		}
			 		if (!inputJSON.get(CommonConstants.CALLING_CODE).toString().isEmpty()){
			 			countryDO.setCallingCode(inputJSON.get(CommonConstants.CALLING_CODE).toString());
			 		}
			 		if (!inputJSON.get(CommonConstants.CURRENCY_TYPE).toString().isEmpty()){
			 			countryDO.setCurrencyType(inputJSON.get(CommonConstants.CURRENCY_TYPE).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			countryDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			countryDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		countryDO.setUpdatedon(new Date());
			 		countryDO.setCreatedon(new Date());
			 		countryDO.setStatus(CommonConstants.ACTIVE);
			 	}
				if(!countryService.persist(countryDO)){
					return CommonWebUtil.buildErrorResponse("Country Already Added").toString();
				}
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Country Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retrieveActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<CountryDO> countryList = countryService.retrieveActive();
				respJSON = CountryUtil.getCountryList(countryList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<CountryDO> countryList = countryService.retrieve();
				respJSON = CountryUtil.getCountryList(countryList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				CountryDO countryDO = new CountryDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<CountryDO> countryList = countryService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		countryDO = countryList.get(0);
			 		if (!inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
			 			countryDO.setCountryName(inputJSON.get(CommonConstants.NAME).toString());
			 		}
			 		if (!inputJSON.get(CommonConstants.CODE).toString().isEmpty()){
			 			countryDO.setCountryCode(inputJSON.get(CommonConstants.CODE).toString());
			 		}
			 		if (!inputJSON.get(CommonConstants.CALLING_CODE).toString().isEmpty()){
			 			countryDO.setCallingCode(inputJSON.get(CommonConstants.CALLING_CODE).toString());
			 		}
			 		if (!inputJSON.get(CommonConstants.CURRENCY_TYPE).toString().isEmpty()){
			 			countryDO.setCurrencyType(inputJSON.get(CommonConstants.CURRENCY_TYPE).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			countryDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		countryDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.STATUS) != null ){
			 			countryDO.setStatus((char) inputJSON.get(CommonConstants.STATUS).toString().charAt(0));
			 		}
			 		if(!countryService.update(countryDO)){
						return CommonWebUtil.buildErrorResponse("Country Already Added").toString();
					}
			 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Country Updated");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/importCountry", method = RequestMethod.POST)
	public @ResponseBody String importCountry(Model model, HttpServletRequest request) {
		try {
				JSONObject inputJSON =  CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
				org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
				JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
				List<CountryDO> countrylist = new ArrayList<CountryDO>();
				Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY);
				for (int i=0; i < fileData.size(); i++){
					JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
						CountryDO countryDO = new CountryDO();
						
						if (colName.get(CommonConstants.NAME) != null && !colName.get(CommonConstants.NAME).toString().isEmpty()){
				 			countryDO.setCountryName(rowJSON.get(colName.get(CommonConstants.NAME)).toString());
				 		}
				 		if (colName.get(CommonConstants.CODE) != null && !colName.get(CommonConstants.CODE).toString().isEmpty()){
				 			countryDO.setCountryCode(rowJSON.get(colName.get(CommonConstants.CODE)).toString());
				 		}
				 		if (colName.get(CommonConstants.CALLING_CODE) != null && !colName.get(CommonConstants.CALLING_CODE).toString().isEmpty()){
				 			countryDO.setCallingCode(rowJSON.get(colName.get(CommonConstants.CALLING_CODE)).toString());
				 		}
				 		if (colName.get(CommonConstants.CURRENCY_TYPE) != null && !colName.get(CommonConstants.CURRENCY_TYPE).toString().isEmpty()){
				 			countryDO.setCurrencyType(rowJSON.get(colName.get(CommonConstants.CURRENCY_TYPE)).toString());
				 		}
				 		if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
				 			countryDO.setUpdatedby(updatedBy.toString());
				 			countryDO.setCreatedby(updatedBy.toString());
				 		}
				 		countryDO.setUpdatedon(new Date());
				 		countryDO.setCreatedon(new Date());
				 		countryDO.setStatus(CommonConstants.ACTIVE);
				 		countrylist.add(countryDO);
				}
				countryService.persistList(countrylist);
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
