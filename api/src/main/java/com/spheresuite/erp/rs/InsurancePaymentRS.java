package com.spheresuite.erp.rs;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.InsurancePaymentDO;
import com.spheresuite.erp.domainobject.InsurancePaymentDetailDO;
import com.spheresuite.erp.domainobject.InsuranceTypeDO;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.InsurancePaymentDetailService;
import com.spheresuite.erp.service.InsurancePaymentService;
import com.spheresuite.erp.service.InsuranceTypeService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.InsurancePaymentUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/insurancepayment")
public class InsurancePaymentRS {

	String validation = null;
	static Logger logger = Logger.getLogger(InsurancePaymentRS.class.getName());

	@Autowired
	private InsurancePaymentService insurancePaymentService;
	
	@Autowired
	private EmployeeService employeeService;
	
	@Autowired
	private InsurancePaymentDetailService insurancePaymentDetailService;
	
	@Autowired
	private InsuranceTypeService insuranceTypeService;
	
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				InsurancePaymentDO insurancePaymentDO = new InsurancePaymentDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		List<EmployeeDO> employeeList = null;
			 		if(inputJSON.get(CommonConstants.EMPID) != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
				 		if(employeeList != null && employeeList.size() > 0){
				 			insurancePaymentDO.setEmployee(employeeList.get(0));
				 			if(inputJSON.get(CommonConstants.TYPE_ID) != null && !inputJSON.get(CommonConstants.TYPE_ID).toString().isEmpty()){
					 			List <InsuranceTypeDO> insuranceTypeList = insuranceTypeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.TYPE_ID).toString()));
						 		if(insuranceTypeList != null && insuranceTypeList.size() > 0){
						 			insurancePaymentDO.setInsuranceType(insuranceTypeList.get(0));
						 			if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
							 			insurancePaymentDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
							 			insurancePaymentDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
							 			if(inputJSON.get(CommonConstants.AMOUNT) != null && !inputJSON.get(CommonConstants.AMOUNT).toString().isEmpty()){
								 			insurancePaymentDO.setAmount(Long.parseLong(inputJSON.get(CommonConstants.AMOUNT).toString()));
								 		}
								 		if(inputJSON.get(CommonConstants.MONTHLY) != null && !inputJSON.get(CommonConstants.MONTHLY).toString().isEmpty()){
								 			insurancePaymentDO.setMonth(inputJSON.get(CommonConstants.MONTHLY).toString());
								 		}
								 		if(inputJSON.get(CommonConstants.NOOFINSTALLMENT) != null && !inputJSON.get(CommonConstants.NOOFINSTALLMENT).toString().isEmpty()){
								 			insurancePaymentDO.setNoOfInstallments(inputJSON.get(CommonConstants.NOOFINSTALLMENT).toString());
								 		}
								 		if(inputJSON.get(CommonConstants.INSTALLMENTAOUNT) != null && !inputJSON.get(CommonConstants.INSTALLMENTAOUNT).toString().isEmpty()){
								 			insurancePaymentDO.setInstallmentAmount(inputJSON.get(CommonConstants.INSTALLMENTAOUNT).toString());
								 		}
								 		insurancePaymentDO.setUpdatedon(new Date());
								 		insurancePaymentDO.setCreatedon(new Date());
								 		insurancePaymentDO.setStatus(CommonConstants.ACTIVESTRING);
								 		InsurancePaymentDO updatedadvancePayment=  insurancePaymentService.persist(insurancePaymentDO);
								 		if(updatedadvancePayment != null){
									 		org.json.simple.JSONArray insuranceDetailList = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.INSURANCEDETAIL).toString());
									 		
									 		//updatedadvancePayment.getAdvancePaymentId()
									 		
									 		for (int j=0; j < insuranceDetailList.size(); j++){
							 					JSONObject monthlyDetail = CommonWebUtil.getInputParams(insuranceDetailList.get(j).toString());
							 					InsurancePaymentDetailDO insurancePaymentDetailDO = new InsurancePaymentDetailDO();
							 					if(monthlyDetail.get(CommonConstants.AMOUNT) != null && !monthlyDetail.get(CommonConstants.AMOUNT).toString().isEmpty()){
							 						insurancePaymentDetailDO.setAmount(monthlyDetail.get(CommonConstants.AMOUNT).toString());
							 					}
							 					if(monthlyDetail.get(CommonConstants.MONTH) != null && !monthlyDetail.get(CommonConstants.MONTH).toString().isEmpty()){
							 						insurancePaymentDetailDO.setMonth(monthlyDetail.get(CommonConstants.MONTH).toString());
							 					}
							 					if(employeeList != null && employeeList.size() > 0){
							 						insurancePaymentDetailDO.setEmployee(employeeList.get(0));
							 					}
							 					insurancePaymentDetailDO.setInsurancePayment(updatedadvancePayment);
							 					insurancePaymentDetailDO.setUpdatedon(new Date());
							 					insurancePaymentDetailDO.setCreatedon(new Date());
							 					insurancePaymentDetailDO.setStatus(CommonConstants.ACTIVESTRING);
							 					
										 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
										 			insurancePaymentDetailDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
										 			insurancePaymentDetailDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
										 		}
										 		
										 		insurancePaymentDetailService.persist(insurancePaymentDetailDO);
										 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Insurance Payment Created For Employee");
									 		}
								 		}
						 			}
						 		}
					 		}
				 		}
			 		}
			 	}
				
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	/*@RequestMapping(value = "/retrieveByEmpId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByEmpId(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<EmployeeBonusDO> employeeAddressList = employeeBonusService.retrieveByEmpId(inputJSON.get(CommonConstants.ID).toString());
					respJSON = EmployeeBonusUtil.getEmployeeBonusList(employeeAddressList, null,null).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}*/
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieveByEmpId(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
					List<InsurancePaymentDO> insurancePayemntList = insurancePaymentService.retrieve();
					respJSON = InsurancePaymentUtil.getInsurancePaymentList(insurancePayemntList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				InsurancePaymentDO insurancePaymentDO = new InsurancePaymentDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<InsurancePaymentDO> insurancePaymentList = insurancePaymentService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		insurancePaymentDO = insurancePaymentList.get(0);
			 		
			 		if(inputJSON.get(CommonConstants.EMPID) != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
				 		if(employeeList != null && employeeList.size() > 0){
				 			insurancePaymentDO.setEmployee(employeeList.get(0));
				 		}
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.AMOUNT) != null && !inputJSON.get(CommonConstants.AMOUNT).toString().isEmpty()){
			 			insurancePaymentDO.setAmount(Long.parseLong(inputJSON.get(CommonConstants.AMOUNT).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.MONTHLY) != null && !inputJSON.get(CommonConstants.MONTHLY).toString().isEmpty()){
			 			insurancePaymentDO.setMonth(inputJSON.get(CommonConstants.MONTHLY).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.NOOFINSTALLMENT) != null && !inputJSON.get(CommonConstants.NOOFINSTALLMENT).toString().isEmpty()){
			 			insurancePaymentDO.setNoOfInstallments(inputJSON.get(CommonConstants.NOOFINSTALLMENT).toString());
			 		}
			 		insurancePaymentDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			insurancePaymentDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		InsurancePaymentDO updatedadvancePayment = insurancePaymentService.update(insurancePaymentDO);
			 		
			 		if(updatedadvancePayment != null){
				 		org.json.simple.JSONArray insuranceDetailList = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.INSURANCEDETAIL).toString());
				 		
				 		//updatedadvancePayment.getAdvancePaymentId()
				 		
				 		for (int j=0; j < insuranceDetailList.size(); j++){
		 					JSONObject monthlyDetail = CommonWebUtil.getInputParams(insuranceDetailList.get(j).toString());
		 					InsurancePaymentDetailDO insuracePaymentDetailDO = new InsurancePaymentDetailDO();
		 					
		 					if(monthlyDetail.get(CommonConstants.ID) != null && !monthlyDetail.get(CommonConstants.ID).toString().isEmpty()){
		 						List <InsurancePaymentDetailDO> detailList = insurancePaymentDetailService.getById(Long.parseLong(monthlyDetail.get(CommonConstants.ID).toString()));
		 			 				if(detailList != null && detailList.size() > 0){
		 			 					insuracePaymentDetailDO = detailList.get(0);
		 			 					
		 			 					if(monthlyDetail.get(CommonConstants.AMOUNT) != null && !monthlyDetail.get(CommonConstants.AMOUNT).toString().isEmpty()){
		 			 						insuracePaymentDetailDO.setAmount(monthlyDetail.get(CommonConstants.AMOUNT).toString());
		 			 					}
		 			 					
		 			 					if(monthlyDetail.get(CommonConstants.MONTH) != null && !monthlyDetail.get(CommonConstants.MONTH).toString().isEmpty()){
		 			 						insuracePaymentDetailDO.setMonth(monthlyDetail.get(CommonConstants.MONTH).toString());
		 			 					}
		 			 					insuracePaymentDetailDO.setUpdatedon(new Date());
		 			 					
		 			 					if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
		 			 						insuracePaymentDetailDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
		 						 		}
		 						 		
		 			 					insurancePaymentDetailService.update(insuracePaymentDetailDO);
		 			 				}
		 					}else{
		 						if(monthlyDetail.get(CommonConstants.AMOUNT) != null && !monthlyDetail.get(CommonConstants.AMOUNT).toString().isEmpty()){
		 							insuracePaymentDetailDO.setAmount(monthlyDetail.get(CommonConstants.AMOUNT).toString());
			 					}
			 					if(monthlyDetail.get(CommonConstants.MONTH) != null && !monthlyDetail.get(CommonConstants.MONTH).toString().isEmpty()){
			 						insuracePaymentDetailDO.setMonth(monthlyDetail.get(CommonConstants.MONTH).toString());
			 					}
			 					if(inputJSON.get(CommonConstants.EMPID) != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 						List <EmployeeDO>	employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
							 		if(employeeList != null && employeeList.size() > 0){
							 			insuracePaymentDetailDO.setEmployee(employeeList.get(0));
							 		}
						 		}
			 					insuracePaymentDetailDO.setInsurancePayment(updatedadvancePayment);
			 					insuracePaymentDetailDO.setUpdatedon(new Date());
			 					insuracePaymentDetailDO.setCreatedon(new Date());
			 					insuracePaymentDetailDO.setStatus(CommonConstants.ACTIVESTRING);
			 					
						 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
						 			insuracePaymentDetailDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
						 			insuracePaymentDetailDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
						 		}
						 		insurancePaymentDetailService.persist(insuracePaymentDetailDO);
		 					}
		 					
				 		}
			 		}
			 		
			 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Advance Payemnt Updated For Employee");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	/*@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public @ResponseBody String delete(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		Boolean employeeAddressList = employeeBonusService.delete(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(employeeAddressList){
			 			CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Bonus Deleted For Employee");
			 		}else{
			 			return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retriveForBonusReport/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveForBonusReport(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.MONTHLY).toString().isEmpty()){
					Long batchId = null;
					List<Long> ids = new ArrayList<Long>();
					if(!inputJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty()){
						batchId = Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString());
						if(batchId == 0){
							batchId = null;
						}else{
							ids = employeeCtcService.retrieveByBatchId(batchId);
						}
					}
					List<EmployeeBonusDO> bonusList = employeeBonusService.retrieveByEmpIdWithDate(ids, inputJSON.get(CommonConstants.MONTHLY).toString());
					respJSON = EmployeeBonusUtil.getEmployeeBonusList(bonusList, CommonConstants.MONTHLYC, inputJSON.get(CommonConstants.MONTHLY).toString()).toString();
				}else{
					if(inputJSON != null && !inputJSON.get(CommonConstants.QUARTERLY).toString().isEmpty()){
						String fromMonth = null;
						String toMonth = null;
						Long batchId = null;
						List<FyDO> fyList = new ArrayList<FyDO>();
						if(!inputJSON.get(CommonConstants.FYID).toString().isEmpty()){
							fyList = fyService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.FYID).toString()));
							if(fyList != null && fyList.size() > 0){
								List<Long> ids = new ArrayList<Long>();
								if(!inputJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty()){
									batchId = Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString());
									if(batchId == 0){
										batchId = null;
									}else{
										ids = employeeCtcService.retrieveByBatchId(batchId);
									}
								}
								if(inputJSON.get(CommonConstants.QUARTERLY).toString().equalsIgnoreCase("Q1")){
									fromMonth = "Apr"+" "+fyList.get(0).getFromyear();
									toMonth = "Jun"+" "+fyList.get(0).getFromyear(); 
									List<EmployeeBonusDO> bonusList = employeeBonusService.retrieveByEmpIdBetweenDate(ids,fromMonth, toMonth);
									if(bonusList != null && bonusList.size() > 0){
										respJSON = EmployeeBonusUtil.MultipleMonthBonuReport(bonusList,CommonConstants.QUARTERLYC, fromMonth+" - "+toMonth).toString();
									}
								}
								
								if(inputJSON.get(CommonConstants.QUARTERLY).toString().equalsIgnoreCase("Q2")){
									fromMonth = "Jul"+" "+fyList.get(0).getFromyear();
									toMonth = "Sep"+" "+fyList.get(0).getFromyear(); 
									List<EmployeeBonusDO> bonusList = employeeBonusService.retrieveByEmpIdBetweenDate(ids,fromMonth, toMonth);
									if(bonusList != null && bonusList.size() > 0){
										respJSON = EmployeeBonusUtil.MultipleMonthBonuReport(bonusList,CommonConstants.QUARTERLYC, fromMonth+" - "+toMonth).toString();
									}
								}
								
								
								if(inputJSON.get(CommonConstants.QUARTERLY).toString().equalsIgnoreCase("Q3")){
									fromMonth = "Oct"+" "+fyList.get(0).getFromyear();
									toMonth = "Dec"+" "+fyList.get(0).getFromyear(); 
									List<EmployeeBonusDO> bonusList = employeeBonusService.retrieveByEmpIdBetweenDate(ids,fromMonth, toMonth);
									if(bonusList != null && bonusList.size() > 0){
										respJSON = EmployeeBonusUtil.MultipleMonthBonuReport(bonusList,CommonConstants.QUARTERLYC, fromMonth+" - "+toMonth).toString();
									}
								}
								if(inputJSON.get(CommonConstants.QUARTERLY).toString().equalsIgnoreCase("Q4")){
									fromMonth = "Jan"+" "+fyList.get(0).getToyear();
									toMonth = "Mar"+" "+fyList.get(0).getToyear(); 
									List<EmployeeBonusDO> bonusList = employeeBonusService.retrieveByEmpIdBetweenDate(ids,fromMonth, toMonth);
									if(bonusList != null && bonusList.size() > 0){
										respJSON = EmployeeBonusUtil.MultipleMonthBonuReport(bonusList,CommonConstants.QUARTERLYC, fromMonth+" - "+toMonth).toString();
									}
								}
							}
							
						}
					}else{
						if(inputJSON != null && !inputJSON.get(CommonConstants.FREQUENCY).toString().isEmpty()
								&& inputJSON.get(CommonConstants.FREQUENCY).toString().equalsIgnoreCase("Y")){
							String fromMonth = null;
							String toMonth = null;
							Long batchId = null;
							List<FyDO> fyList = new ArrayList<FyDO>();
							if(!inputJSON.get(CommonConstants.FYID).toString().isEmpty()){
								fyList = fyService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.FYID).toString()));
								if(fyList != null && fyList.size() > 0){
									List<Long> ids = new ArrayList<Long>();
									if(!inputJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty()){
										batchId = Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString());
										if(batchId == 0){
											batchId = null;
										}else{
											ids = employeeCtcService.retrieveByBatchId(batchId);
										}
									}
									fromMonth = "Apr"+" "+fyList.get(0).getFromyear();
									toMonth = "Mar"+" "+fyList.get(0).getToyear(); 
									List<EmployeeBonusDO> bonusList = employeeBonusService.retrieveByEmpIdBetweenDate(ids,fromMonth, toMonth);
									if(bonusList != null && bonusList.size() > 0){
										respJSON = EmployeeBonusUtil.MultipleMonthBonuReport(bonusList,CommonConstants.YEARLYC, fromMonth+" - "+toMonth).toString();
									}
								}
							}
						}
					}
				}
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}*/
}
