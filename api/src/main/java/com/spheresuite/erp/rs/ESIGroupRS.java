package com.spheresuite.erp.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.ESIGroupDO;
import com.spheresuite.erp.domainobject.PFGroupDO;
import com.spheresuite.erp.service.ESIGroupService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.ESIGroupUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/esigroup")
public class ESIGroupRS {

	String validation = null;
	static Logger logger = Logger.getLogger(ESIGroupRS.class.getName());
	
	@Autowired
	private ESIGroupService esiGroupService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				ESIGroupDO esiGroupDO = new ESIGroupDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
			 			esiGroupDO.setName(inputJSON.get(CommonConstants.NAME).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.AMOUNT) != null && !inputJSON.get(CommonConstants.AMOUNT).toString().isEmpty()){
			 			esiGroupDO.setAmount(Double.parseDouble(inputJSON.get(CommonConstants.AMOUNT).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.DESCRIPTION) != null && !inputJSON.get(CommonConstants.DESCRIPTION).toString().isEmpty()){
			 			esiGroupDO.setDescription(inputJSON.get(CommonConstants.DESCRIPTION).toString());
			 		}
			 		esiGroupDO.setStatus(CommonConstants.ACTIVE);
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			esiGroupDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			esiGroupDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		esiGroupDO.setUpdatedon(new Date());
			 		esiGroupDO.setCreatedon(new Date());
			 	}
				if(!esiGroupService.persist(esiGroupDO)){
		 			return CommonWebUtil.buildErrorResponse("ESI Group Already Added").toString();
				}
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New ESI Group Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				ESIGroupDO esiGroupDO = new ESIGroupDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<ESIGroupDO> esiGroupList = esiGroupService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(esiGroupList != null && esiGroupList.size() >0){
			 			esiGroupDO = esiGroupList.get(0);
				 		if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
				 			esiGroupDO.setName(inputJSON.get(CommonConstants.NAME).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.DESCRIPTION) != null && !inputJSON.get(CommonConstants.DESCRIPTION).toString().isEmpty()){
				 			esiGroupDO.setDescription(inputJSON.get(CommonConstants.DESCRIPTION).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.AMOUNT) != null && !inputJSON.get(CommonConstants.AMOUNT).toString().isEmpty()){
				 			esiGroupDO.setAmount(Double.parseDouble(inputJSON.get(CommonConstants.AMOUNT).toString()));
				 		}
				 		if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
				 			esiGroupDO.setStatus((char) inputJSON.get(CommonConstants.STATUS).toString().charAt(0));
				 		}
				 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			esiGroupDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		esiGroupDO.setUpdatedon(new Date());
				 		if(!esiGroupService.update(esiGroupDO)){
				 			return CommonWebUtil.buildErrorResponse("ESI Group Already Added").toString();
				 		}
				 		CommonUtil.persistRecentAcvtivity(inputJSON.get(CommonConstants.UPDATED_BY).toString(), "ESI Group Updated");
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	@RequestMapping(value = "/retrieveActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<ESIGroupDO> pfGroupList = esiGroupService.retrieveActive();
				//respJSON = payrollGroupList
				respJSON = ESIGroupUtil.getESIGroupListDetails(pfGroupList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<ESIGroupDO> pfGroupList = esiGroupService.retrieve();
				respJSON = ESIGroupUtil.getESIGroupListDetails(pfGroupList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/importESIGroup", method = RequestMethod.POST)
	public @ResponseBody String importESIGroup(Model model, HttpServletRequest request) {
		try {
			JSONObject inputJSON =  CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
			JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
			List<ESIGroupDO> esiGroupList = new ArrayList<ESIGroupDO>();
			Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY);
			for (int i=0; i < fileData.size(); i++){
				JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
				ESIGroupDO esiGroupDO = new ESIGroupDO();
				
				if (colName.get(CommonConstants.NAME) != null && !colName.get(CommonConstants.NAME).toString().isEmpty()){
					esiGroupDO.setName(rowJSON.get(colName.get(CommonConstants.NAME)).toString());
		 		}
				if(colName.get(CommonConstants.AMOUNT) != null && !colName.get(CommonConstants.AMOUNT).toString().isEmpty()){
		 			esiGroupDO.setAmount(Double.parseDouble(rowJSON.get(colName.get(CommonConstants.AMOUNT)).toString()));
		 		}
				if(colName.get(CommonConstants.DESCRIPTION) != null && !colName.get(CommonConstants.DESCRIPTION).toString().isEmpty()){
					esiGroupDO.setDescription(rowJSON.get(colName.get(CommonConstants.DESCRIPTION)).toString());
		 		}
		 		if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
		 			esiGroupDO.setUpdatedby(updatedBy.toString());
		 			esiGroupDO.setCreatedby(updatedBy.toString());
		 		}
		 		esiGroupDO.setUpdatedon(new Date());
		 		esiGroupDO.setCreatedon(new Date());
		 		esiGroupDO.setStatus(CommonConstants.ACTIVE);
		 		esiGroupList.add(esiGroupDO);
			}
			esiGroupService.persistList(esiGroupList);
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
