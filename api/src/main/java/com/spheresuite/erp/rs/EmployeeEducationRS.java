package com.spheresuite.erp.rs;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.CountryDO;
import com.spheresuite.erp.domainobject.EducationLevelDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.EmployeeEducationDO;
import com.spheresuite.erp.domainobject.StateDO;
import com.spheresuite.erp.service.CountryService;
import com.spheresuite.erp.service.EducationLevelService;
import com.spheresuite.erp.service.EmployeeEducationService;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.StateService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.EmployeeEducationUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/employeeeducation")
public class EmployeeEducationRS {

	String validation = null;
	static Logger logger = Logger.getLogger(EmployeeEducationRS.class.getName());

	@Autowired
	private EmployeeEducationService employeeEducationService;
	
	@Autowired
	private CountryService countryService;
	
	@Autowired
	private StateService stateService;
	
	@Autowired
	private EmployeeService employeeService;
	
	@Autowired
	private EducationLevelService educationLevelService;
	
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeEducationDO employeeEducationDO = new EmployeeEducationDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.COUNTRY_ID) != null && !inputJSON.get(CommonConstants.COUNTRY_ID).toString().isEmpty()){
				 		List<CountryDO> countryList = countryService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.COUNTRY_ID).toString()));
			 			if(countryList != null && countryList.size() > 0){
			 				employeeEducationDO.setCountry(countryList.get(0));
			 				if(inputJSON.get(CommonConstants.STATE_ID) != null && !inputJSON.get(CommonConstants.STATE_ID).toString().isEmpty()){
						 		List<StateDO> stateList = stateService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.STATE_ID).toString()));
						 		if(stateList != null && stateList.size() >0){
						 			employeeEducationDO.setState(stateList.get(0));
						 			if(inputJSON.get(CommonConstants.CITY) != null && !inputJSON.get(CommonConstants.CITY).toString().isEmpty()){
							 			employeeEducationDO.setCity(inputJSON.get(CommonConstants.CITY).toString());
							 			if(inputJSON.get(CommonConstants.EMPID).toString() != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
								 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
									 		if(employeeList != null && employeeList.size() > 0){
									 			employeeEducationDO.setEmployee(employeeList.get(0));
									 			if(inputJSON.get(CommonConstants.EDUCATIONLEVELID) != null && !inputJSON.get(CommonConstants.EDUCATIONLEVELID).toString().isEmpty()){
											 		List<EducationLevelDO> educationLevelList = educationLevelService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.EDUCATIONLEVELID).toString()));
											 		if(educationLevelList != null && educationLevelList.size() >0){
											 			employeeEducationDO.setEducationLevel(educationLevelList.get(0));
											 			if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
												 			employeeEducationDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
												 			employeeEducationDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
												 			if(inputJSON.get(CommonConstants.AREA) != null && !inputJSON.get(CommonConstants.AREA).toString().isEmpty()){
													 			employeeEducationDO.setArea(inputJSON.get(CommonConstants.AREA).toString());
													 		}
													 		if(inputJSON.get(CommonConstants.UNIVERSITY) != null && !inputJSON.get(CommonConstants.UNIVERSITY).toString().isEmpty()){
													 			employeeEducationDO.setUniversity(inputJSON.get(CommonConstants.UNIVERSITY).toString());
													 		}
													 		if(inputJSON.get(CommonConstants.YEAR) != null && !inputJSON.get(CommonConstants.YEAR).toString().isEmpty()){
													 			employeeEducationDO.setYearPassed(inputJSON.get(CommonConstants.YEAR).toString());
													 		}
													 		employeeEducationDO.setUpdatedon(new Date());
													 		employeeEducationDO.setCreatedon(new Date());
													 		employeeEducationService.persist(employeeEducationDO);
															CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Employee Education Created");
												 		}
											 		}
										 		}
									 		}
								 		}
							 		}
						 		}
					 		}
			 			}
			 		}
			 	}
				
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retrieveByEmpId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByEmpId(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<EmployeeEducationDO> employeeEducationList = employeeEducationService.retrieveByEmpId(inputJSON.get(CommonConstants.ID).toString());
					respJSON = EmployeeEducationUtil.getEmployeeEducationList(employeeEducationList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeEducationDO employeeEducationDO = new EmployeeEducationDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<EmployeeEducationDO> employeeEducationList = employeeEducationService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		employeeEducationDO = employeeEducationList.get(0);
			 		
			 		if(inputJSON.get(CommonConstants.CITY) != null && !inputJSON.get(CommonConstants.CITY).toString().isEmpty()){
			 			employeeEducationDO.setCity(inputJSON.get(CommonConstants.CITY).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.COUNTRY_ID) != null && !inputJSON.get(CommonConstants.COUNTRY_ID).toString().isEmpty()){
				 		List<CountryDO> countryList = countryService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.COUNTRY_ID).toString()));
			 			if(countryList != null && countryList.size() > 0){
			 				employeeEducationDO.setCountry(countryList.get(0));
			 			}
			 		}
			 		if(inputJSON.get(CommonConstants.STATE_ID) != null && !inputJSON.get(CommonConstants.STATE_ID).toString().isEmpty()){
				 		List<StateDO> stateList = stateService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.STATE_ID).toString()));
				 		if(stateList != null && stateList.size() >0){
				 			employeeEducationDO.setState(stateList.get(0));
				 		}
			 		}
			 		if(inputJSON.get(CommonConstants.EMPID).toString() != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
				 		if(employeeList != null && employeeList.size() > 0){
				 			employeeEducationDO.setEmployee(employeeList.get(0));
				 		}
			 		}
			 		if(inputJSON.get(CommonConstants.AREA) != null && !inputJSON.get(CommonConstants.AREA).toString().isEmpty()){
			 			employeeEducationDO.setArea(inputJSON.get(CommonConstants.AREA).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.UNIVERSITY) != null && !inputJSON.get(CommonConstants.UNIVERSITY).toString().isEmpty()){
			 			employeeEducationDO.setUniversity(inputJSON.get(CommonConstants.UNIVERSITY).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.YEAR) != null && !inputJSON.get(CommonConstants.YEAR).toString().isEmpty()){
			 			employeeEducationDO.setYearPassed(inputJSON.get(CommonConstants.YEAR).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.EDUCATIONLEVELID) != null && !inputJSON.get(CommonConstants.EDUCATIONLEVELID).toString().isEmpty()){
				 		List<EducationLevelDO> educationLevelList = educationLevelService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.EDUCATIONLEVELID).toString()));
				 		if(educationLevelList != null && educationLevelList.size() >0){
				 			employeeEducationDO.setEducationLevel(educationLevelList.get(0));
				 		}
			 		}
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			employeeEducationDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		employeeEducationDO.setUpdatedon(new Date());
			 		employeeEducationService.update(employeeEducationDO);
			 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Employee Education Updated");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
