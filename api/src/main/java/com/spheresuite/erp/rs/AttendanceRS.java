package com.spheresuite.erp.rs;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.AttendanceDO;
import com.spheresuite.erp.domainobject.EmployeeCompOffDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.EmployeeLeavegroupDO;
import com.spheresuite.erp.domainobject.EmployeeLopDO;
import com.spheresuite.erp.domainobject.LeaveManagementDO;
import com.spheresuite.erp.domainobject.LeaveRequestsDO;
import com.spheresuite.erp.service.AttendanceService;
import com.spheresuite.erp.service.EmployeeCompOffService;
import com.spheresuite.erp.service.EmployeeLeavegroupService;
import com.spheresuite.erp.service.EmployeeLopService;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.LeaveManagementService;
import com.spheresuite.erp.service.LeaveRequestsService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.AttendanceUtil;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/attendance")
public class AttendanceRS {

	String validation = null;
	static Logger logger = Logger.getLogger(AttendanceRS.class.getName());

	@Autowired
	private AttendanceService attendanceService;
	
	@Autowired
	private LeaveRequestsService leaveRequestsService;
	
	@Autowired
	private EmployeeLeavegroupService employeeleaveGroupService;
	
	@Autowired
	private EmployeeService employeeService;
	
	@Autowired
	private EmployeeCompOffService employeeCompOffService;
	
	@Autowired
	private LeaveManagementService leaveManagementService;
	
	@Autowired
	private EmployeeLopService employeeLopService;
	
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				AttendanceDO attendanceDO = new AttendanceDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		
			 		if(inputJSON.get(CommonConstants.EMPID) != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
				 		if(employeeList != null && employeeList.size() > 0){
				 			attendanceDO.setEmployee(employeeList.get(0));
				 			if(inputJSON.get(CommonConstants.TIMEIN) != null && !inputJSON.get(CommonConstants.TIMEIN).toString().isEmpty()){
				 				attendanceDO.setTimeIn(inputJSON.get(CommonConstants.TIMEIN).toString());
					 		}
				 			if(inputJSON.get(CommonConstants.TIMEOUT) != null && !inputJSON.get(CommonConstants.TIMEOUT).toString().isEmpty()){
				 				attendanceDO.setTimeOut(inputJSON.get(CommonConstants.TIMEOUT).toString());
					 		}
				 			if(inputJSON.get(CommonConstants.TIMEINDATE) != null  && !inputJSON.get(CommonConstants.TIMEINDATE).toString().isEmpty()){
				 				attendanceDO.setTimeInDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.TIMEINDATE).toString()));
				 			}
				 			if(inputJSON.get(CommonConstants.TIMEOUTDATE) != null  && !inputJSON.get(CommonConstants.TIMEOUTDATE).toString().isEmpty()){
				 				attendanceDO.setTimeOutDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.TIMEOUTDATE).toString()));
				 			}
				 			if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			attendanceDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			attendanceDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 		}
				 			
				 			if(inputJSON.get(CommonConstants.ATTENDANCETYPE) != null ){
				 				attendanceDO.setAttendanceType(inputJSON.get(CommonConstants.ATTENDANCETYPE).toString());
					 		}
				 			
				 			
				 			
					 		attendanceDO.setUpdatedon(new Date());
					 		attendanceDO.setCreatedon(new Date());
					 		if(!attendanceService.persist(attendanceDO)){
								return CommonWebUtil.buildErrorResponse("Attendance Added").toString();
							}
				 		}else{
				 			return CommonWebUtil.buildErrorResponse("Employee not available").toString();
				 		}
			 		}else{
			 			return CommonWebUtil.buildErrorResponse("Employee not available").toString();
			 		}
			 	}
			 	
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Attendance Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		
			 		if(inputJSON.get(CommonConstants.EMPID) != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
				 		if(employeeList != null && employeeList.size() > 0){
				 			AttendanceDO attendanceDO = new AttendanceDO();
				 			if(inputJSON.get(CommonConstants.ID) != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
				 				List<AttendanceDO> attendanceList = attendanceService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
				 				if(attendanceList != null && attendanceList.size() > 0){
				 					attendanceDO = attendanceList.get(0);
				 				}
				 			}else{
				 				attendanceDO = new AttendanceDO();
				 			}
				 			attendanceDO.setEmployee(employeeList.get(0));
				 			
				 			if(inputJSON.get(CommonConstants.TIMEINDATE) != null  && !inputJSON.get(CommonConstants.TIMEINDATE).toString().isEmpty()){
				 				attendanceDO.setTimeInDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.TIMEINDATE).toString()));
				 			}
				 			if(inputJSON.get(CommonConstants.TIMEOUTDATE) != null  && !inputJSON.get(CommonConstants.TIMEOUTDATE).toString().isEmpty()){
				 				attendanceDO.setTimeOutDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.TIMEOUTDATE).toString()));
				 			}
				 			
				 			if(inputJSON.get(CommonConstants.TIMEIN) != null && !inputJSON.get(CommonConstants.TIMEIN).toString().isEmpty()){
				 				attendanceDO.setTimeIn(inputJSON.get(CommonConstants.TIMEIN).toString());
					 		}
				 			if(inputJSON.get(CommonConstants.TIMEOUT) != null && !inputJSON.get(CommonConstants.TIMEOUT).toString().isEmpty()){
				 				attendanceDO.setTimeOut(inputJSON.get(CommonConstants.TIMEOUT).toString());
					 		}
				 			if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			attendanceDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			attendanceDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 		}
				 			
				 			if(inputJSON.get(CommonConstants.ATTENDANCETYPE) != null ){
				 				attendanceDO.setAttendanceType(inputJSON.get(CommonConstants.ATTENDANCETYPE).toString());
				 				if(attendanceDO.getAttendanceType().equals("lpf") || attendanceDO.getAttendanceType().equals("lph")){
				 					//for leave present
				 					LeaveRequestsDO leaveRequestDO = new LeaveRequestsDO(); 
						 				String type = "";
						 				if(attendanceDO.getAttendanceType().charAt(2) == 'f'){
						 					type="f";
						 				}else{
						 					type="h";
						 				}
						 				leaveRequestDO.setType(type);
						 				leaveRequestDO.setStatus('a');
						 				
					 				if(inputJSON.get(CommonConstants.TYPE_ID) != null && !inputJSON.get(CommonConstants.TYPE_ID).toString().isEmpty()){
							 			//leaveRequestDO.setLeaveType(inputJSON.get(CommonConstants.TYPE_ID).toString());
							 			List<LeaveManagementDO> leaveTypeList = leaveManagementService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.TYPE_ID).toString()));
								 		if(leaveTypeList != null && leaveTypeList.size() > 0){
								 			leaveRequestDO.setLeaveManagement(leaveTypeList.get(0));
								 		}
							 		}

						 			if(inputJSON.get(CommonConstants.TIMEINDATE) != null  && !inputJSON.get(CommonConstants.TIMEINDATE).toString().isEmpty()){
						 				leaveRequestDO.setFromDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.TIMEINDATE).toString()));
						 			}
						 			if(inputJSON.get(CommonConstants.TIMEOUTDATE) != null  && !inputJSON.get(CommonConstants.TIMEOUTDATE).toString().isEmpty()){
						 				leaveRequestDO.setToDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.TIMEOUTDATE).toString()));
						 			}
						 			if(inputJSON.get(CommonConstants.LEAVETYPEFROMDATE) != null  && !inputJSON.get(CommonConstants.LEAVETYPEFROMDATE).toString().isEmpty()){
						 				leaveRequestDO.setLeavetypefromDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.LEAVETYPEFROMDATE).toString()));
						 				//leaveRequestDO.setLeavetypefromDate(inputJSON.get(CommonConstants.LEAVETYPEFROMDATE).toString());
						 			}
						 			if(inputJSON.get(CommonConstants.LEAVETYPETODATE) != null  && !inputJSON.get(CommonConstants.LEAVETYPETODATE).toString().isEmpty()){
						 				leaveRequestDO.setLeavetypeToDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.LEAVETYPETODATE).toString()));
						 				//leaveRequestDO.setLeavetypeToDate(inputJSON.get(CommonConstants.LEAVETYPETODATE).toString());
						 			}
						 			
						 			leaveRequestDO.setEmployee(employeeList.get(0));
					 				List<EmployeeLeavegroupDO> leaveGroupList = employeeleaveGroupService.retrieveByEmpId(employeeList.get(0).getEmpId().toString());
					 				if(leaveGroupList != null && leaveGroupList.size() > 0){
					 					leaveRequestDO.setLeaveGroup(leaveGroupList.get(0).getLeaveGroup());
					 				}
					 				if(employeeList.get(0).getReportto() != null){
						 				List<EmployeeDO> employeeListReportTo = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
								 		if(employeeListReportTo != null && employeeListReportTo.size() > 0 && employeeListReportTo.get(0).getCompanyemail() != null){
								 			leaveRequestDO.setReportingTo(employeeListReportTo.get(0).getCompanyemail());
								 		}
							 		}
					 				
						 			leaveRequestDO.setUpdatedon(new Date());
							 		leaveRequestDO.setCreatedon(new Date());
							 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
							 			leaveRequestDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
							 			leaveRequestDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
							 		}
						 			leaveRequestsService.persist(leaveRequestDO);
				 					
				 				}else if(attendanceDO.getAttendanceType().equals("hlhc")){
				 					//for leave present
				 					LeaveRequestsDO leaveRequestDO = new LeaveRequestsDO(); 
						 				String type = "";
						 				/*if(attendanceDO.getAttendanceType().charAt(1) == 'l'){
						 					type="h";
						 				}else{
						 					type="ch";
						 				}
						 				if(type)*/
						 				leaveRequestDO.setType("h");
						 				leaveRequestDO.setStatus('a');
						 				
					 				if(inputJSON.get(CommonConstants.TYPE_ID) != null && !inputJSON.get(CommonConstants.TYPE_ID).toString().isEmpty()){
							 			//leaveRequestDO.setLeaveType(inputJSON.get(CommonConstants.TYPE_ID).toString());
							 			List<LeaveManagementDO> leaveTypeList = leaveManagementService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.TYPE_ID).toString()));
								 		if(leaveTypeList != null && leaveTypeList.size() > 0){
								 			leaveRequestDO.setLeaveManagement(leaveTypeList.get(0));
								 		}
							 		}

						 			if(inputJSON.get(CommonConstants.TIMEINDATE) != null  && !inputJSON.get(CommonConstants.TIMEINDATE).toString().isEmpty()){
						 				leaveRequestDO.setFromDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.TIMEINDATE).toString()));
						 			}
						 			if(inputJSON.get(CommonConstants.TIMEOUTDATE) != null  && !inputJSON.get(CommonConstants.TIMEOUTDATE).toString().isEmpty()){
						 				leaveRequestDO.setToDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.TIMEOUTDATE).toString()));
						 			}
						 			if(inputJSON.get(CommonConstants.LEAVETYPEFROMDATE) != null  && !inputJSON.get(CommonConstants.LEAVETYPEFROMDATE).toString().isEmpty()){
						 				leaveRequestDO.setLeavetypefromDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.LEAVETYPEFROMDATE).toString()));
						 				//leaveRequestDO.setLeavetypefromDate(inputJSON.get(CommonConstants.LEAVETYPEFROMDATE).toString());
						 			}
						 			if(inputJSON.get(CommonConstants.LEAVETYPETODATE) != null  && !inputJSON.get(CommonConstants.LEAVETYPETODATE).toString().isEmpty()){
						 				leaveRequestDO.setLeavetypeToDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.LEAVETYPETODATE).toString()));
						 				//leaveRequestDO.setLeavetypeToDate(inputJSON.get(CommonConstants.LEAVETYPETODATE).toString());
						 			}
						 			
						 			leaveRequestDO.setEmployee(employeeList.get(0));
					 				List<EmployeeLeavegroupDO> leaveGroupList = employeeleaveGroupService.retrieveByEmpId(employeeList.get(0).getEmpId().toString());
					 				if(leaveGroupList != null && leaveGroupList.size() > 0){
					 					leaveRequestDO.setLeaveGroup(leaveGroupList.get(0).getLeaveGroup());
					 				}
					 				if(employeeList.get(0).getReportto() != null){
						 				List<EmployeeDO> employeeListReportTo = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
								 		if(employeeListReportTo != null && employeeListReportTo.size() > 0 && employeeListReportTo.get(0).getCompanyemail() != null){
								 			leaveRequestDO.setReportingTo(employeeListReportTo.get(0).getCompanyemail());
								 		}
							 		}
					 				
						 			leaveRequestDO.setUpdatedon(new Date());
							 		leaveRequestDO.setCreatedon(new Date());
							 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
							 			leaveRequestDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
							 			leaveRequestDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
							 		}
						 			leaveRequestsService.persist(leaveRequestDO);
						 			//comp off
						 			type = "ch";
						 			leaveRequestDO = new LeaveRequestsDO(); 
					 				leaveRequestDO.setType(type);
					 				leaveRequestDO.setStatus('a');
					 				
						 			if(inputJSON.get(CommonConstants.TIMEINDATE) != null  && !inputJSON.get(CommonConstants.TIMEINDATE).toString().isEmpty()){
						 				leaveRequestDO.setFromDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.TIMEINDATE).toString()));
						 			}
						 			if(inputJSON.get(CommonConstants.TYPE_ID_1) != null && !inputJSON.get(CommonConstants.TYPE_ID_1).toString().isEmpty()){
							 			//leaveRequestDO.setLeaveType(inputJSON.get(CommonConstants.TYPE_ID).toString());
							 			List<LeaveManagementDO> leaveTypeList = leaveManagementService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.TYPE_ID_1).toString()));
								 		if(leaveTypeList != null && leaveTypeList.size() > 0){
								 			leaveRequestDO.setLeaveManagement(leaveTypeList.get(0));
								 		}
							 		}
						 			leaveGroupList = employeeleaveGroupService.retrieveByEmpId(employeeList.get(0).getEmpId().toString());
					 				if(leaveGroupList != null && leaveGroupList.size() > 0){
					 					leaveRequestDO.setLeaveGroup(leaveGroupList.get(0).getLeaveGroup());
					 				}
					 				
						 			leaveRequestDO.setEmployee(employeeList.get(0));
					 				if(employeeList.get(0).getReportto() != null){
						 				List<EmployeeDO> employeeListReportTo = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
								 		if(employeeListReportTo != null && employeeListReportTo.size() > 0 && employeeListReportTo.get(0).getCompanyemail() != null){
								 			leaveRequestDO.setReportingTo(employeeListReportTo.get(0).getCompanyemail());
								 		}
							 		}
						 			leaveRequestDO.setUpdatedon(new Date());
							 		leaveRequestDO.setCreatedon(new Date());
						 			leaveRequestsService.persist(leaveRequestDO);
						 			
						 			List <EmployeeCompOffDO> empCompOffList =  employeeCompOffService.retrieveByEmpCompOffWithTypeExpiry(leaveRequestDO.getEmployee().getEmpId(),leaveRequestDO.getFromDate(),leaveRequestDO.getType().charAt(1));
				 					if(empCompOffList != null && empCompOffList.size()>0){
				 						EmployeeCompOffDO empCompOff =    empCompOffList.get(0);
				 						empCompOff.setStatus(CommonConstants.INACTIVE);
				 						employeeCompOffService.update(empCompOff);
				 					}
				 				}else if(attendanceDO.getAttendanceType().equals("cf") || attendanceDO.getAttendanceType().equals("ch")){
				 					LeaveRequestsDO leaveRequestDO = new LeaveRequestsDO(); 
					 				leaveRequestDO.setType(attendanceDO.getAttendanceType());
					 				leaveRequestDO.setStatus('a');
					 				
						 			if(inputJSON.get(CommonConstants.TIMEINDATE) != null  && !inputJSON.get(CommonConstants.TIMEINDATE).toString().isEmpty()){
						 				leaveRequestDO.setFromDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.TIMEINDATE).toString()));
						 			}
						 			
						 			List<EmployeeLeavegroupDO> leaveGroupList = employeeleaveGroupService.retrieveByEmpId(employeeList.get(0).getEmpId().toString());
					 				if(leaveGroupList != null && leaveGroupList.size() > 0){
					 					leaveRequestDO.setLeaveGroup(leaveGroupList.get(0).getLeaveGroup());
					 				}
					 				
					 				if(inputJSON.get(CommonConstants.TYPE_ID) != null && !inputJSON.get(CommonConstants.TYPE_ID).toString().isEmpty()){
							 			//leaveRequestDO.setLeaveType(inputJSON.get(CommonConstants.TYPE_ID).toString());
							 			List<LeaveManagementDO> leaveTypeList = leaveManagementService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.TYPE_ID).toString()));
								 		if(leaveTypeList != null && leaveTypeList.size() > 0){
								 			leaveRequestDO.setLeaveManagement(leaveTypeList.get(0));
								 		}
							 		}
					 				
						 			leaveRequestDO.setEmployee(employeeList.get(0));
					 				if(employeeList.get(0).getReportto() != null){
						 				List<EmployeeDO> employeeListReportTo = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
								 		if(employeeListReportTo != null && employeeListReportTo.size() > 0 && employeeListReportTo.get(0).getCompanyemail() != null){
								 			leaveRequestDO.setReportingTo(employeeListReportTo.get(0).getCompanyemail());
								 		}
							 		}
						 			leaveRequestDO.setUpdatedon(new Date());
							 		leaveRequestDO.setCreatedon(new Date());
						 			leaveRequestsService.persist(leaveRequestDO);
						 			
						 			List <EmployeeCompOffDO> empCompOffList =  employeeCompOffService.retrieveByEmpCompOffWithTypeExpiry(leaveRequestDO.getEmployee().getEmpId(),leaveRequestDO.getFromDate(),leaveRequestDO.getType().charAt(1));
				 					if(empCompOffList != null && empCompOffList.size()>0){
				 						EmployeeCompOffDO empCompOff =    empCompOffList.get(0);
				 						empCompOff.setStatus(CommonConstants.INACTIVE);
				 						employeeCompOffService.update(empCompOff);
				 					}
				 				}else if(attendanceDO.getAttendanceType().equals("p")){
				 					Date fromDate = null;
				 					Date toDate = null;
				 					if(inputJSON.get(CommonConstants.TIMEINDATE) != null  && !inputJSON.get(CommonConstants.TIMEINDATE).toString().isEmpty()){
				 						fromDate = CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.TIMEINDATE).toString());
						 			}
						 			if(inputJSON.get(CommonConstants.TIMEOUTDATE) != null  && !inputJSON.get(CommonConstants.TIMEOUTDATE).toString().isEmpty()){
						 				toDate = CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.TIMEOUTDATE).toString());
						 			}
						 			if(fromDate != null && toDate != null){
						 				leaveRequestsService.deleteLeaveRequestByEmpId(fromDate, toDate, inputJSON.get(CommonConstants.EMPID).toString());
						 			}
			 					}	
					 		}
					 		attendanceDO.setUpdatedon(new Date());
					 		attendanceDO.setCreatedon(new Date());
					 		if(!attendanceService.update(attendanceDO)){
								return CommonWebUtil.buildErrorResponse("Attendance Added").toString();
							}
				 		}else{
				 			return CommonWebUtil.buildErrorResponse("Employee not available").toString();
				 		}
			 		}else{
			 			return CommonWebUtil.buildErrorResponse("Employee not available").toString();
			 		}
			 	}
			 	
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Attendance Updated");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			System.out.println(e);
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<AttendanceDO> attendanceList = attendanceService.retrieve();
				respJSON = AttendanceUtil.getAttendanceList(attendanceList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveSpecificField", method = RequestMethod.GET)
	public @ResponseBody String retrieveSpecificField(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<Object[]> attendanceList = attendanceService.retrieveSpeceficField();
				respJSON = AttendanceUtil.getAttendanceListSpecific(attendanceList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveByEmpId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieve(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<AttendanceDO> attendanceList = attendanceService.retrieveByEmpId(inputJSON.get(CommonConstants.ID).toString());
					respJSON = AttendanceUtil.getAttendanceList(attendanceList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	
	@RequestMapping(value = "/importAttendance", method = RequestMethod.POST)
	public @ResponseBody String importAttendance(Model model, HttpServletRequest request) {
		try {
			JSONObject inputJSON =  CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
			JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
			List<AttendanceDO> attendanceList = new ArrayList<AttendanceDO>();
			Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY);
			for (int i=0; i < fileData.size(); i++){
				JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
				AttendanceDO attendanceDO = new AttendanceDO();
				if(colName.get(CommonConstants.EMPID) != null && !colName.get(CommonConstants.EMPID).toString().isEmpty()){
		 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(rowJSON.get(colName.get(CommonConstants.EMPID)).toString());
			 		if(employeeList != null && employeeList.size() > 0){
			 			attendanceDO.setEmployee(employeeList.get(0));
			 			if(colName.get(CommonConstants.TIMEIN) != null && !colName.get(CommonConstants.TIMEIN).toString().isEmpty()){
			 				attendanceDO.setTimeIn(rowJSON.get(colName.get(CommonConstants.TIMEIN)).toString());
				 		}
			 			if(colName.get(CommonConstants.TIMEOUT) != null && !colName.get(CommonConstants.TIMEOUT).toString().isEmpty()){
			 				attendanceDO.setTimeOut(rowJSON.get(colName.get(CommonConstants.TIMEOUT)).toString());
				 		}
			 			if(colName.get(CommonConstants.TIMEINDATE) != null  && !colName.get(CommonConstants.TIMEINDATE).toString().isEmpty()){
			 				attendanceDO.setTimeInDate(CommonUtil.convertStringToSqlDate(rowJSON.get(colName.get(CommonConstants.TIMEINDATE)).toString()));
			 			}
			 			if(colName.get(CommonConstants.TIMEOUTDATE) != null  && !colName.get(CommonConstants.TIMEOUTDATE).toString().isEmpty()){
			 				attendanceDO.setTimeOutDate(CommonUtil.convertStringToSqlDate(rowJSON.get(colName.get(CommonConstants.TIMEOUTDATE)).toString()));
			 			}
			 			if(colName.get(CommonConstants.UPDATED_BY) != null && !colName.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			attendanceDO.setUpdatedby(updatedBy.toString());
				 			attendanceDO.setCreatedby(updatedBy.toString());
				 		}
			 			
			 			if(colName.get(CommonConstants.STATUS) != null && !colName.get(CommonConstants.STATUS).toString().isEmpty()){
			 				attendanceDO.setAttendanceType(rowJSON.get(colName.get(CommonConstants.STATUS)).toString());
				 		}
			 			
			 			if(colName.get(CommonConstants.STATUS) != null && !colName.get(CommonConstants.STATUS).toString().isEmpty() && rowJSON.get(colName.get(CommonConstants.STATUS)) != null && rowJSON.get(colName.get(CommonConstants.STATUS)).toString().equalsIgnoreCase("a")){
			 				EmployeeLopDO employeeLopDO = new EmployeeLopDO();
			 				employeeLopDO.setEmployee(employeeList.get(0));
			 				if(colName.get(CommonConstants.TIMEINDATE) != null  && !colName.get(CommonConstants.TIMEINDATE).toString().isEmpty()){
				 				employeeLopDO.setStartdate(CommonUtil.convertStringToSqlDate(rowJSON.get(colName.get(CommonConstants.TIMEINDATE)).toString()));
				 			}
			 				if(colName.get(CommonConstants.TIMEOUTDATE) != null  && !colName.get(CommonConstants.TIMEOUTDATE).toString().isEmpty()){
			 					employeeLopDO.setEnddate(CommonUtil.convertStringToSqlDate(rowJSON.get(colName.get(CommonConstants.TIMEOUTDATE)).toString()));
				 			}
			 				employeeLopDO.setUpdatedon(new Date());
			 				if(colName.get(CommonConstants.UPDATED_BY) != null && !colName.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 					employeeLopDO.setUpdatedby(updatedBy.toString());
			 					employeeLopDO.setCreatedby(updatedBy.toString());
					 		}
			 				employeeLopDO.setStatus(CommonConstants.ACTIVE);
			 				List<EmployeeLopDO> employeeLOPList = null;
			 				if(colName.get(CommonConstants.TIMEOUTDATE) != null  && !colName.get(CommonConstants.TIMEOUTDATE).toString().isEmpty()){
						 		employeeLOPList = employeeLopService.retrieveByEmpDate(rowJSON.get(colName.get(CommonConstants.EMPID)).toString(),
						 				CommonUtil.convertStringToSqlDate(rowJSON.get(colName.get(CommonConstants.TIMEINDATE)).toString()), CommonUtil.convertStringToSqlDate(rowJSON.get(colName.get(CommonConstants.TIMEOUTDATE)).toString()));
						 	}else{
						 		employeeLOPList = employeeLopService.retrieveByEmpDate(rowJSON.get(colName.get(CommonConstants.EMPID)).toString(),
						 				CommonUtil.convertStringToSqlDate(rowJSON.get(colName.get(CommonConstants.TIMEINDATE)).toString()), null);
						 	}
						 	
						 	if(employeeLOPList == null || employeeLOPList.size() == 0){
						 		employeeLopService.persist(employeeLopDO);
						 	}
			 			}
			 			
				 	}
				 		attendanceDO.setUpdatedon(new Date());
				 		attendanceDO.setCreatedon(new Date());
				 		attendanceList.add(attendanceDO);
				}
			}
			attendanceService.persistList(attendanceList);
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveByMonthAndEmpId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByMonthAndEmpId(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
					/*System.out.println("CommonUtil.convertMonthToSqlDate(inputJSON.get(CommonConstants.PERIOD).toString())"+CommonUtil.convertMonthToSqlDate(inputJSON.get(CommonConstants.PERIOD).toString()));
					Calendar c = Calendar.getInstance();
					c.setTime(CommonUtil.convertMonthToSqlDate(inputJSON.get(CommonConstants.PERIOD).toString()));
					c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
					System.out.println("c.getTime()"+c.getTime());*/
					Calendar c = Calendar.getInstance();
	 				c.setTime(CommonUtil.convertMonthToSqlDate(inputJSON.get(CommonConstants.PERIOD).toString()));
	 				Calendar endDate = Calendar.getInstance(); 
	 				int  year = c.get(Calendar.YEAR);
	 				 int  monthNumber= c.get(Calendar.MONTH);
	 				endDate.set(year,monthNumber,c.getActualMaximum(Calendar.DAY_OF_MONTH)); 
					List<AttendanceDO> attendanceList = attendanceService.retrieveByEmpId_Date(inputJSON.get(CommonConstants.EMPID).toString(),c.getTime(),endDate.getTime());
					respJSON = AttendanceUtil.getAttendanceListWithOutIntime(attendanceList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveByTeamAttendance/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByTeamAttendance(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
					List<EmployeeDO> empList = employeeService.retrieveReportTo(inputJSON.get(CommonConstants.EMPID).toString());
					List<String> empIds = new ArrayList<String>();
					if(empList != null && empList.size() > 0){
						for (EmployeeDO EmployeeDO : empList) {
							empIds.add(EmployeeDO.getEmpId());
						}
						List<AttendanceDO> attendanceList = attendanceService.retrieveByTeamEmp(empIds);
						respJSON = AttendanceUtil.getAttendanceList(attendanceList).toString();
					} else {
						return CommonWebUtil.buildErrorResponse("Attendance Details Not Available").toString();
					}
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
}
