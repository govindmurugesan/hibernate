package com.spheresuite.erp.rs;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.NotificationSettingsDO;
import com.spheresuite.erp.service.NotificationSettingsService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.NotificationSettingsUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/notificationsettings")
public class NotificationSettingsRS {

	String validation = null;
	static Logger logger = Logger.getLogger(NotificationSettingsRS.class.getName());

	@Autowired
	private NotificationSettingsService notificationSettingsService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				NotificationSettingsDO notificationSettingsDO = new NotificationSettingsDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.HRREQUEST) != null && !inputJSON.get(CommonConstants.HRREQUEST).toString().isEmpty()){
			 			notificationSettingsDO.setHrRequest(inputJSON.get(CommonConstants.HRREQUEST).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.PAYROLL) != null && !inputJSON.get(CommonConstants.PAYROLL).toString().isEmpty()){
			 			notificationSettingsDO.setPayroll(inputJSON.get(CommonConstants.PAYROLL).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.LEAVES) != null && !inputJSON.get(CommonConstants.LEAVES).toString().isEmpty()){
			 			notificationSettingsDO.setLeaves(inputJSON.get(CommonConstants.LEAVES).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.HRPOLICY) != null && !inputJSON.get(CommonConstants.HRPOLICY).toString().isEmpty()){
			 			notificationSettingsDO.setHrpolicies(inputJSON.get(CommonConstants.HRPOLICY).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.HOLIDAYS) != null && !inputJSON.get(CommonConstants.HOLIDAYS).toString().isEmpty()){
			 			notificationSettingsDO.setHolidays(inputJSON.get(CommonConstants.HOLIDAYS).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			notificationSettingsDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			notificationSettingsDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		notificationSettingsDO.setUpdatedon(new Date());
			 		notificationSettingsDO.setCreatedon(new Date());
			 	}
				notificationSettingsService.persist(notificationSettingsDO);
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Notification Settings Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<NotificationSettingsDO> notificationSettingsList = notificationSettingsService.retrieve();
				respJSON = NotificationSettingsUtil.getNotificationSettingsList(notificationSettingsList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				NotificationSettingsDO notificationSettingsDO = new NotificationSettingsDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(!inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 			List<NotificationSettingsDO> notificationSettingsList = notificationSettingsService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 				if(notificationSettingsList != null && notificationSettingsList.size() >0){
				 				notificationSettingsDO = notificationSettingsList.get(0);
				 				if(inputJSON.get(CommonConstants.HRREQUEST) != null){
						 			notificationSettingsDO.setHrRequest(inputJSON.get(CommonConstants.HRREQUEST).toString());
						 		}
						 		if(inputJSON.get(CommonConstants.PAYROLL) != null ){
						 			notificationSettingsDO.setPayroll(inputJSON.get(CommonConstants.PAYROLL).toString());
						 		}
						 		if(inputJSON.get(CommonConstants.LEAVES) != null){
						 			notificationSettingsDO.setLeaves(inputJSON.get(CommonConstants.LEAVES).toString());
						 		}
						 		if(inputJSON.get(CommonConstants.HRPOLICY) != null ){
						 			notificationSettingsDO.setHrpolicies(inputJSON.get(CommonConstants.HRPOLICY).toString());
						 		}
						 		
						 		if(inputJSON.get(CommonConstants.HOLIDAYS) != null){
						 			notificationSettingsDO.setHolidays(inputJSON.get(CommonConstants.HOLIDAYS).toString());
						 		}
						 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null ){
						 			notificationSettingsDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
						 		}
						 		notificationSettingsDO.setUpdatedon(new Date());
						 		notificationSettingsService.update(notificationSettingsDO);
						 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Notification Settings Updated");
			 				}
			 			}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
