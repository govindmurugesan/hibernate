package com.spheresuite.erp.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.ItSavingsSettingsDO;
import com.spheresuite.erp.domainobject.ItSectionDO;
import com.spheresuite.erp.service.ItSavingsSettingsService;
import com.spheresuite.erp.service.ItSectionService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.ItSavingsSettingsUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/itsavingssettings")
public class ItSavingsSettingsRS {

	String validation = null;
	static Logger logger = Logger.getLogger(ItSavingsSettingsRS.class.getName());

	@Autowired
	private ItSavingsSettingsService itSavingsSettingsService;
	
	@Autowired
	private ItSectionService itSectionService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				ItSavingsSettingsDO itSavingsSettingsTypeDO = new ItSavingsSettingsDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
			 			itSavingsSettingsTypeDO.setName(inputJSON.get(CommonConstants.NAME).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.FROMDATE) != null && !inputJSON.get(CommonConstants.FROMDATE).toString().isEmpty()){
			 			itSavingsSettingsTypeDO.setFromYear(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.FROMDATE).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.TODATE) != null && !inputJSON.get(CommonConstants.TODATE).toString().isEmpty()){
			 			itSavingsSettingsTypeDO.setToYear(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.TODATE).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.SECTIONID) != null && !inputJSON.get(CommonConstants.SECTIONID).toString().isEmpty()){
			 			List<ItSectionDO> itSectionList = itSectionService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.SECTIONID).toString()));
				 		if(itSectionList != null && itSectionList.size() > 0){
					 		itSavingsSettingsTypeDO.setItSection(itSectionList.get(0));
				 		}
			 		}
			 		/*if(inputJSON.get(CommonConstants.MAXLIMIT) != null && !inputJSON.get(CommonConstants.MAXLIMIT).toString().isEmpty()){
			 			itSavingsSettingsTypeDO.setMaxLimit(Long.parseLong(inputJSON.get(CommonConstants.MAXLIMIT).toString()));
			 		}*/
			 		itSavingsSettingsTypeDO.setStatus(CommonConstants.ACTIVE);
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			itSavingsSettingsTypeDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			itSavingsSettingsTypeDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		itSavingsSettingsTypeDO.setUpdatedon(new Date());
			 		itSavingsSettingsTypeDO.setCreatedon(new Date());
			 	}
				if(!itSavingsSettingsService.persist(itSavingsSettingsTypeDO)){
					return CommonWebUtil.buildErrorResponse("IT Savings Already Added").toString();
				}
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New IT Savings Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<ItSavingsSettingsDO> itSavingsSettingsList = itSavingsSettingsService.retrieveActive();
				respJSON = ItSavingsSettingsUtil.getITSavingsSettingsList(itSavingsSettingsList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<ItSavingsSettingsDO> itSavingsSettingsList = itSavingsSettingsService.retrieve();
				respJSON = ItSavingsSettingsUtil.getITSavingsSettingsList(itSavingsSettingsList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveByDate/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String retrieve(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				List<ItSavingsSettingsDO> itSavingsSettingsList = itSavingsSettingsService.retrieveByDate((CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.FROMDATE).toString())),CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.TODATE).toString()));
				respJSON = ItSavingsSettingsUtil.getITSavingsSettingsList(itSavingsSettingsList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				ItSavingsSettingsDO itSavingsSettingsTypeDO = new ItSavingsSettingsDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<ItSavingsSettingsDO> itSavingsSettingsList = itSavingsSettingsService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(itSavingsSettingsList != null && itSavingsSettingsList.size() > 0){
			 			itSavingsSettingsTypeDO = itSavingsSettingsList.get(0);
				 		
				 		if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
				 			itSavingsSettingsTypeDO.setName(inputJSON.get(CommonConstants.NAME).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.FROMDATE) != null && !inputJSON.get(CommonConstants.FROMDATE).toString().isEmpty()){
				 			itSavingsSettingsTypeDO.setFromYear(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.FROMDATE).toString()));
				 		}
				 		if(inputJSON.get(CommonConstants.TODATE) != null && !inputJSON.get(CommonConstants.TODATE).toString().isEmpty()){
				 			itSavingsSettingsTypeDO.setToYear(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.TODATE).toString()));
				 		}
				 		if(inputJSON.get(CommonConstants.SECTIONID) != null && !inputJSON.get(CommonConstants.SECTIONID).toString().isEmpty()){
				 			List<ItSectionDO> itSectionList = itSectionService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.SECTIONID).toString()));
					 		if(itSectionList != null && itSectionList.size() > 0){
						 		itSavingsSettingsTypeDO.setItSection(itSectionList.get(0));
					 		}
				 		}
				 		/*if(inputJSON.get(CommonConstants.MAXLIMIT) != null && !inputJSON.get(CommonConstants.MAXLIMIT).toString().isEmpty()){
				 			itSavingsSettingsTypeDO.setMaxLimit(Long.parseLong(inputJSON.get(CommonConstants.MAXLIMIT).toString()));
				 		}*/
				 		if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
				 			itSavingsSettingsTypeDO.setStatus((char) (inputJSON.get(CommonConstants.STATUS).toString().charAt(0)));
				 		}
				 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			itSavingsSettingsTypeDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		itSavingsSettingsTypeDO.setUpdatedon(new Date());
				 		if(!itSavingsSettingsService.update(itSavingsSettingsTypeDO)){
							return CommonWebUtil.buildErrorResponse("ItSavings Already Added").toString();
						}
				 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "IT Savings Updated");
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/importITSavingSettings", method = RequestMethod.POST)
	public @ResponseBody String importITSavingSettings(Model model, HttpServletRequest request) {
		try {
			JSONObject inputJSON =  CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
			JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
			List<ItSavingsSettingsDO> itSavingsSettingsList = new ArrayList<ItSavingsSettingsDO>();
			Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY);
			for (int i=0; i < fileData.size(); i++){
				JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
				ItSavingsSettingsDO itSavingsSettings = new ItSavingsSettingsDO();
				if (colName.get(CommonConstants.SECTIONID) != null && !colName.get(CommonConstants.SECTIONID).toString().isEmpty()){
					List<ItSectionDO> itSectionList = itSectionService.retrieveBySectionName(rowJSON.get(colName.get(CommonConstants.SECTIONID)).toString());
			 		if(itSectionList != null && itSectionList.size() > 0){
			 			itSavingsSettings.setItSection(itSectionList.get(0));
				 		if (colName.get(CommonConstants.NAME) != null && !colName.get(CommonConstants.NAME).toString().isEmpty()){
							itSavingsSettings.setName(rowJSON.get(colName.get(CommonConstants.NAME)).toString());
				 		}
				 		if(colName.get(CommonConstants.FROMDATE) != null && !colName.get(CommonConstants.FROMDATE).toString().isEmpty()){
				 			itSavingsSettings.setFromYear(CommonUtil.convertStringToSqlDate(rowJSON.get(colName.get(CommonConstants.FROMDATE)).toString()));
				 		}
				 		if(colName.get(CommonConstants.TODATE) != null && !colName.get(CommonConstants.TODATE).toString().isEmpty()){
				 			itSavingsSettings.setToYear(CommonUtil.convertStringToSqlDate(rowJSON.get(colName.get(CommonConstants.TODATE)).toString()));
				 		}
				 		if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
				 			itSavingsSettings.setUpdatedby(updatedBy.toString());
				 			itSavingsSettings.setCreatedby(updatedBy.toString());
				 		}
				 		itSavingsSettings.setUpdatedon(new Date());
				 		itSavingsSettings.setCreatedon(new Date());
				 		itSavingsSettings.setStatus(CommonConstants.ACTIVE);
				 		itSavingsSettingsList.add(itSavingsSettings);
			 		}
		 		}
			}
			itSavingsSettingsService.persistList(itSavingsSettingsList);
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
