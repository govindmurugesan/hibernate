package com.spheresuite.erp.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.EarningSubTypeDO;
import com.spheresuite.erp.domainobject.EarningTypeDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.GeneralEarningDO;
import com.spheresuite.erp.domainobject.UnitOrBranchDO;
import com.spheresuite.erp.service.EarningSubTypeService;
import com.spheresuite.erp.service.EarningTypeService;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.GeneralEarningService;
import com.spheresuite.erp.service.UnitOrBranchService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.GeneralEarningUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/generalearning")
public class GeneralEarningRS {

	String validation = null;
	static Logger logger = Logger.getLogger(GeneralEarningRS.class.getName());

	@Autowired
	private GeneralEarningService generalEarningService;
	
	@Autowired
	private EmployeeService employeeService;
	
	@Autowired
	private EarningSubTypeService earningSubTypeService;
	
	@Autowired
	private UnitOrBranchService unitOrBranchService;
	
	@Autowired
	private EarningTypeService earningTypeService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				GeneralEarningDO generalEarningDO = new GeneralEarningDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		List<EmployeeDO> employeeList = null;
			 		if(inputJSON.get(CommonConstants.EMPID) != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
				 		if(employeeList != null && employeeList.size() > 0){
				 			generalEarningDO.setEmployee(employeeList.get(0));
				 			if(inputJSON.get(CommonConstants.TYPE_ID) != null && !inputJSON.get(CommonConstants.TYPE_ID).toString().isEmpty()){
					 			List <EarningSubTypeDO> earningSubTypeList = earningSubTypeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.TYPE_ID).toString()));
						 		if(earningSubTypeList != null && earningSubTypeList.size() > 0){
						 			generalEarningDO.setEarningSubType(earningSubTypeList.get(0));
						 			if(inputJSON.get(CommonConstants.UNIT) != null && !inputJSON.get(CommonConstants.UNIT).toString().isEmpty()){
							 			List <UnitOrBranchDO> unitOrBranchList = unitOrBranchService.retrieveActiveById(Long.parseLong(inputJSON.get(CommonConstants.UNIT).toString()));
								 		if(unitOrBranchList != null && unitOrBranchList.size() > 0){
								 			generalEarningDO.setUnitOrBranch(unitOrBranchList.get(0));
								 			if(inputJSON.get(CommonConstants.EARNINGTYPEID) != null && !inputJSON.get(CommonConstants.EARNINGTYPEID).toString().isEmpty()){
									 			List<EarningTypeDO> earningTypeList = earningTypeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.EARNINGTYPEID).toString()));
									 			if(earningTypeList != null && earningTypeList.size() >0){
									 				generalEarningDO.setEarningType(earningTypeList.get(0));
									 				if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
											 			generalEarningDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
											 			generalEarningDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
											 			if(inputJSON.get(CommonConstants.AMOUNT) != null && !inputJSON.get(CommonConstants.AMOUNT).toString().isEmpty()){
												 			generalEarningDO.setAmount(Long.parseLong(inputJSON.get(CommonConstants.AMOUNT).toString()));
												 		}
												 		if(inputJSON.get(CommonConstants.MONTHLY) != null && !inputJSON.get(CommonConstants.MONTHLY).toString().isEmpty()){
												 			generalEarningDO.setMonth(inputJSON.get(CommonConstants.MONTHLY).toString());
												 		}
												 		generalEarningDO.setUpdatedon(new Date());
												 		generalEarningDO.setCreatedon(new Date());
												 		generalEarningDO.setStatus(CommonConstants.ACTIVESTRING);
												 		if(!generalEarningService.persist(generalEarningDO)){
												 			return CommonWebUtil.buildErrorResponse("General Earning Already Added").toString();
														}
												 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "General Earning Created For Employee");
									 				}
									 			}
									 		}
								 		}
							 		}
						 		}
					 		}
				 		}
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	/*@RequestMapping(value = "/retrieveByEmpId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByEmpId(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<EmployeeBonusDO> employeeAddressList = employeeBonusService.retrieveByEmpId(inputJSON.get(CommonConstants.ID).toString());
					respJSON = EmployeeBonusUtil.getEmployeeBonusList(employeeAddressList, null,null).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}*/
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieveByEmpId(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
					List<GeneralEarningDO> loanPayemntList = generalEarningService.retrieve();
					respJSON = GeneralEarningUtil.getAdvancePaymentList(loanPayemntList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				GeneralEarningDO generalEarningDO = new GeneralEarningDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<GeneralEarningDO> loanPaymentList = generalEarningService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		generalEarningDO = loanPaymentList.get(0);
			 		
			 		if(inputJSON.get(CommonConstants.EMPID) != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
				 		if(employeeList != null && employeeList.size() > 0){
				 			generalEarningDO.setEmployee(employeeList.get(0));
				 		}
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.UNIT) != null && !inputJSON.get(CommonConstants.UNIT).toString().isEmpty()){
			 			List <UnitOrBranchDO> unitOrBranchList = unitOrBranchService.retrieveActiveById(Long.parseLong(inputJSON.get(CommonConstants.UNIT).toString()));
				 		if(unitOrBranchList != null && unitOrBranchList.size() > 0){
				 			generalEarningDO.setUnitOrBranch(unitOrBranchList.get(0));
				 		}
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.EARNINGTYPEID) != null && !inputJSON.get(CommonConstants.EARNINGTYPEID).toString().isEmpty()){
			 			List<EarningTypeDO> earningTypeList = earningTypeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.EARNINGTYPEID).toString()));
			 			if(earningTypeList != null && earningTypeList.size() >0){
			 				generalEarningDO.setEarningType(earningTypeList.get(0));
			 			}
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.TYPE_ID) != null && !inputJSON.get(CommonConstants.TYPE_ID).toString().isEmpty()){
			 			List <EarningSubTypeDO> earningSubTypeList = earningSubTypeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.TYPE_ID).toString()));
				 		if(earningSubTypeList != null && earningSubTypeList.size() > 0){
				 			generalEarningDO.setEarningSubType(earningSubTypeList.get(0));
				 		}
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.AMOUNT) != null && !inputJSON.get(CommonConstants.AMOUNT).toString().isEmpty()){
			 			generalEarningDO.setAmount(Long.parseLong(inputJSON.get(CommonConstants.AMOUNT).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.MONTHLY) != null && !inputJSON.get(CommonConstants.MONTHLY).toString().isEmpty()){
			 			generalEarningDO.setMonth(inputJSON.get(CommonConstants.MONTHLY).toString());
			 		}
			 		generalEarningDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			generalEarningDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		if(!generalEarningService.update(generalEarningDO)){
			 			return CommonWebUtil.buildErrorResponse("Earning SubType Already Added").toString();
					}
			 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Earning SubType Updated For Employee");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/importGeneralEarning", method = RequestMethod.POST)
	public @ResponseBody String importEarningType(Model model, HttpServletRequest request) {
		try {
			JSONObject inputJSON =  CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
			JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
			List<GeneralEarningDO> earningTypelist = new ArrayList<GeneralEarningDO>();
			Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY);
			for (int i=0; i < fileData.size(); i++){
				JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
				GeneralEarningDO earning = new GeneralEarningDO();
		 		
		 		if(colName.get(CommonConstants.EMPID) != null && !colName.get(CommonConstants.EMPID).toString().isEmpty()){
		 			List<EmployeeDO> 	employeeList = employeeService.retriveByEmpId(rowJSON.get(colName.get(CommonConstants.EMPID)).toString());
			 		if(employeeList != null && employeeList.size() > 0){
			 			earning.setEmployee(employeeList.get(0));
			 		}
		 		}
		 		if(colName.get(CommonConstants.TYPENAME) != null && !colName.get(CommonConstants.TYPENAME).toString().isEmpty()){
		 			List <EarningSubTypeDO> earningSubTypeList = earningSubTypeService.retrieveByName(rowJSON.get(colName.get(CommonConstants.TYPENAME)).toString());
			 		if(earningSubTypeList != null && earningSubTypeList.size() > 0){
			 			earning.setEarningSubType(earningSubTypeList.get(0));
			 		}
		 		}
		 		
		 		if(colName.get(CommonConstants.UNITNAME) != null && !colName.get(CommonConstants.UNITNAME).toString().isEmpty()){
		 			List <UnitOrBranchDO> unitOrBranchList = unitOrBranchService.retrieveActiveByName(rowJSON.get(colName.get(CommonConstants.UNITNAME)).toString());
			 		if(unitOrBranchList != null && unitOrBranchList.size() > 0){
			 			earning.setUnitOrBranch(unitOrBranchList.get(0));
			 		}
		 		}
		 		if(colName.get(CommonConstants.EARNINGNAME) != null && !colName.get(CommonConstants.EARNINGNAME).toString().isEmpty()){
		 			List<EarningTypeDO> earningTypeList = earningTypeService.retrieveByName(rowJSON.get(colName.get(CommonConstants.EARNINGNAME)).toString());
		 			if(earningTypeList != null && earningTypeList.size() >0){
		 				earning.setEarningType(earningTypeList.get(0));
		 			}
		 		}
		 		if(colName.get(CommonConstants.AMOUNT) != null && !colName.get(CommonConstants.AMOUNT).toString().isEmpty()){
		 			earning.setAmount(Long.parseLong(rowJSON.get(colName.get(CommonConstants.AMOUNT)).toString()));
		 		}
		 		if(colName.get(CommonConstants.MONTHLY) != null && !colName.get(CommonConstants.MONTHLY).toString().isEmpty()){
		 			earning.setMonth(rowJSON.get(colName.get(CommonConstants.MONTHLY)).toString());
		 		}
		 		if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
		 			earning.setUpdatedby(updatedBy.toString());
		 			earning.setCreatedby(updatedBy.toString());
		 		}
		 		
		 		
		 		earning.setUpdatedon(new Date());
		 		earning.setCreatedon(new Date());
		 		earning.setStatus(CommonConstants.ACTIVESTRING);
		 		earningTypelist.add(earning);
			}
			generalEarningService.persistList(earningTypelist);
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	/*@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public @ResponseBody String delete(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		Boolean employeeAddressList = employeeBonusService.delete(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(employeeAddressList){
			 			CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Bonus Deleted For Employee");
			 		}else{
			 			return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retriveForBonusReport/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveForBonusReport(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.MONTHLY).toString().isEmpty()){
					Long batchId = null;
					List<Long> ids = new ArrayList<Long>();
					if(!inputJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty()){
						batchId = Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString());
						if(batchId == 0){
							batchId = null;
						}else{
							ids = employeeCtcService.retrieveByBatchId(batchId);
						}
					}
					List<EmployeeBonusDO> bonusList = employeeBonusService.retrieveByEmpIdWithDate(ids, inputJSON.get(CommonConstants.MONTHLY).toString());
					respJSON = EmployeeBonusUtil.getEmployeeBonusList(bonusList, CommonConstants.MONTHLYC, inputJSON.get(CommonConstants.MONTHLY).toString()).toString();
				}else{
					if(inputJSON != null && !inputJSON.get(CommonConstants.QUARTERLY).toString().isEmpty()){
						String fromMonth = null;
						String toMonth = null;
						Long batchId = null;
						List<FyDO> fyList = new ArrayList<FyDO>();
						if(!inputJSON.get(CommonConstants.FYID).toString().isEmpty()){
							fyList = fyService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.FYID).toString()));
							if(fyList != null && fyList.size() > 0){
								List<Long> ids = new ArrayList<Long>();
								if(!inputJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty()){
									batchId = Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString());
									if(batchId == 0){
										batchId = null;
									}else{
										ids = employeeCtcService.retrieveByBatchId(batchId);
									}
								}
								if(inputJSON.get(CommonConstants.QUARTERLY).toString().equalsIgnoreCase("Q1")){
									fromMonth = "Apr"+" "+fyList.get(0).getFromyear();
									toMonth = "Jun"+" "+fyList.get(0).getFromyear(); 
									List<EmployeeBonusDO> bonusList = employeeBonusService.retrieveByEmpIdBetweenDate(ids,fromMonth, toMonth);
									if(bonusList != null && bonusList.size() > 0){
										respJSON = EmployeeBonusUtil.MultipleMonthBonuReport(bonusList,CommonConstants.QUARTERLYC, fromMonth+" - "+toMonth).toString();
									}
								}
								
								if(inputJSON.get(CommonConstants.QUARTERLY).toString().equalsIgnoreCase("Q2")){
									fromMonth = "Jul"+" "+fyList.get(0).getFromyear();
									toMonth = "Sep"+" "+fyList.get(0).getFromyear(); 
									List<EmployeeBonusDO> bonusList = employeeBonusService.retrieveByEmpIdBetweenDate(ids,fromMonth, toMonth);
									if(bonusList != null && bonusList.size() > 0){
										respJSON = EmployeeBonusUtil.MultipleMonthBonuReport(bonusList,CommonConstants.QUARTERLYC, fromMonth+" - "+toMonth).toString();
									}
								}
								
								
								if(inputJSON.get(CommonConstants.QUARTERLY).toString().equalsIgnoreCase("Q3")){
									fromMonth = "Oct"+" "+fyList.get(0).getFromyear();
									toMonth = "Dec"+" "+fyList.get(0).getFromyear(); 
									List<EmployeeBonusDO> bonusList = employeeBonusService.retrieveByEmpIdBetweenDate(ids,fromMonth, toMonth);
									if(bonusList != null && bonusList.size() > 0){
										respJSON = EmployeeBonusUtil.MultipleMonthBonuReport(bonusList,CommonConstants.QUARTERLYC, fromMonth+" - "+toMonth).toString();
									}
								}
								if(inputJSON.get(CommonConstants.QUARTERLY).toString().equalsIgnoreCase("Q4")){
									fromMonth = "Jan"+" "+fyList.get(0).getToyear();
									toMonth = "Mar"+" "+fyList.get(0).getToyear(); 
									List<EmployeeBonusDO> bonusList = employeeBonusService.retrieveByEmpIdBetweenDate(ids,fromMonth, toMonth);
									if(bonusList != null && bonusList.size() > 0){
										respJSON = EmployeeBonusUtil.MultipleMonthBonuReport(bonusList,CommonConstants.QUARTERLYC, fromMonth+" - "+toMonth).toString();
									}
								}
							}
							
						}
					}else{
						if(inputJSON != null && !inputJSON.get(CommonConstants.FREQUENCY).toString().isEmpty()
								&& inputJSON.get(CommonConstants.FREQUENCY).toString().equalsIgnoreCase("Y")){
							String fromMonth = null;
							String toMonth = null;
							Long batchId = null;
							List<FyDO> fyList = new ArrayList<FyDO>();
							if(!inputJSON.get(CommonConstants.FYID).toString().isEmpty()){
								fyList = fyService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.FYID).toString()));
								if(fyList != null && fyList.size() > 0){
									List<Long> ids = new ArrayList<Long>();
									if(!inputJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty()){
										batchId = Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString());
										if(batchId == 0){
											batchId = null;
										}else{
											ids = employeeCtcService.retrieveByBatchId(batchId);
										}
									}
									fromMonth = "Apr"+" "+fyList.get(0).getFromyear();
									toMonth = "Mar"+" "+fyList.get(0).getToyear(); 
									List<EmployeeBonusDO> bonusList = employeeBonusService.retrieveByEmpIdBetweenDate(ids,fromMonth, toMonth);
									if(bonusList != null && bonusList.size() > 0){
										respJSON = EmployeeBonusUtil.MultipleMonthBonuReport(bonusList,CommonConstants.YEARLYC, fromMonth+" - "+toMonth).toString();
									}
								}
							}
						}
					}
				}
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}*/
}
