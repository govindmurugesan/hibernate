package com.spheresuite.erp.rs;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.EmployeeTravelDO;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.EmployeeTravelService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.EmployeeTravelUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/emptravel")
public class EmployeeTravelRS {

	String validation = null;
	static Logger logger = Logger.getLogger(EmployeeTravelRS.class.getName());
	
	@Autowired
	private EmployeeTravelService empTravelService;
	
	@Autowired
	private EmployeeService empService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeTravelDO empTravelDO = new EmployeeTravelDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.EMPID) != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			List <EmployeeDO> empDetail = empService.retrieveEmpId(inputJSON.get(CommonConstants.EMPID).toString());
			 			if(empDetail != null && empDetail.size() > 0){
			 				empTravelDO.setEmployee(empDetail.get(0));
			 				//List <EmployeeDO> reportingDetail = empService.retrieveEmpId(inputJSON.get(CommonConstants.REPORTTO).toString());
			 				if(empDetail.get(0).getReportto() != null ){
			 					empTravelDO.setReportingTo(empDetail.get(0).getReportto());
			 					if(inputJSON.get(CommonConstants.FROMDATE) != null && !inputJSON.get(CommonConstants.FROMDATE).toString().isEmpty()){
			 						empTravelDO.setFromDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.FROMDATE).toString()));
			 					}
			 					if(inputJSON.get(CommonConstants.TODATE) != null && !inputJSON.get(CommonConstants.TODATE).toString().isEmpty()){
			 						empTravelDO.setToDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.TODATE).toString()));
			 					}
			 					if(inputJSON.get(CommonConstants.ADDRESS) != null && !inputJSON.get(CommonConstants.ADDRESS).toString().isEmpty()){
			 						empTravelDO.setPlace(inputJSON.get(CommonConstants.ADDRESS).toString());
			 					}
			 					if(inputJSON.get(CommonConstants.COMMENT) != null && !inputJSON.get(CommonConstants.COMMENT).toString().isEmpty()){
			 						empTravelDO.setComments(inputJSON.get(CommonConstants.COMMENT).toString());
			 					}
			 					if(inputJSON.get(CommonConstants.TYPE) != null && !inputJSON.get(CommonConstants.TYPE).toString().isEmpty()){
			 						empTravelDO.setType(inputJSON.get(CommonConstants.TYPE).toString());
			 					}
			 					empTravelDO.setStatus(CommonConstants.P_STATE);
			 					empTravelDO.setUpdatedon(new Date());
			 					empTravelDO.setCreatedon(new Date());
			 					if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
						 			empTravelDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
						 			empTravelDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
						 		}
			 					if(!empTravelService.persist(empTravelDO)){
			 						return CommonWebUtil.buildErrorResponse("Employee Travel Details Already Added").toString();
			 					}
			 					CommonUtil.persistRecentAcvtivity(inputJSON.get(CommonConstants.UPDATED_BY).toString(), "New Employee Travel Details Created");
			 				}else{
			 					return CommonWebUtil.buildErrorResponse("Approver Not Availble. Contact Admin").toString();
			 				}
			 			}else{
			 				return CommonWebUtil.buildErrorResponse("").toString();
			 			}
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	/*@RequestMapping(value = "/retrieveActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<AddressTypeDO> addressTypeList = addressTypeService.retrieveActive();
				respJSON = AddressTypeUtil.getAddressTypeList(addressTypeList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}*/
	
	@RequestMapping(value = "/retrieveByEmpId", method = RequestMethod.POST)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
				if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<EmployeeTravelDO> empTravelList = empTravelService.retrieveByEmpId(inputJSON.get(CommonConstants.ID).toString());
					respJSON = EmployeeTravelUtil.getempTravelList(empTravelList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveByReportingId", method = RequestMethod.POST)
	public @ResponseBody String retrieveByReportingId(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<EmployeeTravelDO> empTravelList = empTravelService.retrieveByReportingId(inputJSON.get(CommonConstants.ID).toString());
			 		//if(empTravelList != null && empTravelList.size() > 0){
			 			respJSON = EmployeeTravelUtil.getempTravelList(empTravelList).toString();
			 		//}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeTravelDO empTravelDO = new EmployeeTravelDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<EmployeeTravelDO> empTravelList = empTravelService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(empTravelList != null && empTravelList.size() > 0){
			 			empTravelDO = empTravelList.get(0);
			 			if(inputJSON.get(CommonConstants.COMMENT) != null && !inputJSON.get(CommonConstants.COMMENT).toString().isEmpty()){
	 						empTravelDO.setComments(inputJSON.get(CommonConstants.COMMENT).toString());
	 					}
	 					
	 					empTravelDO.setStatus(inputJSON.get(CommonConstants.STATUS).toString());
				 		empTravelDO.setUpdatedon(new Date());
				 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			empTravelDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		if(!empTravelService.update(empTravelDO)){
							return CommonWebUtil.buildErrorResponse("Employee Travel Already Added").toString();
						}
				 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Employee Travel  Updated");
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	/*@RequestMapping(value = "/importAddressType", method = RequestMethod.POST)
	public @ResponseBody String importAddressType(Model model, HttpServletRequest request) {
		try {
			JSONObject inputJSON =  CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
			JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
			List<AddressTypeDO> AddressTypeList = new ArrayList<AddressTypeDO>();
			Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY);
			for (int i=0; i < fileData.size(); i++){
				JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
				AddressTypeDO addressTypeDO = new AddressTypeDO();
				if (colName.get(CommonConstants.NAME) != null && !colName.get(CommonConstants.NAME).toString().isEmpty()){
					addressTypeDO.setName(rowJSON.get(colName.get(CommonConstants.NAME)).toString());
		 		}
		 		if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
		 			addressTypeDO.setUpdatedby(updatedBy.toString());
		 			addressTypeDO.setCreatedby(updatedBy.toString());
		 		}
		 		addressTypeDO.setUpdatedon(new Date());
		 		addressTypeDO.setCreatedon(new Date());
		 		addressTypeDO.setStatus(CommonConstants.ACTIVE);
		 		AddressTypeList.add(addressTypeDO);
			}
			addressTypeService.persistList(AddressTypeList);
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}*/
}
