package com.spheresuite.erp.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.AllowanceSettingDO;
import com.spheresuite.erp.domainobject.AttendanceAllowanceDO;
import com.spheresuite.erp.domainobject.DeductionSettingDO;
import com.spheresuite.erp.domainobject.PayrollGroupAttendanceAllowanceDO;
import com.spheresuite.erp.domainobject.PayrollGroupDO;
import com.spheresuite.erp.domainobject.PayrollGroupDeductionDO;
import com.spheresuite.erp.domainobject.PayrollGroupEarningsDO;
import com.spheresuite.erp.domainobject.PayrollGroupTaxDO;
import com.spheresuite.erp.domainobject.PayrollGroupWorkingdaysAllowanceDO;
import com.spheresuite.erp.domainobject.TaxSettingsDO;
import com.spheresuite.erp.domainobject.WorkingDayAllowanceDO;
import com.spheresuite.erp.service.AllowanceSettingsService;
import com.spheresuite.erp.service.AttendanceAllowanceService;
import com.spheresuite.erp.service.DeductionSettingsService;
import com.spheresuite.erp.service.PayrollGroupAttendanceAllowanceService;
import com.spheresuite.erp.service.PayrollGroupDeductionService;
import com.spheresuite.erp.service.PayrollGroupEarningService;
import com.spheresuite.erp.service.PayrollGroupService;
import com.spheresuite.erp.service.PayrollGroupTaxService;
import com.spheresuite.erp.service.PayrollGroupWorkingDayService;
import com.spheresuite.erp.service.TaxSettingsService;
import com.spheresuite.erp.service.WorkingDayAllowanceService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.PayrollGroupUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/payrollgroupsettings")
public class PayrollGroupSettingsRS {

	String validation = null;
	static Logger logger = Logger.getLogger(PayrollGroupSettingsRS.class.getName());
	
	@Autowired
	private PayrollGroupService payrollGroupService;
	
	@Autowired
	private PayrollGroupEarningService payrollGroupEarningService;
	
	@Autowired
	private PayrollGroupDeductionService payrollGroupDeductionService;
	
	@Autowired
	private PayrollGroupTaxService payrollGroupTaxService;
	
	@Autowired
	private PayrollGroupWorkingDayService payrollGroupWorkingDayService;
	
	@Autowired
	private PayrollGroupAttendanceAllowanceService payrollGroupAttendanceAllowanceService;
	
	@Autowired
	private DeductionSettingsService deductionSettingsService;
	
	@Autowired
	private AllowanceSettingsService allowanceSettingsService;
	
	@Autowired
	private TaxSettingsService taxSettingsService;
	
	@Autowired
	private WorkingDayAllowanceService workingDayAllowanceService;
	
	@Autowired
	private AttendanceAllowanceService attendanceAllowanceService;
	
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if (inputJSON.get(CommonConstants.ALLOWENCE_ID) != null && !inputJSON.get(CommonConstants.ALLOWENCE_ID).toString().isEmpty()){
				 		JSONArray resultJSONArray = new JSONArray();
				 		resultJSONArray.put(inputJSON.get(CommonConstants.ALLOWENCE_ID));
				 		String rec = (String) resultJSONArray.get(0).toString();
				 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
				 		for (int i = 0; i < resultJSONArray1.size(); i++) {
				 			PayrollGroupEarningsDO payrollGroupEarningDO = new PayrollGroupEarningsDO();
					 		if(inputJSON.get(CommonConstants.PAYROLLGROUPID) != null && !inputJSON.get(CommonConstants.PAYROLLGROUPID).toString().isEmpty()){
					 			List<PayrollGroupDO> payrollGroupList = payrollGroupService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.PAYROLLGROUPID).toString()));
						 		if(payrollGroupList != null && payrollGroupList.size() >0){
						 			payrollGroupEarningDO.setPayrollGroup(payrollGroupList.get(0));
						 		}
					 		}
					 		
					 		if(resultJSONArray1.get(i) != null ){
						 		List<AllowanceSettingDO> allowanceSettingList = allowanceSettingsService.retrieveById(Long.parseLong(resultJSONArray1.get(i).toString()));
					 			if(allowanceSettingList != null && allowanceSettingList.size() > 0){
					 				payrollGroupEarningDO.setAllowanceSettings(allowanceSettingList.get(0));
					 			}
					 		}
				 			payrollGroupEarningDO.setStatus(CommonConstants.ACTIVE);
					 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			payrollGroupEarningDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			payrollGroupEarningDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 		}
					 		payrollGroupEarningDO.setUpdatedon(new Date());
					 		payrollGroupEarningDO.setCreatedon(new Date());
					 		
					 		if(!payrollGroupEarningService.persist(payrollGroupEarningDO)){
					 			//return CommonWebUtil.buildErrorResponse("Payroll Group Already Added").toString();
							}
				 		}
			 		}
			 		
			 		if (inputJSON.get(CommonConstants.DEDUCTIONID) != null && !inputJSON.get(CommonConstants.DEDUCTIONID).toString().isEmpty()){
				 		JSONArray resultJSONArray = new JSONArray();
				 		resultJSONArray.put(inputJSON.get(CommonConstants.DEDUCTIONID));
				 		String rec = (String) resultJSONArray.get(0).toString();
				 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
				 		for (int i = 0; i < resultJSONArray1.size(); i++) {
				 			PayrollGroupDeductionDO payrollGroupDeductionDO = new PayrollGroupDeductionDO();
					 		if(inputJSON.get(CommonConstants.PAYROLLGROUPID) != null && !inputJSON.get(CommonConstants.PAYROLLGROUPID).toString().isEmpty()){
					 			List<PayrollGroupDO> payrollGroupList = payrollGroupService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.PAYROLLGROUPID).toString()));
						 		if(payrollGroupList != null && payrollGroupList.size() >0){
						 			payrollGroupDeductionDO.setPayrollGroup(payrollGroupList.get(0));
						 		}
					 		}
					 		if(resultJSONArray1.get(i) != null ){
						 		List<DeductionSettingDO> deductionSettingList = deductionSettingsService.retrieveById(Long.parseLong(resultJSONArray1.get(i).toString()));
						 		if(deductionSettingList != null && deductionSettingList.size() > 0){
					 				payrollGroupDeductionDO.setDeductionSettings(deductionSettingList.get(0));
					 			}
					 		}
					 		payrollGroupDeductionDO.setStatus(CommonConstants.ACTIVE);
					 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			payrollGroupDeductionDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			payrollGroupDeductionDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 		}
					 		payrollGroupDeductionDO.setUpdatedon(new Date());
					 		payrollGroupDeductionDO.setCreatedon(new Date());
					 		
					 		if(!payrollGroupDeductionService.persist(payrollGroupDeductionDO)){
					 			//return CommonWebUtil.buildErrorResponse("Payroll Group Already Added").toString();
							}
				 		}
			 		}
			 		
			 		if (inputJSON.get(CommonConstants.TAXID) != null && !inputJSON.get(CommonConstants.TAXID).toString().isEmpty()){
				 		JSONArray resultJSONArray = new JSONArray();
				 		resultJSONArray.put(inputJSON.get(CommonConstants.TAXID));
				 		String rec = (String) resultJSONArray.get(0).toString();
				 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
				 		for (int i = 0; i < resultJSONArray1.size(); i++) {
				 			PayrollGroupTaxDO payrollGroupTaxDO = new PayrollGroupTaxDO();
					 		if(inputJSON.get(CommonConstants.PAYROLLGROUPID) != null && !inputJSON.get(CommonConstants.PAYROLLGROUPID).toString().isEmpty()){
					 			List<PayrollGroupDO> payrollGroupList = payrollGroupService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.PAYROLLGROUPID).toString()));
						 		if(payrollGroupList != null && payrollGroupList.size() >0){
						 			payrollGroupTaxDO.setPayrollGroup(payrollGroupList.get(0));
						 		}
					 		}
					 		if(resultJSONArray1.get(i) != null ){
					 			List<TaxSettingsDO> taxSettingList = taxSettingsService.retrieveById(Long.parseLong(resultJSONArray1.get(i).toString()));
						 		if(taxSettingList != null && taxSettingList.size() > 0){
						 			payrollGroupTaxDO.setTaxSettings(taxSettingList.get(0));
					 			}
					 		}
					 		payrollGroupTaxDO.setStatus(CommonConstants.ACTIVE);
					 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			payrollGroupTaxDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			payrollGroupTaxDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 		}
					 		payrollGroupTaxDO.setUpdatedon(new Date());
					 		payrollGroupTaxDO.setCreatedon(new Date());
					 		
					 		if(!payrollGroupTaxService.persist(payrollGroupTaxDO)){
					 			//return CommonWebUtil.buildErrorResponse("Payroll Group Already Added").toString();
							}
				 		}
			 		}
			 		
			 		
			 		if (inputJSON.get(CommonConstants.WORKALLOWANCEID) != null && !inputJSON.get(CommonConstants.WORKALLOWANCEID).toString().isEmpty()){
				 		JSONArray resultJSONArray = new JSONArray();
				 		resultJSONArray.put(inputJSON.get(CommonConstants.WORKALLOWANCEID));
				 		String rec = (String) resultJSONArray.get(0).toString();
				 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
				 		for (int i = 0; i < resultJSONArray1.size(); i++) {
				 			PayrollGroupWorkingdaysAllowanceDO payrollGroupWorkingdaysAllowanceDO = new PayrollGroupWorkingdaysAllowanceDO();
					 		if(inputJSON.get(CommonConstants.PAYROLLGROUPID) != null && !inputJSON.get(CommonConstants.PAYROLLGROUPID).toString().isEmpty()){
					 			List<PayrollGroupDO> payrollGroupList = payrollGroupService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.PAYROLLGROUPID).toString()));
						 		if(payrollGroupList != null && payrollGroupList.size() >0){
						 			payrollGroupWorkingdaysAllowanceDO.setPayrollGroup(payrollGroupList.get(0));
						 		}
					 		}
					 		if(resultJSONArray1.get(i) != null ){
					 			List<WorkingDayAllowanceDO> workingDayList = workingDayAllowanceService.retrieveById(Long.parseLong(resultJSONArray1.get(i).toString()));
						 		if(workingDayList != null && workingDayList.size() > 0){
						 			payrollGroupWorkingdaysAllowanceDO.setWorkingDayAllowance(workingDayList.get(0));
					 			}
					 		}
					 		payrollGroupWorkingdaysAllowanceDO.setStatus(CommonConstants.ACTIVE);
					 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			payrollGroupWorkingdaysAllowanceDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			payrollGroupWorkingdaysAllowanceDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 		}
					 		payrollGroupWorkingdaysAllowanceDO.setUpdatedon(new Date());
					 		payrollGroupWorkingdaysAllowanceDO.setCreatedon(new Date());
					 		
					 		if(!payrollGroupWorkingDayService.persist(payrollGroupWorkingdaysAllowanceDO)){
					 			//return CommonWebUtil.buildErrorResponse("Payroll Group Already Added").toString();
							}
				 		}
			 		}
			 		
			 		if (inputJSON.get(CommonConstants.ATTENDANCEALLOWANCEID) != null && !inputJSON.get(CommonConstants.ATTENDANCEALLOWANCEID).toString().isEmpty()){
				 		JSONArray resultJSONArray = new JSONArray();
				 		resultJSONArray.put(inputJSON.get(CommonConstants.ATTENDANCEALLOWANCEID));
				 		String rec = (String) resultJSONArray.get(0).toString();
				 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
				 		for (int i = 0; i < resultJSONArray1.size(); i++) {
				 			PayrollGroupAttendanceAllowanceDO payrollGroupAttendanceAllowanceDO = new PayrollGroupAttendanceAllowanceDO();
					 		if(inputJSON.get(CommonConstants.PAYROLLGROUPID) != null && !inputJSON.get(CommonConstants.PAYROLLGROUPID).toString().isEmpty()){
					 			List<PayrollGroupDO> payrollGroupList = payrollGroupService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.PAYROLLGROUPID).toString()));
						 		if(payrollGroupList != null && payrollGroupList.size() >0){
						 			payrollGroupAttendanceAllowanceDO.setPayrollGroup(payrollGroupList.get(0));
						 		}
					 		}
					 		if(resultJSONArray1.get(i) != null ){
					 			List<AttendanceAllowanceDO> attendanceList = attendanceAllowanceService.retrieveById(Long.parseLong(resultJSONArray1.get(i).toString()));
						 		if(attendanceList != null && attendanceList.size() > 0){
						 			payrollGroupAttendanceAllowanceDO.setAttendanceAllowance(attendanceList.get(0));
					 			}
					 		}
					 		payrollGroupAttendanceAllowanceDO.setStatus(CommonConstants.ACTIVE);
					 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			payrollGroupAttendanceAllowanceDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			payrollGroupAttendanceAllowanceDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 		}
					 		payrollGroupAttendanceAllowanceDO.setUpdatedon(new Date());
					 		payrollGroupAttendanceAllowanceDO.setCreatedon(new Date());
					 		
					 		if(!payrollGroupAttendanceAllowanceService.persist(payrollGroupAttendanceAllowanceDO)){
					 			//return CommonWebUtil.buildErrorResponse("Payroll Group Already Added").toString();
							}
				 		}
			 		}
			 		
			 		
			 		
			 		
			 		if (inputJSON.get(CommonConstants.TDSID) != null && !inputJSON.get(CommonConstants.TDSID).toString().isEmpty()){
			 			if(inputJSON.get(CommonConstants.TDSID).toString().equalsIgnoreCase(CommonConstants.TRUE)){
				 			PayrollGroupTaxDO payrollGroupTaxDO = new PayrollGroupTaxDO();
				 			List<PayrollGroupTaxDO> tdsList = payrollGroupTaxService.retrieveByTDS(Long.parseLong(inputJSON.get(CommonConstants.PAYROLLGROUPID).toString()));
		 					if(tdsList == null || tdsList.size() == 0){
						 		if(inputJSON.get(CommonConstants.PAYROLLGROUPID) != null && !inputJSON.get(CommonConstants.PAYROLLGROUPID).toString().isEmpty()){
						 			List<PayrollGroupDO> payrollGroupList = payrollGroupService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.PAYROLLGROUPID).toString()));
							 		if(payrollGroupList != null && payrollGroupList.size() >0){
							 			payrollGroupTaxDO.setPayrollGroup(payrollGroupList.get(0));
							 		}
						 		}
						 		payrollGroupTaxDO.setTds(CommonConstants.APPLY);
						 		payrollGroupTaxDO.setStatus(CommonConstants.ACTIVE);
						 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
						 			payrollGroupTaxDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
						 			payrollGroupTaxDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
						 		}
						 		payrollGroupTaxDO.setUpdatedon(new Date());
						 		payrollGroupTaxDO.setCreatedon(new Date());
						 		
						 		if(!payrollGroupTaxService.persistWithoutDuplicateCheck(payrollGroupTaxDO)){
						 			//return CommonWebUtil.buildErrorResponse("Payroll Group Already Added").toString();
								}
		 					}else{
		 						payrollGroupTaxDO = tdsList.get(0);
		 						payrollGroupTaxDO.setStatus(CommonConstants.ACTIVE);
		 						if(!payrollGroupTaxService.update(payrollGroupTaxDO)){
		 							
		 						}
		 					}
			 			}else{
			 				if(inputJSON.get(CommonConstants.TDSID).toString().equalsIgnoreCase(CommonConstants.FALSE)){
			 					PayrollGroupTaxDO payrollGroupTDSTaxDO = new PayrollGroupTaxDO();
			 					List<PayrollGroupTaxDO> tdsList = payrollGroupTaxService.retrieveByTDS(Long.parseLong(inputJSON.get(CommonConstants.PAYROLLGROUPID).toString()));
			 					if(tdsList != null && tdsList.size() > 0){
			 						payrollGroupTDSTaxDO = tdsList.get(0);
			 						payrollGroupTDSTaxDO.setStatus(CommonConstants.INACTIVE);
			 						if(!payrollGroupTaxService.update(payrollGroupTDSTaxDO)){
			 							
			 						}
			 					}
			 				}
			 			}
			 		}
			 		if (inputJSON.get(CommonConstants.PT_ID) != null && !inputJSON.get(CommonConstants.PT_ID).toString().isEmpty()){
			 			if(inputJSON.get(CommonConstants.PT_ID).toString().equalsIgnoreCase(CommonConstants.TRUE)){
				 			PayrollGroupTaxDO payrollGroupTaxDO = new PayrollGroupTaxDO();
				 			List<PayrollGroupTaxDO> ptList = payrollGroupTaxService.retrieveByPt(Long.parseLong(inputJSON.get(CommonConstants.PAYROLLGROUPID).toString()));
		 					if(ptList == null || ptList.size() == 0){
		 						if(inputJSON.get(CommonConstants.PAYROLLGROUPID) != null && !inputJSON.get(CommonConstants.PAYROLLGROUPID).toString().isEmpty()){
						 			List<PayrollGroupDO> payrollGroupList = payrollGroupService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.PAYROLLGROUPID).toString()));
							 		if(payrollGroupList != null && payrollGroupList.size() >0){
							 			payrollGroupTaxDO.setPayrollGroup(payrollGroupList.get(0));
							 		}
						 		}
						 		payrollGroupTaxDO.setPt(CommonConstants.APPLY);
						 		payrollGroupTaxDO.setStatus(CommonConstants.ACTIVE);
						 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
						 			payrollGroupTaxDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
						 			payrollGroupTaxDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
						 		}
						 		payrollGroupTaxDO.setUpdatedon(new Date());
						 		payrollGroupTaxDO.setCreatedon(new Date());
						 		
						 		if(!payrollGroupTaxService.persistWithoutDuplicateCheck(payrollGroupTaxDO)){
						 			//return CommonWebUtil.buildErrorResponse("Payroll Group Already Added").toString();
								}
		 					}else{
		 						payrollGroupTaxDO = ptList.get(0);
		 						payrollGroupTaxDO.setStatus(CommonConstants.ACTIVE);
		 						if(!payrollGroupTaxService.update(payrollGroupTaxDO)){
		 							
		 						}
		 					}
			 			}else{
			 				if(inputJSON.get(CommonConstants.PT_ID).toString().equalsIgnoreCase(CommonConstants.FALSE)){
			 					PayrollGroupTaxDO payrollGroupTDSTaxDO = new PayrollGroupTaxDO();
			 					List<PayrollGroupTaxDO> ptList = payrollGroupTaxService.retrieveByPt(Long.parseLong(inputJSON.get(CommonConstants.PAYROLLGROUPID).toString()));
			 					if(ptList != null && ptList.size() > 0){
			 						payrollGroupTDSTaxDO = ptList.get(0);
			 						payrollGroupTDSTaxDO.setStatus(CommonConstants.INACTIVE);
			 						if(!payrollGroupTaxService.update(payrollGroupTDSTaxDO)){
			 							
			 						}
			 					}
			 				}
			 			}
			 		}
			 		
			 		if (inputJSON.get(CommonConstants.EDUCATIONCESS) != null && !inputJSON.get(CommonConstants.EDUCATIONCESS).toString().isEmpty()){
			 			if(inputJSON.get(CommonConstants.EDUCATIONCESS).toString().equalsIgnoreCase(CommonConstants.TRUE)){
			 				PayrollGroupTaxDO payrollGroupTaxDO = new PayrollGroupTaxDO();
			 				List<PayrollGroupTaxDO> eduCess = payrollGroupTaxService.retrieveByEducationCess(Long.parseLong(inputJSON.get(CommonConstants.PAYROLLGROUPID).toString()));
		 					if(eduCess == null || eduCess.size() == 0){
		 						if(inputJSON.get(CommonConstants.PAYROLLGROUPID) != null && !inputJSON.get(CommonConstants.PAYROLLGROUPID).toString().isEmpty()){
						 			List<PayrollGroupDO> payrollGroupList = payrollGroupService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.PAYROLLGROUPID).toString()));
							 		if(payrollGroupList != null && payrollGroupList.size() >0){
							 			payrollGroupTaxDO.setPayrollGroup(payrollGroupList.get(0));
							 		}
						 		}
						 		payrollGroupTaxDO.setEducationCess(CommonConstants.APPLY);
						 		payrollGroupTaxDO.setStatus(CommonConstants.ACTIVE);
						 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
						 			payrollGroupTaxDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
						 			payrollGroupTaxDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
						 		}
						 		payrollGroupTaxDO.setUpdatedon(new Date());
						 		payrollGroupTaxDO.setCreatedon(new Date());
						 		
						 		if(!payrollGroupTaxService.persistWithoutDuplicateCheck(payrollGroupTaxDO)){
						 			//return CommonWebUtil.buildErrorResponse("Payroll Group Already Added").toString();
								}
		 					}else{
		 						payrollGroupTaxDO = eduCess.get(0);
		 						payrollGroupTaxDO.setStatus(CommonConstants.ACTIVE);
		 						if(!payrollGroupTaxService.update(payrollGroupTaxDO)){
		 							
		 						}
		 					}
			 			}else{
			 				if(inputJSON.get(CommonConstants.EDUCATIONCESS).toString().equalsIgnoreCase(CommonConstants.FALSE)){
			 					PayrollGroupTaxDO payrollGroupTDSTaxDO = new PayrollGroupTaxDO();
			 					List<PayrollGroupTaxDO> eduCess = payrollGroupTaxService.retrieveByEducationCess(Long.parseLong(inputJSON.get(CommonConstants.PAYROLLGROUPID).toString()));
			 					if(eduCess != null && eduCess.size() > 0){
			 						payrollGroupTDSTaxDO = eduCess.get(0);
			 						payrollGroupTDSTaxDO.setStatus(CommonConstants.INACTIVE);
			 						if(!payrollGroupTaxService.update(payrollGroupTDSTaxDO)){
			 							
			 						}
			 					}
			 				}
			 			}
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.DEACTIVATEDEARNINGMENU) != null && !inputJSON.get(CommonConstants.DEACTIVATEDEARNINGMENU).toString().isEmpty()){
			 			JSONArray resultJSONArray = new JSONArray();
				 		resultJSONArray.put(inputJSON.get(CommonConstants.DEACTIVATEDEARNINGMENU));
				 		String rec = (String) resultJSONArray.get(0).toString();
				 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
				 		List<PayrollGroupEarningsDO> payrollGroupDOlist = new ArrayList<PayrollGroupEarningsDO>();
				 		for (int i = 0; i < resultJSONArray1.size(); i++) {
				 			PayrollGroupEarningsDO payrollGrpDO = new PayrollGroupEarningsDO();
				 			List<PayrollGroupEarningsDO> payrollGroupEarningList = payrollGroupEarningService.retrieveByID(Long.parseLong(resultJSONArray1.get(i).toString()));
				 			if(payrollGroupEarningList != null && payrollGroupEarningList.size() > 0){
				 				payrollGrpDO = payrollGroupEarningList.get(0);
				 				payrollGrpDO.setStatus(CommonConstants.INACTIVE);
				 				payrollGroupDOlist.add(payrollGrpDO);
				 			}
				 		}
				 		payrollGroupEarningService.updateList(payrollGroupDOlist);
			 			
			 		}
			 		if(inputJSON.get(CommonConstants.DEACTIVATEDDEDUCTIONMENU) != null && !inputJSON.get(CommonConstants.DEACTIVATEDDEDUCTIONMENU).toString().isEmpty()){
			 			JSONArray resultJSONArray = new JSONArray();
				 		resultJSONArray.put(inputJSON.get(CommonConstants.DEACTIVATEDDEDUCTIONMENU));
				 		String rec = (String) resultJSONArray.get(0).toString();
				 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
				 		List<PayrollGroupDeductionDO> payrollGroupDOlist = new ArrayList<PayrollGroupDeductionDO>();
				 		for (int i = 0; i < resultJSONArray1.size(); i++) {
				 			PayrollGroupDeductionDO payrollGrpdedctionDO = new PayrollGroupDeductionDO();
				 			List<PayrollGroupDeductionDO> payrollGroupDeductionList = payrollGroupDeductionService.retrieveByID(Long.parseLong(resultJSONArray1.get(i).toString()));
				 			if(payrollGroupDeductionList != null && payrollGroupDeductionList.size() > 0){
				 				payrollGrpdedctionDO = payrollGroupDeductionList.get(0);
				 				payrollGrpdedctionDO.setStatus(CommonConstants.INACTIVE);
				 				payrollGroupDOlist.add(payrollGrpdedctionDO);
				 			}
				 		}
				 		payrollGroupDeductionService.updateList(payrollGroupDOlist);
			 			
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.DEACTIVATEDWORKALLOWANCEMENU) != null && !inputJSON.get(CommonConstants.DEACTIVATEDWORKALLOWANCEMENU).toString().isEmpty()){
			 			JSONArray resultJSONArray = new JSONArray();
				 		resultJSONArray.put(inputJSON.get(CommonConstants.DEACTIVATEDWORKALLOWANCEMENU));
				 		String rec = (String) resultJSONArray.get(0).toString();
				 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
				 		List<PayrollGroupWorkingdaysAllowanceDO> payrollGroupDOlist = new ArrayList<PayrollGroupWorkingdaysAllowanceDO>();
				 		for (int i = 0; i < resultJSONArray1.size(); i++) {
				 			PayrollGroupWorkingdaysAllowanceDO payrollGrpWorkingdayallowanceDO = new PayrollGroupWorkingdaysAllowanceDO();
				 			List<PayrollGroupWorkingdaysAllowanceDO> payrollGroupWorkingdayList = payrollGroupWorkingDayService.retrieveByID(Long.parseLong(resultJSONArray1.get(i).toString()));
				 			if(payrollGroupWorkingdayList != null && payrollGroupWorkingdayList.size() > 0){
				 				payrollGrpWorkingdayallowanceDO = payrollGroupWorkingdayList.get(0);
				 				payrollGrpWorkingdayallowanceDO.setStatus(CommonConstants.INACTIVE);
				 				payrollGroupDOlist.add(payrollGrpWorkingdayallowanceDO);
				 			}
				 		}
				 		payrollGroupWorkingDayService.updateList(payrollGroupDOlist);
			 			
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.DEACTIVATEDATTENDANCEALLOWANCEMENU) != null && !inputJSON.get(CommonConstants.DEACTIVATEDATTENDANCEALLOWANCEMENU).toString().isEmpty()){
			 			JSONArray resultJSONArray = new JSONArray();
				 		resultJSONArray.put(inputJSON.get(CommonConstants.DEACTIVATEDATTENDANCEALLOWANCEMENU));
				 		String rec = (String) resultJSONArray.get(0).toString();
				 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
				 		List<PayrollGroupAttendanceAllowanceDO> payrollGroupDOlist = new ArrayList<PayrollGroupAttendanceAllowanceDO>();
				 		for (int i = 0; i < resultJSONArray1.size(); i++) {
				 			PayrollGroupAttendanceAllowanceDO payrollGrpAttendanceallowanceDO = new PayrollGroupAttendanceAllowanceDO();
				 			List<PayrollGroupAttendanceAllowanceDO> payrollGroupAttendanceList = payrollGroupAttendanceAllowanceService.retrieveByID(Long.parseLong(resultJSONArray1.get(i).toString()));
				 			if(payrollGroupAttendanceList != null && payrollGroupAttendanceList.size() > 0){
				 				payrollGrpAttendanceallowanceDO = payrollGroupAttendanceList.get(0);
				 				payrollGrpAttendanceallowanceDO.setStatus(CommonConstants.INACTIVE);
				 				payrollGroupDOlist.add(payrollGrpAttendanceallowanceDO);
				 			}
				 		}
				 		payrollGroupAttendanceAllowanceService.updateList(payrollGroupDOlist);
			 			
			 		}
			 		
			 		
			 		if(inputJSON.get(CommonConstants.DEACTIVATEDTAXMENU) != null && !inputJSON.get(CommonConstants.DEACTIVATEDTAXMENU).toString().isEmpty()){
			 			JSONArray resultJSONArray = new JSONArray();
				 		resultJSONArray.put(inputJSON.get(CommonConstants.DEACTIVATEDTAXMENU));
				 		String rec = (String) resultJSONArray.get(0).toString();
				 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
				 		List<PayrollGroupTaxDO> payrollGroupTaxDOlist = new ArrayList<PayrollGroupTaxDO>();
				 		for (int i = 0; i < resultJSONArray1.size(); i++) {
				 			PayrollGroupTaxDO payrollGrpTaxDO = new PayrollGroupTaxDO();
				 			List<PayrollGroupTaxDO> payrollGroupTaxList = payrollGroupTaxService.retrieveByID(Long.parseLong(resultJSONArray1.get(i).toString()));
				 			if(payrollGroupTaxList != null && payrollGroupTaxList.size() > 0){
				 				payrollGrpTaxDO = payrollGroupTaxList.get(0);
				 				payrollGrpTaxDO.setStatus(CommonConstants.INACTIVE);
				 				payrollGroupTaxDOlist.add(payrollGrpTaxDO);
				 			}
				 		}
				 		payrollGroupTaxService.updateList(payrollGroupTaxDOlist);
			 		}
			 	}
				
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Payroll Group Settings Updated");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
			 		
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveByGroupId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<PayrollGroupDO> payrollGroupList = payrollGroupService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = PayrollGroupUtil.getPayrollGroupListDetails(payrollGroupList).toString();
					/*List<PayrollGroupEarningsDO> payrollGroupEarningList = payrollGroupEarningService.retrieveByGroupID(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					List<PayrollGroupDeductionDO> payrollGroupDeductionList = payrollGroupDeductionService.retrieveByGroupID(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					List<PayrollGroupTaxDO> payrollGroupTaxList = payrollGroupTaxService.retrieveByGroupID(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = PayrollGroupSettingsUtil.getPayrollGroupSettingsList(payrollGroupEarningList,payrollGroupDeductionList,payrollGroupTaxList).toString();*/
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	/*@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<PayrollGroupDO> payrollGroupList = payrollGroupService.retrieve();
				respJSON = PayrollGroupUtil.getPayrollGroupList(payrollGroupList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}*/
/*	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				PayrollGroupDO payrollGroupDO = new PayrollGroupDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<PayrollGroupDO> payrollGroupList = payrollGroupService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(payrollGroupList != null && payrollGroupList.size() >0){
			 			payrollGroupDO = payrollGroupList.get(0);
				 		if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
				 			payrollGroupDO.setName(inputJSON.get(CommonConstants.NAME).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.DESCRIPTION) != null && !inputJSON.get(CommonConstants.DESCRIPTION).toString().isEmpty()){
				 			payrollGroupDO.setDescription(inputJSON.get(CommonConstants.DESCRIPTION).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
				 			payrollGroupDO.setStatus((char) inputJSON.get(CommonConstants.STATUS).toString().charAt(0));
				 		}
				 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			payrollGroupDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		payrollGroupDO.setUpdatedon(new Date());
				 		if(!payrollGroupService.update(payrollGroupDO)){
				 			return CommonWebUtil.buildErrorResponse("Payroll Group Already Added").toString();
				 		}
				 		CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Payroll Group Updated");
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}*/
}
