package com.spheresuite.erp.rs;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.CompOffExpiryDO;
import com.spheresuite.erp.domainobject.EmployeeCompOffDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.RolesDO;
import com.spheresuite.erp.service.CompOffExpiryService;
import com.spheresuite.erp.service.EmployeeCompOffService;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.LeaveRequestsService;
import com.spheresuite.erp.service.RolesService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.EmployeeCompOffUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/employeecompoff")
public class EmployeeCompOffRS {

	String validation = null;
	static Logger logger = Logger.getLogger(EmployeeCompOffRS.class.getName());
	@Autowired
	private EmployeeCompOffService employeeCompOffService;
	
	@Autowired
	private EmployeeService employeeService;
	
	@Autowired
	private LeaveRequestsService leaveRequestsService;
	
	@Autowired
	private CompOffExpiryService compOffExpiryService;
	
	@Autowired
	private RolesService rolesService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeCompOffDO employeeCompOffDO = new EmployeeCompOffDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.EMPID) != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
				 		if(employeeList != null && employeeList.size() > 0){
				 			employeeCompOffDO.setEmployee(employeeList.get(0));
				 			if(inputJSON.get(CommonConstants.APPLYDATE) != null && !inputJSON.get(CommonConstants.APPLYDATE).toString().isEmpty()){
					 			employeeCompOffDO.setApplyDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.APPLYDATE).toString()));
					 			Calendar c = Calendar.getInstance();
					 			c.setTime(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.APPLYDATE).toString()));
					 			
					 			List<CompOffExpiryDO> compOffExpiry = compOffExpiryService.retrieve();
					 			if(compOffExpiry != null && compOffExpiry.size() > 0){
					 				c.add(Calendar.DAY_OF_MONTH, compOffExpiry.get(0).getNoofdays().intValue());  
						 			employeeCompOffDO.setExpiryDate(c.getTime());
					 			}else{
					 				employeeCompOffDO.setExpiryDate(null);
					 			}
					 			
					 		}
					 		if(inputJSON.get(CommonConstants.COMPOFFTYPE) != null && !inputJSON.get(CommonConstants.COMPOFFTYPE).toString().isEmpty()){
					 			employeeCompOffDO.setCompOffType(inputJSON.get(CommonConstants.COMPOFFTYPE).toString().charAt(0));
		 			 		}
					 		
					 		employeeCompOffDO.setStatus('p');
					 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			employeeCompOffDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			employeeCompOffDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 		}
					 		employeeCompOffDO.setUpdatedon(new Date());
					 		employeeCompOffDO.setCreatedon(new Date());
					 		if(!employeeCompOffService.persist(employeeCompOffDO)){
					 			return CommonWebUtil.buildErrorResponse("Employee CompOff Already Added").toString();
							}
							CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Employee CompOff Created");
				 		}
			 		}
			 		
			 	}
				
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<EmployeeCompOffDO> categoryList = employeeCompOffService.retrieve();
				respJSON = EmployeeCompOffUtil.getEmployeeCompOffList(categoryList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveByEmpId/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByEmpId(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<EmployeeCompOffDO> employeeList = employeeCompOffService.retrieveByEmpId(inputJSON.get(CommonConstants.ID).toString());
					respJSON = EmployeeCompOffUtil.getEmployeeCompOffList(employeeList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	
	@RequestMapping(value = "/retrieveByReportingId/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByReportingId(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && inputJSON.get(CommonConstants.ID)!= null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()
						&& inputJSON.get(CommonConstants.ROLE_ID)!= null && !inputJSON.get(CommonConstants.ROLE_ID).toString().isEmpty()){
					List<RolesDO> rolesList =  rolesService.retriveById(Long.parseLong(inputJSON.get(CommonConstants.ROLE_ID).toString()));
					if(rolesList != null && rolesList.size() > 0){
						if(rolesList.get(0).getName().equalsIgnoreCase(CommonConstants.SUPERADMIN)){
							List<EmployeeCompOffDO> employeeList = employeeCompOffService.retrieve();
							respJSON = EmployeeCompOffUtil.getEmployeeCompOffList(employeeList).toString();
						}else {
							List<EmployeeCompOffDO> employeeList = employeeCompOffService.retrieveByReportingId(inputJSON.get(CommonConstants.ID).toString());
							respJSON = EmployeeCompOffUtil.getEmployeeCompOffList(employeeList).toString();
						}
					}else {
						List<EmployeeCompOffDO> employeeList = employeeCompOffService.retrieveByReportingId(inputJSON.get(CommonConstants.ID).toString());
						respJSON = EmployeeCompOffUtil.getEmployeeCompOffList(employeeList).toString();
					}
					
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	
	
	
	@RequestMapping(value = "/retrieveEmpCompOff", method = RequestMethod.POST)
	public @ResponseBody String retrieveEmpCompOff(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty() && inputJSON.get(CommonConstants.APPLYDATE) != null && !inputJSON.get(CommonConstants.APPLYDATE).toString().isEmpty()){
			 		List<EmployeeCompOffDO> compoffList = employeeCompOffService.retrieveByEmpIdValidCompOff(inputJSON.get(CommonConstants.EMPID).toString(), 
			 				CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.APPLYDATE).toString()));
			 		respJSON = EmployeeCompOffUtil.getEmployeeCompOffList(compoffList).toString();
			 		
			 		/*	if(compoffList != null && compoffList.size() > 0){
			 			List<EmployeeCompOffDO> validCompoffList = new ArrayList<>();
			 			for(EmployeeCompOffDO compoff: compoffList){
			 				if(compoff.getApplyDate() != null && compoff.getExpiryDate() != null){
			 					List<LeaveRequestsDO>  leaveRequestList = leaveRequestsService.empCompoffValidity(inputJSON.get(CommonConstants.EMPID).toString(),compoff.getApplyDate(),compoff.getExpiryDate());
			 					if(leaveRequestList == null || leaveRequestList.size() <= 0){
			 						
			 						validCompoffList.add(compoff);
			 					}
			 				}
			 			}
			 			respJSON = EmployeeCompOffUtil.getEmployeeCompOffList(validCompoffList).toString();
			 		}else{
			 			respJSON = EmployeeCompOffUtil.getEmployeeCompOffList(compoffList).toString();
			 		}*/
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	

	@RequestMapping(value = "/retrieveEmpCompOffWithType", method = RequestMethod.POST)
	public @ResponseBody String retrieveEmpCompOffWithType(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty() && inputJSON.get(CommonConstants.APPLYDATE) != null && !inputJSON.get(CommonConstants.APPLYDATE).toString().isEmpty()  && inputJSON.get(CommonConstants.COMPOFFTYPE) != null && !inputJSON.get(CommonConstants.COMPOFFTYPE).toString().isEmpty()){
			 		List<EmployeeCompOffDO> compoffList = employeeCompOffService.retrieveByEmpIdValidCompOffWithType(inputJSON.get(CommonConstants.EMPID).toString(), 
			 				CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.APPLYDATE).toString()),inputJSON.get(CommonConstants.COMPOFFTYPE).toString().charAt(0));
			 		respJSON = EmployeeCompOffUtil.getEmployeeCompOffList(compoffList).toString();
			 		/*if(compoffList != null && compoffList.size() > 0){
			 			List<EmployeeCompOffDO> validCompoffList = new ArrayList<>();
			 			for(EmployeeCompOffDO compoff: compoffList){
			 				if(compoff.getApplyDate() != null && compoff.getExpiryDate() != null){
			 					String compOffType = "c"+compoff.getCompOffType();
			 					List<LeaveRequestsDO>  leaveRequestList = leaveRequestsService.empCompoffValidity(inputJSON.get(CommonConstants.EMPID).toString(),compoff.getApplyDate(),compoff.getExpiryDate(),compOffType);
			 					if(leaveRequestList == null || leaveRequestList.size() <= 0){
			 						validCompoffList.add(compoff);
			 					}else{
			 						for(LeaveRequestsDO leaveRequest: leaveRequestList){
			 							if(leaveRequest.getType().charAt(1) != compoff.getCompOffType()){
			 								validCompoffList.add(compoff);
			 							}
			 						}
			 					}
			 				}else{
			 					if(compoff.getExpiryDate() == null){
			 						String compOffType = "c"+compoff.getCompOffType();
			 						List<LeaveRequestsDO>  leaveRequestList = leaveRequestsService.empCompoffValidityWithoutExpiry(inputJSON.get(CommonConstants.EMPID).toString(),compoff.getApplyDate(),compOffType);
				 					if(leaveRequestList == null || leaveRequestList.size() <= 0){
				 						validCompoffList.add(compoff);
				 					}else{
				 						for(LeaveRequestsDO leaveRequest: leaveRequestList){
				 							if(leaveRequest.getType().charAt(1) != compoff.getCompOffType()){
				 								validCompoffList.add(compoff);
				 							}
				 						}
				 					}
			 					}
			 				}
			 			}
			 			respJSON = EmployeeCompOffUtil.getEmployeeCompOffList(validCompoffList).toString();
			 		}else{
			 			respJSON = EmployeeCompOffUtil.getEmployeeCompOffList(compoffList).toString();
			 		}*/
			 		
			 		
			 		
			 		
			 		//respJSON = EmployeeCompOffUtil.getEmployeeCompOffList(categoryList).toString();
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeCompOffDO employeeCompOffDO = new EmployeeCompOffDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<EmployeeCompOffDO> earningTypeList = employeeCompOffService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(earningTypeList != null && earningTypeList.size() >0){
				 		employeeCompOffDO = earningTypeList.get(0);
				 		if(inputJSON.get(CommonConstants.APPLYDATE) != null && !inputJSON.get(CommonConstants.APPLYDATE).toString().isEmpty()){
				 			employeeCompOffDO.setApplyDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.APPLYDATE).toString()));
				 			
				 			employeeCompOffDO.setApplyDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.APPLYDATE).toString()));
				 			Calendar c = Calendar.getInstance();
				 			c.setTime(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.APPLYDATE).toString()));
				 			
				 			List<CompOffExpiryDO> compOffExpiry = compOffExpiryService.retrieve();
				 			if(compOffExpiry != null && compOffExpiry.size() > 0){
				 				c.add(Calendar.DAY_OF_MONTH, compOffExpiry.get(0).getNoofdays().intValue());  
					 			employeeCompOffDO.setExpiryDate(c.getTime());
				 			}else{
				 				employeeCompOffDO.setExpiryDate(null);
				 			}
				 		}
				 		
				 		if(inputJSON.get(CommonConstants.EMPID) != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
				 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
					 		if(employeeList != null && employeeList.size() > 0){
					 			employeeCompOffDO.setEmployee(employeeList.get(0));
					 		}
				 		}
				 		
				 		if(inputJSON.get(CommonConstants.COMPOFFTYPE) != null && !inputJSON.get(CommonConstants.COMPOFFTYPE).toString().isEmpty()){
				 			employeeCompOffDO.setCompOffType(inputJSON.get(CommonConstants.COMPOFFTYPE).toString().charAt(0));
	 			 		}
				 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			employeeCompOffDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		employeeCompOffDO.setUpdatedon(new Date());
				 		employeeCompOffService.update(employeeCompOffDO);
				 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Employee CompOff Updated");
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/approve", method = RequestMethod.POST)
	public @ResponseBody String approve(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeCompOffDO employeeCompOffDO = new EmployeeCompOffDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<EmployeeCompOffDO> earningTypeList = employeeCompOffService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(earningTypeList != null && earningTypeList.size() >0){
				 		employeeCompOffDO = earningTypeList.get(0);
				 		
				 		if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
				 			employeeCompOffDO.setStatus(inputJSON.get(CommonConstants.STATUS).toString().charAt(0));
				 		}
				 		
				 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			employeeCompOffDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		employeeCompOffDO.setUpdatedon(new Date());
				 		employeeCompOffService.update(employeeCompOffDO);
				 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Employee CompOff Updated");
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/importCompOff", method = RequestMethod.POST)
	public @ResponseBody String importCompOff(Model model, HttpServletRequest request) {	
		try {
			//if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
				org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
				JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
				List<EmployeeCompOffDO>employeeCompOffDOList = new ArrayList<EmployeeCompOffDO>();
				
				Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY);
				for (int i=0; i < fileData.size(); i++){
					JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
					EmployeeCompOffDO employeeCompOffDO = new EmployeeCompOffDO();
					if(colName.get(CommonConstants.EMPID) != null){
						if(rowJSON.get(colName.get(CommonConstants.EMPID)) != null && !rowJSON.get(colName.get(CommonConstants.EMPID)).toString().isEmpty()){
				 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(rowJSON.get(colName.get(CommonConstants.EMPID)).toString());
					 		if(employeeList != null && employeeList.size() > 0){
					 			employeeCompOffDO.setEmployee(employeeList.get(0));
					 			
								if(colName.get(CommonConstants.APPLYDATE) != null){
							 		if(!rowJSON.get(colName.get(CommonConstants.APPLYDATE)).toString().isEmpty()){
							 			employeeCompOffDO.setApplyDate(CommonUtil.convertStringToSqlDate(rowJSON != null ? rowJSON.get(colName.get(CommonConstants.APPLYDATE)).toString() : null));
							 			Calendar c = Calendar.getInstance();
							 			c.setTime(CommonUtil.convertStringToSqlDate(rowJSON.get(colName.get(CommonConstants.APPLYDATE)).toString()));
							 			
							 			List<CompOffExpiryDO> compOffExpiry = compOffExpiryService.retrieve();
							 			if(compOffExpiry != null && compOffExpiry.size() > 0){
							 				c.add(Calendar.DAY_OF_MONTH, compOffExpiry.get(0).getNoofdays().intValue());  
								 			employeeCompOffDO.setExpiryDate(c.getTime());
							 			}else{
							 				employeeCompOffDO.setExpiryDate(null);
							 			}
							 		}
								}
								
								if(colName.get(CommonConstants.STATUS) != null){
									if(!rowJSON.get(colName.get(CommonConstants.STATUS)).toString().isEmpty()){
											employeeCompOffDO.setStatus(rowJSON.get(colName.get(CommonConstants.STATUS)).toString().charAt(0));
							 		}
								}
								
								if(colName.get(CommonConstants.COMPOFFTYPE) != null){
									if(!rowJSON.get(colName.get(CommonConstants.COMPOFFTYPE)).toString().isEmpty()){
											employeeCompOffDO.setCompOffType(rowJSON.get(colName.get(CommonConstants.COMPOFFTYPE)).toString().charAt(0));
							 		}
								}
								
						 		if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
						 			employeeCompOffDO.setUpdatedby(updatedBy.toString());
						 			employeeCompOffDO.setCreatedby(updatedBy.toString());
						 		}
						 		employeeCompOffDO.setCreatedon(new Date());
						 		employeeCompOffDO.setUpdatedon(new Date());
						 		employeeCompOffDOList.add(employeeCompOffDO);
					 		}
				 		}
					}
				}
				employeeCompOffService.persist(employeeCompOffDOList);
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
}
}
