package com.spheresuite.erp.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.EarningSubTypeDO;
import com.spheresuite.erp.domainobject.EarningTypeDO;
import com.spheresuite.erp.service.EarningSubTypeService;
import com.spheresuite.erp.service.EarningTypeService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.EarningSubTypeUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/earningsubtype")
public class EarningSubTypeRS {

	String validation = null;
	static Logger logger = Logger.getLogger(EarningSubTypeRS.class.getName());
	@Autowired
	private EarningSubTypeService earningSubTypeService;
	
	@Autowired
	private EarningTypeService earningTypeService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EarningSubTypeDO earningTypeDO = new EarningSubTypeDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.EARNINGTYPEID) != null && !inputJSON.get(CommonConstants.EARNINGTYPEID).toString().isEmpty()){
			 			List<EarningTypeDO> earningTypeList = earningTypeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.EARNINGTYPEID).toString()));
			 			if(earningTypeList != null && earningTypeList.size() >0){
			 				earningTypeDO.setEarningType(earningTypeList.get(0));
			 				if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			earningTypeDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			earningTypeDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
						 			earningTypeDO.setName(inputJSON.get(CommonConstants.NAME).toString());
						 			earningTypeDO.setStatus(CommonConstants.ACTIVE);
							 		earningTypeDO.setUpdatedon(new Date());
							 		earningTypeDO.setCreatedon(new Date());
							 		if(!earningSubTypeService.persist(earningTypeDO)){
							 			return CommonWebUtil.buildErrorResponse("Earning SubType Already Added").toString();
									}
									CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Earning SubType Created");
						 		}
					 		}
			 			}
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<EarningSubTypeDO> cagtegoryList = earningSubTypeService.retrieveActive();
				respJSON = EarningSubTypeUtil.getEarningTypeList(cagtegoryList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<EarningSubTypeDO> categoryList = earningSubTypeService.retrieve();
				respJSON = EarningSubTypeUtil.getEarningTypeList(categoryList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveByEarningSubTypeId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByDeductionTypeId(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<EarningSubTypeDO> loanTypeList = earningSubTypeService.retrieveByEarningId(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = EarningSubTypeUtil.getEarningTypeList(loanTypeList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EarningSubTypeDO earningTypeDO = new EarningSubTypeDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<EarningSubTypeDO> earningTypeList = earningSubTypeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(earningTypeList != null && earningTypeList.size() >0){
				 		earningTypeDO = earningTypeList.get(0);
				 		if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
				 			earningTypeDO.setName(inputJSON.get(CommonConstants.NAME).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.EARNINGTYPEID) != null && !inputJSON.get(CommonConstants.EARNINGTYPEID).toString().isEmpty()){
				 			List<EarningTypeDO> earningList = earningTypeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.EARNINGTYPEID).toString()));
				 			if(earningList != null && earningList.size() >0){
				 				earningTypeDO.setEarningType(earningList.get(0));
				 			}
				 		}
				 		if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
				 			earningTypeDO.setStatus((char) inputJSON.get(CommonConstants.STATUS).toString().charAt(0));
				 		}
				 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			earningTypeDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		earningTypeDO.setUpdatedon(new Date());
				 		if(!earningSubTypeService.update(earningTypeDO)){
				 			return CommonWebUtil.buildErrorResponse("Earning SubType Already Added").toString();
				 		}
				 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Earning SubType Updated");
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/importEarningType", method = RequestMethod.POST)
	public @ResponseBody String importEarningType(Model model, HttpServletRequest request) {
		try {
			JSONObject inputJSON =  CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
			JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
			List<EarningSubTypeDO> earningTypelist = new ArrayList<EarningSubTypeDO>();
			Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY);
			for (int i=0; i < fileData.size(); i++){
				JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
				EarningSubTypeDO earningType = new EarningSubTypeDO();
				if(colName.get(CommonConstants.EARNINGID) != null && !colName.get(CommonConstants.EARNINGID).toString().isEmpty()){
		 			List<EarningTypeDO> earningList = earningTypeService.retrieveById(Long.parseLong(colName.get(CommonConstants.EARNINGID).toString()));
		 			if(earningList != null && earningList.size() >0){
		 				earningType.setEarningType(earningList.get(0));
		 				if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
				 			earningType.setUpdatedby(updatedBy.toString());
				 			earningType.setCreatedby(updatedBy.toString());
				 			if (colName.get(CommonConstants.NAME) != null && !colName.get(CommonConstants.NAME).toString().isEmpty()){
								earningType.setName(rowJSON.get(colName.get(CommonConstants.NAME)).toString());
								earningType.setUpdatedon(new Date());
						 		earningType.setCreatedon(new Date());
						 		earningType.setStatus(CommonConstants.ACTIVE);
						 		earningTypelist.add(earningType);
					 		}
				 		}
		 			}
		 		}
			}
			earningSubTypeService.persistList(earningTypelist);
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
