package com.spheresuite.erp.rs;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.AttachmentsDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.LeadEmailDO;
import com.spheresuite.erp.service.AttachmentsService;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.LeadEmailService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.EmailProxyUtil;
import com.spheresuite.erp.web.util.GmailUtil;
import com.spheresuite.erp.web.util.LeadEmailUtil;
import com.spheresuite.erp.web.util.MailBoxUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/email")
public class EmailRS {

	String validation = null;
	static Logger logger = Logger.getLogger(EmailRS.class.getName());
	
	@Autowired
	private EmployeeService employeeService;
	
	@Autowired
	private LeadEmailService leadEmailService;
	
	@Autowired
	private AttachmentsService attachmentsService;
	
	@RequestMapping(value = "/retrieveInbox", method = RequestMethod.POST)
	public @ResponseBody String retrieveInbox(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 			/*Long empId = null;
			 			String emailType = null;
			 			String emailId = null;
			 			String emailPassword = null;
			 			empId = request.getSession().getAttribute("empIds") != null ? Long.parseLong(request.getSession().getAttribute("empIds").toString()) : null;
		 				if(empId != null && empId == Long.parseLong(inputJSON.get(CommonConstants.ID).toString())){
		 					emailType = request.getSession().getAttribute("emailType") != null ? request.getSession().getAttribute("emailType").toString() : null;
		 					emailPassword = request.getSession().getAttribute("emailPassword") != null ? request.getSession().getAttribute("emailPassword").toString() : null;
		 					emailId = request.getSession().getAttribute("emailId") != null ? request.getSession().getAttribute("emailId").toString() : null;
			 				if(emailType != null && emailType.equalsIgnoreCase(CommonConstants.GMAIL)){
			 					if(emailId != null && emailPassword != null){
			 						List<LeadEmailDO> LeadList = GmailUtil.getByMessageUid(emailId, emailPassword,561);
					 				respJSON = LeadEmailUtil.getLeadEmailList(LeadList).toString();
			 					}
			 				}
			 			}*/
				 		List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.ID).toString());
				 		if(employeeList != null && employeeList.size() > 0){
				 			//if(employeeList.get(0).getEmailType() != null && employeeList.get(0).getEmailType().equalsIgnoreCase(CommonConstants.GMAIL)){
				 				if(employeeList.get(0).getEmailId() != null && employeeList.get(0).getEmailPassword() != null && employeeList.get(0).getEmailType() != null 
				 						&& employeeList.get(0).getEmailType().contains("Gmail")){
				 					byte[] decodedBytes = Base64.getDecoder().decode(employeeList.get(0).getEmailPassword());
									String stringDecode = new String(decodedBytes, "UTF-8");
									String folderName = !inputJSON.get(CommonConstants.FOLDERS).toString().isEmpty()?inputJSON.get(CommonConstants.FOLDERS).toString():"";
					 				List<LeadEmailDO> LeadList = GmailUtil.getAllInbox(employeeList.get(0).getEmailId(), stringDecode.toString(), employeeList.get(0).getEmailType(), folderName);
					 				//List<LeadEmailDO> LeadList = MailBoxUtil.getAllInbox(employeeList.get(0).getEmailId(), employeeList.get(0).getEmailPassword(), Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					 				if(LeadList.size() > 0){
					 					respJSON = LeadEmailUtil.getLeadEmailList(LeadList).toString();
					 				}
					 				//resultJSON.put(CommonConstants.ERRORS, "");
					 				//JSONArray resultJSONArray = new JSONArray(arrayMessages);
					 				//for (LeadEmailDO leadEmail : leadEmailEmailList) {
					 					//resultJSONArray.put(getLeadEmailDetailObject(leadEmail));
					 				//}
					 				
					 				//resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
					 				//responseJSON.put(CommonConstants.RESPONSE, resultJSON);
				 				}if(employeeList.get(0).getEmailId() != null && employeeList.get(0).getEmailPassword() != null && employeeList.get(0).getEmailType() != null 
				 						&& employeeList.get(0).getEmailType().contains("Exchange")){
				 					byte[] decodedBytes = Base64.getDecoder().decode(employeeList.get(0).getEmailPassword());
									String stringDecode = new String(decodedBytes, "UTF-8");
									String folderName = !inputJSON.get(CommonConstants.FOLDERS).toString().isEmpty()?inputJSON.get(CommonConstants.FOLDERS).toString():"";
					 				List<LeadEmailDO> LeadList = GmailUtil.getAllInbox(employeeList.get(0).getEmailId(), stringDecode.toString(), employeeList.get(0).getEmailType(),folderName);
					 				//List<LeadEmailDO> LeadList = MailBoxUtil.getAllInbox(employeeList.get(0).getEmailId(), employeeList.get(0).getEmailPassword(), Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					 				if(LeadList.size() > 0){
					 					respJSON = LeadEmailUtil.getLeadEmailList(LeadList).toString();
					 				}
					 				//resultJSON.put(CommonConstants.ERRORS, "");
					 				//JSONArray resultJSONArray = new JSONArray(arrayMessages);
					 				//for (LeadEmailDO leadEmail : leadEmailEmailList) {
					 					//resultJSONArray.put(getLeadEmailDetailObject(leadEmail));
					 				//}
					 				
					 				//resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
					 				//responseJSON.put(CommonConstants.RESPONSE, resultJSON);
				 				}
				 			//}
				 		}
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/addInbox", method = RequestMethod.POST)
	public @ResponseBody String addInbox(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){			 			
				 		List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.ID).toString());
				 		if(employeeList != null && employeeList.size() > 0){
				 			if(employeeList.get(0).getEmailType() != null && employeeList.get(0).getEmailType().contains(CommonConstants.GMAIL)){
				 				if(employeeList.get(0).getEmailId() != null && employeeList.get(0).getEmailPassword() != null){
				 					byte[] decodedBytes = Base64.getDecoder().decode(employeeList.get(0).getEmailPassword());
									String stringDecode = new String(decodedBytes, "UTF-8");
					 				MailBoxUtil.addInbox(employeeList.get(0).getEmailId(), stringDecode.toString(), Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					 				respJSON = CommonWebUtil.buildSuccessResponse().toString();
				 				}
				 			}
				 		}
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveMessage", method = RequestMethod.POST)
	public @ResponseBody String retrieveMessage(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		if (inputJSON != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty() && !inputJSON.get(CommonConstants.ID).toString().isEmpty() && !inputJSON.get(CommonConstants.LABEL).toString().isEmpty()){
				 		List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
				 		if(employeeList != null && employeeList.size() > 0){
				 			//if(employeeList.get(0).getEmailType() != null && employeeList.get(0).getEmailType().contains(CommonConstants.GMAIL)){
				 				if(employeeList.get(0).getEmailId() != null && employeeList.get(0).getEmailPassword() != null
				 						&& employeeList.get(0).getEmailType() != null){
				 					byte[] decodedBytes = Base64.getDecoder().decode(employeeList.get(0).getEmailPassword());
									String stringDecode = new String(decodedBytes, "UTF-8");
					 				List<LeadEmailDO> LeadList = GmailUtil.getByMessageUid(employeeList.get(0).getEmailId(), stringDecode.toString(), Long.parseLong(inputJSON.get(CommonConstants.ID).toString()),
					 						inputJSON.get(CommonConstants.LABEL).toString(), employeeList.get(0).getEmailType());
					 				respJSON = LeadEmailUtil.getLeadEmailList(LeadList).toString();
				 				}
				 			//}
				 		}
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@SuppressWarnings("unused")
	@RequestMapping(value = "/retriveById/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveById(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<LeadEmailDO> emailList = leadEmailService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					List<LeadEmailDO> emailListNew = GmailUtil.getSingleMessage(emailList);
					/*List<EmployeeDO> employeeList = employeeService.retriveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = EmployeeUtil.getEmployeeList(employeeList).toString();*/
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveSent", method = RequestMethod.POST)
	public @ResponseBody String retrieveSent(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.ID).toString());
				 		if(employeeList != null && employeeList.size() > 0){
				 			//if(employeeList.get(0).getEmailType() != null && employeeList.get(0).getEmailType().contains(CommonConstants.GMAIL)){
				 				if(employeeList.get(0).getEmailId() != null && employeeList.get(0).getEmailPassword() != null){
				 					byte[] decodedBytes = Base64.getDecoder().decode(employeeList.get(0).getEmailPassword());
									String stringDecode = new String(decodedBytes, "UTF-8");
					 				List<LeadEmailDO> LeadList = GmailUtil.getAllSentMail(employeeList.get(0).getEmailId(), stringDecode.toString());
					 				respJSON = LeadEmailUtil.getLeadEmailList(LeadList).toString();
				 				}
				 			//}
				 		}
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/sendEmail", method = RequestMethod.POST)
	public @ResponseBody String sendEmail(Model model, HttpServletRequest request,HttpServletResponse response) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		if (inputJSON != null && !inputJSON.get(CommonConstants.FROM).toString().isEmpty()){
			 			String fromEmail = !inputJSON.get(CommonConstants.FROM).toString().isEmpty()?inputJSON.get(CommonConstants.FROM).toString():"";
			 			String toEmails = !inputJSON.get(CommonConstants.TO).toString().isEmpty()?inputJSON.get(CommonConstants.TO).toString():"";
			 			String emailBody =  !inputJSON.get(CommonConstants.BODY).toString().isEmpty()?inputJSON.get(CommonConstants.BODY).toString():"";
			 			String ccMails =  !inputJSON.get(CommonConstants.CC).toString().isEmpty()?inputJSON.get(CommonConstants.CC).toString():"";
			 			String subject =  !inputJSON.get(CommonConstants.SUBJECT).toString().isEmpty()?inputJSON.get(CommonConstants.SUBJECT).toString():"";
			 			String bccMails =  !inputJSON.get(CommonConstants.BCC).toString().isEmpty()?inputJSON.get(CommonConstants.BCC).toString():"";
			 			byte[] decodedBytes = Base64.getDecoder().decode(inputJSON.get(CommonConstants.PASSWORD).toString());
			 			String type =  !inputJSON.get(CommonConstants.TYPE).toString().isEmpty()?inputJSON.get(CommonConstants.TYPE).toString():"";;
						String password = new String(decodedBytes, "UTF-8");
						String[] fileUrl = null;
						String[] fileName = null;
						String[] fileType = null;
						if(!inputJSON.get(CommonConstants.FILE).toString().isEmpty()){
							JSONArray resultJSONArray = new JSONArray();
					 		resultJSONArray.put(inputJSON.get(CommonConstants.FILE));
					 		String rec = (String) resultJSONArray.get(0).toString();
					 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
					 		fileUrl = new String[resultJSONArray1.size()];
					 		for(int i=0; i<resultJSONArray1.size();i++){
					 			JSONObject inputJSON1 = (JSONObject) resultJSONArray1.get(i);
					 			fileUrl[i] = inputJSON1.get(CommonConstants.FILE).toString();
					 		}
					 		
					 		JSONArray resultJSONArrayFileName = new JSONArray();
					 		resultJSONArrayFileName.put(inputJSON.get(CommonConstants.FILENAME));
					 		String rec1 = (String) resultJSONArrayFileName.get(0).toString();
					 		org.json.simple.JSONArray resultJSONArrayFileName1 = CommonWebUtil.getInputParamsArray(rec1);
					 		fileName = new String[resultJSONArrayFileName1.size()];
					 		for(int i=0; i<resultJSONArrayFileName1.size();i++){
					 			JSONObject inputJSON1 = (JSONObject) resultJSONArrayFileName1.get(i);
					 			fileName[i] = inputJSON1.get(CommonConstants.FILENAME).toString();
					 		}
					 		
					 		JSONArray resultJSONArrayFileType = new JSONArray();
					 		resultJSONArrayFileType.put(inputJSON.get(CommonConstants.FILE_TYPE));
					 		String rec2 = (String) resultJSONArrayFileType.get(0).toString();
					 		org.json.simple.JSONArray resultJSONArrayFileType1 = CommonWebUtil.getInputParamsArray(rec2);
					 		fileType = new String[resultJSONArrayFileType1.size()];
					 		for(int i=0; i<resultJSONArrayFileType1.size();i++){
					 			JSONObject inputJSON1 = (JSONObject) resultJSONArrayFileType1.get(i);
					 			fileType[i] = inputJSON1.get(CommonConstants.FILE_TYPE).toString();
					 		}
						}
			 			boolean mailStatus = EmailProxyUtil.sendEmails(request, fromEmail, toEmails, ccMails, bccMails, subject, emailBody, password.toString(), type, fileUrl, fileName, fileType); 
						if(mailStatus){
							attachmentsService.delete();
							return CommonWebUtil.buildSuccessResponse().toString();
						}else{
							return CommonWebUtil.buildErrorResponse("Network Failed").toString();
						}
			 		}
			 	}
			 	/*if (request.getParameter("from") != null && request.getParameter("to") != null){
		 			String fromEmail = request.getParameter(CommonConstants.FROM);
		 			String toEmails = request.getParameter(CommonConstants.TO);
		 			String emailBody =  request.getParameter(CommonConstants.BODY);
		 			String ccMails =  request.getParameter(CommonConstants.CC);
		 			String subject = request.getParameter(CommonConstants.SUBJECT);
		 			String bccMails =  request.getParameter(CommonConstants.BCC);
		 			String type =  request.getParameter(CommonConstants.TYPE);
		 			byte[] decodedBytes = Base64.getDecoder().decode(request.getParameter(CommonConstants.PASSWORD).toString());
					String password = new String(decodedBytes, "UTF-8");
					System.out.println("password    "+password);
					JSONArray resultJSONArray = new JSONArray();
				 		resultJSONArray.put(request.getParameter(CommonConstants.ATTACH_FILE));
				 		String rec = (String) resultJSONArray.get(0).toString();
				 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
				 		//List<Long> fileList = new ArrayList<Long>();
				 		for(int i=0; i<resultJSONArray1.size();i++){
				 			JSONObject inputJSON1 = (JSONObject) resultJSONArray1.get(i);
				 			System.out.println(inputJSON1.get(CommonConstants.FILENAME).toString());
				 		}
		 			boolean mailStatus = EmailProxyUtil.sendEmails(request, fromEmail, toEmails, ccMails, bccMails, subject, emailBody, password.toString(), type); 
					if(mailStatus){
						//attachmentsService.delete();
						return CommonWebUtil.buildSuccessResponse().toString();
					}else{
						return CommonWebUtil.buildErrorResponse("").toString();
					}
 			 	}*/
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("Network Failed").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/attachments", method = RequestMethod.POST)
	public @ResponseBody String attachments(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				AttachmentsDO attachmentsDO = new AttachmentsDO();
				/*JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("data").toString());*/
			 	if (request.getParameter(CommonConstants.FILE) != null){
			 		attachmentsDO.setAttachFile(request.getParameter(CommonConstants.FILE));
			 		attachmentsDO.setFiletype(request.getParameter(CommonConstants.TYPE));
			 		attachmentsDO.setFileName(request.getParameter(CommonConstants.FILENAME));
			 		attachmentsService.persist(attachmentsDO);
 			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveLeadInbox", method = RequestMethod.POST)
	public @ResponseBody String retrieveLeadInbox(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
				 		List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.ID).toString());
				 		if(employeeList != null && employeeList.size() > 0){
			 				if(employeeList.get(0).getEmailId() != null && employeeList.get(0).getEmailPassword() != null
			 						&& employeeList.get(0).getEmailType() != null){
			 					if(!inputJSON.get(CommonConstants.PRIMARYEMAIL).toString().isEmpty()){
				 					byte[] decodedBytes = Base64.getDecoder().decode(employeeList.get(0).getEmailPassword());
									String stringDecode = new String(decodedBytes, "UTF-8");
									String primaryEmail = inputJSON.get(CommonConstants.PRIMARYEMAIL).toString();
									List<LeadEmailDO> LeadList = GmailUtil.getLeadMails(employeeList.get(0).getEmailId(), stringDecode.toString(),primaryEmail, employeeList.get(0).getEmailType());
					 				if(LeadList.size() > 0){
					 					respJSON = LeadEmailUtil.getLeadEmailList(LeadList).toString();
					 				}
			 					}	
			 				}
				 		}
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveUnreadInbox", method = RequestMethod.POST)
	public @ResponseBody String retrieveUnreadInbox(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
				 		List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.ID).toString());
				 		if(employeeList != null && employeeList.size() > 0){
			 				if(employeeList.get(0).getEmailId() != null && employeeList.get(0).getEmailPassword() != null && employeeList.get(0).getEmailType() != null 
			 						&& employeeList.get(0).getEmailType().contains("Gmail")){
			 					byte[] decodedBytes = Base64.getDecoder().decode(employeeList.get(0).getEmailPassword());
								String stringDecode = new String(decodedBytes, "UTF-8");
				 				List<LeadEmailDO> LeadList = GmailUtil.getUnReadMessageInbox(employeeList.get(0).getEmailId(), stringDecode.toString(), employeeList.get(0).getEmailType());
				 				if(LeadList.size() > 0){
				 					respJSON = LeadEmailUtil.getLeadEmailList(LeadList).toString();
				 				}
			 				}if(employeeList.get(0).getEmailId() != null && employeeList.get(0).getEmailPassword() != null && employeeList.get(0).getEmailType() != null 
			 						&& employeeList.get(0).getEmailType().contains("Exchange")){
			 					byte[] decodedBytes = Base64.getDecoder().decode(employeeList.get(0).getEmailPassword());
								String stringDecode = new String(decodedBytes, "UTF-8");
				 				List<LeadEmailDO> LeadList = GmailUtil.getUnReadMessageInbox(employeeList.get(0).getEmailId(), stringDecode.toString(), employeeList.get(0).getEmailType());
				 				if(LeadList.size() > 0){
				 					respJSON = LeadEmailUtil.getLeadEmailList(LeadList).toString();
				 				}
			 				}
				 		}
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/mailUnread", method = RequestMethod.POST)
	public @ResponseBody String mailUnread(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
				 		List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.ID).toString());
				 		JSONArray resultJSONArray = new JSONArray();
				 		resultJSONArray.put(inputJSON.get(CommonConstants.UID));
				 		String rec = (String) resultJSONArray.get(0).toString();
				 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
				 		List<Long> uidList = new ArrayList<Long>();
				 		for(int i=0; i<resultJSONArray1.size();i++){
				 			JSONObject inputJSON1 = (JSONObject) resultJSONArray1.get(i);
				 			if(!inputJSON1.get(CommonConstants.ID).toString().isEmpty()){
				 				uidList.add(Long.parseLong(inputJSON1.get(CommonConstants.ID).toString()));
				 			}
				 		}
				 		if(employeeList != null && employeeList.size() > 0){
			 				if(employeeList.get(0).getEmailId() != null && employeeList.get(0).getEmailPassword() != null && employeeList.get(0).getEmailType() != null 
			 						&& employeeList.get(0).getEmailType().contains("Gmail")){
			 					byte[] decodedBytes = Base64.getDecoder().decode(employeeList.get(0).getEmailPassword());
								String stringDecode = new String(decodedBytes, "UTF-8");
				 				List<LeadEmailDO> LeadList = GmailUtil.showUnReadMessage(employeeList.get(0).getEmailId(), stringDecode.toString(), employeeList.get(0).getEmailType(), uidList, inputJSON.get(CommonConstants.LABEL).toString(), inputJSON.get(CommonConstants.TYPE).toString());
				 				if(LeadList.size() > 0){
				 					respJSON = LeadEmailUtil.getLeadEmailList(LeadList).toString();
				 				}
			 				}if(employeeList.get(0).getEmailId() != null && employeeList.get(0).getEmailPassword() != null && employeeList.get(0).getEmailType() != null 
			 						&& employeeList.get(0).getEmailType().contains("Exchange")){
			 					byte[] decodedBytes = Base64.getDecoder().decode(employeeList.get(0).getEmailPassword());
								String stringDecode = new String(decodedBytes, "UTF-8");
				 				List<LeadEmailDO> LeadList = GmailUtil.showUnReadMessage(employeeList.get(0).getEmailId(), stringDecode.toString(), employeeList.get(0).getEmailType(), uidList, inputJSON.get(CommonConstants.LABEL).toString(), inputJSON.get(CommonConstants.TYPE).toString());
				 				if(LeadList.size() > 0){
				 					respJSON = LeadEmailUtil.getLeadEmailList(LeadList).toString();
				 				}
			 				}
				 		}
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/mailDelete", method = RequestMethod.POST)
	public @ResponseBody String mailDelete(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		if (inputJSON != null && !inputJSON.get(CommonConstants.UID).toString().isEmpty() && inputJSON.get(CommonConstants.UID) != null){
				 		List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.ID).toString());
				 		/*JSONArray resultJSONArray = new JSONArray();
				 		resultJSONArray.put(inputJSON.get(CommonConstants.UID));
				 		String rec = (String) resultJSONArray.get(0).toString();
				 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
				 		List<Long> uidList = new ArrayList<Long>();
				 		for(int i=0; i<resultJSONArray1.size();i++){
				 			JSONObject inputJSON1 = (JSONObject) resultJSONArray1.get(i);
				 			if(!inputJSON1.get(CommonConstants.ID).toString().isEmpty()){
				 				uidList.add(Long.parseLong(inputJSON1.get(CommonConstants.ID).toString()));
				 			}
				 		}*/
				 		Long uidList = Long.parseLong(inputJSON.get(CommonConstants.UID).toString());
				 		if(employeeList != null && employeeList.size() > 0){
			 				if(employeeList.get(0).getEmailId() != null && employeeList.get(0).getEmailPassword() != null && employeeList.get(0).getEmailType() != null 
			 						&& employeeList.get(0).getEmailType().equalsIgnoreCase("Gmail")){
			 					byte[] decodedBytes = Base64.getDecoder().decode(employeeList.get(0).getEmailPassword());
								String stringDecode = new String(decodedBytes, "UTF-8");
				 				List<LeadEmailDO> LeadList = GmailUtil.deleteMessage(employeeList.get(0).getEmailId(), stringDecode.toString(), employeeList.get(0).getEmailType(), uidList, inputJSON.get(CommonConstants.LABEL).toString(), employeeList.get(0).getEmailType());
				 				if(LeadList.size() > 0){
				 					respJSON = LeadEmailUtil.getLeadEmailList(LeadList).toString();
				 				}
			 				}if(employeeList.get(0).getEmailId() != null && employeeList.get(0).getEmailPassword() != null && employeeList.get(0).getEmailType() != null 
			 						&& employeeList.get(0).getEmailType().equalsIgnoreCase("Exchange")){
			 					byte[] decodedBytes = Base64.getDecoder().decode(employeeList.get(0).getEmailPassword());
								String stringDecode = new String(decodedBytes, "UTF-8");
				 				List<LeadEmailDO> LeadList = GmailUtil.deleteMessage(employeeList.get(0).getEmailId(), stringDecode.toString(), employeeList.get(0).getEmailType(), uidList, inputJSON.get(CommonConstants.LABEL).toString(), employeeList.get(0).getEmailType());
				 				if(LeadList.size() > 0){
				 					respJSON = LeadEmailUtil.getLeadEmailList(LeadList).toString();
				 				}
			 				}
				 		}
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
}
