package com.spheresuite.erp.rs;

import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.EmailSettingsDO;
import com.spheresuite.erp.service.EmailSettingsService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.EmailSettingsUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/emailSettings")
public class EmailSettingRS {

	String validation = null;
	static Logger logger = Logger.getLogger(EmailSettingRS.class.getName());

	@Autowired
	private EmailSettingsService emailSettingsService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				EmailSettingsDO emailSettingsDO = new EmailSettingsDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.MAIL_TYPE) != null && !inputJSON.get(CommonConstants.MAIL_TYPE).toString().isEmpty()){
			 			emailSettingsDO.setMailtype(inputJSON.get(CommonConstants.MAIL_TYPE).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.IMAP_HOST) != null && !inputJSON.get(CommonConstants.IMAP_HOST).toString().isEmpty()){
			 			emailSettingsDO.setImaphost(inputJSON.get(CommonConstants.IMAP_HOST).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.PORT) != null && !inputJSON.get(CommonConstants.PORT).toString().isEmpty()){
			 			emailSettingsDO.setImapport(inputJSON.get(CommonConstants.PORT).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.SMTP_PORT) != null && !inputJSON.get(CommonConstants.SMTP_PORT).toString().isEmpty()){
			 			emailSettingsDO.setSmtpport(inputJSON.get(CommonConstants.SMTP_PORT).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.SMTP_HOST) != null && !inputJSON.get(CommonConstants.SMTP_HOST).toString().isEmpty()){
			 			emailSettingsDO.setSmtphost(inputJSON.get(CommonConstants.SMTP_HOST).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.EMAIL) != null && !inputJSON.get(CommonConstants.EMAIL).toString().isEmpty()){
			 			emailSettingsDO.setSendemail(inputJSON.get(CommonConstants.EMAIL).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.PASSWORD) != null && !inputJSON.get(CommonConstants.PASSWORD).toString().isEmpty()){
			 			String pwd = inputJSON.get(CommonConstants.PASSWORD).toString();
			 			emailSettingsDO.setSendpassword(Base64.getEncoder().encodeToString(pwd.getBytes("UTF-8")));
			 		}
			 		if(inputJSON.get(CommonConstants.AUTHENTICATE) != null && !inputJSON.get(CommonConstants.AUTHENTICATE).toString().isEmpty()){
			 			emailSettingsDO.setAuthType(inputJSON.get(CommonConstants.AUTHENTICATE).toString().equalsIgnoreCase(CommonConstants.TRUE)?0:1);
			 		}
			 		if(inputJSON.get(CommonConstants.TLS_ENABLED) != null && !inputJSON.get(CommonConstants.TLS_ENABLED).toString().isEmpty()){
			 			emailSettingsDO.setTlsEnable(inputJSON.get(CommonConstants.TLS_ENABLED).toString().equalsIgnoreCase(CommonConstants.TRUE)?0:1);
			 		}
			 		emailSettingsDO.setStatus(CommonConstants.ACTIVE);
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			emailSettingsDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			emailSettingsDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		emailSettingsDO.setUpdatedon(new Date());
			 		emailSettingsDO.setCreatedon(new Date());
			 	}
				if(!emailSettingsService.persist(emailSettingsDO)){
					return CommonWebUtil.buildErrorResponse("Email Settings Already Added").toString();
				}
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Email Settings Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<EmailSettingsDO> emailSettingsList = emailSettingsService.retrieve();
				respJSON = EmailSettingsUtil.getEmailSettingsList(emailSettingsList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EmailSettingsDO emailSettingsDO = new EmailSettingsDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<EmailSettingsDO> emailSettingsList = emailSettingsService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(emailSettingsList != null && emailSettingsList.size() > 0){
				 		emailSettingsDO = emailSettingsList.get(0);
				 		if(inputJSON.get(CommonConstants.MAIL_TYPE) != null && !inputJSON.get(CommonConstants.MAIL_TYPE).toString().isEmpty()){
				 			emailSettingsDO.setMailtype(inputJSON.get(CommonConstants.MAIL_TYPE).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.IMAP_HOST) != null && !inputJSON.get(CommonConstants.IMAP_HOST).toString().isEmpty()){
				 			emailSettingsDO.setImaphost(inputJSON.get(CommonConstants.IMAP_HOST).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.PORT) != null && !inputJSON.get(CommonConstants.PORT).toString().isEmpty()){
				 			emailSettingsDO.setImapport(inputJSON.get(CommonConstants.PORT).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.SMTP_PORT) != null && !inputJSON.get(CommonConstants.SMTP_PORT).toString().isEmpty()){
				 			emailSettingsDO.setSmtpport(inputJSON.get(CommonConstants.SMTP_PORT).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.SMTP_HOST) != null && !inputJSON.get(CommonConstants.SMTP_HOST).toString().isEmpty()){
				 			emailSettingsDO.setSmtphost(inputJSON.get(CommonConstants.SMTP_HOST).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.EMAIL) != null && !inputJSON.get(CommonConstants.EMAIL).toString().isEmpty()){
				 			emailSettingsDO.setSendemail(inputJSON.get(CommonConstants.EMAIL).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.PASSWORD) != null && !inputJSON.get(CommonConstants.PASSWORD).toString().isEmpty()){
				 			String pwd = inputJSON.get(CommonConstants.PASSWORD).toString();
				 			emailSettingsDO.setSendpassword(Base64.getEncoder().encodeToString(pwd.getBytes("UTF-8")));
				 		}
				 		if(inputJSON.get(CommonConstants.AUTHENTICATE) != null && !inputJSON.get(CommonConstants.AUTHENTICATE).toString().isEmpty()){
				 			emailSettingsDO.setAuthType(inputJSON.get(CommonConstants.AUTHENTICATE).toString().equalsIgnoreCase(CommonConstants.TRUE)?0:1);
				 		}
				 		if(inputJSON.get(CommonConstants.TLS_ENABLED) != null && !inputJSON.get(CommonConstants.TLS_ENABLED).toString().isEmpty()){
				 			emailSettingsDO.setTlsEnable(inputJSON.get(CommonConstants.TLS_ENABLED).toString().equalsIgnoreCase(CommonConstants.TRUE)?0:1);
				 		}
				 		emailSettingsDO.setUpdatedon(new Date());
				 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			emailSettingsDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
				 			emailSettingsDO.setStatus((char) inputJSON.get(CommonConstants.STATUS).toString().charAt(0));
				 		}
				 		if(!emailSettingsService.update(emailSettingsDO)){
							return CommonWebUtil.buildErrorResponse("Email Settings Already Added").toString();
						}
				 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Email Settings Updated");
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/importEmailSettings", method = RequestMethod.POST)
	public @ResponseBody String importEmailSettings(Model model, HttpServletRequest request) {
		try {
			JSONObject inputJSON =  CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
			JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
			List<EmailSettingsDO> emailSettingsList = new ArrayList<EmailSettingsDO>();
			Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY);
			for (int i=0; i < fileData.size(); i++){
				JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
				EmailSettingsDO emailSettingsDO = new EmailSettingsDO();
				
				if(colName.get(CommonConstants.MAIL_TYPE) != null && !colName.get(CommonConstants.MAIL_TYPE).toString().isEmpty()){
		 			emailSettingsDO.setMailtype(rowJSON.get(colName.get(CommonConstants.MAIL_TYPE)).toString());
		 		}
		 		if(colName.get(CommonConstants.IMAP_HOST) != null && !colName.get(CommonConstants.IMAP_HOST).toString().isEmpty()){
		 			emailSettingsDO.setImaphost(rowJSON.get(colName.get(CommonConstants.IMAP_HOST)).toString());
		 		}
		 		if(colName.get(CommonConstants.PORT) != null && !colName.get(CommonConstants.PORT).toString().isEmpty()){
		 			emailSettingsDO.setImapport(rowJSON.get(colName.get(CommonConstants.PORT)).toString());
		 		}
		 		if(colName.get(CommonConstants.SMTP_PORT) != null && !colName.get(CommonConstants.SMTP_PORT).toString().isEmpty()){
		 			emailSettingsDO.setSmtpport(rowJSON.get(colName.get(CommonConstants.SMTP_PORT)).toString());
		 		}
		 		if(colName.get(CommonConstants.SMTP_HOST) != null && !colName.get(CommonConstants.SMTP_HOST).toString().isEmpty()){
		 			emailSettingsDO.setSmtphost(rowJSON.get(colName.get(CommonConstants.SMTP_HOST)).toString());
		 		}
		 		if(colName.get(CommonConstants.EMAIL) != null && !colName.get(CommonConstants.EMAIL).toString().isEmpty()){
		 			emailSettingsDO.setSendemail(rowJSON.get(colName.get(CommonConstants.EMAIL)).toString());
		 		}
		 		if(colName.get(CommonConstants.PASSWORD) != null && !colName.get(CommonConstants.PASSWORD).toString().isEmpty()){
		 			String pwd = rowJSON.get(colName.get(CommonConstants.PASSWORD)).toString();
		 			emailSettingsDO.setSendpassword(Base64.getEncoder().encodeToString(pwd.getBytes("UTF-8")));
		 		}
		 		if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
		 			emailSettingsDO.setUpdatedby(updatedBy.toString());
		 			emailSettingsDO.setCreatedby(updatedBy.toString());
		 		}
		 		emailSettingsDO.setUpdatedon(new Date());
		 		emailSettingsDO.setCreatedon(new Date());
		 		emailSettingsDO.setStatus(CommonConstants.ACTIVE);
		 		emailSettingsList.add(emailSettingsDO);
			}
			emailSettingsService.persistList(emailSettingsList);
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
