package com.spheresuite.erp.rs;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.auth0.jwt.JWTVerifyException;
import com.spheresuite.erp.domainobject.HrPoliciesDO;
import com.spheresuite.erp.domainobject.HrPoliciesDocDO;
import com.spheresuite.erp.domainobject.NotificationSettingsDO;
import com.spheresuite.erp.domainobject.RolesDO;
import com.spheresuite.erp.service.HrPoliciesDocService;
import com.spheresuite.erp.service.HrPoliciesService;
import com.spheresuite.erp.service.NotificationSettingsService;
import com.spheresuite.erp.service.RolesService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.HolidaysUtil;
import com.spheresuite.erp.web.util.HrPoliciesUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/hrpolicy")
public class HrPoliciesRS {

	String validation = null;
	static Logger logger = Logger.getLogger(HrPoliciesRS.class.getName());

	@Autowired
	private HrPoliciesService hrPoliciesService;
	
	@Autowired
	private RolesService roleService;
	
	@Autowired
	private NotificationSettingsService notificationSettingsService;
	
	@Autowired
	private HrPoliciesDocService hrPoliciesDocService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		HrPoliciesDO hrPoliciesDO = new HrPoliciesDO(); 
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && inputJSON.get(CommonConstants.VALUE) != null && !inputJSON.get(CommonConstants.VALUE).toString().isEmpty()){
			 		//if(inputJSON.get(CommonConstants.ROLE_ID) != null && !inputJSON.get(CommonConstants.ROLE_ID).toString().isEmpty()){
				 		if(inputJSON.get(CommonConstants.VALUE) != null && !inputJSON.get(CommonConstants.VALUE).toString().isEmpty()){
				 			hrPoliciesDO.setHrPolice(inputJSON.get(CommonConstants.VALUE).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.ROLE_ID) != null && !inputJSON.get(CommonConstants.ROLE_ID).toString().isEmpty()){
				 			List<RolesDO> rolesList = roleService.retriveById(Long.parseLong(inputJSON.get(CommonConstants.ROLE_ID).toString()));
				 			if(rolesList != null && rolesList.size() > 0){
				 				hrPoliciesDO.setRole(rolesList.get(0));
				 			}
			 			}
				 		
		 				if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 				hrPoliciesDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 				hrPoliciesDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 				if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
				 				hrPoliciesDO.setStatus((char) (inputJSON.get(CommonConstants.STATUS).toString().charAt(0)));
					 		}
			 				hrPoliciesDO.setUpdatedon(new Date());
			 				hrPoliciesDO.setCreatedon(new Date());
			 				hrPoliciesService.persist(hrPoliciesDO);
				 			CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Hr Policy Created");
				 			CommonUtil.sendMailToReportingHeads(request,inputJSON,"hrpolicies/manage","hrpolicies","pending");
				 		}
		 			//}
			 	}
			}else{
		 		return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
		 	}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponseId(hrPoliciesDO.getHrPoliceId().toString()).toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) throws InvalidKeyException, NoSuchAlgorithmException, IllegalStateException, SignatureException, IOException, JWTVerifyException {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<HrPoliciesDO> hrPoliciesList = hrPoliciesService.retrieve();
				respJSON = HrPoliciesUtil.getHrPoliciesList(hrPoliciesList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveById/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveById(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<RolesDO> rolesList = roleService.retriveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					List<HrPoliciesDO> hrPoliciesList = new ArrayList<HrPoliciesDO>();
					if(rolesList != null && rolesList.size() > 0){
						if(rolesList.get(0).getName().equalsIgnoreCase("Super Admin")){
							hrPoliciesList = hrPoliciesService.retrieve();
						}else{
							hrPoliciesList = hrPoliciesService.retrieveByRoleId(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
						}
					}
					respJSON = HrPoliciesUtil.getHrPoliciesList(hrPoliciesList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveByRoleId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByRoleId(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<HrPoliciesDO> hrPoliciesList = hrPoliciesService.retrieveByRoleId(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = HrPoliciesUtil.getHrPoliciesList(hrPoliciesList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		HrPoliciesDO hrPoliciesDO = new HrPoliciesDO(); 
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && inputJSON.get(CommonConstants.ID) != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 			//if(!inputJSON.get(CommonConstants.VALUE).toString().isEmpty()){
			 				
			 				List<HrPoliciesDO> hrPolicyList = hrPoliciesService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 				if(hrPolicyList != null && hrPolicyList.size() > 0){
			 					hrPoliciesDO = hrPolicyList.get(0);
			 					if(inputJSON.get(CommonConstants.VALUE) != null && !inputJSON.get(CommonConstants.VALUE).toString().isEmpty()){
						 			hrPoliciesDO.setHrPolice(inputJSON.get(CommonConstants.VALUE).toString());
						 		}
						 		if(inputJSON.get(CommonConstants.ROLE_ID) != null && !inputJSON.get(CommonConstants.ROLE_ID).toString().isEmpty()){
						 			List<RolesDO> rolesList = roleService.retriveById(Long.parseLong(inputJSON.get(CommonConstants.ROLE_ID).toString()));
						 			if(rolesList != null && rolesList.size() > 0){
						 				hrPoliciesDO.setRole(rolesList.get(0));
						 			}
					 			}
						 		if(inputJSON.get(CommonConstants.COMMENT) != null && !inputJSON.get(CommonConstants.COMMENT).toString().isEmpty()){
					 				hrPoliciesDO.setComment(inputJSON.get(CommonConstants.COMMENT).toString());
						 		}
				 				hrPoliciesDO.setUpdatedon(new Date());
				 				if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 				hrPoliciesDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
						 		}
				 				if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
				 					hrPoliciesDO.setStatus((char) (inputJSON.get(CommonConstants.STATUS).toString().charAt(0)));

						 		}
				 				hrPoliciesService.update(hrPoliciesDO);
				 				if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty() && inputJSON.get(CommonConstants.STATUS).toString().equalsIgnoreCase("c")){
					 				CommonUtil.sendMailToReportingHeads(request,inputJSON,"hrpolicy","hrpolicies", "cancel");
					 			}
					 			if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty() && inputJSON.get(CommonConstants.STATUS).toString().equalsIgnoreCase("a")){
					 				CommonUtil.sendMailToReportingHeads(request,inputJSON,"hrpolicy","hrpolicies", "approved");
					 			}
					 			if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty() && inputJSON.get(CommonConstants.STATUS).toString().equalsIgnoreCase("r")){
					 				CommonUtil.sendMailToReportingHeads(request,inputJSON,"hrpolicy","hrpolicies", "rejected");
					 			}
					 			if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty() && inputJSON.get(CommonConstants.STATUS).toString().equalsIgnoreCase("p")){
					 				//hrPoliciesDocService.delete(hrPoliciesDO.getHrPoliceId());
					 				CommonUtil.sendMailToReportingHeads(request,inputJSON,"hrpolicies/manage","hrpolicies","pending");
					 			}
				 				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "HR Policy Updated");
			 				}
			 			//}
			 		}
			 	}
				
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponseId(hrPoliciesDO.getHrPoliceId().toString()).toString();
	}
	
	@RequestMapping(value = "/inActive", method = RequestMethod.POST)
	public @ResponseBody String inActive(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && inputJSON.get(CommonConstants.ID) != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 			if(!inputJSON.get(CommonConstants.VALUE).toString().isEmpty()){
			 				HrPoliciesDO hrPoliciesDO = new HrPoliciesDO(); 
			 				List<HrPoliciesDO> hrPolicyList = hrPoliciesService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 				if(hrPolicyList != null && hrPolicyList.size() > 0){
			 					hrPoliciesDO = hrPolicyList.get(0);
			 					if(inputJSON.get(CommonConstants.VALUE).toString() != null && !inputJSON.get(CommonConstants.VALUE).toString().isEmpty()){
			 						hrPoliciesDO.setHrPolice(inputJSON.get(CommonConstants.VALUE).toString());
			 					}
				 				hrPoliciesDO.setUpdatedon(new Date());
				 				if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 				hrPoliciesDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
						 		}
				 				if(inputJSON.get(CommonConstants.STATUS).toString() != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
				 					hrPoliciesDO.setStatus((char) inputJSON.get(CommonConstants.STATUS).toString().charAt(0));
						 		}
				 				/*if(!inputJSON.get(CommonConstants.ROLE_ID).toString().isEmpty()){
				 					hrPoliciesDO.setRoleId(Long.parseLong(inputJSON.get(CommonConstants.ROLE_ID).toString()));
				 				}else{
				 					hrPoliciesDO.setRoleId(null);
				 				}*/
				 				hrPoliciesService.update(hrPoliciesDO);
				 				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Hr Policy Updated");
			 				}
			 			}
			 		}
			 	}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}

	@RequestMapping(value = "/updatepic", method = RequestMethod.POST)
	public @ResponseBody String updatepic(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				HrPoliciesDocDO hrpoliciesdoc = new HrPoliciesDocDO();
			 	if (request.getParameter(CommonConstants.FILE) != null && request.getParameter(CommonConstants.ID) != null){
			 			if(hrPoliciesDocService.delete(Long.parseLong(request.getParameter(CommonConstants.ID)))){
			 				hrpoliciesdoc.setHrPoliceId(Long.parseLong(request.getParameter(CommonConstants.ID)));
					 		hrpoliciesdoc.setPhoto(request.getParameter(CommonConstants.FILE));
					 		hrpoliciesdoc.setFileName(request.getParameter(CommonConstants.NAME));
					 		hrpoliciesdoc.setUpdatedon(new Date());
					 		hrpoliciesdoc.setCreatedon(new Date());
			 				if(request.getParameter(CommonConstants.UPDATED_BY) != null && !request.getParameter(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 					hrpoliciesdoc.setUpdatedby(request.getParameter(CommonConstants.UPDATED_BY).toString());
			 					hrpoliciesdoc.setCreatedby(request.getParameter(CommonConstants.UPDATED_BY).toString());
					 		}
			 				hrPoliciesDocService.persist(hrpoliciesdoc);
				 		}else{
				 			return CommonWebUtil.buildErrorResponse("").toString();
				 		}
 			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveByApproverId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByApproverId(@PathVariable String inputParams, Model model, HttpServletRequest request)  {
		String respJSON = null;
		Boolean isApprover = false;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<NotificationSettingsDO> notificationSettingsList = notificationSettingsService.retrieve();
	 				if(notificationSettingsList != null && notificationSettingsList.size() >0){
	 					if(notificationSettingsList.get(0).getHrpolicies() != null){
	 						List<String> hrPoliciesList = Arrays.asList(notificationSettingsList.get(0).getHrpolicies().split(CommonConstants.COMMA));
	 						if(hrPoliciesList !=null && hrPoliciesList.size() > 0){
	 							for(String approver : hrPoliciesList ){
	 								if(approver.equalsIgnoreCase(inputJSON.get(CommonConstants.ID).toString())){
	 									isApprover = true;
	 									List<HrPoliciesDO> hrPoliciesDetails = hrPoliciesService.retrieve();
	 									respJSON = HrPoliciesUtil.getHrPoliciesList(hrPoliciesDetails).toString();
	 								}
	 							}
	 							if(!isApprover){
	 								respJSON = HolidaysUtil.notApproverResponse().toString();
	 							}
	 						}
	 					}
	 				}
				}else{
					return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
}
