package com.spheresuite.erp.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.EmployeeBonusDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.FyDO;
import com.spheresuite.erp.service.EmployeeBonusService;
import com.spheresuite.erp.service.EmployeeCtcService;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.FyService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.EmployeeBonusUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/employeebonus")
public class EmployeeBonusRS {

	String validation = null;
	static Logger logger = Logger.getLogger(EmployeeBonusRS.class.getName());

	@Autowired
	private EmployeeBonusService employeeBonusService;
	
	@Autowired
	private FyService fyService;
	
	@Autowired
	private EmployeeCtcService employeeCtcService;
	
	@Autowired
	private EmployeeService employeeService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeBonusDO employeeBonusDO = new EmployeeBonusDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.EMPID) != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
				 		if(employeeList != null && employeeList.size() > 0){
				 			employeeBonusDO.setEmployee(employeeList.get(0));
				 			if(inputJSON.get(CommonConstants.AMOUNT) != null && !inputJSON.get(CommonConstants.AMOUNT).toString().isEmpty()){
					 			employeeBonusDO.setAmount(Long.parseLong(inputJSON.get(CommonConstants.AMOUNT).toString()));
					 		}
					 		if(inputJSON.get(CommonConstants.BONUSMONTH) != null && !inputJSON.get(CommonConstants.BONUSMONTH).toString().isEmpty()){
					 			employeeBonusDO.setBonusMonth(inputJSON.get(CommonConstants.BONUSMONTH).toString());
					 		}
					 		if(inputJSON.get(CommonConstants.COMMENT) != null && !inputJSON.get(CommonConstants.COMMENT).toString().isEmpty()){
					 			employeeBonusDO.setComment(inputJSON.get(CommonConstants.COMMENT).toString());
					 		}
					 		employeeBonusDO.setUpdatedon(new Date());
					 		employeeBonusDO.setCreatedon(new Date());
					 		employeeBonusDO.setStatus(CommonConstants.ACTIVE);
					 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			employeeBonusDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			employeeBonusDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			employeeBonusService.persist(employeeBonusDO);
								CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Bonus Created For Employee");
					 		}
				 		}
			 		}
			 		
			 	}
				
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retrieveByEmpId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByEmpId(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<EmployeeBonusDO> employeeAddressList = employeeBonusService.retrieveByEmpId(inputJSON.get(CommonConstants.ID).toString());
					respJSON = EmployeeBonusUtil.getEmployeeBonusList(employeeAddressList, null,null).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieveByEmpId(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
					List<EmployeeBonusDO> employeeBonusList = employeeBonusService.retrieve();
					respJSON = EmployeeBonusUtil.getEmployeeBonusList(employeeBonusList, null,null).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeBonusDO employeeBonusDO = new EmployeeBonusDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<EmployeeBonusDO> employeeBonusList = employeeBonusService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		employeeBonusDO = employeeBonusList.get(0);
			 		
			 		if(inputJSON.get(CommonConstants.EMPID) != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
				 		if(employeeList != null && employeeList.size() > 0){
				 			employeeBonusDO.setEmployee(employeeList.get(0));
				 		}
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.AMOUNT) != null && !inputJSON.get(CommonConstants.AMOUNT).toString().isEmpty()){
			 			employeeBonusDO.setAmount(Long.parseLong(inputJSON.get(CommonConstants.AMOUNT).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.BONUSMONTH) != null && !inputJSON.get(CommonConstants.BONUSMONTH).toString().isEmpty()){
			 			employeeBonusDO.setBonusMonth(inputJSON.get(CommonConstants.BONUSMONTH).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.COMMENT) != null && !inputJSON.get(CommonConstants.COMMENT).toString().isEmpty()){
			 			employeeBonusDO.setComment(inputJSON.get(CommonConstants.COMMENT).toString());
			 		}
			 		employeeBonusDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			employeeBonusDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		employeeBonusService.update(employeeBonusDO);
			 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Bonus Updated For Employee");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public @ResponseBody String delete(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		Boolean employeeAddressList = employeeBonusService.delete(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(employeeAddressList){
			 			CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Bonus Deleted For Employee");
			 		}else{
			 			return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retriveForBonusReport/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveForBonusReport(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.MONTHLY).toString().isEmpty()){
					Long batchId = null;
					List<Long> ids = new ArrayList<Long>();
					if(!inputJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty()){
						batchId = Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString());
						if(batchId == 0){
							batchId = null;
						}else{
							ids = employeeCtcService.retrieveByBatchId(batchId);
						}
					}
					List<EmployeeBonusDO> bonusList = employeeBonusService.retrieveByEmpIdWithDate(ids, inputJSON.get(CommonConstants.MONTHLY).toString());
					respJSON = EmployeeBonusUtil.getEmployeeBonusList(bonusList, CommonConstants.MONTHLYC, inputJSON.get(CommonConstants.MONTHLY).toString()).toString();
				}else{
					if(inputJSON != null && !inputJSON.get(CommonConstants.QUARTERLY).toString().isEmpty()){
						String fromMonth = null;
						String toMonth = null;
						Long batchId = null;
						List<FyDO> fyList = new ArrayList<FyDO>();
						if(!inputJSON.get(CommonConstants.FYID).toString().isEmpty()){
							fyList = fyService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.FYID).toString()));
							if(fyList != null && fyList.size() > 0){
								List<Long> ids = new ArrayList<Long>();
								if(!inputJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty()){
									batchId = Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString());
									if(batchId == 0){
										batchId = null;
									}else{
										ids = employeeCtcService.retrieveByBatchId(batchId);
									}
								}
								if(inputJSON.get(CommonConstants.QUARTERLY).toString().equalsIgnoreCase("Q1")){
									fromMonth = "Apr"+" "+fyList.get(0).getFromyear();
									toMonth = "Jun"+" "+fyList.get(0).getFromyear(); 
									List<EmployeeBonusDO> bonusList = employeeBonusService.retrieveByEmpIdBetweenDate(ids,fromMonth, toMonth);
									if(bonusList != null && bonusList.size() > 0){
										respJSON = EmployeeBonusUtil.MultipleMonthBonuReport(bonusList,CommonConstants.QUARTERLYC, fromMonth+" - "+toMonth).toString();
									}
								}
								
								if(inputJSON.get(CommonConstants.QUARTERLY).toString().equalsIgnoreCase("Q2")){
									fromMonth = "Jul"+" "+fyList.get(0).getFromyear();
									toMonth = "Sep"+" "+fyList.get(0).getFromyear(); 
									List<EmployeeBonusDO> bonusList = employeeBonusService.retrieveByEmpIdBetweenDate(ids,fromMonth, toMonth);
									if(bonusList != null && bonusList.size() > 0){
										respJSON = EmployeeBonusUtil.MultipleMonthBonuReport(bonusList,CommonConstants.QUARTERLYC, fromMonth+" - "+toMonth).toString();
									}
								}
								
								
								if(inputJSON.get(CommonConstants.QUARTERLY).toString().equalsIgnoreCase("Q3")){
									fromMonth = "Oct"+" "+fyList.get(0).getFromyear();
									toMonth = "Dec"+" "+fyList.get(0).getFromyear(); 
									List<EmployeeBonusDO> bonusList = employeeBonusService.retrieveByEmpIdBetweenDate(ids,fromMonth, toMonth);
									if(bonusList != null && bonusList.size() > 0){
										respJSON = EmployeeBonusUtil.MultipleMonthBonuReport(bonusList,CommonConstants.QUARTERLYC, fromMonth+" - "+toMonth).toString();
									}
								}
								if(inputJSON.get(CommonConstants.QUARTERLY).toString().equalsIgnoreCase("Q4")){
									fromMonth = "Jan"+" "+fyList.get(0).getToyear();
									toMonth = "Mar"+" "+fyList.get(0).getToyear(); 
									List<EmployeeBonusDO> bonusList = employeeBonusService.retrieveByEmpIdBetweenDate(ids,fromMonth, toMonth);
									if(bonusList != null && bonusList.size() > 0){
										respJSON = EmployeeBonusUtil.MultipleMonthBonuReport(bonusList,CommonConstants.QUARTERLYC, fromMonth+" - "+toMonth).toString();
									}
								}
							}
							
						}
					}else{
						if(inputJSON != null && !inputJSON.get(CommonConstants.FREQUENCY).toString().isEmpty()
								&& inputJSON.get(CommonConstants.FREQUENCY).toString().equalsIgnoreCase("Y")){
							String fromMonth = null;
							String toMonth = null;
							Long batchId = null;
							List<FyDO> fyList = new ArrayList<FyDO>();
							if(!inputJSON.get(CommonConstants.FYID).toString().isEmpty()){
								fyList = fyService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.FYID).toString()));
								if(fyList != null && fyList.size() > 0){
									List<Long> ids = new ArrayList<Long>();
									if(!inputJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty()){
										batchId = Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString());
										if(batchId == 0){
											batchId = null;
										}else{
											ids = employeeCtcService.retrieveByBatchId(batchId);
										}
									}
									fromMonth = "Apr"+" "+fyList.get(0).getFromyear();
									toMonth = "Mar"+" "+fyList.get(0).getToyear(); 
									List<EmployeeBonusDO> bonusList = employeeBonusService.retrieveByEmpIdBetweenDate(ids,fromMonth, toMonth);
									if(bonusList != null && bonusList.size() > 0){
										respJSON = EmployeeBonusUtil.MultipleMonthBonuReport(bonusList,CommonConstants.YEARLYC, fromMonth+" - "+toMonth).toString();
									}
								}
							}
						}
					}
				}
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
}
