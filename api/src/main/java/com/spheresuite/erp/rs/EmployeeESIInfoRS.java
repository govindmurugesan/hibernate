package com.spheresuite.erp.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.ESIGroupDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.EmployeeESIInfoDO;
import com.spheresuite.erp.service.ESIGroupService;
import com.spheresuite.erp.service.EmployeeESIInfoService;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.PFGroupService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.EmployeePF_ESIInfoUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/employeeesiinformation")
public class EmployeeESIInfoRS {

	String validation = null;
	static Logger logger = Logger.getLogger(EmployeeBankInformationRS.class.getName());

	@Autowired
	private EmployeeESIInfoService employeeESIInfoService;
	
	@Autowired
	private EmployeeService employeeService;
	
	@Autowired
	private ESIGroupService esiGroupService;
	
	@Autowired
	private PFGroupService pfGroupService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeESIInfoDO employeePFESIInfoDO = new EmployeeESIInfoDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.EMPID).toString() != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
				 		if(employeeList != null && employeeList.size() > 0){
				 			employeePFESIInfoDO.setEmployee(employeeList.get(0));
				 			
				 			if(inputJSON.get(CommonConstants.ESIACCOUNTNO) != null && !inputJSON.get(CommonConstants.ESIACCOUNTNO).toString().isEmpty()){
					 			employeePFESIInfoDO.setESIAccNo(inputJSON.get(CommonConstants.ESIACCOUNTNO).toString());
					 		}
					 		
					 		if(inputJSON.get(CommonConstants.ESITYPE) != null && !inputJSON.get(CommonConstants.ESITYPE).toString().isEmpty()){
					 			employeePFESIInfoDO.setType(inputJSON.get(CommonConstants.ESITYPE).toString());
					 		}
					 		
					 		if(inputJSON.get(CommonConstants.ESIAMOUNT) != null && !inputJSON.get(CommonConstants.ESIAMOUNT).toString().isEmpty()){
					 			employeePFESIInfoDO.setAmount(Long.parseLong(inputJSON.get(CommonConstants.ESIAMOUNT).toString()));
					 		}
					 		
					 		if(inputJSON.get(CommonConstants.ESIFROM) != null && !inputJSON.get(CommonConstants.ESIFROM).toString().isEmpty()){
					 			employeePFESIInfoDO.setFromDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.ESIFROM).toString()));
					 		}
					 		
					 		if(inputJSON.get(CommonConstants.ESITO) != null && !inputJSON.get(CommonConstants.ESITO).toString().isEmpty()){
					 			employeePFESIInfoDO.setToDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.ESITO).toString()));
					 		}
					 		
					 		if(inputJSON.get(CommonConstants.ESIGROUPID) != null && !inputJSON.get(CommonConstants.ESIGROUPID).toString().isEmpty()){
					 			List<ESIGroupDO> esiGroupList = esiGroupService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ESIGROUPID).toString()));
						 		if(esiGroupList != null && esiGroupList.size() > 0){
						 			employeePFESIInfoDO.setEsiGroup(esiGroupList.get(0));
						 		}
					 		}
					 		
					 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			employeePFESIInfoDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			employeePFESIInfoDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 		}
					 		
					 		employeePFESIInfoDO.setUpdatedon(new Date());
					 		employeePFESIInfoDO.setCreatedon(new Date());
					 		
					 		employeeESIInfoService.persist(employeePFESIInfoDO);
							CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Details Updated");
				 		}
			 		}
			 	}
			 	
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retrieveByEmpId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByEmpId(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<EmployeeESIInfoDO> employeeDetailList = employeeESIInfoService.retrieveByEmpId(inputJSON.get(CommonConstants.ID).toString());
					respJSON = EmployeePF_ESIInfoUtil.getEmployeeESIInfoList(employeeDetailList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeESIInfoDO employeePfAndESIInformationDO = new EmployeeESIInfoDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<EmployeeESIInfoDO> employeeDetailList = employeeESIInfoService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		employeePfAndESIInformationDO = employeeDetailList.get(0);
			 		
			 		if(inputJSON.get(CommonConstants.ESIACCOUNTNO) != null && !inputJSON.get(CommonConstants.ESIACCOUNTNO).toString().isEmpty()){
			 			employeePfAndESIInformationDO.setESIAccNo(inputJSON.get(CommonConstants.ESIACCOUNTNO).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.ESITYPE) != null && !inputJSON.get(CommonConstants.ESITYPE).toString().isEmpty()){
			 			employeePfAndESIInformationDO.setType(inputJSON.get(CommonConstants.ESITYPE).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.ESIAMOUNT) != null && !inputJSON.get(CommonConstants.ESIAMOUNT).toString().isEmpty()){
			 			employeePfAndESIInformationDO.setAmount(Long.parseLong(inputJSON.get(CommonConstants.ESIAMOUNT).toString()));
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.ESIFROM) != null && !inputJSON.get(CommonConstants.ESIFROM).toString().isEmpty()){
			 			employeePfAndESIInformationDO.setFromDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.ESIFROM).toString()));
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.ESITO) != null && !inputJSON.get(CommonConstants.ESITO).toString().isEmpty()){
			 			employeePfAndESIInformationDO.setToDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.ESITO).toString()));
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.ESIGROUPID) != null && !inputJSON.get(CommonConstants.ESIGROUPID).toString().isEmpty()){
			 			List<ESIGroupDO> esiGroupList = esiGroupService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ESIGROUPID).toString()));
				 		if(esiGroupList != null && esiGroupList.size() > 0){
				 			employeePfAndESIInformationDO.setEsiGroup(esiGroupList.get(0));
				 		}
			 		}
			 		if(inputJSON.get(CommonConstants.EMPID).toString() != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
				 		if(employeeList != null && employeeList.size() > 0){
				 			employeePfAndESIInformationDO.setEmployee(employeeList.get(0));
				 		}
			 		}
			 		employeePfAndESIInformationDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			employeePfAndESIInformationDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		employeeESIInfoService.update(employeePfAndESIInformationDO);
			 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Details Updated");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/importEmpESI", method = RequestMethod.POST)
	public @ResponseBody String importState(Model model, HttpServletRequest request) {
		try {
			JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
			JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
			List<EmployeeESIInfoDO> pflist = new ArrayList<EmployeeESIInfoDO>();
			Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY);
			for (int i=0; i < fileData.size(); i++){
				JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
				EmployeeESIInfoDO empesiDO = new EmployeeESIInfoDO();
		 		if(colName.get(CommonConstants.EMPID) != null && !colName.get(CommonConstants.EMPID).toString().isEmpty()){
		 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(rowJSON.get(colName.get(CommonConstants.EMPID)).toString());
		 			
			 		if(employeeList != null && employeeList.size() > 0){
			 			empesiDO.setEmployee(employeeList.get(0));
			 			
			 			if(colName.get(CommonConstants.ESIGROUPNAME) != null && !colName.get(CommonConstants.ESIGROUPNAME).toString().isEmpty()){
				 			List<ESIGroupDO> esiGroupList = esiGroupService.retrieveByName(rowJSON.get(colName.get(CommonConstants.ESIGROUPNAME)).toString());
					 		if(esiGroupList != null && esiGroupList.size() > 0){
					 			empesiDO.setEsiGroup(esiGroupList.get(0));
					 		}
					 		
					 		if(colName.get(CommonConstants.ESIACCOUNTNO) != null && !colName.get(CommonConstants.ESIACCOUNTNO).toString().isEmpty()){
					 			if(rowJSON.get(colName.get(CommonConstants.ESIACCOUNTNO)) != null && !rowJSON.get(colName.get(CommonConstants.ESIACCOUNTNO)).toString().isEmpty()){
					 				empesiDO.setESIAccNo(rowJSON.get(colName.get(CommonConstants.ESIACCOUNTNO)).toString());
					 			}
					 		}
					 		
					 		if(colName.get(CommonConstants.TYPE) != null && !colName.get(CommonConstants.TYPE).toString().isEmpty()){
					 			if(rowJSON.get(colName.get(CommonConstants.TYPE)) != null && !rowJSON.get(colName.get(CommonConstants.TYPE)).toString().isEmpty()){
					 				empesiDO.setType(rowJSON.get(colName.get(CommonConstants.TYPE)).toString());
					 			}
					 		}
					 		
					 		if(colName.get(CommonConstants.AMOUNT) != null && !colName.get(CommonConstants.AMOUNT).toString().isEmpty()){
					 			if(rowJSON.get(colName.get(CommonConstants.AMOUNT)) != null && !rowJSON.get(colName.get(CommonConstants.AMOUNT)).toString().isEmpty()){
					 				empesiDO.setAmount(Long.parseLong(rowJSON.get(colName.get(CommonConstants.AMOUNT)).toString()));
					 			}
					 		}
					 		
					 		if(colName.get(CommonConstants.FROMDATE) != null && !colName.get(CommonConstants.FROMDATE).toString().isEmpty()){
					 			if(rowJSON.get(colName.get(CommonConstants.FROMDATE)) != null && !rowJSON.get(colName.get(CommonConstants.FROMDATE)).toString().isEmpty()){
					 				empesiDO.setFromDate(CommonUtil.convertStringToSqlDate(rowJSON.get(colName.get(CommonConstants.FROMDATE)).toString()));
					 			}
					 		}
					 		
					 		if(colName.get(CommonConstants.TODATE) != null && !colName.get(CommonConstants.TODATE).toString().isEmpty()){
					 			if(rowJSON.get(colName.get(CommonConstants.TODATE)) != null && !rowJSON.get(colName.get(CommonConstants.TODATE)).toString().isEmpty()){
					 				empesiDO.setToDate(CommonUtil.convertStringToSqlDate(rowJSON.get(colName.get(CommonConstants.TODATE)).toString()));
					 			}
					 		}
					 		
					 		
			 				if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
			 					empesiDO.setUpdatedby(updatedBy.toString());
			 					empesiDO.setCreatedby(updatedBy.toString());
					 		}
			 				empesiDO.setUpdatedon(new Date());
			 				empesiDO.setCreatedon(new Date());
			 				pflist.add(empesiDO);
				 		}
			 			
			 			
			 			
				 		
				 		
		 			}
	 			}
			}
			employeeESIInfoService.persistList(pflist);
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
