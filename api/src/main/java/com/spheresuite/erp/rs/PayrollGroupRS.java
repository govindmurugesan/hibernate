package com.spheresuite.erp.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.PayrollGroupDO;
import com.spheresuite.erp.domainobject.UnitOrBranchDO;
import com.spheresuite.erp.service.PayrollGroupService;
import com.spheresuite.erp.service.UnitOrBranchService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.PayrollGroupUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/payrollgroup")
public class PayrollGroupRS {

	String validation = null;
	static Logger logger = Logger.getLogger(PayrollGroupRS.class.getName());
	
	@Autowired
	private PayrollGroupService payrollGroupService;
	
	@Autowired
	private UnitOrBranchService unitOrBranchService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				PayrollGroupDO payrollGroupDO = new PayrollGroupDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
			 			payrollGroupDO.setName(inputJSON.get(CommonConstants.NAME).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.UNIT) != null && !inputJSON.get(CommonConstants.UNIT).toString().isEmpty()){
			 			List <UnitOrBranchDO> unitList = unitOrBranchService.retrieveActiveById(Long.parseLong(inputJSON.get(CommonConstants.UNIT).toString()));
			 			if(unitList != null && unitList.size()>0){
			 				payrollGroupDO.setUnitOrBranch(unitList.get(0));
			 			}
			 		}
			 		if(inputJSON.get(CommonConstants.DESCRIPTION) != null && !inputJSON.get(CommonConstants.DESCRIPTION).toString().isEmpty()){
			 			payrollGroupDO.setDescription(inputJSON.get(CommonConstants.DESCRIPTION).toString());
			 		}
			 		payrollGroupDO.setStatus(CommonConstants.ACTIVE);
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			payrollGroupDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			payrollGroupDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		payrollGroupDO.setUpdatedon(new Date());
			 		payrollGroupDO.setCreatedon(new Date());
			 	}
				if(!payrollGroupService.persist(payrollGroupDO)){
		 			return CommonWebUtil.buildErrorResponse("Payroll Group Already Added").toString();
				}
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Payroll Group Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<PayrollGroupDO> payrollGroupList = payrollGroupService.retrieveActive();
				//respJSON = payrollGroupList
				respJSON = PayrollGroupUtil.getPayrollGroupList(payrollGroupList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<PayrollGroupDO> payrollGroupList = payrollGroupService.retrieve();
				respJSON = PayrollGroupUtil.getPayrollGroupList(payrollGroupList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				PayrollGroupDO payrollGroupDO = new PayrollGroupDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<PayrollGroupDO> payrollGroupList = payrollGroupService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(payrollGroupList != null && payrollGroupList.size() >0){
			 			payrollGroupDO = payrollGroupList.get(0);
				 		if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
				 			payrollGroupDO.setName(inputJSON.get(CommonConstants.NAME).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.DESCRIPTION) != null && !inputJSON.get(CommonConstants.DESCRIPTION).toString().isEmpty()){
				 			payrollGroupDO.setDescription(inputJSON.get(CommonConstants.DESCRIPTION).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
				 			payrollGroupDO.setStatus((char) inputJSON.get(CommonConstants.STATUS).toString().charAt(0));
				 		}
				 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			payrollGroupDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		payrollGroupDO.setUpdatedon(new Date());
				 		if(!payrollGroupService.update(payrollGroupDO)){
				 			return CommonWebUtil.buildErrorResponse("Payroll Group Already Added").toString();
				 		}
				 		CommonUtil.persistRecentAcvtivity(inputJSON.get(CommonConstants.UPDATED_BY).toString(), "Payroll Group Updated");
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/importPayrollGroup", method = RequestMethod.POST)
	public @ResponseBody String importPayrollBatch(Model model, HttpServletRequest request) {
		try {
			JSONObject inputJSON =  CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
			JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
			List<PayrollGroupDO> payrollGroupList = new ArrayList<PayrollGroupDO>();
			Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY);
			for (int i=0; i < fileData.size(); i++){
				JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
				PayrollGroupDO payrollGroup = new PayrollGroupDO();
				
				if (colName.get(CommonConstants.NAME) != null && !colName.get(CommonConstants.NAME).toString().isEmpty()){
					payrollGroup.setName(rowJSON.get(colName.get(CommonConstants.NAME)).toString());
		 		}
				if(colName.get(CommonConstants.DESCRIPTION) != null && !colName.get(CommonConstants.DESCRIPTION).toString().isEmpty()){
					payrollGroup.setDescription(rowJSON.get(colName.get(CommonConstants.DESCRIPTION)).toString());
		 		}
				if(inputJSON.get(CommonConstants.UNIT) != null && !inputJSON.get(CommonConstants.UNIT).toString().isEmpty()){
		 			List <UnitOrBranchDO> unitList = unitOrBranchService.retrieveActiveById(Long.parseLong(inputJSON.get(CommonConstants.UNIT).toString()));
		 			if(unitList != null && unitList.size()>0){
		 				payrollGroup.setUnitOrBranch(unitList.get(0));
		 			}
		 		}
		 		if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
		 			payrollGroup.setUpdatedby(updatedBy.toString());
		 			payrollGroup.setCreatedby(updatedBy.toString());
		 		}
		 		payrollGroup.setUpdatedon(new Date());
		 		payrollGroup.setCreatedon(new Date());
		 		payrollGroup.setStatus(CommonConstants.ACTIVE);
		 		payrollGroupList.add(payrollGroup);
			}
			payrollGroupService.persistList(payrollGroupList);
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
