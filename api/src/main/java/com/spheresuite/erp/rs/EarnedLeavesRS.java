package com.spheresuite.erp.rs;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.auth0.jwt.JWTVerifyException;
import com.spheresuite.erp.domainobject.EarnedLeavesDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.service.EarnedLeavesService;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.EarnedLeavesUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/earnedLeaves")
public class EarnedLeavesRS {

	String validation = null;
	static Logger logger = Logger.getLogger(EmployeeBankInformationRS.class.getName());

	@Autowired
	private EarnedLeavesService earnedLeavesService;
	
	@Autowired
	private EmployeeService employeeService;
	
	/*@Autowired
	private ESIGroupService esiGroupService;
	
	@Autowired
	private PFGroupService pfGroupService;*/
	
	/*@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeePFInfoDO employeePFESIInfoDO = new EmployeePFInfoDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.EMPID).toString() != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
				 		if(employeeList != null && employeeList.size() > 0){
				 			employeePFESIInfoDO.setEmployee(employeeList.get(0));
				 			
				 			if(inputJSON.get(CommonConstants.PFACCOUNTNO) != null && !inputJSON.get(CommonConstants.PFACCOUNTNO).toString().isEmpty()){
					 			employeePFESIInfoDO.setPFAccNo(inputJSON.get(CommonConstants.PFACCOUNTNO).toString());
					 		}
					 		
					 		if(inputJSON.get(CommonConstants.PFTYPE) != null && !inputJSON.get(CommonConstants.PFTYPE).toString().isEmpty()){
					 			employeePFESIInfoDO.setType(inputJSON.get(CommonConstants.PFTYPE).toString());
					 		}
					 		
					 		if(inputJSON.get(CommonConstants.UAN) != null && !inputJSON.get(CommonConstants.UAN).toString().isEmpty()){
					 			employeePFESIInfoDO.setUan(inputJSON.get(CommonConstants.UAN).toString());
					 		}
					 		
					 		if(inputJSON.get(CommonConstants.PFAMOUNT) != null && !inputJSON.get(CommonConstants.PFAMOUNT).toString().isEmpty()){
					 			employeePFESIInfoDO.setAmount(Long.parseLong(inputJSON.get(CommonConstants.PFAMOUNT).toString()));
					 		}
					 		
					 		if(inputJSON.get(CommonConstants.PFFROM) != null && !inputJSON.get(CommonConstants.PFFROM).toString().isEmpty()){
					 			employeePFESIInfoDO.setFromDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.PFFROM).toString()));
					 		}
					 		
					 		if(inputJSON.get(CommonConstants.PFTO) != null && !inputJSON.get(CommonConstants.PFTO).toString().isEmpty()){
					 			employeePFESIInfoDO.setToDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.PFTO).toString()));
					 		}
					 		
					 		if(inputJSON.get(CommonConstants.PFGROUPID) != null && !inputJSON.get(CommonConstants.PFGROUPID).toString().isEmpty()){
					 			List<PFGroupDO> pfGroupList = pfGroupService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.PFGROUPID).toString()));
						 		if(pfGroupList != null && pfGroupList.size() > 0){
						 			employeePFESIInfoDO.setPfGroup(pfGroupList.get(0));
						 		}
					 		}
					 		
					 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			employeePFESIInfoDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			employeePFESIInfoDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 		}
					 		
					 		employeePFESIInfoDO.setUpdatedon(new Date());
					 		employeePFESIInfoDO.setCreatedon(new Date());
					 		
					 		employeePF_ESIInfoService.persist(employeePFESIInfoDO);
							CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Details Updated");
				 		}
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.ESIACCOUNTNO) != null && !inputJSON.get(CommonConstants.ESIACCOUNTNO).toString().isEmpty()){
			 			employeePFESIInfoDO.setESIAccNo(inputJSON.get(CommonConstants.ESIACCOUNTNO).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.ESIGROUPID) != null && !inputJSON.get(CommonConstants.ESIGROUPID).toString().isEmpty()){
			 			List<ESIGroupDO> esiGroupList = esiGroupService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ESIGROUPID).toString()));
				 		if(esiGroupList != null && esiGroupList.size() > 0){
				 			employeePFESIInfoDO.setEsiGroup(esiGroupList.get(0));
				 		}
			 		}
			 	}
			 	
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeePFInfoDO employeePfAndESIInformationDO = new EmployeePFInfoDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<EmployeePFInfoDO> employeeDetailList = employeePF_ESIInfoService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		employeePfAndESIInformationDO = employeeDetailList.get(0);
			 		
			 		if(inputJSON.get(CommonConstants.PFACCOUNTNO) != null && !inputJSON.get(CommonConstants.PFACCOUNTNO).toString().isEmpty()){
			 			employeePfAndESIInformationDO.setPFAccNo(inputJSON.get(CommonConstants.PFACCOUNTNO).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.ESIACCOUNTNO) != null && !inputJSON.get(CommonConstants.ESIACCOUNTNO).toString().isEmpty()){
			 			employeePfAndESIInformationDO.setESIAccNo(inputJSON.get(CommonConstants.ESIACCOUNTNO).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.PFTYPE) != null && !inputJSON.get(CommonConstants.PFTYPE).toString().isEmpty()){
			 			employeePfAndESIInformationDO.setType(inputJSON.get(CommonConstants.PFTYPE).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.UAN) != null && !inputJSON.get(CommonConstants.UAN).toString().isEmpty()){
			 			employeePfAndESIInformationDO.setUan(inputJSON.get(CommonConstants.UAN).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.PFAMOUNT) != null && !inputJSON.get(CommonConstants.PFAMOUNT).toString().isEmpty()){
			 			employeePfAndESIInformationDO.setAmount(Long.parseLong(inputJSON.get(CommonConstants.PFAMOUNT).toString()));
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.PFFROM) != null && !inputJSON.get(CommonConstants.PFFROM).toString().isEmpty()){
			 			employeePfAndESIInformationDO.setFromDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.PFFROM).toString()));
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.PFTO) != null && !inputJSON.get(CommonConstants.PFTO).toString().isEmpty()){
			 			employeePfAndESIInformationDO.setToDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.PFTO).toString()));
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.PFGROUPID) != null && !inputJSON.get(CommonConstants.PFGROUPID).toString().isEmpty()){
			 			List<PFGroupDO> pfGroupList = pfGroupService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.PFGROUPID).toString()));
				 		if(pfGroupList != null && pfGroupList.size() > 0){
				 			employeePfAndESIInformationDO.setPfGroup(pfGroupList.get(0));
				 		}
			 		}
			 		if(inputJSON.get(CommonConstants.ESIGROUPID) != null && !inputJSON.get(CommonConstants.ESIGROUPID).toString().isEmpty()){
			 			List<ESIGroupDO> esiGroupList = esiGroupService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ESIGROUPID).toString()));
				 		if(esiGroupList != null && esiGroupList.size() > 0){
				 			employeePfAndESIInformationDO.setEsiGroup(esiGroupList.get(0));
				 		}
			 		}
			 		if(inputJSON.get(CommonConstants.EMPID).toString() != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
				 		if(employeeList != null && employeeList.size() > 0){
				 			employeePfAndESIInformationDO.setEmployee(employeeList.get(0));
				 		}
			 		}
			 		employeePfAndESIInformationDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			employeePfAndESIInformationDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		employeePF_ESIInfoService.update(employeePfAndESIInformationDO);
			 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Details Updated");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}*/
	
	@RequestMapping(value = "/retrieveByEmpId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByEmpId(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<EarnedLeavesDO> employeeDetailList = earnedLeavesService.retrieveByEmpId(inputJSON.get(CommonConstants.ID).toString());
					respJSON = EarnedLeavesUtil.getEmployeeEarnedLeavesList(employeeDetailList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) throws InvalidKeyException, NoSuchAlgorithmException, IllegalStateException, SignatureException, IOException, JWTVerifyException {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<EarnedLeavesDO> earnedLeaveList = earnedLeavesService.retrieve();
				respJSON = EarnedLeavesUtil.getEmployeeEarnedLeavesList(earnedLeaveList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	@RequestMapping(value = "/import", method = RequestMethod.POST)
	public @ResponseBody String importState(Model model, HttpServletRequest request) {
		try {
			JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
			JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
			List<EarnedLeavesDO> earnedLeavelist = new ArrayList<EarnedLeavesDO>();
			Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY);
			for (int i=0; i < fileData.size(); i++){
				JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
				EarnedLeavesDO earnedLeaveDO = new EarnedLeavesDO();
		 		if(colName.get(CommonConstants.EMPID) != null && !colName.get(CommonConstants.EMPID).toString().isEmpty()){
		 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(rowJSON.get(colName.get(CommonConstants.EMPID)).toString());
		 			
			 		if(employeeList != null && employeeList.size() > 0){
			 			earnedLeaveDO.setEmployee(employeeList.get(0));
			 			
				 		/*if(colName.get(CommonConstants.AVAILEDLEAVES) != null && !colName.get(CommonConstants.AVAILEDLEAVES).toString().isEmpty()){
				 			earnedLeaveDO.setAvailedLeaves(Long.parseLong(rowJSON.get(colName.get(CommonConstants.AVAILEDLEAVES)).toString()));
				 		}*/
				 		
				 		if(colName.get(CommonConstants.AVAILABLELEAVES) != null && !colName.get(CommonConstants.AVAILABLELEAVES).toString().isEmpty()){
				 			earnedLeaveDO.setAvailableLeaves(Long.parseLong(rowJSON.get(colName.get(CommonConstants.AVAILABLELEAVES)).toString()));
				 		}
				 		
				 		if(colName.get(CommonConstants.FROMDATE) != null && !colName.get(CommonConstants.FROMDATE).toString().isEmpty()){
				 			earnedLeaveDO.setFromDate(CommonUtil.convertStringToSqlDate(rowJSON.get(colName.get(CommonConstants.FROMDATE)).toString()));
				 		}
				 		
				 		if(colName.get(CommonConstants.TODATE) != null && !colName.get(CommonConstants.TODATE).toString().isEmpty()){
				 			earnedLeaveDO.setToDate(CommonUtil.convertStringToSqlDate(rowJSON.get(colName.get(CommonConstants.TODATE)).toString()));
				 		}
		 				if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
		 					earnedLeaveDO.setUpdatedby(updatedBy.toString());
		 					earnedLeaveDO.setCreatedby(updatedBy.toString());
				 		}
		 				earnedLeaveDO.setUpdatedon(new Date());
		 				earnedLeaveDO.setCreatedon(new Date());
		 				earnedLeavelist.add(earnedLeaveDO);
		 			}
	 			}
			}
			earnedLeavesService.persistList(earnedLeavelist);
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
