package com.spheresuite.erp.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.FyDO;
import com.spheresuite.erp.service.FyService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.FyUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/fy")
public class FyRS {

	String validation = null;
	static Logger logger = Logger.getLogger(FyRS.class.getName());

	@Autowired
	private FyService fyService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				FyDO fyDO = new FyDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.FYFROM) != null && !inputJSON.get(CommonConstants.FYFROM).toString().isEmpty()){
			 			fyDO.setFromyear(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.FYFROM).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.FYTO) != null && !inputJSON.get(CommonConstants.FYTO).toString().isEmpty()){
			 			fyDO.setToyear(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.FYTO).toString()));
			 		}
			 		fyDO.setStatus(CommonConstants.ACTIVE);
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			fyDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			fyDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		fyDO.setUpdatedon(new Date());
			 		fyDO.setCreatedon(new Date());
			 	}
				if(!fyService.persist(fyDO)){
					return CommonWebUtil.buildErrorResponse("Financial Year Already Added").toString();
				}
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Financial Year Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<FyDO> leadStatusList = fyService.retrieveActive();
				respJSON = FyUtil.getFyList(leadStatusList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<FyDO> leadStatusList = fyService.retrieve();
				respJSON = FyUtil.getFyList(leadStatusList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveById/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveById(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
					if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<FyDO> leadStatusList = fyService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = FyUtil.getFyList(leadStatusList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				FyDO fyDO = new FyDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<FyDO> fyList = fyService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(fyList != null && fyList.size() > 0){
				 		fyDO = fyList.get(0);
				 		if(inputJSON.get(CommonConstants.FYFROM) != null && !inputJSON.get(CommonConstants.FYFROM).toString().isEmpty()){
				 			fyDO.setFromyear(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.FYFROM).toString()));
				 		}
				 		if(inputJSON.get(CommonConstants.FYTO) != null && !inputJSON.get(CommonConstants.FYTO).toString().isEmpty()){
				 			fyDO.setToyear(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.FYTO).toString()));
				 		}
				 		fyDO.setUpdatedon(new Date());
				 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			fyDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
				 			fyDO.setStatus((char) inputJSON.get(CommonConstants.STATUS).toString().charAt(0));
				 		}
				 		if(!fyService.update(fyDO)){
							return CommonWebUtil.buildErrorResponse("Financial Year Already Added").toString();
						}
				 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Financial Year Updated");
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/importFY", method = RequestMethod.POST)
	public @ResponseBody String importFY(Model model, HttpServletRequest request) {
		try {
			JSONObject inputJSON =  CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
			JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
			List<FyDO> fyList = new ArrayList<FyDO>();
			Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY);
			for (int i=0; i < fileData.size(); i++){
				JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
				FyDO fyDO = new FyDO();
				if(colName.get(CommonConstants.FYFROM) != null && !colName.get(CommonConstants.FYFROM).toString().isEmpty()){
		 			fyDO.setFromyear(CommonUtil.convertStringToSqlDate(rowJSON.get(colName.get(CommonConstants.FYFROM)).toString()));
		 		}
		 		if(colName.get(CommonConstants.FYTO) != null && !colName.get(CommonConstants.FYTO).toString().isEmpty()){
		 			fyDO.setToyear(CommonUtil.convertStringToSqlDate(rowJSON.get(colName.get(CommonConstants.FYTO)).toString()));
		 		}
		 		if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
		 			fyDO.setUpdatedby(updatedBy.toString());
		 			fyDO.setCreatedby(updatedBy.toString());
		 		}
		 		fyDO.setUpdatedon(new Date());
		 		fyDO.setCreatedon(new Date());
		 		fyDO.setStatus(CommonConstants.ACTIVE);
		 		fyList.add(fyDO);
			}
			fyService.persistList(fyList);
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
