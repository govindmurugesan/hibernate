package com.spheresuite.erp.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.ItSectionDO;
import com.spheresuite.erp.service.ItSectionService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.ItSectionUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/itSection")
public class ItSectionRS {

	String validation = null;
	static Logger logger = Logger.getLogger(ItSectionRS.class.getName());

	@Autowired
	private ItSectionService itSectionService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				ItSectionDO itSectionDO = new ItSectionDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.FROMDATE) != null && !inputJSON.get(CommonConstants.FROMDATE).toString().isEmpty()){
			 			itSectionDO.setFromYear(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.FROMDATE).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.TODATE) != null && !inputJSON.get(CommonConstants.TODATE).toString().isEmpty()){
			 			itSectionDO.setToYear(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.TODATE).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.SECTION) != null && !inputJSON.get(CommonConstants.SECTION).toString().isEmpty()){
			 			itSectionDO.setSection(inputJSON.get(CommonConstants.SECTION).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.MAXLIMIT) != null && !inputJSON.get(CommonConstants.MAXLIMIT).toString().isEmpty()){
			 			itSectionDO.setMaxLimit(Long.parseLong(inputJSON.get(CommonConstants.MAXLIMIT).toString()));
			 		}
			 		itSectionDO.setStatus(CommonConstants.ACTIVE);
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			itSectionDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			itSectionDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		itSectionDO.setUpdatedon(new Date());
			 		itSectionDO.setCreatedon(new Date());
			 	}
				if(!itSectionService.persist(itSectionDO)){
					return CommonWebUtil.buildErrorResponse("IT Section Already Added").toString();
				}
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New IT Section Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<ItSectionDO> itSectionList = itSectionService.retrieveActive();
				respJSON = ItSectionUtil.getSectionList(itSectionList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<ItSectionDO> itSectionList = itSectionService.retrieve();
				respJSON = ItSectionUtil.getSectionList(itSectionList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	/*@RequestMapping(value = "/retrieveByDate/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieve(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				List<ItSectionDO> itSectionList = itSectionService.retrieveByDate(inputJSON.get(CommonConstants.FROMDATE).toString(),inputJSON.get(CommonConstants.TODATE).toString());
				respJSON = ItSectionUtil.getSectionList(itSectionList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}*/
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				ItSectionDO itSectionDO = new ItSectionDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<ItSectionDO> itSectionList = itSectionService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(itSectionList != null && itSectionList.size() > 0){
				 		itSectionDO = itSectionList.get(0);
				 		
				 		if(inputJSON.get(CommonConstants.FROMDATE) != null && !inputJSON.get(CommonConstants.FROMDATE).toString().isEmpty()){
				 			itSectionDO.setFromYear(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.FROMDATE).toString()));
				 		}
				 		if(inputJSON.get(CommonConstants.TODATE) != null && !inputJSON.get(CommonConstants.TODATE).toString().isEmpty()){
				 			itSectionDO.setToYear(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.TODATE).toString()));
				 		}
				 		if(inputJSON.get(CommonConstants.SECTION) != null && !inputJSON.get(CommonConstants.SECTION).toString().isEmpty()){
				 			itSectionDO.setSection(inputJSON.get(CommonConstants.SECTION).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.MAXLIMIT) != null && !inputJSON.get(CommonConstants.MAXLIMIT).toString().isEmpty()){
				 			itSectionDO.setMaxLimit(Long.parseLong(inputJSON.get(CommonConstants.MAXLIMIT).toString()));
				 		}
				 		if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
				 			itSectionDO.setStatus((char)inputJSON.get(CommonConstants.STATUS).toString().charAt(0));
				 		}
				 		itSectionDO.setUpdatedon(new Date());
				 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			itSectionDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		if(!itSectionService.update(itSectionDO)){
							return CommonWebUtil.buildErrorResponse("ItSection Already Added").toString();
						}
				 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "ItSection Updated");
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/importITSection", method = RequestMethod.POST)
	public @ResponseBody String importITSection(Model model, HttpServletRequest request) {
		try {
			JSONObject inputJSON =  CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
			JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
			List<ItSectionDO> itSectionlist = new ArrayList<ItSectionDO>();
			Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY);
			for (int i=0; i < fileData.size(); i++){
				JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
				ItSectionDO itSectionDO = new ItSectionDO();
				
				if (colName.get(CommonConstants.SECTION) != null && !colName.get(CommonConstants.SECTION).toString().isEmpty()){
					itSectionDO.setSection(rowJSON.get(colName.get(CommonConstants.SECTION)).toString());
		 		}
				if(colName.get(CommonConstants.MAXLIMIT) != null && !colName.get(CommonConstants.MAXLIMIT).toString().isEmpty()){
					itSectionDO.setMaxLimit(Long.parseLong(rowJSON.get(colName.get(CommonConstants.MAXLIMIT)).toString()));
				}
				if(colName.get(CommonConstants.TODATE) != null && !colName.get(CommonConstants.TODATE).toString().isEmpty()){
					itSectionDO.setToYear(CommonUtil.convertStringToSqlDate(rowJSON.get(colName.get(CommonConstants.TODATE)).toString()));
				}
				if(colName.get(CommonConstants.FROMDATE) != null && !colName.get(CommonConstants.FROMDATE).toString().isEmpty()){
					itSectionDO.setFromYear(CommonUtil.convertStringToSqlDate(rowJSON.get(colName.get(CommonConstants.FROMDATE)).toString()));
				}
		 		if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
		 			itSectionDO.setUpdatedby(updatedBy.toString());
		 			itSectionDO.setCreatedby(updatedBy.toString());
		 		}
		 		itSectionDO.setUpdatedon(new Date());
		 		itSectionDO.setCreatedon(new Date());
		 		itSectionDO.setStatus(CommonConstants.ACTIVE);
		 		itSectionlist.add(itSectionDO);
			}
			itSectionService.persistList(itSectionlist);
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
