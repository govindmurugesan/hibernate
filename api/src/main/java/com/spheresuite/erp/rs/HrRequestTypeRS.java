package com.spheresuite.erp.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.HrRequestTypeDO;
import com.spheresuite.erp.service.HrRequestTypeService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.HrRequestTypeUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/hrRequestType")
public class HrRequestTypeRS {

	String validation = null;
	static Logger logger = Logger.getLogger(HrRequestTypeRS.class.getName());

	@Autowired
	private HrRequestTypeService hrRequestTypeService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				HrRequestTypeDO hrRequestTypeDO = new HrRequestTypeDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.REQUESTTYPE).toString() != null && !inputJSON.get(CommonConstants.REQUESTTYPE).toString().isEmpty()){
			 			hrRequestTypeDO.setRequestType(inputJSON.get(CommonConstants.REQUESTTYPE).toString());
			 		}
			 		hrRequestTypeDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			hrRequestTypeDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			hrRequestTypeDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		hrRequestTypeDO.setStatus(CommonConstants.ACTIVE);
			 	}
				if(!hrRequestTypeService.persist(hrRequestTypeDO)){
		 			return CommonWebUtil.buildErrorResponse("HR Request Type Already Added").toString();
				}
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New HR RequestType Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<HrRequestTypeDO> hrRequestTypeList = hrRequestTypeService.retrieve();
				respJSON = HrRequestTypeUtil.getHrRequestTypeList(hrRequestTypeList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				HrRequestTypeDO hrRequestTypeDO = new HrRequestTypeDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<HrRequestTypeDO> hrRequestTypeList = hrRequestTypeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(hrRequestTypeList != null && hrRequestTypeList.size() > 0){
				 		hrRequestTypeDO = hrRequestTypeList.get(0);
				 		if(inputJSON.get(CommonConstants.REQUESTTYPE).toString() != null && !inputJSON.get(CommonConstants.REQUESTTYPE).toString().isEmpty()){
				 			hrRequestTypeDO.setRequestType(inputJSON.get(CommonConstants.REQUESTTYPE).toString());
				 		}
				 		hrRequestTypeDO.setUpdatedon(new Date());
				 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			hrRequestTypeDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
				 			hrRequestTypeDO.setStatus((char)inputJSON.get(CommonConstants.STATUS).toString().charAt(0));
				 		}
				 		if(!hrRequestTypeService.update(hrRequestTypeDO)){
				 			return CommonWebUtil.buildErrorResponse("HR Request Type Already Added").toString();
				 		}
				 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "HR RequestType Updated");
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/importHRRequestType", method = RequestMethod.POST)
	public @ResponseBody String importHRRequestType(Model model, HttpServletRequest request) {
		try {
			JSONObject inputJSON =  CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
			JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
			List<HrRequestTypeDO> hrRequestTypeList = new ArrayList<HrRequestTypeDO>();
			Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY);
			for (int i=0; i < fileData.size(); i++){
				JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
				HrRequestTypeDO hrRequestTypeDO = new HrRequestTypeDO();
				if (colName.get(CommonConstants.REQUESTTYPE) != null && !colName.get(CommonConstants.REQUESTTYPE).toString().isEmpty()){
					hrRequestTypeDO.setRequestType(rowJSON.get(colName.get(CommonConstants.REQUESTTYPE)).toString());
		 		}
		 		if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
		 			hrRequestTypeDO.setUpdatedby(updatedBy.toString());
		 			hrRequestTypeDO.setCreatedby(updatedBy.toString());
		 		}
		 		hrRequestTypeDO.setUpdatedon(new Date());
		 		hrRequestTypeDO.setCreatedon(new Date());
		 		hrRequestTypeDO.setStatus(CommonConstants.ACTIVE);
		 		hrRequestTypeList.add(hrRequestTypeDO);
			}
			hrRequestTypeService.persistList(hrRequestTypeList);
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
