package com.spheresuite.erp.rs;

import io.jsonwebtoken.SignatureException;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.auth0.jwt.JWTVerifyException;
import com.spheresuite.erp.domainobject.EmployeeOnboardDO;
import com.spheresuite.erp.service.EmployeeOnboardService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.EmailProxyUtil;
import com.spheresuite.erp.web.util.EmployeeOnboardUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/employeeonboard")
public class EmployeeOnboardRS {

	String validation = null;
	static Logger logger = Logger.getLogger(EmployeeOnboardRS.class.getName());

	@Autowired
	private EmployeeOnboardService employeeOnboardService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		EmployeeOnboardDO employeeOnboardDO = new EmployeeOnboardDO();
		try {
			if (WebManager.authenticateSession(request)) {
				
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
			 			employeeOnboardDO.setFirstname(inputJSON.get(CommonConstants.NAME).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.PERSONALCONTACT) != null && !inputJSON.get(CommonConstants.PERSONALCONTACT).toString().isEmpty()){
			 			employeeOnboardDO.setPersonalcontact(inputJSON.get(CommonConstants.PERSONALCONTACT).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.PERSONALEMAIL) != null && !inputJSON.get(CommonConstants.PERSONALEMAIL).toString().isEmpty()){
			 			employeeOnboardDO.setPersonalemail(inputJSON.get(CommonConstants.PERSONALEMAIL).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			employeeOnboardDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			employeeOnboardDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		employeeOnboardDO.setUpdatedon(new Date());
			 		employeeOnboardDO.setCreatedon(new Date());
			 		employeeOnboardDO.setStatus(CommonConstants.PENDING_STATUS);
			 		/*if(!employeeOnboardService.persist(employeeOnboardDO)){
						return CommonWebUtil.buildErrorResponse("Employee OnBoard Details Already Added").toString();
					}*/
			 		
		 			}else{
		 				return CommonWebUtil.buildErrorResponse("").toString();
		 			}
			 		List<EmployeeOnboardDO> employeeOnboardList = employeeOnboardService.retrieveByEmail(inputJSON.get(CommonConstants.PERSONALEMAIL).toString());
			 		if(employeeOnboardList.size() == 0){
			 			employeeOnboardService.persist(employeeOnboardDO);
				 		String URL = CommonUtil.getUIUrl(request);
						URL = CommonConstants.HTTP+URL+"#!/employee/onboardform";
						String url = URL+"/"+employeeOnboardDO.getEmployeeOnboardId();
						String emailBody = "Cick Here to Onboard : " +url;
						String host = request.getServletContext().getInitParameter(CommonConstants.HOST);
						String fromEmail = CommonConstants.SENDMAIL;
						String toEmails = employeeOnboardDO.getPersonalemail();
				 		boolean mailStatus = EmailProxyUtil.sendEmail(request,host,  fromEmail,  toEmails, "", "", emailBody, false, url, "onboardEmployee", employeeOnboardDO.getFirstname()); 
						if(mailStatus){
							CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New EmployeeOnboard Created");
							return CommonWebUtil.buildSuccessResponseId(employeeOnboardDO.getEmployeeOnboardId().toString()).toString();
						}else{
							employeeOnboardService.delete(employeeOnboardDO.getEmployeeOnboardId());
							return CommonWebUtil.buildErrorResponse("Network Error").toString();
						}
			 		} else {
			 			return CommonWebUtil.buildErrorResponse("Email Already Available").toString();
			 		}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			employeeOnboardService.delete(employeeOnboardDO.getEmployeeOnboardId());
			return CommonWebUtil.buildErrorResponse("Network Error").toString();
		}		
	}
	
	
	@RequestMapping(value = "/retrieveByOnboardId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByOnboardId(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<EmployeeOnboardDO> employeeOnboardList = employeeOnboardService.retriveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = EmployeeOnboardUtil.getEmployeeList(employeeOnboardList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeOnboardDO employeeOnboardDO = new EmployeeOnboardDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
		 				List<EmployeeOnboardDO> employeeOnboardList = employeeOnboardService.retriveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
				 		if(employeeOnboardList != null && employeeOnboardList.size() > 0){
				 			employeeOnboardDO = employeeOnboardList.get(0);
				 			if(inputJSON.get(CommonConstants.FIRSTNAME) != null && !inputJSON.get(CommonConstants.FIRSTNAME).toString().isEmpty()){
				 				employeeOnboardDO.setFirstname(inputJSON.get(CommonConstants.FIRSTNAME).toString());
					 		}
				 			if(inputJSON.get(CommonConstants.LASTNAME) != null && !inputJSON.get(CommonConstants.LASTNAME).toString().isEmpty()){
				 				employeeOnboardDO.setLastname(inputJSON.get(CommonConstants.LASTNAME).toString());
					 		}
				 			if(inputJSON.get(CommonConstants.MIDDLENAME) != null && !inputJSON.get(CommonConstants.MIDDLENAME).toString().isEmpty()){
				 				employeeOnboardDO.setMiddlename(inputJSON.get(CommonConstants.MIDDLENAME).toString());
					 		}
				 			if(inputJSON.get(CommonConstants.PERSONALCONTACT) != null && !inputJSON.get(CommonConstants.PERSONALCONTACT).toString().isEmpty()){
				 				employeeOnboardDO.setPersonalcontact(inputJSON.get(CommonConstants.PERSONALCONTACT).toString());
					 		}
				 			if(inputJSON.get(CommonConstants.PRIMARYCONTACT) != null && !inputJSON.get(CommonConstants.PRIMARYCONTACT).toString().isEmpty()){
				 				employeeOnboardDO.setPrimarycontact(inputJSON.get(CommonConstants.PRIMARYCONTACT).toString());
					 		}
				 			if(inputJSON.get(CommonConstants.PERSONALCONTACT) != null && !inputJSON.get(CommonConstants.PERSONALCONTACT).toString().isEmpty()){
				 				employeeOnboardDO.setPersonalcontact(inputJSON.get(CommonConstants.PERSONALCONTACT).toString());
					 		}
				 			if(inputJSON.get(CommonConstants.PERSONALEMAIL) != null && !inputJSON.get(CommonConstants.PERSONALEMAIL).toString().isEmpty()){
				 				employeeOnboardDO.setPersonalemail(inputJSON.get(CommonConstants.PERSONALEMAIL).toString());
					 		}
				 			if(inputJSON.get(CommonConstants.AADHAR) != null && !inputJSON.get(CommonConstants.AADHAR).toString().isEmpty()){
				 				employeeOnboardDO.setAadhar(inputJSON.get(CommonConstants.AADHAR).toString());
					 		}
				 			if(inputJSON.get(CommonConstants.PANNO) != null && !inputJSON.get(CommonConstants.PANNO).toString().isEmpty()){
				 				employeeOnboardDO.setPanno(inputJSON.get(CommonConstants.PANNO).toString());
					 		}
				 			if(inputJSON.get(CommonConstants.DATEOFBIRTH) != null && !inputJSON.get(CommonConstants.DATEOFBIRTH).toString().isEmpty()){
				 				employeeOnboardDO.setDateofbirth(inputJSON.get(CommonConstants.DATEOFBIRTH).toString());
					 		}
				 			if(inputJSON.get(CommonConstants.GENDER) != null && !inputJSON.get(CommonConstants.GENDER).toString().isEmpty()){
				 				employeeOnboardDO.setGender((char) inputJSON.get(CommonConstants.GENDER).toString().charAt(0));
					 		}
				 			if(inputJSON.get(CommonConstants.REASON) != null && !inputJSON.get(CommonConstants.REASON).toString().isEmpty()){
				 				employeeOnboardDO.setReason(inputJSON.get(CommonConstants.REASON).toString());
					 		}
				 			if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
				 				employeeOnboardDO.setStatus((char)  inputJSON.get(CommonConstants.STATUS).toString().charAt(0));
					 		}
					 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			employeeOnboardDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 		}
					 		employeeOnboardDO.setUpdatedon(new Date());
					 		if(!employeeOnboardService.update(employeeOnboardDO)){
								return CommonWebUtil.buildErrorResponse("Employee OnBoard Details Already Added").toString();
							}
					 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Employee OnBoard Updated");
				 		}
		 		}else{
		 			return CommonWebUtil.buildErrorResponse(CommonConstants.COMPANYEMAIL_FOUND).toString();
		 		}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) throws InvalidKeyException, NoSuchAlgorithmException, IllegalStateException, SignatureException, IOException, JWTVerifyException {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<EmployeeOnboardDO> employeeOnboardList = employeeOnboardService.retrieve();
				respJSON = EmployeeOnboardUtil.getEmployeeList(employeeOnboardList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
}
