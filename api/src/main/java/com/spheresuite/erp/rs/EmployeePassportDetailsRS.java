package com.spheresuite.erp.rs;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.EmployeePassportDetailsDO;
import com.spheresuite.erp.service.EmployeePassportDetailsService;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.EmployeePassportDetailsUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/employeepassportdetails")
public class EmployeePassportDetailsRS {

	String validation = null;
	static Logger logger = Logger.getLogger(EmployeePassportDetailsRS.class.getName());

	@Autowired
	private EmployeePassportDetailsService employeePassportDetailsService;
	
	@Autowired
	private EmployeeService employeeService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeePassportDetailsDO employeePassportInfo = new EmployeePassportDetailsDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.EMPID).toString() != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
				 		if(employeeList != null && employeeList.size() > 0){
				 			employeePassportInfo.setEmployee(employeeList.get(0));
				 			if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			employeePassportInfo.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			employeePassportInfo.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			if(inputJSON.get(CommonConstants.NUMBER)!= null && !inputJSON.get(CommonConstants.NUMBER).toString().isEmpty()){
						 			employeePassportInfo.setPassportNumber(inputJSON.get(CommonConstants.NUMBER).toString());
						 		}
						 		if(inputJSON.get(CommonConstants.PLACEOFISSUE) != null && !inputJSON.get(CommonConstants.PLACEOFISSUE).toString().isEmpty()){
						 			employeePassportInfo.setPlaceOfIssue(inputJSON.get(CommonConstants.PLACEOFISSUE).toString());
						 		}
						 		if(inputJSON.get(CommonConstants.DATEISSUED).toString() != null && !inputJSON.get(CommonConstants.DATEISSUED).toString().isEmpty()){
						 			employeePassportInfo.setDateOfIssue(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.DATEISSUED).toString()));
						 		}
						 		if(inputJSON.get(CommonConstants.DATEOFEXPIRY).toString() != null && !inputJSON.get(CommonConstants.DATEOFEXPIRY).toString().isEmpty()){
						 			employeePassportInfo.setDateOfExpiry(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.DATEOFEXPIRY).toString()));
						 		}
						 		employeePassportInfo.setUpdatedon(new Date());
						 		employeePassportInfo.setCreatedon(new Date());
						 		employeePassportDetailsService.persist(employeePassportInfo);
								CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Passport Information Created For Employee");
					 		}
				 		}
			 		}
			 	}
			 	
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveByEmpId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByEmpId(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<EmployeePassportDetailsDO> employeePassportList = employeePassportDetailsService.retrieveByEmpId(inputJSON.get(CommonConstants.ID).toString());
					respJSON = EmployeePassportDetailsUtil.getEmployeePassportInfoList(employeePassportList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeePassportDetailsDO employeePassportDO = new EmployeePassportDetailsDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<EmployeePassportDetailsDO> employeePassportInfoList = employeePassportDetailsService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		employeePassportDO = employeePassportInfoList.get(0);
			 		if(inputJSON.get(CommonConstants.NUMBER)!= null && !inputJSON.get(CommonConstants.NUMBER).toString().isEmpty()){
			 			employeePassportDO.setPassportNumber(inputJSON.get(CommonConstants.NUMBER).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.PLACEOFISSUE) != null && !inputJSON.get(CommonConstants.PLACEOFISSUE).toString().isEmpty()){
			 			employeePassportDO.setPlaceOfIssue(inputJSON.get(CommonConstants.PLACEOFISSUE).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.DATEISSUED).toString() != null && !inputJSON.get(CommonConstants.DATEISSUED).toString().isEmpty()){
			 			employeePassportDO.setDateOfIssue(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.DATEISSUED).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.DATEOFEXPIRY).toString() != null && !inputJSON.get(CommonConstants.DATEOFEXPIRY).toString().isEmpty()){
			 			employeePassportDO.setDateOfExpiry(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.DATEOFEXPIRY).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.EMPID) != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
				 		if(employeeList != null && employeeList.size() > 0){
				 			employeePassportDO.setEmployee(employeeList.get(0));
				 		}
			 		}
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			employeePassportDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		employeePassportDO.setUpdatedon(new Date());
			 		employeePassportDetailsService.update(employeePassportDO);
			 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Passport Information Updated For Employee");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
