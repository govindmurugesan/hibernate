package com.spheresuite.erp.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.AllowanceSettingDO;
import com.spheresuite.erp.domainobject.ESIGroupDO;
import com.spheresuite.erp.domainobject.ESIGroupEarningsDO;
import com.spheresuite.erp.service.AllowanceSettingsService;
import com.spheresuite.erp.service.ESIGroupEarningService;
import com.spheresuite.erp.service.ESIGroupService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.ESIGroupUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/esigroupsettings")
public class ESIGroupSettingsRS {

	String validation = null;
	static Logger logger = Logger.getLogger(ESIGroupSettingsRS.class.getName());
	
	@Autowired
	private ESIGroupService esiGroupService;
	
	@Autowired
	private ESIGroupEarningService esiGroupEarningService;
	
	@Autowired
	private AllowanceSettingsService allowanceSettingsService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		
			 		ESIGroupDO esi = new ESIGroupDO();
			 		if(inputJSON.get(CommonConstants.ESIGROUPID) != null && !inputJSON.get(CommonConstants.ESIGROUPID).toString().isEmpty()){
			 			List<ESIGroupDO> esiGroupList = esiGroupService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ESIGROUPID).toString()));
				 		if(esiGroupList != null && esiGroupList.size() >0){
				 			esi = esiGroupList.get(0);
				 			if(inputJSON.get(CommonConstants.AMOUNT) != null && !inputJSON.get(CommonConstants.AMOUNT).toString().isEmpty()){
				 				esi.setAmount(Double.parseDouble(inputJSON.get(CommonConstants.AMOUNT).toString()));
				 			}
				 			esiGroupService.update(esi);
				 			
				 		}
			 		}
			 		
			 		
			 		if (inputJSON.get(CommonConstants.ALLOWENCE_ID) != null && !inputJSON.get(CommonConstants.ALLOWENCE_ID).toString().isEmpty()){
				 		JSONArray resultJSONArray = new JSONArray();
				 		resultJSONArray.put(inputJSON.get(CommonConstants.ALLOWENCE_ID));
				 		String rec = (String) resultJSONArray.get(0).toString();
				 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
				 		for (int i = 0; i < resultJSONArray1.size(); i++) {
				 			ESIGroupEarningsDO esiGroupEarningDO = new ESIGroupEarningsDO();
					 		if(inputJSON.get(CommonConstants.ESIGROUPID) != null && !inputJSON.get(CommonConstants.ESIGROUPID).toString().isEmpty()){
					 			List<ESIGroupDO> esiGroupList = esiGroupService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ESIGROUPID).toString()));
						 		if(esiGroupList != null && esiGroupList.size() >0){
						 			esiGroupEarningDO.setEsiGroup(esiGroupList.get(0));
						 		}
					 		}
					 		
					 		if(resultJSONArray1.get(i) != null ){
						 		List<AllowanceSettingDO> allowanceSettingList = allowanceSettingsService.retrieveById(Long.parseLong(resultJSONArray1.get(i).toString()));
					 			if(allowanceSettingList != null && allowanceSettingList.size() > 0){
					 				esiGroupEarningDO.setAllowanceSettings(allowanceSettingList.get(0));
					 			}
					 		}
					 		esiGroupEarningDO.setStatus(CommonConstants.ACTIVE);
					 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			esiGroupEarningDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			esiGroupEarningDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 		}
					 		esiGroupEarningDO.setUpdatedon(new Date());
					 		esiGroupEarningDO.setCreatedon(new Date());
					 		
					 		if(!esiGroupEarningService.persist(esiGroupEarningDO)){
					 			//return CommonWebUtil.buildErrorResponse("Payroll Group Already Added").toString();
							}
				 		}
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.DEACTIVATEDEARNINGMENU) != null && !inputJSON.get(CommonConstants.DEACTIVATEDEARNINGMENU).toString().isEmpty()){
			 			JSONArray resultJSONArray = new JSONArray();
				 		resultJSONArray.put(inputJSON.get(CommonConstants.DEACTIVATEDEARNINGMENU));
				 		String rec = (String) resultJSONArray.get(0).toString();
				 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
				 		List<ESIGroupEarningsDO> esiGroupDOlist = new ArrayList<ESIGroupEarningsDO>();
				 		for (int i = 0; i < resultJSONArray1.size(); i++) {
				 			ESIGroupEarningsDO esiGrpDO = new ESIGroupEarningsDO();
				 			List<ESIGroupEarningsDO> esiGroupEarningList = esiGroupEarningService.retrieveByID(Long.parseLong(resultJSONArray1.get(i).toString()));
				 			if(esiGroupEarningList != null && esiGroupEarningList.size() > 0){
				 				esiGrpDO = esiGroupEarningList.get(0);
				 				esiGrpDO.setStatus(CommonConstants.INACTIVE);
				 				esiGroupDOlist.add(esiGrpDO);
				 			}
				 		}
				 		esiGroupEarningService.updateList(esiGroupDOlist);
			 		}
			 	}
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "ESI Group Settings Updated");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
			 		
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveByGroupId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<ESIGroupDO> esiGroupList = esiGroupService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = ESIGroupUtil.getESIGroupListDetails(esiGroupList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
}
