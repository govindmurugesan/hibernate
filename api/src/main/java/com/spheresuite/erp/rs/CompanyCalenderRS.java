package com.spheresuite.erp.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.CompanyCalenderDO;
import com.spheresuite.erp.domainobject.PayrollBatchDO;
import com.spheresuite.erp.domainobject.UnitOrBranchDO;
import com.spheresuite.erp.service.CompanyCalenderService;
import com.spheresuite.erp.service.PayrollBatchService;
import com.spheresuite.erp.service.UnitOrBranchService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.CompanyCalenderUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/companyCalender")
public class CompanyCalenderRS {

	String validation = null;
	static Logger logger = Logger.getLogger(CompanyCalenderRS.class.getName());

	@Autowired
	private CompanyCalenderService companyCalenderService;
	
	@Autowired
	private PayrollBatchService payrollBatchService;
	
	@Autowired
	private UnitOrBranchService unitOrBranchService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				org.json.simple.JSONArray inputJSON = CommonWebUtil.getInputParamsArray(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && inputJSON.size() > 0){
			 		JSONObject rowJSON = CommonWebUtil.getInputParams(inputJSON.get(0).toString());
			 		if(rowJSON.get(CommonConstants.MONTHLY) != null && rowJSON.get(CommonConstants.PAYROLLBATCHID) != null){
				 		boolean deleteData= companyCalenderService.deleteByMonth(rowJSON.get(CommonConstants.MONTHLY).toString(),Long.parseLong(rowJSON.get(CommonConstants.PAYROLLBATCHID).toString()));
				 		List<CompanyCalenderDO> companyCalenderList = new ArrayList<CompanyCalenderDO>(); 
				 		if(deleteData){
				 			for (int i=0; i < inputJSON.size(); i++){
				 				CompanyCalenderDO companyCalenderDO = new CompanyCalenderDO();
				 				JSONObject record = CommonWebUtil.getInputParams(inputJSON.get(i).toString());
				 				if(record.get(CommonConstants.MONTHLY) != null && !record.get(CommonConstants.MONTHLY).toString().isEmpty()){
				 					companyCalenderDO.setMonth(record.get(CommonConstants.MONTHLY).toString());
						 		}
				 				if(record.get(CommonConstants.DATE) != null && !record.get(CommonConstants.DATE).toString().isEmpty()){
				 					companyCalenderDO.setDate(CommonUtil.convertStringToSqlDate(record.get(CommonConstants.DATE).toString()));
						 		}
				 				if(record.get(CommonConstants.DAYS) != null && !record.get(CommonConstants.DAYS).toString().isEmpty()){
				 					companyCalenderDO.setMinimunWorkingDays(record.get(CommonConstants.DAYS).toString());
						 		}
				 				if(record.get(CommonConstants.NO_OF_SALARY_DAYS) != null && !record.get(CommonConstants.NO_OF_SALARY_DAYS).toString().isEmpty()){
				 					companyCalenderDO.setNoOfSalaryDays(record.get(CommonConstants.NO_OF_SALARY_DAYS).toString());
						 		}
				 				if(record.get(CommonConstants.PAYROLLBATCHID) != null && !record.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty()){
					 				List<PayrollBatchDO> batchList = payrollBatchService.retrieveById(Long.parseLong(record.get(CommonConstants.PAYROLLBATCHID).toString()));
							 		if(batchList != null && batchList.size() > 0){
							 			companyCalenderDO.setPayrollBatch(batchList.get(0));
							 		}
				 				}
				 				
				 				if(record.get(CommonConstants.UNIT) != null && !record.get(CommonConstants.UNIT).toString().isEmpty()){
						 			List<UnitOrBranchDO> unitOrBranchDOList = unitOrBranchService.retrieveActiveById(Long.parseLong(record.get(CommonConstants.UNIT).toString()));
						 			if(unitOrBranchDOList != null && unitOrBranchDOList.size() > 0){
						 				companyCalenderDO.setUnitOrBranch(unitOrBranchDOList.get(0));
						 			}
					 			}
				 				
				 				companyCalenderDO.setUpdatedon(new Date());
				 				companyCalenderDO.setCreatedon(new Date());
						 		if(record.get(CommonConstants.UPDATED_BY).toString() != null && !record.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
						 			companyCalenderDO.setUpdatedby(record.get(CommonConstants.UPDATED_BY).toString());
						 			companyCalenderDO.setCreatedby(record.get(CommonConstants.UPDATED_BY).toString());
						 		}
						 		companyCalenderList.add(companyCalenderDO);
				 			}
				 			companyCalenderService.persist(companyCalenderList);
				 	}
			 		CommonUtil.persistRecentAcvtivity((rowJSON.get(CommonConstants.UPDATED_BY).toString()), "Calender Created");
			 	}
			 }
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveByMonth/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByMonth(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && inputJSON.get(CommonConstants.MONTHLY) != null && !inputJSON.get(CommonConstants.MONTHLY).toString().isEmpty() && inputJSON.get(CommonConstants.PAYROLLBATCHID) != null && !inputJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty()){
					List<CompanyCalenderDO> companyCalenderList = companyCalenderService.retrieveByMonth(inputJSON.get(CommonConstants.MONTHLY).toString(),Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString()));
					respJSON = CompanyCalenderUtil.getCalenderList(companyCalenderList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	
	/*
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<DesignationDO> employeeTypeList = designationService.retrieve();
				respJSON = DesignationUtil.getDesignationList(employeeTypeList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				DesignationDO designationDO = new DesignationDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<DesignationDO> designationList = designationService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(designationList != null && designationList.size() > 0){
			 			designationDO = designationList.get(0);
				 		if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
				 			designationDO.setName(inputJSON.get(CommonConstants.NAME).toString());
				 		}
				 		designationDO.setUpdatedon(new Date());
				 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			designationDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
				 			designationDO.setStatus((char) inputJSON.get(CommonConstants.STATUS).toString().charAt(0));
				 		}
				 		if(!designationService.update(designationDO)){
							return CommonWebUtil.buildErrorResponse("Designation Already Added").toString();
						}
				 		CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Designation Updated");
			 		}
			 		}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}*/
}
