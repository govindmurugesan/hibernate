package com.spheresuite.erp.rs;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.EmployeeCtcDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.PayrollBatchDO;
import com.spheresuite.erp.service.EmployeeCtcService;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.PayrollBatchService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.EmployeeCtcUtil;
import com.spheresuite.erp.web.util.EmployeeUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/empctc")
public class EmployeeCtcRS {

	String validation = null;
	static Logger logger = Logger.getLogger(EmployeeCtcRS.class.getName());

	@Autowired
	private EmployeeCtcService employeeCtcService;
	
	@Autowired
	private EmployeeService employeeService;
	
	@Autowired
	private PayrollBatchService payrollBatchService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeCtcDO empCtcDO = new EmployeeCtcDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		List<EmployeeCtcDO> empCtcList = employeeCtcService.retrieveActive(inputJSON.get(CommonConstants.EMPID).toString());
		 			if(empCtcList != null && empCtcList.size() > 0){
		 				return CommonWebUtil.buildErrorResponse("Employee Already Have Active CTC").toString();
		 			}else{
		 				if(inputJSON.get(CommonConstants.EMPID) != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
				 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
					 		if(employeeList != null && employeeList.size() > 0){
					 			empCtcDO.setEmployee(employeeList.get(0));
					 		}
				 		}
		 				if(inputJSON.get(CommonConstants.PAYROLLBATCHID) != null && !inputJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty()){
			 				List<PayrollBatchDO> batchList = payrollBatchService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString()));
					 		if(batchList != null && batchList.size() > 0){
					 			empCtcDO.setPayrollBatch(batchList.get(0));
					 		}
		 				}
		 				if(inputJSON.get(CommonConstants.EMPCTC) != null && !inputJSON.get(CommonConstants.EMPCTC).toString().isEmpty()){
		 					empCtcDO.setEmpctc(Long.parseLong(inputJSON.get(CommonConstants.EMPCTC).toString()));
		 				}
		 				if(inputJSON.get(CommonConstants.CTC_TYPE) != null && !inputJSON.get(CommonConstants.CTC_TYPE).toString().isEmpty()){
		 					empCtcDO.setCtcType(inputJSON.get(CommonConstants.CTC_TYPE).toString());
		 				}
		 				if(inputJSON.get(CommonConstants.STARTDATE) != null && !inputJSON.get(CommonConstants.STARTDATE).toString().isEmpty()){
		 					empCtcDO.setStartdate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.STARTDATE).toString()));
		 				}
		 				
		 				if(inputJSON.get(CommonConstants.ENDDATE) != null && !inputJSON.get(CommonConstants.ENDDATE).toString().isEmpty()){
		 					empCtcDO.setEnddate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.ENDDATE).toString()));
		 				}
				 		
				 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			empCtcDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 			empCtcDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		empCtcDO.setUpdatedon(new Date());
				 		empCtcDO.setCreatedon(new Date());
				 		empCtcDO.setStatus(CommonConstants.ACTIVE);
				 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Componsation Created For Employee");
		 			}
			 	}
				employeeCtcService.persist(empCtcDO);
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retriveByEmpId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveByEmpId(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
				List<EmployeeCtcDO> compensationList = employeeCtcService.retrieveByEmpId(inputJSON.get(CommonConstants.ID).toString());
				respJSON = EmployeeCtcUtil.getempCompensationList(compensationList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retriveActive/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveActive(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
				List<EmployeeCtcDO> compensationList = employeeCtcService.retrieveActive(inputJSON.get(CommonConstants.ID).toString());
				respJSON = EmployeeCtcUtil.getempCompensationList(compensationList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retriveByEmpIdWithEarnings/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveByEmpIdWithEarnings(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<EmployeeCtcDO> compensationList = employeeCtcService.retrieveEmpIdeWithDate(inputJSON.get(CommonConstants.ID).toString(),CommonUtil.convertStringToSqlDate(CommonUtil.convertDateToYearWithOutTime(new Date())));
					respJSON = EmployeeCtcUtil.getempCompensationListWithEarnings(compensationList).toString();
				}else{
					return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
				}
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retriveWithEarnings/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveWithEarnings(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<EmployeeCtcDO> compensationList = employeeCtcService.retrieveActive(inputJSON.get(CommonConstants.ID).toString());
					respJSON = EmployeeCtcUtil.getempCompensationListWithEarnings(compensationList).toString();
				}else{
					return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
				}
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retriveWithEarningsDate/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveWithEarningsDate(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<EmployeeCtcDO> compensationList = employeeCtcService.retrieveActive(inputJSON.get(CommonConstants.ID).toString());
					respJSON = EmployeeCtcUtil.getempCompensationListWithEarningsDateYtd(compensationList, inputJSON.get(CommonConstants.MONTHLY).toString()).toString();
				}else{
					return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
				}
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retriveWithEarningsDateYtd/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveWithEarningsDateYtd(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<EmployeeCtcDO> compensationList = employeeCtcService.retrieveActive(inputJSON.get(CommonConstants.ID).toString());
					respJSON = EmployeeCtcUtil.getempCompensationListWithEarningsDateYtd(compensationList, inputJSON.get(CommonConstants.MONTHLY).toString()).toString();
				}else{
					return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
				}
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<EmployeeCtcDO> empCompensationList = employeeCtcService.retrieve();
				respJSON = EmployeeCtcUtil.getempCompensationList(empCompensationList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	
	@RequestMapping(value = "/retrieveAll", method = RequestMethod.GET)
	public @ResponseBody String retrieveAll(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<EmployeeCtcDO> empCompensationList = employeeCtcService.retrieveAll();
				respJSON = EmployeeCtcUtil.getempCompensationList(empCompensationList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/empwithoutctc", method = RequestMethod.GET)
	public @ResponseBody String empWithOutCtc(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<EmployeeCtcDO> empCompensationList = employeeCtcService.retrieve();
				List<String> empIds = new ArrayList<String>();
				for (EmployeeCtcDO employeeCtcDO : empCompensationList) {
					empIds.add(employeeCtcDO.getEmployee().getEmpId());
				}
				List<EmployeeDO> employeeList = employeeService.retrieveActiveNoCtc(empIds);
				respJSON = EmployeeUtil.getEmployeeListWithOutPhoto(employeeList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeCtcDO empCompensationDO = new EmployeeCtcDO();
				int i =0;
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		if(!inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			List<EmployeeCtcDO> empCompensationList = employeeCtcService.retrieveActive(inputJSON.get(CommonConstants.EMPID).toString());
			 			if(empCompensationList != null && empCompensationList.size() > 0){
			 				EmployeeCtcDO empCompensation = new EmployeeCtcDO();
			 				empCompensation = empCompensationList.get(0);
			 				empCompensation.setStatus('i');
			 				if(empCompensation.getEnddate() == null){
			 					if(!inputJSON.get(CommonConstants.STARTDATE).toString().isEmpty()){
			 						Calendar cal = Calendar.getInstance();
			 						cal.setTime(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.STARTDATE).toString()));
			 						cal.add(Calendar.DATE, -1);
			 						String enddate = CommonUtil.convertDateToYearWithOutTime(empCompensation.getStartdate());
			 						Calendar cal1 = Calendar.getInstance();
			 						cal1.setTime(CommonUtil.convertStringToSqlDate(enddate));
			 						Date dateBefore1Days = cal.getTime();
			 						if(cal1.before(cal)){
			 							empCompensation.setEnddate(dateBefore1Days);
			 						}else{
			 							empCompensation.setEnddate(empCompensation.getStartdate());
			 						}
			 					}else{
			 						i++;
			 					}
			 				}
			 				if(i == 0){
			 					employeeCtcService.update(empCompensation);
			 				}
			 			}
			 		}
			 		if(inputJSON.get(CommonConstants.EMPID) != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
				 		if(employeeList != null && employeeList.size() > 0){
				 			empCompensationDO.setEmployee(employeeList.get(0));
				 		}
			 		}
	 				if(inputJSON.get(CommonConstants.PAYROLLBATCHID) != null && !inputJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty()){
		 				List<PayrollBatchDO> batchList = payrollBatchService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString()));
				 		if(batchList != null && batchList.size() > 0){
				 			empCompensationDO.setPayrollBatch(batchList.get(0));
				 		}
	 				}
	 				if(inputJSON.get(CommonConstants.EMPCTC) != null && !inputJSON.get(CommonConstants.EMPCTC).toString().isEmpty()){
	 					empCompensationDO.setEmpctc(Long.parseLong(inputJSON.get(CommonConstants.EMPCTC).toString()));
	 				}
	 				if(inputJSON.get(CommonConstants.CTC_TYPE) != null && !inputJSON.get(CommonConstants.CTC_TYPE).toString().isEmpty()){
	 					empCompensationDO.setCtcType(inputJSON.get(CommonConstants.CTC_TYPE).toString());
	 				}
	 				if(inputJSON.get(CommonConstants.STARTDATE) != null && !inputJSON.get(CommonConstants.STARTDATE).toString().isEmpty()){
	 					empCompensationDO.setStartdate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.STARTDATE).toString()));
	 				}
	 				if(inputJSON.get(CommonConstants.ENDDATE) != null && !inputJSON.get(CommonConstants.ENDDATE).toString().isEmpty()){
	 					empCompensationDO.setEnddate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.ENDDATE).toString()));
	 				}
			 		empCompensationDO.setUpdatedon(new Date());
			 		empCompensationDO.setCreatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY)!= null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			empCompensationDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			empCompensationDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		empCompensationDO.setStatus(CommonConstants.ACTIVE);
			 	}
			 	if(i == 0){
			 		employeeCtcService.update(empCompensationDO);
			 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Componsation Updated For Employee");
			 	}
				
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/updateDate", method = RequestMethod.POST)
	public @ResponseBody String updateDate(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
				EmployeeCtcDO empCompensation = new EmployeeCtcDO();
			 	if (inputJSON != null){
			 		if(!inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 			List<EmployeeCtcDO> empCompensationList = employeeCtcService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 			if(empCompensationList != null && empCompensationList.size() > 0){
			 				empCompensation = empCompensationList.get(0);
			 				empCompensation.setStartdate(!inputJSON.get(CommonConstants.STARTDATE).toString().isEmpty() ? CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.STARTDATE).toString()) : null);
			 				empCompensation.setEnddate(!inputJSON.get(CommonConstants.ENDDATE).toString().isEmpty() ? CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.ENDDATE).toString()) : null);
			 				if(inputJSON.get(CommonConstants.PAYROLLBATCHID) != null && !inputJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty()){
				 				List<PayrollBatchDO> batchList = payrollBatchService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString()));
						 		if(batchList != null && batchList.size() > 0){
						 			empCompensation.setPayrollBatch(batchList.get(0));
						 		}
			 				}
			 				if(inputJSON.get(CommonConstants.CTC_TYPE) != null && !inputJSON.get(CommonConstants.CTC_TYPE).toString().isEmpty()){
			 					empCompensation.setCtcType(inputJSON.get(CommonConstants.CTC_TYPE).toString());
			 				}
			 				empCompensation.setUpdatedon(new Date());
					 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			empCompensation.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			empCompensation.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 		}
			 			}
			 		}
			 	}
		 		employeeCtcService.update(empCompensation);
		 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Employee Compensation Updated");
				
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	
	@RequestMapping(value = "/importEmpCtc", method = RequestMethod.POST)
	public @ResponseBody String importEmpCtc(Model model, HttpServletRequest request) {	
	try {
		//if (WebManager.authenticateSession(request)) {
			JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
			JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
			List<EmployeeCtcDO> empCompensationDOList = new ArrayList<EmployeeCtcDO>();
			
			Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY);
			for (int i=0; i < fileData.size(); i++){
				JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
				EmployeeCtcDO empCompensationDO = new EmployeeCtcDO();
				if(colName.get(CommonConstants.EMPID) != null){
					/*List<EmployeeDO> employeeList = new ArrayList<EmployeeDO>();
					employeeList = employeeService.retrieveEmpId(rowJSON.get(colName.get(CommonConstants.EMPID)).toString());
					if(employeeList !=null && employeeList.size() > 0){
						empCompensationDO.setEmpId(Long.parseLong(employeeList.get(0).getEmpId()));
					}*/
					
					if(rowJSON.get(colName.get(CommonConstants.EMPID)) != null && !rowJSON.get(colName.get(CommonConstants.EMPID)).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(rowJSON.get(colName.get(CommonConstants.EMPID)).toString());
				 		if(employeeList != null && employeeList.size() > 0){
				 			
				 			if(colName.get(CommonConstants.PAYROLLBATCHNAME) != null){
						 		if(!rowJSON.get(colName.get(CommonConstants.PAYROLLBATCHNAME)).toString().isEmpty()){
						 			List<PayrollBatchDO> payrollBatchDoList = payrollBatchService.retrieveByName(rowJSON.get(colName.get(CommonConstants.PAYROLLBATCHNAME)).toString());
						 			if(payrollBatchDoList != null && payrollBatchDoList.size() > 0 ){
						 				empCompensationDO.setPayrollBatch(payrollBatchDoList.get(0));
						 				empCompensationDO.setEmployee(employeeList.get(0));
						 				if(colName.get(CommonConstants.EMPCTC) != null){
											empCompensationDO.setEmpctc(!rowJSON.get(colName.get(CommonConstants.EMPCTC)).toString().isEmpty() ? Long.parseLong(rowJSON.get(colName.get(CommonConstants.EMPCTC)).toString()) : null);
										}
										if(colName.get(CommonConstants.STARTDATE) != null){
									 		if(!rowJSON.get(colName.get(CommonConstants.STARTDATE)).toString().isEmpty()){
									 			empCompensationDO.setStartdate(CommonUtil.convertStringToSqlDate(rowJSON != null ? rowJSON.get(colName.get(CommonConstants.STARTDATE)).toString() : null));
									 		}
										}
										
										if(colName.get(CommonConstants.ENDDATE) != null){
									 		if(!rowJSON.get(colName.get(CommonConstants.ENDDATE)).toString().isEmpty()){
									 			empCompensationDO.setEnddate(CommonUtil.convertStringToSqlDate(rowJSON != null ? rowJSON.get(colName.get(CommonConstants.ENDDATE)).toString() : null));
									 		}
										}
										
										if(colName.get(CommonConstants.CTC_TYPE) != null){
									 		if(!rowJSON.get(colName.get(CommonConstants.CTC_TYPE)).toString().isEmpty()){
									 			empCompensationDO.setCtcType(rowJSON != null ? rowJSON.get(colName.get(CommonConstants.CTC_TYPE)).toString() : null);
									 		}
										}
										
										if(colName.get(CommonConstants.STATUS) != null){
											if(!rowJSON.get(colName.get(CommonConstants.STATUS)).toString().isEmpty()){
												if(rowJSON.get(colName.get(CommonConstants.STATUS)).toString().equalsIgnoreCase("Active")){
													empCompensationDO.setStatus(CommonConstants.ACTIVE);
												} else{
													empCompensationDO.setStatus(CommonConstants.INACTIVE);
												}
									 		}else{
												empCompensationDO.setStatus(CommonConstants.ACTIVE);
											}
										}
										
								 		if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
								 			empCompensationDO.setUpdatedby(updatedBy.toString());
								 			empCompensationDO.setCreatedby(updatedBy.toString());
								 		}
								 		empCompensationDO.setUpdatedon(new Date());
								 		empCompensationDOList.add(empCompensationDO);
										
						 			}
						 		}
							}
				 			
				 			
				 		}
			 		}
				}
			}
			employeeCtcService.persistList(empCompensationDOList);
	}catch (Exception e) {
		e.printStackTrace();
		return CommonWebUtil.buildErrorResponse("").toString();
	}
	return CommonWebUtil.buildSuccessResponse().toString();
}

}
