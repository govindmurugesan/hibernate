package com.spheresuite.erp.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.CountryDO;
import com.spheresuite.erp.domainobject.StateDO;
import com.spheresuite.erp.service.CountryService;
import com.spheresuite.erp.service.StateService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.StateUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/state")
public class StateRS {

	String validation = null;
	static Logger logger = Logger.getLogger(StateRS.class.getName());
	
	@Autowired
	private StateService stateService;
	
	@Autowired
	private CountryService countryService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				StateDO stateDO = new StateDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.COUNTRY_ID) != null && !inputJSON.get(CommonConstants.COUNTRY_ID).toString().isEmpty()){
			 			List<CountryDO> countryList = countryService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.COUNTRY_ID).toString()));
			 			if(countryList != null && countryList.size() > 0){
			 				stateDO.setCountry(countryList.get(0));
			 				if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			stateDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			stateDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
						 			stateDO.setStateName(inputJSON.get(CommonConstants.NAME).toString());
						 			stateDO.setUpdatedon(new Date());
							 		stateDO.setCreatedon(new Date());
							 		stateDO.setStatus(CommonConstants.ACTIVE);
							 		if(!stateService.persist(stateDO)){
										return CommonWebUtil.buildErrorResponse("State Already Added").toString();
									}
									CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New State Created");
						 		}
					 		}
			 			}
		 			}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
			List<StateDO> stateList = stateService.retrieveActive();
			respJSON = StateUtil.getStateList(stateList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveByCountryId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByCountryId(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<StateDO> stateList = stateService.retrieveByCountryId(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = StateUtil.getStateList(stateList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<StateDO> stateList = stateService.retrieve();
				respJSON = StateUtil.getStateList(stateList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				StateDO stateDO = new StateDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<StateDO> stateList = stateService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(stateList != null && stateList.size() >0){
				 		stateDO = stateList.get(0);
				 		if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
				 			stateDO.setStateName(inputJSON.get(CommonConstants.NAME).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.COUNTRY_ID) != null && !inputJSON.get(CommonConstants.COUNTRY_ID).toString().isEmpty()){
				 			List<CountryDO> countryList = countryService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.COUNTRY_ID).toString()));
				 			if(countryList != null && countryList.size() > 0){
				 				stateDO.setCountry(countryList.get(0));
				 			}
			 			}
				 		stateDO.setUpdatedon(new Date());
				 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			stateDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
				 			stateDO.setStatus((char) inputJSON.get(CommonConstants.STATUS).toString().charAt(0));
				 		}
				 		if(!stateService.update(stateDO)){
							return CommonWebUtil.buildErrorResponse("State Already Added").toString();
						}
				 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "State Updated");
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/importState", method = RequestMethod.POST)
	public @ResponseBody String importState(Model model, HttpServletRequest request) {
		try {
			JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
			JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
			List<StateDO> statelist = new ArrayList<StateDO>();
			Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY);
			for (int i=0; i < fileData.size(); i++){
				JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
				StateDO stateDO = new StateDO();
		 		if(colName.get(CommonConstants.COUNTRY_NAME) != null && !colName.get(CommonConstants.COUNTRY_NAME).toString().isEmpty()){
		 			List<CountryDO> countryList = countryService.retrieveByName(rowJSON.get(colName.get(CommonConstants.COUNTRY_NAME)).toString());
		 			if(countryList != null && countryList.size() > 0){
		 				stateDO.setCountry(countryList.get(0));
		 				if(colName.get(CommonConstants.NAME) != null && !colName.get(CommonConstants.NAME).toString().isEmpty()){
				 			stateDO.setStateName(rowJSON.get(colName.get(CommonConstants.NAME)).toString());
				 		}
		 				if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
				 			stateDO.setUpdatedby(updatedBy.toString());
				 			stateDO.setCreatedby(updatedBy.toString());
				 		}
				 		stateDO.setUpdatedon(new Date());
				 		stateDO.setCreatedon(new Date());
				 		stateDO.setStatus(CommonConstants.ACTIVE);
				 		statelist.add(stateDO);
		 			}
	 			}
			}
			stateService.persistList(statelist);
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
}
