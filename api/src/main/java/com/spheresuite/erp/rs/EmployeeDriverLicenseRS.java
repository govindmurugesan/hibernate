package com.spheresuite.erp.rs;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.CountryDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.EmployeeDriverLicenseDO;
import com.spheresuite.erp.domainobject.StateDO;
import com.spheresuite.erp.service.CountryService;
import com.spheresuite.erp.service.EmployeeDriverLicenseService;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.StateService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.EmployeeDriverLicenseUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/employeedriverlicense")
public class EmployeeDriverLicenseRS {

	String validation = null;
	static Logger logger = Logger.getLogger(EmployeeDriverLicenseRS.class.getName());

	@Autowired
	private EmployeeDriverLicenseService employeeDriverLicenseService;
	
	@Autowired
	private CountryService countryService;
	
	@Autowired
	private StateService stateService;
	
	@Autowired
	private EmployeeService employeeService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeDriverLicenseDO employeeDriverLicensenDO = new EmployeeDriverLicenseDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.COUNTRY_ID) != null && !inputJSON.get(CommonConstants.COUNTRY_ID).toString().isEmpty()){
				 		List<CountryDO> countryList = countryService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.COUNTRY_ID).toString()));
			 			if(countryList != null && countryList.size() > 0){
			 				employeeDriverLicensenDO.setCountry(countryList.get(0));
			 				if(inputJSON.get(CommonConstants.STATE_ID) != null && !inputJSON.get(CommonConstants.STATE_ID).toString().isEmpty()){
						 		List<StateDO> stateList = stateService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.STATE_ID).toString()));
						 		if(stateList != null && stateList.size() >0){
						 			employeeDriverLicensenDO.setState(stateList.get(0));
						 			if(inputJSON.get(CommonConstants.CITY) != null && !inputJSON.get(CommonConstants.CITY).toString().isEmpty()){
							 			employeeDriverLicensenDO.setCity(inputJSON.get(CommonConstants.CITY).toString());
							 			if(inputJSON.get(CommonConstants.EMPID).toString() != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
								 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
									 		if(employeeList != null && employeeList.size() > 0){
									 			employeeDriverLicensenDO.setEmployee(employeeList.get(0));
									 			if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
										 			employeeDriverLicensenDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
										 			employeeDriverLicensenDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
										 			if(inputJSON.get(CommonConstants.LICENSENUMBER) != null && !inputJSON.get(CommonConstants.LICENSENUMBER).toString().isEmpty()){
											 			employeeDriverLicensenDO.setLicenseNumber(inputJSON.get(CommonConstants.LICENSENUMBER).toString());
											 		}
											 		if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
											 			employeeDriverLicensenDO.setName(inputJSON.get(CommonConstants.NAME).toString());
											 		}
											 		if(inputJSON.get(CommonConstants.DATEISSUED) != null && !inputJSON.get(CommonConstants.DATEISSUED).toString().isEmpty()){
											 			employeeDriverLicensenDO.setDateIssued(inputJSON.get(CommonConstants.DATEISSUED).toString());
											 		}
											 		if(inputJSON.get(CommonConstants.EXPIRYDATE) != null && !inputJSON.get(CommonConstants.EXPIRYDATE).toString().isEmpty()){
											 			employeeDriverLicensenDO.setExpiryDate(inputJSON.get(CommonConstants.EXPIRYDATE).toString());
											 		}
											 		if(inputJSON.get(CommonConstants.DATEISSUED) != null && !inputJSON.get(CommonConstants.DATEISSUED).toString().isEmpty()){
											 			employeeDriverLicensenDO.setDateIssued(inputJSON.get(CommonConstants.DATEISSUED).toString());
											 		}
											 		if(inputJSON.get(CommonConstants.ZIP) != null && !inputJSON.get(CommonConstants.ZIP).toString().isEmpty()){
											 			employeeDriverLicensenDO.setZip(Long.parseLong(inputJSON.get(CommonConstants.ZIP).toString()));
											 		}
											 		if(inputJSON.get(CommonConstants.ADDRESS1) != null && !inputJSON.get(CommonConstants.ADDRESS1).toString().isEmpty()){
											 			employeeDriverLicensenDO.setAddress1(inputJSON.get(CommonConstants.ADDRESS1).toString());
											 		}
											 		if(inputJSON.get(CommonConstants.ADDRESS2) != null && !inputJSON.get(CommonConstants.ADDRESS2).toString().isEmpty()){
											 			employeeDriverLicensenDO.setAddress2(inputJSON.get(CommonConstants.ADDRESS2).toString());
											 		}
											 		employeeDriverLicensenDO.setCreatedon(new Date());
											 		employeeDriverLicensenDO.setUpdatedon(new Date());
											 		employeeDriverLicenseService.persist(employeeDriverLicensenDO);
													CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Employee DrivingLicense Created");
									 			}
									 		}
								 		}
						 			}
						 		}
					 		}
			 			}
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retrieveByEmpId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByEmpId(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<EmployeeDriverLicenseDO> employeeAddressList = employeeDriverLicenseService.retrieveByEmpId(inputJSON.get(CommonConstants.ID).toString());
					respJSON = EmployeeDriverLicenseUtil.getEmployeeLicenseList(employeeAddressList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeDriverLicenseDO employeeDriverLicensenDO = new EmployeeDriverLicenseDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<EmployeeDriverLicenseDO> employeeAddressList = employeeDriverLicenseService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		employeeDriverLicensenDO = employeeAddressList.get(0);
			 		if(inputJSON.get(CommonConstants.CITY) != null && !inputJSON.get(CommonConstants.CITY).toString().isEmpty()){
			 			employeeDriverLicensenDO.setCity(inputJSON.get(CommonConstants.CITY).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.COUNTRY_ID) != null && !inputJSON.get(CommonConstants.COUNTRY_ID).toString().isEmpty()){
				 		List<CountryDO> countryList = countryService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.COUNTRY_ID).toString()));
			 			if(countryList != null && countryList.size() > 0){
			 				employeeDriverLicensenDO.setCountry(countryList.get(0));
			 			}
			 		}
			 		if(inputJSON.get(CommonConstants.STATE_ID) != null && !inputJSON.get(CommonConstants.STATE_ID).toString().isEmpty()){
				 		List<StateDO> stateList = stateService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.STATE_ID).toString()));
				 		if(stateList != null && stateList.size() >0){
				 			employeeDriverLicensenDO.setState(stateList.get(0));
				 		}
			 		}
			 		if(inputJSON.get(CommonConstants.EMPID).toString() != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
				 		if(employeeList != null && employeeList.size() > 0){
				 			employeeDriverLicensenDO.setEmployee(employeeList.get(0));
				 		}
			 		}
			 		if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
			 			employeeDriverLicensenDO.setName(inputJSON.get(CommonConstants.NAME).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.DATEISSUED) != null && !inputJSON.get(CommonConstants.DATEISSUED).toString().isEmpty()){
			 			employeeDriverLicensenDO.setDateIssued(inputJSON.get(CommonConstants.DATEISSUED).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.EXPIRYDATE) != null && !inputJSON.get(CommonConstants.EXPIRYDATE).toString().isEmpty()){
			 			employeeDriverLicensenDO.setExpiryDate(inputJSON.get(CommonConstants.EXPIRYDATE).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.DATEISSUED) != null && !inputJSON.get(CommonConstants.DATEISSUED).toString().isEmpty()){
			 			employeeDriverLicensenDO.setDateIssued(inputJSON.get(CommonConstants.DATEISSUED).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.ZIP) != null && !inputJSON.get(CommonConstants.ZIP).toString().isEmpty()){
			 			employeeDriverLicensenDO.setZip(Long.parseLong(inputJSON.get(CommonConstants.ZIP).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.LICENSENUMBER) != null && !inputJSON.get(CommonConstants.LICENSENUMBER).toString().isEmpty()){
			 			employeeDriverLicensenDO.setLicenseNumber(inputJSON.get(CommonConstants.LICENSENUMBER).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.ADDRESS1) != null && !inputJSON.get(CommonConstants.ADDRESS1).toString().isEmpty()){
			 			employeeDriverLicensenDO.setAddress1(inputJSON.get(CommonConstants.ADDRESS1).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.ADDRESS2) != null && !inputJSON.get(CommonConstants.ADDRESS2).toString().isEmpty()){
			 			employeeDriverLicensenDO.setAddress2(inputJSON.get(CommonConstants.ADDRESS2).toString());
			 		}
			 		employeeDriverLicensenDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			employeeDriverLicensenDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		employeeDriverLicenseService.update(employeeDriverLicensenDO);
			 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Employee DrivingLicense Updated");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
