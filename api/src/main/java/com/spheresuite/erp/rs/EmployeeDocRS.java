package com.spheresuite.erp.rs;

import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.EmployeeDocDO;
import com.spheresuite.erp.service.EmployeeDocService;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.EmployeeDocUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/employeedocument")
public class EmployeeDocRS {

	String validation = null;
	static Logger logger = Logger.getLogger(EmployeeDocRS.class.getName());

	@Autowired
	private EmployeeDocService employeeDocService;
	
	@Autowired
	private EmployeeService employeeService;
	
	@RequestMapping(value = "/retrieveById/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveById(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<EmployeeDocDO> employeeDocList = employeeDocService.retrieveByEmpId(inputJSON.get(CommonConstants.ID).toString());
					respJSON = EmployeeDocUtil.getEmployeeDocList(employeeDocList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String updatepic(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeDocDO empDocDO = new EmployeeDocDO();
				/*JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("data").toString());*/
			 	if (request.getParameter("file") != null && request.getParameter("id") != null){
			 		
			 		if(request.getParameter(CommonConstants.ID) != null){
			 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(request.getParameter(CommonConstants.ID).toString());
				 		if(employeeList != null && employeeList.size() > 0){
				 			empDocDO.setEmployee(employeeList.get(0));
				 			if(request.getParameter(CommonConstants.FILE) != null){
					 			empDocDO.setPhoto(request.getParameter("file"));
					 		}
					 		if(request.getParameter(CommonConstants.NAME) != null){
					 			empDocDO.setFileName(request.getParameter(CommonConstants.NAME));
					 		}
					 		employeeDocService.persist(empDocDO);
				 		}
			 		}
 			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
