package com.spheresuite.erp.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.ExpenseTypeDO;
import com.spheresuite.erp.service.ExpenseTypeService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.ExpenseTypeUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/expensetype")
public class ExpenseTypeRS {

	String validation = null;
	static Logger logger = Logger.getLogger(ExpenseTypeRS.class.getName());
	
	@Autowired
	private ExpenseTypeService expenseTypeService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				ExpenseTypeDO expenseTypeDO = new ExpenseTypeDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
			 			expenseTypeDO.setName(inputJSON.get(CommonConstants.NAME).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.DISPLAYNAME) != null && !inputJSON.get(CommonConstants.DISPLAYNAME).toString().isEmpty()){
			 			expenseTypeDO.setDisplayName(inputJSON.get(CommonConstants.DISPLAYNAME).toString());
			 		}
			 		expenseTypeDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			expenseTypeDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			expenseTypeDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		expenseTypeDO.setStatus(CommonConstants.ACTIVESTRING);
			 	}
				if(!expenseTypeService.persist(expenseTypeDO)){
					return CommonWebUtil.buildErrorResponse("Expense Type Already Added").toString();
				}
				CommonUtil.persistRecentAcvtivity(inputJSON.get(CommonConstants.UPDATED_BY).toString(), "New Expense Type Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<ExpenseTypeDO> expenseTypeList = expenseTypeService.retrieveActive();
				respJSON = ExpenseTypeUtil.getExpenseTypeList(expenseTypeList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<ExpenseTypeDO> expenseTypeList = expenseTypeService.retrieve();
				respJSON = ExpenseTypeUtil.getExpenseTypeList(expenseTypeList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				ExpenseTypeDO expenseTypeDO = new ExpenseTypeDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<ExpenseTypeDO> expenseTypeList = expenseTypeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(expenseTypeList != null && expenseTypeList.size() > 0){
			 			expenseTypeDO = expenseTypeList.get(0);
				 		if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
				 			expenseTypeDO.setName(inputJSON.get(CommonConstants.NAME).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.DISPLAYNAME) != null && !inputJSON.get(CommonConstants.DISPLAYNAME).toString().isEmpty()){
				 			expenseTypeDO.setDisplayName(inputJSON.get(CommonConstants.DISPLAYNAME).toString());
				 		}
				 		expenseTypeDO.setUpdatedon(new Date());
				 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			expenseTypeDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
				 			expenseTypeDO.setStatus((inputJSON.get(CommonConstants.STATUS).toString()));
				 		}
				 		if(!expenseTypeService.update(expenseTypeDO)){
							return CommonWebUtil.buildErrorResponse("Expense Type Already Added").toString();
						}
				 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Address Type Updated");
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/importAddressType", method = RequestMethod.POST)
	public @ResponseBody String importAddressType(Model model, HttpServletRequest request) {
		try {
			JSONObject inputJSON =  CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
			JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
			List<ExpenseTypeDO> expenseTypeList = new ArrayList<ExpenseTypeDO>();
			Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY);
			for (int i=0; i < fileData.size(); i++){
				JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
				ExpenseTypeDO expenseTypeDO = new ExpenseTypeDO();
				if (colName.get(CommonConstants.NAME) != null && !colName.get(CommonConstants.NAME).toString().isEmpty()){
					expenseTypeDO.setName(rowJSON.get(colName.get(CommonConstants.NAME)).toString());
		 		}
				if (colName.get(CommonConstants.DISPLAYNAME) != null && !colName.get(CommonConstants.DISPLAYNAME).toString().isEmpty()){
					expenseTypeDO.setDisplayName(rowJSON.get(colName.get(CommonConstants.DISPLAYNAME)).toString());
		 		}
		 		if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
		 			expenseTypeDO.setUpdatedby(updatedBy.toString());
		 			expenseTypeDO.setCreatedby(updatedBy.toString());
		 		}
		 		expenseTypeDO.setUpdatedon(new Date());
		 		expenseTypeDO.setCreatedon(new Date());
		 		expenseTypeDO.setStatus(CommonConstants.ACTIVESTRING);
		 		expenseTypeList.add(expenseTypeDO);
			}
			expenseTypeService.persistList(expenseTypeList);
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
