package com.spheresuite.erp.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.LeaveManagementDO;
import com.spheresuite.erp.service.LeaveManagementService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.LeaveManagementUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/leavemanagement")
public class LeaveManagementRS {

	String validation = null;
	static Logger logger = Logger.getLogger(LeaveManagementRS.class.getName());

	@Autowired
	private LeaveManagementService leaveManagementService;
	
	/*@Autowired
	private LeaveTypeService leaveTypeService;*/
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		/*
			 		List<LeaveManagementDO> leaveRecord = leaveManagementService.retrieveByType(Long.parseLong(inputJSON.get(CommonConstants.TYPE).toString()));
			 		if(leaveRecord != null && leaveRecord.size() > 0){
			 			LeaveManagementDO leaveMangamentDOToUpdate = new LeaveManagementDO();
			 			leaveMangamentDOToUpdate = leaveRecord.get(0);
			 			leaveMangamentDOToUpdate.setStatus(CommonConstants.INACTIVE);
			 			leaveManagementService.update(leaveMangamentDOToUpdate);
			 		}*/
			 		LeaveManagementDO leaveManagementDO = new LeaveManagementDO(); 
			 		
			 		/*if(inputJSON.get(CommonConstants.TYPE) != null && !inputJSON.get(CommonConstants.TYPE).toString().isEmpty()){
				 		List<LeaveTypeDO> leaveTypeList = leaveTypeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.TYPE).toString()));
				 		if(leaveTypeList != null && leaveTypeList.size() > 0){
				 			leaveManagementDO.setLeaveType(leaveTypeList.get(0));
				 		}
			 		}*/
			 		if(inputJSON.get(CommonConstants.TYPE) != null && !inputJSON.get(CommonConstants.TYPE).toString().isEmpty()){
		 				leaveManagementDO.setLeaveType(inputJSON.get(CommonConstants.TYPE).toString());
		 			}
			 		if(inputJSON.get(CommonConstants.DAYS) != null && !inputJSON.get(CommonConstants.DAYS).toString().isEmpty()){
			 			leaveManagementDO.setNumberOfDays(Double.parseDouble(inputJSON.get(CommonConstants.DAYS).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.DAYS_ALLOWED) != null && !inputJSON.get(CommonConstants.DAYS_ALLOWED).toString().isEmpty()){
			 			leaveManagementDO.setDaysAllowed(Double.parseDouble(inputJSON.get(CommonConstants.DAYS_ALLOWED).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.FROMDATE) != null){
		 				leaveManagementDO.setFromDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.FROMDATE).toString()));
		 			}
		 			if(inputJSON.get(CommonConstants.TODATE) != null){
		 				leaveManagementDO.setToDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.TODATE).toString()));
		 			}
		 			if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
		 				leaveManagementDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
		 				leaveManagementDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
		 			if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
		 				leaveManagementDO.setStatus((char)  inputJSON.get(CommonConstants.STATUS).toString().charAt(0));
		 			}
		 			if(inputJSON.get(CommonConstants.CARRYFORWORD) != null && !inputJSON.get(CommonConstants.CARRYFORWORD).toString().isEmpty()){
		 				leaveManagementDO.setCarryForward(inputJSON.get(CommonConstants.CARRYFORWORD).toString());
		 			}
		 			if(inputJSON.get(CommonConstants.COMPOFF) != null && !inputJSON.get(CommonConstants.COMPOFF).toString().isEmpty()){
		 				leaveManagementDO.setCompoff(inputJSON.get(CommonConstants.COMPOFF).toString());
		 			}
		 			if(inputJSON.get(CommonConstants.MULTIPLYBY) != null && !inputJSON.get(CommonConstants.MULTIPLYBY).toString().isEmpty()){
		 				leaveManagementDO.setMultiplyby(Double.parseDouble(inputJSON.get(CommonConstants.MULTIPLYBY).toString()));
		 			}
		 			if(inputJSON.get(CommonConstants.DIVIDEDBY) != null && !inputJSON.get(CommonConstants.DIVIDEDBY).toString().isEmpty()){
		 				leaveManagementDO.setDividedBy(Double.parseDouble(inputJSON.get(CommonConstants.DIVIDEDBY).toString()));
		 			}
		 			if(inputJSON.get(CommonConstants.EARNEDLEAVE) != null && !inputJSON.get(CommonConstants.EARNEDLEAVE).toString().isEmpty()){
		 				leaveManagementDO.setEarnedLeave(inputJSON.get(CommonConstants.EARNEDLEAVE).toString());
		 			}
		 			
		 			if(inputJSON.get(CommonConstants.PERMISSION) != null && !inputJSON.get(CommonConstants.PERMISSION).toString().isEmpty()){
		 				leaveManagementDO.setPermission(inputJSON.get(CommonConstants.PERMISSION).toString());
		 			}
		 			if(inputJSON.get(CommonConstants.SATURDAY) != null  && !inputJSON.get(CommonConstants.SATURDAY).toString().isEmpty()){
		 				leaveManagementDO.setSaturday(inputJSON.get(CommonConstants.SATURDAY).toString());
		 			}
		 			if(inputJSON.get(CommonConstants.SUNDAY) != null  && !inputJSON.get(CommonConstants.SUNDAY).toString().isEmpty()){
		 				leaveManagementDO.setSunday(inputJSON.get(CommonConstants.SUNDAY).toString());
		 			}
		 			leaveManagementDO.setUpdatedon(new Date());
			 		leaveManagementDO.setCreatedon(new Date());
			 		if(!leaveManagementService.persist(leaveManagementDO)){
			 			return CommonWebUtil.buildErrorResponse("Leaves Already Added For This Type").toString();
			 		}
		 			
		 			CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Leave Management Created");
			 	}
			}else{
		 		return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
		 	}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request)  {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
					List<LeaveManagementDO> leaveList = leaveManagementService.retrieve();
					respJSON = LeaveManagementUtil.getLeaveList(leaveList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && inputJSON.get(CommonConstants.ID) != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		LeaveManagementDO leaveMangamentDO = new LeaveManagementDO(); 
		 				List<LeaveManagementDO> leaveList = leaveManagementService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
		 				if(leaveList != null && leaveList.size() > 0){
		 					leaveMangamentDO = leaveList.get(0);
		 					/*if(inputJSON.get(CommonConstants.TYPE) != null && !inputJSON.get(CommonConstants.TYPE).toString().isEmpty()){
						 		List<LeaveTypeDO> leaveTypeList = leaveTypeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.TYPE).toString()));
						 		if(leaveTypeList != null && leaveTypeList.size() > 0){
						 			leaveMangamentDO.setLeaveType(leaveTypeList.get(0));
						 		}
					 		}*/
		 					if(inputJSON.get(CommonConstants.TYPE) != null && !inputJSON.get(CommonConstants.TYPE).toString().isEmpty()){
		 						leaveMangamentDO.setLeaveType(inputJSON.get(CommonConstants.TYPE).toString());
				 			}
			 				if(inputJSON.get(CommonConstants.DAYS) != null){
			 					leaveMangamentDO.setNumberOfDays((Double.parseDouble(inputJSON.get(CommonConstants.DAYS).toString())));
					 		}
			 				if(inputJSON.get(CommonConstants.DAYS_ALLOWED) != null && !inputJSON.get(CommonConstants.DAYS_ALLOWED).toString().isEmpty()){
			 					leaveMangamentDO.setDaysAllowed(Double.parseDouble(inputJSON.get(CommonConstants.DAYS_ALLOWED).toString()));
					 		}
				 			if(inputJSON.get(CommonConstants.FROMDATE) != null){
				 				leaveMangamentDO.setFromDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.FROMDATE).toString()));
				 			}
				 			if(inputJSON.get(CommonConstants.TODATE) != null){
				 				leaveMangamentDO.setToDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.TODATE).toString()));
				 			}
				 			if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
				 				leaveMangamentDO.setStatus((char) inputJSON.get(CommonConstants.STATUS).toString().charAt(0));
					 		}
				 			if(inputJSON.get(CommonConstants.CARRYFORWORD) != null && !inputJSON.get(CommonConstants.CARRYFORWORD).toString().isEmpty()){
				 				leaveMangamentDO.setCarryForward(inputJSON.get(CommonConstants.CARRYFORWORD).toString());
				 			}
			 				if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 					leaveMangamentDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 		}
			 				if(inputJSON.get(CommonConstants.COMPOFF) != null && !inputJSON.get(CommonConstants.COMPOFF).toString().isEmpty()){
			 					leaveMangamentDO.setCompoff(inputJSON.get(CommonConstants.COMPOFF).toString());
				 			}
			 				if(inputJSON.get(CommonConstants.MULTIPLYBY) != null && !inputJSON.get(CommonConstants.MULTIPLYBY).toString().isEmpty()){
			 					leaveMangamentDO.setMultiplyby(Double.parseDouble(inputJSON.get(CommonConstants.MULTIPLYBY).toString()));
				 			}
				 			if(inputJSON.get(CommonConstants.DIVIDEDBY) != null && !inputJSON.get(CommonConstants.DIVIDEDBY).toString().isEmpty()){
				 				leaveMangamentDO.setDividedBy(Double.parseDouble(inputJSON.get(CommonConstants.DIVIDEDBY).toString()));
				 			}
				 			if(inputJSON.get(CommonConstants.EARNEDLEAVE) != null && !inputJSON.get(CommonConstants.EARNEDLEAVE).toString().isEmpty()){
				 				leaveMangamentDO.setEarnedLeave(inputJSON.get(CommonConstants.EARNEDLEAVE).toString());
				 			}
			 				if(inputJSON.get(CommonConstants.PERMISSION) != null && !inputJSON.get(CommonConstants.PERMISSION).toString().isEmpty()){
			 					leaveMangamentDO.setPermission(inputJSON.get(CommonConstants.PERMISSION).toString());
				 			}
			 				if(inputJSON.get(CommonConstants.SATURDAY) != null  && !inputJSON.get(CommonConstants.SATURDAY).toString().isEmpty()){
			 					leaveMangamentDO.setSaturday(inputJSON.get(CommonConstants.SATURDAY).toString());
				 			}
				 			if(inputJSON.get(CommonConstants.SUNDAY) != null  && !inputJSON.get(CommonConstants.SUNDAY).toString().isEmpty()){
				 				leaveMangamentDO.setSunday(inputJSON.get(CommonConstants.SUNDAY).toString());
				 			}
			 				leaveMangamentDO.setUpdatedon(new Date());
			 				leaveManagementService.update(leaveMangamentDO);
			 				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Leave Management Updated");
		 				}
			 		}
			 	}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveActive/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(@PathVariable String inputParams, Model model, HttpServletRequest request)  {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<LeaveManagementDO> leaveDetail = leaveManagementService.retriveActiveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = LeaveManagementUtil.getLeaveList(leaveDetail).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	/*@RequestMapping(value = "/retrieveByType/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByType(@PathVariable String inputParams, Model model, HttpServletRequest request)  {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<LeaveManagementDO> leaveRequestsList = leaveManagementService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = LeaveManagementUtil.getLeaveList(leaveRequestsList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}*/
	
	@RequestMapping(value = "/retrieveAllActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveAllActive(Model model, HttpServletRequest request)  {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<LeaveManagementDO> leaveList = leaveManagementService.retrieveAllActive();
				respJSON = LeaveManagementUtil.getLeaveList(leaveList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/importLeaveManagement", method = RequestMethod.POST)
	public @ResponseBody String importLeaveManagement(Model model, HttpServletRequest request) {
		try {
			JSONObject inputJSON =  CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
			JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
			List<LeaveManagementDO> leaveManagementList = new ArrayList<LeaveManagementDO>();
			Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY);
			for (int i=0; i < fileData.size(); i++){
				JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
				LeaveManagementDO leaveManagementDO = new LeaveManagementDO();
				if(colName.get(CommonConstants.TYPE) != null && !colName.get(CommonConstants.TYPE).toString().isEmpty()){
			 		/*List<LeaveTypeDO> leaveTypeList = leaveTypeService.retrieveByTypeName(rowJSON.get(colName.get(CommonConstants.TYPE)).toString());
			 		if(leaveTypeList != null && leaveTypeList.size() > 0){*/
			 			
			 			//leaveManagementDO.setLeaveType(leaveTypeList.get(0));
			 			if(colName.get(CommonConstants.TYPE) != null && !colName.get(CommonConstants.TYPE).toString().isEmpty()){
			 				leaveManagementDO.setLeaveType(rowJSON.get(colName.get(CommonConstants.TYPE)).toString());
			 			}
			 			
			 			if(colName.get(CommonConstants.DAYS) != null && !colName.get(CommonConstants.DAYS).toString().isEmpty()){
				 			leaveManagementDO.setNumberOfDays(Double.parseDouble(rowJSON.get(colName.get(CommonConstants.DAYS)).toString()));
				 		}
				 		if(colName.get(CommonConstants.DAYS_ALLOWED) != null && !colName.get(CommonConstants.DAYS_ALLOWED).toString().isEmpty()){
				 			leaveManagementDO.setDaysAllowed(Double.parseDouble(rowJSON.get(colName.get(CommonConstants.DAYS_ALLOWED)).toString()));
				 		}
				 		if(colName.get(CommonConstants.FROMDATE) != null){
			 				leaveManagementDO.setFromDate(CommonUtil.convertStringToSqlDate(rowJSON.get(colName.get(CommonConstants.FROMDATE)).toString()));
			 			}
			 			if(colName.get(CommonConstants.TODATE) != null){
			 				leaveManagementDO.setToDate(CommonUtil.convertStringToSqlDate(rowJSON.get(colName.get(CommonConstants.TODATE)).toString()));
			 			}
			 			if(colName.get(CommonConstants.CARRYFORWORD) != null && !colName.get(CommonConstants.CARRYFORWORD).toString().isEmpty()){
			 				leaveManagementDO.setCarryForward(rowJSON.get(colName.get(CommonConstants.CARRYFORWORD)).toString());
			 			}
			 			if(colName.get(CommonConstants.COMPOFF) != null && !colName.get(CommonConstants.COMPOFF).toString().isEmpty()){
			 				leaveManagementDO.setCompoff(rowJSON.get(colName.get(CommonConstants.COMPOFF)).toString());
			 			}
			 			
			 			if(colName.get(CommonConstants.MULTIPLYBY) != null && !colName.get(CommonConstants.MULTIPLYBY).toString().isEmpty()){
			 				leaveManagementDO.setMultiplyby(Double.parseDouble(rowJSON.get(colName.get(CommonConstants.MULTIPLYBY)).toString()));
			 			}
			 			if(colName.get(CommonConstants.DIVIDEDBY) != null && !colName.get(CommonConstants.DIVIDEDBY).toString().isEmpty()){
			 				leaveManagementDO.setDividedBy(Double.parseDouble(rowJSON.get(colName.get(CommonConstants.DIVIDEDBY)).toString()));
			 			}
			 			if(colName.get(CommonConstants.EARNEDLEAVE) != null && !colName.get(CommonConstants.EARNEDLEAVE).toString().isEmpty()){
			 				leaveManagementDO.setEarnedLeave(rowJSON.get(colName.get(CommonConstants.EARNEDLEAVE)).toString());
			 			}
			 			
			 			if(colName.get(CommonConstants.PERMISSION) != null && !colName.get(CommonConstants.PERMISSION).toString().isEmpty()){
			 				leaveManagementDO.setPermission(rowJSON.get(colName.get(CommonConstants.PERMISSION)).toString());
			 			}
			 			if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
			 				leaveManagementDO.setUpdatedby(updatedBy.toString());
			 				leaveManagementDO.setCreatedby(updatedBy.toString());
				 		}
			 			leaveManagementDO.setUpdatedon(new Date());
			 			leaveManagementDO.setCreatedon(new Date());
			 			leaveManagementDO.setStatus(CommonConstants.ACTIVE);
			 			leaveManagementList.add(leaveManagementDO);
			 		//}
		 		}
			}
			leaveManagementService.persistList(leaveManagementList);
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
