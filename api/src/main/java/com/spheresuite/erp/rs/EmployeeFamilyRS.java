package com.spheresuite.erp.rs;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.EmployeeFamilyDO;
import com.spheresuite.erp.service.EmployeeFamilyService;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.EmployeeFamilyUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/employeefamily")
public class EmployeeFamilyRS {

	String validation = null;
	static Logger logger = Logger.getLogger(EmployeeFamilyRS.class.getName());

	@Autowired
	private EmployeeFamilyService employeeFamilyService;
	
	@Autowired
	private EmployeeService employeeService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeFamilyDO empFamilyDo = new EmployeeFamilyDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.EMPID).toString() != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
				 		if(employeeList != null && employeeList.size() > 0){
				 			empFamilyDo.setEmployee(employeeList.get(0));
				 			if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			empFamilyDo.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			empFamilyDo.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			if(inputJSON.get(CommonConstants.FATHERNAME) != null && !inputJSON.get(CommonConstants.FATHERNAME).toString().isEmpty()){
						 			empFamilyDo.setFathername(inputJSON.get(CommonConstants.FATHERNAME).toString());
						 		}
						 		if(inputJSON.get(CommonConstants.MOTHERNAME) != null && !inputJSON.get(CommonConstants.MOTHERNAME).toString().isEmpty()){
						 			empFamilyDo.setMothername(!inputJSON.get(CommonConstants.MOTHERNAME).toString().isEmpty() ? inputJSON.get(CommonConstants.MOTHERNAME).toString() : null);
						 		}
						 		empFamilyDo.setCreatedon(new Date());
						 		empFamilyDo.setUpdatedon(new Date());
						 		employeeFamilyService.persist(empFamilyDo);
								CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Employee Family Information Created");
					 		}
				 		}
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveByEmpId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByEmpId(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<EmployeeFamilyDO> cagtegoryList = employeeFamilyService.retrieveByEmpId(inputJSON.get(CommonConstants.ID).toString());
					respJSON = EmployeeFamilyUtil.getEmployeeFamilyList(cagtegoryList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeFamilyDO empFamilyDo = new EmployeeFamilyDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<EmployeeFamilyDO> employeeFamilyList = employeeFamilyService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		empFamilyDo = employeeFamilyList.get(0);
			 		if(inputJSON.get(CommonConstants.FATHERNAME) != null && !inputJSON.get(CommonConstants.FATHERNAME).toString().isEmpty()){
			 			empFamilyDo.setFathername(inputJSON.get(CommonConstants.FATHERNAME).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.MOTHERNAME) != null && !inputJSON.get(CommonConstants.MOTHERNAME).toString().isEmpty()){
			 			empFamilyDo.setMothername(!inputJSON.get(CommonConstants.MOTHERNAME).toString().isEmpty() ? inputJSON.get(CommonConstants.MOTHERNAME).toString() : null);
			 		}
			 		if(inputJSON.get(CommonConstants.EMPID).toString() != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
				 		if(employeeList != null && employeeList.size() > 0){
				 			empFamilyDo.setEmployee(employeeList.get(0));
				 		}
			 		}
			 		empFamilyDo.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			empFamilyDo.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		employeeFamilyService.update(empFamilyDo);
			 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Employee Family Information Updated");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
