package com.spheresuite.erp.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.CountryDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.StateDO;
import com.spheresuite.erp.domainobject.UnitOrBranchDO;
import com.spheresuite.erp.service.CountryService;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.StateService;
import com.spheresuite.erp.service.UnitOrBranchService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.UnitOrBranchUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/unitorbranch")
public class UnitOrBranchRS {

	String validation = null;
	static Logger logger = Logger.getLogger(UnitOrBranchRS.class.getName());
	
	@Autowired
	private UnitOrBranchService unitOrBranchService;
	
	@Autowired
	private CountryService countryService;
	
	@Autowired
	private StateService stateService;
	
	@Autowired
	private EmployeeService employeeService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				UnitOrBranchDO unitOrBranchDO = new UnitOrBranchDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.COUNTRY_ID) != null && !inputJSON.get(CommonConstants.COUNTRY_ID).toString().isEmpty()){
			 			List<CountryDO> countryList = countryService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.COUNTRY_ID).toString()));
			 			if(countryList != null && countryList.size() > 0){
			 				unitOrBranchDO.setCountry(countryList.get(0));
			 				if(inputJSON.get(CommonConstants.STATE_ID) != null && !inputJSON.get(CommonConstants.STATE_ID).toString().isEmpty()){
					 			List<StateDO> stateList = stateService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.STATE_ID).toString()));
					 			if(stateList != null && stateList.size() > 0){
					 				unitOrBranchDO.setState(stateList.get(0));
					 				if(inputJSON.get(CommonConstants.CITY) != null && !inputJSON.get(CommonConstants.CITY).toString().isEmpty()){
							 			unitOrBranchDO.setUnitOrBranch(inputJSON.get(CommonConstants.CITY).toString());
							 			if(inputJSON.get(CommonConstants.ADDRESS) != null && !inputJSON.get(CommonConstants.ADDRESS).toString().isEmpty()){
								 			unitOrBranchDO.setAddress(inputJSON.get(CommonConstants.ADDRESS).toString());
								 			if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
									 			unitOrBranchDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
									 			unitOrBranchDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
									 			if(inputJSON.get(CommonConstants.MANAGERID) != null && !inputJSON.get(CommonConstants.MANAGERID).toString().isEmpty()){
										 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.MANAGERID).toString());
											 		if(employeeList != null && employeeList.size() > 0){
											 			unitOrBranchDO.setManagerId(employeeList.get(0));
											 		}
										 		}
									 			unitOrBranchDO.setUpdatedon(new Date());
										 		unitOrBranchDO.setCreatedon(new Date());
										 		unitOrBranchDO.setStatus(CommonConstants.ACTIVE);
										 		if(!unitOrBranchService.persist(unitOrBranchDO)){
													return CommonWebUtil.buildErrorResponse("Unit/Branch Already Added").toString();
												}
												CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Unit Or Branch Created");
									 		}
								 		}
							 		}
					 			}
				 			}
			 			}
		 			}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<UnitOrBranchDO> unitOrBranchList = unitOrBranchService.retrieveActive();
				respJSON = UnitOrBranchUtil.getUnitOrBranchList(unitOrBranchList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<UnitOrBranchDO> unitOrBranchList = unitOrBranchService.retrieve();
				respJSON = UnitOrBranchUtil.getUnitOrBranchList(unitOrBranchList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				UnitOrBranchDO unitOrBranchDO = new UnitOrBranchDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<UnitOrBranchDO> unitOrBranchDOList = unitOrBranchService.retrieveActiveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		unitOrBranchDO = unitOrBranchDOList.get(0);
			 		if(inputJSON.get(CommonConstants.ADDRESS) != null && !inputJSON.get(CommonConstants.ADDRESS).toString().isEmpty()){
			 			unitOrBranchDO.setAddress(inputJSON.get(CommonConstants.ADDRESS).toString());
			 		}
			 		unitOrBranchDO.setUnitOrBranch(!inputJSON.get(CommonConstants.CITY).toString().isEmpty()? inputJSON.get(CommonConstants.CITY).toString() : null);
			 		
			 		if(inputJSON.get(CommonConstants.COUNTRY_ID) != null && !inputJSON.get(CommonConstants.COUNTRY_ID).toString().isEmpty()){
			 			List<CountryDO> countryList = countryService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.COUNTRY_ID).toString()));
			 			if(countryList != null && countryList.size() > 0){
			 				unitOrBranchDO.setCountry(countryList.get(0));
			 			}
		 			}
			 		if(inputJSON.get(CommonConstants.STATE_ID) != null && !inputJSON.get(CommonConstants.STATE_ID).toString().isEmpty()){
			 			List<StateDO> stateList = stateService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.STATE_ID).toString()));
			 			if(stateList != null && stateList.size() > 0){
			 				unitOrBranchDO.setState(stateList.get(0));
			 			}
		 			}
			 		if(inputJSON.get(CommonConstants.MANAGERID) != null && !inputJSON.get(CommonConstants.MANAGERID).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.MANAGERID).toString());
				 		if(employeeList != null && employeeList.size() > 0){
				 			unitOrBranchDO.setManagerId(employeeList.get(0));
				 		}
			 		}
			 		unitOrBranchDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			unitOrBranchDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
			 			unitOrBranchDO.setStatus((char)inputJSON.get(CommonConstants.STATUS).toString().charAt(0));
			 		}
			 		if(!unitOrBranchService.update(unitOrBranchDO)){
						return CommonWebUtil.buildErrorResponse("Unit Or Branch Already Added").toString();
					}
			 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Unit Or Branch Updated");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/importUnitOrBranch", method = RequestMethod.POST)
	public @ResponseBody String importUnitOrBranch(Model model, HttpServletRequest request) {
		try {
			//if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
				org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
				JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
				List<UnitOrBranchDO> UnitOrBranchlist = new ArrayList<UnitOrBranchDO>();
				Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY);
				for (int i=0; i < fileData.size(); i++){
					JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
					UnitOrBranchDO unitOrBranchDO = new UnitOrBranchDO();
					
			 		if(colName.get(CommonConstants.COUNTRY_NAME) != null && !colName.get(CommonConstants.COUNTRY_NAME).toString().isEmpty()){
			 			List<CountryDO> countryList = countryService.retrieveByName(rowJSON.get(colName.get(CommonConstants.COUNTRY_NAME)).toString());
			 			if(countryList != null && countryList.size() > 0){
			 				unitOrBranchDO.setCountry(countryList.get(0));
			 				if(colName.get(CommonConstants.STATE_NAME) != null && !colName.get(CommonConstants.STATE_NAME).toString().isEmpty()){
					 			List<StateDO> stateList = stateService.retrieveByName(rowJSON.get(colName.get(CommonConstants.STATE_NAME)).toString());
					 			if(stateList != null && stateList.size() > 0){
					 				unitOrBranchDO.setState(stateList.get(0));
							 		if(colName.get(CommonConstants.CITY) != null && !colName.get(CommonConstants.CITY).toString().isEmpty()){
							 			unitOrBranchDO.setUnitOrBranch(rowJSON.get(colName.get(CommonConstants.CITY)).toString());
							 		}
							 		if(colName.get(CommonConstants.ADDRESS) != null && !colName.get(CommonConstants.ADDRESS).toString().isEmpty()){
							 			unitOrBranchDO.setAddress(rowJSON.get(colName.get(CommonConstants.ADDRESS)).toString());
							 		}
							 		if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
							 			unitOrBranchDO.setUpdatedby(updatedBy.toString());
							 			unitOrBranchDO.setCreatedby(updatedBy.toString());
							 		}
							 		unitOrBranchDO.setUpdatedon(new Date());
							 		unitOrBranchDO.setCreatedon(new Date());
							 		unitOrBranchDO.setStatus(CommonConstants.ACTIVE);
							 		UnitOrBranchlist.add(unitOrBranchDO);
					 			}
				 			}
			 			}
		 			}
				}
				unitOrBranchService.persistList(UnitOrBranchlist);
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
}
