package com.spheresuite.erp.rs;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.EmployeeOtherInformationDO;
import com.spheresuite.erp.service.EmployeeOtherInformationService;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.EmployeeOtherInformationUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/employeeotherinformation")
public class EmployeeOtherInfoInformationRS {

	String validation = null;
	static Logger logger = Logger.getLogger(EmployeeOtherInfoInformationRS.class.getName());

	@Autowired
	private EmployeeOtherInformationService employeeOtherInformationService;
	
	@Autowired
	private EmployeeService employeeService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeOtherInformationDO employeeOtherInformationDO = new EmployeeOtherInformationDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.EMPID).toString() != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
				 		if(employeeList != null && employeeList.size() > 0){
				 			employeeOtherInformationDO.setEmployee(employeeList.get(0));
				 			if(inputJSON.get(CommonConstants.VOTERID) != null && !inputJSON.get(CommonConstants.VOTERID).toString().isEmpty()){
					 			employeeOtherInformationDO.setVoterId(inputJSON.get(CommonConstants.VOTERID).toString());
					 		}
					 		if(inputJSON.get(CommonConstants.RATIONCARD) != null && !inputJSON.get(CommonConstants.RATIONCARD).toString().isEmpty()){
					 			employeeOtherInformationDO.setRationCard(inputJSON.get(CommonConstants.RATIONCARD).toString());
					 		}
					 		if(inputJSON.get(CommonConstants.MARITALSTATUS) != null && !inputJSON.get(CommonConstants.MARITALSTATUS).toString().isEmpty()){
					 			employeeOtherInformationDO.setMaritalStatus(inputJSON.get(CommonConstants.MARITALSTATUS).toString());
					 		}
					 		if(inputJSON.get(CommonConstants.SPOUSENAME) != null && !inputJSON.get(CommonConstants.SPOUSENAME).toString().isEmpty()){
					 			employeeOtherInformationDO.setSpouseName(inputJSON.get(CommonConstants.SPOUSENAME).toString());
					 		}
					 		if(inputJSON.get(CommonConstants.CHILDREN) != null && !inputJSON.get(CommonConstants.CHILDREN).toString().isEmpty()){
					 			employeeOtherInformationDO.setChildern(Long.parseLong(inputJSON.get(CommonConstants.CHILDREN).toString()));
					 		}
					 		if(inputJSON.get(CommonConstants.BLOODGROUP) != null && !inputJSON.get(CommonConstants.BLOODGROUP).toString().isEmpty()){
					 			employeeOtherInformationDO.setBloodGroup(inputJSON.get(CommonConstants.BLOODGROUP).toString());
					 		}
					 		employeeOtherInformationDO.setCreatedon(new Date());
					 		employeeOtherInformationDO.setUpdatedon(new Date());
					 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			employeeOtherInformationDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			employeeOtherInformationDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			employeeOtherInformationService.persist(employeeOtherInformationDO);
								CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Employee Other Information Created");
					 		}
				 		}
			 		}
			 	}
				
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveByEmpId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByEmpId(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<EmployeeOtherInformationDO> employeeOtherInformationList = employeeOtherInformationService.retrieveByEmpId(inputJSON.get(CommonConstants.ID).toString());
					respJSON = EmployeeOtherInformationUtil.getEmployeeOtherInfoList(employeeOtherInformationList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeOtherInformationDO employeeOtherInformationDO = new EmployeeOtherInformationDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<EmployeeOtherInformationDO> employeeOtherInformationList = employeeOtherInformationService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		employeeOtherInformationDO = employeeOtherInformationList.get(0);
			 		if(inputJSON.get(CommonConstants.VOTERID) != null && !inputJSON.get(CommonConstants.VOTERID).toString().isEmpty()){
			 			employeeOtherInformationDO.setVoterId(inputJSON.get(CommonConstants.VOTERID).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.RATIONCARD) != null && !inputJSON.get(CommonConstants.RATIONCARD).toString().isEmpty()){
			 			employeeOtherInformationDO.setRationCard(inputJSON.get(CommonConstants.RATIONCARD).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.MARITALSTATUS) != null && !inputJSON.get(CommonConstants.MARITALSTATUS).toString().isEmpty()){
			 			employeeOtherInformationDO.setMaritalStatus(inputJSON.get(CommonConstants.MARITALSTATUS).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.SPOUSENAME) != null && !inputJSON.get(CommonConstants.SPOUSENAME).toString().isEmpty()){
			 			employeeOtherInformationDO.setSpouseName(inputJSON.get(CommonConstants.SPOUSENAME).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.CHILDREN) != null && !inputJSON.get(CommonConstants.CHILDREN).toString().isEmpty()){
			 			employeeOtherInformationDO.setChildern(Long.parseLong(inputJSON.get(CommonConstants.CHILDREN).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.EMPID).toString() != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
				 		if(employeeList != null && employeeList.size() > 0){
				 			employeeOtherInformationDO.setEmployee(employeeList.get(0));
				 		}
			 		}
			 		if(inputJSON.get(CommonConstants.BLOODGROUP) != null && !inputJSON.get(CommonConstants.BLOODGROUP).toString().isEmpty()){
			 			employeeOtherInformationDO.setBloodGroup(inputJSON.get(CommonConstants.BLOODGROUP).toString());
			 		}
			 		employeeOtherInformationDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			employeeOtherInformationDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		employeeOtherInformationService.update(employeeOtherInformationDO);
			 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Employee Other Information Updated");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
