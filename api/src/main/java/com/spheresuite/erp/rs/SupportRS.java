package com.spheresuite.erp.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.IssueTypeDO;
import com.spheresuite.erp.domainobject.PriorityLevelDO;
import com.spheresuite.erp.domainobject.RolesDO;
import com.spheresuite.erp.domainobject.StatusDO;
import com.spheresuite.erp.domainobject.SupportDO;
import com.spheresuite.erp.domainobject.SupportDocDO;
import com.spheresuite.erp.domainobject.UserDO;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.IssueTypeService;
import com.spheresuite.erp.service.PriorityLevelService;
import com.spheresuite.erp.service.RolesService;
import com.spheresuite.erp.service.StatusService;
import com.spheresuite.erp.service.SupportDocService;
import com.spheresuite.erp.service.SupportService;
import com.spheresuite.erp.service.UserService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.SupportUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/support")
public class SupportRS {

	String validation = null;
	static Logger logger = Logger.getLogger(SupportRS.class.getName());

	@Autowired
	private SupportService supportService;
	
	@Autowired
	private EmployeeService employeeService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private SupportDocService supportDocService;
	
	@Autowired
	private RolesService rolesService;
	
	@Autowired
	private IssueTypeService issueTypeService;
	
	@Autowired
	private PriorityLevelService priorityLevelService;
	
	@Autowired
	private StatusService statusService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				SupportDO supportDO = new SupportDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){

			 		if(inputJSON.get(CommonConstants.ISSUE_TYPE) != null && !inputJSON.get(CommonConstants.ISSUE_TYPE).toString().isEmpty()){
			 			List<IssueTypeDO> issueTypeList = issueTypeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ISSUE_TYPE).toString()));
			 			if(issueTypeList != null && issueTypeList.size() > 0){
			 				supportDO.setIssueType(issueTypeList.get(0));
			 			}
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.PRIORITY_LEVEL_ID) != null && !inputJSON.get(CommonConstants.PRIORITY_LEVEL_ID).toString().isEmpty()){
			 			List<PriorityLevelDO> priorityList = priorityLevelService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.PRIORITY_LEVEL_ID).toString()));
			 			if(priorityList != null && priorityList.size() > 0){
			 				supportDO.setPriorityLevel(priorityList.get(0));
			 			}
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.STATUS_ID) != null && !inputJSON.get(CommonConstants.STATUS_ID).toString().isEmpty()){
			 			List<StatusDO> statusList = statusService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.STATUS_ID).toString()));
			 			if(statusList != null && statusList.size() > 0){
			 				supportDO.setStatusDetails(statusList.get(0));
			 			}
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.COMPANY) != null && !inputJSON.get(CommonConstants.COMPANY).toString().isEmpty()){
			 			supportDO.setOrganization(inputJSON.get(CommonConstants.COMPANY).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.ISSUE_SUMMARY) != null && !inputJSON.get(CommonConstants.ISSUE_SUMMARY).toString().isEmpty()){
			 			supportDO.setIssueSummary(inputJSON.get(CommonConstants.ISSUE_SUMMARY).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.ISSUE_DETAILS) != null && !inputJSON.get(CommonConstants.ISSUE_DETAILS).toString().isEmpty()){
			 			supportDO.setIssueDetails(inputJSON.get(CommonConstants.ISSUE_DETAILS).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.EMPID) != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retrieveEmpId(inputJSON.get(CommonConstants.EMPID).toString());
			 			if(employeeList != null && employeeList.size() > 0){
			 				supportDO.setEmpId(employeeList.get(0).getEmpId());
			 				if(employeeList.get(0).getCompanyemail() != null){
			 					supportDO.setEmail(employeeList.get(0).getCompanyemail());
			 				}
			 				if(employeeList.get(0).getPrimarycontact() != null){
			 					supportDO.setMobile(employeeList.get(0).getPrimarycontact());
			 				}
			 				if(employeeList.get(0).getPersonalcontact() != null){
			 					supportDO.setPhone(employeeList.get(0).getPersonalcontact());
			 				}
			 				if(employeeList.get(0).getMiddlename() != null) {
			 					supportDO.setEmpName(String.valueOf(employeeList.get(0).getFirstname() + " " +employeeList.get(0).getMiddlename() + " " + employeeList.get(0).getLastname()));
			 				}else {
			 					supportDO.setEmpName(String.valueOf(employeeList.get(0).getFirstname() + " " + employeeList.get(0).getLastname()));
			 				}
			 			}
			 		}
			 		
			 		supportDO.setUpdatedon(new Date());
			 		supportDO.setCreatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			supportDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			supportDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			supportDO.setEmpId(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 	}
				supportService.persist(supportDO);
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Support Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retrieveById/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveById(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<SupportDO> contactList = supportService.retriveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = SupportUtil.getSupportList(contactList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<Long> deptIds = new ArrayList<Long>();
				int i = 0;
				if(request.getSession().getAttribute("deptIds") != null){
					deptIds = (List<Long>) request.getSession().getAttribute("deptIds");
				}
				List<String> empIds = new ArrayList<String>();
				if(deptIds != null && deptIds.size() > 0){
					empIds = employeeService.retrieveByDeptIds(deptIds);
					/*List<String> newList = employeeService.retrieveByDeptIds(deptIds);
					for (String deptId : newList) { 
						empIds.add(String.valueOf(deptId)); 
					}*/
				}else if(request.getSession().getAttribute("empIds") != null){
					empIds = new ArrayList<String>();
					String empid = request.getSession().getAttribute("empIds").toString();
					empIds.add(empid);
					List<UserDO> userList = userService.retriveByEmpId(empid.toString());
					if(userList.get(0).getRole().getRoleId() != null){
						List<RolesDO> roleList = rolesService.retriveById(userList.get(0).getRole().getRoleId());
						if(roleList != null && roleList.size() > 0){
							if(roleList.get(0).getName().equalsIgnoreCase(CommonConstants.SUPERADMIN)){
								i=1;
							}
						}else{
							empIds = new ArrayList<String>();
						}
					}else{
						empIds = new ArrayList<String>();
					}
					//empIds = employeeService.retrieveByDeptIds(deptIds);
				}
				
				if(i==1){
					List<SupportDO> contactList = supportService.retrieveAll();
					respJSON = SupportUtil.getSupportList(contactList).toString();
				}else{
					List<SupportDO> contactList = supportService.retrieve(empIds);
					if( contactList !=null && contactList.size() > 0){
						respJSON = SupportUtil.getSupportList(contactList).toString();
					}
					//List<SupportDO> contactList = supportService.retrieveAll();
					//respJSON = SupportUtil.getSupportList(contactList).toString();
				}
				
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retriveByEmailId/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String retriveByEmailId(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.EMAIL).toString().isEmpty()){
					List<SupportDO> contactList = supportService.retriveByEmailId(inputJSON.get(CommonConstants.EMAIL).toString());
					respJSON = SupportUtil.getSupportList(contactList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				SupportDO supportDO = new SupportDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<SupportDO> contactList = supportService.retriveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		supportDO = contactList.get(0);
			 		if(inputJSON.get(CommonConstants.ISSUE_TYPE) != null && !inputJSON.get(CommonConstants.ISSUE_TYPE).toString().isEmpty()){
			 			List<IssueTypeDO> issueTypeList = issueTypeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ISSUE_TYPE).toString()));
			 			if(issueTypeList != null && issueTypeList.size() > 0){
			 				supportDO.setIssueType(issueTypeList.get(0));
			 			}
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.PRIORITY_LEVEL_ID) != null && !inputJSON.get(CommonConstants.PRIORITY_LEVEL_ID).toString().isEmpty()){
			 			List<PriorityLevelDO> priorityList = priorityLevelService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.PRIORITY_LEVEL_ID).toString()));
			 			if(priorityList != null && priorityList.size() > 0){
			 				supportDO.setPriorityLevel(priorityList.get(0));
			 			}
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.STATUS_ID) != null && !inputJSON.get(CommonConstants.STATUS_ID).toString().isEmpty()){
			 			List<StatusDO> statusList = statusService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.STATUS_ID).toString()));
			 			if(statusList != null && statusList.size() > 0){
			 				supportDO.setStatusDetails(statusList.get(0));
			 			}
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.ISSUE_SUMMARY) != null && !inputJSON.get(CommonConstants.ISSUE_SUMMARY).toString().isEmpty()){
			 			supportDO.setIssueSummary(inputJSON.get(CommonConstants.ISSUE_SUMMARY).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.ISSUE_DETAILS) != null && !inputJSON.get(CommonConstants.ISSUE_DETAILS).toString().isEmpty()){
			 			supportDO.setIssueDetails(inputJSON.get(CommonConstants.ISSUE_DETAILS).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.COMPANY) != null && !inputJSON.get(CommonConstants.COMPANY).toString().isEmpty()){
			 			supportDO.setOrganization(inputJSON.get(CommonConstants.COMPANY).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.EMPID) != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retrieveEmpId(inputJSON.get(CommonConstants.EMPID).toString());
			 			if(employeeList != null && employeeList.size() > 0){
			 				supportDO.setEmpId(employeeList.get(0).getEmpId());
			 				if(employeeList.get(0).getCompanyemail() != null){
			 					supportDO.setEmail(employeeList.get(0).getCompanyemail());
			 				}
			 				if(employeeList.get(0).getPrimarycontact() != null){
			 					supportDO.setMobile(employeeList.get(0).getPrimarycontact());
			 				}
			 				if(employeeList.get(0).getPersonalcontact() != null){
			 					supportDO.setPhone(employeeList.get(0).getPersonalcontact());
			 				}
			 				if(employeeList.get(0).getMiddlename() != null) {
			 					supportDO.setEmpName(String.valueOf(employeeList.get(0).getFirstname() + " " +employeeList.get(0).getMiddlename() + " " + employeeList.get(0).getLastname()));
			 				}else {
			 					supportDO.setEmpName(String.valueOf(employeeList.get(0).getFirstname() + " " + employeeList.get(0).getLastname()));
			 				}
			 				
			 			}
			 			
			 		}
			 		supportDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			supportDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		supportService.update(supportDO);
			 		supportDocService.delete(supportDO.getSupportId());
			 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Support Updated");
			 	}

			 }else{
					return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/updatepic", method = RequestMethod.POST)
	public @ResponseBody String updatepic(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				SupportDocDO supportDocDO = new SupportDocDO();
				/*JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("data").toString());*/
			 	if (request.getParameter("file") != null && request.getParameter("id") != null){
			 		List<SupportDO> supportList = supportService.retriveById(Long.parseLong(request.getParameter("id").toString()));
			 		if(supportList != null && supportList.size() > 0){
				 		supportDocDO.setSupport(supportList.get(0));
				 		supportDocDO.setPhoto(request.getParameter("file"));
				 		supportDocDO.setFileName(request.getParameter("name"));
				 		supportDocDO.setUpdatedon(new Date());
				 		supportDocDO.setCreatedon(new Date());
				 		if(request.getParameter(CommonConstants.UPDATED_BY) != null && !request.getParameter(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			supportDocDO.setUpdatedBy(request.getParameter(CommonConstants.UPDATED_BY).toString());
				 			supportDocDO.setCreatedBy(request.getParameter(CommonConstants.UPDATED_BY).toString());
				 		}
				 		supportDocService.persist(supportDocDO);
			 		}else{
			 			return CommonWebUtil.buildErrorResponse("").toString();
			 		}
 			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
}
