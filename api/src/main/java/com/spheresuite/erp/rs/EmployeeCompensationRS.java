package com.spheresuite.erp.rs;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.auth0.jwt.JWTVerifyException;
import com.spheresuite.erp.domainobject.CompanyCalenderDO;
import com.spheresuite.erp.domainobject.EmployeeCompensationDO;
import com.spheresuite.erp.domainobject.EmployeeCtcDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.PayrollBatchDO;
import com.spheresuite.erp.domainobject.PayrollGroupDO;
import com.spheresuite.erp.service.CompanyCalenderService;
import com.spheresuite.erp.service.EmployeeCompensationService;
import com.spheresuite.erp.service.EmployeeCtcService;
import com.spheresuite.erp.service.EmployeeDeductionService;
import com.spheresuite.erp.service.EmployeeEarningsService;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.EmployeeTaxService;
import com.spheresuite.erp.service.PayrollBatchService;
import com.spheresuite.erp.service.PayrollGroupService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.EmployeeCompensationUtil;
import com.spheresuite.erp.web.util.EmployeeCtcUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/empcompensation")
public class EmployeeCompensationRS {

	String validation = null;
	static Logger logger = Logger.getLogger(EmployeeCompensationRS.class.getName());

	@Autowired
	private EmployeeCompensationService employeeCompensationService;
	
	@Autowired
	private EmployeeDeductionService employeeDeductionService;
	
	@Autowired
	private EmployeeEarningsService employeeEarningsService;
	
	@Autowired
	private EmployeeTaxService employeeTaxService;
	
	@Autowired
	private EmployeeService employeeService;
	
	@Autowired
	private PayrollGroupService payrollGroupService;
	
	@Autowired
	private PayrollBatchService payrollBatchService;
	
	@Autowired
	private EmployeeCtcService employeeCtcService;
	
	@Autowired
	private CompanyCalenderService companyCalenderService;
	
	
	
	
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeCompensationDO empCompensationDO = new EmployeeCompensationDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		List<EmployeeCompensationDO> empCompensationList = employeeCompensationService.retrieveByEmpIdAndStatus(inputJSON.get(CommonConstants.EMPID).toString());
		 			if(empCompensationList != null && empCompensationList.size() > 0){
		 				EmployeeCompensationDO empCompensationUpdateDO = new EmployeeCompensationDO();
		 				empCompensationUpdateDO = empCompensationList.get(0);
		 				empCompensationUpdateDO.setStatus(CommonConstants.INACTIVE);
		 					/*if(!inputJSON.get(CommonConstants.EFFECTIVEFROM).toString().isEmpty()){
		 						Calendar cal = Calendar.getInstance();
		 						cal.setTime(CommonUtil.convertStringToDateMMM(inputJSON.get(CommonConstants.EFFECTIVEFROM).toString()));
		 						cal.add(Calendar.DATE, -1);
		 						String enddate = CommonUtil.convertDateToYearWithOutTime(empCompensationUpdateDO.getEffectivefrom());
		 						Calendar cal1 = Calendar.getInstance();
		 						cal1.setTime(CommonUtil.convertStringToSqlDate(enddate));
		 						Date dateBefore1Days = cal.getTime();
		 						if(cal1.before(cal)){
		 							empCompensationUpdateDO.setEffectiveto(dateBefore1Days);
		 						}else{
		 							empCompensationUpdateDO.setEffectiveto(empCompensationUpdateDO.getEffectivefrom());
		 						}
		 					}*/
			 				employeeCompensationService.update(empCompensationUpdateDO);
		 				}
		 				
		 			if(inputJSON.get(CommonConstants.EMPID).toString() != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
				 		if(employeeList != null && employeeList.size() > 0){
				 			empCompensationDO.setEmployee(employeeList.get(0));
				 		
				 		}
			 		}
		 			if(inputJSON.get(CommonConstants.EMPCTC).toString() != null && !inputJSON.get(CommonConstants.EMPCTC).toString().isEmpty()){
		 				List<EmployeeCtcDO> empCtcDetails = employeeCtcService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.EMPCTC).toString()));
			 			if(empCtcDetails != null && empCtcDetails.size() > 0){
				 			empCompensationDO.setEmpCtc(empCtcDetails.get(0));
			 			}
			 		}
		 			if(inputJSON.get(CommonConstants.PAYROLLGROUPID) != null && !inputJSON.get(CommonConstants.PAYROLLGROUPID).toString().isEmpty()){
			 			List<PayrollGroupDO> payrollGroupList = payrollGroupService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.PAYROLLGROUPID).toString()));
				 		if(payrollGroupList != null && payrollGroupList.size() >0){
				 			empCompensationDO.setPayrollGroup(payrollGroupList.get(0));
				 		}
			 		}
		 			if(inputJSON.get(CommonConstants.EFFECTIVEFROM).toString() != null && !inputJSON.get(CommonConstants.EFFECTIVEFROM).toString().isEmpty()){
		 				empCompensationDO.setEffectivefrom(CommonUtil.convertStringToDateMMM(inputJSON.get(CommonConstants.EFFECTIVEFROM).toString()));
		 			}
		 			if(inputJSON.get(CommonConstants.EFFECTIVETO).toString() != null && !inputJSON.get(CommonConstants.EFFECTIVETO).toString().isEmpty()){
		 				empCompensationDO.setEffectiveto(CommonUtil.convertStringToDateMMM(inputJSON.get(CommonConstants.EFFECTIVETO).toString()));
		 			}	
		 			empCompensationDO.setStatus(CommonConstants.ACTIVE);
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			empCompensationDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			empCompensationDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		empCompensationDO.setUpdatedon(new Date());
			 		empCompensationDO.setCreatedon(new Date());
			 	
			 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Employee Componsation Created");
			 	}
				employeeCompensationService.persist(empCompensationDO);
				
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveAll", method = RequestMethod.GET)
	public @ResponseBody String retrieveAll(Model model, HttpServletRequest request) throws InvalidKeyException, NoSuchAlgorithmException, IllegalStateException, SignatureException, IOException, JWTVerifyException {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<EmployeeCompensationDO> empCompensationList = employeeCompensationService.retrieveActive();
				respJSON = EmployeeCompensationUtil.getempActiveCompensation(empCompensationList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	/*@RequestMapping(value = "/retrieve/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieve(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<EmployeeCompensationDO> empCompensationList = employeeCompensationService.retrieveActive();
					respJSON = EmployeeCompensationUtil.getempActiveCompensation(empCompensationList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}*/
	
	@RequestMapping(value = "/retrieveById/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveById(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<EmployeeCompensationDO> empCompensationList = employeeCompensationService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = EmployeeCompensationUtil.getempCompensationDetails(empCompensationList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	
	@RequestMapping(value = "/retrieveWithComponsesationDateAndPayrollBatch", method = RequestMethod.POST)
	public @ResponseBody String retrieveWithComponsesationDateAndPayrollBatch(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
				int noOfwrkingDays = 0;
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List <PayrollBatchDO> batchList = payrollBatchService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					if(batchList  != null && batchList.size() > 0){
						
						if(batchList.get(0).getPayrollType().equalsIgnoreCase("Monthly")){
							Calendar c = Calendar.getInstance();
							c.setTime(CommonUtil.convertMonthToSqlDate(inputJSON.get(CommonConstants.MONTHLY).toString()));
							c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
							List<EmployeeCtcDO> empCtcList = employeeCtcService.retrieveBatchAndMonth(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()), c.getTime());
				 			//if(empCtcList != null && empCtcList.size() > 0){
				 				respJSON = EmployeeCompensationUtil.getempPayrollDetails(empCtcList, inputJSON.get(CommonConstants.MONTHLY).toString(), c.getTime()).toString();
				 			/*}else{
				 				
				 			}*/
						}else if (batchList.get(0).getPayrollType().equalsIgnoreCase("Weekly")) {
							
						/*}{
							if(batchList.get(0).getPayrollType().equalsIgnoreCase("Weekly")){*/
								
								Calendar calFrom = Calendar.getInstance();
								calFrom.setTime(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.FROMDATE).toString()));
								int fromyear = calFrom.get(Calendar.YEAR);
								String frommonth  = new SimpleDateFormat("MMMM").format(calFrom.getTime());
								frommonth = frommonth.substring(0,3) + " "+ fromyear;
								if(batchList.get(0).getPayrollBatchId() != null){
									List<CompanyCalenderDO> companyCalenderList = companyCalenderService.retrieveByMonth(frommonth,batchList.get(0).getPayrollBatchId());
									if(companyCalenderList != null && companyCalenderList.size() > 0){
										if(companyCalenderList.get(0).getMinimunWorkingDays() != null && !companyCalenderList.get(0).getMinimunWorkingDays().toString().isEmpty()){
											noOfwrkingDays = Integer.parseInt(companyCalenderList.get(0).getMinimunWorkingDays().toString());
										}
									}
								}
								if(noOfwrkingDays > 0){
									/*Calendar c = Calendar.getInstance();
									c.setTime(CommonUtil.convertMonthToSqlDate(inputJSON.get(CommonConstants.MONTHLY).toString()));
									c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));*/
									Date d1 = CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.FROMDATE).toString());
									Date d2 = CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.TODATE).toString());
									int daysdiff = 0;
									Calendar c = Calendar.getInstance();
									c.setTime(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.TODATE).toString()));
									c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
									long diff = d2.getTime() - d1.getTime();
									long diffDays = diff / (24 * 60 * 60 * 1000) + 1;
									daysdiff = (int) diffDays;
									
									int sundays=0;
									Calendar start=Calendar.getInstance();
									start.setTime(d1);
									Calendar end=Calendar.getInstance();
									end.setTime(d2);
									 for (Date date = start.getTime(); start.before(end) || start.equals(end); start.add(Calendar.DATE, 1), date = start.getTime()) {
											Calendar weekend = Calendar.getInstance();
											weekend.setTime(date);
											if (/*weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && */
												weekend.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY){
												sundays++;
											}
										}
									if(sundays >0 && daysdiff > 0){
										daysdiff = daysdiff -sundays;
									}
									List<EmployeeCtcDO> empCtcList = employeeCtcService.retrieveWeeklyBatchWithDate(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()), c.getTime());
									respJSON = EmployeeCompensationUtil.getempWeeklyPayrollDetails(empCtcList, frommonth, c.getTime(),daysdiff,d1,d2).toString();
								}else{
									respJSON = CommonWebUtil.buildErrorResponse("Number Of Working Days Not Available").toString();
								}
							}else{
								if(batchList.get(0).getPayrollType().equalsIgnoreCase("Daily")){
									Calendar calDate = Calendar.getInstance();
									calDate.setTime(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.DATE).toString()));
									System.out.println("dat"+calDate.getTime());
									int fromyear = calDate.get(Calendar.YEAR);
									String frommonth  = new SimpleDateFormat("MMMM").format(calDate.getTime());
									frommonth = frommonth.substring(0,3) + " "+ fromyear;

									if(batchList.get(0).getPayrollBatchId() != null){
										List<CompanyCalenderDO> companyCalenderList = companyCalenderService.retrieveByMonth(frommonth,batchList.get(0).getPayrollBatchId());
										if(companyCalenderList != null && companyCalenderList.size() > 0){
											if(companyCalenderList.get(0).getMinimunWorkingDays() != null && !companyCalenderList.get(0).getMinimunWorkingDays().toString().isEmpty()){
												noOfwrkingDays = Integer.parseInt(companyCalenderList.get(0).getMinimunWorkingDays().toString());
											}
										}
									}
									if(noOfwrkingDays > 0){
										List<EmployeeCtcDO> empCtcList = employeeCtcService.retrieveDailyBatchWithDate(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()), calDate.getTime());
										System.out.println("empCtcList"+empCtcList);
										respJSON = EmployeeCompensationUtil.getEmpDailyPayrollDetails(empCtcList, calDate.getTime(),Long.parseLong(inputJSON.get(CommonConstants.ID).toString()),frommonth).toString();
									}else{
										respJSON = CommonWebUtil.buildErrorResponse("Number Of Working Days Not Available").toString();
									}
									
								}
							}
					}
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	/*@RequestMapping(value = "/retrieveWithComponsesationDateAndPayrollBatch/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveWithComponsesationDateAndPayrollBatch(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty() && !inputJSON.get(CommonConstants.MONTHLY).toString().isEmpty()){
					List<EmployeeCompensationDO> empCompensationList = employeeCompensationService.retrieveWithComponsesationDateAndPayrollBatch(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()), CommonUtil.convertMonthToSqlDate(inputJSON.get(CommonConstants.MONTHLY).toString()));
					respJSON = EmployeeCompensationUtil.getempPayrollDetails(empCompensationList, inputJSON.get(CommonConstants.MONTHLY).toString()).toString();
					//respJSON = EmployeeCompensationUtil.getempCompensationListWithBatch(empCompensationList, Long.parseLong(inputJSON.get(CommonConstants.ID).toString()), inputJSON.get(CommonConstants.MONTHLY).toString()).toString();
				
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}*/
	
	@RequestMapping(value = "/retrieveByEmpId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByEmpId(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<EmployeeCompensationDO> empCompensationList = employeeCompensationService.retrieveByEmpIdAndStatus(inputJSON.get(CommonConstants.ID).toString());
					if(empCompensationList != null && empCompensationList.size() > 0){
						respJSON = EmployeeCompensationUtil.getEmpAvailbleComponsationDeatils(empCompensationList).toString();
					}else{
						List<EmployeeCtcDO> ctcList = employeeCtcService.retrieveActive(inputJSON.get(CommonConstants.ID).toString());
						respJSON = EmployeeCtcUtil.getempCompensationListWithEarnings(ctcList).toString();
					}
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	
/*	@RequestMapping(value = "/retriveByEmpId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveByEmpId(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
				List<EmployeeCompensationDO> compensationList = employeeCompensationService.retrieveByEmpId(inputJSON.get(CommonConstants.ID).toString());
				respJSON = EmployeeCompensationUtil.getempCompensationList(compensationList)c;
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	
	
	@RequestMapping(value = "/retrieveById/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveById(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<EmployeeCompensationDO> empCompensationList = employeeCompensationService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = EmployeeCompensationUtil.getempCompensationListWithEarningList(empCompensationList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveAll", method = RequestMethod.GET)
	public @ResponseBody String retrieveAll(Model model, HttpServletRequest request) throws InvalidKeyException, NoSuchAlgorithmException, IllegalStateException, SignatureException, IOException, JWTVerifyException {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<EmployeeCompensationDO> empCompensationList = employeeCompensationService.retrieve();
				respJSON = EmployeeCompensationUtil.getempCompensationList(empCompensationList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	
	@RequestMapping(value = "/retrieveWithDate/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveWithDate(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty() && !inputJSON.get(CommonConstants.MONTHLY).toString().isEmpty()){
					List<EmployeeCompensationDO> empCompensationList = employeeCompensationService.retrieve();
					respJSON = EmployeeCompensationUtil.getempCompensationListWithBatch(empCompensationList, Long.parseLong(inputJSON.get(CommonConstants.ID).toString()), inputJSON.get(CommonConstants.MONTHLY).toString()).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}*/
	
	/*@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeCompensationDO empCompensationDO = new EmployeeCompensationDO();
				int i =0;
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		if(!inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			List<EmployeeCompensationDO> empCompensationList = employeeCompensationService.retrieveActive(Long.parseLong(inputJSON.get(CommonConstants.EMPID).toString()));
			 			if(empCompensationList != null && empCompensationList.size() > 0){
			 				EmployeeCompensationDO empCompensation = new EmployeeCompensationDO();
			 				empCompensation = empCompensationList.get(0);
			 				empCompensation.setStatus('i');
			 				if(empCompensation.getEnddate() == null){
			 					if(!inputJSON.get(CommonConstants.STARTDATE).toString().isEmpty()){
			 						Calendar cal = Calendar.getInstance();
			 						cal.setTime(CommonUtil.convertStringToSqlDateMonth(inputJSON.get(CommonConstants.STARTDATE).toString()));
			 						cal.add(Calendar.DATE, -1);
			 						String enddate = CommonUtil.convertDateToYearWithOutTime(empCompensation.getStartdate());
			 						Calendar cal1 = Calendar.getInstance();
			 						cal1.setTime(CommonUtil.convertStringToSqlDate(enddate));
			 						Date dateBefore1Days = cal.getTime();
			 						if(cal1.before(cal)){
			 							empCompensation.setEnddate(dateBefore1Days);
			 						}else{
			 							empCompensation.setEnddate(empCompensation.getStartdate());
			 						}
			 					}else{
			 						i++;
			 					}
			 				}
			 				if(i == 0){
			 					employeeCompensationService.update(empCompensation);
			 				}
			 			}
			 		}
			 		
			 		empCompensationDO.setEmpId(!inputJSON.get(CommonConstants.EMPID).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.EMPID).toString()) : null);
			 		empCompensationDO.setEmpctc(!inputJSON.get(CommonConstants.EMPCTC).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.EMPCTC).toString()) : null);
			 		empCompensationDO.setStartdate(!inputJSON.get(CommonConstants.STARTDATE).toString().isEmpty() ? CommonUtil.convertStringToSqlDateMonth(inputJSON.get(CommonConstants.STARTDATE).toString()) : null);
			 		empCompensationDO.setEnddate(!inputJSON.get(CommonConstants.ENDDATE).toString().isEmpty() ? CommonUtil.convertStringToSqlDateMonth(inputJSON.get(CommonConstants.ENDDATE).toString()) : null);
			 		empCompensationDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			empCompensationDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			empCompensationDO.setCreatedby(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
			 		}
			 		empCompensationDO.setStatus('a');
			 	}
			 	if(i == 0){
			 		employeeCompensationService.update(empCompensationDO);
			 		CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Employee Compensation Updated");
			 	}
				
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}*/
	
	/*@RequestMapping(value = "/retrieveWithDateForStatus/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveWithDateForStatus(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty() && !inputJSON.get(CommonConstants.MONTHLY).toString().isEmpty()){
					List<EmployeeCompensationDO> empCompensationList = employeeCompensationService.retrieve();
					respJSON = EmployeeCompensationUtil.getempCompensationListWithBatch(empCompensationList, Long.parseLong(inputJSON.get(CommonConstants.ID).toString()), inputJSON.get(CommonConstants.MONTHLY).toString()).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}*/
	
	
	@RequestMapping(value = "/importComponesation", method = RequestMethod.POST)
	public @ResponseBody String importComponesation(Model model, HttpServletRequest request) {
		try {
			JSONObject inputJSON =  CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
			JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
			List<EmployeeCompensationDO> empCompensationlist = new ArrayList<EmployeeCompensationDO>();
			Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY);
			for (int i=0; i < fileData.size(); i++){
				JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
				EmployeeCompensationDO empCompensationDO = new EmployeeCompensationDO();
				
				/*if (colName.get(CommonConstants.NAME) != null && !colName.get(CommonConstants.NAME).toString().isEmpty()){
					departmentDO.setName(rowJSON.get(colName.get(CommonConstants.NAME)).toString());
		 		}*/
				if(colName.get(CommonConstants.EMPID) != null && !colName.get(CommonConstants.EMPID).toString().isEmpty()){
		 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(rowJSON.get(colName.get(CommonConstants.EMPID)).toString());
			 		if(employeeList != null && employeeList.size() > 0){
			 			empCompensationDO.setEmployee(employeeList.get(0));
			 			if(colName.get(CommonConstants.PAYROLLGROUPID) != null && !colName.get(CommonConstants.PAYROLLGROUPID).toString().isEmpty()){
				 			List<PayrollGroupDO> payrollGroupList = payrollGroupService.retrieveByName(rowJSON.get(colName.get(CommonConstants.PAYROLLGROUPID)).toString());
					 		if(payrollGroupList != null && payrollGroupList.size() >0){
					 			empCompensationDO.setPayrollGroup(payrollGroupList.get(0));
					 			if(colName.get(CommonConstants.EMPCTC) != null && !colName.get(CommonConstants.EMPCTC).toString().isEmpty()){
					 				List<EmployeeCtcDO> empCtcDetails = employeeCtcService.retrieveByEmpCtc(Long.parseLong(rowJSON.get(colName.get(CommonConstants.EMPCTC)).toString()), rowJSON.get(colName.get(CommonConstants.EMPID)).toString());
						 			if(empCtcDetails != null && empCtcDetails.size() > 0){
						 				empCompensationDO.setEmpCtc(empCtcDetails.get(0));
						 			}
						 		}
					 			
					 			if(colName.get(CommonConstants.EFFECTIVEFROM) != null && !colName.get(CommonConstants.EFFECTIVEFROM).toString().isEmpty()){
					 				empCompensationDO.setEffectivefrom(CommonUtil.convertStringToSqlDate(rowJSON.get(colName.get(CommonConstants.EFFECTIVEFROM)).toString()));
					 			}
					 			if(colName.get(CommonConstants.EFFECTIVETO) != null && !colName.get(CommonConstants.EFFECTIVETO).toString().isEmpty() && rowJSON.get(colName.get(CommonConstants.EFFECTIVETO)) != null && !rowJSON.get(colName.get(CommonConstants.EFFECTIVETO)).toString().isEmpty()){
					 				empCompensationDO.setEffectiveto(CommonUtil.convertStringToSqlDate(rowJSON.get(colName.get(CommonConstants.EFFECTIVETO)).toString()));
					 			}	
					 			
					 			if(colName.get(CommonConstants.STATUS) != null){
									if(!rowJSON.get(colName.get(CommonConstants.STATUS)).toString().isEmpty()){
										if(rowJSON.get(colName.get(CommonConstants.STATUS)).toString().equalsIgnoreCase("Active")){
											empCompensationDO.setStatus(CommonConstants.ACTIVE);
										} else{
											empCompensationDO.setStatus(CommonConstants.INACTIVE);
										}
							 		}else{
										empCompensationDO.setStatus(CommonConstants.ACTIVE);
									}
								}
					 			
					 			if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
						 			empCompensationDO.setUpdatedby(updatedBy.toString());
						 			empCompensationDO.setCreatedby(updatedBy.toString());
						 		}
						 		empCompensationDO.setUpdatedon(new Date());
						 		empCompensationDO.setCreatedon(new Date());
						 		empCompensationlist.add(empCompensationDO);
					 		}
				 		}
			 			
			 			
			 		}
		 		}
	 			
			}
			employeeCompensationService.persistList(empCompensationlist);
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
}
