package com.spheresuite.erp.rs;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.auth0.jwt.JWTVerifyException;
import com.spheresuite.erp.domainobject.DepartmentDO;
import com.spheresuite.erp.domainobject.DesignationDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.EmployeeTypeDO;
import com.spheresuite.erp.domainobject.UnitOrBranchDO;
import com.spheresuite.erp.domainobject.WorkLocationDO;
import com.spheresuite.erp.service.DepartmentService;
import com.spheresuite.erp.service.DesignationService;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.EmployeeTypeService;
import com.spheresuite.erp.service.LeadEmailService;
import com.spheresuite.erp.service.UnitOrBranchService;
import com.spheresuite.erp.service.WorkLocationService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.EmployeeUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/employee")
public class EmployeeRS {

	String validation = null;
	static Logger logger = Logger.getLogger(EmployeeRS.class.getName());

	@Autowired
	private EmployeeService employeeService;
	
	@Autowired
	private LeadEmailService leadEmailService;
	
	@Autowired
	private UnitOrBranchService unitOrBranchService;
	
	@Autowired
	private EmployeeTypeService employeeTypeService;
	
	@Autowired
	private DepartmentService departmentService;
	
	@Autowired
	private WorkLocationService worklocationService;
	
	@Autowired
	private DesignationService designationService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeDO employeeDO = new EmployeeDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.UNIT) != null && !inputJSON.get(CommonConstants.UNIT).toString().isEmpty()){
			 			List<UnitOrBranchDO> unitOrBranchDOList = unitOrBranchService.retrieveActiveById(Long.parseLong(inputJSON.get(CommonConstants.UNIT).toString()));
			 			if(unitOrBranchDOList != null && unitOrBranchDOList.size() > 0){
			 				employeeDO.setUnitOrBranch(unitOrBranchDOList.get(0));
			 				if(inputJSON.get(CommonConstants.DEPT) != null && !inputJSON.get(CommonConstants.DEPT).toString().isEmpty()){
					 			List<DepartmentDO> deptList = departmentService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.DEPT).toString()));
					 			if(deptList != null && deptList.size() > 0){
					 				employeeDO.setDepartment(deptList.get(0));
					 				if(inputJSON.get(CommonConstants.EMPID) != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
							 			employeeDO.setEmpId(inputJSON.get(CommonConstants.EMPID).toString());
							 			if(inputJSON.get(CommonConstants.FIRSTNAME) != null && !inputJSON.get(CommonConstants.FIRSTNAME).toString().isEmpty()){
								 			employeeDO.setFirstname(inputJSON.get(CommonConstants.FIRSTNAME).toString());
								 			if(inputJSON.get(CommonConstants.GENDER) != null && !inputJSON.get(CommonConstants.GENDER).toString().isEmpty()){
									 			employeeDO.setGender((char) inputJSON.get(CommonConstants.GENDER).toString().charAt(0));
									 			if(inputJSON.get(CommonConstants.DATEOFBIRTH) != null && !inputJSON.get(CommonConstants.DATEOFBIRTH).toString().isEmpty()){
										 			employeeDO.setDateofbirth(inputJSON.get(CommonConstants.DATEOFBIRTH).toString());
										 			if(inputJSON.get(CommonConstants.STARTDATE) != null && !inputJSON.get(CommonConstants.STARTDATE).toString().isEmpty()){
											 			employeeDO.setStartdate(inputJSON.get(CommonConstants.STARTDATE).toString());
											 			if(inputJSON.get(CommonConstants.LASTNAME) != null && !inputJSON.get(CommonConstants.LASTNAME).toString().isEmpty()){
												 			employeeDO.setLastname(inputJSON.get(CommonConstants.LASTNAME).toString());
												 		}
												 		if(inputJSON.get(CommonConstants.MIDDLENAME) != null && !inputJSON.get(CommonConstants.MIDDLENAME).toString().isEmpty()){
												 			employeeDO.setMiddlename(inputJSON.get(CommonConstants.MIDDLENAME).toString());
												 		}
												 		if(inputJSON.get(CommonConstants.PERSONALCONTACT) != null && !inputJSON.get(CommonConstants.PERSONALCONTACT).toString().isEmpty()){
												 			employeeDO.setPersonalcontact(inputJSON.get(CommonConstants.PERSONALCONTACT).toString());
												 		}
												 		if(inputJSON.get(CommonConstants.PRIMARYCONTACT) != null && !inputJSON.get(CommonConstants.PRIMARYCONTACT).toString().isEmpty()){
												 			employeeDO.setPrimarycontact(inputJSON.get(CommonConstants.PRIMARYCONTACT).toString());
												 		}
												 		if(inputJSON.get(CommonConstants.PERSONALEMAIL) != null && !inputJSON.get(CommonConstants.PERSONALEMAIL).toString().isEmpty()){
												 			employeeDO.setPersonalemail(inputJSON.get(CommonConstants.PERSONALEMAIL).toString());
												 		}
												 		if(inputJSON.get(CommonConstants.COMPANYEMAIL) != null && !inputJSON.get(CommonConstants.COMPANYEMAIL).toString().isEmpty()){
												 			employeeDO.setCompanyemail(inputJSON.get(CommonConstants.COMPANYEMAIL).toString());
												 		}
												 		if(inputJSON.get(CommonConstants.AADHAR) != null && !inputJSON.get(CommonConstants.AADHAR).toString().isEmpty()){
												 			employeeDO.setAadhar(inputJSON.get(CommonConstants.AADHAR).toString());
												 		}
												 		if(inputJSON.get(CommonConstants.PANNO) != null && !inputJSON.get(CommonConstants.PANNO).toString().isEmpty()){
												 			employeeDO.setPanno(inputJSON.get(CommonConstants.PANNO).toString());
												 		}
												 		
												 		if(inputJSON.get(CommonConstants.REPORTTO) != null && !inputJSON.get(CommonConstants.REPORTTO).toString().isEmpty()){
												 			employeeDO.setReportto( inputJSON.get(CommonConstants.REPORTTO).toString());
												 		}
												 		if(inputJSON.get(CommonConstants.JOBDESC) != null && !inputJSON.get(CommonConstants.JOBDESC).toString().isEmpty()){
												 			employeeDO.setJobdesc(inputJSON.get(CommonConstants.JOBDESC).toString());
												 		}
												 		employeeDO.setStatus(CommonConstants.ACTIVE);
												 		if(inputJSON.get(CommonConstants.WORKLOCATION) != null && !inputJSON.get(CommonConstants.WORKLOCATION).toString().isEmpty()){
												 			List<WorkLocationDO> worklocationList = worklocationService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.WORKLOCATION).toString()));
												 			if(worklocationList != null && worklocationList.size() > 0){
												 				employeeDO.setWorklocation(worklocationList.get(0));
												 			}
											 			}
												 		if(inputJSON.get(CommonConstants.TITLE_ID) != null && !inputJSON.get(CommonConstants.TITLE_ID).toString().isEmpty()){
												 			List<DesignationDO> designationList = designationService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.TITLE_ID).toString()));
												 			if(designationList != null && designationList.size() > 0){
												 				employeeDO.setDesignation(designationList.get(0));
												 			}
											 			}
												 		if(inputJSON.get(CommonConstants.EMPTYPE) != null && !inputJSON.get(CommonConstants.EMPTYPE).toString().isEmpty()){
												 			List<EmployeeTypeDO> empTypeList = employeeTypeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.EMPTYPE).toString()));
												 			if(empTypeList != null && empTypeList.size() > 0){
												 				employeeDO.setEmployeeType(empTypeList.get(0));
												 			}
											 			}
												 		
												 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
												 			employeeDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
												 			employeeDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
												 		}
												 		employeeDO.setUpdatedon(new Date());
												 		employeeDO.setCreatedon(new Date());
												 		if(!employeeService.persist(employeeDO)){
												 			return CommonWebUtil.buildErrorResponse("Employee Already Exist").toString();
												 		}
												 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Employee Created");
											 		}
										 		}
									 		}
								 		}
							 		}
					 			}
				 			}
			 			}
		 			}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retriveById/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String retriveById(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.ID).toString());
					respJSON = EmployeeUtil.getEmployeeList(employeeList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) throws InvalidKeyException, NoSuchAlgorithmException, IllegalStateException, SignatureException, IOException, JWTVerifyException {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<EmployeeDO> employeeList = employeeService.retrieve();
				respJSON = EmployeeUtil.getEmployeeList(employeeList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveNameId", method = RequestMethod.GET)
	public @ResponseBody String retrieveNameId(Model model, HttpServletRequest request) throws InvalidKeyException, NoSuchAlgorithmException, IllegalStateException, SignatureException, IOException, JWTVerifyException {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<Object[]> employeeList = employeeService.retrieveNameId();
				respJSON = EmployeeUtil.getEmployeeNameId(employeeList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retriveByUnitId/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String retriveByUnitId(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<Object[]> employeeList = employeeService.retrieveByUnit(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = EmployeeUtil.getEmployeeNameId(employeeList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveSpecificField", method = RequestMethod.GET)
	public @ResponseBody String retrieveSpecificField(Model model, HttpServletRequest request) throws InvalidKeyException, NoSuchAlgorithmException, IllegalStateException, SignatureException, IOException, JWTVerifyException {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<Object[]> employeeList = employeeService.retrieveAll();
				respJSON = EmployeeUtil.getEmployeeListObject(employeeList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(Model model, HttpServletRequest request) throws InvalidKeyException, NoSuchAlgorithmException, IllegalStateException, SignatureException, IOException, JWTVerifyException {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<EmployeeDO> employeeList = employeeService.retrieveActive();
				respJSON = EmployeeUtil.getEmployeeList(employeeList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveId", method = RequestMethod.GET)
	public @ResponseBody String retrieveId(Model model, HttpServletRequest request) throws InvalidKeyException, NoSuchAlgorithmException, IllegalStateException, SignatureException, IOException, JWTVerifyException {
		String empid = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<EmployeeDO> employeeList = employeeService.retrieveForId();
				if(employeeList.get(0).getEmpId() != null){
					empid = employeeList.get(0).getEmpId();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch(NumberFormatException ex){  
			empid = null;
			return CommonWebUtil.buildSuccessResponseId(empid).toString();
		}catch(Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponseId(empid).toString();
	}

	@RequestMapping(value = "/retriveByEmail/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String retriveByEmail(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.EMAIL).toString().isEmpty()){
					List<EmployeeDO> employeeList = employeeService.retrieveByEmail(inputJSON.get(CommonConstants.EMAIL).toString());
					respJSON = EmployeeUtil.getEmployeeList(employeeList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
		
	@RequestMapping(value = "/update", method = RequestMethod.POST)
		public @ResponseBody String update(Model model, HttpServletRequest request) {
			try {
				if (WebManager.authenticateSession(request)) {
					EmployeeDO employeeDO = new EmployeeDO();
					JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
				 	if (inputJSON != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
				 		List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
				 		if(employeeList != null && employeeList.size() > 0){
				 			employeeDO = employeeList.get(0);
				 			if(inputJSON.get(CommonConstants.FIRSTNAME) != null && !inputJSON.get(CommonConstants.FIRSTNAME).toString().isEmpty()){
					 			employeeDO.setFirstname(inputJSON.get(CommonConstants.FIRSTNAME).toString());
					 		}
					 		if(inputJSON.get(CommonConstants.LASTNAME) != null && !inputJSON.get(CommonConstants.LASTNAME).toString().isEmpty()){
					 			employeeDO.setLastname(inputJSON.get(CommonConstants.LASTNAME).toString());
					 		}
					 		if(inputJSON.get(CommonConstants.MIDDLENAME) != null && !inputJSON.get(CommonConstants.MIDDLENAME).toString().isEmpty()){
					 			employeeDO.setMiddlename(inputJSON.get(CommonConstants.MIDDLENAME).toString());
					 		}
					 		if(inputJSON.get(CommonConstants.PERSONALCONTACT) != null && !inputJSON.get(CommonConstants.PERSONALCONTACT).toString().isEmpty()){
					 			employeeDO.setPersonalcontact(inputJSON.get(CommonConstants.PERSONALCONTACT).toString());
					 		}
					 		if(inputJSON.get(CommonConstants.PRIMARYCONTACT) != null && !inputJSON.get(CommonConstants.PRIMARYCONTACT).toString().isEmpty()){
					 			employeeDO.setPrimarycontact(inputJSON.get(CommonConstants.PRIMARYCONTACT).toString());
					 		}
					 		if(inputJSON.get(CommonConstants.PERSONALEMAIL) != null && !inputJSON.get(CommonConstants.PERSONALEMAIL).toString().isEmpty()){
					 			employeeDO.setPersonalemail(inputJSON.get(CommonConstants.PERSONALEMAIL).toString());
					 		}
					 		if(inputJSON.get(CommonConstants.COMPANYEMAIL) != null && !inputJSON.get(CommonConstants.COMPANYEMAIL).toString().isEmpty()){
					 			employeeDO.setCompanyemail(inputJSON.get(CommonConstants.COMPANYEMAIL).toString());
					 		}
					 		if(inputJSON.get(CommonConstants.AADHAR) != null && !inputJSON.get(CommonConstants.AADHAR).toString().isEmpty()){
					 			employeeDO.setAadhar(inputJSON.get(CommonConstants.AADHAR).toString());
					 		}
					 		if(inputJSON.get(CommonConstants.PANNO) != null && !inputJSON.get(CommonConstants.PANNO).toString().isEmpty()){
					 			employeeDO.setPanno(inputJSON.get(CommonConstants.PANNO).toString());
					 		}
					 		if(inputJSON.get(CommonConstants.DATEOFBIRTH) != null && !inputJSON.get(CommonConstants.DATEOFBIRTH).toString().isEmpty()){
					 			employeeDO.setDateofbirth(inputJSON.get(CommonConstants.DATEOFBIRTH).toString());
					 		}
					 		if(inputJSON.get(CommonConstants.REPORTTO) != null && !inputJSON.get(CommonConstants.REPORTTO).toString().isEmpty()){
					 			employeeDO.setReportto( inputJSON.get(CommonConstants.REPORTTO).toString());
					 		}
					 		if(inputJSON.get(CommonConstants.TITLE_ID) != null && !inputJSON.get(CommonConstants.TITLE_ID).toString().isEmpty()){
					 			List<DesignationDO> designationList = designationService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.TITLE_ID).toString()));
					 			if(designationList != null && designationList.size() > 0){
					 				employeeDO.setDesignation(designationList.get(0));
					 			}
				 			}
					 		if(inputJSON.get(CommonConstants.JOBDESC) != null && !inputJSON.get(CommonConstants.JOBDESC).toString().isEmpty()){
					 			employeeDO.setJobdesc(inputJSON.get(CommonConstants.JOBDESC).toString());
					 		}
					 		if(inputJSON.get(CommonConstants.STARTDATE) != null && !inputJSON.get(CommonConstants.STARTDATE).toString().isEmpty()){
					 			employeeDO.setStartdate(inputJSON.get(CommonConstants.STARTDATE).toString());
					 		}
					 		if(inputJSON.get(CommonConstants.GENDER) != null && !inputJSON.get(CommonConstants.GENDER).toString().isEmpty()){
					 			employeeDO.setGender((char) inputJSON.get(CommonConstants.GENDER).toString().charAt(0));
					 		}
					 		if(inputJSON.get(CommonConstants.UNIT) != null && !inputJSON.get(CommonConstants.UNIT).toString().isEmpty()){
					 			List<UnitOrBranchDO> unitOrBranchDOList = unitOrBranchService.retrieveActiveById(Long.parseLong(inputJSON.get(CommonConstants.UNIT).toString()));
					 			if(unitOrBranchDOList != null && unitOrBranchDOList.size() > 0){
					 				employeeDO.setUnitOrBranch(unitOrBranchDOList.get(0));
					 			}
				 			}
					 		
					 		if(inputJSON.get(CommonConstants.WORKLOCATION) != null && !inputJSON.get(CommonConstants.WORKLOCATION).toString().isEmpty()){
					 			List<WorkLocationDO> worklocationList = worklocationService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.WORKLOCATION).toString()));
					 			if(worklocationList != null && worklocationList.size() > 0){
					 				employeeDO.setWorklocation(worklocationList.get(0));
					 			}
				 			}
					 		
					 		if(inputJSON.get(CommonConstants.EMPTYPE) != null && !inputJSON.get(CommonConstants.EMPTYPE).toString().isEmpty()){
					 			List<EmployeeTypeDO> empTypeList = employeeTypeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.EMPTYPE).toString()));
					 			if(empTypeList != null && empTypeList.size() > 0){
					 				employeeDO.setEmployeeType(empTypeList.get(0));
					 			}
				 			}
					 		if(inputJSON.get(CommonConstants.DEPT) != null && !inputJSON.get(CommonConstants.DEPT).toString().isEmpty()){
					 			List<DepartmentDO> deptList = departmentService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.DEPT).toString()));
					 			if(deptList != null && deptList.size() > 0){
					 				employeeDO.setDepartment(deptList.get(0));
					 			}
				 			}
					 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			employeeDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			employeeDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 		}
					 		if(!employeeService.update(employeeDO)){
								return CommonWebUtil.buildErrorResponse("Employee Already Added").toString();
							}
					 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Employee details Updated");
						}
				 	}
				}else{
					return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
				}
			}catch (Exception e) {
				return CommonWebUtil.buildErrorResponse("").toString();
			}
			return CommonWebUtil.buildSuccessResponse().toString();
		}
	
	@RequestMapping(value = "/updatepic", method = RequestMethod.POST)
	public @ResponseBody String updatepic(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeDO employeeDO = new EmployeeDO();
				/*JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("data").toString());*/
			 	if (request.getParameter("id") != null){
			 		List<EmployeeDO> employeeList = employeeService.retriveByEmpId(request.getParameter("id"));
			 		if(request.getParameter("file") != null && !request.getParameter("file").equals("")){
			 			if(employeeList != null && employeeList.size() > 0){
				 			employeeDO = employeeList.get(0);
					 		employeeDO.setPhoto(request.getParameter("file"));
					 		employeeDO.setUpdatedon(new Date());
					 		employeeService.update(employeeDO);
				 		}
			 		}else{
			 			if(employeeList != null && employeeList.size() > 0){
				 			employeeDO = employeeList.get(0);
					 		employeeDO.setPhoto(null);
					 		employeeDO.setUpdatedon(new Date());
					 		employeeService.update(employeeDO);
				 		}
			 		}
			 		
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/updateEmail", method = RequestMethod.POST)
	public @ResponseBody String updateEmail(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeDO employeeDO = new EmployeeDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
				if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.ID).toString());
			 		if(employeeList != null && employeeList.size() > 0){
			 			employeeDO = employeeList.get(0);
			 			if(inputJSON.get(CommonConstants.EMAIL).toString() != null && inputJSON.get(CommonConstants.PASSWORD).toString() != null
			 					){//&& inputJSON.get(CommonConstants.TYPE).toString() != null
			 				employeeDO.setEmailId(inputJSON.get(CommonConstants.EMAIL).toString()); 
			 				String pwd = inputJSON.get(CommonConstants.PASSWORD).toString();
			 				employeeDO.setEmailPassword(Base64.getEncoder().encodeToString(pwd.getBytes("UTF-8")));
			 				employeeDO.setUpdatedon(new Date());
			 				employeeDO.setEmailType(inputJSON.get(CommonConstants.TYPE).toString());
					 		employeeService.update(employeeDO);
					 		//MailBoxUtil.addInbox(employeeDO.getEmailId(), employeeDO.getEmailPassword(), employeeDO.getId());
			 			}else{
			 				return CommonWebUtil.buildErrorResponse("").toString();
			 			}
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	
	@RequestMapping(value = "/resetEmail", method = RequestMethod.POST)
	public @ResponseBody String resetEmail(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeDO employeeDO = new EmployeeDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
				if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.ID).toString());
			 		if(employeeList != null && employeeList.size() > 0){
			 			employeeDO = employeeList.get(0);
		 				employeeDO.setEmailId(null); 
		 				employeeDO.setEmailPassword(null);
		 				employeeDO.setUpdatedon(new Date());
		 				employeeDO.setEmailType(null);
				 		employeeService.update(employeeDO);
				 		leadEmailService.delete(employeeDO.getEmpId());
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	public String checkDuplicate(String empId, String companyEmail){
		return employeeService.checkDuplicate(empId, companyEmail);
	}
	
	public String checkDuplicateUpdate(String empId, String companyEmail, Long Id) {
		return employeeService.checkDuplicateUpdate(empId, companyEmail, Id);
	}
	
	@RequestMapping(value = "/importEmp", method = RequestMethod.POST)
	public @ResponseBody String importEmp(Model model, HttpServletRequest request) {
		try {
			//if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
				org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
				JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
				List<EmployeeDO> empDOlist = new ArrayList<EmployeeDO>();
				Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY);
				for (int i=0; i < fileData.size(); i++){
					JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
					//if(colName.get(CommonConstants.COMPANYEMAIL) != null && !rowJSON.get(colName.get(CommonConstants.COMPANYEMAIL)).toString().isEmpty() ){
						//String flag = checkDuplicate(null,rowJSON.get(colName.get(CommonConstants.COMPANYEMAIL)).toString());
						// if(!flag.equalsIgnoreCase(CommonConstants.COMPANYEMAIL)){
							 EmployeeDO employeeDO = new EmployeeDO();
								/*
								if(colName.get(CommonConstants.EMPID) != null){
									Long empId=0L;
									List<EmployeeDO> employeeList = employeeService.retrieve();
									try {
									if(employeeList != null && employeeList.size() > 0){
										Collections.reverse(employeeList);
										if(employeeList.get(0).getEmpId() != null){
											empId = Long.parseLong(employeeList.get(0).getEmpId());
											empId = empId+1;
										}
									}
									}catch(NumberFormatException ex){  
										empId = 0L;
									}
									if(rowJSON.get(colName.get(CommonConstants.EMPID)).toString().isEmpty()){
										employeeDO.setEmpId(empId.toString());
									}else{
										employeeDO.setEmpId(rowJSON.get(colName.get(CommonConstants.EMPID)).toString());
									}
									
								}else{
									Long empId=0L;
									List<EmployeeDO> employeeList = employeeService.retrieve();
									try {
									if(employeeList != null && employeeList.size() > 0){
										Collections.reverse(employeeList);
										if(employeeList.get(0).getEmpId() != null){
											empId = Long.parseLong(employeeList.get(0).getEmpId());
											empId = empId+1;
										}
									}
									}catch(NumberFormatException ex){  
										empId = 0L;
									}
									employeeDO.setEmpId(empId.toString());
								}*/
							 	if(colName.get(CommonConstants.UNIT) != null && !rowJSON.get(colName.get(CommonConstants.UNIT)).toString().isEmpty()){
						 			List<UnitOrBranchDO> unitOrBranchDOList = unitOrBranchService.retrieveActiveByName(rowJSON.get(colName.get(CommonConstants.UNIT)).toString());
						 			if(unitOrBranchDOList != null && unitOrBranchDOList.size() > 0){
						 				employeeDO.setUnitOrBranch(unitOrBranchDOList.get(0));
						 				 if(colName.get(CommonConstants.EMPID) != null){
											 employeeDO.setEmpId(rowJSON.get(colName.get(CommonConstants.EMPID)).toString());
											 if(colName.get(CommonConstants.FIRSTNAME) != null){
													employeeDO.setFirstname(!rowJSON.get(colName.get(CommonConstants.FIRSTNAME)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.FIRSTNAME)).toString() : null);
													if(colName.get(CommonConstants.GENDER) != null){
														employeeDO.setGender((char) (rowJSON != null ? rowJSON.get(colName.get(CommonConstants.GENDER)).toString().charAt(0) : ""));
														if(colName.get(CommonConstants.DEPT) != null){
													 		if(!rowJSON.get(colName.get(CommonConstants.DEPT)).toString().isEmpty()){
													 			List<DepartmentDO> departmentList = departmentService.retrieveByName(rowJSON.get(colName.get(CommonConstants.DEPT)).toString());
													 			if(departmentList != null && departmentList.size() > 0 ){
													 				employeeDO.setDepartment(departmentList.get(0));
													 				if(colName.get(CommonConstants.DATEOFBIRTH) != null){
																		employeeDO.setDateofbirth(!rowJSON.get(colName.get(CommonConstants.DATEOFBIRTH)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.DATEOFBIRTH)).toString() : null);
																		if(colName.get(CommonConstants.STARTDATE) != null){
																			employeeDO.setStartdate(!rowJSON.get(colName.get(CommonConstants.STARTDATE)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.STARTDATE)).toString() : null);
																			if(colName.get(CommonConstants.COMPANYEMAIL) != null){
																				employeeDO.setCompanyemail(rowJSON.get(colName.get(CommonConstants.COMPANYEMAIL)).toString());
																				if(colName.get(CommonConstants.WORKLOCATION) != null && !rowJSON.get(colName.get(CommonConstants.WORKLOCATION)).toString().isEmpty()){
																		 			List<WorkLocationDO> worklocationList = worklocationService.retrieveByName(rowJSON.get(colName.get(CommonConstants.WORKLOCATION)).toString());
																		 			if(worklocationList != null && worklocationList.size() > 0){
																		 				employeeDO.setWorklocation(worklocationList.get(0));
																		 				if(colName.get(CommonConstants.EMPTYPE) != null){
																					 		if(!rowJSON.get(colName.get(CommonConstants.EMPTYPE)).toString().isEmpty()){
																					 			List<EmployeeTypeDO> empTypeList = employeeTypeService.retrieveByName(rowJSON.get(colName.get(CommonConstants.EMPTYPE)).toString());
																					 			if(empTypeList != null && empTypeList.size() > 0){
																					 				employeeDO.setEmployeeType(empTypeList.get(0));
																					 			}
																					 		}
																						}
																						if(colName.get(CommonConstants.LASTNAME) != null){
																							employeeDO.setLastname(!rowJSON.get(colName.get(CommonConstants.LASTNAME)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.LASTNAME)).toString() : null);
																						}
																						if(colName.get(CommonConstants.MIDDLENAME) != null){
																							employeeDO.setMiddlename(!rowJSON.get(colName.get(CommonConstants.MIDDLENAME)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.MIDDLENAME)).toString() : null);
																						}
																						if(colName.get(CommonConstants.PERSONALCONTACT) != null){
																							employeeDO.setPersonalcontact(!rowJSON.get(colName.get(CommonConstants.PERSONALCONTACT)).toString().isEmpty()? rowJSON.get(colName.get(CommonConstants.PERSONALCONTACT)).toString() : null);
																						}
																						if(colName.get(CommonConstants.PRIMARYCONTACT) != null){
																							employeeDO.setPrimarycontact(!rowJSON.get(colName.get(CommonConstants.PRIMARYCONTACT)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.PRIMARYCONTACT)).toString() : null);
																						}
																						if(colName.get(CommonConstants.PERSONALEMAIL) != null){
																							employeeDO.setPersonalemail(!rowJSON.get(colName.get(CommonConstants.PERSONALEMAIL)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.PERSONALEMAIL)).toString() : null);
																						}
																						if(colName.get(CommonConstants.AADHAR) != null){
																							employeeDO.setAadhar(!rowJSON.get(colName.get(CommonConstants.AADHAR)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.AADHAR)).toString() : null);
																						}
																						if(colName.get(CommonConstants.PANNO) != null){
																							employeeDO.setPanno(!rowJSON.get(colName.get(CommonConstants.PANNO)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.PANNO)).toString() : null);
																						}
																						if(colName.get(CommonConstants.REPORTTO) != null){
																					 		if(!rowJSON.get(colName.get(CommonConstants.REPORTTO)).toString().isEmpty()){
																					 			List<EmployeeDO> employeeList = employeeService.retrieveActive();
																					 			if(employeeList != null && employeeList.size() > 0){
																									for(EmployeeDO empDO : employeeList){
																										String Name = empDO.getFirstname() + " " +empDO.getMiddlename() + " " + empDO.getLastname();
																										if(Name.toLowerCase().contains(rowJSON.get(colName.get(CommonConstants.REPORTTO)).toString().toLowerCase())){
																											employeeDO.setReportto(empDO.getEmpId().toString());
																										}
																									}
																								}
																					 		}
																						}
																						if(colName.get(CommonConstants.TITLE) != null){
																					 		if(!rowJSON.get(colName.get(CommonConstants.TITLE)).toString().isEmpty()){
																					 			List<DesignationDO> designationList = designationService.retrieveByName(rowJSON.get(colName.get(CommonConstants.TITLE)).toString());
																					 			if(designationList != null && designationList.size() > 0){
																					 				employeeDO.setDesignation(designationList.get(0));
																					 			}
																					 		}
																						}
																						
																						if(colName.get(CommonConstants.JOBDESC) != null){
																							employeeDO.setJobdesc(!rowJSON.get(colName.get(CommonConstants.JOBDESC)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.JOBDESC)).toString() : null);
																						}
																						
																				 		if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
																				 			employeeDO.setUpdatedby(updatedBy.toString());
																				 			employeeDO.setCreatedby(updatedBy.toString());
																				 		}
																				 		employeeDO.setUpdatedon(new Date());
																				 		employeeDO.setCreatedon(new Date());
																				 		employeeDO.setStatus(CommonConstants.ACTIVE);
																						empDOlist.add(employeeDO);
																		 			}
																	 			}
																			}
																		}
													 				}
													 			}
													 		}
														}
													}
											 }
										 }
						 			}
					 			}
						// }
					//}
				}
				employeeService.persistList(empDOlist);
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveEmpReporting/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveReportingDetails(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.ID).toString());
					respJSON = EmployeeUtil.getEmployeeReportingList(employeeList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	
	@RequestMapping(value = "/retrieveReportingEmpById/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByTeamAttendance(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
					List<EmployeeDO> empList = employeeService.retrieveReportTo(inputJSON.get(CommonConstants.EMPID).toString());
					if(empList != null && empList.size() > 0){
						respJSON =EmployeeUtil.getEmployeeList(empList).toString();
						/*for (EmployeeDO EmployeeDO : empList) {
							empIds.add(EmployeeDO.getReportto());
						}
						List<AttendanceDO> attendanceList = attendanceService.retrieveByTeamEmp(empIds);
						respJSON = AttendanceUtil.getAttendanceList(attendanceList).toString();*/
					} else {
						return CommonWebUtil.buildErrorResponse("Employee Details Not Available").toString();
					}
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
}
