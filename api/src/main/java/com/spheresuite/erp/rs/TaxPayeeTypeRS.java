package com.spheresuite.erp.rs;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.TaxPayeeTypeDO;
import com.spheresuite.erp.service.TaxPayeeTypeService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.TaxPayeeTypeUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/taxpayeetype")
public class TaxPayeeTypeRS {

	String validation = null;
	static Logger logger = Logger.getLogger(TaxPayeeTypeRS.class.getName());
	@Autowired
	private TaxPayeeTypeService taxPayeeTypeService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				TaxPayeeTypeDO taxPayeeTypeDO = new TaxPayeeTypeDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.TAXPAYEETYPE) != null && !inputJSON.get(CommonConstants.TAXPAYEETYPE).toString().isEmpty()){
			 			taxPayeeTypeDO.setTaxpayeeType(inputJSON.get(CommonConstants.TAXPAYEETYPE).toString());
			 		}
			 		taxPayeeTypeDO.setUpdatedon(new Date());
			 		taxPayeeTypeDO.setCreatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			taxPayeeTypeDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			taxPayeeTypeDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		taxPayeeTypeDO.setStatus(CommonConstants.ACTIVE);
			 	}
				if(!taxPayeeTypeService.persist(taxPayeeTypeDO)){
		 			return CommonWebUtil.buildErrorResponse("Tax Payee Type Already Added").toString();
				}
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Tax Payee Type Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<TaxPayeeTypeDO> taxPayeeTypeList = taxPayeeTypeService.retrieve();
				respJSON = TaxPayeeTypeUtil.getTaxPayeeTypeList(taxPayeeTypeList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<TaxPayeeTypeDO> taxPayeeTypeList =  taxPayeeTypeService.retrieveActive(CommonConstants.ACTIVE);
				respJSON = TaxPayeeTypeUtil.getTaxPayeeTypeList(taxPayeeTypeList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveActivePayee/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveActivePayee(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
					if(inputJSON != null && !inputJSON.get(CommonConstants.FROMDATE).toString().isEmpty()
							&& !inputJSON.get(CommonConstants.TODATE).toString().isEmpty()){
					List<TaxPayeeTypeDO> taxPayeeTypeList =  taxPayeeTypeService.retrieveActive(CommonConstants.ACTIVE);
					respJSON = TaxPayeeTypeUtil.getTaxPayeeSlabSetting(taxPayeeTypeList,
							inputJSON.get(CommonConstants.FROMDATE).toString(), inputJSON.get(CommonConstants.TODATE).toString()).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				TaxPayeeTypeDO taxPayeeTypeDO = new TaxPayeeTypeDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<TaxPayeeTypeDO> taxPayeeTypeList = taxPayeeTypeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(taxPayeeTypeList != null && taxPayeeTypeList.size() >0){
				 		taxPayeeTypeDO = taxPayeeTypeList.get(0);
				 		if(inputJSON.get(CommonConstants.TAXPAYEETYPE) != null && !inputJSON.get(CommonConstants.TAXPAYEETYPE).toString().isEmpty()){
				 			taxPayeeTypeDO.setTaxpayeeType(inputJSON.get(CommonConstants.TAXPAYEETYPE).toString());
				 		}
				 		taxPayeeTypeDO.setUpdatedon(new Date());
				 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			taxPayeeTypeDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
				 			taxPayeeTypeDO.setStatus((char)inputJSON.get(CommonConstants.STATUS).toString().charAt(0));
				 		}
				 		if(!taxPayeeTypeService.update(taxPayeeTypeDO)){
				 			return CommonWebUtil.buildErrorResponse("Tax Payee Type Already Added").toString();
				 		}
				 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Tax Payee Type Updated");
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
