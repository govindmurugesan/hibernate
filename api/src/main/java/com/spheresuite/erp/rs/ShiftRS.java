package com.spheresuite.erp.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.ShiftDO;
import com.spheresuite.erp.service.ShiftService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.ShiftUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/shift")
public class ShiftRS {

	String validation = null;
	static Logger logger = Logger.getLogger(ShiftRS.class.getName());

	@Autowired
	private ShiftService shiftService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				ShiftDO shiftDO = new ShiftDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		
			 		if (inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
			 			shiftDO.setName(inputJSON.get(CommonConstants.NAME).toString());
			 		}
			 		if (inputJSON.get(CommonConstants.STARTTIME) != null && !inputJSON.get(CommonConstants.STARTTIME).toString().isEmpty()){
			 			shiftDO.setStartTime(inputJSON.get(CommonConstants.STARTTIME).toString());
			 		}
			 		if (inputJSON.get(CommonConstants.ENDTIME) != null && !inputJSON.get(CommonConstants.ENDTIME).toString().isEmpty()){
			 			shiftDO.setEndTime(inputJSON.get(CommonConstants.ENDTIME).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.EMPID) != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			shiftDO.setIsManager(inputJSON.get(CommonConstants.EMPID).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.MAX_HOUR_EXCUSE) != null && !inputJSON.get(CommonConstants.MAX_HOUR_EXCUSE).toString().isEmpty()){
			 			shiftDO.setMaxHrExcuse(inputJSON.get(CommonConstants.MAX_HOUR_EXCUSE).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.NO_OF_EXCUSE) != null && !inputJSON.get(CommonConstants.NO_OF_EXCUSE).toString().isEmpty()){
			 			shiftDO.setNoOfExcuse(inputJSON.get(CommonConstants.NO_OF_EXCUSE).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			shiftDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			shiftDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		shiftDO.setUpdatedon(new Date());
			 		shiftDO.setCreatedon(new Date());
			 		shiftDO.setStatus(CommonConstants.ACTIVE);
			 	}
				if(!shiftService.persist(shiftDO)){
					return CommonWebUtil.buildErrorResponse("Shift  Already Added").toString();
				}
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Shift  Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retrieveActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<ShiftDO> shiftList = shiftService.retrieveActive();
				respJSON = ShiftUtil.getShiftList(shiftList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<ShiftDO> shiftList = shiftService.retrieve();
				respJSON = ShiftUtil.getShiftList(shiftList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				ShiftDO shiftDO = new ShiftDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<ShiftDO> shiftList = shiftService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		shiftDO = shiftList.get(0);
			 		if (!inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
			 			shiftDO.setName(inputJSON.get(CommonConstants.NAME).toString());
			 		}
			 		if (!inputJSON.get(CommonConstants.STARTTIME).toString().isEmpty()){
			 			shiftDO.setStartTime(inputJSON.get(CommonConstants.STARTTIME).toString());
			 		}
			 		if (!inputJSON.get(CommonConstants.ENDTIME).toString().isEmpty()){
			 			shiftDO.setEndTime(inputJSON.get(CommonConstants.ENDTIME).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			shiftDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		shiftDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.STATUS) != null ){
			 			shiftDO.setStatus((char) inputJSON.get(CommonConstants.STATUS).toString().charAt(0));
			 		}
			 		if(!shiftService.update(shiftDO)){
						return CommonWebUtil.buildErrorResponse("Shift  Already Added").toString();
					}
			 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Shift  Updated");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/importCountry", method = RequestMethod.POST)
	public @ResponseBody String importCountry(Model model, HttpServletRequest request) {
		try {
				JSONObject inputJSON =  CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
				org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
				//JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
				List<ShiftDO> shiftlist = new ArrayList<ShiftDO>();
				Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY);
				for (int i=0; i < fileData.size(); i++){
					//JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
						ShiftDO shiftDO = new ShiftDO();
						
						if (!inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
				 			shiftDO.setName(inputJSON.get(CommonConstants.NAME).toString());
				 		}
				 		if (!inputJSON.get(CommonConstants.STARTTIME).toString().isEmpty()){
				 			shiftDO.setStartTime(inputJSON.get(CommonConstants.STARTTIME).toString());
				 		}
				 		if (!inputJSON.get(CommonConstants.ENDTIME).toString().isEmpty()){
				 			shiftDO.setEndTime(inputJSON.get(CommonConstants.ENDTIME).toString());
				 		}
				 		if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
				 			shiftDO.setUpdatedby(updatedBy.toString());
				 			shiftDO.setCreatedby(updatedBy.toString());
				 		}
				 		shiftDO.setUpdatedon(new Date());
				 		shiftDO.setCreatedon(new Date());
				 		shiftDO.setStatus(CommonConstants.ACTIVE);
				 		shiftlist.add(shiftDO);
				}
				shiftService.persistList(shiftlist);
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
