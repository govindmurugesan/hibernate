package com.spheresuite.erp.rs;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.AdvancePaymentDO;
import com.spheresuite.erp.domainobject.AdvancePaymentDetailDO;
import com.spheresuite.erp.domainobject.AdvanceTypeDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.service.AdvancePaymentDetailService;
import com.spheresuite.erp.service.AdvancePaymentService;
import com.spheresuite.erp.service.AdvanceTypeService;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.AdvancePaymentUtil;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/advancepayment")
public class AdvancePaymentRS {

	String validation = null;
	static Logger logger = Logger.getLogger(AdvancePaymentRS.class.getName());

	@Autowired
	private AdvancePaymentService advancementService;
	
	@Autowired
	private EmployeeService employeeService;
	
	@Autowired
	private AdvancePaymentDetailService advancePaymentDetailService;
	
	@Autowired
	private AdvanceTypeService advanceTypeService;
	
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				AdvancePaymentDO advancePaymentDO = new AdvancePaymentDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		List<EmployeeDO> employeeList = null;
			 		if(inputJSON.get(CommonConstants.EMPID) != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
				 		if(employeeList != null && employeeList.size() > 0){
				 			advancePaymentDO.setEmployee(employeeList.get(0));
				 		}
			 		}
			 		if(inputJSON.get(CommonConstants.TYPE_ID) != null && !inputJSON.get(CommonConstants.TYPE_ID).toString().isEmpty()){
			 			List <AdvanceTypeDO> advanceTypeList = advanceTypeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.TYPE_ID).toString()));
				 		if(advanceTypeList != null && advanceTypeList.size() > 0){
				 			advancePaymentDO.setAdvanceType(advanceTypeList.get(0));
				 		}
			 		}
			 		if(inputJSON.get(CommonConstants.AMOUNT) != null && !inputJSON.get(CommonConstants.AMOUNT).toString().isEmpty()){
			 			advancePaymentDO.setAmount(Long.parseLong(inputJSON.get(CommonConstants.AMOUNT).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.MONTHLY) != null && !inputJSON.get(CommonConstants.MONTHLY).toString().isEmpty()){
			 			advancePaymentDO.setMonth(inputJSON.get(CommonConstants.MONTHLY).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.NOOFINSTALLMENT) != null && !inputJSON.get(CommonConstants.NOOFINSTALLMENT).toString().isEmpty()){
			 			advancePaymentDO.setNoOfInstallments(inputJSON.get(CommonConstants.NOOFINSTALLMENT).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.INSTALLMENTAOUNT) != null && !inputJSON.get(CommonConstants.INSTALLMENTAOUNT).toString().isEmpty()){
			 			advancePaymentDO.setInstallmentAmount(inputJSON.get(CommonConstants.INSTALLMENTAOUNT).toString());
			 		}
			 		advancePaymentDO.setUpdatedon(new Date());
			 		advancePaymentDO.setCreatedon(new Date());
			 		advancePaymentDO.setStatus(CommonConstants.ACTIVESTRING);
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			advancePaymentDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			advancePaymentDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 	
			 		AdvancePaymentDO updatedadvancePayment=  advancementService.persist(advancePaymentDO);
			 		if(updatedadvancePayment != null){
				 		org.json.simple.JSONArray advanceDetailList = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.ADVANCEDETAIL).toString());
				 		
				 		//updatedadvancePayment.getAdvancePaymentId()
				 		
				 		for (int j=0; j < advanceDetailList.size(); j++){
		 					JSONObject monthlyDetail = CommonWebUtil.getInputParams(advanceDetailList.get(j).toString());
		 					AdvancePaymentDetailDO advancePaymentDetailDO = new AdvancePaymentDetailDO();
		 					System.out.println("monthlyDetail.get(CommonConstants.AMOUNT)"+monthlyDetail.get(CommonConstants.AMOUNT));
		 					if(monthlyDetail.get(CommonConstants.AMOUNT) != null && !monthlyDetail.get(CommonConstants.AMOUNT).toString().isEmpty()){
		 						advancePaymentDetailDO.setAmount(monthlyDetail.get(CommonConstants.AMOUNT).toString());
		 					}
		 					if(monthlyDetail.get(CommonConstants.MONTH) != null && !monthlyDetail.get(CommonConstants.MONTH).toString().isEmpty()){
		 						advancePaymentDetailDO.setMonth(monthlyDetail.get(CommonConstants.MONTH).toString());
		 					}
		 					if(employeeList != null && employeeList.size() > 0){
		 						advancePaymentDetailDO.setEmployee(employeeList.get(0));
		 					}
		 					advancePaymentDetailDO.setAdvancePayment(updatedadvancePayment);
		 					advancePaymentDetailDO.setUpdatedon(new Date());
		 					advancePaymentDetailDO.setCreatedon(new Date());
		 					advancePaymentDetailDO.setStatus(CommonConstants.ACTIVESTRING);
		 					
					 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			advancePaymentDetailDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			advancePaymentDetailDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 		}
					 		
					 		advancePaymentDetailService.persist(advancePaymentDetailDO);
				 		}
			 		}
			 	}
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Advance Payment Created For Employee");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	/*@RequestMapping(value = "/retrieveByEmpId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByEmpId(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<EmployeeBonusDO> employeeAddressList = employeeBonusService.retrieveByEmpId(inputJSON.get(CommonConstants.ID).toString());
					respJSON = EmployeeBonusUtil.getEmployeeBonusList(employeeAddressList, null,null).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}*/
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieveByEmpId(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
					List<AdvancePaymentDO> advancePayemntList = advancementService.retrieve();
					respJSON = AdvancePaymentUtil.getAdvancePaymentList(advancePayemntList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				AdvancePaymentDO advancePaymentDO = new AdvancePaymentDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<AdvancePaymentDO> advancePaymentList = advancementService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		advancePaymentDO = advancePaymentList.get(0);
			 		
			 		if(inputJSON.get(CommonConstants.EMPID) != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
				 		if(employeeList != null && employeeList.size() > 0){
				 			advancePaymentDO.setEmployee(employeeList.get(0));
				 		}
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.AMOUNT) != null && !inputJSON.get(CommonConstants.AMOUNT).toString().isEmpty()){
			 			advancePaymentDO.setAmount(Long.parseLong(inputJSON.get(CommonConstants.AMOUNT).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.MONTHLY) != null && !inputJSON.get(CommonConstants.MONTHLY).toString().isEmpty()){
			 			advancePaymentDO.setMonth(inputJSON.get(CommonConstants.MONTHLY).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.NOOFINSTALLMENT) != null && !inputJSON.get(CommonConstants.NOOFINSTALLMENT).toString().isEmpty()){
			 			advancePaymentDO.setNoOfInstallments(inputJSON.get(CommonConstants.NOOFINSTALLMENT).toString());
			 		}
			 		advancePaymentDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			advancePaymentDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		AdvancePaymentDO updatedadvancePayment = advancementService.update(advancePaymentDO);
			 		
			 		if(updatedadvancePayment != null){
				 		org.json.simple.JSONArray advanceDetailList = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.ADVANCEDETAIL).toString());
				 		
				 		//updatedadvancePayment.getAdvancePaymentId()
				 		
				 		for (int j=0; j < advanceDetailList.size(); j++){
		 					JSONObject monthlyDetail = CommonWebUtil.getInputParams(advanceDetailList.get(j).toString());
		 					AdvancePaymentDetailDO advancePaymentDetailDO = new AdvancePaymentDetailDO();
		 					
		 					if(monthlyDetail.get(CommonConstants.ID) != null && !monthlyDetail.get(CommonConstants.ID).toString().isEmpty()){
		 						List <AdvancePaymentDetailDO> detailList = advancePaymentDetailService.getById(Long.parseLong(monthlyDetail.get(CommonConstants.ID).toString()));
		 			 				if(detailList != null && detailList.size() > 0){
		 			 					advancePaymentDetailDO = detailList.get(0);
		 			 					
		 			 					if(monthlyDetail.get(CommonConstants.AMOUNT) != null && !monthlyDetail.get(CommonConstants.AMOUNT).toString().isEmpty()){
		 			 						advancePaymentDetailDO.setAmount(monthlyDetail.get(CommonConstants.AMOUNT).toString());
		 			 					}
		 			 					
		 			 					if(monthlyDetail.get(CommonConstants.MONTH) != null && !monthlyDetail.get(CommonConstants.MONTH).toString().isEmpty()){
		 			 						advancePaymentDetailDO.setMonth(monthlyDetail.get(CommonConstants.MONTH).toString());
		 			 					}
		 			 					advancePaymentDetailDO.setUpdatedon(new Date());
		 			 					
		 			 					if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
		 						 			advancePaymentDetailDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
		 						 		}
		 						 		
		 						 		advancePaymentDetailService.update(advancePaymentDetailDO);
		 			 				}
		 					}else{
		 						if(monthlyDetail.get(CommonConstants.AMOUNT) != null && !monthlyDetail.get(CommonConstants.AMOUNT).toString().isEmpty()){
			 						advancePaymentDetailDO.setAmount(monthlyDetail.get(CommonConstants.AMOUNT).toString());
			 					}
			 					if(monthlyDetail.get(CommonConstants.MONTH) != null && !monthlyDetail.get(CommonConstants.MONTH).toString().isEmpty()){
			 						advancePaymentDetailDO.setMonth(monthlyDetail.get(CommonConstants.MONTH).toString());
			 					}
			 					if(inputJSON.get(CommonConstants.EMPID) != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 						List <EmployeeDO>	employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
							 		if(employeeList != null && employeeList.size() > 0){
							 			advancePaymentDetailDO.setEmployee(employeeList.get(0));
							 		}
						 		}
			 					advancePaymentDetailDO.setAdvancePayment(updatedadvancePayment);
			 					advancePaymentDetailDO.setUpdatedon(new Date());
			 					advancePaymentDetailDO.setCreatedon(new Date());
			 					advancePaymentDetailDO.setStatus(CommonConstants.ACTIVESTRING);
			 					
						 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
						 			advancePaymentDetailDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
						 			advancePaymentDetailDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
						 		}
						 		advancePaymentDetailService.persist(advancePaymentDetailDO);
		 					}
		 					
				 		}
			 		}
			 		
			 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Advance Payemnt Updated For Employee");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	/*@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public @ResponseBody String delete(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		Boolean employeeAddressList = employeeBonusService.delete(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(employeeAddressList){
			 			CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Bonus Deleted For Employee");
			 		}else{
			 			return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retriveForBonusReport/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveForBonusReport(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.MONTHLY).toString().isEmpty()){
					Long batchId = null;
					List<Long> ids = new ArrayList<Long>();
					if(!inputJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty()){
						batchId = Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString());
						if(batchId == 0){
							batchId = null;
						}else{
							ids = employeeCtcService.retrieveByBatchId(batchId);
						}
					}
					List<EmployeeBonusDO> bonusList = employeeBonusService.retrieveByEmpIdWithDate(ids, inputJSON.get(CommonConstants.MONTHLY).toString());
					respJSON = EmployeeBonusUtil.getEmployeeBonusList(bonusList, CommonConstants.MONTHLYC, inputJSON.get(CommonConstants.MONTHLY).toString()).toString();
				}else{
					if(inputJSON != null && !inputJSON.get(CommonConstants.QUARTERLY).toString().isEmpty()){
						String fromMonth = null;
						String toMonth = null;
						Long batchId = null;
						List<FyDO> fyList = new ArrayList<FyDO>();
						if(!inputJSON.get(CommonConstants.FYID).toString().isEmpty()){
							fyList = fyService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.FYID).toString()));
							if(fyList != null && fyList.size() > 0){
								List<Long> ids = new ArrayList<Long>();
								if(!inputJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty()){
									batchId = Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString());
									if(batchId == 0){
										batchId = null;
									}else{
										ids = employeeCtcService.retrieveByBatchId(batchId);
									}
								}
								if(inputJSON.get(CommonConstants.QUARTERLY).toString().equalsIgnoreCase("Q1")){
									fromMonth = "Apr"+" "+fyList.get(0).getFromyear();
									toMonth = "Jun"+" "+fyList.get(0).getFromyear(); 
									List<EmployeeBonusDO> bonusList = employeeBonusService.retrieveByEmpIdBetweenDate(ids,fromMonth, toMonth);
									if(bonusList != null && bonusList.size() > 0){
										respJSON = EmployeeBonusUtil.MultipleMonthBonuReport(bonusList,CommonConstants.QUARTERLYC, fromMonth+" - "+toMonth).toString();
									}
								}
								
								if(inputJSON.get(CommonConstants.QUARTERLY).toString().equalsIgnoreCase("Q2")){
									fromMonth = "Jul"+" "+fyList.get(0).getFromyear();
									toMonth = "Sep"+" "+fyList.get(0).getFromyear(); 
									List<EmployeeBonusDO> bonusList = employeeBonusService.retrieveByEmpIdBetweenDate(ids,fromMonth, toMonth);
									if(bonusList != null && bonusList.size() > 0){
										respJSON = EmployeeBonusUtil.MultipleMonthBonuReport(bonusList,CommonConstants.QUARTERLYC, fromMonth+" - "+toMonth).toString();
									}
								}
								
								
								if(inputJSON.get(CommonConstants.QUARTERLY).toString().equalsIgnoreCase("Q3")){
									fromMonth = "Oct"+" "+fyList.get(0).getFromyear();
									toMonth = "Dec"+" "+fyList.get(0).getFromyear(); 
									List<EmployeeBonusDO> bonusList = employeeBonusService.retrieveByEmpIdBetweenDate(ids,fromMonth, toMonth);
									if(bonusList != null && bonusList.size() > 0){
										respJSON = EmployeeBonusUtil.MultipleMonthBonuReport(bonusList,CommonConstants.QUARTERLYC, fromMonth+" - "+toMonth).toString();
									}
								}
								if(inputJSON.get(CommonConstants.QUARTERLY).toString().equalsIgnoreCase("Q4")){
									fromMonth = "Jan"+" "+fyList.get(0).getToyear();
									toMonth = "Mar"+" "+fyList.get(0).getToyear(); 
									List<EmployeeBonusDO> bonusList = employeeBonusService.retrieveByEmpIdBetweenDate(ids,fromMonth, toMonth);
									if(bonusList != null && bonusList.size() > 0){
										respJSON = EmployeeBonusUtil.MultipleMonthBonuReport(bonusList,CommonConstants.QUARTERLYC, fromMonth+" - "+toMonth).toString();
									}
								}
							}
							
						}
					}else{
						if(inputJSON != null && !inputJSON.get(CommonConstants.FREQUENCY).toString().isEmpty()
								&& inputJSON.get(CommonConstants.FREQUENCY).toString().equalsIgnoreCase("Y")){
							String fromMonth = null;
							String toMonth = null;
							Long batchId = null;
							List<FyDO> fyList = new ArrayList<FyDO>();
							if(!inputJSON.get(CommonConstants.FYID).toString().isEmpty()){
								fyList = fyService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.FYID).toString()));
								if(fyList != null && fyList.size() > 0){
									List<Long> ids = new ArrayList<Long>();
									if(!inputJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty()){
										batchId = Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString());
										if(batchId == 0){
											batchId = null;
										}else{
											ids = employeeCtcService.retrieveByBatchId(batchId);
										}
									}
									fromMonth = "Apr"+" "+fyList.get(0).getFromyear();
									toMonth = "Mar"+" "+fyList.get(0).getToyear(); 
									List<EmployeeBonusDO> bonusList = employeeBonusService.retrieveByEmpIdBetweenDate(ids,fromMonth, toMonth);
									if(bonusList != null && bonusList.size() > 0){
										respJSON = EmployeeBonusUtil.MultipleMonthBonuReport(bonusList,CommonConstants.YEARLYC, fromMonth+" - "+toMonth).toString();
									}
								}
							}
						}
					}
				}
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}*/
}
