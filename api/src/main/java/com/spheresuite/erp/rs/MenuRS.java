package com.spheresuite.erp.rs;

import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.MenuDO;
import com.spheresuite.erp.service.MenuService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.MenuUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/menus")
public class MenuRS {
	
	static Logger logger = Logger.getLogger(MenuRS.class.getName());

	@Autowired
	private MenuService menuService;
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<MenuDO> menuList = menuService.retrieveMainMenu();
				respJSON = MenuUtil.getMenuList1(menuList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveForRole", method = RequestMethod.GET)
	public @ResponseBody String retrieveForRole(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<MenuDO> menuList = menuService.retrieveByProductId(Long.parseLong("1"));
				respJSON = MenuUtil.getMenuListforRole(menuList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveByProduct/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByProduct(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<MenuDO> menuList = menuService.retrieveByProductId(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = MenuUtil.getMenuList(menuList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveBySuperMenu/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveBySuperMenu(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<MenuDO> menuList = menuService.retrieveSuperMenu(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = MenuUtil.getMenuList(menuList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}

}
