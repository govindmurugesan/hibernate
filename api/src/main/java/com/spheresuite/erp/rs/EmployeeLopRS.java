package com.spheresuite.erp.rs;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.EmployeeLopDO;
import com.spheresuite.erp.service.EmployeeCtcService;
import com.spheresuite.erp.service.EmployeeLopService;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.FyService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.EmployeeLopUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/employeelop")
public class EmployeeLopRS {

	String validation = null;
	static Logger logger = Logger.getLogger(EmployeeLopRS.class.getName());

	@Autowired
	private EmployeeLopService employeeLopService;
	
	@Autowired
	private EmployeeCtcService employeeCtcService;
	
	@Autowired
	private FyService fyService;
	
	@Autowired
	private EmployeeService employeeService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeLopDO employeeLopDO = new EmployeeLopDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.EMPID)  != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
				 		if(employeeList != null && employeeList.size() > 0){
				 			employeeLopDO.setEmployee(employeeList.get(0));
				 		}
			 		}
			 		if(inputJSON.get(CommonConstants.STARTDATE)  != null && !inputJSON.get(CommonConstants.STARTDATE).toString().isEmpty()){
			 			employeeLopDO.setStartdate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.STARTDATE).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.LOPTYPE)  != null && !inputJSON.get(CommonConstants.LOPTYPE).toString().isEmpty()){
			 			employeeLopDO.setType(inputJSON.get(CommonConstants.LOPTYPE).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.ENDDATE) != null && !inputJSON.get(CommonConstants.ENDDATE).toString().isEmpty()){
			 			employeeLopDO.setEnddate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.ENDDATE).toString()));
			 		}
			 		employeeLopDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			employeeLopDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			employeeLopDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		employeeLopDO.setStatus(CommonConstants.ACTIVE);
			 	}
			 	List<EmployeeLopDO> employeeList = null;
			 	if(inputJSON.get(CommonConstants.ENDDATE) != null && !inputJSON.get(CommonConstants.ENDDATE).toString().isEmpty()){
			 		employeeList = employeeLopService.retrieveByEmpDate(inputJSON.get(CommonConstants.EMPID).toString(),
				 			CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.STARTDATE).toString()), CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.ENDDATE).toString()));
			 	}else{
			 		employeeList = employeeLopService.retrieveByEmpDate(inputJSON.get(CommonConstants.EMPID).toString(),
				 			CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.STARTDATE).toString()), null);
			 	}
			 	
			 	if(employeeList != null && employeeList.size() > 0){
			 		return CommonWebUtil.buildErrorResponse("LOP Already Added For This Date").toString();
			 	}else{
			 		employeeLopService.persist(employeeLopDO);
					CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Loss of Pay Created For Employee");
			 	}
				
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retrieveByEmpId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByEmpId(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<EmployeeLopDO> employeeLopList = employeeLopService.retrieveByEmpId(inputJSON.get(CommonConstants.ID).toString());
					respJSON = EmployeeLopUtil.getEmployeeLopList(employeeLopList, null,null).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieveByEmpId(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
					List<EmployeeLopDO> employeeLopList = employeeLopService.retrieve();
					respJSON = EmployeeLopUtil.getEmployeeLopList(employeeLopList, null,null).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeLopDO employeeLopDO = new EmployeeLopDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<EmployeeLopDO> employeeLopList = employeeLopService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		employeeLopDO = employeeLopList.get(0);
			 		if(inputJSON.get(CommonConstants.EMPID) != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
				 		if(employeeList != null && employeeList.size() > 0){
				 			employeeLopDO.setEmployee(employeeList.get(0));
				 		}
			 		}
			 		if(inputJSON.get(CommonConstants.STARTDATE) != null && !inputJSON.get(CommonConstants.STARTDATE).toString().isEmpty()){
			 			employeeLopDO.setStartdate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.STARTDATE).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.ENDDATE)  != null && !inputJSON.get(CommonConstants.ENDDATE).toString().isEmpty()){
			 			employeeLopDO.setEnddate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.ENDDATE).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.LOPTYPE)  != null && !inputJSON.get(CommonConstants.LOPTYPE).toString().isEmpty()){
			 			employeeLopDO.setType(inputJSON.get(CommonConstants.LOPTYPE).toString());
			 		}
			 		employeeLopDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY)  != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			employeeLopDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
			 			employeeLopDO.setStatus(inputJSON.get(CommonConstants.STATUS).toString().charAt(0));
			 		}
			 	
			 		List<EmployeeLopDO> employeeList = null;
				 	if(inputJSON.get(CommonConstants.ENDDATE) != null && !inputJSON.get(CommonConstants.ENDDATE).toString().isEmpty()){
				 		employeeList = employeeLopService.retrieveByEmpDateForUpdate(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()),
				 				inputJSON.get(CommonConstants.EMPID).toString(),
					 			CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.STARTDATE).toString()), CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.ENDDATE).toString()));
				 	}else{
				 		employeeList = employeeLopService.retrieveByEmpDateForUpdate(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()),
				 				inputJSON.get(CommonConstants.EMPID).toString(),
					 			CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.STARTDATE).toString()), null);
				 	}
				 	
			 		if(employeeList != null && employeeList.size() > 0){
				 		return CommonWebUtil.buildErrorResponse("LOP Already Added For This Date").toString();
				 	}else{
				 		employeeLopService.update(employeeLopDO);
				 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Loss of Pay Updated For Employee");
				 	}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public @ResponseBody String delete(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		Boolean lopList = employeeLopService.delete(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(lopList){
			 			CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Loss Of Pay Deleted For Employee");
			 		}else{
			 			return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/deleteByDate", method = RequestMethod.POST)
	public @ResponseBody String deleteByDate(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && inputJSON.get(CommonConstants.ID) != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty() &&
			 			inputJSON.get(CommonConstants.STARTDATE)!=null && !inputJSON.get(CommonConstants.STARTDATE).toString().isEmpty() && 
			 			inputJSON.get(CommonConstants.ENDDATE)!=null && !inputJSON.get(CommonConstants.ENDDATE).toString().isEmpty()){
			 		Boolean lopList = employeeLopService.deleteByDate(inputJSON.get(CommonConstants.ID).toString(), CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.STARTDATE).toString()),
			 															CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.ENDDATE).toString()));
			 		if(lopList){
			 			CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Loss Of Pay Deleted For Employee");
			 		}else{
			 			return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	
	/*@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public @ResponseBody String delete(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeLopDO employeeLopDO = new EmployeeLopDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<EmployeeLopDO> employeeLopList = employeeLopService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		employeeLopDO = employeeLopList.get(0);
			 		employeeLopDO.setStatus(CommonConstants.INACTIVE);
			 		employeeLopDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			employeeLopDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		employeeLopService.update(employeeLopDO);
			 		CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Allowance Type Updated");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}*/
	
/*	@RequestMapping(value = "/retriveForLopReport/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveForLopReport(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.MONTHLY).toString().isEmpty()){
					Long batchId = null;
					List<Long> ids = new ArrayList<Long>();
					if(!inputJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty()){
						batchId = Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString());
						if(batchId == 0){
							batchId = null;
						}else{
							ids = employeeCtcService.retrieveByBatchId(batchId);
						}
					}
					List<EmployeeLopDO> lopList = employeeLopService.retrieveByEmpWithDate(ids, inputJSON.get(CommonConstants.MONTHLY).toString());
					respJSON = EmployeeLopUtil.getEmployeeLopList(lopList, CommonConstants.MONTHLYC, inputJSON.get(CommonConstants.MONTHLY).toString()).toString();
				}else{
					if(inputJSON != null && !inputJSON.get(CommonConstants.QUARTERLY).toString().isEmpty()){
						String fromMonth = null;
						String toMonth = null;
						Long batchId = null;
						List<FyDO> fyList = new ArrayList<FyDO>();
						if(!inputJSON.get(CommonConstants.FYID).toString().isEmpty()){
							fyList = fyService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.FYID).toString()));
							if(fyList != null && fyList.size() > 0){
								List<Long> ids = new ArrayList<Long>();
								if(!inputJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty()){
									batchId = Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString());
									if(batchId == 0){
										batchId = null;
									}else{
										ids = employeeCtcService.retrieveByBatchId(batchId);
									}
								}
								if(inputJSON.get(CommonConstants.QUARTERLY).toString().equalsIgnoreCase("Q1")){
									fromMonth = fyList.get(0).getFromyear()+"-"+"04";
									toMonth = fyList.get(0).getFromyear()+"-"+"06"; 
									List<EmployeeLopDO> lopList = employeeLopService.retrieveByEmpIdBetweenDate(ids,fromMonth, toMonth);
									if(lopList != null && lopList.size() > 0){
										respJSON = EmployeeLopUtil.getEmployeeLopList(lopList,CommonConstants.QUARTERLYC, fromMonth+" - "+toMonth).toString();
									}
								}
								
								if(inputJSON.get(CommonConstants.QUARTERLY).toString().equalsIgnoreCase("Q2")){
									fromMonth = fyList.get(0).getFromyear()+"-"+"07";
									toMonth = fyList.get(0).getFromyear()+"-"+"09"; 
									List<EmployeeLopDO> lopList = employeeLopService.retrieveByEmpIdBetweenDate(ids,fromMonth, toMonth);
									if(lopList != null && lopList.size() > 0){
										respJSON = EmployeeLopUtil.getEmployeeLopList(lopList,CommonConstants.QUARTERLYC, fromMonth+" - "+toMonth).toString();
									}
								}
								
								if(inputJSON.get(CommonConstants.QUARTERLY).toString().equalsIgnoreCase("Q3")){
									fromMonth = fyList.get(0).getFromyear()+"-"+"10";
									toMonth = fyList.get(0).getFromyear()+"-"+"12";  
									List<EmployeeLopDO> lopList = employeeLopService.retrieveByEmpIdBetweenDate(ids,fromMonth, toMonth);
									if(lopList != null && lopList.size() > 0){
										respJSON = EmployeeLopUtil.getEmployeeLopList(lopList,CommonConstants.QUARTERLYC, fromMonth+" - "+toMonth).toString();
									}
								}
								
								if(inputJSON.get(CommonConstants.QUARTERLY).toString().equalsIgnoreCase("Q4")){
									fromMonth = fyList.get(0).getFromyear()+"-"+"01";
									toMonth = fyList.get(0).getToyear()+"-"+"03"; 
									List<EmployeeLopDO> lopList = employeeLopService.retrieveByEmpIdBetweenDate(ids,fromMonth, toMonth);
									if(lopList != null && lopList.size() > 0){
										respJSON = EmployeeLopUtil.getEmployeeLopList(lopList,CommonConstants.QUARTERLYC, fromMonth+" - "+toMonth).toString();
									}
								}
							}
							
						}
					}else{
						if(inputJSON != null && !inputJSON.get(CommonConstants.FREQUENCY).toString().isEmpty()
								&& inputJSON.get(CommonConstants.FREQUENCY).toString().equalsIgnoreCase("Y")){
							String fromMonth = null;
							String toMonth = null;
							Long batchId = null;
							List<FyDO> fyList = new ArrayList<FyDO>();
							if(!inputJSON.get(CommonConstants.FYID).toString().isEmpty()){
								fyList = fyService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.FYID).toString()));
								if(fyList != null && fyList.size() > 0){
									List<Long> ids = new ArrayList<Long>();
									if(!inputJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty()){
										batchId = Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString());
										if(batchId == 0){
											batchId = null;
										}else{
											ids = employeeCtcService.retrieveByBatchId(batchId);
										}
									}
									fromMonth = fyList.get(0).getFromyear()+"-"+"04";
									toMonth = fyList.get(0).getToyear()+"-"+"03"; 
									List<EmployeeLopDO> lopList = employeeLopService.retrieveByEmpIdBetweenDate(ids,fromMonth, toMonth);
									if(lopList != null && lopList.size() > 0){
										respJSON = EmployeeLopUtil.getEmployeeLopList(lopList,CommonConstants.YEARLYC, fromMonth+" - "+toMonth).toString();
									}
								}
							}
						}
					}
				}
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}*/
}
