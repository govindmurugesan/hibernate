package com.spheresuite.erp.rs;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.DeductionSettingDO;
import com.spheresuite.erp.domainobject.DeductionTypeDO;
import com.spheresuite.erp.service.DeductionSettingsService;
import com.spheresuite.erp.service.DeductionTypeService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.DeductionSettingsUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/deductionsettings")
public class DeductionSettingsRS {

	String validation = null;
	static Logger logger = Logger.getLogger(DeductionSettingsRS.class.getName());

	@Autowired
	private DeductionSettingsService deductionSettingsService;
	
	@Autowired
	private DeductionTypeService deductionTypeService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				DeductionSettingDO deductionSettingDO = new DeductionSettingDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		
			 		if(inputJSON.get(CommonConstants.DEDUCTION_TYPE_ID) != null && !inputJSON.get(CommonConstants.DEDUCTION_TYPE_ID).toString().isEmpty()){
				 		List<DeductionTypeDO> deductionTypeList = deductionTypeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.DEDUCTION_TYPE_ID).toString()));
				 		if(deductionTypeList != null && deductionTypeList.size() > 0){
				 			deductionSettingDO.setDeductionType(deductionTypeList.get(0));
				 		}
			 		}
			 		if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
			 			deductionSettingDO.setName(inputJSON.get(CommonConstants.NAME).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.SECTION) != null && !inputJSON.get(CommonConstants.SECTION).toString().isEmpty()){
			 			deductionSettingDO.setSection(inputJSON.get(CommonConstants.SECTION).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.PREPOST_TAX) != null && !inputJSON.get(CommonConstants.PREPOST_TAX).toString().isEmpty()){
			 			deductionSettingDO.setPreposttax(inputJSON.get(CommonConstants.PREPOST_TAX).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.TYPE) != null && !inputJSON.get(CommonConstants.TYPE).toString().isEmpty()){
			 			deductionSettingDO.setType(inputJSON.get(CommonConstants.TYPE).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.FIXEDAMOUNT) != null && !inputJSON.get(CommonConstants.FIXEDAMOUNT).toString().isEmpty()){
			 			deductionSettingDO.setFixedamount(inputJSON.get(CommonConstants.FIXEDAMOUNT).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.PERCENTAGEAMOUNT) != null && !inputJSON.get(CommonConstants.PERCENTAGEAMOUNT).toString().isEmpty()){
			 			deductionSettingDO.setPercentageamount(inputJSON.get(CommonConstants.PERCENTAGEAMOUNT).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.BASICGROSS_PAY) != null && !inputJSON.get(CommonConstants.BASICGROSS_PAY).toString().isEmpty()){
			 			deductionSettingDO.setBasicgrosspay(inputJSON.get(CommonConstants.BASICGROSS_PAY).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.NOTES) != null && !inputJSON.get(CommonConstants.NOTES).toString().isEmpty()){
			 			deductionSettingDO.setNotes(inputJSON.get(CommonConstants.NOTES).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			deductionSettingDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			deductionSettingDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		deductionSettingDO.setStatus(CommonConstants.ACTIVE);
			 		if(inputJSON.get(CommonConstants.STARTDATE) != null && !inputJSON.get(CommonConstants.STARTDATE).toString().isEmpty()){
			 			deductionSettingDO.setStartdate( CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.STARTDATE).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.ENDDATE) != null && !inputJSON.get(CommonConstants.ENDDATE).toString().isEmpty()){
			 			deductionSettingDO.setEnddate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.ENDDATE).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.DISPLAYNAME) != null && !inputJSON.get(CommonConstants.DISPLAYNAME).toString().isEmpty()){
			 			deductionSettingDO.setDisplayname(inputJSON.get(CommonConstants.DISPLAYNAME).toString());
			 		}
			 		deductionSettingDO.setUpdatedon(new Date());
			 	}
				deductionSettingsService.persist(deductionSettingDO);
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Deduction Settings Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<DeductionSettingDO> deductionSettingList = deductionSettingsService.retrieve();
				respJSON = DeductionSettingsUtil.getDeductionSettingsList(deductionSettingList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				//int i =0;
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		if(!inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
			 			List<DeductionSettingDO> deductionSettingList = deductionSettingsService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 			if(deductionSettingList != null && deductionSettingList.size() > 0){
			 				DeductionSettingDO deductionSettingDO = new DeductionSettingDO();
			 				deductionSettingDO = deductionSettingList.get(0);
			 				/*allowanceSettingDO.setStatus('i');
			 				if(allowanceSettingDO.getEnddate() == null){
			 					if(!inputJSON.get(CommonConstants.STARTDATE).toString().isEmpty()){
			 						Calendar cal = Calendar.getInstance();
			 						cal.setTime(CommonUtil.convertStringToSqlDateMonth(inputJSON.get(CommonConstants.STARTDATE).toString()));
			 						cal.add(Calendar.DATE, -1);
			 						String enddate = CommonUtil.convertDateToYearWithOutTime(allowanceSettingDO.getStartdate());
			 						Calendar cal1 = Calendar.getInstance();
			 						cal1.setTime(CommonUtil.convertStringToSqlDate(enddate));
			 						Date dateBefore1Days = cal.getTime();
			 						if(cal1.before(cal)){
			 							allowanceSettingDO.setEnddate(dateBefore1Days);
			 						}else{
			 							allowanceSettingDO.setEnddate(allowanceSettingDO.getStartdate());
			 						}
			 					}else{
			 						i++;
			 					}
			 				}
			 				
			 				if(i == 0){
			 					if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 						allowanceSettingDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
						 		}
			 					allowanceSettingDO.setUpdatedon(new Date());
			 					deductionSettingsService.update(allowanceSettingDO);
			 				}*/
			 				
			 				if(inputJSON.get(CommonConstants.DEDUCTION_TYPE_ID) != null && !inputJSON.get(CommonConstants.DEDUCTION_TYPE_ID).toString().isEmpty()){
						 		List<DeductionTypeDO> deductionTypeList = deductionTypeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.DEDUCTION_TYPE_ID).toString()));
						 		if(deductionTypeList != null && deductionTypeList.size() > 0){
						 			deductionSettingDO.setDeductionType(deductionTypeList.get(0));
						 		}
					 		}
					 		if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
					 			deductionSettingDO.setName(inputJSON.get(CommonConstants.NAME).toString());
					 		}
					 		if(inputJSON.get(CommonConstants.SECTION) != null && !inputJSON.get(CommonConstants.SECTION).toString().isEmpty()){
					 			deductionSettingDO.setSection(inputJSON.get(CommonConstants.SECTION).toString());
					 		}
					 		if(inputJSON.get(CommonConstants.PREPOST_TAX) != null && !inputJSON.get(CommonConstants.PREPOST_TAX).toString().isEmpty()){
					 			deductionSettingDO.setPreposttax(inputJSON.get(CommonConstants.PREPOST_TAX).toString());
					 		}
					 		if(inputJSON.get(CommonConstants.TYPE) != null && !inputJSON.get(CommonConstants.TYPE).toString().isEmpty()){
					 			deductionSettingDO.setType(inputJSON.get(CommonConstants.TYPE).toString());
					 		}
					 		if(inputJSON.get(CommonConstants.PREPOST_TAX) != null && !inputJSON.get(CommonConstants.PREPOST_TAX).toString().isEmpty()){
					 			deductionSettingDO.setPreposttax(inputJSON.get(CommonConstants.PREPOST_TAX).toString());
					 		}
					 		if(inputJSON.get(CommonConstants.FIXEDAMOUNT) != null && !inputJSON.get(CommonConstants.FIXEDAMOUNT).toString().isEmpty()){
					 			deductionSettingDO.setFixedamount(inputJSON.get(CommonConstants.FIXEDAMOUNT).toString());
					 		}
					 		if(inputJSON.get(CommonConstants.PERCENTAGEAMOUNT) != null && !inputJSON.get(CommonConstants.PERCENTAGEAMOUNT).toString().isEmpty()){
					 			deductionSettingDO.setPercentageamount(inputJSON.get(CommonConstants.PERCENTAGEAMOUNT).toString());
					 		}
					 		if(inputJSON.get(CommonConstants.BASICGROSS_PAY) != null && !inputJSON.get(CommonConstants.BASICGROSS_PAY).toString().isEmpty()){
					 			deductionSettingDO.setBasicgrosspay(inputJSON.get(CommonConstants.BASICGROSS_PAY).toString());
					 		}
					 		if(inputJSON.get(CommonConstants.NOTES) != null && !inputJSON.get(CommonConstants.NOTES).toString().isEmpty()){
					 			deductionSettingDO.setNotes(inputJSON.get(CommonConstants.NOTES).toString());
					 		}
					 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			deductionSettingDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 		}
					 		deductionSettingDO.setUpdatedon(new Date());
					 		deductionSettingDO.setStatus(CommonConstants.ACTIVE);
					 		if(inputJSON.get(CommonConstants.STARTDATE) != null && !inputJSON.get(CommonConstants.STARTDATE).toString().isEmpty()){
					 			deductionSettingDO.setStartdate( CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.STARTDATE).toString()));
					 		}
					 		if(inputJSON.get(CommonConstants.ENDDATE) != null && !inputJSON.get(CommonConstants.ENDDATE).toString().isEmpty()){
					 			deductionSettingDO.setEnddate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.ENDDATE).toString()));
					 		}
					 		if(inputJSON.get(CommonConstants.DISPLAYNAME) != null && !inputJSON.get(CommonConstants.DISPLAYNAME).toString().isEmpty()){
					 			deductionSettingDO.setDisplayname(inputJSON.get(CommonConstants.DISPLAYNAME).toString());
					 		}
					 		deductionSettingsService.update(deductionSettingDO);
					 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Deduction Settings Updated");
			 			}
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retriveAmount", method = RequestMethod.POST)
	public @ResponseBody String retriveById(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty() && !inputJSON.get(CommonConstants.EMPCTC).toString().isEmpty()){
					List<DeductionSettingDO> deductionSettingList = deductionSettingsService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = DeductionSettingsUtil.getDeductionSettingsListForAmount(deductionSettingList, Double.parseDouble(inputJSON.get(CommonConstants.EMPCTC).toString()),Double.parseDouble(inputJSON.get(CommonConstants.BACSICPAYLABEL).toString())).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
}
