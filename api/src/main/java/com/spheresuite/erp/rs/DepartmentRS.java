package com.spheresuite.erp.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.DepartmentDO;
import com.spheresuite.erp.domainobject.RolesDO;
import com.spheresuite.erp.domainobject.UserDO;
import com.spheresuite.erp.service.DepartmentService;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.RolesService;
import com.spheresuite.erp.service.UserService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.DepartmentUtil;
import com.spheresuite.erp.web.util.RolesUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/department")
public class DepartmentRS {

	String validation = null;
	static Logger logger = Logger.getLogger(DepartmentRS.class.getName());

	@Autowired
	private DepartmentService departmentService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private RolesService rolesService;
	
	@Autowired
	private EmployeeService employeeService;
	
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) { 
			
		try {
			if (WebManager.authenticateSession(request)) {
				DepartmentDO departmentDO = new DepartmentDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
			 			departmentDO.setName(inputJSON.get(CommonConstants.NAME).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.EMPID) != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			departmentDO.setIsManager(inputJSON.get(CommonConstants.EMPID).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.FROMTIME) != null && !inputJSON.get(CommonConstants.FROMTIME).toString().isEmpty() && inputJSON.get(CommonConstants.FROMTIME).toString().length() > 4){
			 			departmentDO.setFromTime(inputJSON.get(CommonConstants.FROMTIME).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.TOTIME) != null && !inputJSON.get(CommonConstants.TOTIME).toString().isEmpty() && inputJSON.get(CommonConstants.TOTIME).toString().length() > 4){
			 			departmentDO.setToTime(inputJSON.get(CommonConstants.TOTIME).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.MAX_HOUR_EXCUSE) != null && !inputJSON.get(CommonConstants.MAX_HOUR_EXCUSE).toString().isEmpty()){
			 			departmentDO.setMaxHrExcuse(inputJSON.get(CommonConstants.MAX_HOUR_EXCUSE).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.NO_OF_EXCUSE) != null && !inputJSON.get(CommonConstants.NO_OF_EXCUSE).toString().isEmpty()){
			 			departmentDO.setNoOfExcuse(inputJSON.get(CommonConstants.NO_OF_EXCUSE).toString());
			 		}
			 		departmentDO.setUpdatedon(new Date());
			 		departmentDO.setCreatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			departmentDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			departmentDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		departmentDO.setStatus(CommonConstants.ACTIVE);
			 	}
			 	if(!departmentService.persist(departmentDO)){
					return CommonWebUtil.buildErrorResponse("Department Already Added").toString();
				}
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Department Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retrieveActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<DepartmentDO> departmentList = departmentService.retrieveActive();
				respJSON = DepartmentUtil.getDepartmentList(departmentList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<DepartmentDO> departmentList = departmentService.retrieve();
				respJSON = DepartmentUtil.getDepartmentList(departmentList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				DepartmentDO departmentDO = new DepartmentDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<DepartmentDO> deptList = departmentService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(deptList != null && deptList.size() > 0){
				 		departmentDO = deptList.get(0);
				 		if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
				 			departmentDO.setName(inputJSON.get(CommonConstants.NAME).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.EMPID) != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
				 			departmentDO.setIsManager(inputJSON.get(CommonConstants.EMPID).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			departmentDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.FROMTIME) != null && !inputJSON.get(CommonConstants.FROMTIME).toString().isEmpty() && inputJSON.get(CommonConstants.FROMTIME).toString().length() > 4){
				 			departmentDO.setFromTime(!inputJSON.get(CommonConstants.FROMTIME).toString().isEmpty() ? inputJSON.get(CommonConstants.FROMTIME).toString() : "");
				 		}
				 		if(inputJSON.get(CommonConstants.TOTIME) != null && !inputJSON.get(CommonConstants.TOTIME).toString().isEmpty() && inputJSON.get(CommonConstants.TOTIME).toString().length() > 4){
				 			departmentDO.setToTime(!inputJSON.get(CommonConstants.TOTIME).toString().isEmpty() ? inputJSON.get(CommonConstants.TOTIME).toString() : "");
				 		}
				 		if(inputJSON.get(CommonConstants.MAX_HOUR_EXCUSE) != null && !inputJSON.get(CommonConstants.MAX_HOUR_EXCUSE).toString().isEmpty()){
				 			departmentDO.setMaxHrExcuse(inputJSON.get(CommonConstants.MAX_HOUR_EXCUSE).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.NO_OF_EXCUSE) != null && !inputJSON.get(CommonConstants.NO_OF_EXCUSE).toString().isEmpty()){
				 			departmentDO.setNoOfExcuse(inputJSON.get(CommonConstants.NO_OF_EXCUSE).toString());
				 		}
				 		departmentDO.setUpdatedon(new Date());
				 		if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
				 			departmentDO.setStatus((char) inputJSON.get(CommonConstants.STATUS).toString().charAt(0));
				 		}
				 		if(!departmentService.update(departmentDO)){
							return CommonWebUtil.buildErrorResponse("Department Already Added").toString();
						}
				 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Department Updated");
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveRoleByUser/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveRoleByUser(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
					List<UserDO> userList = userService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
					List<RolesDO> rolesList = rolesService.retriveById(userList.get(0).getRole().getRoleId());
					respJSON = RolesUtil.getRolesList(rolesList).toString();
				}
				
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/importDepartment", method = RequestMethod.POST)
	public @ResponseBody String importDepartment(Model model, HttpServletRequest request) {
		try {
			JSONObject inputJSON =  CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
			JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
			List<DepartmentDO> departmentlist = new ArrayList<DepartmentDO>();
			Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY);
			for (int i=0; i < fileData.size(); i++){
				JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
				DepartmentDO departmentDO = new DepartmentDO();
				
				if (colName.get(CommonConstants.NAME) != null && !colName.get(CommonConstants.NAME).toString().isEmpty()){
					departmentDO.setName(rowJSON.get(colName.get(CommonConstants.NAME)).toString());
		 		}
		 		/*if(colName.get(CommonConstants.EMP_NAME) != null && !colName.get(CommonConstants.EMP_NAME).toString().isEmpty()){
		 			departmentDO.setIsManager(rowJSON.get(colName.get(CommonConstants.EMP_NAME)).toString());
		 			List<EmployeeDO> employeeList = employeeService.retrieveEmpName();
		 			if(employeeList != null && employeeList.size() > 0){
						for(EmployeeDO empDO : employeeList){
							String Name = empDO.getFirstname() + " " +empDO.getMiddlename() + " " + empDO.getLastname();
							if(Name.toLowerCase().contains(rowJSON.get(colName.get(CommonConstants.REPORTTO)).toString().toLowerCase())){
								employeeDO.setReportto(empDO.getEmpId().toString());
							}
						}
					}
		 			
		 		}*/
		 		if(colName.get(CommonConstants.FROMTIME) != null && !colName.get(CommonConstants.FROMTIME).toString().isEmpty() && colName.get(CommonConstants.FROMTIME).toString().length() > 4){
		 			departmentDO.setFromTime(rowJSON.get(colName.get(CommonConstants.FROMTIME)).toString());
		 		}
		 		if(colName.get(CommonConstants.TOTIME) != null && !colName.get(CommonConstants.TOTIME).toString().isEmpty() && colName.get(CommonConstants.TOTIME).toString().length() > 4){
		 			departmentDO.setToTime(rowJSON.get(colName.get(CommonConstants.TOTIME)).toString());
		 		}
		 		if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
		 			departmentDO.setUpdatedby(updatedBy.toString());
		 			departmentDO.setCreatedby(updatedBy.toString());
		 		}
		 		departmentDO.setUpdatedon(new Date());
		 		departmentDO.setCreatedon(new Date());
		 		departmentDO.setStatus(CommonConstants.ACTIVE);
		 		departmentlist.add(departmentDO);
			}
			departmentService.persistList(departmentlist);
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
