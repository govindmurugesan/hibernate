package com.spheresuite.erp.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.EmployeeShiftDO;
import com.spheresuite.erp.domainobject.ShiftDO;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.EmployeeShiftService;
import com.spheresuite.erp.service.ShiftService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.EmployeeShiftUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/employeeshift")
public class EmployeeShiftRS {

	String validation = null;
	static Logger logger = Logger.getLogger(EmployeeShiftRS.class.getName());

	@Autowired
	private EmployeeShiftService shiftService;
	
	@Autowired
	private EmployeeService employeeService;
	
	@Autowired
	private ShiftService shiftMasterService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeShiftDO shiftDO = new EmployeeShiftDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		
			 		if (inputJSON.get(CommonConstants.FROMDATE) != null && !inputJSON.get(CommonConstants.FROMDATE).toString().isEmpty()){
			 			shiftDO.setFromDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.FROMDATE).toString()));
			 		}
			 		if (inputJSON.get(CommonConstants.TODATE) != null && !inputJSON.get(CommonConstants.TODATE).toString().isEmpty()){
			 			shiftDO.setToDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.TODATE).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.EMPID) != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retrieveEmpId(inputJSON.get(CommonConstants.EMPID).toString());
			 			if(employeeList != null && employeeList.size() > 0){
			 				shiftDO.setEmployee(employeeList.get(0));
			 			}
			 		}
			 		if(inputJSON.get(CommonConstants.SHIFT_CODE) != null && !inputJSON.get(CommonConstants.SHIFT_CODE).toString().isEmpty()){
			 			List<ShiftDO> shiftList = shiftMasterService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.SHIFT_CODE).toString()));
			 			if(shiftList != null && shiftList.size() > 0){
			 				shiftDO.setShift(shiftList.get(0));
			 			}
			 		}
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			shiftDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			shiftDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		shiftDO.setUpdatedon(new Date());
			 		shiftDO.setCreatedon(new Date());
			 	}
				if(!shiftService.persist(shiftDO)){
					return CommonWebUtil.buildErrorResponse("Shift  Already Added").toString();
				}
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Employee Shift  Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
		
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<EmployeeShiftDO> shiftList = shiftService.retrieve();
				respJSON = EmployeeShiftUtil.getShiftList(shiftList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveById/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveById(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<EmployeeShiftDO> employeeDetailList = shiftService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = EmployeeShiftUtil.getShiftList(employeeDetailList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveByEmpId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByEmpId(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<EmployeeShiftDO> employeeDetailList = shiftService.retrieveByEmpId(inputJSON.get(CommonConstants.ID).toString());
					respJSON = EmployeeShiftUtil.getShiftList(employeeDetailList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveByEmpDate", method = RequestMethod.POST)
	public @ResponseBody String retrieveByEmpDate(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if (inputJSON.get(CommonConstants.DATE) != null && !inputJSON.get(CommonConstants.DATE).toString().isEmpty()){
			 			List<EmployeeShiftDO> employeeDetailList = shiftService.retrieveByEmpDate(inputJSON.get(CommonConstants.ID).toString(), CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.DATE).toString()));
						respJSON = EmployeeShiftUtil.getShiftList(employeeDetailList).toString();
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveByDate", method = RequestMethod.POST)
	public @ResponseBody String retrieveByDate(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if (inputJSON.get(CommonConstants.DATE) != null && !inputJSON.get(CommonConstants.DATE).toString().isEmpty()){
			 			List<EmployeeShiftDO> employeeDetailList = shiftService.retrieveByDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.DATE).toString()));
						respJSON = EmployeeShiftUtil.getShiftList(employeeDetailList).toString();
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeShiftDO shiftDO = new EmployeeShiftDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<EmployeeShiftDO> employeeShiftList = shiftService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		shiftDO = employeeShiftList.get(0);
			 		if (inputJSON.get(CommonConstants.FROMDATE) != null && !inputJSON.get(CommonConstants.FROMDATE).toString().isEmpty()){
			 			shiftDO.setFromDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.FROMDATE).toString()));
			 		}
			 		if (inputJSON.get(CommonConstants.TODATE) != null && !inputJSON.get(CommonConstants.TODATE).toString().isEmpty()){
			 			shiftDO.setToDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.TODATE).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.EMPID) != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retrieveEmpId(inputJSON.get(CommonConstants.EMPID).toString());
			 			if(employeeList != null && employeeList.size() > 0){
			 				shiftDO.setEmployee(employeeList.get(0));
			 			}
			 		}
			 		if(inputJSON.get(CommonConstants.SHIFT_CODE) != null && !inputJSON.get(CommonConstants.SHIFT_CODE).toString().isEmpty()){
			 			List<ShiftDO> shiftList = shiftMasterService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.SHIFT_CODE).toString()));
			 			if(shiftList != null && shiftList.size() > 0){
			 				shiftDO.setShift(shiftList.get(0));
			 			}
			 		}
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			shiftDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		shiftDO.setUpdatedon(new Date());
			 		
			 		if(!shiftService.update(shiftDO)){
						return CommonWebUtil.buildErrorResponse("Employee Shift  Already Added").toString();
					}
			 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Employee Shift  Updated");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/importEmployeeShift", method = RequestMethod.POST)
	public @ResponseBody String importCountry(Model model, HttpServletRequest request) {
		try {
			JSONObject inputJSON =  CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
			JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
			List<EmployeeShiftDO> empShiftList = new ArrayList<EmployeeShiftDO>();
			Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY);
			for (int i=0; i < fileData.size(); i++){
				JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
				EmployeeShiftDO empShiftDO = new EmployeeShiftDO();
				if(colName.get(CommonConstants.EMPID) != null && !colName.get(CommonConstants.EMPID).toString().isEmpty()){
					if(rowJSON.get(colName.get(CommonConstants.EMPID)) != null && !rowJSON.get(colName.get(CommonConstants.EMPID)).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(rowJSON.get(colName.get(CommonConstants.EMPID)).toString());
				 		if(employeeList != null && employeeList.size() > 0){
				 			empShiftDO.setEmployee(employeeList.get(0));
					 	}
					}
				}
				if(colName.get(CommonConstants.FROMDATE) != null && !colName.get(CommonConstants.FROMDATE).toString().isEmpty()){
					if(rowJSON.get(colName.get(CommonConstants.FROMDATE)) != null && !rowJSON.get(colName.get(CommonConstants.FROMDATE)).toString().isEmpty()){
						empShiftDO.setFromDate(CommonUtil.convertStringToSqlDate(rowJSON.get(colName.get(CommonConstants.FROMDATE)).toString()));
					}
		 		}
	 			if(colName.get(CommonConstants.TODATE) != null && !colName.get(CommonConstants.TODATE).toString().isEmpty()){
	 				if(rowJSON.get(colName.get(CommonConstants.TODATE)) != null && !rowJSON.get(colName.get(CommonConstants.TODATE)).toString().isEmpty()){
						empShiftDO.setToDate(CommonUtil.convertStringToSqlDate(rowJSON.get(colName.get(CommonConstants.TODATE)).toString()));
					}
		 		}
	 			if(colName.get(CommonConstants.SHIFT_CODE) != null  && !colName.get(CommonConstants.SHIFT_CODE).toString().isEmpty()){
	 				if(rowJSON.get(colName.get(CommonConstants.SHIFT_CODE)) != null && !rowJSON.get(colName.get(CommonConstants.SHIFT_CODE)).toString().isEmpty()){
	 					List<ShiftDO> shiftList = shiftMasterService.retrieveByName(rowJSON.get(colName.get(CommonConstants.SHIFT_CODE)).toString());
				 		if(shiftList != null && shiftList.size() > 0){
				 			empShiftDO.setShift(shiftList.get(0));
					 	}
					}
	 			}
	 			if(colName.get(CommonConstants.UPDATED_BY) != null && !colName.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
	 				empShiftDO.setUpdatedby(updatedBy.toString());
	 				empShiftDO.setCreatedby(updatedBy.toString());
		 		}
		 		empShiftDO.setUpdatedon(new Date());
		 		empShiftDO.setCreatedon(new Date());
		 		empShiftList.add(empShiftDO);
			}
			shiftService.persistList(empShiftList);

		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
