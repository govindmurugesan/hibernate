package com.spheresuite.erp.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.PFDO;
import com.spheresuite.erp.service.PFService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.PFUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/pf")
public class PFRS {

	String validation = null;
	static Logger logger = Logger.getLogger(PFRS.class.getName());

	@Autowired
	private PFService pfService;
	
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				PFDO pfDO = new PFDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.EMPLOYERPERCENTAGE) != null && !inputJSON.get(CommonConstants.EMPLOYERPERCENTAGE).toString().isEmpty()){
			 			pfDO.setEmployerPercentage(Double.parseDouble(inputJSON.get(CommonConstants.EMPLOYERPERCENTAGE).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.PERCENTAGE) != null && !inputJSON.get(CommonConstants.PERCENTAGE).toString().isEmpty()){
			 			pfDO.setEmployeePercentage(Double.parseDouble(inputJSON.get(CommonConstants.PERCENTAGE).toString()));
			 		}
			 		pfDO.setUpdatedon(new Date());
			 		pfDO.setCreatedon(new Date());
			 		pfDO.setStatus(CommonConstants.ACTIVE);
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			pfDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			pfDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 	}
				if(!pfService.persist(pfDO)){
					return CommonWebUtil.buildErrorResponse("PF Percentage Already Added").toString();
				}
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "PF Percentage Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<PFDO> pflist = pfService.retrieve();
				respJSON = PFUtil.getpfList(pflist).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				PFDO pfDO = new PFDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<PFDO> pfList = pfService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(pfList != null && pfList.size() > 0){
			 			pfDO = pfList.get(0);
			 			if(inputJSON.get(CommonConstants.EMPLOYERPERCENTAGE) != null && !inputJSON.get(CommonConstants.EMPLOYERPERCENTAGE).toString().isEmpty()){
				 			pfDO.setEmployerPercentage(Double.parseDouble(inputJSON.get(CommonConstants.EMPLOYERPERCENTAGE).toString()));
				 		}
				 		if(inputJSON.get(CommonConstants.PERCENTAGE) != null && !inputJSON.get(CommonConstants.PERCENTAGE).toString().isEmpty()){
				 			pfDO.setEmployeePercentage(Double.parseDouble(inputJSON.get(CommonConstants.PERCENTAGE).toString()));
				 		}
				 		if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
				 			pfDO.setStatus((char) (inputJSON.get(CommonConstants.STATUS).toString().charAt(0)));
				 		}
				 		pfDO.setUpdatedon(new Date());
				 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			pfDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		if(!pfService.update(pfDO)){
							return CommonWebUtil.buildErrorResponse("PF Percentage Already Added").toString();
						}
				 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "PF Percentage Updated");
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/importEducationCess", method = RequestMethod.POST)
	public @ResponseBody String importEducationCess(Model model, HttpServletRequest request) {
		try {
			JSONObject inputJSON =  CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
			JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
			List<PFDO> pfList = new ArrayList<PFDO>();
			Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY);
			for (int i=0; i < fileData.size(); i++){
				JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
				PFDO pfDO = new PFDO();
				
				if(colName.get(CommonConstants.EMPLOYERPERCENTAGE) != null && !colName.get(CommonConstants.EMPLOYERPERCENTAGE).toString().isEmpty()){
		 			pfDO.setEmployerPercentage(Double.parseDouble(rowJSON.get(colName.get(CommonConstants.EMPLOYERPERCENTAGE)).toString()));
		 		}
		 		if(colName.get(CommonConstants.PERCENTAGE) != null && !colName.get(CommonConstants.PERCENTAGE).toString().isEmpty()){
		 			pfDO.setEmployeePercentage(Double.parseDouble(rowJSON.get(colName.get(CommonConstants.PERCENTAGE)).toString()));
		 		}
		 		if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
		 			pfDO.setUpdatedby(updatedBy.toString());
		 			pfDO.setCreatedby(updatedBy.toString());
		 		}
		 		pfDO.setUpdatedon(new Date());
		 		pfDO.setCreatedon(new Date());
		 		pfDO.setStatus(CommonConstants.ACTIVE);
		 		pfList.add(pfDO);
			}
			pfService.persistList(pfList);
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
