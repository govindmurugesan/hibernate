package com.spheresuite.erp.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.EmployeeContactInfoDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.service.EmployeeContactInfoService;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.EmployeeContactInfoUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/employeecontactinfo")
public class EmployeeContactInfoRS {

	String validation = null;
	static Logger logger = Logger.getLogger(EmployeeContactInfoRS.class.getName());

	@Autowired
	private EmployeeContactInfoService employeeContactInfoService;
	
	@Autowired
	private EmployeeService employeeService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeContactInfoDO employeeContactInfoDO = new EmployeeContactInfoDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.HOMEPHONE).toString() != null && !inputJSON.get(CommonConstants.HOMEPHONE).toString().isEmpty()){
			 			employeeContactInfoDO.setHomePhone(Long.parseLong(inputJSON.get(CommonConstants.HOMEPHONE).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.CELLPHONE).toString() != null && !inputJSON.get(CommonConstants.CELLPHONE).toString().isEmpty()){
			 			employeeContactInfoDO.setCellPhone(Long.parseLong(inputJSON.get(CommonConstants.CELLPHONE).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.WORKPHONE).toString() != null && !inputJSON.get(CommonConstants.WORKPHONE).toString().isEmpty()){
			 			employeeContactInfoDO.setWorkPhone(Long.parseLong(inputJSON.get(CommonConstants.WORKPHONE).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.COMPANYEMAIL).toString() != null && !inputJSON.get(CommonConstants.COMPANYEMAIL).toString().isEmpty()){
			 			employeeContactInfoDO.setCompanyEmail(inputJSON.get(CommonConstants.COMPANYEMAIL).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.PERSONALEMAIL).toString() != null && !inputJSON.get(CommonConstants.PERSONALEMAIL).toString().isEmpty()){
			 			employeeContactInfoDO.setPersonalEmail(inputJSON.get(CommonConstants.PERSONALEMAIL).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.OTHEREMAIL).toString() != null && !inputJSON.get(CommonConstants.OTHEREMAIL).toString().isEmpty()){
			 			employeeContactInfoDO.setOtherEmail(inputJSON.get(CommonConstants.OTHEREMAIL).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.EMPID).toString() != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
				 		if(employeeList != null && employeeList.size() > 0){
				 			employeeContactInfoDO.setEmployee(employeeList.get(0));
				 		}
			 		}
			 		employeeContactInfoDO.setUpdatedon(new Date());
			 		employeeContactInfoDO.setCreatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			employeeContactInfoDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			employeeContactInfoDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 	}
				employeeContactInfoService.persist(employeeContactInfoDO);
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Contact Information Created For Employee");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveByEmpId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByEmpId(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<EmployeeContactInfoDO> employeeContactInfoList = employeeContactInfoService.retrieveByEmpId(inputJSON.get(CommonConstants.ID).toString());
					respJSON = EmployeeContactInfoUtil.getEmployeeContactInfoList(employeeContactInfoList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeContactInfoDO employeeContactInfoDO = new EmployeeContactInfoDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<EmployeeContactInfoDO> employeeContactInfoList = employeeContactInfoService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		employeeContactInfoDO = employeeContactInfoList.get(0);
			 		if(inputJSON.get(CommonConstants.HOMEPHONE).toString() != null && !inputJSON.get(CommonConstants.HOMEPHONE).toString().isEmpty()){
			 			employeeContactInfoDO.setHomePhone(Long.parseLong(inputJSON.get(CommonConstants.HOMEPHONE).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.CELLPHONE).toString() != null && !inputJSON.get(CommonConstants.CELLPHONE).toString().isEmpty()){
			 			employeeContactInfoDO.setCellPhone(Long.parseLong(inputJSON.get(CommonConstants.CELLPHONE).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.WORKPHONE).toString() != null && !inputJSON.get(CommonConstants.WORKPHONE).toString().isEmpty()){
			 			employeeContactInfoDO.setWorkPhone(Long.parseLong(inputJSON.get(CommonConstants.WORKPHONE).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.COMPANYEMAIL).toString() != null && !inputJSON.get(CommonConstants.COMPANYEMAIL).toString().isEmpty()){
			 			employeeContactInfoDO.setCompanyEmail(inputJSON.get(CommonConstants.COMPANYEMAIL).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.PERSONALEMAIL).toString() != null && !inputJSON.get(CommonConstants.PERSONALEMAIL).toString().isEmpty()){
			 			employeeContactInfoDO.setPersonalEmail(inputJSON.get(CommonConstants.PERSONALEMAIL).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.OTHEREMAIL).toString() != null && !inputJSON.get(CommonConstants.OTHEREMAIL).toString().isEmpty()){
			 			employeeContactInfoDO.setOtherEmail(inputJSON.get(CommonConstants.OTHEREMAIL).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.EMPID).toString() != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
				 		if(employeeList != null && employeeList.size() > 0){
				 			employeeContactInfoDO.setEmployee(employeeList.get(0));
				 		}
			 		}
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			employeeContactInfoDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		employeeContactInfoDO.setUpdatedon(new Date());
			 		employeeContactInfoService.update(employeeContactInfoDO);
			 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Contact Information Updated For Employee");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/importEmployeeContactInfo", method = RequestMethod.POST)
	public @ResponseBody String importCountry(Model model, HttpServletRequest request) {
		try {
				JSONObject inputJSON =  CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
				org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
				JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
				List<EmployeeContactInfoDO> empContactlist = new ArrayList<EmployeeContactInfoDO>();
				Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY);
				for (int i=0; i < fileData.size(); i++){
					JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
					EmployeeContactInfoDO employeeContactInfoDO = new EmployeeContactInfoDO();
					
					if(colName.get(CommonConstants.EMPID).toString() != null && !colName.get(CommonConstants.EMPID).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(rowJSON.get(colName.get(CommonConstants.EMPID)).toString());
				 		if(employeeList != null && employeeList.size() > 0){
				 			employeeContactInfoDO.setEmployee(employeeList.get(0));
				 		}
				 		
				 		if (colName.get(CommonConstants.HOMEPHONE) != null && !colName.get(CommonConstants.HOMEPHONE).toString().isEmpty()){
							employeeContactInfoDO.setHomePhone(Long.parseLong(rowJSON.get(colName.get(CommonConstants.HOMEPHONE)).toString()));
				 		}
				 		if (colName.get(CommonConstants.CELLPHONE) != null && !colName.get(CommonConstants.CELLPHONE).toString().isEmpty()){
				 			employeeContactInfoDO.setCellPhone(Long.parseLong(rowJSON.get(colName.get(CommonConstants.CELLPHONE)).toString()));
				 		}
				 		if (colName.get(CommonConstants.WORKPHONE) != null && !colName.get(CommonConstants.WORKPHONE).toString().isEmpty()){
				 			employeeContactInfoDO.setWorkPhone(Long.parseLong(rowJSON.get(colName.get(CommonConstants.WORKPHONE)).toString()));
				 		}
				 		if (colName.get(CommonConstants.COMPANYEMAIL) != null && !colName.get(CommonConstants.COMPANYEMAIL).toString().isEmpty()){
				 			employeeContactInfoDO.setCompanyEmail(rowJSON.get(colName.get(CommonConstants.COMPANYEMAIL)).toString());
				 		}
				 		
				 		if (colName.get(CommonConstants.PERSONALEMAIL) != null && !colName.get(CommonConstants.PERSONALEMAIL).toString().isEmpty()){
				 			employeeContactInfoDO.setPersonalEmail(rowJSON.get(colName.get(CommonConstants.PERSONALEMAIL)).toString());
				 		}
				 		
				 		if (colName.get(CommonConstants.OTHEREMAIL) != null && !colName.get(CommonConstants.OTHEREMAIL).toString().isEmpty()){
				 			employeeContactInfoDO.setOtherEmail(rowJSON.get(colName.get(CommonConstants.OTHEREMAIL)).toString());
				 		}
				 		
				 		if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
				 			employeeContactInfoDO.setUpdatedby(updatedBy.toString());
				 			employeeContactInfoDO.setCreatedby(updatedBy.toString());
				 		}
				 		employeeContactInfoDO.setUpdatedon(new Date());
				 		employeeContactInfoDO.setCreatedon(new Date());
				 		empContactlist.add(employeeContactInfoDO);
			 		}
				}
				employeeContactInfoService.persistList(empContactlist);
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
