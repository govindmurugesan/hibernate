package com.spheresuite.erp.rs;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.AllowanceSettingDO;
import com.spheresuite.erp.domainobject.EarningTypeDO;
import com.spheresuite.erp.service.AllowanceSettingsService;
import com.spheresuite.erp.service.EarningTypeService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.AllowanceSettingsUtil;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/allowancesettings")
public class AllowanceSettingsRS {

	String validation = null;
	static Logger logger = Logger.getLogger(AllowanceSettingsRS.class.getName());
	
	@Autowired
	private AllowanceSettingsService allowanceSettingsService;
	
	@Autowired
	private EarningTypeService earningTypeService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				AllowanceSettingDO allowanceSettingDO = new AllowanceSettingDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.ALLOWANCE_TYPE_ID) != null && !inputJSON.get(CommonConstants.ALLOWANCE_TYPE_ID).toString().isEmpty()){
			 			List<EarningTypeDO> earningTypeList = earningTypeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ALLOWANCE_TYPE_ID).toString()));
				 		if(earningTypeList != null && earningTypeList.size() >0){
				 			allowanceSettingDO.setEarningType(earningTypeList.get(0));
				 		}
			 		}
			 		if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
			 			allowanceSettingDO.setName(inputJSON.get(CommonConstants.NAME).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.TAXABLE) != null && !inputJSON.get(CommonConstants.TAXABLE).toString().isEmpty()){
			 			allowanceSettingDO.setTaxable(inputJSON.get(CommonConstants.TAXABLE).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.SECTION) != null && !inputJSON.get(CommonConstants.SECTION).toString().isEmpty()){
			 			allowanceSettingDO.setSection(inputJSON.get(CommonConstants.SECTION).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.NOTES) != null && !inputJSON.get(CommonConstants.NOTES).toString().isEmpty()){
			 			allowanceSettingDO.setNotes(inputJSON.get(CommonConstants.NOTES).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.STARTDATE) != null && !inputJSON.get(CommonConstants.STARTDATE).toString().isEmpty()){
			 			allowanceSettingDO.setStartdate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.STARTDATE).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.ENDDATE) != null && !inputJSON.get(CommonConstants.ENDDATE).toString().isEmpty()){
				 		allowanceSettingDO.setEnddate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.ENDDATE).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.TYPE) != null && !inputJSON.get(CommonConstants.TYPE).toString().isEmpty()){
			 			allowanceSettingDO.setType(inputJSON.get(CommonConstants.TYPE).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.PERCENTAGEAMOUNT) != null && !inputJSON.get(CommonConstants.PERCENTAGEAMOUNT).toString().isEmpty()){
			 			allowanceSettingDO.setPercentageamount(inputJSON.get(CommonConstants.PERCENTAGEAMOUNT).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.FIXEDAMOUNT) != null && !inputJSON.get(CommonConstants.FIXEDAMOUNT).toString().isEmpty()){
			 			allowanceSettingDO.setFixedamount(inputJSON.get(CommonConstants.FIXEDAMOUNT).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.BASICGROSS_PAY) != null && !inputJSON.get(CommonConstants.BASICGROSS_PAY).toString().isEmpty()){
			 			allowanceSettingDO.setBasicgrosspay(inputJSON.get(CommonConstants.BASICGROSS_PAY).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.DISPLAYNAME) != null && !inputJSON.get(CommonConstants.DISPLAYNAME).toString().isEmpty()){
			 			allowanceSettingDO.setDisplayname(inputJSON.get(CommonConstants.DISPLAYNAME).toString());
			 		}
			 		allowanceSettingDO.setStatus(CommonConstants.ACTIVE);
			 		if(inputJSON.get(CommonConstants.UPDATED_BY)!= null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			allowanceSettingDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			allowanceSettingDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		allowanceSettingDO.setUpdatedon(new Date());
			 		allowanceSettingDO.setCreatedon(new Date());
			 	}
				allowanceSettingsService.persist(allowanceSettingDO);
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Allowance Settings Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<AllowanceSettingDO> allowanceSettingList = allowanceSettingsService.retrieve();
				respJSON = AllowanceSettingsUtil.getAllowanceSettingsList(allowanceSettingList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
				//int i =0;
			 	if (inputJSON != null){
			 		if(!inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
			 			List<AllowanceSettingDO> allowanceSettingList = allowanceSettingsService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 			if(allowanceSettingList != null && allowanceSettingList.size() > 0){
			 				AllowanceSettingDO allowanceSetting = new AllowanceSettingDO();
			 				allowanceSetting = allowanceSettingList.get(0);
			 				/*allowanceSettingDO.setStatus('i');
			 				if(allowanceSettingDO.getEnddate() == null){
			 					if(!inputJSON.get(CommonConstants.STARTDATE).toString().isEmpty()){
			 						Calendar cal = Calendar.getInstance();
			 						cal.setTime(CommonUtil.convertStringToSqlDateMonth(inputJSON.get(CommonConstants.STARTDATE).toString()));
			 						cal.add(Calendar.DATE, -1);
			 						String enddate = CommonUtil.convertDateToYearWithOutTime(allowanceSettingDO.getStartdate());
			 						Calendar cal1 = Calendar.getInstance();
			 						cal1.setTime(CommonUtil.convertStringToSqlDate(enddate));
			 						Date dateBefore1Days = cal.getTime();
			 						if(cal1.before(cal)){
			 							allowanceSettingDO.setEnddate(dateBefore1Days);
			 						}else{
			 							allowanceSettingDO.setEnddate(allowanceSettingDO.getStartdate());
			 						}
			 					}else{
			 						i++;
			 					}
			 				}
			 				
			 				if(i == 0){
			 					allowanceSettingDO.setUpdatedon(new Date());
			 					allowanceSettingsService.update(allowanceSettingDO);
			 				}*/
			 				
			 				
			 				if(inputJSON.get(CommonConstants.ALLOWANCE_TYPE_ID) != null && !inputJSON.get(CommonConstants.ALLOWANCE_TYPE_ID).toString().isEmpty()){
					 			List<EarningTypeDO> earningTypeList = earningTypeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ALLOWANCE_TYPE_ID).toString()));
						 		if(earningTypeList != null && earningTypeList.size() >0){
						 			allowanceSetting.setEarningType(earningTypeList.get(0));
						 		}
					 		}
					 		if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
					 			allowanceSetting.setName(inputJSON.get(CommonConstants.NAME).toString());
					 		}
					 		if(inputJSON.get(CommonConstants.TAXABLE) != null && !inputJSON.get(CommonConstants.TAXABLE).toString().isEmpty()){
					 			allowanceSetting.setTaxable(inputJSON.get(CommonConstants.TAXABLE).toString());
					 		}
					 		if(inputJSON.get(CommonConstants.SECTION) != null && !inputJSON.get(CommonConstants.SECTION).toString().isEmpty()){
					 			allowanceSetting.setSection(inputJSON.get(CommonConstants.SECTION).toString());
					 		}
					 		if(inputJSON.get(CommonConstants.NOTES) != null && !inputJSON.get(CommonConstants.NOTES).toString().isEmpty()){
					 			allowanceSetting.setNotes(inputJSON.get(CommonConstants.NOTES).toString());
					 		}
					 		if(inputJSON.get(CommonConstants.STARTDATE) != null && !inputJSON.get(CommonConstants.STARTDATE).toString().isEmpty()){
					 			allowanceSetting.setStartdate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.STARTDATE).toString()));
					 		}
					 		if(inputJSON.get(CommonConstants.ENDDATE) != null && !inputJSON.get(CommonConstants.ENDDATE).toString().isEmpty()){
					 			allowanceSetting.setEnddate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.ENDDATE).toString()));
					 		}
					 		if(inputJSON.get(CommonConstants.TYPE) != null && !inputJSON.get(CommonConstants.TYPE).toString().isEmpty()){
					 			allowanceSetting.setType(inputJSON.get(CommonConstants.TYPE).toString());
					 		}
					 		if(inputJSON.get(CommonConstants.PERCENTAGEAMOUNT) != null && !inputJSON.get(CommonConstants.PERCENTAGEAMOUNT).toString().isEmpty()){
					 			allowanceSetting.setPercentageamount(inputJSON.get(CommonConstants.PERCENTAGEAMOUNT).toString());
					 		}
					 		if(inputJSON.get(CommonConstants.FIXEDAMOUNT) != null && !inputJSON.get(CommonConstants.FIXEDAMOUNT).toString().isEmpty()){
					 			allowanceSetting.setFixedamount(inputJSON.get(CommonConstants.FIXEDAMOUNT).toString());
					 		}
					 		if(inputJSON.get(CommonConstants.BASICGROSS_PAY) != null && !inputJSON.get(CommonConstants.BASICGROSS_PAY).toString().isEmpty()){
					 			allowanceSetting.setBasicgrosspay(inputJSON.get(CommonConstants.BASICGROSS_PAY).toString());
					 		}
					 		if(inputJSON.get(CommonConstants.DISPLAYNAME) != null && !inputJSON.get(CommonConstants.DISPLAYNAME).toString().isEmpty()){
					 			allowanceSetting.setDisplayname(inputJSON.get(CommonConstants.DISPLAYNAME).toString());
					 		}
					 		if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
					 			allowanceSetting.setStatus((char) inputJSON.get(CommonConstants.STATUS).toString().charAt(0));
					 		}
					 		if(inputJSON.get(CommonConstants.UPDATED_BY)!= null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			allowanceSetting.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			allowanceSetting.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 		}
					 		allowanceSetting.setUpdatedon(new Date());
					 		allowanceSettingsService.update(allowanceSetting);
					 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Allowance Settings Updated");
			 			}
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retriveAmount", method = RequestMethod.POST)
	public @ResponseBody String retriveById(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty() && !inputJSON.get(CommonConstants.EMPCTC).toString().isEmpty()){
					
					List<AllowanceSettingDO> allowanceSettingList = allowanceSettingsService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = AllowanceSettingsUtil.getAllowanceSettingsListForAmount(allowanceSettingList, Double.parseDouble(inputJSON.get(CommonConstants.EMPCTC).toString())).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retriveAmountWithDate", method = RequestMethod.POST)
	public @ResponseBody String retriveByIdWithDate(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty() && !inputJSON.get(CommonConstants.EMPCTC).toString().isEmpty()){
					List<AllowanceSettingDO> allowanceSettingList = allowanceSettingsService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = AllowanceSettingsUtil.getAllowanceSettingsListForAmount(allowanceSettingList, Double.parseDouble(inputJSON.get(CommonConstants.EMPCTC).toString())).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
}
