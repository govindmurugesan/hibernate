
package com.spheresuite.erp.rs;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.ItSavingsDO;
import com.spheresuite.erp.domainobject.ItSavingsDocDO;
import com.spheresuite.erp.domainobject.ItSavingsSettingsDO;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.ItSavingDocService;
import com.spheresuite.erp.service.ItSavingsService;
import com.spheresuite.erp.service.ItSavingsSettingsService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.ItSavingsUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/itsavings")
public class ItSavingsRS {

	String validation = null;
	static Logger logger = Logger.getLogger(ItSavingsRS.class.getName());

	@Autowired
	private ItSavingsService itSavingsService;
	
	@Autowired
	private ItSavingDocService itSavingDocService;
	
	@Autowired
	private EmployeeService employeeService;
	
	@Autowired
	private ItSavingsSettingsService itSavingsSettingsService;
	
	
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		ItSavingsDO itSavingsDO = new ItSavingsDO();
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
		 			if(inputJSON.get(CommonConstants.EMPID) != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
				 		if(employeeList != null && employeeList.size() > 0){
				 			itSavingsDO.setEmployee(employeeList.get(0));
				 		}
			 		}
		 			if(inputJSON.get(CommonConstants.ITSAVINGSETTINGID) != null && !inputJSON.get(CommonConstants.ITSAVINGSETTINGID).toString().isEmpty()){
		 				List<ItSavingsSettingsDO> itSavingsSettingsList = itSavingsSettingsService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ITSAVINGSETTINGID).toString()));
				 		if(itSavingsSettingsList != null && itSavingsSettingsList.size() > 0){
				 			itSavingsDO.setItSavingsSettings(itSavingsSettingsList.get(0));
				 		}
		 			}
		 			if(inputJSON.get(CommonConstants.FROMDATE) != null && !inputJSON.get(CommonConstants.FROMDATE).toString().isEmpty()){
		 				itSavingsDO.setFromYear(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.FROMDATE).toString()));
		 			}
		 			if(inputJSON.get(CommonConstants.TODATE) != null && !inputJSON.get(CommonConstants.TODATE).toString().isEmpty()){
		 				itSavingsDO.setToYear(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.TODATE).toString()));
		 			}
		 			if(inputJSON.get(CommonConstants.AMOUNT) != null && !inputJSON.get(CommonConstants.AMOUNT).toString().isEmpty()){
		 				itSavingsDO.setAmount(Double.parseDouble(inputJSON.get(CommonConstants.AMOUNT).toString()));
		 			}
		 			if(inputJSON.get(CommonConstants.COMMENT) != null && !inputJSON.get(CommonConstants.COMMENT).toString().isEmpty()){
		 				itSavingsDO.setComments(inputJSON.get(CommonConstants.COMMENT).toString());
		 			}
		 			itSavingsDO.setUpdatedon(new Date());
		 			itSavingsDO.setCreatedon(new Date());
		 			if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			itSavingsDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			itSavingsDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		itSavingsDO.setStatus(CommonConstants.SUBMITTED);
			 	}
				itSavingsService.persist(itSavingsDO);
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New IT saving  Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponseId(itSavingsDO.getItSavingsId().toString()).toString();
	}
	
	
	@RequestMapping(value = "/retrieveByEmpId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				List<ItSavingsDO> itSavingsList = itSavingsService.retrieveByEmpId(inputJSON.get(CommonConstants.ID).toString());
				respJSON = ItSavingsUtil.getITSavingsSettingsList(itSavingsList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<ItSavingsDO> itSavingsList = itSavingsService.retrieve();
				respJSON = ItSavingsUtil.getITSavingsSettingsList(itSavingsList).toString(); 
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		ItSavingsDO itSavingsDO = new ItSavingsDO();
		try {
			if (WebManager.authenticateSession(request)) {
				
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<ItSavingsDO> itSavingsList = itSavingsService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		itSavingsDO = itSavingsList.get(0);
			 		
			 		if(inputJSON.get(CommonConstants.EMPID) != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
				 		if(employeeList != null && employeeList.size() > 0){
				 			itSavingsDO.setEmployee(employeeList.get(0));
				 		}
			 		}
		 			if(inputJSON.get(CommonConstants.ITSAVINGSETTINGID) != null && !inputJSON.get(CommonConstants.ITSAVINGSETTINGID).toString().isEmpty()){
		 				List<ItSavingsSettingsDO> itSavingsSettingsList = itSavingsSettingsService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ITSAVINGSETTINGID).toString()));
				 		if(itSavingsSettingsList != null && itSavingsSettingsList.size() > 0){
				 			itSavingsDO.setItSavingsSettings(itSavingsSettingsList.get(0));
				 		}
		 			}
		 			if(inputJSON.get(CommonConstants.FROMDATE) != null && !inputJSON.get(CommonConstants.FROMDATE).toString().isEmpty()){
		 				itSavingsDO.setFromYear(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.FROMDATE).toString()));
		 			}
		 			if(inputJSON.get(CommonConstants.TODATE) != null && !inputJSON.get(CommonConstants.TODATE).toString().isEmpty()){
		 				itSavingsDO.setToYear(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.TODATE).toString()));
		 			}
		 			if(inputJSON.get(CommonConstants.AMOUNT) != null && !inputJSON.get(CommonConstants.AMOUNT).toString().isEmpty()){
		 				itSavingsDO.setAmount(Double.parseDouble(inputJSON.get(CommonConstants.AMOUNT).toString()));
		 			}
		 			if(inputJSON.get(CommonConstants.COMMENT) != null && !inputJSON.get(CommonConstants.COMMENT).toString().isEmpty()){
		 				itSavingsDO.setComments(inputJSON.get(CommonConstants.COMMENT).toString());
		 			}
		 			if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
		 				itSavingsDO.setStatus((char) (inputJSON.get(CommonConstants.STATUS).toString().charAt(0)));
		 			}
			 		itSavingsService.update(itSavingsDO);
			 		//itSavingDocService.delete(itSavingsDO.getId());
			 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "IT savings Updated");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponseId(itSavingsDO.getItSavingsId().toString()).toString();
	}
	
	@RequestMapping(value = "/updatepic", method = RequestMethod.POST)
	public @ResponseBody String updatepic(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				ItSavingsDocDO itsavingdoc = new ItSavingsDocDO();
				/*JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("data").toString());*/
			 	if (request.getParameter(CommonConstants.FILE) != null && request.getParameter(CommonConstants.ID) != null){
			 		itsavingdoc.setItsavingId(Long.parseLong(request.getParameter(CommonConstants.ID)));
			 		itsavingdoc.setPhoto(request.getParameter(CommonConstants.FILE));
			 		itsavingdoc.setFileName(request.getParameter(CommonConstants.NAME));
			 		itSavingDocService.persist(itsavingdoc);
 			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
