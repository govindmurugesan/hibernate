package com.spheresuite.erp.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.TaxSlabsSettingsDO;
import com.spheresuite.erp.service.TaxSlabsSettingsService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.TaxSlabsSettingsUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/taxslabssettings")
public class TaxSlabsSettingsRS {

	String validation = null;
	static Logger logger = Logger.getLogger(TaxSlabsSettingsRS.class.getName());

	@Autowired
	private TaxSlabsSettingsService taxSlabsSettingsService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				TaxSlabsSettingsDO taxSlabsSettingsDO = new TaxSlabsSettingsDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.FROMAMOUNT) != null && !inputJSON.get(CommonConstants.FROMAMOUNT).toString().isEmpty()){
			 			taxSlabsSettingsDO.setAmountfrom(Long.parseLong(inputJSON.get(CommonConstants.FROMAMOUNT).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.TOAMOUNT) != null && !inputJSON.get(CommonConstants.TOAMOUNT).toString().isEmpty()){
			 			taxSlabsSettingsDO.setAmountto(Long.parseLong(inputJSON.get(CommonConstants.TOAMOUNT).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.PERCENTAGEAMOUNT) != null && !inputJSON.get(CommonConstants.PERCENTAGEAMOUNT).toString().isEmpty()){
			 			taxSlabsSettingsDO.setPercentageamount(Double.parseDouble(inputJSON.get(CommonConstants.PERCENTAGEAMOUNT).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.FROMAGE) != null && !inputJSON.get(CommonConstants.FROMAGE).toString().isEmpty()){
			 			taxSlabsSettingsDO.setEmpAgeFrom(Long.parseLong(inputJSON.get(CommonConstants.FROMAGE).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.TOAGE) != null && !inputJSON.get(CommonConstants.TOAGE).toString().isEmpty()){
			 			taxSlabsSettingsDO.setEmpAgeTo(Long.parseLong(inputJSON.get(CommonConstants.TOAGE).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.STARTDATE) != null && !inputJSON.get(CommonConstants.STARTDATE).toString().isEmpty()){
			 			taxSlabsSettingsDO.setStartdate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.STARTDATE).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.ENDDATE) != null && !inputJSON.get(CommonConstants.ENDDATE).toString().isEmpty()){
			 			taxSlabsSettingsDO.setEnddate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.ENDDATE).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			taxSlabsSettingsDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			taxSlabsSettingsDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		taxSlabsSettingsDO.setStatus(CommonConstants.ACTIVE);
			 		taxSlabsSettingsDO.setUpdatedon(new Date());
			 		taxSlabsSettingsDO.setCreatedon(new Date());
			 	}
				taxSlabsSettingsService.persist(taxSlabsSettingsDO);
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Tax Slab Settings Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<TaxSlabsSettingsDO> taxSlabSettingList = taxSlabsSettingsService.retrieve();
				respJSON = TaxSlabsSettingsUtil.getTaxSettingsList(taxSlabSettingList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	/*@RequestMapping(value = "/retrieveActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<TaxSlabsSettingsDO> adeductionSettingList = taxSlabsSettingsService.retrieveActive();
				respJSON = TaxSlabsSettingsUtil.getTaxSettingsList(adeductionSettingList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}*/
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(!inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 			List<TaxSlabsSettingsDO> taxSlabSettingList = taxSlabsSettingsService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 			if(taxSlabSettingList != null && taxSlabSettingList.size() > 0){
			 				TaxSlabsSettingsDO taxSlab = new TaxSlabsSettingsDO();
			 				taxSlab =taxSlabSettingList.get(0);
			 				/*taxSlabsSettingsDO.setStatus('i');
			 				if(taxSlabsSettingsDO.getEnddate() == null){
			 					if(!inputJSON.get(CommonConstants.STARTDATE).toString().isEmpty()){
			 						taxSlabsSettingsDO.setEnddate(inputJSON.get(CommonConstants.STARTDATE).toString());
			 					}
			 				}
		 					if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			taxSlabsSettingsDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			taxSlabsSettingsDO.setUpdatedon(new Date());
					 			taxSlabsSettingsService.update(taxSlabsSettingsDO);
					 		}
			 				TaxSlabsSettingsDO taxSetting = new TaxSlabsSettingsDO();*/
			 				if(inputJSON.get(CommonConstants.FROMAMOUNT) != null && !inputJSON.get(CommonConstants.FROMAMOUNT).toString().isEmpty()){
			 					taxSlab.setAmountfrom(Long.parseLong(inputJSON.get(CommonConstants.FROMAMOUNT).toString()));
					 		}
					 		if(inputJSON.get(CommonConstants.TOAMOUNT) != null && !inputJSON.get(CommonConstants.TOAMOUNT).toString().isEmpty()){
					 			taxSlab.setAmountto(Long.parseLong(inputJSON.get(CommonConstants.TOAMOUNT).toString()));
					 		}
					 		if(inputJSON.get(CommonConstants.PERCENTAGEAMOUNT) != null && !inputJSON.get(CommonConstants.PERCENTAGEAMOUNT).toString().isEmpty()){
					 			taxSlab.setPercentageamount(Double.parseDouble(inputJSON.get(CommonConstants.PERCENTAGEAMOUNT).toString()));
					 		}
					 		if(inputJSON.get(CommonConstants.FROMAGE) != null && !inputJSON.get(CommonConstants.FROMAGE).toString().isEmpty()){
					 			taxSlab.setEmpAgeFrom(Long.parseLong(inputJSON.get(CommonConstants.FROMAGE).toString()));
					 		}
					 		if(inputJSON.get(CommonConstants.TOAGE) != null && !inputJSON.get(CommonConstants.TOAGE).toString().isEmpty()){
					 			taxSlab.setEmpAgeTo(Long.parseLong(inputJSON.get(CommonConstants.TOAGE).toString()));
					 		}
					 		if(inputJSON.get(CommonConstants.STARTDATE) != null && !inputJSON.get(CommonConstants.STARTDATE).toString().isEmpty()){
					 			taxSlab.setStartdate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.STARTDATE).toString()));
					 		}
					 		if(inputJSON.get(CommonConstants.ENDDATE) != null && !inputJSON.get(CommonConstants.ENDDATE).toString().isEmpty()){
					 			taxSlab.setEnddate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.ENDDATE).toString()));
					 		}
					 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			taxSlab.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 		}
					 		if(inputJSON.get(CommonConstants.STATUS).toString() != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
					 			taxSlab.setStatus((char) (inputJSON.get(CommonConstants.STATUS).toString().charAt(0)));
					 		}
					 		taxSlab.setUpdatedon(new Date());
					 		taxSlabsSettingsService.update(taxSlab);
					 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Tax Slab Settings Updated");
			 			}
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/importTaxSlabSettings", method = RequestMethod.POST)
	public @ResponseBody String importTaxSlabSettings(Model model, HttpServletRequest request) {
		try {
			JSONObject inputJSON =  CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
			JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
			List<TaxSlabsSettingsDO> taxSlabsSettingList = new ArrayList<TaxSlabsSettingsDO>();
			Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY);
			for (int i=0; i < fileData.size(); i++){
				JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
				TaxSlabsSettingsDO taxSlabsSettingDO = new TaxSlabsSettingsDO();
				
				if(colName.get(CommonConstants.FROMAMOUNT) != null && !colName.get(CommonConstants.FROMAMOUNT).toString().isEmpty()){
					taxSlabsSettingDO.setAmountfrom(Long.parseLong(rowJSON.get(colName.get(CommonConstants.FROMAMOUNT)).toString()));
		 		}
				if(colName.get(CommonConstants.TOAMOUNT) != null && !colName.get(CommonConstants.TOAMOUNT).toString().isEmpty()){
					taxSlabsSettingDO.setAmountto(Long.parseLong(rowJSON.get(colName.get(CommonConstants.TOAMOUNT)).toString()));
		 		}
				if(colName.get(CommonConstants.PERCENTAGEAMOUNT) != null && !colName.get(CommonConstants.PERCENTAGEAMOUNT).toString().isEmpty()){
					taxSlabsSettingDO.setPercentageamount(Double.parseDouble(rowJSON.get(colName.get(CommonConstants.PERCENTAGEAMOUNT)).toString()));
		 		}
				if(colName.get(CommonConstants.FROMAGE) != null && !colName.get(CommonConstants.FROMAGE).toString().isEmpty()){
					taxSlabsSettingDO.setEmpAgeFrom(Long.parseLong(rowJSON.get(colName.get(CommonConstants.FROMAGE)).toString()));
		 		}
				if(colName.get(CommonConstants.TOAGE) != null && !colName.get(CommonConstants.TOAGE).toString().isEmpty()){
					taxSlabsSettingDO.setEmpAgeTo(Long.parseLong(rowJSON.get(colName.get(CommonConstants.TOAGE)).toString()));
		 		}
				if(colName.get(CommonConstants.STARTDATE) != null && !colName.get(CommonConstants.STARTDATE).toString().isEmpty()){
					taxSlabsSettingDO.setStartdate(CommonUtil.convertStringToSqlDate(rowJSON.get(colName.get(CommonConstants.STARTDATE)).toString()));
		 		}
				if(colName.get(CommonConstants.ENDDATE) != null && !colName.get(CommonConstants.ENDDATE).toString().isEmpty()){
					taxSlabsSettingDO.setEnddate(CommonUtil.convertStringToSqlDate(rowJSON.get(colName.get(CommonConstants.ENDDATE)).toString()));
		 		}
		 		if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
		 			taxSlabsSettingDO.setUpdatedby(updatedBy.toString());
		 			taxSlabsSettingDO.setCreatedby(updatedBy.toString());
		 		}
		 		taxSlabsSettingDO.setUpdatedon(new Date());
		 		taxSlabsSettingDO.setCreatedon(new Date());
		 		taxSlabsSettingDO.setStatus(CommonConstants.ACTIVE);
		 		taxSlabsSettingList.add(taxSlabsSettingDO);
			}
			taxSlabsSettingsService.persistList(taxSlabsSettingList);
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
