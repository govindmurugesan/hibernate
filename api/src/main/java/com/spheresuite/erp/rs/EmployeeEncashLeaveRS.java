package com.spheresuite.erp.rs;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.EmployeeEncashLeaveDO;
import com.spheresuite.erp.service.EmployeeEncashLeaveService;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.EmployeeEncashLeaveUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/empencashleave")
public class EmployeeEncashLeaveRS {

	String validation = null;
	static Logger logger = Logger.getLogger(EmployeeEncashLeaveRS.class.getName());

	@Autowired
	private EmployeeEncashLeaveService employeeEncashLeaveService;
	
	@Autowired
	private EmployeeService employeeService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeEncashLeaveDO employeeLeavegroupDO = new EmployeeEncashLeaveDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.EMPID).toString() != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
				 		if(employeeList != null && employeeList.size() > 0){
				 			employeeLeavegroupDO.setEmployee(employeeList.get(0));
				 		}
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.DAYS) != null && !inputJSON.get(CommonConstants.DAYS).toString().isEmpty()){
			 			employeeLeavegroupDO.setNumberofenchashleave(Long.parseLong(inputJSON.get(CommonConstants.DAYS).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.FROMDATE) != null){
			 			employeeLeavegroupDO.setFromDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.FROMDATE).toString()));
		 			}
		 			if(inputJSON.get(CommonConstants.TODATE) != null){
		 				employeeLeavegroupDO.setToDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.TODATE).toString()));
		 			}
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			employeeLeavegroupDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			employeeLeavegroupDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		employeeLeavegroupDO.setStatus('a');
			 		employeeLeavegroupDO.setUpdatedon(new Date());
			 		employeeLeavegroupDO.setCreatedon(new Date());
			 	}
			 	employeeEncashLeaveService.persist(employeeLeavegroupDO);
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Leave Group Updated");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retrieveByEmpId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByEmpId(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<EmployeeEncashLeaveDO> employeeDetailList = employeeEncashLeaveService.retrieveByEmpId(inputJSON.get(CommonConstants.ID).toString());
					respJSON = EmployeeEncashLeaveUtil.getEmployeeEncashLeaveList(employeeDetailList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveEncashLeavesByEmpId", method = RequestMethod.POST)
	public @ResponseBody String retrieveAvailableLeavesByEmpId(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
				if (inputJSON != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
					List<Object[]> leaveRequestsList = employeeEncashLeaveService.retrieveAvailableLeavesByEmpId(inputJSON.get(CommonConstants.EMPID).toString(),
							CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.LEAVETYPEFROMDATE).toString()),
							CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.LEAVETYPETODATE).toString()));
					respJSON = EmployeeEncashLeaveUtil.getEmployeeEncashLeave(leaveRequestsList).toString();
			 	}
			}
			 	}catch (Exception e) {
					e.printStackTrace();
					return CommonWebUtil.buildErrorResponse("").toString();
				}
				return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
}
