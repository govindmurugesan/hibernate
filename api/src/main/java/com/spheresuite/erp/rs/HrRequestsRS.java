
package com.spheresuite.erp.rs;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.DepartmentDO;
import com.spheresuite.erp.domainobject.HrRequestDocDO;
import com.spheresuite.erp.domainobject.HrRequestTypeDO;
import com.spheresuite.erp.domainobject.HrRequestsDO;
import com.spheresuite.erp.domainobject.NotificationSettingsDO;
import com.spheresuite.erp.service.DepartmentService;
import com.spheresuite.erp.service.HrRequestDocService;
import com.spheresuite.erp.service.HrRequestTypeService;
import com.spheresuite.erp.service.HrRequestsService;
import com.spheresuite.erp.service.NotificationSettingsService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.HrRequestsUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/hrrequests")
public class HrRequestsRS {

	@Autowired
	private HrRequestsService hrRequestsService;
	
	@Autowired
	private HrRequestDocService hrRequestDocService;
	
	@Autowired
	private HrRequestTypeService hrRequestTypeService;
	
	@Autowired
	private DepartmentService departmentService;
	
	@Autowired
	private NotificationSettingsService notificationSettingsService;
	
	String validation = null;
	static Logger logger = Logger.getLogger(HrPoliciesRS.class.getName());
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		HrRequestsDO hrRequestDO = new HrRequestsDO(); 
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
				if (inputJSON != null){
					if(inputJSON.get(CommonConstants.TYPE) != null && !inputJSON.get(CommonConstants.TYPE).toString().isEmpty()){
						List<HrRequestTypeDO> hrRequestTypeList = hrRequestTypeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.TYPE).toString()));
				 		if(hrRequestTypeList != null && hrRequestTypeList.size() > 0){
				 			hrRequestDO.setHrRequestType(hrRequestTypeList.get(0));
				 			if(inputJSON.get(CommonConstants.DEPT) != null && !inputJSON.get(CommonConstants.DEPT).toString().isEmpty()){
								List<DepartmentDO> deptList = departmentService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.DEPT).toString()));
						 		if(deptList != null && deptList.size() > 0){
						 			hrRequestDO.setDepartment(deptList.get(0));
						 		}
				 			}
				 			if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 				hrRequestDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 				hrRequestDO.setEmpId(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 				hrRequestDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 				if(inputJSON.get(CommonConstants.DESC) != null && !inputJSON.get(CommonConstants.DESC).toString().isEmpty()){
									hrRequestDO.setDescription(inputJSON.get(CommonConstants.DESC).toString());
								}
								if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
									hrRequestDO.setStatus((char) inputJSON.get(CommonConstants.STATUS).toString().charAt(0));
								}
					 			hrRequestDO.setUpdatedon(new Date());
					 			hrRequestDO.setCreatedon(new Date());
					 			hrRequestsService.persist(hrRequestDO);
					 			CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New HR Request Submitted");
					 			CommonUtil.sendMailToReportingHeads(request,inputJSON,"hrrequest/companiesrequest","hrrequests","pending");
					 		}
				 		}
					}
			 	}
			}else{
		 		return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
		 	}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponseId(hrRequestDO.getHrRequestId().toString()).toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request)  {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<HrRequestsDO> hrRequestsList = hrRequestsService.retrieve();
				respJSON = HrRequestsUtil.getHrRequestsList(hrRequestsList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		HrRequestsDO hrRequestsDO = new HrRequestsDO(); 
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && inputJSON.get(CommonConstants.ID) != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
		 				List<HrRequestsDO> hrRequestList = hrRequestsService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
		 				if(hrRequestList != null && hrRequestList.size() > 0){
		 					hrRequestsDO = hrRequestList.get(0);
		 					if(inputJSON.get(CommonConstants.TYPE) != null && !inputJSON.get(CommonConstants.TYPE).toString().isEmpty()){
								List<HrRequestTypeDO> hrRequestTypeList = hrRequestTypeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.TYPE).toString()));
						 		if(hrRequestTypeList != null && hrRequestTypeList.size() > 0){
						 			hrRequestsDO.setHrRequestType(hrRequestTypeList.get(0));
						 		}
							}
		 					if(inputJSON.get(CommonConstants.DESC) != null && !inputJSON.get(CommonConstants.DESC).toString().isEmpty()){
		 						hrRequestsDO.setDescription(inputJSON.get(CommonConstants.DESC).toString());
							}
			 				if(inputJSON.get(CommonConstants.COMMENT) != null && !inputJSON.get(CommonConstants.COMMENT).toString().isEmpty()){
			 					hrRequestsDO.setComments(inputJSON.get(CommonConstants.COMMENT).toString());
			 				}
			 				hrRequestsDO.setUpdatedon(new Date());
			 				if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 					hrRequestsDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 		}
			 				if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
			 					hrRequestsDO.setStatus((char) inputJSON.get(CommonConstants.STATUS).toString().charAt(0));
					 		}
			 				hrRequestsService.update(hrRequestsDO);
			 				//hrRequestDocService.delete(hrRequestsDO.getHrRequestId());
			 				if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty() && inputJSON.get(CommonConstants.STATUS).toString().equalsIgnoreCase("f")){
			 					CommonUtil.sendMailToReportingHeads(request,inputJSON,"hrrequests","hrrequests","approved");
				 			}
			 				if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty() && inputJSON.get(CommonConstants.STATUS).toString().equalsIgnoreCase("p")){
			 					CommonUtil.sendMailToReportingHeads(request,inputJSON,"hrrequests","hrrequests","pending");
				 			}
			 				if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty() && inputJSON.get(CommonConstants.STATUS).toString().equalsIgnoreCase("r")){
			 					CommonUtil.sendMailToReportingHeads(request,inputJSON,"hrrequests","hrrequests","reject");
				 			}
			 				if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty() && inputJSON.get(CommonConstants.STATUS).toString().equalsIgnoreCase("c")){
			 					CommonUtil.sendMailToReportingHeads(request,inputJSON,"hrrequests","hrrequests","cancel");
				 			}
			 				//CommonUtil.sendMailToReportingHeadsForUpdate(request, inputJSON, "hrrequests", "hrRequest");
			 				//CommonUtil.sendMailToReportingHeads(request,inputJSON,"hrrequests","hrRequest");
			 			//	CommonUtil.sendMailToReportingHeads(request,inputJSON,"hrrequest/companiesrequest","hrRequest","pending");
			 				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "HR Request Updated");
		 				}
			 		}
			 	}
				
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponseId(hrRequestsDO.getHrRequestId().toString()).toString();
	}
	
	@RequestMapping(value = "/retrieveByEmpId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByEmpId(@PathVariable String inputParams, Model model, HttpServletRequest request)  {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
					List<HrRequestsDO> hrRequestsList = hrRequestsService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
					respJSON = HrRequestsUtil.getHrRequestsList(hrRequestsList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveById/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveById(@PathVariable String inputParams, Model model, HttpServletRequest request)  {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<HrRequestsDO> hrRequestsList = hrRequestsService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = HrRequestsUtil.getHrRequestsList(hrRequestsList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/updatepic", method = RequestMethod.POST)
	public @ResponseBody String updatepic(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				HrRequestDocDO hrRequestDoc = new HrRequestDocDO();
			 	if (request.getParameter("file") != null && request.getParameter("id") != null){
			 		hrRequestDoc.setHrrequestId(Long.parseLong(request.getParameter("id")));
			 		hrRequestDoc.setPhoto(request.getParameter("file"));
			 		hrRequestDoc.setFileName(request.getParameter("name"));
			 		hrRequestDocService.persist(hrRequestDoc);
 			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveByApproverId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByApproverId(@PathVariable String inputParams, Model model, HttpServletRequest request)  {
		String respJSON = null;
		Boolean isApprover = false;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<NotificationSettingsDO> notificationSettingsList = notificationSettingsService.retrieve();
	 				if(notificationSettingsList != null && notificationSettingsList.size() >0){
	 					if(notificationSettingsList.get(0).getHrRequest() != null){
	 						List<String> hrRequestList = Arrays.asList(notificationSettingsList.get(0).getHrRequest().split(CommonConstants.COMMA));
	 						if(hrRequestList !=null && hrRequestList.size() > 0){
	 							for(String approver : hrRequestList ){
	 								if(approver.equalsIgnoreCase(inputJSON.get(CommonConstants.ID).toString())){
	 									isApprover = true;
	 									List<HrRequestsDO> hrRequestsList = hrRequestsService.retrieve();
	 									respJSON = HrRequestsUtil.getHrRequestsList(hrRequestsList).toString();
	 								}
	 								
	 							}
	 							if(!isApprover){
	 								respJSON = HrRequestsUtil.notApproverResponse().toString();
	 							}
 						}
	 				}
					
	 				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
}
	
