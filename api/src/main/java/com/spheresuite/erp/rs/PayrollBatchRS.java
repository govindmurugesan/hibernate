package com.spheresuite.erp.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.PayrollBatchDO;
import com.spheresuite.erp.domainobject.UnitOrBranchDO;
import com.spheresuite.erp.service.PayrollBatchService;
import com.spheresuite.erp.service.UnitOrBranchService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.PayrollBatchUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/payrollbatch")
public class PayrollBatchRS {

	String validation = null;
	static Logger logger = Logger.getLogger(PayrollBatchRS.class.getName());

	@Autowired
	private PayrollBatchService payrollBatchService;
	
	@Autowired
	private UnitOrBranchService unitOrBranchService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				PayrollBatchDO batchDO = new PayrollBatchDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.UNIT) != null && !inputJSON.get(CommonConstants.UNIT).toString().isEmpty()){
			 			List<UnitOrBranchDO> unitOrBranchDOList = unitOrBranchService.retrieveActiveById(Long.parseLong(inputJSON.get(CommonConstants.UNIT).toString()));
			 			if(unitOrBranchDOList != null && unitOrBranchDOList.size() > 0){
			 				batchDO.setUnitOrBranch(unitOrBranchDOList.get(0));
			 				if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			batchDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			batchDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
						 			batchDO.setName(inputJSON.get(CommonConstants.NAME).toString());
						 		}
						 		if(inputJSON.get(CommonConstants.DISPLAYNAME) != null && !inputJSON.get(CommonConstants.DISPLAYNAME).toString().isEmpty()){
						 			batchDO.setDisplayname(inputJSON.get(CommonConstants.DISPLAYNAME).toString());
						 		}
						 		if(inputJSON.get(CommonConstants.OTCHECK) != null && !inputJSON.get(CommonConstants.OTCHECK).toString().isEmpty()){
						 			batchDO.setOtCheck(inputJSON.get(CommonConstants.OTCHECK).toString());
						 		}
						 		if(inputJSON.get(CommonConstants.MIN_HR_OT) != null && !inputJSON.get(CommonConstants.MIN_HR_OT).toString().isEmpty()){
						 			batchDO.setMinHrOT(inputJSON.get(CommonConstants.MIN_HR_OT).toString());
						 		}
						 		if(inputJSON.get(CommonConstants.TYPE) != null && !inputJSON.get(CommonConstants.TYPE).toString().isEmpty()){
						 			batchDO.setPayrollType(inputJSON.get(CommonConstants.TYPE).toString());
						 			batchDO.setStatus(CommonConstants.ACTIVE);
							 		batchDO.setUpdatedon(new Date());
							 		batchDO.setCreatedon(new Date());
							 		if(!payrollBatchService.persist(batchDO)){
							 			return CommonWebUtil.buildErrorResponse("Payroll Batch Already Added").toString();
									}
									CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Payroll Batch Created");
						 		}
					 		}
			 			}
		 			}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retrieveActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<PayrollBatchDO> batchList = payrollBatchService.retrieveActive();
				respJSON = PayrollBatchUtil.getBatchList(batchList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<PayrollBatchDO> batchList = payrollBatchService.retrieve();
				respJSON = PayrollBatchUtil.getBatchList(batchList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveByUnitId/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByUnitId(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<PayrollBatchDO> batchList = payrollBatchService.retrieveByUnitId(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = PayrollBatchUtil.getBatchList(batchList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	
	@RequestMapping(value = "/retrieveByPayrollType/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByPayrollType(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.TYPE).toString().isEmpty()){
					List<PayrollBatchDO> batchList = payrollBatchService.retrieveByPayrollType(inputJSON.get(CommonConstants.TYPE).toString());
					respJSON = PayrollBatchUtil.getBatchList(batchList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				PayrollBatchDO batchDO = new PayrollBatchDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<PayrollBatchDO> batchList = payrollBatchService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		
			 		if(batchList != null && batchList.size() > 0){
			 			batchDO = batchList.get(0);
				 		if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
				 			batchDO.setName(inputJSON.get(CommonConstants.NAME).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.DISPLAYNAME) != null && !inputJSON.get(CommonConstants.DISPLAYNAME).toString().isEmpty()){
				 			batchDO.setDisplayname(inputJSON.get(CommonConstants.DISPLAYNAME).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.TYPE) != null && !inputJSON.get(CommonConstants.TYPE).toString().isEmpty()){
				 			batchDO.setPayrollType(inputJSON.get(CommonConstants.TYPE).toString());
				 		}
				 		batchDO.setUpdatedon(new Date());
				 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			batchDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
				 			batchDO.setStatus((char) inputJSON.get(CommonConstants.STATUS).toString().charAt(0));
				 		}
				 		if(inputJSON.get(CommonConstants.OTCHECK) != null && !inputJSON.get(CommonConstants.OTCHECK).toString().isEmpty()){
				 			batchDO.setOtCheck(inputJSON.get(CommonConstants.OTCHECK).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.MIN_HR_OT) != null && !inputJSON.get(CommonConstants.MIN_HR_OT).toString().isEmpty()){
				 			batchDO.setMinHrOT(inputJSON.get(CommonConstants.MIN_HR_OT).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.UNIT) != null && !inputJSON.get(CommonConstants.UNIT).toString().isEmpty()){
				 			List<UnitOrBranchDO> unitOrBranchDOList = unitOrBranchService.retrieveActiveById(Long.parseLong(inputJSON.get(CommonConstants.UNIT).toString()));
				 			if(unitOrBranchDOList != null && unitOrBranchDOList.size() > 0){
				 				batchDO.setUnitOrBranch(unitOrBranchDOList.get(0));
				 			}
			 			}
				 		
				 		if(!payrollBatchService.update(batchDO)){
				 			return CommonWebUtil.buildErrorResponse("Payroll Batch Already Added").toString();
				 		}
				 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Payroll Batch Updated");
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/importPayrollBatch", method = RequestMethod.POST)
	public @ResponseBody String importPayrollBatch(Model model, HttpServletRequest request) {
		try {
			JSONObject inputJSON =  CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
			JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
			List<PayrollBatchDO> payrollBatchList = new ArrayList<PayrollBatchDO>();
			Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY);
			for (int i=0; i < fileData.size(); i++){
				JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
				PayrollBatchDO payrollBatchDO = new PayrollBatchDO();
				if(colName.get(CommonConstants.UNIT) != null && !colName.get(CommonConstants.UNIT).toString().isEmpty()){
		 			List<UnitOrBranchDO> unitOrBranchDOList = unitOrBranchService.retrieveActiveById(Long.parseLong(rowJSON.get(colName.get(CommonConstants.UNIT)).toString()));
		 			if(unitOrBranchDOList != null && unitOrBranchDOList.size() > 0){
		 				payrollBatchDO.setUnitOrBranch(unitOrBranchDOList.get(0));
		 				if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
				 			payrollBatchDO.setUpdatedby(updatedBy.toString());
				 			payrollBatchDO.setCreatedby(updatedBy.toString());
				 			if (colName.get(CommonConstants.NAME) != null && !colName.get(CommonConstants.NAME).toString().isEmpty()){
								payrollBatchDO.setName(rowJSON.get(colName.get(CommonConstants.NAME)).toString());
					 		}
							if(colName.get(CommonConstants.TYPE) != null && !colName.get(CommonConstants.TYPE).toString().isEmpty()){
								payrollBatchDO.setPayrollType(rowJSON.get(colName.get(CommonConstants.TYPE)).toString());
					 		}
							if (colName.get(CommonConstants.DISPLAYNAME) != null && !colName.get(CommonConstants.DISPLAYNAME).toString().isEmpty()){
								payrollBatchDO.setDisplayname(rowJSON.get(colName.get(CommonConstants.DISPLAYNAME)).toString());
					 		}
					 		
					 		payrollBatchDO.setUpdatedon(new Date());
					 		payrollBatchDO.setCreatedon(new Date());
					 		payrollBatchDO.setStatus(CommonConstants.ACTIVE);
					 		payrollBatchList.add(payrollBatchDO);
				 		}
		 			}
				}
			}
			payrollBatchService.persistList(payrollBatchList);
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
