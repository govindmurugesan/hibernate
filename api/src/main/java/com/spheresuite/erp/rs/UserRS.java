package com.spheresuite.erp.rs;

import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.RolesDO;
import com.spheresuite.erp.domainobject.SupplierAccessDO;
import com.spheresuite.erp.domainobject.UserDO;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.RolesService;
import com.spheresuite.erp.service.UserService;
import com.spheresuite.erp.supplier.service.SupplierAccessService;
import com.spheresuite.erp.supplier.web.util.SupplierAccessUtil;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.util.JwtTokenGenerator;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.EmailProxyUtil;
import com.spheresuite.erp.web.util.UserUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/user")
public class UserRS {
	
	String validation = null;
	
	static Logger logger = Logger.getLogger(UserRS.class.getName());


	@Autowired
	private UserService userService;
	

	@Autowired
	private EmployeeService employeeService;
	
	@Autowired
	private RolesService rolesService;
	
	@Autowired
	private SupplierAccessService supplierAccessService;
	
	

	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				UserDO userDO = new UserDO();
				List<EmployeeDO> employeeList = null;
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.EMPID) != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
			 			if(employeeList != null && employeeList.size() > 0){
				 			userDO.setEmployee(employeeList.get(0));
				 			if(inputJSON.get(CommonConstants.ROLE_ID) != null && !inputJSON.get(CommonConstants.ROLE_ID).toString().isEmpty()){
					 			List<RolesDO> rolesList = rolesService.retriveById(Long.parseLong(inputJSON.get(CommonConstants.ROLE_ID).toString()));
								if(rolesList != null){
									userDO.setRole(rolesList.get(0));
									if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
							 			userDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
							 			userDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
							 			userDO.setIsDeleted(0L);
										userDO.setUpdatedon(new Date());
										char[] temppasssword = UserUtil.geek_Password(5);
										String pwd = temppasssword.toString();
										userDO.setTemppassowrd(Base64.getEncoder().encodeToString(pwd.getBytes("UTF-8")));
										userDO.setStatus(CommonConstants.PEDING);
										
										if(!userService.persist(userDO)){
											return CommonWebUtil.buildErrorResponse("User Already Exist").toString();
										}
										CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New User Created");
										if(userDO != null && userDO.getUserId() != null){
									 		if(employeeList != null && employeeList.size() > 0){ 
									 			if(employeeList.get(0).getCompanyemail() != null ){
										 			String host = request.getServletContext().getInitParameter(CommonConstants.HOST);
													String fromEmail = CommonConstants.SENDMAIL;
													String toEmails = employeeList.get(0).getCompanyemail();
													String userName = null;
													
													List<EmployeeDO> empList = employeeService.retriveByEmpId(userDO.getEmployee().getEmpId());
													if(empList != null && empList.size() > 0){
														userName =  String.valueOf(empList.get(0).getFirstname());
													}
													String URL = CommonUtil.getUIUrl(request);
													URL = CommonConstants.HTTP+URL+"#!/changepassword/";
													String url = URL+userDO.getTemppassowrd()+"/"+employeeList.get(0).getCompanyemail()+"/"+employeeList.get(0).getEmpId()+"/N"+"/U";
													String emailBody = "Cick Here Login : " +URL+userDO.getTemppassowrd()+"/"+employeeList.get(0).getCompanyemail()+"/N"+"/U";
											 		boolean mailStatus = EmailProxyUtil.sendEmail(request,host,  fromEmail,  toEmails, "", "", emailBody, false, url, "inviteUser", userName); 
													if(mailStatus){
														return CommonWebUtil.buildSuccessResponse().toString();
													}else{
														return CommonWebUtil.buildErrorResponse("").toString();
													}
									 			}else{
											 		return CommonWebUtil.buildErrorResponse("Email Id not available to send mail").toString();
											 	}
									 		}else{
										 		return CommonWebUtil.buildErrorResponse("Employee not available").toString();
										 	}
									}
								}
					 		}
				 		}
			 		}
			 		/*if(inputJSON.get(CommonConstants.EMAIL) != null && !inputJSON.get(CommonConstants.EMAIL).toString().isEmpty()){
			 			userDO.setEmail(inputJSON.get(CommonConstants.EMAIL).toString().toString());
			 		}*/
			 	}else{
			 		return CommonWebUtil.buildErrorResponse("User Id not available to send mail").toString();
			 	}
			}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/resendInvite/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String resendInvite(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && inputJSON.get(CommonConstants.ID) != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<UserDO> userList = userService.retriveByEmpId(inputJSON.get(CommonConstants.ID).toString());
					if(userList != null && userList.size() > 0){
						if(userList.get(0).getTemppassowrd() != null && userList.get(0).getEmployee().getCompanyemail() != null){
							String host = request.getServletContext().getInitParameter(CommonConstants.HOST);
							String fromEmail = CommonConstants.SENDMAIL;
							String toEmails = userList.get(0).getEmployee().getCompanyemail();
							String URL = CommonUtil.getUIUrl(request);
							URL = CommonConstants.HTTP+URL+"#!/changepassword/";
							String emailBody = "Cick Here Login : " +URL+userList.get(0).getTemppassowrd()+"/"+userList.get(0).getEmployee().getCompanyemail()+"/"+userList.get(0).getEmployee().getEmpId()+"/N"+"/U";
							String url = URL+userList.get(0).getTemppassowrd()+"/"+userList.get(0).getEmployee().getCompanyemail()+"/"+userList.get(0).getEmployee().getEmpId()+"/N"+"/U";

							String userName = null;
							List<EmployeeDO> empList = employeeService.retrieveEmpId(userList.get(0).getEmployee().getEmpId().toString());
							if(empList != null && empList.size() > 0){
								userName =  String.valueOf(empList.get(0).getFirstname() );
							}
							boolean mailStatus = EmailProxyUtil.sendEmail(request,host,  fromEmail,  toEmails, "", "", emailBody, false, url, "reInvite", userName);
							if(mailStatus){
								return CommonWebUtil.buildSuccessResponse().toString();
							}else{
								return CommonWebUtil.buildErrorResponse("").toString();
							}
						}
					}
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<UserDO> userList = userService.retrieve();
				respJSON = UserUtil.getUserList(userList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<UserDO> userList = userService.retrieveActive();
				respJSON = UserUtil.getUserList(userList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retriveById/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveById(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && inputJSON.get(CommonConstants.ID) != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<UserDO> userList = userService.retriveByEmpId(inputJSON.get(CommonConstants.ID).toString());
					respJSON = UserUtil.getUserList(userList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && inputJSON.get(CommonConstants.ID) != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		UserDO userDO = new UserDO();
			 		List<UserDO>userDOList = userService.retriveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));;
				 	if (userDOList != null && userDOList.size() > 0){
				 		userDO = userDOList.get(0);
				 		if(inputJSON.get(CommonConstants.ROLE_ID) != null && !inputJSON.get(CommonConstants.ROLE_ID).toString().isEmpty()){
				 			List<RolesDO> rolesList = rolesService.retriveById(Long.parseLong(inputJSON.get(CommonConstants.ROLE_ID).toString()));
							if(rolesList != null){
								userDO.setRole(rolesList.get(0));
							}
				 		}
				 		/*if(inputJSON.get(CommonConstants.EMAIL) != null && !inputJSON.get(CommonConstants.EMAIL).toString().isEmpty()){
				 			userDO.setEmail(inputJSON.get(CommonConstants.EMAIL).toString().toString());
				 		}*/
						userDO.setUpdatedon(new Date());
						if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			userDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
						if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
							userDO.setStatus((char)  inputJSON.get(CommonConstants.STATUS).toString().charAt(0));
				 		}
						if(userDO.getStatus() != 'd'){
							userDO.setIsDeleted(0L);
						}else{
							userDO.setIsDeleted(1L);
						}
						//userService.update(userDO);
						if(!userService.update(userDO)){
							return CommonWebUtil.buildErrorResponse("User Already Added").toString();
						}
						CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "User Updated");
				 	}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/updatepassword", method = RequestMethod.POST)
	public @ResponseBody String updatepassword(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && inputJSON.get(CommonConstants.EMAIL) != null && !inputJSON.get(CommonConstants.EMAIL).toString().isEmpty()
						&& inputJSON.get(CommonConstants.PASSWORD) != null && !inputJSON.get(CommonConstants.PASSWORD).toString().isEmpty()){
			 		UserDO userDO = new UserDO();
			 		List<UserDO>userDOList = userService.retrieveByEmailId(inputJSON.get(CommonConstants.EMAIL).toString());
				 	if (userDOList != null){
				 		userDO = userDOList.get(0);
				 		/*Base64.Encoder encoder = Base64.getEncoder(); 
						byte[] enString = encoder.encode(inputJSON.get(CommonConstants.PASSWORD).toString().getBytes());*/
						String pwd = inputJSON.get(CommonConstants.PASSWORD).toString();
						userDO.setPassword(Base64.getEncoder().encodeToString(pwd.getBytes("UTF-8")));
				 		//String enString = CommonUtil.encryptText("testseed",inputJSON != null ? CommonConstants.PASSWORD.toString() : "");
						userDO.setUpdatedon(new Date());
						userService.update(userDO);
						CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Password Updated");
				 	}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/delete/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String delete(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
			 	if (inputJSON != null && inputJSON.get(CommonConstants.ID) != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		UserDO userDO = new UserDO();
			 		List<UserDO> userList = userService.retriveByEmpId(inputJSON.get(CommonConstants.ID).toString());
				 	if (userList != null){
				 		userDO = userList.get(0);
				 		userDO.setIsDeleted(1L);
				 		userDO.setStatus('d');
						userDO.setUpdatedon(new Date());
						userService.update(userDO);
				 	}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/cancelUser/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String cancelUser(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
			 	if (inputJSON != null && inputJSON.get(CommonConstants.ID) != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		UserDO userDO = new UserDO();
			 		List<UserDO> userList = userService.retriveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
				 	if (userList != null && userList.size() > 0){
				 		userDO = userList.get(0);
				 		userDO.setIsDeleted(1L);
				 		userDO.setStatus('c');
						userDO.setUpdatedon(new Date());
						userService.update(userDO);
				 	}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveForLogin", method = RequestMethod.POST)
	public @ResponseBody String retrieveForLogin(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			//if (WebManager.authenticateSession(request)) {
			JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			if(inputJSON != null && inputJSON.get(CommonConstants.EMAIL) != null && !inputJSON.get(CommonConstants.EMAIL).toString().isEmpty()
					&& inputJSON.get(CommonConstants.PASSWORD) != null && !inputJSON.get(CommonConstants.PASSWORD).toString().isEmpty()){
				List<UserDO> userList = userService.retriveByEmpId(inputJSON.get(CommonConstants.EMAIL).toString());
				if(userList != null && userList.size() > 0) {
					byte[] decodedBytes = Base64.getDecoder().decode(userList.get(0).getPassword());
					String stringDecode = new String(decodedBytes, "UTF-8");
					if(stringDecode.toString().equals(inputJSON.get(CommonConstants.PASSWORD).toString())){
						String token = new JwtTokenGenerator().createJWT(userList);
						HttpSession session = request.getSession();
						session.setAttribute("token", token);
						//session.setMaxInactiveInterval(60*60);
						respJSON = UserUtil.getUserForLogin(userList,token).toString();
					}
					
				} 
				List<SupplierAccessDO> supplierList = supplierAccessService.retrieveByEmailId(inputJSON.get(CommonConstants.EMAIL).toString());
				if(supplierList != null && supplierList.size() > 0) {
					byte[] decodedBytes = Base64.getDecoder().decode(supplierList.get(0).getPassword());
					String stringDecode = new String(decodedBytes, "UTF-8");
					if(stringDecode.toString().equals(inputJSON.get(CommonConstants.PASSWORD).toString())){
						String token = new JwtTokenGenerator().createSupplierJWT(supplierList);
						HttpSession session = request.getSession();
						session.setAttribute("token", token);
						//session.setMaxInactiveInterval(60*60);
						respJSON = SupplierAccessUtil.getUserForLogin(supplierList,token).toString();
					}
					
				}
			}
			//}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
			
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/resetpassword", method = RequestMethod.POST)
	public @ResponseBody String resetpassword(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && inputJSON.get(CommonConstants.EMAIL) != null && !inputJSON.get(CommonConstants.EMAIL).toString().isEmpty() 
			 			&& inputJSON.get(CommonConstants.TEMP_PASSWORD) != null && !inputJSON.get(CommonConstants.TEMP_PASSWORD).toString().isEmpty()
			 			&& inputJSON.get(CommonConstants.NEWPASSWORD) != null && !inputJSON.get(CommonConstants.NEWPASSWORD).toString().isEmpty() 
			 			&& inputJSON.get(CommonConstants.EMPID) != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 		List<EmployeeDO> empList =  employeeService.retrieveEmpId(inputJSON.get(CommonConstants.EMPID).toString());
			 		if(empList != null && empList.size() > 0){
			 			UserDO userDO = new UserDO();
				 		List<UserDO>userDOList = userService.retrieveByTempPassword(inputJSON.get(CommonConstants.TEMP_PASSWORD).toString(),
				 				empList.get(0).getEmpId());
					 	if (userDOList != null && userDOList.size() > 0){
					 		userDO = userDOList.get(0);
					 		String pwd = inputJSON.get(CommonConstants.NEWPASSWORD).toString();
							userDO.setPassword(Base64.getEncoder().encodeToString(pwd.getBytes("UTF-8")));
							userDO.setTemppassowrd(null);
							userDO.setUpdatedon(new Date());
							userDO.setStatus(CommonConstants.ACTIVE);
							userService.update(userDO);
					 	}else{
					 		return CommonWebUtil.buildErrorResponse("User not availble").toString();
					 	}
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/sendPassword", method = RequestMethod.POST)
	public @ResponseBody String sendPassword(Model model, HttpServletRequest request) {
		try {
			JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			if(inputJSON != null && inputJSON.get(CommonConstants.EMAIL) != null && !inputJSON.get(CommonConstants.EMAIL).toString().isEmpty()){
				List<UserDO> userList = userService.retriveByEmpId(inputJSON.get(CommonConstants.EMAIL).toString());
				if(userList != null && userList.size() > 0){
					if(userList.get(0).getEmployee().getCompanyemail() != null){
						String host = request.getServletContext().getInitParameter(CommonConstants.HOST);
						String fromEmail = request.getServletContext().getInitParameter(CommonConstants.FROMEMAIL);
						String toEmails = userList.get(0).getEmployee().getCompanyemail();
						if(userList.get(0).getEmployee() != null){
							UserDO userDO = new UserDO();
							userDO = userList.get(0);
							/*char[] temppasssword = UserUtil.geek_Password(5);
							userDO.setTemppassowrd(temppasssword.toString());*/
							char[] temppasssword = UserUtil.geek_Password(5);
							//Base64.Encoder encoder = Base64.getEncoder(); 
							//byte[] enString = encoder.encode(temppasssword.toString().getBytes());
							//String enString = CommonUtil.encryptText("testseed",temppasssword.toString());
							//userDO.setTemppassowrd(enString.toString());
							String pwd = temppasssword.toString();
							userDO.setTemppassowrd(Base64.getEncoder().encodeToString(pwd.getBytes("UTF-8")));
							userDO.setPassword(null);
							//userDO = userService.update(userDO);
							userDO.setUpdatedon(new Date());
							if(userService.update(userDO)){
								String URL = CommonUtil.getUIUrl(request);
								URL = CommonConstants.HTTP+URL+"#!/changepassword/";
								String emailBody = URL+userDO.getTemppassowrd()+"/"+userList.get(0).getEmployee().getCompanyemail()+"/"+userList.get(0).getEmployee().getEmpId()+"/F"+"/U";
								String userName = null;
								String empId = null;
								List<EmployeeDO> empList = employeeService.retrieveEmpId(userList.get(0).getEmployee().getEmpId());
								if(empList != null && empList.size() > 0){
									userName =  String.valueOf(empList.get(0).getFirstname() );
									empId = String.valueOf(empList.get(0).getEmpId() );
								}
								
								String url = URL+userDO.getTemppassowrd()+"/"+userList.get(0).getEmployee().getCompanyemail()+"/"+empId+"/F"+"/U";
						 		boolean mailStatus = EmailProxyUtil.sendEmail(request,host,  fromEmail,  toEmails, "", "", emailBody, false, url, "resetPassword", userName); 
	
								if(mailStatus){
									return CommonWebUtil.buildSuccessResponse().toString();
								}else{
									return CommonWebUtil.buildErrorResponse(CommonConstants.EMAIL_NOT_SENT).toString();
								}
							}else{
								return CommonWebUtil.buildErrorResponse("").toString();
							}
							
						}else{
							return CommonWebUtil.buildErrorResponse(CommonConstants.EMPID_NOT_REGISTER).toString();
						}
					}else{
						return CommonWebUtil.buildErrorResponse("Email Id Not found").toString();
					}
				}else{
					return CommonWebUtil.buildErrorResponse(CommonConstants.EMPID_NOT_REGISTER).toString();
				}
			}else{
				return CommonWebUtil.buildErrorResponse("").toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
	}
	
	@RequestMapping(value = "/resetpasswordWithoutAuth", method = RequestMethod.POST)
	public @ResponseBody String resetpasswordWithoutAuth(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && inputJSON.get(CommonConstants.EMPID) != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty() 
			 			&& inputJSON.get(CommonConstants.NEWPASSWORD) != null && !inputJSON.get(CommonConstants.NEWPASSWORD).toString().isEmpty()){
			 		UserDO userDO = new UserDO();
			 		List<UserDO>userDOList = userService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
				 	if (userDOList != null && userDOList.size() > 0){
				 		userDO = userDOList.get(0);
				 		String pwd = inputJSON.get(CommonConstants.NEWPASSWORD).toString();
						userDO.setPassword(Base64.getEncoder().encodeToString(pwd.getBytes("UTF-8")));
						userDO.setTemppassowrd(null);
						userDO.setUpdatedon(new Date());
						userDO.setStatus(CommonConstants.ACTIVE);
						userService.updatePassword(userDO);
				 	}else{
				 		return CommonWebUtil.buildErrorResponse("").toString();
				 	}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/importUser/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String importEmp(@PathVariable String inputParams,  Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
				JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
			//	List<UserDO> userlist = new ArrayList<UserDO>();
				String updatedBy = null;
				if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY).toString();
				}
				for (int i=0; i < fileData.size(); i++){
					UserDO userDO = new UserDO();
					JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
					if(rowJSON.get(colName.get(CommonConstants.EMPID)) != null && !rowJSON.get(colName.get(CommonConstants.EMPID)).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(rowJSON.get(colName.get(CommonConstants.EMPID)).toString());
			 			if(employeeList != null && employeeList.size() > 0){
				 			userDO.setEmployee(employeeList.get(0));
				 		
					 		if(rowJSON.get(colName.get(CommonConstants.ROLE_NAME)) != null && ! rowJSON.get(colName.get(CommonConstants.ROLE_NAME)).toString().isEmpty()){
					 			List<RolesDO> rolesList = rolesService.retriveByName(rowJSON.get(colName.get(CommonConstants.ROLE_NAME)).toString());
								if(rolesList != null){
									userDO.setRole(rolesList.get(0));
								}
					 		}
					 		/*if(rowJSON.get(colName.get(CommonConstants.EMAIL)) != null && !rowJSON.get(colName.get(CommonConstants.EMAIL)).toString().isEmpty()){
					 			userDO.setEmail(rowJSON.get(colName.get(CommonConstants.EMAIL)).toString());
					 		}*/
							userDO.setIsDeleted(0L);
							userDO.setUpdatedon(new Date());
							if(updatedBy != null){
					 			userDO.setUpdatedby((String) updatedBy);
					 			userDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 		}
							char[] temppasssword = UserUtil.geek_Password(5);
							String pwd = temppasssword.toString();
							userDO.setTemppassowrd(Base64.getEncoder().encodeToString(pwd.getBytes("UTF-8")));
							userDO.setStatus(CommonConstants.PEDING);
							
							if(!userService.persist(userDO)){
								//return CommonWebUtil.buildErrorResponse("User Already Exist").toString();
							}else{
								if(userDO != null && userDO.getUserId() != null && userDO.getEmployee().getCompanyemail() != null ){
									List<EmployeeDO> empList = employeeService.retriveByEmpId(userDO.getEmployee().getEmpId());
									if(empList != null && empList.size() > 0){
										String host = request.getServletContext().getInitParameter(CommonConstants.HOST);
										String fromEmail = CommonConstants.SENDMAIL;
										String toEmails = userDO.getEmployee().getCompanyemail();
										String userName = null;
										userName =  String.valueOf(empList.get(0).getFirstname());
										String URL = CommonUtil.getUIUrl(request);
										URL = CommonConstants.HTTP+URL+"#!/changepassword/";
										String url = URL+userDO.getTemppassowrd()+"/"+userDO.getEmployee().getCompanyemail()+"/"+String.valueOf(empList.get(0).getEmpId())+"/N"+"/U";
										String emailBody = "Cick Here Login : " +URL+userDO.getTemppassowrd()+"/"+userDO.getEmployee().getCompanyemail()+"/N"+"/U";
								 		boolean mailStatus = EmailProxyUtil.sendEmail(request,host,  fromEmail,  toEmails, "", "", emailBody, false, url, "inviteUser", userName); 
										if(mailStatus){
											return CommonWebUtil.buildSuccessResponse().toString();
										}else{
											return CommonWebUtil.buildErrorResponse("").toString();
										}
									}
							 	}
							}
			 			}
					
					}	
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
}