package com.spheresuite.erp.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.CategoryDO;
import com.spheresuite.erp.service.CategoryService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CategoryUtil;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/category")
public class CategoryRS {

	String validation = null;
	static Logger logger = Logger.getLogger(CategoryRS.class.getName());

	@Autowired
	private CategoryService categoryService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				CategoryDO categoryDO = new CategoryDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
			 			categoryDO.setName(inputJSON.get(CommonConstants.NAME).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.DESC) != null && !inputJSON.get(CommonConstants.DESC).toString().isEmpty()){
			 			categoryDO.setDescripetion(inputJSON.get(CommonConstants.DESC).toString());
			 		}
			 		categoryDO.setStatus(CommonConstants.ACTIVE);
			 	
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			categoryDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			categoryDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		categoryDO.setUpdatedon(new Date());
			 		categoryDO.setCreatedon(new Date());
			 	}
				if(!categoryService.persist(categoryDO)){
		 			return CommonWebUtil.buildErrorResponse("Categery Already Added").toString();
				}
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Caregory Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<CategoryDO> cagtegoryList = categoryService.retrieveActive();
				respJSON = CategoryUtil.getCategoryList(cagtegoryList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<CategoryDO> categoryList = categoryService.retrieve();
				respJSON = CategoryUtil.getCategoryList(categoryList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				CategoryDO categoryDO = new CategoryDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<CategoryDO> categoryList = categoryService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		categoryDO = categoryList.get(0);
			 		if(categoryList != null && categoryList.size() > 0){
			 			if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
				 			categoryDO.setName(inputJSON.get(CommonConstants.NAME).toString());
				 		}
			 			if(inputJSON.get(CommonConstants.DESC) != null && !inputJSON.get(CommonConstants.DESC).toString().isEmpty()){
			 				categoryDO.setDescripetion(inputJSON.get(CommonConstants.DESC).toString());
				 		}
			 			if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
			 				categoryDO.setStatus((char) inputJSON.get(CommonConstants.STATUS).toString().charAt(0));
				 		}
			 			categoryDO.setUpdatedon(new Date());
				 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			categoryDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		if(!categoryService.update(categoryDO)){
				 			return CommonWebUtil.buildErrorResponse("Categery Already Added").toString();
				 		}
				 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Category Updated");
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/importCategory", method = RequestMethod.POST)
	public @ResponseBody String importCategory(Model model, HttpServletRequest request) {
		try {
			JSONObject inputJSON =  CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
			JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
			List<CategoryDO> categorylist = new ArrayList<CategoryDO>();
			Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY);
			for (int i=0; i < fileData.size(); i++){
				JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
				CategoryDO categoryDO = new CategoryDO();
				
				if (colName.get(CommonConstants.NAME) != null && !colName.get(CommonConstants.NAME).toString().isEmpty()){
					categoryDO.setName(rowJSON.get(colName.get(CommonConstants.NAME)).toString());
		 		}
				if (colName.get(CommonConstants.DESC) != null && !colName.get(CommonConstants.DESC).toString().isEmpty()){
					categoryDO.setDescripetion(rowJSON.get(colName.get(CommonConstants.DESC)).toString());
		 		}
		 		if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
		 			categoryDO.setUpdatedby(updatedBy.toString());
		 			categoryDO.setCreatedby(updatedBy.toString());
		 		}
		 		categoryDO.setUpdatedon(new Date());
		 		categoryDO.setCreatedon(new Date());
		 		categoryDO.setStatus(CommonConstants.ACTIVE);
		 		categorylist.add(categoryDO);
			}
			categoryService.persistList(categorylist);
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
