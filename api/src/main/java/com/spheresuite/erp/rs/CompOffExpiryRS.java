package com.spheresuite.erp.rs;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.CompOffExpiryDO;
import com.spheresuite.erp.service.CompOffExpiryService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.CompOffExpiryUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/compoffexpiry")
public class CompOffExpiryRS {

	String validation = null;
	static Logger logger = Logger.getLogger(CompOffExpiryRS.class.getName());
	@Autowired
	private CompOffExpiryService compOffExpiryService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				CompOffExpiryDO compOffExpiryDO = new CompOffExpiryDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.NUMBEROFDAYS) != null && !inputJSON.get(CommonConstants.NUMBEROFDAYS).toString().isEmpty()){
			 			compOffExpiryDO.setNoofdays(Long.parseLong(inputJSON.get(CommonConstants.NUMBEROFDAYS).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			compOffExpiryDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			compOffExpiryDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		compOffExpiryDO.setUpdatedon(new Date());
			 		compOffExpiryDO.setCreatedon(new Date());
			 	}
				if(!compOffExpiryService.persist(compOffExpiryDO)){
		 			return CommonWebUtil.buildErrorResponse("Earning Type Already Added").toString();
				}
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "CompOff ExpiryDays Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<CompOffExpiryDO> categoryList = compOffExpiryService.retrieve();
				respJSON = CompOffExpiryUtil.getCompOffList(categoryList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				CompOffExpiryDO compOffExpiryDO = new CompOffExpiryDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<CompOffExpiryDO> earningTypeList = compOffExpiryService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(earningTypeList != null && earningTypeList.size() >0){
				 		compOffExpiryDO = earningTypeList.get(0);
				 		if(inputJSON.get(CommonConstants.NUMBEROFDAYS) != null && !inputJSON.get(CommonConstants.NUMBEROFDAYS).toString().isEmpty()){
				 			compOffExpiryDO.setNoofdays(Long.parseLong(inputJSON.get(CommonConstants.NUMBEROFDAYS).toString()));
				 		}
				 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			compOffExpiryDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		compOffExpiryDO.setUpdatedon(new Date());
				 		compOffExpiryService.update(compOffExpiryDO);
				 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "CompOff ExpiryDays Updated");
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
}
