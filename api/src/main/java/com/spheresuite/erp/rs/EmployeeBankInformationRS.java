package com.spheresuite.erp.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.EmployeeBankInformationDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.service.EmployeeBankInformationService;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.EmployeeBankInformationUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/employeebankinformation")
public class EmployeeBankInformationRS {

	String validation = null;
	static Logger logger = Logger.getLogger(EmployeeBankInformationRS.class.getName());

	@Autowired
	private EmployeeBankInformationService employeeBankInformationService;
	
	@Autowired
	private EmployeeService employeeService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeBankInformationDO employeeBankInformationDO = new EmployeeBankInformationDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.EMPID).toString() != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
				 		if(employeeList != null && employeeList.size() > 0){
				 			employeeBankInformationDO.setEmployee(employeeList.get(0));
				 			if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			employeeBankInformationDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			employeeBankInformationDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			if(inputJSON.get(CommonConstants.ACCOUNTNAME) != null && !inputJSON.get(CommonConstants.ACCOUNTNAME).toString().isEmpty()){
						 			employeeBankInformationDO.setAccountName(inputJSON.get(CommonConstants.ACCOUNTNAME).toString());
						 		}
						 		if(inputJSON.get(CommonConstants.BANKNAME) != null && !inputJSON.get(CommonConstants.BANKNAME).toString().isEmpty()){
						 			employeeBankInformationDO.setBankName(inputJSON.get(CommonConstants.BANKNAME).toString());
						 		}
						 		if(inputJSON.get(CommonConstants.IFSCCODE) != null && !inputJSON.get(CommonConstants.IFSCCODE).toString().isEmpty()){
						 			employeeBankInformationDO.setIfscCode(inputJSON.get(CommonConstants.IFSCCODE).toString());
						 		}
						 		if(inputJSON.get(CommonConstants.BRANCHNAME) != null && !inputJSON.get(CommonConstants.BRANCHNAME).toString().isEmpty()){
						 			employeeBankInformationDO.setBranchName(inputJSON.get(CommonConstants.BRANCHNAME).toString());
						 		}
						 		
						 		if(inputJSON.get(CommonConstants.ACCOUNTNUMBER) != null && !inputJSON.get(CommonConstants.ACCOUNTNUMBER).toString().isEmpty()){
						 			employeeBankInformationDO.setAccountNumber(Long.parseLong(inputJSON.get(CommonConstants.ACCOUNTNUMBER).toString()));
						 		}
						 		
						 		employeeBankInformationDO.setUpdatedon(new Date());
						 		employeeBankInformationDO.setCreatedon(new Date());
						 		employeeBankInformationService.persist(employeeBankInformationDO);
								CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Bank Information Updated");
				 			}
				 		}
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retrieveByEmpId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByEmpId(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<EmployeeBankInformationDO> employeeAddressList = employeeBankInformationService.retrieveByEmpId(inputJSON.get(CommonConstants.ID).toString());
					respJSON = EmployeeBankInformationUtil.getEmployeeBankInfoList(employeeAddressList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeBankInformationDO employeeBankInformationDO = new EmployeeBankInformationDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<EmployeeBankInformationDO> employeeAddressList = employeeBankInformationService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		employeeBankInformationDO = employeeAddressList.get(0);
			 		if(inputJSON.get(CommonConstants.ACCOUNTNAME) != null && !inputJSON.get(CommonConstants.ACCOUNTNAME).toString().isEmpty()){
			 			employeeBankInformationDO.setAccountName(inputJSON.get(CommonConstants.ACCOUNTNAME).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.BANKNAME) != null && !inputJSON.get(CommonConstants.BANKNAME).toString().isEmpty()){
			 			employeeBankInformationDO.setBankName(inputJSON.get(CommonConstants.BANKNAME).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.IFSCCODE) != null && !inputJSON.get(CommonConstants.IFSCCODE).toString().isEmpty()){
			 			employeeBankInformationDO.setIfscCode(inputJSON.get(CommonConstants.IFSCCODE).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.BRANCHNAME) != null && !inputJSON.get(CommonConstants.BRANCHNAME).toString().isEmpty()){
			 			employeeBankInformationDO.setBranchName(inputJSON.get(CommonConstants.BRANCHNAME).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.EMPID).toString() != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
				 		if(employeeList != null && employeeList.size() > 0){
				 			employeeBankInformationDO.setEmployee(employeeList.get(0));
				 		}
			 		}
			 		if(inputJSON.get(CommonConstants.ACCOUNTNUMBER) != null && !inputJSON.get(CommonConstants.ACCOUNTNUMBER).toString().isEmpty()){
			 			employeeBankInformationDO.setAccountNumber(Long.parseLong(inputJSON.get(CommonConstants.ACCOUNTNUMBER).toString()));
			 		}
			 		employeeBankInformationDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			employeeBankInformationDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		employeeBankInformationService.update(employeeBankInformationDO);
			 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Bank Information Updated");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/importEmployeeBankInfo", method = RequestMethod.POST)
	public @ResponseBody String importCountry(Model model, HttpServletRequest request) {
		try {
				JSONObject inputJSON =  CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
				org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
				JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
				List<EmployeeBankInformationDO> empBankInfolist = new ArrayList<EmployeeBankInformationDO>();
				Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY);
				for (int i=0; i < fileData.size(); i++){
					JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
					EmployeeBankInformationDO empBankInfoDO = new EmployeeBankInformationDO();
					
					if(colName.get(CommonConstants.EMPID).toString() != null && !colName.get(CommonConstants.EMPID).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(rowJSON.get(colName.get(CommonConstants.EMPID)).toString());
				 		if(employeeList != null && employeeList.size() > 0){
				 			empBankInfoDO.setEmployee(employeeList.get(0));
				 		}
				 		if (colName.get(CommonConstants.ACCOUNTNAME) != null && !colName.get(CommonConstants.ACCOUNTNAME).toString().isEmpty()){
				 			empBankInfoDO.setAccountName(rowJSON.get(colName.get(CommonConstants.ACCOUNTNAME)).toString());
				 		}
				 		if (colName.get(CommonConstants.BANKNAME) != null && !colName.get(CommonConstants.BANKNAME).toString().isEmpty()){
				 			empBankInfoDO.setBankName(rowJSON.get(colName.get(CommonConstants.BANKNAME)).toString());
				 		}
				 		if (colName.get(CommonConstants.IFSCCODE) != null && !colName.get(CommonConstants.IFSCCODE).toString().isEmpty()){
				 			empBankInfoDO.setIfscCode(rowJSON.get(colName.get(CommonConstants.IFSCCODE)).toString());
				 		}
				 		if (colName.get(CommonConstants.BRANCHNAME) != null && !colName.get(CommonConstants.BRANCHNAME).toString().isEmpty()){
				 			empBankInfoDO.setBranchName(rowJSON.get(colName.get(CommonConstants.BRANCHNAME)).toString());
				 		}
				 		
				 		if (colName.get(CommonConstants.ACCOUNTNUMBER) != null && !colName.get(CommonConstants.ACCOUNTNUMBER).toString().isEmpty()){
				 			empBankInfoDO.setAccountNumber(Long.parseLong(rowJSON.get(colName.get(CommonConstants.ACCOUNTNUMBER)).toString()));
				 		}
				 		if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
				 			empBankInfoDO.setUpdatedby(updatedBy.toString());
				 			empBankInfoDO.setCreatedby(updatedBy.toString());
				 		}
				 		empBankInfoDO.setUpdatedon(new Date());
				 		empBankInfoDO.setCreatedon(new Date());
				 		empBankInfolist.add(empBankInfoDO);
			 		}
				}
				employeeBankInformationService.persistList(empBankInfolist);
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
