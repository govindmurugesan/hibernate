package com.spheresuite.erp.rs;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.DeductionTypeDO;
import com.spheresuite.erp.domainobject.DeductionSubTypeDO;
import com.spheresuite.erp.service.DeductionTypeService;
import com.spheresuite.erp.service.DeductionSubTypeService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.LoanTypeUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/loantype")
public class DeductionSubTypeRS {

	String validation = null;
	static Logger logger = Logger.getLogger(DeductionSubTypeRS.class.getName());
	
	@Autowired
	private DeductionSubTypeService loanTypeService;
	
	@Autowired
	private DeductionTypeService deductionTypeService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				DeductionSubTypeDO loanTypeDO = new DeductionSubTypeDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.DEDUCTION_TYPE_ID) != null && !inputJSON.get(CommonConstants.DEDUCTION_TYPE_ID).toString().isEmpty()){
			 			List<DeductionTypeDO> deductionTypeList = deductionTypeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.DEDUCTION_TYPE_ID).toString()));
			 			if(deductionTypeList != null && deductionTypeList.size() >0){
			 				loanTypeDO.setDeductionType(deductionTypeList.get(0));
			 				if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			loanTypeDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			loanTypeDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
						 			loanTypeDO.setName(inputJSON.get(CommonConstants.NAME).toString());
						 			loanTypeDO.setUpdatedon(new Date());
							 		loanTypeDO.setStatus(CommonConstants.ACTIVESTRING);
							 	}
								if(!loanTypeService.persist(loanTypeDO)){
									return CommonWebUtil.buildErrorResponse("Deduction Sub Type Already Added").toString();
								}
								CommonUtil.persistRecentAcvtivity(inputJSON.get(CommonConstants.UPDATED_BY).toString(), "New Deduction Sub Type Created");
						 		}
					 		}
			 			}
			 		}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	/*@RequestMapping(value = "/retrieveActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<AdvanceTypeDO> addressTypeList = advanceTypeService.retrieveActive();
				respJSON = AddressTypeUtil.getAddressTypeList(addressTypeList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}*/
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<DeductionSubTypeDO> loanTypeList = loanTypeService.retrieve();
				respJSON = LoanTypeUtil.getLoanTypeList(loanTypeList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveByDeductionTypeId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByDeductionTypeId(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<DeductionSubTypeDO> loanTypeList = loanTypeService.retrieveByDeductionTypeId(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = LoanTypeUtil.getLoanTypeList(loanTypeList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				DeductionSubTypeDO loanTypeDO = new DeductionSubTypeDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<DeductionSubTypeDO> loanTypeList = loanTypeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(loanTypeList != null && loanTypeList.size() > 0){
			 			loanTypeDO = loanTypeList.get(0);
				 		if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
				 			loanTypeDO.setName(inputJSON.get(CommonConstants.NAME).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.DEDUCTION_TYPE_ID) != null && !inputJSON.get(CommonConstants.DEDUCTION_TYPE_ID).toString().isEmpty()){
				 			List<DeductionTypeDO> deductionTypeList = deductionTypeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.DEDUCTION_TYPE_ID).toString()));
				 			if(deductionTypeList != null && deductionTypeList.size() >0){
				 				loanTypeDO.setDeductionType(deductionTypeList.get(0));
				 			}
				 		}
				 		loanTypeDO.setUpdatedon(new Date());
				 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			loanTypeDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
				 			loanTypeDO.setStatus((inputJSON.get(CommonConstants.STATUS).toString()));
				 		}
				 		if(!loanTypeService.update(loanTypeDO)){
							return CommonWebUtil.buildErrorResponse("Loan Type Already Added").toString();
						}
				 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Loan Type Updated");
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	/*@RequestMapping(value = "/importAddressType", method = RequestMethod.POST)
	public @ResponseBody String importAddressType(Model model, HttpServletRequest request) {
		try {
			JSONObject inputJSON =  CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
			JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
			List<AdvanceTypeDO> AddressTypeList = new ArrayList<AdvanceTypeDO>();
			Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY);
			for (int i=0; i < fileData.size(); i++){
				JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
				AdvanceTypeDO AdvanceTypeDO = new AdvanceTypeDO();
				if (colName.get(CommonConstants.NAME) != null && !colName.get(CommonConstants.NAME).toString().isEmpty()){
					AdvanceTypeDO.setName(rowJSON.get(colName.get(CommonConstants.NAME)).toString());
		 		}
		 		if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
		 			AdvanceTypeDO.setUpdatedby(updatedBy.toString());
		 			AdvanceTypeDO.setCreatedby(updatedBy.toString());
		 		}
		 		AdvanceTypeDO.setUpdatedon(new Date());
		 		AdvanceTypeDO.setCreatedon(new Date());
		 		AdvanceTypeDO.setStatus(CommonConstants.ACTIVE);
		 		AddressTypeList.add(AdvanceTypeDO);
			}
			addressTypeService.persistList(AddressTypeList);
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}*/
}
