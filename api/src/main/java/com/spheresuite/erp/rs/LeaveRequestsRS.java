package com.spheresuite.erp.rs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.EmployeeCompOffDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.EmployeeLeavegroupDO;
import com.spheresuite.erp.domainobject.LeaveGroupSettingsDO;
import com.spheresuite.erp.domainobject.LeaveManagementDO;
import com.spheresuite.erp.domainobject.LeaveRequestsDO;
import com.spheresuite.erp.domainobject.MonthlyPermissionDO;
import com.spheresuite.erp.domainobject.NotificationSettingsDO;
import com.spheresuite.erp.domainobject.UserDO;
import com.spheresuite.erp.service.EmployeeCompOffService;
import com.spheresuite.erp.service.EmployeeLeavegroupService;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.LeaveGroupSettingsService;
import com.spheresuite.erp.service.LeaveManagementService;
import com.spheresuite.erp.service.LeaveRequestsService;
import com.spheresuite.erp.service.MonthlyPermissionService;
import com.spheresuite.erp.service.NotificationSettingsService;
import com.spheresuite.erp.service.UserService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.LeaveRequestsUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/leaverequests")
public class LeaveRequestsRS {

	String validation = null;
	static Logger logger = Logger.getLogger(LeaveRequestsRS.class.getName());
	
	
	@Autowired
	private LeaveRequestsService leaveRequestsService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private EmployeeService employeeService;
	
	@Autowired
	private LeaveManagementService leaveManagementService;
	
	/*@Autowired
	private LeaveTypeService leaveTypeService;*/
	
	@Autowired
	private EmployeeLeavegroupService employeeleaveGroupService;
	
	@Autowired
	private LeaveGroupSettingsService leaveGroupSettingService;
	
	@Autowired
	private NotificationSettingsService notificationSettingsService;
	
	@Autowired
	private MonthlyPermissionService monthlyPermissionService;
	
	@Autowired
	private EmployeeCompOffService employeeCompOffService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		LeaveRequestsDO leaveRequestDO = new LeaveRequestsDO(); 
				 			if(inputJSON.get(CommonConstants.EMPID) != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
					 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
						 		if(employeeList != null && employeeList.size() > 0){
						 			leaveRequestDO.setEmployee(employeeList.get(0));
						 				List<EmployeeLeavegroupDO> leaveGroupList = employeeleaveGroupService.retrieveByEmpId(employeeList.get(0).getEmpId().toString());
						 				if(leaveGroupList != null && leaveGroupList.size() > 0){
						 					leaveRequestDO.setLeaveGroup(leaveGroupList.get(0).getLeaveGroup());
						 					if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
								 				leaveRequestDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
								 				leaveRequestDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
								 				if(inputJSON.get(CommonConstants.DESC) != null && !inputJSON.get(CommonConstants.DESC).toString().isEmpty()){
										 			leaveRequestDO.setDescription(inputJSON.get(CommonConstants.DESC).toString());
										 		}
								 				if(inputJSON.get(CommonConstants.TYPE_ID) != null && !inputJSON.get(CommonConstants.TYPE_ID).toString().isEmpty()){
											 		List<LeaveManagementDO> leaveTypeList = leaveManagementService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.TYPE_ID).toString()));
											 		if(leaveTypeList != null && leaveTypeList.size() > 0){
											 			leaveRequestDO.setLeaveManagement(leaveTypeList.get(0));
											 		}
								 				}
										 		if(inputJSON.get(CommonConstants.PERMISSIONFROM) != null && !inputJSON.get(CommonConstants.PERMISSIONFROM).toString().isEmpty()){
										 			leaveRequestDO.setPermisssionformhours(CommonUtil.convertStringToDateTimePick(inputJSON.get(CommonConstants.PERMISSIONFROM).toString()));
										 		}
										 		
										 		if(inputJSON.get(CommonConstants.PERMISSIONTO) != null && !inputJSON.get(CommonConstants.PERMISSIONTO).toString().isEmpty()){
										 			leaveRequestDO.setPermisssiontohours(CommonUtil.convertStringToDateTimePick(inputJSON.get(CommonConstants.PERMISSIONTO).toString()));
										 		}
										 		
										 		if(inputJSON.get(CommonConstants.DESC) != null && !inputJSON.get(CommonConstants.DESC).toString().isEmpty()){
										 			leaveRequestDO.setDescription(inputJSON.get(CommonConstants.DESC).toString());
										 		}
										 		
										 		if(inputJSON.get(CommonConstants.REPORTTO_EMAIL) != null && !inputJSON.get(CommonConstants.REPORTTO_EMAIL).toString().isEmpty()){
										 			leaveRequestDO.setReportingTo(inputJSON.get(CommonConstants.REPORTTO_EMAIL).toString());
										 		}
									 			if(inputJSON.get(CommonConstants.LEAVEDAY) != null && !inputJSON.get(CommonConstants.LEAVEDAY).toString().isEmpty()){
									 				leaveRequestDO.setType(inputJSON.get(CommonConstants.LEAVEDAY).toString());
										 		}
									 			if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
									 				leaveRequestDO.setStatus((char) inputJSON.get(CommonConstants.STATUS).toString().charAt(0));
									 			}
									 			if(inputJSON.get(CommonConstants.HOURS) != null  && !inputJSON.get(CommonConstants.HOURS).toString().isEmpty()){
									 				leaveRequestDO.setNumberofhours(inputJSON.get(CommonConstants.HOURS).toString());
										 		}
									 			if(inputJSON.get(CommonConstants.FROMDATE) != null  && !inputJSON.get(CommonConstants.FROMDATE).toString().isEmpty()){
									 				leaveRequestDO.setFromDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.FROMDATE).toString()));
									 			}
									 			if(inputJSON.get(CommonConstants.TODATE) != null  && !inputJSON.get(CommonConstants.TODATE).toString().isEmpty()){
									 				leaveRequestDO.setToDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.TODATE).toString()));
									 			}
									 			if(inputJSON.get(CommonConstants.LEAVETYPEFROMDATE) != null  && !inputJSON.get(CommonConstants.LEAVETYPEFROMDATE).toString().isEmpty()){
									 				leaveRequestDO.setLeavetypefromDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.LEAVETYPEFROMDATE).toString()));
									 				//leaveRequestDO.setLeavetypefromDate(inputJSON.get(CommonConstants.LEAVETYPEFROMDATE).toString());
									 			}
									 			if(inputJSON.get(CommonConstants.LEAVETYPETODATE) != null  && !inputJSON.get(CommonConstants.LEAVETYPETODATE).toString().isEmpty()){
									 				leaveRequestDO.setLeavetypeToDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.LEAVETYPETODATE).toString()));
									 				//leaveRequestDO.setLeavetypeToDate(inputJSON.get(CommonConstants.LEAVETYPETODATE).toString());
									 			}
									 			leaveRequestDO.setUpdatedon(new Date());
										 		leaveRequestDO.setCreatedon(new Date());
									 			leaveRequestsService.persist(leaveRequestDO);
									 			CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Leave Request Submitted");
									 			CommonUtil.sendMailToReportingHeads(request,inputJSON,"leave/companiesrequests","leaveRequest","pending");
									 		}
						 				}
						 		}
					 		}
				 			
				 		
			 		
			 		/*if(inputJSON.get(CommonConstants.TYPE_ID) != null && !inputJSON.get(CommonConstants.TYPE_ID).toString().isEmpty()){
				 			leaveRequestDO.setLeaveType(inputJSON.get(CommonConstants.TYPE_ID).toString());
			 		}*/
			 		
			 	}
			}else{
		 		return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
		 	}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request)  {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<LeaveRequestsDO> leaveRequestsList = leaveRequestsService.retrieve();
				respJSON = LeaveRequestsUtil.getLeaveRequestsList(leaveRequestsList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && inputJSON.get(CommonConstants.ID) != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 			LeaveRequestsDO leaveRequestDO = new LeaveRequestsDO(); 
		 				List<LeaveRequestsDO> leaveRequestList = leaveRequestsService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
		 				if(leaveRequestList != null && leaveRequestList.size() > 0){
		 					leaveRequestDO = leaveRequestList.get(0);
		 					/*if(inputJSON.get(CommonConstants.TYPE) != null && !inputJSON.get(CommonConstants.TYPE).toString().isEmpty()){
						 		List<LeaveTypeDO> leaveTypeList = leaveTypeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.TYPE).toString()));
						 		if(leaveTypeList != null && leaveTypeList.size() > 0){
						 			leaveRequestDO.setLeaveType(leaveTypeList.get(0));
						 		}
					 		}*/
		 					
		 					if(inputJSON.get(CommonConstants.TYPE) != null && !inputJSON.get(CommonConstants.TYPE).toString().isEmpty()){
		 						List<LeaveManagementDO> leaveTypeList = leaveManagementService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.TYPE).toString()));
						 		if(leaveTypeList != null && leaveTypeList.size() > 0){
						 			leaveRequestDO.setLeaveManagement(leaveTypeList.get(0));
						 		}
					 		}
		 					
		 					if(inputJSON.get(CommonConstants.PERMISSIONFROM) != null && !inputJSON.get(CommonConstants.PERMISSIONFROM).toString().isEmpty()){
					 			leaveRequestDO.setPermisssionformhours(CommonUtil.convertStringToDateTimePick(inputJSON.get(CommonConstants.PERMISSIONFROM).toString()));
					 		}
					 		
					 		if(inputJSON.get(CommonConstants.PERMISSIONTO) != null && !inputJSON.get(CommonConstants.PERMISSIONTO).toString().isEmpty()){
					 			leaveRequestDO.setPermisssiontohours(CommonUtil.convertStringToDateTimePick(inputJSON.get(CommonConstants.PERMISSIONTO).toString()));
					 		}
		 					
		 					if(inputJSON.get(CommonConstants.DESC) != null && !inputJSON.get(CommonConstants.DESC).toString().isEmpty()){
					 			leaveRequestDO.setDescription(inputJSON.get(CommonConstants.DESC).toString());
					 		}
					 		
				 			if(inputJSON.get(CommonConstants.LEAVEDAY) != null && !inputJSON.get(CommonConstants.LEAVEDAY).toString().isEmpty()){
				 				leaveRequestDO.setType(inputJSON.get(CommonConstants.LEAVEDAY).toString());
					 		}
				 			if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
				 				leaveRequestDO.setStatus((char) inputJSON.get(CommonConstants.STATUS).toString().charAt(0));
					 		}
				 			if(inputJSON.get(CommonConstants.HOURS) != null){
				 				leaveRequestDO.setNumberofhours(inputJSON.get(CommonConstants.HOURS).toString());
					 		}
				 			if(inputJSON.get(CommonConstants.COMMENT) != null && !inputJSON.get(CommonConstants.COMMENT).toString().isEmpty()){
				 				leaveRequestDO.setComments(inputJSON.get(CommonConstants.COMMENT).toString());
				 			}
				 			/*if(inputJSON.get(CommonConstants.FROMDATE) != null  && !inputJSON.get(CommonConstants.FROMDATE).toString().isEmpty()){
				 				leaveRequestDO.setFromDate(inputJSON.get(CommonConstants.FROMDATE).toString());
				 			}
				 			if(inputJSON.get(CommonConstants.TODATE) != null  && !inputJSON.get(CommonConstants.TODATE).toString().isEmpty()){
				 				leaveRequestDO.setToDate(inputJSON.get(CommonConstants.TODATE).toString());
				 			}*/
				 			
				 			
				 			
				 			
				 			/*if(inputJSON.get(CommonConstants.FROMDATE) != null  && !inputJSON.get(CommonConstants.FROMDATE).toString().isEmpty()){
				 				leaveRequestDO.setFromDate((CommonConstants.FROMDATE).toString());
				 			}*/
				 			/*if(leaveRequestDO.getFromDate() == null && inputJSON.get(CommonConstants.FROMDATE) != null){
				 				leaveRequestDO.setFromDate(inputJSON.get(CommonConstants.FROMDATE).toString());
				 			}*/
				 			/*if(inputJSON.get(CommonConstants.TODATE) != null  && !inputJSON.get(CommonConstants.TODATE).toString().isEmpty()){
				 				leaveRequestDO.setToDate(inputJSON.get(CommonConstants.TODATE).toString());
				 			}*/
				 			/*if(leaveRequestDO.getToDate() == null && inputJSON.get(CommonConstants.TODATE) != null){
				 				leaveRequestDO.setToDate(inputJSON.get(CommonConstants.TODATE).toString());
				 			}*/
				 			if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 				leaveRequestDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 		}
				 			/*if(inputJSON.get(CommonConstants.LEAVETYPEFROMDATE) != null){
				 				leaveRequestDO.setLeavetypefromDate(inputJSON.get(CommonConstants.LEAVETYPEFROMDATE).toString());
				 			}
				 			if(inputJSON.get(CommonConstants.LEAVETYPETODATE) != null){
				 				leaveRequestDO.setLeavetypeToDate(inputJSON.get(CommonConstants.LEAVETYPETODATE).toString());
				 			}*/
				 			leaveRequestDO.setUpdatedon(new Date());
				 			leaveRequestsService.update(leaveRequestDO);
				 			CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Leave Request Updated");
				 			if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty() && inputJSON.get(CommonConstants.STATUS).toString().equalsIgnoreCase("n")){
				 				CommonUtil.sendMailToReportingHeads(request,inputJSON,"leave/companiesrequests","leaveRequest", "cancel Requrst");
				 			}
				 			
				 			
				 			
		 				}
			 		}
			 	}
				
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/updateStatus", method = RequestMethod.POST)
	public @ResponseBody String updateStatus(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && inputJSON.get(CommonConstants.ID) != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 			LeaveRequestsDO leaveRequestDO = new LeaveRequestsDO(); 
		 				List<LeaveRequestsDO> leaveRequestList = leaveRequestsService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
		 				if(leaveRequestList != null && leaveRequestList.size() > 0){
		 					leaveRequestDO = leaveRequestList.get(0);
					 		leaveRequestDO.setUpdatedon(new Date());
				 			if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 				leaveRequestDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 		}
				 			leaveRequestDO.setStatus((char) (inputJSON != null ? inputJSON.get(CommonConstants.STATUS).toString().charAt(0) : null));
				 			leaveRequestDO.setComments(!inputJSON.get(CommonConstants.COMMENT).toString().isEmpty() ? inputJSON.get(CommonConstants.COMMENT).toString() : null);
				 			
				 			
				 			if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty() && inputJSON.get(CommonConstants.STATUS).toString().equalsIgnoreCase("a")){
				 				if(leaveRequestDO.getType().equalsIgnoreCase(CommonConstants.CHTYPE) || leaveRequestDO.getType().equalsIgnoreCase(CommonConstants.CFTYPE)){
				 					List <EmployeeCompOffDO> empCompOffList =  employeeCompOffService.retrieveByEmpCompOffWithTypeExpiry(leaveRequestDO.getEmployee().getEmpId(),leaveRequestDO.getFromDate(),leaveRequestDO.getType().charAt(1));
				 					if(empCompOffList != null && empCompOffList.size()>0){
				 						leaveRequestsService.update(leaveRequestDO);
				 						EmployeeCompOffDO empCompOff =    empCompOffList.get(0);
				 						empCompOff.setStatus(CommonConstants.INACTIVE);
				 						employeeCompOffService.update(empCompOff);
				 					}else{
				 						return CommonWebUtil.buildErrorResponse("Since Employee Is Not Eligible For This Comp Off, You Can't Approve").toString();
				 					}
				 				}else{
				 					leaveRequestsService.update(leaveRequestDO);
				 				}
				 			}else{
				 				leaveRequestsService.update(leaveRequestDO);
				 			}
				 			if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty() && inputJSON.get(CommonConstants.STATUS).toString().equalsIgnoreCase("c")){
				 				CommonUtil.sendMailToReportingHeads(request,inputJSON,"leaves","leaveRequest", "cancel");
				 			}
				 			if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty() && inputJSON.get(CommonConstants.STATUS).toString().equalsIgnoreCase("a")){
				 				if(leaveRequestDO.getType().equalsIgnoreCase(CommonConstants.CHTYPE) || leaveRequestDO.getType().equalsIgnoreCase(CommonConstants.CFTYPE)){
				 					List <EmployeeCompOffDO> empCompOffList =  employeeCompOffService.retrieveByEmpCompOffWithTypeExpiry(leaveRequestDO.getEmployee().getEmpId(),leaveRequestDO.getFromDate(),leaveRequestDO.getType().charAt(1));
				 					if(empCompOffList != null && empCompOffList.size()>0){
				 						EmployeeCompOffDO empCompOff =    empCompOffList.get(0);
				 						empCompOff.setStatus(CommonConstants.INACTIVE);
				 						employeeCompOffService.update(empCompOff);
				 					}
				 				}
				 			
				 				
				 				CommonUtil.sendMailToReportingHeads(request,inputJSON,"leaves","leaveRequest", "approved");
				 			}
				 			if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty() && inputJSON.get(CommonConstants.STATUS).toString().equalsIgnoreCase("r")){
				 				CommonUtil.sendMailToReportingHeads(request,inputJSON,"leaves","leaveRequest", "rejected");
				 			}
				 			//CommonUtil.sendMailToReportingHeads(request,inputJSON,"leave/companiesrequests","leaveRequest");
				 			CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Leave Request Updated");
		 				}
			 		}
			 	}
				
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveByEmpId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByEmpId(@PathVariable String inputParams, Model model, HttpServletRequest request)  {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
					List<LeaveRequestsDO> leaveRequestsList = leaveRequestsService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
					respJSON = LeaveRequestsUtil.getLeaveRequestsList(leaveRequestsList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveAllPermissionForEmpForMonth", method = RequestMethod.POST)
	public @ResponseBody String retrieveAllPermissionForEmpForMonth(Model model, HttpServletRequest request)  {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
				if(inputJSON != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty() && !inputJSON.get(CommonConstants.DATE).toString().isEmpty()){
					String fromDate = CommonUtil.convertDateToYearWithOutTime(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.DATE).toString()));
					if(fromDate != null){
						String date[] = fromDate.split("-");
						
 						List<LeaveRequestsDO> leaveRequestsList = leaveRequestsService.retrieveAllPermissionForEmpForMonth(inputJSON.get(CommonConstants.EMPID).toString(),Integer.parseInt(date[0]),Integer.parseInt(date[1]));
 						List<MonthlyPermissionDO> monthlypermissionDO = monthlyPermissionService.retrieveActive();
 						if(leaveRequestsList != null && leaveRequestsList.size() > 0){
 							if(monthlypermissionDO != null && monthlypermissionDO.size() > 0){
 	 							if(monthlypermissionDO.get(0).getNumberOfPermissionPerMonth() != null){
 	 								if(leaveRequestsList.size() >= monthlypermissionDO.get(0).getNumberOfPermissionPerMonth()){
 	 									return CommonWebUtil.buildErrorResponse("").toString();
 	 								}else{
 	 									String totalPermissionTaken = LeaveRequestsUtil.getLeaveRequestsListTime(leaveRequestsList).toString();
 	 									if(totalPermissionTaken != null){
 	 										ArrayList<String> timestampsList = new ArrayList<String>();
 	 										timestampsList.add(totalPermissionTaken);
 	 										timestampsList.add(inputJSON.get(CommonConstants.HOURS).toString()+":00");
 	 										String balancePermissionTime = CommonUtil.totalHours(timestampsList);
 	 										if(monthlypermissionDO.get(0).getPermissionHoursPerMonth() != null){
 	 											String balancePermissionTimeArray [] = balancePermissionTime.split(":");
 	 											String monthlyPermissionTimeArray [] =  monthlypermissionDO.get(0).getPermissionHoursPerMonth().split(":");
 	 											if(Integer.parseInt(balancePermissionTimeArray[0]) <  Integer.parseInt(monthlyPermissionTimeArray[0])){
 	 												return CommonWebUtil.buildSuccessResponse().toString();
 	 											}else if(Integer.parseInt(balancePermissionTimeArray[0]) == Integer.parseInt(monthlyPermissionTimeArray[0])){
 	 												if(Integer.parseInt(balancePermissionTimeArray[1]) <= Integer.parseInt(monthlyPermissionTimeArray[1])){
 	 													return CommonWebUtil.buildSuccessResponse().toString();
 	 												}else{
 	 													return CommonWebUtil.buildErrorResponse("").toString();
 	 												}
 	 											}else{
 	 												return CommonWebUtil.buildErrorResponse("").toString();
 	 											}
 	 										}else{
 	 											return CommonWebUtil.buildErrorResponse("").toString();
 	 										}
 	 									}
 	 								}
 	 							}else{
 									String totalPermissionTaken = LeaveRequestsUtil.getLeaveRequestsListTime(leaveRequestsList).toString();
 									if(totalPermissionTaken != null){
 										ArrayList<String> timestampsList = new ArrayList<String>();
 										timestampsList.add(totalPermissionTaken);
 										timestampsList.add(inputJSON.get(CommonConstants.HOURS).toString()+":00");
 										String balancePermissionTime = CommonUtil.totalHours(timestampsList);
 										if(monthlypermissionDO.get(0).getPermissionHoursPerMonth() != null){
 											String balancePermissionTimeArray [] = balancePermissionTime.split(":");
 											String monthlyPermissionTimeArray [] =  monthlypermissionDO.get(0).getPermissionHoursPerMonth().split(":");
 											if(Integer.parseInt(balancePermissionTimeArray[0]) <  Integer.parseInt(monthlyPermissionTimeArray[0])){
 												return CommonWebUtil.buildSuccessResponse().toString();
 											}else if(Integer.parseInt(balancePermissionTimeArray[0]) == Integer.parseInt(monthlyPermissionTimeArray[0])){
 												if(Integer.parseInt(balancePermissionTimeArray[1]) <= Integer.parseInt(monthlyPermissionTimeArray[1])){
 													return CommonWebUtil.buildSuccessResponse().toString();
 												}else{
 													return CommonWebUtil.buildErrorResponse("").toString();
 												}
 											}else{
 												return CommonWebUtil.buildErrorResponse("").toString();
 											}
 										}else{
 											return CommonWebUtil.buildErrorResponse("").toString();
 										}
 									}
 	 							}
 	 						}else{
 	 							return CommonWebUtil.buildSuccessResponse().toString();
 	 						}
 						}else{
 							return CommonWebUtil.buildSuccessResponse().toString();
 						}
					}
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveById/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveById(@PathVariable String inputParams, Model model, HttpServletRequest request)  {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<LeaveRequestsDO> leaveRequestsList = leaveRequestsService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = LeaveRequestsUtil.getLeaveRequestsList(leaveRequestsList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveAvailableLeavesByEmpId", method = RequestMethod.POST)
	public @ResponseBody String retrieveAvailableLeavesByEmpId(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
				if (inputJSON != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
					List<LeaveRequestsDO> leaveRequestsList = leaveRequestsService.retrieveAvailableLeavesByEmpId(inputJSON.get(CommonConstants.EMPID).toString(),CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.LEAVETYPEFROMDATE).toString()),CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.LEAVETYPETODATE).toString()),Long.parseLong(inputJSON.get(CommonConstants.TYPE).toString()));
					respJSON = LeaveRequestsUtil.getEmpLeaveRequestsList(leaveRequestsList).toString();
			 	}
			}
			 	}catch (Exception e) {
					e.printStackTrace();
					return CommonWebUtil.buildErrorResponse("").toString();
				}
				return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrievePermissionByEmpIdDate", method = RequestMethod.POST)
	public @ResponseBody String retrievePermissionByEmpIdDate(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
				if (inputJSON != null && inputJSON.get(CommonConstants.EMPID) != null && inputJSON.get(CommonConstants.DATE) != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty() && !inputJSON.get(CommonConstants.DATE).toString().isEmpty() ){
					Date date = CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.DATE).toString());
			        Calendar cal = Calendar.getInstance();
			        cal.setTime(date);
			        int year = cal.get(Calendar.YEAR);
			        int month = cal.get(Calendar.MONTH) ;
			        Calendar calendarStart=Calendar.getInstance();
				    calendarStart.set(Calendar.YEAR,year);
				    calendarStart.set(Calendar.MONTH,month);
				    calendarStart.set(Calendar.DAY_OF_MONTH,1);
				    
				    Calendar calendarEnd=Calendar.getInstance();
				    calendarEnd.set(Calendar.YEAR,year);
				    calendarEnd.set(Calendar.MONTH,month);
				    calendarEnd.set(Calendar.DAY_OF_MONTH,cal.getActualMaximum(Calendar.DAY_OF_MONTH));
				    
					List<LeaveRequestsDO> leaveRequestsList = leaveRequestsService.retrievePermissionByMonth(inputJSON.get(CommonConstants.EMPID).toString(),calendarStart.getTime(),calendarEnd.getTime(),CommonConstants.P_STATE);
					respJSON = LeaveRequestsUtil.getLeaveRequestsList(leaveRequestsList).toString();
			 	}
			}
			 	}catch (Exception e) {
					e.printStackTrace();
					return CommonWebUtil.buildErrorResponse("").toString();
				}
				return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	
/*	@RequestMapping(value = "/retrieveAvailableLeavesByEmpId", method = RequestMethod.POST)
	public @ResponseBody String retrieveAvailableLeavesByEmpId(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
				if (inputJSON != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
					List<LeaveRequestsDO> leaveRequestsList = leaveRequestsService.retrieveAvailableLeavesByEmpId(inputJSON.get(CommonConstants.EMPID).toString(),CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.LEAVETYPEFROMDATE).toString()),CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.LEAVETYPETODATE).toString()),Long.parseLong(inputJSON.get(CommonConstants.TYPE).toString()));
					respJSON = LeaveRequestsUtil.getEmpLeaveRequestsList(leaveRequestsList).toString();
			 	}
			}
			 	}catch (Exception e) {
					e.printStackTrace();
					return CommonWebUtil.buildErrorResponse("").toString();
				}
				return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}*/
	/*@RequestMapping(value = "/retrieveAvailableLeavesByEmpId/{inputParams}", method = RequestMethod.POST)
	public @ResponseBody String retrieveAvailableLeavesByEmpId(@PathVariable String inputParams, Model model, HttpServletRequest request)  {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
					
					List<LeaveRequestsDO> leaveRequestsList = leaveRequestsService.retrieveAvailableLeavesByEmpId(inputJSON.get(CommonConstants.EMPID).toString(),CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.LEAVETYPEFROMDATE).toString()),CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.LEAVETYPETODATE).toString()),Long.parseLong(inputJSON.get(CommonConstants.TYPE).toString()));
					respJSON = LeaveRequestsUtil.getEmpLeaveRequestsList(leaveRequestsList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	*/
	

	@RequestMapping(value = "/retrieveLeaveStatus", method = RequestMethod.GET)
	public @ResponseBody String retrieveLeaveStatus(Model model, HttpServletRequest request)  {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				//List<LeaveManagementDO> leaveList = leaveManagementService.retrieveAllActive();
				List<UserDO> activeUsersList = userService.retrieveActive();
				//respJSON = LeaveRequestsUtil.getLeaveStatus(activeUsersList,leaveList).toString();
				respJSON = LeaveRequestsUtil.getLeaveStatus(activeUsersList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveByApproverId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByApproverId(@PathVariable String inputParams, Model model, HttpServletRequest request)  {
		String respJSON = null;
		Boolean isApprover = false;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<NotificationSettingsDO> notificationSettingsList = notificationSettingsService.retrieve();
	 				if(notificationSettingsList != null && notificationSettingsList.size() >0){
	 					if(notificationSettingsList.get(0).getLeaves() != null){
	 						List<String> leaveApproverList = Arrays.asList(notificationSettingsList.get(0).getLeaves().split(CommonConstants.COMMA));
	 						if(leaveApproverList !=null && leaveApproverList.size() > 0){
	 							for(String approver : leaveApproverList ){
	 								if(approver.equalsIgnoreCase(inputJSON.get(CommonConstants.ID).toString())){
	 									isApprover = true;
	 									List<LeaveRequestsDO> leaveRequestsList = leaveRequestsService.retrieve();
	 									respJSON = LeaveRequestsUtil.getLeaveRequestsList(leaveRequestsList).toString();
	 								}
	 							}
	 							if(!isApprover){
	 								List<EmployeeDO> employeeDO = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.ID).toString());
	 			 					if(employeeDO != null && employeeDO.size() > 0 && employeeDO.get(0).getCompanyemail() !=null){
	 			 						List<LeaveRequestsDO> leaveRequestList = leaveRequestsService.retrieveByReportingTo(employeeDO.get(0).getCompanyemail());
	 									respJSON = LeaveRequestsUtil.getLeaveRequestsList(leaveRequestList).toString();
	 			 					}
	 							}
	 						}
	 					}
	 				}else{
	 					List<EmployeeDO> employeeDO = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.ID).toString());
	 					if(employeeDO != null && employeeDO.size() > 0 && employeeDO.get(0).getCompanyemail() !=null){
	 						List<LeaveRequestsDO> leaveRequestList = leaveRequestsService.retrieveByReportingTo(employeeDO.get(0).getCompanyemail());
							respJSON = LeaveRequestsUtil.getLeaveRequestsList(leaveRequestList).toString();
	 					}
	 					
	 				}
				}else{
					return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	
	@RequestMapping(value = "/retrieveByFilter", method = RequestMethod.POST)
	public @ResponseBody String retrieveByFilter(Model model, HttpServletRequest request)  {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
				//JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null){
					Long type = null;
					if(inputJSON.get(CommonConstants.TYPE) != null && !inputJSON.get(CommonConstants.TYPE).toString().isEmpty()){
						type = Long.parseLong(inputJSON.get(CommonConstants.TYPE).toString());
					}
					
					if((inputJSON.get(CommonConstants.FROMDATE) != null && !inputJSON.get(CommonConstants.FROMDATE).toString().isEmpty()) && (inputJSON.get(CommonConstants.EMPID) == null) && (inputJSON.get(CommonConstants.TODATE) == null)){
						/*int year= Integer.parseInt(inputJSON.get(CommonConstants.FROMDATE).toString());
						Calendar satrtDate = Calendar.getInstance();    
						satrtDate.set(year,0,1); 
						satrtDate.set(Calendar.DAY_OF_MONTH, satrtDate.getActualMinimum(Calendar.DAY_OF_MONTH));
						Calendar endDate = Calendar.getInstance(); 
						endDate.set(year,11,1); 
						endDate.set(Calendar.DAY_OF_MONTH, endDate.getActualMaximum(Calendar.DAY_OF_MONTH));*/
						
						List<UserDO> activeUsersList = userService.retrieveActive();
						if(activeUsersList != null && activeUsersList.size() > 0){
							//List<LeaveTypeDO> leaveTypeList = leaveTypeService.retrieveActive();
					 			//respJSON = LeaveRequestsUtil.getLeaveRequestsDetailForFilter(activeUsersList, satrtDate.getTime(), endDate.getTime(), leaveTypeList).toString();
							if(inputJSON.get(CommonConstants.UNIT) != null && !inputJSON.get(CommonConstants.UNIT).toString().isEmpty()){
								respJSON = LeaveRequestsUtil.getLeaveRequestsDetailForFilter(activeUsersList, CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.FROMDATE).toString()), null, Long.parseLong(inputJSON.get(CommonConstants.UNIT).toString()), type).toString();
							}else{
								respJSON = LeaveRequestsUtil.getLeaveRequestsDetailForFilter(activeUsersList, CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.FROMDATE).toString()), null, null, type).toString();
							}
							
							
						}
						return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
					}
					if((inputJSON.get(CommonConstants.FROMDATE) != null && !inputJSON.get(CommonConstants.FROMDATE).toString().isEmpty()) && (inputJSON.get(CommonConstants.EMPID) == null) && (inputJSON.get(CommonConstants.TODATE) != null || !inputJSON.get(CommonConstants.TODATE).toString().isEmpty())){
						/*int year= Integer.parseInt(inputJSON.get(CommonConstants.YEAR).toString());
						int month= Integer.parseInt(inputJSON.get(CommonConstants.MONTHLY).toString());
						Calendar satrtDate = Calendar.getInstance();    
						satrtDate.set(year,month,1); 
						satrtDate.set(Calendar.DAY_OF_MONTH, satrtDate.getActualMinimum(Calendar.DAY_OF_MONTH));
						Calendar endDate = Calendar.getInstance(); 
						endDate.set(year,month,1); 
						endDate.set(Calendar.DAY_OF_MONTH, endDate.getActualMaximum(Calendar.DAY_OF_MONTH));*/
						List<UserDO> activeUsersList = userService.retrieveActive();
						if(activeUsersList != null && activeUsersList.size() > 0){
							//List<LeaveTypeDO> leaveTypeList = leaveTypeService.retrieveActive();
							if(inputJSON.get(CommonConstants.UNIT) != null && !inputJSON.get(CommonConstants.UNIT).toString().isEmpty()){
								respJSON = LeaveRequestsUtil.getLeaveRequestsDetailForFilter(activeUsersList, CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.FROMDATE).toString()), CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.TODATE).toString()), Long.parseLong(inputJSON.get(CommonConstants.UNIT).toString()), type).toString();
							}else{
								respJSON = LeaveRequestsUtil.getLeaveRequestsDetailForFilter(activeUsersList, CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.FROMDATE).toString()), null, null,type).toString();
							}
							
					 		//respJSON = LeaveRequestsUtil.getLeaveRequestsDetailForFilter(activeUsersList, CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.FROMDATE).toString()), CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.TODATE).toString())).toString();
						}
						return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
					}
					if((inputJSON.get(CommonConstants.FROMDATE) != null && !inputJSON.get(CommonConstants.FROMDATE).toString().isEmpty()) && (inputJSON.get(CommonConstants.EMPID) != null || !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()) && (inputJSON.get(CommonConstants.TODATE) != null && !inputJSON.get(CommonConstants.TODATE).toString().isEmpty())){
						/*int year= Integer.parseInt(inputJSON.get(CommonConstants.YEAR).toString());
						int month= Integer.parseInt(inputJSON.get(CommonConstants.MONTHLY).toString());
						Calendar satrtDate = Calendar.getInstance();    
						satrtDate.set(year,month,1); 
						satrtDate.set(Calendar.DAY_OF_MONTH, satrtDate.getActualMinimum(Calendar.DAY_OF_MONTH));
						Calendar endDate = Calendar.getInstance(); 
						endDate.set(year,month,1); 
						endDate.set(Calendar.DAY_OF_MONTH, endDate.getActualMaximum(Calendar.DAY_OF_MONTH));*/
						//	List<LeaveTypeDO> leaveTypeList = leaveTypeService.retrieveActive();
					 		respJSON = LeaveRequestsUtil.getEmpLeaveRequestsDetailForFilter(inputJSON.get(CommonConstants.EMPID).toString(),CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.FROMDATE).toString()), CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.TODATE).toString()), type).toString();
					 		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
					}
					if((inputJSON.get(CommonConstants.FROMDATE) == null) && (inputJSON.get(CommonConstants.EMPID) != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()) && (inputJSON.get(CommonConstants.TODATE) == null)){
							//List<LeaveTypeDO> leaveTypeList = leaveTypeService.retrieveActive();
							Date statDate = null;
							Date endDate = null;
					 		respJSON = LeaveRequestsUtil.getEmpLeaveRequestsDetailForFilter(inputJSON.get(CommonConstants.EMPID).toString(),statDate, endDate,type).toString();
					 		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
					}
					if((inputJSON.get(CommonConstants.FROMDATE) != null && !inputJSON.get(CommonConstants.FROMDATE).toString().isEmpty()) && (inputJSON.get(CommonConstants.EMPID) != null || !inputJSON.get(CommonConstants.EMPID).toString().isEmpty())){
						/*int year= Integer.parseInt(inputJSON.get(CommonConstants.YEAR).toString());
						Calendar satrtDate = Calendar.getInstance();    
						satrtDate.set(year,0,1); 
						satrtDate.set(Calendar.DAY_OF_MONTH, satrtDate.getActualMinimum(Calendar.DAY_OF_MONTH));
						Calendar endDate = Calendar.getInstance(); 
						endDate.set(year,11,1); 
						endDate.set(Calendar.DAY_OF_MONTH, endDate.getActualMaximum(Calendar.DAY_OF_MONTH));*/
							//List<LeaveTypeDO> leaveTypeList = leaveTypeService.retrieveActive();
						Calendar endDate = Calendar.getInstance(); 
					 		respJSON = LeaveRequestsUtil.getEmpLeaveRequestsDetailForFilter(inputJSON.get(CommonConstants.EMPID).toString(),CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.FROMDATE).toString()), endDate.getTime(),type).toString();
					 		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
					}
					if(inputJSON.get(CommonConstants.FROMDATE) == null  && inputJSON.get(CommonConstants.EMPID) == null && inputJSON.get(CommonConstants.TODATE) == null && inputJSON.get(CommonConstants.UNIT) != null && !inputJSON.get(CommonConstants.UNIT).toString().isEmpty()){
						List<UserDO> activeUsersList = userService.retrieveUserByUnitId(Long.parseLong(inputJSON.get(CommonConstants.UNIT).toString()));
						respJSON = LeaveRequestsUtil.getLeaveRequestsDetailForFilter(activeUsersList,null, null, Long.parseLong(inputJSON.get(CommonConstants.UNIT).toString()), type).toString();
					 		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
					}
					
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveLeaveStatusByEmpId", method = RequestMethod.POST)
	public @ResponseBody String retrieveLeaveStatusByEmpId(Model model, HttpServletRequest request)  {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
				if(inputJSON != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
					List<EmployeeLeavegroupDO> empLeavegrp = employeeleaveGroupService.retrieveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
					if(empLeavegrp != null && empLeavegrp.size() >0){
						Calendar cal = Calendar.getInstance();
						int year = Calendar.getInstance().get(Calendar.YEAR);
						cal.set(Calendar.YEAR, year);
						cal.set(Calendar.DAY_OF_YEAR, 1);    
						Date start = cal.getTime();
						cal.set(Calendar.YEAR, year);
						cal.set(Calendar.MONTH, 11); 
						cal.set(Calendar.DAY_OF_MONTH, 31); 

						Date end = cal.getTime();
						List <LeaveGroupSettingsDO> leaveGrpSettingList  = leaveGroupSettingService.retrieveAvailableLeavesByGrpIdAndDate(empLeavegrp.get(0).getLeaveGroup().getLeaveGroupId(),start,end);
						
						if(inputJSON.get(CommonConstants.FROMDATE) != null &&!inputJSON.get(CommonConstants.FROMDATE).toString().isEmpty() && inputJSON.get(CommonConstants.TODATE) != null &&!inputJSON.get(CommonConstants.TODATE).toString().isEmpty()){
							respJSON = LeaveRequestsUtil.getLeaveReport(leaveGrpSettingList,inputJSON.get(CommonConstants.EMPID).toString(),start,end,CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.FROMDATE).toString()),CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.TODATE).toString())).toString();
						}else{
							respJSON = LeaveRequestsUtil.getLeaveReport(leaveGrpSettingList,inputJSON.get(CommonConstants.EMPID).toString(),start,end,null,null).toString();
							
						}
						
					}else{
						respJSON = CommonWebUtil.buildSuccessResponse().toString();
					}
				}
				//List<LeaveManagementDO> leaveList = leaveManagementService.retrieveAllActive();
				
				//respJSON = LeaveRequestsUtil.getLeaveStatus(activeUsersList,leaveList).toString();
				
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
}
