package com.spheresuite.erp.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.CountryDO;
import com.spheresuite.erp.domainobject.StateDO;
import com.spheresuite.erp.domainobject.WorkLocationDO;
import com.spheresuite.erp.service.CountryService;
import com.spheresuite.erp.service.StateService;
import com.spheresuite.erp.service.WorkLocationService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;
import com.spheresuite.erp.web.util.WorkLocationUtil;

@Controller
@RequestMapping(value = "/worklocation")
public class WorkLocationRS {

	String validation = null;
	static Logger logger = Logger.getLogger(WorkLocationRS.class.getName());
	
	@Autowired
	private WorkLocationService workLocationService;
	
	@Autowired
	private CountryService countryService;
	
	@Autowired
	private StateService stateService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				WorkLocationDO workLocationDO = new WorkLocationDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.CITY) != null && !inputJSON.get(CommonConstants.CITY).toString().isEmpty()){
			 			workLocationDO.setWorklocation(inputJSON.get(CommonConstants.CITY).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.ADDRESS) != null && !inputJSON.get(CommonConstants.ADDRESS).toString().isEmpty()){
			 			workLocationDO.setAddress(inputJSON.get(CommonConstants.ADDRESS).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.COUNTRY_ID) != null && !inputJSON.get(CommonConstants.COUNTRY_ID).toString().isEmpty()){
			 			List<CountryDO> countryList = countryService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.COUNTRY_ID).toString()));
			 			if(countryList != null && countryList.size() > 0){
			 				workLocationDO.setCountry(countryList.get(0));
			 			}
		 			}
			 		if(inputJSON.get(CommonConstants.STATE_ID) != null && !inputJSON.get(CommonConstants.STATE_ID).toString().isEmpty()){
			 			List<StateDO> stateList = stateService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.STATE_ID).toString()));
			 			if(stateList != null && stateList.size() > 0){
			 				workLocationDO.setState(stateList.get(0));
			 			}
		 			}
			 		workLocationDO.setUpdatedon(new Date());
			 		workLocationDO.setCreatedon(new Date());
			 		workLocationDO.setStatus(CommonConstants.ACTIVE);
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			workLocationDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			workLocationDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 	}
			 	if(!workLocationService.persist(workLocationDO)){
					return CommonWebUtil.buildErrorResponse("Work Location Already Added").toString();
				}
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Wrok Location Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveByStateId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByStateId(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<WorkLocationDO> workLocationList = workLocationService.retrieveByStateId(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = WorkLocationUtil.getWorkLocationList(workLocationList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<WorkLocationDO> worklocationList = workLocationService.retrieveActive();
				respJSON = WorkLocationUtil.getWorkLocationList(worklocationList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<WorkLocationDO> worklocationList = workLocationService.retrieve();
				respJSON = WorkLocationUtil.getWorkLocationList(worklocationList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				WorkLocationDO worklocationDO = new WorkLocationDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<WorkLocationDO> worklocationList = workLocationService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		worklocationDO = worklocationList.get(0);
			 		if(inputJSON.get(CommonConstants.ADDRESS) != null && !inputJSON.get(CommonConstants.ADDRESS).toString().isEmpty()){
			 			worklocationDO.setAddress(inputJSON.get(CommonConstants.ADDRESS).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.CITY) != null && !inputJSON.get(CommonConstants.CITY).toString().isEmpty()){
			 			worklocationDO.setWorklocation(inputJSON.get(CommonConstants.CITY).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.COUNTRY_ID) != null && !inputJSON.get(CommonConstants.COUNTRY_ID).toString().isEmpty()){
			 			List<CountryDO> countryList = countryService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.COUNTRY_ID).toString()));
			 			if(countryList != null && countryList.size() > 0){
			 				worklocationDO.setCountry(countryList.get(0));
			 			}
		 			}
			 		if(inputJSON.get(CommonConstants.STATE_ID) != null && !inputJSON.get(CommonConstants.STATE_ID).toString().isEmpty()){
			 			List<StateDO> stateList = stateService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.STATE_ID).toString()));
			 			if(stateList != null && stateList.size() > 0){
			 				worklocationDO.setState(stateList.get(0));
			 			}
		 			}
			 		worklocationDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			worklocationDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
			 			worklocationDO.setStatus((char)inputJSON.get(CommonConstants.STATUS).toString().charAt(0));
			 		}
			 		if(!workLocationService.update(worklocationDO)){
						return CommonWebUtil.buildErrorResponse("Work Location Already Added").toString();
					}
			 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Wrok Location Updated");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/importWorkLocation", method = RequestMethod.POST)
	public @ResponseBody String importWorkLocation(Model model, HttpServletRequest request) {
		try {
			//if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
				org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
				JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
				List<WorkLocationDO> workLocationlist = new ArrayList<WorkLocationDO>();
				Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY);
				for (int i=0; i < fileData.size(); i++){
					JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
					WorkLocationDO workLocationDO = new WorkLocationDO();
					
			 		if(colName.get(CommonConstants.COUNTRY_NAME) != null && !colName.get(CommonConstants.COUNTRY_NAME).toString().isEmpty()){
			 			List<CountryDO> countryList = countryService.retrieveByName(rowJSON.get(colName.get(CommonConstants.COUNTRY_NAME)).toString());
			 			if(countryList != null && countryList.size() > 0){
			 				workLocationDO.setCountry(countryList.get(0));
			 				
			 				if(colName.get(CommonConstants.STATE_NAME) != null && !colName.get(CommonConstants.STATE_NAME).toString().isEmpty()){
					 			List<StateDO> stateList = stateService.retrieveByName(rowJSON.get(colName.get(CommonConstants.STATE_NAME)).toString());
					 			if(stateList != null && stateList.size() > 0){
					 				workLocationDO.setState(stateList.get(0));
					 				
					 				if(colName.get(CommonConstants.CITY) != null && !colName.get(CommonConstants.CITY).toString().isEmpty()){
							 			workLocationDO.setWorklocation(rowJSON.get(colName.get(CommonConstants.CITY)).toString());
							 		}
							 		if(colName.get(CommonConstants.ADDRESS) != null && !colName.get(CommonConstants.ADDRESS).toString().isEmpty()){
							 			workLocationDO.setAddress(rowJSON.get(colName.get(CommonConstants.ADDRESS)).toString());
							 		}
							 		if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
							 			workLocationDO.setUpdatedby(updatedBy.toString());
							 			workLocationDO.setCreatedby(updatedBy.toString());
							 		}
							 		workLocationDO.setUpdatedon(new Date());
							 		workLocationDO.setCreatedon(new Date());
							 		workLocationDO.setStatus(CommonConstants.ACTIVE);
							 		workLocationlist.add(workLocationDO);
					 			}
				 			}
			 				
			 			}
		 			}
				}
				workLocationService.persistList(workLocationlist);
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
}
