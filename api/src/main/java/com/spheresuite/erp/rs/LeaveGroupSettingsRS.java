package com.spheresuite.erp.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.EmployeeLeavegroupDO;
import com.spheresuite.erp.domainobject.LeaveGroupDO;
import com.spheresuite.erp.domainobject.LeaveGroupSettingsDO;
import com.spheresuite.erp.domainobject.LeaveManagementDO;
import com.spheresuite.erp.service.EmployeeLeavegroupService;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.LeaveGroupService;
import com.spheresuite.erp.service.LeaveGroupSettingsService;
import com.spheresuite.erp.service.LeaveManagementService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.LeaveGroupSettingsUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/leavegroupsettings")
public class LeaveGroupSettingsRS {

	String validation = null;
	static Logger logger = Logger.getLogger(LeaveGroupSettingsRS.class.getName());
	
	@Autowired
	private LeaveGroupService leaveGroupService;
	
	@Autowired
	private LeaveManagementService leaveManagementService;
	
	@Autowired
	private LeaveGroupSettingsService leaveGroupSettingsService;
	
	@Autowired
	private EmployeeService employeeService;
	
	@Autowired
	private EmployeeLeavegroupService employeeleaveGroupService;
	
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if (inputJSON.get(CommonConstants.LEAVES) != null && !inputJSON.get(CommonConstants.LEAVES).toString().isEmpty()){
				 		JSONArray resultJSONArray = new JSONArray();
				 		resultJSONArray.put(inputJSON.get(CommonConstants.LEAVES));
				 		String rec = (String) resultJSONArray.get(0).toString();
				 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
				 		for (int i = 0; i < resultJSONArray1.size(); i++) {
				 			LeaveGroupSettingsDO leaveGroupSettingsDO = new LeaveGroupSettingsDO();
				 			
				 			
				 			List<LeaveGroupSettingsDO> leaveList = leaveGroupSettingsService.retrieveByGrpAndLeaveId(Long.parseLong(inputJSON.get(CommonConstants.LEAVEGROUP_ID).toString()),Long.parseLong(resultJSONArray1.get(i).toString()) );
				 			if(leaveList != null && leaveList.size() > 0){
				 				if(leaveList.get(0).getStatus() == CommonConstants.INACTIVE){
				 					leaveGroupSettingsDO = leaveList.get(0);
				 					leaveGroupSettingsDO.setStatus(CommonConstants.ACTIVE);
				 					leaveGroupSettingsService.update(leaveGroupSettingsDO);
				 				}
				 			}else{
				 				if(inputJSON.get(CommonConstants.LEAVEGROUP_ID) != null && !inputJSON.get(CommonConstants.LEAVEGROUP_ID).toString().isEmpty()){
						 			List<LeaveGroupDO> leaveGroupList = leaveGroupService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.LEAVEGROUP_ID).toString()));
							 		if(leaveGroupList != null && leaveGroupList.size() >0){
							 			leaveGroupSettingsDO.setLeaveGroup(leaveGroupList.get(0));
							 		}
						 		}
						 		if(resultJSONArray1.get(i) != null ){
							 		List<LeaveManagementDO> leaveManagmentList = leaveManagementService.retrieveById(Long.parseLong(resultJSONArray1.get(i).toString()));
						 			if(leaveManagmentList != null && leaveManagmentList.size() > 0){
						 				leaveGroupSettingsDO.setLeaveManagement(leaveManagmentList.get(0));
						 			}
						 		}
						 		leaveGroupSettingsDO.setStatus(CommonConstants.ACTIVE);
						 		
						 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
						 			leaveGroupSettingsDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
						 			leaveGroupSettingsDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
						 		}
						 		leaveGroupSettingsDO.setUpdatedon(new Date());
						 		leaveGroupSettingsDO.setCreatedon(new Date());
						 		
						 		if(!leaveGroupSettingsService.persist(leaveGroupSettingsDO)){
						 			//return CommonWebUtil.buildErrorResponse("Payroll Group Already Added").toString();
								}
				 			}
				 		}
			 		}
			 		if(inputJSON.get(CommonConstants.DEACTIVATELEAVEMENU) != null && !inputJSON.get(CommonConstants.DEACTIVATELEAVEMENU).toString().isEmpty()){
			 			JSONArray resultJSONArray = new JSONArray();
				 		resultJSONArray.put(inputJSON.get(CommonConstants.DEACTIVATELEAVEMENU));
				 		String rec = (String) resultJSONArray.get(0).toString();
				 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
				 		List<LeaveGroupSettingsDO> leaveGroupSettingslist = new ArrayList<LeaveGroupSettingsDO>();
				 		for (int i = 0; i < resultJSONArray1.size(); i++) {
				 			LeaveGroupSettingsDO leaveGrpDO = new LeaveGroupSettingsDO();
				 			List<LeaveGroupSettingsDO> leaveGroupList = leaveGroupSettingsService.retrieveByID(Long.parseLong(resultJSONArray1.get(i).toString()));
				 			if(leaveGroupList != null && leaveGroupList.size() > 0){
				 				leaveGrpDO = leaveGroupList.get(0);
				 				leaveGrpDO.setStatus(CommonConstants.INACTIVE);
				 				leaveGroupSettingslist.add(leaveGrpDO);
				 			}
				 		}
				 		leaveGroupSettingsService.updateList(leaveGroupSettingslist);
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.PERMISSION_CHECKED) != null && !inputJSON.get(CommonConstants.PERMISSION_CHECKED).toString().isEmpty()){
			 			LeaveGroupSettingsDO leaveGroupSettingsDO = new LeaveGroupSettingsDO();
			 			List<LeaveGroupSettingsDO> leaveList = leaveGroupSettingsService.retrieveByGrpAndPermission(Long.parseLong(inputJSON.get(CommonConstants.LEAVEGROUP_ID).toString()),inputJSON.get(CommonConstants.PERMISSION_CHECKED).toString());
			 			if(leaveList != null && leaveList.size() > 0){
			 				if(leaveList.get(0).getStatus() == CommonConstants.INACTIVE){
			 					leaveGroupSettingsDO = leaveList.get(0);
			 					leaveGroupSettingsDO.setStatus(CommonConstants.ACTIVE);
			 					leaveGroupSettingsService.update(leaveGroupSettingsDO);
			 				}else{
			 					leaveGroupSettingsDO = leaveList.get(0);
			 					leaveGroupSettingsDO.setStatus(CommonConstants.ACTIVE);
			 					if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
						 			leaveGroupSettingsDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
						 		}
			 					leaveGroupSettingsDO.setUpdatedon(new Date());
			 					leaveGroupSettingsService.update(leaveGroupSettingsDO);
			 				}
			 			}else{
			 				if(inputJSON.get(CommonConstants.LEAVEGROUP_ID) != null && !inputJSON.get(CommonConstants.LEAVEGROUP_ID).toString().isEmpty()){
					 			List<LeaveGroupDO> leaveGroupList = leaveGroupService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.LEAVEGROUP_ID).toString()));
						 		if(leaveGroupList != null && leaveGroupList.size() >0){
						 			leaveGroupSettingsDO.setLeaveGroup(leaveGroupList.get(0));
						 		}
					 		}
					 		leaveGroupSettingsDO.setStatus(CommonConstants.ACTIVE);
					 		if(inputJSON.get(CommonConstants.PERMISSION_CHECKED) != null && !inputJSON.get(CommonConstants.PERMISSION_CHECKED).toString().isEmpty()){
					 			leaveGroupSettingsDO.setPermission(inputJSON.get(CommonConstants.PERMISSION_CHECKED).toString());
					 		}
					 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			leaveGroupSettingsDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			leaveGroupSettingsDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 		}
					 		leaveGroupSettingsDO.setUpdatedon(new Date());
					 		leaveGroupSettingsDO.setCreatedon(new Date());
					 		
					 		if(!leaveGroupSettingsService.persist(leaveGroupSettingsDO)){
					 			//return CommonWebUtil.buildErrorResponse("Payroll Group Already Added").toString();
							}
			 			}
		 			}else{
		 				List<LeaveGroupSettingsDO> leaveList = leaveGroupSettingsService.retrieveByGrpAndPermission(Long.parseLong(inputJSON.get(CommonConstants.LEAVEGROUP_ID).toString()),"y");
			 			if(leaveList != null && leaveList.size() > 0){
			 				LeaveGroupSettingsDO leaveGroupSettingsDO = new LeaveGroupSettingsDO();
			 					leaveGroupSettingsDO = leaveList.get(0);
			 					leaveGroupSettingsDO.setStatus(CommonConstants.INACTIVE);
			 					if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
						 			leaveGroupSettingsDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
						 		}
			 					leaveGroupSettingsDO.setUpdatedon(new Date());
			 					leaveGroupSettingsService.update(leaveGroupSettingsDO);
			 			}
		 			}
			 		if(inputJSON.get(CommonConstants.COMPENSATION_CHECKED) != null && !inputJSON.get(CommonConstants.COMPENSATION_CHECKED).toString().isEmpty()){
			 			LeaveGroupSettingsDO leaveGroupSettingsDO = new LeaveGroupSettingsDO();
			 			List<LeaveGroupSettingsDO> leaveList = leaveGroupSettingsService.retrieveByGrpAndCompensation(Long.parseLong(inputJSON.get(CommonConstants.LEAVEGROUP_ID).toString()),inputJSON.get(CommonConstants.COMPENSATION_CHECKED).toString());
			 			if(leaveList != null && leaveList.size() > 0){
			 				if(leaveList.get(0).getStatus() == CommonConstants.INACTIVE){
			 					leaveGroupSettingsDO = leaveList.get(0);
			 					leaveGroupSettingsDO.setStatus(CommonConstants.ACTIVE);
			 					leaveGroupSettingsService.update(leaveGroupSettingsDO);
			 				}else{
			 					leaveGroupSettingsDO = leaveList.get(0);
			 					leaveGroupSettingsDO.setStatus(CommonConstants.ACTIVE);
			 					if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
						 			leaveGroupSettingsDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
						 		}
			 					leaveGroupSettingsDO.setUpdatedon(new Date());
			 					leaveGroupSettingsService.update(leaveGroupSettingsDO);
			 				}
			 			}else{
			 				if(inputJSON.get(CommonConstants.LEAVEGROUP_ID) != null && !inputJSON.get(CommonConstants.LEAVEGROUP_ID).toString().isEmpty()){
					 			List<LeaveGroupDO> leaveGroupList = leaveGroupService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.LEAVEGROUP_ID).toString()));
						 		if(leaveGroupList != null && leaveGroupList.size() >0){
						 			leaveGroupSettingsDO.setLeaveGroup(leaveGroupList.get(0));
						 		}
					 		}
					 		leaveGroupSettingsDO.setStatus(CommonConstants.ACTIVE);
					 		if(inputJSON.get(CommonConstants.COMPENSATION_CHECKED) != null && !inputJSON.get(CommonConstants.COMPENSATION_CHECKED).toString().isEmpty()){
					 			leaveGroupSettingsDO.setCompOff(inputJSON.get(CommonConstants.COMPENSATION_CHECKED).toString());
					 		}
					 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			leaveGroupSettingsDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			leaveGroupSettingsDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 		}
					 		leaveGroupSettingsDO.setUpdatedon(new Date());
					 		leaveGroupSettingsDO.setCreatedon(new Date());
					 		
					 		if(!leaveGroupSettingsService.persist(leaveGroupSettingsDO)){
					 			//return CommonWebUtil.buildErrorResponse("Payroll Group Already Added").toString();
							}
			 			}
		 			}else{
		 				List<LeaveGroupSettingsDO> leaveList = leaveGroupSettingsService.retrieveByGrpAndCompensation(Long.parseLong(inputJSON.get(CommonConstants.LEAVEGROUP_ID).toString()),"y");
			 			if(leaveList != null && leaveList.size() > 0){
			 				LeaveGroupSettingsDO leaveGroupSettingsDO = new LeaveGroupSettingsDO();
			 					leaveGroupSettingsDO = leaveList.get(0);
			 					leaveGroupSettingsDO.setStatus(CommonConstants.INACTIVE);
			 					if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
						 			leaveGroupSettingsDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
						 		}
			 					leaveGroupSettingsDO.setUpdatedon(new Date());
			 					leaveGroupSettingsService.update(leaveGroupSettingsDO);
			 			}
		 			}
			 	}
				
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Leave Group Settings Updated");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
			 		
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveByGroupId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<LeaveGroupSettingsDO> leaveGroupList = leaveGroupSettingsService.retrieveByGroupIdWithStatus(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = LeaveGroupSettingsUtil.getLeaveGroupDetails(leaveGroupList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	
	@RequestMapping(value = "/retrieveAvailableLeavesByEmpId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveAvailableLeavesByEmpId(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if (inputJSON != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
					List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
					if(employeeList != null && employeeList.size() > 0){
						List<EmployeeLeavegroupDO> leaveGroupList = employeeleaveGroupService.retrieveByEmpId(employeeList.get(0).getEmpId().toString());
						if(leaveGroupList != null && leaveGroupList.size() > 0 && leaveGroupList.get(0).getLeaveGroup() != null){
							 long millis=System.currentTimeMillis();  
							 java.sql.Date date =new java.sql.Date(millis);  
							 /*SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");  
							 Date date = new Date();  
							  System.out.println("current date"+formatter.format(date));*/ 
							//Date d = formatter.format(date);
							List<LeaveGroupSettingsDO> leaveList = leaveGroupSettingsService.retrieveAvailableLeavesByGrpId(leaveGroupList.get(0).getLeaveGroup().getLeaveGroupId(),date);
							respJSON = LeaveGroupSettingsUtil.getLeaveTypesForEmployee(leaveList).toString();
						
						}else{
							return CommonWebUtil.buildErrorResponse("Leave Group Not Available").toString();
						}
					}
			 	}
			}
			 	}catch (Exception e) {
					e.printStackTrace();
					return CommonWebUtil.buildErrorResponse("").toString();
				}
				return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
}
