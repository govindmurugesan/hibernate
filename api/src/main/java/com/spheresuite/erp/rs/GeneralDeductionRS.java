package com.spheresuite.erp.rs;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.DeductionSubTypeDO;
import com.spheresuite.erp.domainobject.DeductionTypeDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.GeneralDeductionDO;
import com.spheresuite.erp.domainobject.GeneralDeductionDetailDO;
import com.spheresuite.erp.domainobject.UnitOrBranchDO;
import com.spheresuite.erp.service.DeductionSubTypeService;
import com.spheresuite.erp.service.DeductionTypeService;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.GeneralDeductionService;
import com.spheresuite.erp.service.GeneralDeductionDetailService;
import com.spheresuite.erp.service.UnitOrBranchService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.GeneralDeducationUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/loanpayment")
public class GeneralDeductionRS {

	String validation = null;
	static Logger logger = Logger.getLogger(GeneralDeductionRS.class.getName());

	@Autowired
	private GeneralDeductionService loanPaymentService;
	
	@Autowired
	private EmployeeService employeeService;
	
	@Autowired
	private GeneralDeductionDetailService loanPaymentDetailService;
	
	@Autowired
	private DeductionSubTypeService loanTypeService;
	
	@Autowired
	private UnitOrBranchService unitOrBranchService;
	
	@Autowired
	private DeductionTypeService deductionTypeService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				GeneralDeductionDO loanPaymentDO = new GeneralDeductionDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		List<EmployeeDO> employeeList = null;
			 		if(inputJSON.get(CommonConstants.EMPID) != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
				 		if(employeeList != null && employeeList.size() > 0){
				 			loanPaymentDO.setEmployee(employeeList.get(0));
				 			if(inputJSON.get(CommonConstants.TYPE_ID) != null && !inputJSON.get(CommonConstants.TYPE_ID).toString().isEmpty()){
					 			List <DeductionSubTypeDO> loanTypeList = loanTypeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.TYPE_ID).toString()));
						 		if(loanTypeList != null && loanTypeList.size() > 0){
						 			loanPaymentDO.setDeductionSubType(loanTypeList.get(0));
						 			if(inputJSON.get(CommonConstants.UNIT) != null && !inputJSON.get(CommonConstants.UNIT).toString().isEmpty()){
							 			List <UnitOrBranchDO> unitOrBranchList = unitOrBranchService.retrieveActiveById(Long.parseLong(inputJSON.get(CommonConstants.UNIT).toString()));
								 		if(unitOrBranchList != null && unitOrBranchList.size() > 0){
								 			loanPaymentDO.setUnitOrBranch(unitOrBranchList.get(0));
								 			if(inputJSON.get(CommonConstants.DEDUCTION_TYPE_ID) != null && !inputJSON.get(CommonConstants.DEDUCTION_TYPE_ID).toString().isEmpty()){
									 			List<DeductionTypeDO> deductionTypeList = deductionTypeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.DEDUCTION_TYPE_ID).toString()));
									 			if(deductionTypeList != null && deductionTypeList.size() >0){
									 				loanPaymentDO.setDeductionType(deductionTypeList.get(0));
									 				if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
											 			loanPaymentDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
											 			loanPaymentDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
											 			if(inputJSON.get(CommonConstants.AMOUNT) != null && !inputJSON.get(CommonConstants.AMOUNT).toString().isEmpty()){
												 			loanPaymentDO.setAmount(Long.parseLong(inputJSON.get(CommonConstants.AMOUNT).toString()));
												 		}
												 		if(inputJSON.get(CommonConstants.MONTHLY) != null && !inputJSON.get(CommonConstants.MONTHLY).toString().isEmpty()){
												 			loanPaymentDO.setMonth(inputJSON.get(CommonConstants.MONTHLY).toString());
												 		}
												 		if(inputJSON.get(CommonConstants.NOOFINSTALLMENT) != null && !inputJSON.get(CommonConstants.NOOFINSTALLMENT).toString().isEmpty()){
												 			loanPaymentDO.setNoOfInstallments(inputJSON.get(CommonConstants.NOOFINSTALLMENT).toString());
												 		}
												 		if(inputJSON.get(CommonConstants.INSTALLMENTAOUNT) != null && !inputJSON.get(CommonConstants.INSTALLMENTAOUNT).toString().isEmpty()){
												 			loanPaymentDO.setInstallmentAmount(inputJSON.get(CommonConstants.INSTALLMENTAOUNT).toString());
												 		}
												 		loanPaymentDO.setUpdatedon(new Date());
												 		loanPaymentDO.setCreatedon(new Date());
												 		loanPaymentDO.setStatus(CommonConstants.ACTIVESTRING);
												 		GeneralDeductionDO updatedadvancePayment=  loanPaymentService.persist(loanPaymentDO);
												 		if(updatedadvancePayment != null){
													 		org.json.simple.JSONArray loanDetailList = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.LOANDETAIL).toString());
													 		
													 		//updatedadvancePayment.getAdvancePaymentId()
													 		
													 		for (int j=0; j < loanDetailList.size(); j++){
											 					JSONObject monthlyDetail = CommonWebUtil.getInputParams(loanDetailList.get(j).toString());
											 					GeneralDeductionDetailDO loanPaymentDetailDO = new GeneralDeductionDetailDO();
											 					if(monthlyDetail.get(CommonConstants.AMOUNT) != null && !monthlyDetail.get(CommonConstants.AMOUNT).toString().isEmpty()){
											 						loanPaymentDetailDO.setAmount(monthlyDetail.get(CommonConstants.AMOUNT).toString());
											 					}
											 					if(monthlyDetail.get(CommonConstants.MONTH) != null && !monthlyDetail.get(CommonConstants.MONTH).toString().isEmpty()){
											 						loanPaymentDetailDO.setMonth(monthlyDetail.get(CommonConstants.MONTH).toString());
											 					}
											 					if(employeeList != null && employeeList.size() > 0){
											 						loanPaymentDetailDO.setEmployee(employeeList.get(0));
											 					}
											 					loanPaymentDetailDO.setLoanPayment(updatedadvancePayment);
											 					loanPaymentDetailDO.setUpdatedon(new Date());
											 					loanPaymentDetailDO.setCreatedon(new Date());
											 					loanPaymentDetailDO.setStatus(CommonConstants.ACTIVESTRING);
											 					
														 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
														 			loanPaymentDetailDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
														 			loanPaymentDetailDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
														 		}
														 		
														 		loanPaymentDetailService.persist(loanPaymentDetailDO);
														 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New General Deduction Created For Employee");
													 		}
												 		}
									 				}
									 			}
									 		}
								 		}
							 		}
						 		}
					 		}
				 		}
			 		}
			 	}
				
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	/*@RequestMapping(value = "/retrieveByEmpId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByEmpId(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<EmployeeBonusDO> employeeAddressList = employeeBonusService.retrieveByEmpId(inputJSON.get(CommonConstants.ID).toString());
					respJSON = EmployeeBonusUtil.getEmployeeBonusList(employeeAddressList, null,null).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}*/
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieveByEmpId(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
					List<GeneralDeductionDO> loanPayemntList = loanPaymentService.retrieve();
					respJSON = GeneralDeducationUtil.getAdvancePaymentList(loanPayemntList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				GeneralDeductionDO loanPaymentDO = new GeneralDeductionDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<GeneralDeductionDO> loanPaymentList = loanPaymentService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		loanPaymentDO = loanPaymentList.get(0);
			 		
			 		if(inputJSON.get(CommonConstants.EMPID) != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
				 		if(employeeList != null && employeeList.size() > 0){
				 			loanPaymentDO.setEmployee(employeeList.get(0));
				 		}
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.UNIT) != null && !inputJSON.get(CommonConstants.UNIT).toString().isEmpty()){
			 			List <UnitOrBranchDO> unitOrBranchList = unitOrBranchService.retrieveActiveById(Long.parseLong(inputJSON.get(CommonConstants.UNIT).toString()));
				 		if(unitOrBranchList != null && unitOrBranchList.size() > 0){
				 			loanPaymentDO.setUnitOrBranch(unitOrBranchList.get(0));
				 		}
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.DEDUCTION_TYPE_ID) != null && !inputJSON.get(CommonConstants.DEDUCTION_TYPE_ID).toString().isEmpty()){
			 			List<DeductionTypeDO> deductionTypeList = deductionTypeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.DEDUCTION_TYPE_ID).toString()));
			 			if(deductionTypeList != null && deductionTypeList.size() >0){
			 				loanPaymentDO.setDeductionType(deductionTypeList.get(0));
			 			}
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.AMOUNT) != null && !inputJSON.get(CommonConstants.AMOUNT).toString().isEmpty()){
			 			loanPaymentDO.setAmount(Long.parseLong(inputJSON.get(CommonConstants.AMOUNT).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.MONTHLY) != null && !inputJSON.get(CommonConstants.MONTHLY).toString().isEmpty()){
			 			loanPaymentDO.setMonth(inputJSON.get(CommonConstants.MONTHLY).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.NOOFINSTALLMENT) != null && !inputJSON.get(CommonConstants.NOOFINSTALLMENT).toString().isEmpty()){
			 			loanPaymentDO.setNoOfInstallments(inputJSON.get(CommonConstants.NOOFINSTALLMENT).toString());
			 		}
			 		loanPaymentDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			loanPaymentDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		GeneralDeductionDO updatedadvancePayment = loanPaymentService.update(loanPaymentDO);
			 		
			 		if(updatedadvancePayment != null){
				 		org.json.simple.JSONArray loanDetailList = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.LOANDETAIL).toString());
				 		
				 		//updatedadvancePayment.getAdvancePaymentId()
				 		
				 		for (int j=0; j < loanDetailList.size(); j++){
		 					JSONObject monthlyDetail = CommonWebUtil.getInputParams(loanDetailList.get(j).toString());
		 					GeneralDeductionDetailDO loanPaymentDetailDO = new GeneralDeductionDetailDO();
		 					
		 					if(monthlyDetail.get(CommonConstants.ID) != null && !monthlyDetail.get(CommonConstants.ID).toString().isEmpty()){
		 						List <GeneralDeductionDetailDO> detailList = loanPaymentDetailService.getById(Long.parseLong(monthlyDetail.get(CommonConstants.ID).toString()));
		 			 				if(detailList != null && detailList.size() > 0){
		 			 					loanPaymentDetailDO = detailList.get(0);
		 			 					
		 			 					if(monthlyDetail.get(CommonConstants.AMOUNT) != null && !monthlyDetail.get(CommonConstants.AMOUNT).toString().isEmpty()){
		 			 						loanPaymentDetailDO.setAmount(monthlyDetail.get(CommonConstants.AMOUNT).toString());
		 			 					}
		 			 					
		 			 					if(monthlyDetail.get(CommonConstants.MONTH) != null && !monthlyDetail.get(CommonConstants.MONTH).toString().isEmpty()){
		 			 						loanPaymentDetailDO.setMonth(monthlyDetail.get(CommonConstants.MONTH).toString());
		 			 					}
		 			 					loanPaymentDetailDO.setUpdatedon(new Date());
		 			 					
		 			 					if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
		 			 						loanPaymentDetailDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
		 						 		}
		 						 		
		 			 					loanPaymentDetailService.update(loanPaymentDetailDO);
		 			 				}
		 					}else{
		 						if(monthlyDetail.get(CommonConstants.AMOUNT) != null && !monthlyDetail.get(CommonConstants.AMOUNT).toString().isEmpty()){
		 							loanPaymentDetailDO.setAmount(monthlyDetail.get(CommonConstants.AMOUNT).toString());
			 					}
			 					if(monthlyDetail.get(CommonConstants.MONTH) != null && !monthlyDetail.get(CommonConstants.MONTH).toString().isEmpty()){
			 						loanPaymentDetailDO.setMonth(monthlyDetail.get(CommonConstants.MONTH).toString());
			 					}
			 					if(inputJSON.get(CommonConstants.EMPID) != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 						List <EmployeeDO>	employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
							 		if(employeeList != null && employeeList.size() > 0){
							 			loanPaymentDetailDO.setEmployee(employeeList.get(0));
							 		}
						 		}
			 					loanPaymentDetailDO.setLoanPayment(updatedadvancePayment);
			 					loanPaymentDetailDO.setUpdatedon(new Date());
			 					loanPaymentDetailDO.setCreatedon(new Date());
			 					loanPaymentDetailDO.setStatus(CommonConstants.ACTIVESTRING);
			 					
						 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
						 			loanPaymentDetailDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
						 			loanPaymentDetailDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
						 		}
						 		loanPaymentDetailService.persist(loanPaymentDetailDO);
		 					}
		 					
				 		}
			 		}
			 		
			 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Loan Payemnt Updated For Employee");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/importGeneralDeduction", method = RequestMethod.POST)
	public @ResponseBody String importEarningType(Model model, HttpServletRequest request) {
		try {
			/*JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
			Calendar currentMonth = Calendar.getInstance();
	        SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM");
	        System.out.println("Curret month of date : "
	                + dateFormat.format(CommonUtil.convertMonthToSqlDate(inputJSON.get("month").toString())));*/
	        // Increment month
	      /*  currentMonth.add(Calendar.MONTH, 1);
	        System.out.println("Next month : "
	        + dateFormat.format(currentMonth.getTime()));*/
		//	JSONObject inputJSON =  CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
	        
	        /*Date currentDate = CommonUtil.convertMonthToSqlDate(inputJSON.get("month").toString());
	        		
	        for (int i = 0; i < 4; i++) {
	        	Calendar currentMonth1 = Calendar.getInstance();
	        	currentMonth1.setTime(currentDate);
	        	
	        	if(i != 0)currentMonth1.add(Calendar.MONTH, 1);
	        	Date nextMonth = currentMonth1.getTime();
	        //	System.out.println("montppp"+ dateFormat.format(nextMonth.getTime()));
		       // System.out.println("yr"+ currentMonth1.get(Calendar.YEAR));
		        currentDate = currentMonth1.getTime();
		        String dateToAdd = dateFormat.format(nextMonth.getTime()).substring(0, 3) +" "+ currentMonth1.get(Calendar.YEAR);
		        System.out.println(dateToAdd);
		       // System.out.println("ths s curr"+currentMonth1);
			}*/
	        
	        
	        
	        
	       
	        
	        
	        //System.out.format("next month: %s\n", nextMonth);
	        
			JSONObject inputJSON =  CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM");
			org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
			JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
			List<GeneralDeductionDO> deductionList = new ArrayList<GeneralDeductionDO>();
			
			
			
			
			Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY);
			for (int i=0; i < fileData.size(); i++){
				JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
				GeneralDeductionDO deduction = new GeneralDeductionDO();
				List<EmployeeDO> employeeList = null;
		 		if(colName.get(CommonConstants.EMPID) != null && !colName.get(CommonConstants.EMPID).toString().isEmpty()){
		 			employeeList = employeeService.retriveByEmpId(rowJSON.get(colName.get(CommonConstants.EMPID)).toString());
			 		if(employeeList != null && employeeList.size() > 0){
			 			deduction.setEmployee(employeeList.get(0));
			 			
			 			
			 			if(colName.get(CommonConstants.DEDUCTIONTYPE_NAME) != null && !colName.get(CommonConstants.DEDUCTIONTYPE_NAME).toString().isEmpty()){
				 			List<DeductionTypeDO> deductionTypeList = deductionTypeService.retrieveByName(rowJSON.get(colName.get(CommonConstants.DEDUCTIONTYPE_NAME)).toString());
				 			if(deductionTypeList != null && deductionTypeList.size() >0){
				 				deduction.setDeductionType(deductionTypeList.get(0));
				 				
				 				if(colName.get(CommonConstants.TYPENAME) != null && !colName.get(CommonConstants.TYPENAME).toString().isEmpty()){
						 			List <DeductionSubTypeDO> loanTypeList = loanTypeService.retrieveByName(rowJSON.get(colName.get(CommonConstants.TYPENAME)).toString());
							 		if(loanTypeList != null && loanTypeList.size() > 0){
							 			deduction.setDeductionSubType(loanTypeList.get(0));
							 			
							 			if(colName.get(CommonConstants.UNITNAME) != null && !colName.get(CommonConstants.UNITNAME).toString().isEmpty()){
								 			List <UnitOrBranchDO> unitOrBranchList = unitOrBranchService.retrieveActiveByName(rowJSON.get(colName.get(CommonConstants.UNITNAME)).toString());
									 		if(unitOrBranchList != null && unitOrBranchList.size() > 0){
									 			deduction.setUnitOrBranch(unitOrBranchList.get(0));
									 		}
								 		}
							 			
							 			if(colName.get(CommonConstants.AMOUNT) != null && !colName.get(CommonConstants.AMOUNT).toString().isEmpty()){
								 			deduction.setAmount(Long.parseLong(rowJSON.get(colName.get(CommonConstants.AMOUNT)).toString()));
								 		}
								 		if(colName.get(CommonConstants.MONTHLY) != null && !colName.get(CommonConstants.MONTHLY).toString().isEmpty()){
								 			deduction.setMonth(rowJSON.get(colName.get(CommonConstants.MONTHLY)).toString());
								 		}
								 		if(colName.get(CommonConstants.NOOFINSTALLMENT) != null && !colName.get(CommonConstants.NOOFINSTALLMENT).toString().isEmpty()){
								 			deduction.setNoOfInstallments(rowJSON.get(colName.get(CommonConstants.NOOFINSTALLMENT)).toString());
								 		}
								 		
								 		if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
								 			deduction.setUpdatedby(updatedBy.toString());
								 			deduction.setCreatedby(updatedBy.toString());
								 		}
								 		deduction.setUpdatedon(new Date());
								 		deduction.setCreatedon(new Date());
								 		deduction.setStatus(CommonConstants.ACTIVESTRING);
								 		//earningTypelist.add(deduction);
										GeneralDeductionDO updatedadvancePayment=  loanPaymentService.persist(deduction);
										
										if(updatedadvancePayment != null){
											if(colName.get(CommonConstants.AMOUNT) != null && !colName.get(CommonConstants.AMOUNT).toString().isEmpty() && colName.get(CommonConstants.NOOFINSTALLMENT) != null && !colName.get(CommonConstants.NOOFINSTALLMENT).toString().isEmpty()){
									 			deduction.setNoOfInstallments(rowJSON.get(colName.get(CommonConstants.NOOFINSTALLMENT)).toString());
									 			long noOfInstalment = Long.parseLong(rowJSON.get(colName.get(CommonConstants.NOOFINSTALLMENT)).toString());
									 			Date currentDate = CommonUtil.convertMonthToSqlDate(rowJSON.get(colName.get(CommonConstants.MONTHLY)).toString());
									 			Long amount = Long.parseLong(rowJSON.get(colName.get(CommonConstants.AMOUNT)).toString())/Long.parseLong(rowJSON.get(colName.get(CommonConstants.NOOFINSTALLMENT)).toString());
									 			for (int j=0; j < noOfInstalment; j++){
								 		        	Calendar currentMonth1 = Calendar.getInstance();
								 		        	currentMonth1.setTime(currentDate);
								 		        	if(j != 0)currentMonth1.add(Calendar.MONTH, 1);
								 		        	Date nextMonth = currentMonth1.getTime();
								 			        currentDate = currentMonth1.getTime();
								 			        String dateToAdd = dateFormat.format(nextMonth.getTime()).substring(0, 3) +" "+ currentMonth1.get(Calendar.YEAR);
								 			        System.out.println(dateToAdd);
								 			        GeneralDeductionDetailDO loanPaymentDetailDO = new GeneralDeductionDetailDO();
								 			        loanPaymentDetailDO.setAmount(amount.toString());
								 			        loanPaymentDetailDO.setMonth(dateToAdd.toString());
								 			        loanPaymentDetailDO.setEmployee(employeeList.get(0));
								 			        loanPaymentDetailDO.setLoanPayment(updatedadvancePayment);
								 					loanPaymentDetailDO.setUpdatedon(new Date());
								 					loanPaymentDetailDO.setCreatedon(new Date());
								 					loanPaymentDetailDO.setStatus(CommonConstants.ACTIVESTRING);
								 					if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
											 			loanPaymentDetailDO.setUpdatedby(updatedBy.toString());
											 			loanPaymentDetailDO.setCreatedby(updatedBy.toString());
											 		}
											 		loanPaymentDetailService.persist(loanPaymentDetailDO);
									 			}
									 		}
								 		}
							 		}
						 		}
				 				
				 				
				 			}
				 		}
			 			
			 			
			 		}
		 		}
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	/*@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public @ResponseBody String delete(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		Boolean employeeAddressList = employeeBonusService.delete(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(employeeAddressList){
			 			CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Bonus Deleted For Employee");
			 		}else{
			 			return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retriveForBonusReport/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveForBonusReport(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.MONTHLY).toString().isEmpty()){
					Long batchId = null;
					List<Long> ids = new ArrayList<Long>();
					if(!inputJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty()){
						batchId = Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString());
						if(batchId == 0){
							batchId = null;
						}else{
							ids = employeeCtcService.retrieveByBatchId(batchId);
						}
					}
					List<EmployeeBonusDO> bonusList = employeeBonusService.retrieveByEmpIdWithDate(ids, inputJSON.get(CommonConstants.MONTHLY).toString());
					respJSON = EmployeeBonusUtil.getEmployeeBonusList(bonusList, CommonConstants.MONTHLYC, inputJSON.get(CommonConstants.MONTHLY).toString()).toString();
				}else{
					if(inputJSON != null && !inputJSON.get(CommonConstants.QUARTERLY).toString().isEmpty()){
						String fromMonth = null;
						String toMonth = null;
						Long batchId = null;
						List<FyDO> fyList = new ArrayList<FyDO>();
						if(!inputJSON.get(CommonConstants.FYID).toString().isEmpty()){
							fyList = fyService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.FYID).toString()));
							if(fyList != null && fyList.size() > 0){
								List<Long> ids = new ArrayList<Long>();
								if(!inputJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty()){
									batchId = Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString());
									if(batchId == 0){
										batchId = null;
									}else{
										ids = employeeCtcService.retrieveByBatchId(batchId);
									}
								}
								if(inputJSON.get(CommonConstants.QUARTERLY).toString().equalsIgnoreCase("Q1")){
									fromMonth = "Apr"+" "+fyList.get(0).getFromyear();
									toMonth = "Jun"+" "+fyList.get(0).getFromyear(); 
									List<EmployeeBonusDO> bonusList = employeeBonusService.retrieveByEmpIdBetweenDate(ids,fromMonth, toMonth);
									if(bonusList != null && bonusList.size() > 0){
										respJSON = EmployeeBonusUtil.MultipleMonthBonuReport(bonusList,CommonConstants.QUARTERLYC, fromMonth+" - "+toMonth).toString();
									}
								}
								
								if(inputJSON.get(CommonConstants.QUARTERLY).toString().equalsIgnoreCase("Q2")){
									fromMonth = "Jul"+" "+fyList.get(0).getFromyear();
									toMonth = "Sep"+" "+fyList.get(0).getFromyear(); 
									List<EmployeeBonusDO> bonusList = employeeBonusService.retrieveByEmpIdBetweenDate(ids,fromMonth, toMonth);
									if(bonusList != null && bonusList.size() > 0){
										respJSON = EmployeeBonusUtil.MultipleMonthBonuReport(bonusList,CommonConstants.QUARTERLYC, fromMonth+" - "+toMonth).toString();
									}
								}
								
								
								if(inputJSON.get(CommonConstants.QUARTERLY).toString().equalsIgnoreCase("Q3")){
									fromMonth = "Oct"+" "+fyList.get(0).getFromyear();
									toMonth = "Dec"+" "+fyList.get(0).getFromyear(); 
									List<EmployeeBonusDO> bonusList = employeeBonusService.retrieveByEmpIdBetweenDate(ids,fromMonth, toMonth);
									if(bonusList != null && bonusList.size() > 0){
										respJSON = EmployeeBonusUtil.MultipleMonthBonuReport(bonusList,CommonConstants.QUARTERLYC, fromMonth+" - "+toMonth).toString();
									}
								}
								if(inputJSON.get(CommonConstants.QUARTERLY).toString().equalsIgnoreCase("Q4")){
									fromMonth = "Jan"+" "+fyList.get(0).getToyear();
									toMonth = "Mar"+" "+fyList.get(0).getToyear(); 
									List<EmployeeBonusDO> bonusList = employeeBonusService.retrieveByEmpIdBetweenDate(ids,fromMonth, toMonth);
									if(bonusList != null && bonusList.size() > 0){
										respJSON = EmployeeBonusUtil.MultipleMonthBonuReport(bonusList,CommonConstants.QUARTERLYC, fromMonth+" - "+toMonth).toString();
									}
								}
							}
							
						}
					}else{
						if(inputJSON != null && !inputJSON.get(CommonConstants.FREQUENCY).toString().isEmpty()
								&& inputJSON.get(CommonConstants.FREQUENCY).toString().equalsIgnoreCase("Y")){
							String fromMonth = null;
							String toMonth = null;
							Long batchId = null;
							List<FyDO> fyList = new ArrayList<FyDO>();
							if(!inputJSON.get(CommonConstants.FYID).toString().isEmpty()){
								fyList = fyService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.FYID).toString()));
								if(fyList != null && fyList.size() > 0){
									List<Long> ids = new ArrayList<Long>();
									if(!inputJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty()){
										batchId = Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString());
										if(batchId == 0){
											batchId = null;
										}else{
											ids = employeeCtcService.retrieveByBatchId(batchId);
										}
									}
									fromMonth = "Apr"+" "+fyList.get(0).getFromyear();
									toMonth = "Mar"+" "+fyList.get(0).getToyear(); 
									List<EmployeeBonusDO> bonusList = employeeBonusService.retrieveByEmpIdBetweenDate(ids,fromMonth, toMonth);
									if(bonusList != null && bonusList.size() > 0){
										respJSON = EmployeeBonusUtil.MultipleMonthBonuReport(bonusList,CommonConstants.YEARLYC, fromMonth+" - "+toMonth).toString();
									}
								}
							}
						}
					}
				}
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}*/
}
