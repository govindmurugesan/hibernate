package com.spheresuite.erp.rs;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.ItSlabEffectiveDO;
import com.spheresuite.erp.service.ItSlabEffectiveService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.ItSlabEffetctiveUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/itSlabEffective")
public class ItSlabEffectiveRS {

	String validation = null;
	static Logger logger = Logger.getLogger(ItSlabEffectiveRS.class.getName());

	@Autowired
	private ItSlabEffectiveService itSlabEffectiveService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				ItSlabEffectiveDO itSlabEffectiveDO = new ItSlabEffectiveDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.FROM) != null && !inputJSON.get(CommonConstants.FROM).toString().isEmpty()){
			 			itSlabEffectiveDO.setEffectivefromDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.FROM).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.TO) != null && !inputJSON.get(CommonConstants.TO).toString().isEmpty()){
			 			itSlabEffectiveDO.setEffectiveToDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.TO).toString()));
			 		}
			 		itSlabEffectiveDO.setStatus(CommonConstants.ACTIVE);
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			itSlabEffectiveDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			itSlabEffectiveDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		itSlabEffectiveDO.setUpdatedon(new Date());
			 		itSlabEffectiveDO.setCreatedon(new Date());
			 	}
				if(!itSlabEffectiveService.persist(itSlabEffectiveDO)){
					return CommonWebUtil.buildErrorResponse("Date Already Added For This Period").toString();
				}
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New IT Slab Effective Date Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
/*	@RequestMapping(value = "/retrieveActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<ItSlabEffectiveDO> itSlabEffectiveList = itSlabEffectiveService.retrieveActive();
				respJSON = ItSectionUtil.getSectionList(itSlabEffectiveList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}*/
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<ItSlabEffectiveDO> itSlabEffectiveList = itSlabEffectiveService.retrieve();
				respJSON = ItSlabEffetctiveUtil.getItSlabEffectiveList(itSlabEffectiveList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	/*@RequestMapping(value = "/retrieveByDate/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieve(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				List<ItSlabEffectiveDO> itSectionList = itSectionService.retrieveByDate(inputJSON.get(CommonConstants.FROMDATE).toString(),inputJSON.get(CommonConstants.TODATE).toString());
				respJSON = ItSectionUtil.getSectionList(itSectionList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}*/
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				ItSlabEffectiveDO itSlabEffectiveDO = new ItSlabEffectiveDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<ItSlabEffectiveDO> itSlabEffectiveList = itSlabEffectiveService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(itSlabEffectiveList != null && itSlabEffectiveList.size() > 0){
			 			itSlabEffectiveDO = itSlabEffectiveList.get(0);
				 		
				 		if(inputJSON.get(CommonConstants.FROM) != null && !inputJSON.get(CommonConstants.FROM).toString().isEmpty()){
				 			itSlabEffectiveDO.setEffectivefromDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.FROM).toString()));
				 		}
				 		if(inputJSON.get(CommonConstants.TO) != null && !inputJSON.get(CommonConstants.TO).toString().isEmpty()){
				 			itSlabEffectiveDO.setEffectiveToDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.TO).toString()));
				 		}
				 		if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
				 			itSlabEffectiveDO.setStatus((char)inputJSON.get(CommonConstants.STATUS).toString().charAt(0));
				 		}
				 		itSlabEffectiveDO.setUpdatedon(new Date());
				 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			itSlabEffectiveDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		if(!itSlabEffectiveService.update(itSlabEffectiveDO)){
							return CommonWebUtil.buildErrorResponse("Date Already Added For This Period").toString();
						}
				 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "It Slab Effetive Updated");
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	/*@RequestMapping(value = "/importITSection", method = RequestMethod.POST)
	public @ResponseBody String importITSection(Model model, HttpServletRequest request) {
		try {
			JSONObject inputJSON =  CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
			JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
			List<ItSlabEffectiveDO> itSectionlist = new ArrayList<ItSlabEffectiveDO>();
			Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY);
			for (int i=0; i < fileData.size(); i++){
				JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
				ItSlabEffectiveDO ItSlabEffectiveDO = new ItSlabEffectiveDO();
				
				if (colName.get(CommonConstants.SECTION) != null && !colName.get(CommonConstants.SECTION).toString().isEmpty()){
					ItSlabEffectiveDO.setSection(rowJSON.get(colName.get(CommonConstants.SECTION)).toString());
		 		}
				if(colName.get(CommonConstants.MAXLIMIT) != null && !colName.get(CommonConstants.MAXLIMIT).toString().isEmpty()){
					ItSlabEffectiveDO.setMaxLimit(Long.parseLong(rowJSON.get(colName.get(CommonConstants.MAXLIMIT)).toString()));
				}
				if(colName.get(CommonConstants.TODATE) != null && !colName.get(CommonConstants.TODATE).toString().isEmpty()){
					ItSlabEffectiveDO.setToYear(CommonUtil.convertStringToSqlDate(rowJSON.get(colName.get(CommonConstants.TODATE)).toString()));
				}
				if(colName.get(CommonConstants.FROMDATE) != null && !colName.get(CommonConstants.FROMDATE).toString().isEmpty()){
					ItSlabEffectiveDO.setFromYear(CommonUtil.convertStringToSqlDate(rowJSON.get(colName.get(CommonConstants.FROMDATE)).toString()));
				}
		 		if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
		 			ItSlabEffectiveDO.setUpdatedby(updatedBy.toString());
		 			ItSlabEffectiveDO.setCreatedby(updatedBy.toString());
		 		}
		 		ItSlabEffectiveDO.setUpdatedon(new Date());
		 		ItSlabEffectiveDO.setCreatedon(new Date());
		 		ItSlabEffectiveDO.setStatus(CommonConstants.ACTIVE);
		 		itSectionlist.add(ItSlabEffectiveDO);
			}
			itSectionService.persistList(itSectionlist);
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}*/
}
