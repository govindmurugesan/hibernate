package com.spheresuite.erp.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.LeaveManagementDO;
import com.spheresuite.erp.domainobject.WorkingDayAllowanceDO;
import com.spheresuite.erp.domainobject.WorkingDayLeavetypeDO;
import com.spheresuite.erp.service.LeaveManagementService;
import com.spheresuite.erp.service.WorkingDayAllowanceService;
import com.spheresuite.erp.service.WorkingDayLeaveTypeService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;
import com.spheresuite.erp.web.util.WorkingDayAllowanceUtil;

@Controller
@RequestMapping(value = "/workingDayAllowance")
public class WorkingDayAllowanceRS {

	String validation = null;
	static Logger logger = Logger.getLogger(WorkingDayAllowanceRS.class.getName());
	
	@Autowired
	private WorkingDayAllowanceService workingDayAllowanceService;
	
	@Autowired
	private LeaveManagementService leaveManagementService;
	
	@Autowired
	private WorkingDayLeaveTypeService workingDayLeaveService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				WorkingDayAllowanceDO workingDayAllowanceDO = new WorkingDayAllowanceDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
			 			workingDayAllowanceDO.setName(inputJSON.get(CommonConstants.NAME).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.COST) != null && !inputJSON.get(CommonConstants.COST).toString().isEmpty()){
			 			workingDayAllowanceDO.setCost(Double.parseDouble(inputJSON.get(CommonConstants.COST).toString()));
			 		}
			 		/*if(inputJSON.get(CommonConstants.COSTEFFECTIVE) != null && !inputJSON.get(CommonConstants.COSTEFFECTIVE).toString().isEmpty()){
			 			workingDayAllowanceDO.setCostEffective(inputJSON.get(CommonConstants.COSTEFFECTIVE).toString());
			 		}*/
			 		/*if(inputJSON.get(CommonConstants.TYPE) != null && !inputJSON.get(CommonConstants.TYPE).toString().isEmpty()){
			 			workingDayAllowanceDO.setType(inputJSON.get(CommonConstants.TYPE).toString());
			 		}*/
			 		/*if(inputJSON.get(CommonConstants.CONDITION) != null && !inputJSON.get(CommonConstants.CONDITION).toString().isEmpty()){
			 			workingDayAllowanceDO.setCalculationType(inputJSON.get(CommonConstants.CONDITION).toString());
			 		}*/
			 		workingDayAllowanceDO.setStatus(CommonConstants.ACTIVE);
			 		workingDayAllowanceDO.setUpdatedon(new Date());
			 		workingDayAllowanceDO.setCreatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			workingDayAllowanceDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			workingDayAllowanceDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		
			 		WorkingDayAllowanceDO Details= workingDayAllowanceService.persist(workingDayAllowanceDO);
			 		if(Details != null && Details.getWorkingDayAllowanceId() != null){
			 			if(inputJSON.get(CommonConstants.LEAVETYPE) != null && ! inputJSON.get(CommonConstants.LEAVETYPE).toString().isEmpty()){
			 				org.json.simple.JSONArray leaveList = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.LEAVETYPE).toString());
			 				//List<PayrollEarningsDetailsDO> earningDetailsList = new ArrayList<PayrollEarningsDetailsDO>(); 
			 				List<WorkingDayLeavetypeDO> workingLeavetypeList = new ArrayList<WorkingDayLeavetypeDO>();; 
			 				for (int j=0; j < leaveList.size(); j++){
			 					JSONObject workingdayLeaveObject = CommonWebUtil.getInputParams(leaveList.get(j).toString());
			 					WorkingDayLeavetypeDO workingDayLeavetypeDO = new WorkingDayLeavetypeDO();
			 					List<LeaveManagementDO> leaveMangmentList = leaveManagementService.retrieveById(Long.parseLong(workingdayLeaveObject.get(CommonConstants.TYPEID).toString()));
				 				if(leaveMangmentList != null && leaveMangmentList.size() > 0){
				 					workingDayLeavetypeDO.setLeaveManagement(leaveMangmentList.get(0));
				 					workingDayLeavetypeDO.setWorkingDayAllowance(workingDayAllowanceDO);
				 					workingDayLeavetypeDO.setStatus(CommonConstants.ACTIVESTRING);
				 					workingDayLeavetypeDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 					workingDayLeavetypeDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 					workingDayLeavetypeDO.setUpdatedon(new Date());
				 					workingDayLeavetypeDO.setCreatedon(new Date());
				 					workingLeavetypeList.add(workingDayLeavetypeDO);
							 		//workingDayLeaveService.persist(workingDayLeavetypeDO);
				 				}
			 				}
			 				if(workingLeavetypeList != null && workingLeavetypeList.size() > 0){
			 					workingDayLeaveService.persistList(workingLeavetypeList);
			 				}
			 			}
			 		}
			 		
			 	}
			 	
			 	
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New WorkingDay Allowance created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	/*@RequestMapping(value = "/retrieveActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<AddressTypeDO> addressTypeList = addressTypeService.retrieveActive();
				respJSON = AddressTypeUtil.getAddressTypeList(addressTypeList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}*/
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<WorkingDayAllowanceDO> workingDayAllowanceList = workingDayAllowanceService.retrieve();
				respJSON = WorkingDayAllowanceUtil.getWorkingDayAllowanceList(workingDayAllowanceList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				WorkingDayAllowanceDO workingDayAllowanceDO = new WorkingDayAllowanceDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<WorkingDayAllowanceDO> workingDayAllowanceList = workingDayAllowanceService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(workingDayAllowanceList != null && workingDayAllowanceList.size() > 0){
			 			workingDayAllowanceDO = workingDayAllowanceList.get(0);
			 			if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
				 			workingDayAllowanceDO.setName(inputJSON.get(CommonConstants.NAME).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.COST) != null && !inputJSON.get(CommonConstants.COST).toString().isEmpty()){
				 			workingDayAllowanceDO.setCost(Double.parseDouble(inputJSON.get(CommonConstants.COST).toString()));
				 		}
				 		/*if(inputJSON.get(CommonConstants.COSTEFFECTIVE) != null && !inputJSON.get(CommonConstants.COSTEFFECTIVE).toString().isEmpty()){
				 			workingDayAllowanceDO.setCostEffective(inputJSON.get(CommonConstants.COSTEFFECTIVE).toString());
				 		}*/
				 		/*if(inputJSON.get(CommonConstants.TYPE) != null && !inputJSON.get(CommonConstants.TYPE).toString().isEmpty()){
				 			workingDayAllowanceDO.setType(inputJSON.get(CommonConstants.TYPE).toString());
				 		}
*/				 		/*if(inputJSON.get(CommonConstants.CONDITION) != null && !inputJSON.get(CommonConstants.CONDITION).toString().isEmpty()){
				 			workingDayAllowanceDO.setCalculationType(inputJSON.get(CommonConstants.CONDITION).toString());
				 		}*/
				 		if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
				 			workingDayAllowanceDO.setStatus((char) (inputJSON.get(CommonConstants.STATUS).toString().charAt(0)));
				 		}
				 		workingDayAllowanceService.update(workingDayAllowanceDO);
				 		if(workingDayAllowanceDO != null && workingDayAllowanceDO.getWorkingDayAllowanceId() != null){
				 			if(inputJSON.get(CommonConstants.LEAVETYPE) != null && ! inputJSON.get(CommonConstants.LEAVETYPE).toString().isEmpty()){
				 				org.json.simple.JSONArray leaveList = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.LEAVETYPE).toString());
				 				//List<PayrollEarningsDetailsDO> earningDetailsList = new ArrayList<PayrollEarningsDetailsDO>(); 
				 				List<WorkingDayLeavetypeDO> workingLeavetypeList = new ArrayList<WorkingDayLeavetypeDO>();; 
				 				for (int j=0; j < leaveList.size(); j++){
				 					JSONObject workingdayLeaveObject = CommonWebUtil.getInputParams(leaveList.get(j).toString());
				 					WorkingDayLeavetypeDO workingDayLeavetypeDO = new WorkingDayLeavetypeDO();
				 					List<LeaveManagementDO> leaveMangmentList = leaveManagementService.retrieveById(Long.parseLong(workingdayLeaveObject.get(CommonConstants.TYPEID).toString()));
					 				if(leaveMangmentList != null && leaveMangmentList.size() > 0){
					 					workingDayLeavetypeDO.setLeaveManagement(leaveMangmentList.get(0));
					 					workingDayLeavetypeDO.setWorkingDayAllowance(workingDayAllowanceDO);
					 					workingDayLeavetypeDO.setStatus(CommonConstants.ACTIVESTRING);
					 					workingDayLeavetypeDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 					workingDayLeavetypeDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 					workingDayLeavetypeDO.setUpdatedon(new Date());
					 					workingDayLeavetypeDO.setCreatedon(new Date());
					 					workingLeavetypeList.add(workingDayLeavetypeDO);
								 		//workingDayLeaveService.persist(workingDayLeavetypeDO);
					 				}
				 				}
				 				if(workingLeavetypeList != null && workingLeavetypeList.size() > 0){
				 					workingDayLeaveService.persistList(workingLeavetypeList);
				 				}
				 			}
				 		}
				 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Allowance Updated");
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
}
