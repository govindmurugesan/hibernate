package com.spheresuite.erp.rs;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.AdvancePaymentDetailDO;
import com.spheresuite.erp.domainobject.AllowanceSettingDO;
import com.spheresuite.erp.domainobject.AttendanceAllowanceDO;
import com.spheresuite.erp.domainobject.DASettingsDO;
import com.spheresuite.erp.domainobject.DeductionSettingDO;
import com.spheresuite.erp.domainobject.EmployeeBonusDO;
import com.spheresuite.erp.domainobject.EmployeeCompensationDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.EmployeeExpenseDO;
import com.spheresuite.erp.domainobject.EmployeeLopDO;
import com.spheresuite.erp.domainobject.EmployeePayrollDO;
import com.spheresuite.erp.domainobject.GeneralDeductionDetailDO;
import com.spheresuite.erp.domainobject.GeneralEarningDO;
import com.spheresuite.erp.domainobject.InsurancePaymentDetailDO;
import com.spheresuite.erp.domainobject.PayrollBatchDO;
import com.spheresuite.erp.domainobject.PayrollDeductionDetailsDO;
import com.spheresuite.erp.domainobject.PayrollEarningsDetailsDO;
import com.spheresuite.erp.domainobject.PayrollGroupDO;
import com.spheresuite.erp.domainobject.PayrollGroupDeductionDO;
import com.spheresuite.erp.domainobject.PayrollGroupEarningsDO;
import com.spheresuite.erp.domainobject.PayrollGroupTaxDO;
import com.spheresuite.erp.domainobject.PayrollTaxDetailsDO;
import com.spheresuite.erp.domainobject.TaxSettingsDO;
import com.spheresuite.erp.domainobject.WorkingDayAllowanceDO;
import com.spheresuite.erp.service.AdvancePaymentDetailService;
import com.spheresuite.erp.service.AllowanceSettingsService;
import com.spheresuite.erp.service.AttendanceAllowanceService;
import com.spheresuite.erp.service.DASettingsService;
import com.spheresuite.erp.service.DeductionSettingsService;
import com.spheresuite.erp.service.EmployeeBonusService;
import com.spheresuite.erp.service.EmployeeCompensationService;
import com.spheresuite.erp.service.EmployeeDeductionService;
import com.spheresuite.erp.service.EmployeeEarningsService;
import com.spheresuite.erp.service.EmployeeExpenseService;
import com.spheresuite.erp.service.EmployeeLopService;
import com.spheresuite.erp.service.EmployeePayrollService;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.EmployeeTaxService;
import com.spheresuite.erp.service.FyService;
import com.spheresuite.erp.service.GeneralDeductionDetailService;
import com.spheresuite.erp.service.GeneralEarningService;
import com.spheresuite.erp.service.InsurancePaymentDetailService;
import com.spheresuite.erp.service.PayrollBatchService;
import com.spheresuite.erp.service.PayrollDeductionDetailsService;
import com.spheresuite.erp.service.PayrollEarningsDetailsService;
import com.spheresuite.erp.service.PayrollGroupDeductionService;
import com.spheresuite.erp.service.PayrollGroupEarningService;
import com.spheresuite.erp.service.PayrollGroupService;
import com.spheresuite.erp.service.PayrollGroupTaxService;
import com.spheresuite.erp.service.PayrollLopDetailsService;
import com.spheresuite.erp.service.PayrollTaxDetailsService;
import com.spheresuite.erp.service.TaxSettingsService;
import com.spheresuite.erp.service.WorkingDayAllowanceService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.EmployeePayrollUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/empPayroll")
public class EmployeePayrollRS {

	String validation = null;
	static Logger logger = Logger.getLogger(EmployeePayrollRS.class.getName());

	@Autowired
	private EmployeePayrollService employeePayrollService;
	
	@Autowired
	private PayrollLopDetailsService payrollLopDetailsService;
	
	@Autowired
	private EmployeeLopService employeeLopService;
	
	@Autowired
	private EmployeeEarningsService employeeEarningsService;
	
	@Autowired
	private EmployeeDeductionService employeeDeductionService;
	
	@Autowired
	private PayrollDeductionDetailsService payrollDeductionDetailsService;
	
	@Autowired
	private PayrollTaxDetailsService payrollTaxDetailsService;
	
	@Autowired
	private PayrollEarningsDetailsService payrollEarningsDetailsService;
	
	@Autowired
	private EmployeeTaxService employeeTaxService;
	
	@Autowired
	private FyService fyService;
	
	@Autowired
	private PayrollGroupService payrollGroupService;
	
	@Autowired
	private PayrollGroupEarningService payrollGroupEarningService;
	
	@Autowired
	private PayrollGroupDeductionService payrollGroupDeductionService;
	
	@Autowired
	private PayrollGroupTaxService payrollGroupTaxService;
	
	@Autowired
	private EmployeeService employeeService;
	
	@Autowired
	private EmployeeCompensationService employeeCompensationService;
	
	@Autowired
	private PayrollBatchService payrollBatchService;
	
	@Autowired
	private EmployeeBonusService employeeBonusService;
	
	@Autowired
	private DASettingsService daSettingsService;
	
	@Autowired
	private AdvancePaymentDetailService advancePaymentDetailService;
	
	@Autowired
	private GeneralDeductionDetailService loanPaymentDetailService;
	
	@Autowired
	private InsurancePaymentDetailService insurancePaymentDetailService;
	
	@Autowired
	private GeneralEarningService generalEarningService;
	
	@Autowired
	private AllowanceSettingsService allowanceSettingsService;
	
	@Autowired
	private DeductionSettingsService deductionSettingsService;

	@Autowired
	private TaxSettingsService  taxSettingsService;
	
	@Autowired
	private WorkingDayAllowanceService  workingDayAllowanceService;
	
	@Autowired
	private AttendanceAllowanceService  attendanceAllowanceService;
	
	@Autowired
	private EmployeeExpenseService  employeeExpenseService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				org.json.simple.JSONArray inputJSON = CommonWebUtil.getInputParamsArray(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && inputJSON.size() > 0){
			 		for (int i=0; i < inputJSON.size(); i++){
			 			EmployeePayrollDO employeePayrollDO = new EmployeePayrollDO();
			 			JSONObject rowJSON = CommonWebUtil.getInputParams(inputJSON.get(i).toString());
			 			List<PayrollGroupDO> payrollGroupList = null;
			 			List<EmployeePayrollDO> payrollList = null;
			 			if(rowJSON.get(CommonConstants.PAYROLLBATCHID) != null && !rowJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty()){
			 				List<PayrollBatchDO> batchList = payrollBatchService.retrieveById(Long.parseLong(rowJSON.get(CommonConstants.PAYROLLBATCHID).toString()));
					 		if(batchList != null && batchList.size() > 0){
					 			if(batchList.get(0).getPayrollType().equalsIgnoreCase("Weekly")){
					 				SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
					 				String strFromDate= formatter.format(CommonUtil.convertStringToDate(rowJSON.get(CommonConstants.PAYROLLWEEKFROM).toString()));
					 				SimpleDateFormat formatter2 = new SimpleDateFormat("dd-MM-yyyy");
					 				String strToDate= formatter2.format(CommonUtil.convertStringToDate(rowJSON.get(CommonConstants.PAYROLLWEEKTO).toString()));
					 				payrollList = employeePayrollService.retriveByWeekAndEmp(strFromDate, strToDate,rowJSON.get(CommonConstants.EMPID).toString());
					 			}else if(batchList.get(0).getPayrollType().equalsIgnoreCase("Daily")){
					 				System.out.println("aa"+rowJSON.get(CommonConstants.PAYROLLDATE).toString());
					 				payrollList = employeePayrollService.retriveDailyEmpPayroll(rowJSON.get(CommonConstants.EMPID).toString(),rowJSON.get(CommonConstants.PAYROLLDATE).toString(),Long.parseLong(rowJSON.get(CommonConstants.PAYROLLBATCHID).toString()));
					 			}
					 			else{
					 				payrollList = employeePayrollService.retriveByMonthAndEmp(rowJSON.get(CommonConstants.PAYROLLMONTH).toString(), rowJSON.get(CommonConstants.EMPID).toString());
					 			}
					 			
					 			employeePayrollDO.setPayrollBatch(batchList.get(0));
					 		}
		 				}
			 			
			 			if(payrollList == null || payrollList.size() <= 0){
			 				if(rowJSON.get(CommonConstants.PAYROLLGROUPID) != null && !rowJSON.get(CommonConstants.PAYROLLGROUPID).toString().isEmpty()){
					 			 payrollGroupList = payrollGroupService.retrieveById(Long.parseLong(rowJSON.get(CommonConstants.PAYROLLGROUPID).toString()));
						 		if(payrollGroupList != null && payrollGroupList.size() >0){
						 			employeePayrollDO.setPayrollGroup(payrollGroupList.get(0));
						 		}
					 		}
			 				if(rowJSON.get(CommonConstants.PAYROLLMONTH) != null && !rowJSON.get(CommonConstants.PAYROLLMONTH).toString().isEmpty()){
			 					employeePayrollDO.setPayrollMonth(rowJSON.get(CommonConstants.PAYROLLMONTH).toString());
			 				}
			 				if(rowJSON.get(CommonConstants.PAYROLLWEEKFROM) != null && !rowJSON.get(CommonConstants.PAYROLLWEEKFROM).toString().isEmpty()){
			 					employeePayrollDO.setPayrollWeekFrom(rowJSON.get(CommonConstants.PAYROLLWEEKFROM).toString());
			 				}
			 				if(rowJSON.get(CommonConstants.PAYROLLWEEKTO) != null && !rowJSON.get(CommonConstants.PAYROLLWEEKTO).toString().isEmpty()){
			 					employeePayrollDO.setPayrollWeekTo(rowJSON.get(CommonConstants.PAYROLLWEEKTO).toString());
			 				}
			 				if(rowJSON.get(CommonConstants.PAYROLLDATE) != null && !rowJSON.get(CommonConstants.PAYROLLDATE).toString().isEmpty()){
			 					System.out.println("rowJSON.get(CommonConstants.PAYROLLDATE)"+rowJSON.get(CommonConstants.PAYROLLDATE));
			 					employeePayrollDO.setPayrollDate(rowJSON.get(CommonConstants.PAYROLLDATE).toString());
			 				}
			 				/*if(rowJSON.get(CommonConstants.PAYROLLMONTH) != null && !rowJSON.get(CommonConstants.PAYROLLMONTH).toString().isEmpty()){
			 					employeePayrollDO.setPayrollMonth(rowJSON.get(CommonConstants.PAYROLLMONTH).toString());
			 				}*/
			 				if(rowJSON.get(CommonConstants.PAYROLLBATCHID) != null && !rowJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty()){
				 				List<PayrollBatchDO> batchList = payrollBatchService.retrieveById(Long.parseLong(rowJSON.get(CommonConstants.PAYROLLBATCHID).toString()));
						 		if(batchList != null && batchList.size() > 0){
						 			employeePayrollDO.setPayrollBatch(batchList.get(0));
						 		}
			 				}
			 				if(rowJSON.get(CommonConstants.EMPID) != null && !rowJSON.get(CommonConstants.EMPID).toString().isEmpty()){
					 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(rowJSON.get(CommonConstants.EMPID).toString());
						 		if(employeeList != null && employeeList.size() > 0){
						 			employeePayrollDO.setEmployee(employeeList.get(0));
						 		}
					 		}
			 				if(rowJSON.get(CommonConstants.ID) != null && !rowJSON.get(CommonConstants.ID).toString().isEmpty()){
			 					List<EmployeeCompensationDO> empCompensationList = employeeCompensationService.retrieveById(Long.parseLong(rowJSON.get(CommonConstants.ID).toString()));
						 		if(empCompensationList != null && empCompensationList.size() > 0){
						 			employeePayrollDO.setEmployeeCompensation(empCompensationList.get(0));
						 		}
					 		}
			 				if(rowJSON.get(CommonConstants.NETMONTHLY) != null && !rowJSON.get(CommonConstants.NETMONTHLY).toString().isEmpty()){
			 					employeePayrollDO.setNetmonth(Double.parseDouble(rowJSON.get(CommonConstants.NETMONTHLY).toString()));
			 				}
				 			if(rowJSON.get(CommonConstants.TAXMONTHLY) != null && !rowJSON.get(CommonConstants.TAXMONTHLY).toString().isEmpty() && !rowJSON.get(CommonConstants.TAXMONTHLY).toString().equalsIgnoreCase("null")){
				 				employeePayrollDO.setTaxMonth((Double.parseDouble(rowJSON.get(CommonConstants.TAXMONTHLY).toString())));
				 			}
				 			if(rowJSON.get(CommonConstants.EARNINGMONTHLY) != null && !rowJSON.get(CommonConstants.EARNINGMONTHLY).toString().isEmpty() && !rowJSON.get(CommonConstants.EARNINGMONTHLY).toString().equalsIgnoreCase("null")){
				 				employeePayrollDO.setEarningmonth((Double.parseDouble(rowJSON.get(CommonConstants.EARNINGMONTHLY).toString())));
				 			}
				 			if(rowJSON.get(CommonConstants.DEDUCTIONMONTHLY) != null && !rowJSON.get(CommonConstants.DEDUCTIONMONTHLY).toString().isEmpty()){
				 				employeePayrollDO.setDeductionmonth((Double.parseDouble(rowJSON.get(CommonConstants.DEDUCTIONMONTHLY).toString())));
				 			}
				 			if(rowJSON.get(CommonConstants.BASICGROSS_PAY) != null && !rowJSON.get(CommonConstants.BASICGROSS_PAY).toString().isEmpty()){
				 				employeePayrollDO.setGrossPay((Double.parseDouble(rowJSON.get(CommonConstants.BASICGROSS_PAY).toString())));
				 			}
				 			
				 			employeePayrollDO.setUpdatedon(new Date());
				 			employeePayrollDO.setCreatedon(new Date());
					 		if(rowJSON.get(CommonConstants.UPDATED_BY).toString() != null && !rowJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			employeePayrollDO.setUpdatedby(rowJSON.get(CommonConstants.UPDATED_BY).toString());
					 			employeePayrollDO.setCreatedby(rowJSON.get(CommonConstants.UPDATED_BY).toString());
					 		}
					 		employeePayrollDO.setStatus(CommonConstants.DRAFT);
					 		
					 		EmployeePayrollDO employeePayrollObject = employeePayrollService.persist(employeePayrollDO);
					 		
					 		if(employeePayrollObject != null && !employeePayrollObject.getPayrollId().toString().isEmpty()){
					 			if(rowJSON.get(CommonConstants.EARNINGLIST) != null && ! rowJSON.get(CommonConstants.EARNINGLIST).toString().isEmpty()){
					 				org.json.simple.JSONArray earningList = CommonWebUtil.getInputParamsArray(rowJSON.get(CommonConstants.EARNINGLIST).toString());
					 				List<PayrollEarningsDetailsDO> earningDetailsList = new ArrayList<PayrollEarningsDetailsDO>(); 
					 				for (int j=0; j < earningList.size(); j++){
					 					JSONObject earningObject = CommonWebUtil.getInputParams(earningList.get(j).toString());
					 					PayrollEarningsDetailsDO earingDO = new PayrollEarningsDetailsDO();
					 					if(earningObject.get(CommonConstants.NAME) != null && !earningObject.get(CommonConstants.NAME).toString().isEmpty()){
					 						earingDO.setName(earningObject.get(CommonConstants.NAME).toString());
					 					}
					 					if(earningObject.get(CommonConstants.MONTHLY) != null && !earningObject.get(CommonConstants.MONTHLY).toString().isEmpty()){
					 						earingDO.setMonthly((Double.parseDouble(earningObject.get(CommonConstants.MONTHLY).toString())));
					 					}
					 					if(earningObject.get(CommonConstants.YTD) != null && !earningObject.get(CommonConstants.YTD).toString().isEmpty()){
					 						earingDO.setYtd((Double.parseDouble(earningObject.get(CommonConstants.YTD).toString())));
					 					}
					 					if(payrollGroupList != null && payrollGroupList.size() >0){
								 			earingDO.setPayrollGroup(payrollGroupList.get(0));
								 		}
					 					if(earningObject.get(CommonConstants.EARNINGID) != null && !earningObject.get(CommonConstants.EARNINGID).toString().isEmpty()){
					 						
					 						List<PayrollGroupEarningsDO> payrollGroupEarningList = payrollGroupEarningService.retrieveByID(Long.parseLong(earningObject.get(CommonConstants.EARNINGID).toString()));
									 		if(payrollGroupEarningList != null && payrollGroupEarningList.size() >0){
									 			earingDO.setPayrollGroupEarning(payrollGroupEarningList.get(0));
									 		}
								 		}
					 					
					 					if(earningObject.get(CommonConstants.BONUSID) != null && !earningObject.get(CommonConstants.BONUSID).toString().isEmpty()){
					 						List<EmployeeBonusDO> bonusList = employeeBonusService.retrieveById(Long.parseLong(earningObject.get(CommonConstants.BONUSID).toString()));
									 		if(bonusList != null && bonusList.size() >0){
									 			earingDO.setBonus(bonusList.get(0));
									 		}
								 		}
					 					
					 					if(earningObject.get(CommonConstants.DEARNESSALLOWEANCEID) != null && !earningObject.get(CommonConstants.DEARNESSALLOWEANCEID).toString().isEmpty()){
					 						List<DASettingsDO> daList = daSettingsService.retrieveById(Long.parseLong(earningObject.get(CommonConstants.DEARNESSALLOWEANCEID).toString()));
					 				 		if(daList != null && daList.size() > 0){
									 			earingDO.setDearnessAllowance((daList.get(0)));
									 		}
								 		}
					 					earingDO.setPayroll(employeePayrollObject);
					 					earingDO.setUpdatedon(new Date());
					 					earingDO.setCreatedon(new Date());
								 		if(rowJSON.get(CommonConstants.UPDATED_BY).toString() != null && !rowJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
								 			earingDO.setUpdatedby(rowJSON.get(CommonConstants.UPDATED_BY).toString());
								 			earingDO.setCreatedby(rowJSON.get(CommonConstants.UPDATED_BY).toString());
								 		}
					 					
					 					earningDetailsList.add(earingDO);
						 			}
					 				payrollEarningsDetailsService.persist(earningDetailsList);
					 			}
					 			
					 			if(rowJSON.get(CommonConstants.DEDUCTIONLIST) != null && ! rowJSON.get(CommonConstants.DEDUCTIONLIST).toString().isEmpty()){
					 				org.json.simple.JSONArray deductionList = CommonWebUtil.getInputParamsArray(rowJSON.get(CommonConstants.DEDUCTIONLIST).toString());
					 				List<PayrollDeductionDetailsDO> deductionDetailsList = new ArrayList<PayrollDeductionDetailsDO>(); 
					 				for (int j=0; j < deductionList.size(); j++){
					 					JSONObject deductionObject = CommonWebUtil.getInputParams(deductionList.get(j).toString());
					 					PayrollDeductionDetailsDO deductionDO = new PayrollDeductionDetailsDO();
					 					if(deductionObject.get(CommonConstants.NAME) != null && !deductionObject.get(CommonConstants.NAME).toString().isEmpty()){
					 						deductionDO.setName(deductionObject.get(CommonConstants.NAME).toString());
					 					}
					 					if(deductionObject.get(CommonConstants.MONTHLY) != null && !deductionObject.get(CommonConstants.MONTHLY).toString().isEmpty()){
					 						deductionDO.setMonthly((Double.parseDouble(deductionObject.get(CommonConstants.MONTHLY).toString())));
					 					}
					 					if(deductionObject.get(CommonConstants.YTD) != null && !deductionObject.get(CommonConstants.YTD).toString().isEmpty()){
					 						deductionDO.setYtd((Double.parseDouble(deductionObject.get(CommonConstants.YTD).toString())));
					 					}
					 					if(payrollGroupList != null && payrollGroupList.size() >0){
					 						deductionDO.setPayrollGroup(payrollGroupList.get(0));
								 		}
					 					if(deductionObject.get(CommonConstants.DEDUCTIONID) != null && !deductionObject.get(CommonConstants.DEDUCTIONID).toString().isEmpty()){
					 						List<PayrollGroupDeductionDO> payrollGroupDeductionList = payrollGroupDeductionService.retrieveByID(Long.parseLong(deductionObject.get(CommonConstants.DEDUCTIONID).toString()));
									 		if(payrollGroupDeductionList != null && payrollGroupDeductionList.size() >0){
									 			deductionDO.setPayrollGroupDeduction(payrollGroupDeductionList.get(0));
									 		}
								 		}
					 					if(deductionObject.get(CommonConstants.DEARNESSALLOWEANCEID) != null && !deductionObject.get(CommonConstants.DEARNESSALLOWEANCEID).toString().isEmpty()){
					 						List<DASettingsDO> daList = daSettingsService.retrieveById(Long.parseLong(deductionObject.get(CommonConstants.DEARNESSALLOWEANCEID).toString()));
					 				 		if(daList != null && daList.size() > 0){
					 				 			deductionDO.setDearnessAllowance((daList.get(0)));
									 		}
								 		}
					 					
					 					deductionDO.setPayroll(employeePayrollObject);
					 					deductionDO.setUpdatedon(new Date());
					 					deductionDO.setCreatedon(new Date());
								 		if(rowJSON.get(CommonConstants.UPDATED_BY).toString() != null && !rowJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
								 			deductionDO.setUpdatedby(rowJSON.get(CommonConstants.UPDATED_BY).toString());
								 			deductionDO.setCreatedby(rowJSON.get(CommonConstants.UPDATED_BY).toString());
								 		}
					 					deductionDetailsList.add(deductionDO);
						 			}
					 				payrollDeductionDetailsService.persist(deductionDetailsList);
					 				
					 				List<AdvancePaymentDetailDO> advancepaymentList = advancePaymentDetailService.getByMonthAndEmpId(rowJSON.get(CommonConstants.EMPID).toString(),rowJSON.get(CommonConstants.PAYROLLMONTH).toString());
					 				if(advancepaymentList != null && advancepaymentList.size() > 0){
					 					for(AdvancePaymentDetailDO advancePayment:advancepaymentList){
					 						AdvancePaymentDetailDO paymentDetail = new AdvancePaymentDetailDO();
					 						paymentDetail = advancePayment;
					 						paymentDetail.setStatus(CommonConstants.SSTATUS);
					 						paymentDetail.setUpdatedon(new Date());
					 						advancePaymentDetailService.update(paymentDetail);
					 					}
					 				}
					 				
					 				List<GeneralDeductionDetailDO> loanpaymentList = loanPaymentDetailService.getByMonthAndEmpId(rowJSON.get(CommonConstants.EMPID).toString(),rowJSON.get(CommonConstants.PAYROLLMONTH).toString());
					 				if(loanpaymentList != null && loanpaymentList.size() > 0){
					 					for(GeneralDeductionDetailDO loanPayment:loanpaymentList){
					 						GeneralDeductionDetailDO paymentDetail = new GeneralDeductionDetailDO();
					 						paymentDetail = loanPayment;
					 						paymentDetail.setStatus(CommonConstants.SSTATUS);
					 						paymentDetail.setUpdatedon(new Date());
					 						loanPaymentDetailService.update(paymentDetail);
					 					}
					 				}
					 				
					 				List<InsurancePaymentDetailDO> insurancepaymentList = insurancePaymentDetailService.getByMonthAndEmpId(rowJSON.get(CommonConstants.EMPID).toString(),rowJSON.get(CommonConstants.PAYROLLMONTH).toString());
					 				if(insurancepaymentList != null && insurancepaymentList.size() > 0){
					 					for(InsurancePaymentDetailDO insurancepayment:insurancepaymentList){
					 						InsurancePaymentDetailDO paymentDetail = new InsurancePaymentDetailDO();
					 						paymentDetail = insurancepayment;
					 						paymentDetail.setStatus(CommonConstants.SSTATUS);
					 						paymentDetail.setUpdatedon(new Date());
					 						insurancePaymentDetailService.update(paymentDetail);
					 					}
					 				}
						 			
					 			}
					 			if(rowJSON.get(CommonConstants.TAXLIST) != null && ! rowJSON.get(CommonConstants.TAXLIST).toString().isEmpty()){
					 				org.json.simple.JSONArray taxList = CommonWebUtil.getInputParamsArray(rowJSON.get(CommonConstants.TAXLIST).toString());
					 				List<PayrollTaxDetailsDO> taxDetailsList = new ArrayList<PayrollTaxDetailsDO>(); 
					 				for (int j=0; j < taxList.size(); j++){
					 					JSONObject taxObject = CommonWebUtil.getInputParams(taxList.get(j).toString());
					 					PayrollTaxDetailsDO taxDO = new PayrollTaxDetailsDO();
					 					if(taxObject.get(CommonConstants.NAME) != null && !taxObject.get(CommonConstants.NAME).toString().isEmpty()){
					 						taxDO.setName(taxObject.get(CommonConstants.NAME).toString());
					 					}
					 					if(taxObject.get(CommonConstants.MONTHLY) != null && !taxObject.get(CommonConstants.MONTHLY).toString().isEmpty()){
					 						taxDO.setMonthly((Double.parseDouble(taxObject.get(CommonConstants.MONTHLY).toString())));
					 					}
					 					if(taxObject.get(CommonConstants.YTD) != null && !taxObject.get(CommonConstants.YTD).toString().isEmpty()){
					 						taxDO.setYtd((Double.parseDouble(taxObject.get(CommonConstants.YTD).toString())));
					 					}
					 					if(payrollGroupList != null && payrollGroupList.size() >0){
					 						taxDO.setPayrollGroup(payrollGroupList.get(0));
								 		}
					 					if(taxObject.get(CommonConstants.TAXID) != null && !taxObject.get(CommonConstants.TAXID).toString().isEmpty()){
					 						List<PayrollGroupTaxDO> payrollGroupTaxList = payrollGroupTaxService.retrieveByID(Long.parseLong(taxObject.get(CommonConstants.TAXID).toString()));
									 		if(payrollGroupTaxList != null && payrollGroupTaxList.size() >0){
									 			taxDO.setPayrollGroupTax(payrollGroupTaxList.get(0));
									 		}
								 		}
					 					taxDO.setPayroll(employeePayrollObject);
					 					taxDO.setUpdatedon(new Date());
					 					taxDO.setCreatedon(new Date());
								 		if(rowJSON.get(CommonConstants.UPDATED_BY).toString() != null && !rowJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
								 			taxDO.setUpdatedby(rowJSON.get(CommonConstants.UPDATED_BY).toString());
								 			taxDO.setCreatedby(rowJSON.get(CommonConstants.UPDATED_BY).toString());
								 		}
					 					
					 					taxDetailsList.add(taxDO);
						 			}
					 				payrollTaxDetailsService.persist(taxDetailsList);
					 			}
					 		}
			 			}else{
			 				employeePayrollDO = payrollList.get(0);
			 				if(employeePayrollDO.getStatus() == CommonConstants.NEW){
			 					employeePayrollDO.setStatus(CommonConstants.DRAFT);
						 		employeePayrollService.update(employeePayrollDO);
			 				}
			 				
			 			}
			 			
			 		}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retriveForSubmitApprovel", method = RequestMethod.POST)
	public @ResponseBody String retriveForSubmitApprovel(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
				if(inputJSON != null && inputJSON.get(CommonConstants.MONTHLY) != null && !inputJSON.get(CommonConstants.MONTHLY).toString().isEmpty() && inputJSON.get(CommonConstants.PAYROLLBATCHID) != null && !inputJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty()){
					List<EmployeePayrollDO> compensationList = employeePayrollService.retriveForSubmitApprovel(inputJSON.get(CommonConstants.MONTHLY).toString(), Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString()));
					respJSON = EmployeePayrollUtil.getPayrollDetail(compensationList).toString();
				}
				if(inputJSON != null && inputJSON.get(CommonConstants.PAYROLLWEEKTO) != null && !inputJSON.get(CommonConstants.PAYROLLWEEKTO).toString().isEmpty() && inputJSON.get(CommonConstants.PAYROLLWEEKFROM) != null && !inputJSON.get(CommonConstants.PAYROLLWEEKFROM).toString().isEmpty() && inputJSON.get(CommonConstants.PAYROLLBATCHID) != null &&!inputJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty()){
					SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
					String strFromDate= formatter.format(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.PAYROLLWEEKFROM).toString()));
					
					SimpleDateFormat formatter2 = new SimpleDateFormat("dd-MM-yyyy");
					String strToDate= formatter2.format(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.PAYROLLWEEKTO).toString()));
					
					List<EmployeePayrollDO> compensationList = employeePayrollService.retriveWeelyPayrollForSubmitApprovel(strFromDate,strToDate,Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString()));
					if(compensationList != null && compensationList.size() >0){
						respJSON = EmployeePayrollUtil.getPayrollDetail(compensationList).toString();
					}
				}
				if(inputJSON != null && inputJSON.get(CommonConstants.PAYROLLDATE) != null && !inputJSON.get(CommonConstants.PAYROLLDATE).toString().isEmpty()  && inputJSON.get(CommonConstants.PAYROLLBATCHID) != null &&!inputJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty()){
					
					SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
					String strDate= formatter.format(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.PAYROLLDATE).toString()));
					
					
					List<EmployeePayrollDO> compensationList = employeePayrollService.retriveDailyPayrollForSubmitApprovel(strDate,Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString()));
					if(compensationList != null && compensationList.size() >0){
						respJSON = EmployeePayrollUtil.getPayrollDetail(compensationList).toString();
					}
				}
				/*else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
				}*/
		}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retriveForProcessPayroll", method = RequestMethod.POST)
	public @ResponseBody String retriveForProcessPayroll(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
				if(inputJSON != null ){
					List<PayrollGroupDO> payrollGroupList = null;
		 			List<EmployeePayrollDO> payrollList = null;
		 			if(inputJSON.get(CommonConstants.PAYROLLBATCHID) != null && !inputJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty()){
		 				List<PayrollBatchDO> batchList = payrollBatchService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString()));
				 		if(batchList != null && batchList.size() > 0){
				 			if(batchList.get(0).getPayrollType().equalsIgnoreCase("Weekly")){
				 				SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
				 				String strFromDate= formatter.format(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.PAYROLLWEEKFROM).toString()));
				 				SimpleDateFormat formatter2 = new SimpleDateFormat("dd-MM-yyyy");
				 				String strToDate= formatter2.format(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.PAYROLLWEEKTO).toString()));
				 				List<EmployeePayrollDO> compensationList = employeePayrollService.retriveWeelyPayrollForProcess(strFromDate,strToDate,Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString()));
				 				respJSON = EmployeePayrollUtil.getPayrollDetail(compensationList).toString();
				 			}else if(batchList.get(0).getPayrollType().equalsIgnoreCase("Monthly")){
				 				List<EmployeePayrollDO> compensationList = employeePayrollService.retriveForProcessPayroll(inputJSON.get(CommonConstants.MONTHLY).toString(), Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString()));
								respJSON = EmployeePayrollUtil.getPayrollDetail(compensationList).toString();
				 			}else{
				 				if(batchList.get(0).getPayrollType().equalsIgnoreCase("Daily")){
				 					SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
					 				String strDate= formatter.format(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.PAYROLLDATE).toString()));
				 					List<EmployeePayrollDO> compensationList = employeePayrollService.retriveDailyPayrollForProcess(strDate, Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString()));
									respJSON = EmployeePayrollUtil.getPayrollDetail(compensationList).toString();
				 				}
				 			}
				 		}
		 			}
					
				
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	
	@RequestMapping(value = "/submitForApprove", method = RequestMethod.POST)
	public @ResponseBody String submitForApprove(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				org.json.simple.JSONArray inputJSON = CommonWebUtil.getInputParamsArray(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && inputJSON.size() > 0){
			 		for (int i=0; i < inputJSON.size(); i++){
			 			EmployeePayrollDO employeePayrollDO = new EmployeePayrollDO();
			 			JSONObject rowJSON = CommonWebUtil.getInputParams(inputJSON.get(i).toString());
			 			List<EmployeePayrollDO> payrollList = employeePayrollService.retrieveById(Long.parseLong(rowJSON.get(CommonConstants.ID).toString()));
			 			if(payrollList != null && payrollList.size() > 0){
			 				employeePayrollDO = payrollList.get(0);
			 				if(employeePayrollDO.getStatus() == CommonConstants.DRAFT){
			 					employeePayrollDO.setStatus(CommonConstants.WSTATE);
						 		employeePayrollService.update(employeePayrollDO);
			 				}
			 			}
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/approvePayroll", method = RequestMethod.POST)
	public @ResponseBody String approvePayroll(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
		 		if(!inputJSON.get(CommonConstants.IDS).toString().isEmpty()){
			 		JSONArray resultJSONArray = new JSONArray();
		 			resultJSONArray.put(inputJSON.get(CommonConstants.IDS));
			 		String rec = (String) resultJSONArray.get(0).toString();
			 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
			 		for (int i = 0; i < resultJSONArray1.size(); i++) {
			 			JSONObject rowJSON = (JSONObject) resultJSONArray1.get(i);
			 			EmployeePayrollDO employeePayrollDO = new EmployeePayrollDO();
			 			List<EmployeePayrollDO> payrollList = employeePayrollService.retrieveById(Long.parseLong(rowJSON.get(CommonConstants.ID).toString()));
			 			if(payrollList != null && payrollList.size() > 0){
			 				employeePayrollDO = payrollList.get(0);
			 				if(employeePayrollDO.getStatus() == CommonConstants.WSTATE){
			 					employeePayrollDO.setStatus(CommonConstants.ACTIVE);
						 		employeePayrollService.update(employeePayrollDO);
			 				}
			 			}
			 		}
			 		return CommonWebUtil.buildSuccessResponse().toString();
		 		}
				/*org.json.simple.JSONArray inputJSON = CommonWebUtil.getInputParamsArray(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && inputJSON.size() > 0){
			 		for (int i=0; i < inputJSON.size(); i++){
			 			EmployeePayrollDO employeePayrollDO = new EmployeePayrollDO();
			 			JSONObject rowJSON = CommonWebUtil.getInputParams(inputJSON.get(i).toString());
			 			List<EmployeePayrollDO> payrollList = employeePayrollService.retrieveById(Long.parseLong(rowJSON.get(CommonConstants.ID).toString()));
			 			if(payrollList != null && payrollList.size() > 0){
			 				employeePayrollDO = payrollList.get(0);
			 				if(employeePayrollDO.getStatus() == CommonConstants.WSTATE){
			 					employeePayrollDO.setStatus(CommonConstants.ACTIVE);
						 		employeePayrollService.update(employeePayrollDO);
			 				}
			 			}
			 		}
			 	}*/
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				org.json.simple.JSONArray inputJSON = CommonWebUtil.getInputParamsArray(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && inputJSON.size() > 0){
			 		for (int i=0; i < inputJSON.size(); i++){
			 			EmployeePayrollDO employeePayrollDO = new EmployeePayrollDO();
			 			JSONObject rowJSON = CommonWebUtil.getInputParams(inputJSON.get(i).toString());
			 			List<EmployeePayrollDO> payrollList = employeePayrollService.retrieveById(Long.parseLong(rowJSON.get(CommonConstants.ID).toString()));
			 			if(payrollList != null && payrollList.size() > 0){
			 				employeePayrollDO = payrollList.get(0);
			 				if(employeePayrollDO.getPayrollEarning() != null && employeePayrollDO.getPayrollEarning().size() > 0){
			 					for(PayrollEarningsDetailsDO earningList : employeePayrollDO.getPayrollEarning()){
			 						if(earningList.getBonus() != null && earningList.getBonus().getEmpBonusId() != null){
			 							List<EmployeeBonusDO> bonusList = employeeBonusService.retrieveById(Long.parseLong(earningList.getBonus().getEmpBonusId().toString()));
								 		if(bonusList != null && bonusList.size() >0){
								 			EmployeeBonusDO bonuDO = new EmployeeBonusDO();
								 			bonuDO = bonusList.get(0);
								 			bonuDO.setPaidOn(new Date());
								 			bonuDO.setStatus(CommonConstants.PSTATE);
								 			employeeBonusService.update(bonuDO);
								 		}
			 						}
			 					}
			 				}
			 				int year = (int) Long.parseLong(employeePayrollDO.getPayrollMonth().substring(employeePayrollDO.getPayrollMonth().length() - 4));
			 				String selectedMonth = employeePayrollDO.getPayrollMonth().substring(0,3);
			 				Date date = new SimpleDateFormat("MMM").parse(selectedMonth);
			 		        Calendar cal = Calendar.getInstance();
			 		        cal.setTime(date);
			 		        int  monthNumber=cal.get(Calendar.MONTH);
			 		        Calendar satrtDate = Calendar.getInstance();    
			 				satrtDate.set(year,monthNumber,1); 
			 				satrtDate.set(Calendar.DAY_OF_MONTH, satrtDate.getActualMinimum(Calendar.DAY_OF_MONTH));
			 				Calendar endDate = Calendar.getInstance(); 
			 				endDate.set(year,monthNumber,1); 
			 				endDate.set(Calendar.DAY_OF_MONTH, endDate.getActualMaximum(Calendar.DAY_OF_MONTH));
			 				List<EmployeeLopDO> lopList = employeeLopService.retrieveByEmpIdDateStatus(employeePayrollDO.getEmployee().getEmpId(), satrtDate.getTime(), endDate.getTime());
			 				if(lopList != null && lopList.size()>0){
			 					for(EmployeeLopDO lopDetails : lopList){
			 						EmployeeLopDO lopDO = new EmployeeLopDO();
				 					lopDO = lopDetails;
				 					lopDO.setProcessedOn(new Date());
				 					lopDO.setStatus(CommonConstants.PSTATE);
				 					employeeLopService.update(lopDO);
			 					}
			 				}
			 				List<AdvancePaymentDetailDO> advancepaymentList = advancePaymentDetailService.getByStatusAndEmpId(employeePayrollDO.getEmployee().getEmpId(),employeePayrollDO.getPayrollMonth());
			 				if(advancepaymentList != null && advancepaymentList.size() > 0){
			 					for(AdvancePaymentDetailDO advancePayment:advancepaymentList){
			 						AdvancePaymentDetailDO paymentDetail = new AdvancePaymentDetailDO();
			 						paymentDetail = advancePayment;
			 						paymentDetail.setStatus(CommonConstants.P_STATE);
			 						paymentDetail.setUpdatedon(new Date());
			 						advancePaymentDetailService.update(paymentDetail);
			 					}
			 				}
			 				
			 				List<GeneralDeductionDetailDO> loanpaymentList = loanPaymentDetailService.getByStatusAndEmpId(employeePayrollDO.getEmployee().getEmpId(),employeePayrollDO.getPayrollMonth());
			 				if(loanpaymentList != null && loanpaymentList.size() > 0){
			 					for(GeneralDeductionDetailDO loanPayment:loanpaymentList){
			 						GeneralDeductionDetailDO paymentDetail = new GeneralDeductionDetailDO();
			 						paymentDetail = loanPayment;
			 						paymentDetail.setStatus(CommonConstants.P_STATE);
			 						paymentDetail.setUpdatedon(new Date());
			 						loanPaymentDetailService.update(paymentDetail);
			 					}
			 				}
			 				
			 				List<InsurancePaymentDetailDO> insurancepaymentList = insurancePaymentDetailService.getByStatusAndEmpId(employeePayrollDO.getEmployee().getEmpId(),employeePayrollDO.getPayrollMonth());
			 				if(insurancepaymentList != null && insurancepaymentList.size() > 0){
			 					for(InsurancePaymentDetailDO insurancepaymen:insurancepaymentList){
			 						InsurancePaymentDetailDO paymentDetail = new InsurancePaymentDetailDO();
			 						paymentDetail = insurancepaymen;
			 						paymentDetail.setStatus(CommonConstants.P_STATE);
			 						paymentDetail.setUpdatedon(new Date());
			 						insurancePaymentDetailService.update(paymentDetail);
			 					}
			 				}
			 				List<GeneralEarningDO> generalpaymentList = generalEarningService.retrieveActiveByEmpIdAndMonth(employeePayrollDO.getEmployee().getEmpId(), employeePayrollDO.getPayrollMonth());
			 				if(generalpaymentList != null && generalpaymentList.size() > 0){
			 					for(GeneralEarningDO generalpayment:generalpaymentList){
			 						GeneralEarningDO generalPaymentDetail = new GeneralEarningDO();
			 						generalPaymentDetail = generalpayment;
			 						generalPaymentDetail.setStatus(CommonConstants.P_STATE);
			 						generalPaymentDetail.setUpdatedon(new Date());
			 						generalEarningService.update(generalPaymentDetail);
			 					}
			 				}
			 				List<EmployeeExpenseDO>  empExpenseList = employeeExpenseService.retrieveByMonthAndEmpId(employeePayrollDO.getEmployee().getEmpId(), satrtDate.getTime(), endDate.getTime(), CommonConstants.ACTIVESTRING);
			 				
			 				if(empExpenseList != null && empExpenseList.size() > 0){
			 					for(EmployeeExpenseDO expense:empExpenseList){
			 						EmployeeExpenseDO expenseDetail = new EmployeeExpenseDO();
			 						expenseDetail = expense;
			 						expenseDetail.setStatus(CommonConstants.P_STATE);
			 						expenseDetail.setUpdatedon(new Date());
			 						expenseDetail.setPaidon(new Date());
			 						employeeExpenseService.update(expenseDetail);
			 					}
			 				}
			 				
			 				
			 				
			 				/*List<AdvancePaymentDO> advancepaymentList = advancePaymentService.retrieveActiveByEmpId(employeePayrollDO.getEmployee().getEmpId());
			 				if(advancepaymentList != null && advancepaymentList.size() > 0){
			 					for(AdvancePaymentDO advancePayment : advancepaymentList){
				 					if(advancePayment.getMonth() != null){
				 						
				 						System.out.println("month1"+advancePayment.getMonth());
				 						System.out.println("month1"+employeePayrollDO.getPayrollMonth());
				 						LocalDate bday = LocalDate.of((int) Long.parseLong(advancePayment.getMonth().substring(advancePayment.getMonth().length() - 4)), CommonUtil.convertMonthToSqlDate(advancePayment.getMonth()).getMonth(), (int) Long.parseLong("01"));
				 				        LocalDate today = LocalDate.of((int) Long.parseLong(employeePayrollDO.getPayrollMonth().substring(employeePayrollDO.getPayrollMonth().length() - 4)), CommonUtil.convertMonthToSqlDate(employeePayrollDO.getPayrollMonth()).getMonth(), (int) Long.parseLong("01"));
				 					        
				 					     Period age = Period.between(bday, today);
				 					       // int years = age.getYears();
				 					        int months = age.getMonths();
				 					        if((months+1) >0 && (months+1) == Integer.parseInt(advancePayment.getNoOfInstallments().toString())){
				 					        	//installment += Double.parseDouble(advancePayment.getNoOfInstallments().toString());
				 					        	System.out
														.println("eql");
				 					        	
				 					        	AdvancePaymentDO endInstallment = advancePayment;
				 					        	endInstallment.setStatus(CommonConstants.INACTIVESTRING);
				 					        	advancePaymentService.update(endInstallment);
				 					        }
				 					       System.out.println("number of months: " + (months+1) );
				 					        System.out.println("oooo"+Integer.parseInt(advancePayment.getNoOfInstallments().toString()));
				 						
				 					}
			 					}
			 				}*/
			 				
			 				
			 				if(employeePayrollDO.getStatus() == CommonConstants.ACTIVE){
			 					employeePayrollDO.setStatus(CommonConstants.PSTATE);
						 		employeePayrollService.update(employeePayrollDO);
			 				}
			 			}
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveSummary", method = RequestMethod.GET)
	public @ResponseBody String retrieveSummary(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<Object[]> empCompensationList = employeePayrollService.retrieveForSummary();
				respJSON = EmployeePayrollUtil.ObjectArray(empCompensationList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveByAllEmpByBatchId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByAllEmpByBatchId(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		String errMsg = "";
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.PAYROLLMONTH).toString().isEmpty()){
					List<Object[]> empCompensationList = employeePayrollService.retrieveForSummaryByBatchId(inputJSON.get(CommonConstants.PAYROLLMONTH).toString());
				respJSON = EmployeePayrollUtil.ObjectArrayByBatchId(empCompensationList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}
		}catch (Exception e) {
			errMsg = e.getMessage();
			return CommonWebUtil.buildErrorResponse(e.getMessage()).toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse(errMsg).toString();
	}
	
	
	@RequestMapping(value = "/retrieveByAllEmp/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByAllEmp(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty()){
					
					if(inputJSON.get(CommonConstants.PAYROLLBATCHID) != null && !inputJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty()){
		 				List<PayrollBatchDO> batchList = payrollBatchService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString()));
				 		if(batchList != null && batchList.size() > 0){
				 			if(batchList.get(0).getPayrollType().equalsIgnoreCase("Weekly")){
				 				List<Object[]> empCompensationList = employeePayrollService.retrieveForWeeklySummaryByBatchId(inputJSON.get(CommonConstants.PAYROLLMONTH).toString());
								respJSON = EmployeePayrollUtil.ObjectArrayByBatchId1(empCompensationList).toString();
							
				 			}else{
				 				List<EmployeePayrollDO> compensationList = employeePayrollService.retrieveByAllEmp(inputJSON.get(CommonConstants.PAYROLLMONTH).toString(),
										Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString()), (char) (inputJSON != null ? inputJSON.get(CommonConstants.STATUS).toString().charAt(0) : ""));
									respJSON = EmployeePayrollUtil.getempPayrollList(compensationList).toString();
				 			}
				 			
				 		}
					}
				
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	
	
	@RequestMapping(value = "/retrieveByAllWeekEmp", method = RequestMethod.POST)
	public @ResponseBody String retrieveByAllWeekEmp(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
				if(inputJSON != null && !inputJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty()){
					
					if(inputJSON.get(CommonConstants.PAYROLLBATCHID) != null && !inputJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty()){
		 				List<PayrollBatchDO> batchList = payrollBatchService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString()));
				 		if(batchList != null && batchList.size() > 0){
				 			if(batchList.get(0).getPayrollType().equalsIgnoreCase("Weekly")){
				 				SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
				 				String strFromDate= formatter.format(CommonUtil.convertStringToDate(inputJSON.get(CommonConstants.PAYROLLWEEKFROM).toString()));
				 				SimpleDateFormat formatter2 = new SimpleDateFormat("dd-MM-yyyy");
				 				String strToDate= formatter2.format(CommonUtil.convertStringToDate(inputJSON.get(CommonConstants.PAYROLLWEEKTO).toString()));
				 				List<EmployeePayrollDO> payrollList = employeePayrollService.retriveWeelyPayrollForApprovel(strFromDate, strToDate,Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString()));
								respJSON = EmployeePayrollUtil.getempPayrollList(payrollList).toString();
				 			}
				 			
				 		}
					}
				
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	
	@RequestMapping(value = "/retriveByEmpId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveByEmpId(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
				List<EmployeePayrollDO> compensationList = employeePayrollService.retrieveByEmpIdAndStatus(inputJSON.get(CommonConstants.ID).toString());
				respJSON = EmployeePayrollUtil.getempPayrollList(compensationList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/decline", method = RequestMethod.POST)
	public @ResponseBody String decline(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
		 		if(!inputJSON.get(CommonConstants.IDS).toString().isEmpty()){
			 		JSONArray resultJSONArray = new JSONArray();
		 			resultJSONArray.put(inputJSON.get(CommonConstants.IDS));
			 		String rec = (String) resultJSONArray.get(0).toString();
			 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
			 		for (int i = 0; i < resultJSONArray1.size(); i++) {
			 			JSONObject inputJSON1 = (JSONObject) resultJSONArray1.get(i);
			 			if(payrollTaxDetailsService.deletePayrollById(Long.parseLong(inputJSON1.get(CommonConstants.ID).toString()))){
			 				if(payrollDeductionDetailsService.deletePayrollById(Long.parseLong(inputJSON1.get(CommonConstants.ID).toString()))){
			 					if(payrollEarningsDetailsService.deletePayrollById(Long.parseLong(inputJSON1.get(CommonConstants.ID).toString()))){
			 						if(employeePayrollService.deletePayrollById(Long.parseLong(inputJSON1.get(CommonConstants.ID).toString()))){
			 						}else{
						 				return CommonWebUtil.buildErrorResponse("").toString();
						 			}
					 			}else{
					 				return CommonWebUtil.buildErrorResponse("").toString();
					 			}
				 			}else{
				 				return CommonWebUtil.buildErrorResponse("").toString();
				 			}
			 			}else{
			 				return CommonWebUtil.buildErrorResponse("").toString();
			 			}
			 		}
			 		return CommonWebUtil.buildSuccessResponse().toString();
		 		}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retrivePayrollHeadings", method = RequestMethod.GET)
	public @ResponseBody String retrivePayrollHeadings(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List <AllowanceSettingDO> allwanceList = allowanceSettingsService.retrieve();
				List <DeductionSettingDO> deductionList = deductionSettingsService.retrieve();
				List <TaxSettingsDO> taxList = taxSettingsService.retrieve();
				List <WorkingDayAllowanceDO> workingDayList = workingDayAllowanceService.retrieve();
				List <AttendanceAllowanceDO> attendeanceList = attendanceAllowanceService.retrieve();
				
				respJSON = EmployeePayrollUtil.getPayrollHeadingList(allwanceList,deductionList,taxList,workingDayList,attendeanceList).toString();
				
				/*JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
				List<EmployeePayrollDO> compensationList = employeePayrollService.retrieveByEmpIdAndStatus(inputJSON.get(CommonConstants.ID).toString());
				respJSON = EmployeePayrollUtil.getempPayrollList(compensationList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}*/
		}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	
	@RequestMapping(value = "/retrieveTdsByMonth/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveTdsByMonth(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		String errMsg = "";
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				List<EmployeePayrollDO> compensationList = null;
				if(inputJSON != null &&  inputJSON.get(CommonConstants.PAYROLLMONTH) != null && !inputJSON.get(CommonConstants.PAYROLLMONTH).toString().isEmpty()){
					if(inputJSON.get(CommonConstants.PAYROLLMONTH) != null && !inputJSON.get(CommonConstants.PAYROLLMONTH).toString().isEmpty()){
						if(inputJSON.get(CommonConstants.EMPID) != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
							compensationList = employeePayrollService.retrieveByPayrollMonthByEmpID(inputJSON.get(CommonConstants.PAYROLLMONTH).toString(),CommonConstants.PSTATE,inputJSON.get(CommonConstants.EMPID).toString());
						}else{
							if(inputJSON.get(CommonConstants.PAYROLLMONTH) != null && !inputJSON.get(CommonConstants.PAYROLLMONTH).toString().isEmpty()){
								compensationList = employeePayrollService.retrieveByPayrollMonth(inputJSON.get(CommonConstants.PAYROLLMONTH).toString(),CommonConstants.PSTATE);
							}
						}
					}else{
						if(inputJSON.get(CommonConstants.EMPID) != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
							compensationList = employeePayrollService.retrieveByEmpIdAndStatus(inputJSON.get(CommonConstants.EMPID).toString());
						}
					}
					respJSON = EmployeePayrollUtil.getPayrollMonthDetails(compensationList).toString();
			}else{
				if(inputJSON != null && inputJSON.get(CommonConstants.EMPID) != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
						compensationList = employeePayrollService.retrieveByEmpIdAndStatus(inputJSON.get(CommonConstants.EMPID).toString());
						respJSON = EmployeePayrollUtil.getPayrollMonthDetails(compensationList).toString();
				}else{
					return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
				}
				
			}
		}
		}catch (Exception e) {
			e.printStackTrace();
			errMsg = e.getMessage();
			return CommonWebUtil.buildErrorResponse(e.getMessage()).toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse(errMsg).toString();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
/*	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		if(!inputJSON.get(CommonConstants.PAYROLLLIST).toString().isEmpty()){
				 		JSONArray resultJSONArray = new JSONArray();
			 			resultJSONArray.put(inputJSON.get(CommonConstants.PAYROLLLIST));
				 		String rec = (String) resultJSONArray.get(0).toString();
				 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
				 		for (int i = 0; i < resultJSONArray1.size(); i++) {
				 			JSONObject inputJSON1 = (JSONObject) resultJSONArray1.get(i);
				 			EmployeePayrollDO employeePayrollDO = new EmployeePayrollDO();
				 			List<EmployeePayrollDO> payrollList = employeePayrollService.retriveByMonthAndEmp(inputJSON.get(CommonConstants.PAYROLLMONTH).toString(), Long.parseLong(inputJSON1.get(CommonConstants.EMPID).toString()));
				 			if(payrollList == null || payrollList.size() <= 0){
				 			employeePayrollDO.setPayrollbatchId(!inputJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString()) : null);
			 				employeePayrollDO.setPayrollMonth(!inputJSON.get(CommonConstants.PAYROLLMONTH).toString().isEmpty() ? inputJSON.get(CommonConstants.PAYROLLMONTH).toString() : null);
			 				employeePayrollDO.setPayrollType(!inputJSON.get(CommonConstants.PAYROLLTYPE).toString().isEmpty() ? inputJSON.get(CommonConstants.PAYROLLTYPE).toString() : null);
				 			employeePayrollDO.setEmpId(!inputJSON1.get(CommonConstants.EMPID).toString().isEmpty() ? Long.parseLong(inputJSON1.get(CommonConstants.EMPID).toString()) : null);
				 			employeePayrollDO.setEmpName(!inputJSON1.get(CommonConstants.EMPNAME).toString().isEmpty() ? inputJSON1.get(CommonConstants.EMPNAME).toString() : null);
				 			employeePayrollDO.setNetmonth(!inputJSON1.get(CommonConstants.NETMONTHLY).toString().isEmpty() ? Double.parseDouble(inputJSON1.get(CommonConstants.NETMONTHLY).toString()) : null);
				 			if(!inputJSON1.get(CommonConstants.TAXMONTHLY).toString().isEmpty() && !inputJSON1.get(CommonConstants.TAXMONTHLY).toString().equalsIgnoreCase("null")){
				 				employeePayrollDO.setTaxMonth(Double.parseDouble(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(inputJSON1.get(CommonConstants.TAXMONTHLY).toString()))));
				 			}
				 			employeePayrollDO.setEarningmonth(!inputJSON1.get(CommonConstants.EARNINGMONTHLY).toString().isEmpty() ? Double.parseDouble(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(inputJSON1.get(CommonConstants.EARNINGMONTHLY).toString()))) : null);
				 			if(!inputJSON1.get(CommonConstants.DEDUCTIONMONTHLY).toString().isEmpty()){
				 				employeePayrollDO.setDeductionmonth(Double.parseDouble(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(inputJSON1.get(CommonConstants.DEDUCTIONMONTHLY).toString()))));
				 			}
				 			
				 			
				 			
					 		employeePayrollDO.setGrossPay(!inputJSON1.get(CommonConstants.BASICGROSS_PAY).toString().isEmpty() ? Double.parseDouble(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(inputJSON1.get(CommonConstants.BASICGROSS_PAY).toString()))) : null);
					 		employeePayrollDO.setUpdatedon(new Date());
					 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			employeePayrollDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			employeePayrollDO.setCreatedby(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
					 		}
					 		employeePayrollDO.setStatus('d');
					 		employeePayrollService.persist(employeePayrollDO);
					 		List<EmployeeLopDO> employeeLopList = employeeLopService.retrieveByEmpId(employeePayrollDO.getEmpId().toString());
				 			int noOfDaysLop = 0;
				 			Double lossOfPayAmount = 0D;
				 			if(employeePayrollDO.getPayrollMonth() != null){
				 				if(employeeLopList != null && employeeLopList.size() > 0){
				 					Calendar start = Calendar.getInstance();
				 					start.setTime(employeeLopList.get(0).getStartdate());
				 					Calendar end = Calendar.getInstance();
				 					end.setTime(employeeLopList.get(0).getEnddate());
				 					
				 					for (Date date = start.getTime(); start.before(end) || start.equals(end); start.add(Calendar.DATE, 1), date = start.getTime()) {
				 						Calendar weekend = Calendar.getInstance();
				 						weekend.setTime(date);
				 						if (weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && 
				 							weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){
				 							if(employeePayrollDO.getPayrollMonth().equalsIgnoreCase(CommonUtil.convertDateToStringWithOutDate(weekend.getTime()))){
				 								noOfDaysLop++;
				 							}
				 						}
				 					}
				 				}
				 			}
				 			if(employeePayrollDO.getPayrollMonth() != null){
				 				if(employeePayrollDO.getGrossPay() != null){
				 					Double ctc = employeePayrollDO.getGrossPay();
				 					Date s = CommonUtil.convertDateToStringWithOutDate(employeePayrollDO.getPayrollMonth());
				 			        Calendar c=Calendar.getInstance();
				 			        c.setTime(s);
				 			        int days = c.getActualMaximum(Calendar.DAY_OF_MONTH);
				 					Long perDay = (long) (ctc/days);
				 					if(noOfDaysLop > 0){
				 						lossOfPayAmount = (double) (noOfDaysLop * perDay);
				 					}
				 				}
				 			}
					 		if(lossOfPayAmount > 0){
					 			PayrollLopDetailsDO employeeLopDO = new PayrollLopDetailsDO();
						 		employeeLopDO.setEmpId(employeePayrollDO.getEmpId());
						 		employeeLopDO.setMonthly(lossOfPayAmount);
						 		employeeLopDO.setPayrollMonth(employeePayrollDO.getPayrollMonth()); 
						 		payrollLopDetailsService.persist(employeeLopDO);
					 		}
					 		
				 			if(!inputJSON1.get(CommonConstants.ID).toString().isEmpty()){
					 			List<PayrollEarningsDetailsDO> earningDetailsList = new ArrayList<PayrollEarningsDetailsDO>(); 
					 			List<EmployeeEarningsDO> earningsList = employeeEarningsService.retrieveByEmpCompensationId(Long.parseLong(inputJSON1.get(CommonConstants.ID).toString()));
					 			for (EmployeeEarningsDO employeeEarningsDO : earningsList) {
					 				PayrollEarningsDetailsDO earningDetails = new PayrollEarningsDetailsDO(); 
					 				earningDetails.setPayrollid(employeePayrollDO.getId());
					 				earningDetails.setEarningsId(employeeEarningsDO.getEarningsId());
					 				earningDetails.setMonthly(Double.parseDouble(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(employeeEarningsDO.getMonthly()))));
					 				earningDetails.setYtd(Double.parseDouble(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(employeeEarningsDO.getYtd()))));
					 				earningDetails.setUpdatedon(new Date());
						 			earningDetailsList.add(earningDetails);
								}
						 		payrollEarningsDetailsService.persist(earningDetailsList);
					 		}
				 			
				 			if(!inputJSON1.get(CommonConstants.ID).toString().isEmpty()){
					 			List<PayrollDeductionDetailsDO> deductionDetailsList = new ArrayList<PayrollDeductionDetailsDO>(); 
								List<EmployeeDeductionDO> deductionList = employeeDeductionService.retrieveByEmpCompensationId(Long.parseLong(inputJSON1.get(CommonConstants.ID).toString()));
					 			for (EmployeeDeductionDO employeeDeductionDO : deductionList) {
					 				PayrollDeductionDetailsDO deductionDetails = new PayrollDeductionDetailsDO();
					 				deductionDetails.setPayrollid(employeePayrollDO.getId());
						 			deductionDetails.setDeductionId(employeeDeductionDO.getEmpdeductionid());
						 			deductionDetails.setMonthly(Double.parseDouble(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(employeeDeductionDO.getMonthly()))));
						 			deductionDetails.setYtd(Double.parseDouble(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(employeeDeductionDO.getYtd()))));
						 			deductionDetails.setUpdatedon(new Date());
						 			deductionDetailsList.add(deductionDetails);
								}
						 		payrollDeductionDetailsService.persist(deductionDetailsList);
				 			}
				 			
				 			if(!inputJSON1.get(CommonConstants.ID).toString().isEmpty()){
					 			List<PayrollTaxDetailsDO> payDetailsList = new ArrayList<PayrollTaxDetailsDO>(); 
								List<EmployeeTaxDO> taxList = employeeTaxService.retrieveByEmpCompensationId(Long.parseLong(inputJSON1.get(CommonConstants.ID).toString()));
					 			for (EmployeeTaxDO employeeDeductionDO : taxList) {
					 				PayrollTaxDetailsDO deductionDetails = new PayrollTaxDetailsDO();
					 				deductionDetails.setPayrollid(employeePayrollDO.getId());
						 			deductionDetails.setTaxId(employeeDeductionDO.getEmptaxid());
						 			deductionDetails.setMonthly(Double.parseDouble(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(employeeDeductionDO.getMonthly()))));
						 			deductionDetails.setYtd(Double.parseDouble(CommonUtil.convertDoubleValueWithTwoAbsDecimalPoints(Double.valueOf(employeeDeductionDO.getYtd()))));
						 			deductionDetails.setUpdatedon(new Date());
						 			payDetailsList.add(deductionDetails);
								}
						 		payrollTaxDetailsService.persist(payDetailsList);
				 			}
			 			}else{
			 				employeePayrollDO = payrollList.get(0);
			 				employeePayrollDO.setStatus('d');
					 		employeePayrollService.update(employeePayrollDO);
			 			}
			 		}
			 		CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Payroll Ran");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}*/
	
	
	/*@RequestMapping(value = "/retriveByEmpId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveByEmpId(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
				List<EmployeePayrollDO> compensationList = employeePayrollService.retrieveByEmpId(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
				respJSON = EmployeePayrollUtil.getempPayrollList(compensationList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}*/
/*	
	@RequestMapping(value = "/retriveById/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveById(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
				List<EmployeePayrollDO> compensationList = employeePayrollService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
				respJSON = EmployeePayrollUtil.getempPayrollList(compensationList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	*/
	/*@RequestMapping(value = "/retrieveByAllEmp/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByAllEmp(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.PAYROLLMONTH).toString().isEmpty()){
					
				List<EmployeePayrollDO> compensationList = employeePayrollService.retrieveByAllEmp(inputJSON.get(CommonConstants.PAYROLLMONTH).toString(),
						Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString()), (char) (inputJSON != null ? inputJSON.get(CommonConstants.STATUS).toString().charAt(0) : ""));
				respJSON = EmployeePayrollUtil.getempPayrollList(compensationList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	*/
	/*@RequestMapping(value = "/retrieveByAllEmpByBatchId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByAllEmpByBatchId(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.PAYROLLMONTH).toString().isEmpty()){
					
					List<Object[]> empCompensationList = employeePayrollService.retrieveForSummaryByBatchId(inputJSON.get(CommonConstants.PAYROLLMONTH).toString());, (char) (inputJSON != null ? inputJSON.get(CommonConstants.STATUS).toString().charAt(0) : ""));
				respJSON = EmployeePayrollUtil.ObjectArrayByBatchId(empCompensationList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}*/
	
	/*@RequestMapping(value = "/retriveByMonth/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveByMonth(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.MONTHLY).toString().isEmpty() && !inputJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty()){
				List<EmployeePayrollDO> compensationList = employeePayrollService.retriveByMonth(inputJSON.get(CommonConstants.MONTHLY).toString(), Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString()));
				respJSON = EmployeePayrollUtil.getempPayrollList(compensationList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}*/
	
	/*@RequestMapping(value = "/retriveByStatus/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveByStatus(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.MONTHLY).toString().isEmpty()&& !inputJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty()){
				List<EmployeePayrollDO> compensationList = employeePayrollService.retriveByStatus(inputJSON.get(CommonConstants.MONTHLY).toString(),  Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString()));
				respJSON = EmployeePayrollUtil.getempPayrollList(compensationList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}*/
	
	/*@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<EmployeePayrollDO> empCompensationList = employeePayrollService.retrieve();
				respJSON = EmployeePayrollUtil.getempPayrollList(empCompensationList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveSummary", method = RequestMethod.GET)
	public @ResponseBody String retrieveSummary(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<Object[]> empCompensationList = employeePayrollService.retrieveForSummary();
				respJSON = EmployeePayrollUtil.ObjectArray(empCompensationList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
		 		if(!inputJSON.get(CommonConstants.IDS).toString().isEmpty()){
			 		JSONArray resultJSONArray = new JSONArray();
		 			resultJSONArray.put(inputJSON.get(CommonConstants.IDS));
			 		String rec = (String) resultJSONArray.get(0).toString();
			 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
			 		for (int i = 0; i < resultJSONArray1.size(); i++) {
			 			JSONObject inputJSON1 = (JSONObject) resultJSONArray1.get(i);
			 			List<EmployeePayrollDO> empPayrollList = employeePayrollService.retrieveById(Long.parseLong(inputJSON1.get(CommonConstants.ID).toString()));
			 			EmployeePayrollDO employeePayrollDO = new EmployeePayrollDO();
			 			if(empPayrollList != null && empPayrollList.size() > 0){
			 				employeePayrollDO = empPayrollList.get(0);
			 				employeePayrollDO.setStatus('p');
			 				employeePayrollDO.setUpdatedon(new Date());
					 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			employeePayrollDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			employeePayrollDO.setCreatedby(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
					 		}
			 				employeePayrollService.update(employeePayrollDO);
			 			}
			 		}
		 		}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/decline", method = RequestMethod.POST)
	public @ResponseBody String decline(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
		 		if(!inputJSON.get(CommonConstants.IDS).toString().isEmpty()){
			 		JSONArray resultJSONArray = new JSONArray();
		 			resultJSONArray.put(inputJSON.get(CommonConstants.IDS));
			 		String rec = (String) resultJSONArray.get(0).toString();
			 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
			 		for (int i = 0; i < resultJSONArray1.size(); i++) {
			 			JSONObject inputJSON1 = (JSONObject) resultJSONArray1.get(i);
			 			List<EmployeePayrollDO> empPayrollList = employeePayrollService.retrieveById(Long.parseLong(inputJSON1.get(CommonConstants.ID).toString()));
			 			EmployeePayrollDO employeePayrollDO = new EmployeePayrollDO();
			 			if(empPayrollList != null && empPayrollList.size() > 0){
			 				employeePayrollDO = empPayrollList.get(0);
			 				employeePayrollDO.setStatus('v');
			 				employeePayrollDO.setUpdatedon(new Date());
					 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			employeePayrollDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			employeePayrollDO.setCreatedby(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
					 		}
			 				employeePayrollService.update(employeePayrollDO);
			 			}
			 		}
		 		}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retriveForReport/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveForReportMonth(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.MONTHLY).toString().isEmpty()){
					Long batchId = null;
					if(!inputJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty()){
						batchId = Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString());
						if(batchId == 0){
							batchId = null;
						}
					}
					List<EmployeePayrollDO> compensationList = employeePayrollService.retriveForReportMonth(inputJSON.get(CommonConstants.MONTHLY).toString(),
						batchId);
					if(compensationList != null && compensationList.size() > 0){
						respJSON = EmployeePayrollUtil.SingleMonthDeductionReport(compensationList, CommonConstants.MONTHLYC, inputJSON.get(CommonConstants.MONTHLY).toString()).toString();
					}
				}else{
					if(inputJSON != null && !inputJSON.get(CommonConstants.QUARTERLY).toString().isEmpty()){
						String fromMonth = null;
						String toMonth = null;
						Long batchId = null;
						List<FyDO> fyList = new ArrayList<FyDO>();
						if(!inputJSON.get(CommonConstants.FYID).toString().isEmpty()){
							fyList = fyService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.FYID).toString()));
							if(fyList != null && fyList.size() > 0){
								if(!inputJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty()){
									batchId = Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString());
									if(batchId == 0){
										batchId = null;
									}
								}
								if(inputJSON.get(CommonConstants.QUARTERLY).toString().equalsIgnoreCase("Q1")){
									fromMonth = "Apr"+" "+fyList.get(0).getFromyear();
									toMonth = "Jun"+" "+fyList.get(0).getFromyear(); 
									List<EmployeePayrollDO> compensationList = employeePayrollService.retriveForReportQuarter(fromMonth, toMonth,	batchId);
									if(compensationList != null && compensationList.size() > 0){
										respJSON = EmployeePayrollUtil.MultipleMonthDeductionReport(compensationList, fromMonth, toMonth,CommonConstants.QUARTERLYC).toString();
									}
								}
								
								if(inputJSON.get(CommonConstants.QUARTERLY).toString().equalsIgnoreCase("Q2")){
									fromMonth = "Jul"+" "+fyList.get(0).getFromyear();
									toMonth = "Sep"+" "+fyList.get(0).getFromyear(); 
									List<EmployeePayrollDO> compensationList = employeePayrollService.retriveForReportQuarter(fromMonth, toMonth,	batchId);
									if(compensationList != null && compensationList.size() > 0){
										respJSON = EmployeePayrollUtil.MultipleMonthDeductionReport(compensationList, fromMonth, toMonth,CommonConstants.QUARTERLYC).toString();
									}
								}
								
								
								if(inputJSON.get(CommonConstants.QUARTERLY).toString().equalsIgnoreCase("Q3")){
									fromMonth = "Oct"+" "+fyList.get(0).getFromyear();
									toMonth = "Dec"+" "+fyList.get(0).getFromyear(); 
									List<EmployeePayrollDO> compensationList = employeePayrollService.retriveForReportQuarter(fromMonth, toMonth,	batchId);
									if(compensationList != null && compensationList.size() > 0){
										respJSON = EmployeePayrollUtil.MultipleMonthDeductionReport(compensationList, fromMonth, toMonth,CommonConstants.QUARTERLYC).toString();
									}
								}
								if(inputJSON.get(CommonConstants.QUARTERLY).toString().equalsIgnoreCase("Q4")){
									fromMonth = "Jan"+" "+fyList.get(0).getToyear();
									toMonth = "Mar"+" "+fyList.get(0).getToyear(); 
									List<EmployeePayrollDO> compensationList = employeePayrollService.retriveForReportQuarter(fromMonth, toMonth,	batchId);
									if(compensationList != null && compensationList.size() > 0){
										respJSON = EmployeePayrollUtil.MultipleMonthDeductionReport(compensationList, fromMonth, toMonth,CommonConstants.QUARTERLYC).toString();
									}
								}
							}
							
						}
					}else{
						if(inputJSON != null && !inputJSON.get(CommonConstants.FREQUENCY).toString().isEmpty()
								&& inputJSON.get(CommonConstants.FREQUENCY).toString().equalsIgnoreCase("Y")){
							String fromMonth = null;
							String toMonth = null;
							Long batchId = null;
							List<FyDO> fyList = new ArrayList<FyDO>();
							if(!inputJSON.get(CommonConstants.FYID).toString().isEmpty()){
								fyList = fyService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.FYID).toString()));
								if(fyList != null && fyList.size() > 0){
									if(!inputJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty()){
										batchId = Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString());
										if(batchId == 0){
											batchId = null;
										}
									}
									fromMonth = "Apr"+" "+fyList.get(0).getFromyear();
									toMonth = "Mar"+" "+fyList.get(0).getToyear(); 
									List<EmployeePayrollDO> compensationList = employeePayrollService.retriveForReportQuarter(fromMonth, toMonth,	batchId);
									if(compensationList != null && compensationList.size() > 0){
										respJSON = EmployeePayrollUtil.MultipleMonthDeductionReport(compensationList, fromMonth, toMonth,CommonConstants.YEARLYC).toString();
									}
								}
							}
						}
					}
				}
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retriveForTaxReport/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveForTaxReport(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.MONTHLY).toString().isEmpty()){
					Long batchId = null;
					if(!inputJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty()){
						batchId = Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString());
						if(batchId == 0){
							batchId = null;
						}
					}
					List<EmployeePayrollDO> compensationList = employeePayrollService.retriveForReportMonth(inputJSON.get(CommonConstants.MONTHLY).toString(),
						batchId);
					if(compensationList != null && compensationList.size() > 0){
						respJSON = EmployeePayrollUtil.SingleMonthTaxReport(compensationList, CommonConstants.MONTHLYC, inputJSON.get(CommonConstants.MONTHLY).toString()).toString();
					}
				}else{
					if(inputJSON != null && !inputJSON.get(CommonConstants.QUARTERLY).toString().isEmpty()){
						String fromMonth = null;
						String toMonth = null;
						Long batchId = null;
						List<FyDO> fyList = new ArrayList<FyDO>();
						if(!inputJSON.get(CommonConstants.FYID).toString().isEmpty()){
							fyList = fyService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.FYID).toString()));
							if(fyList != null && fyList.size() > 0){
								if(!inputJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty()){
									batchId = Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString());
									if(batchId == 0){
										batchId = null;
									}
								}
								if(inputJSON.get(CommonConstants.QUARTERLY).toString().equalsIgnoreCase("Q1")){
									fromMonth = "Apr"+" "+fyList.get(0).getFromyear();
									toMonth = "Jun"+" "+fyList.get(0).getFromyear(); 
									List<EmployeePayrollDO> compensationList = employeePayrollService.retriveForReportQuarter(fromMonth, toMonth,	batchId);
									if(compensationList != null && compensationList.size() > 0){
										respJSON = EmployeePayrollUtil.MultipleMonthTaxReport(compensationList, fromMonth, toMonth,CommonConstants.QUARTERLYC).toString();
									}
								}
								
								if(inputJSON.get(CommonConstants.QUARTERLY).toString().equalsIgnoreCase("Q2")){
									fromMonth = "Jul"+" "+fyList.get(0).getFromyear();
									toMonth = "Sep"+" "+fyList.get(0).getFromyear(); 
									List<EmployeePayrollDO> compensationList = employeePayrollService.retriveForReportQuarter(fromMonth, toMonth,	batchId);
									if(compensationList != null && compensationList.size() > 0){
										respJSON = EmployeePayrollUtil.MultipleMonthTaxReport(compensationList, fromMonth, toMonth,CommonConstants.QUARTERLYC).toString();
									}
								}
								
								
								if(inputJSON.get(CommonConstants.QUARTERLY).toString().equalsIgnoreCase("Q3")){
									fromMonth = "Oct"+" "+fyList.get(0).getFromyear();
									toMonth = "Dec"+" "+fyList.get(0).getFromyear(); 
									List<EmployeePayrollDO> compensationList = employeePayrollService.retriveForReportQuarter(fromMonth, toMonth,	batchId);
									if(compensationList != null && compensationList.size() > 0){
										respJSON = EmployeePayrollUtil.MultipleMonthTaxReport(compensationList, fromMonth, toMonth,CommonConstants.QUARTERLYC).toString();
									}
								}
								if(inputJSON.get(CommonConstants.QUARTERLY).toString().equalsIgnoreCase("Q4")){
									fromMonth = "Jan"+" "+fyList.get(0).getToyear();
									toMonth = "Mar"+" "+fyList.get(0).getToyear(); 
									List<EmployeePayrollDO> compensationList = employeePayrollService.retriveForReportQuarter(fromMonth, toMonth,	batchId);
									if(compensationList != null && compensationList.size() > 0){
										respJSON = EmployeePayrollUtil.MultipleMonthTaxReport(compensationList, fromMonth, toMonth,CommonConstants.QUARTERLYC).toString();
									}
								}
							}
							
						}
					}else{
						if(inputJSON != null && !inputJSON.get(CommonConstants.FREQUENCY).toString().isEmpty()
								&& inputJSON.get(CommonConstants.FREQUENCY).toString().equalsIgnoreCase("Y")){
							String fromMonth = null;
							String toMonth = null;
							Long batchId = null;
							List<FyDO> fyList = new ArrayList<FyDO>();
							if(!inputJSON.get(CommonConstants.FYID).toString().isEmpty()){
								fyList = fyService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.FYID).toString()));
								if(fyList != null && fyList.size() > 0){
									if(!inputJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty()){
										batchId = Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString());
										if(batchId == 0){
											batchId = null;
										}
									}
									fromMonth = "Apr"+" "+fyList.get(0).getFromyear();
									toMonth = "Mar"+" "+fyList.get(0).getToyear(); 
									List<EmployeePayrollDO> compensationList = employeePayrollService.retriveForReportQuarter(fromMonth, toMonth,	batchId);
									if(compensationList != null && compensationList.size() > 0){
										respJSON = EmployeePayrollUtil.MultipleMonthTaxReport(compensationList, fromMonth, toMonth,CommonConstants.YEARLYC).toString();
									}
								}
							}
						}
					}
				}
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retriveForYtdReport/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveForYtdReport(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.MONTHLY).toString().isEmpty()){
					Long batchId = null;
					if(!inputJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty()){
						batchId = Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString());
						if(batchId == 0){
							batchId = null;
						}
					}
					List<EmployeePayrollDO> compensationList = employeePayrollService.retriveForReportMonth(inputJSON.get(CommonConstants.MONTHLY).toString(),
						batchId);
					if(compensationList != null && compensationList.size() > 0){
						respJSON = EmployeePayrollUtil.YtdReport(compensationList, inputJSON.get(CommonConstants.MONTHLY).toString(), CommonConstants.MONTHLYC).toString();
					}
				}else{
					if(inputJSON != null && !inputJSON.get(CommonConstants.QUARTERLY).toString().isEmpty()){
						String fromMonth = null;
						String toMonth = null;
						Long batchId = null;
						List<FyDO> fyList = new ArrayList<FyDO>();
						if(!inputJSON.get(CommonConstants.FYID).toString().isEmpty()){
							fyList = fyService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.FYID).toString()));
							if(fyList != null && fyList.size() > 0){
								if(!inputJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty()){
									batchId = Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString());
									if(batchId == 0){
										batchId = null;
									}
								}
								if(inputJSON.get(CommonConstants.QUARTERLY).toString().equalsIgnoreCase("Q1")){
									fromMonth = "Apr"+" "+fyList.get(0).getFromyear();
									toMonth = "Jun"+" "+fyList.get(0).getFromyear(); 
									List<EmployeePayrollDO> compensationList = employeePayrollService.retriveForReportQuarter(fromMonth, toMonth,	batchId);
									if(compensationList != null && compensationList.size() > 0){
										respJSON = EmployeePayrollUtil.YtdForMultipleMonth(compensationList, fromMonth, toMonth, CommonConstants.QUARTERLYC).toString();
									}
								}
								
								if(inputJSON.get(CommonConstants.QUARTERLY).toString().equalsIgnoreCase("Q2")){
									fromMonth = "Jul"+" "+fyList.get(0).getFromyear();
									toMonth = "Sep"+" "+fyList.get(0).getFromyear(); 
									List<EmployeePayrollDO> compensationList = employeePayrollService.retriveForReportQuarter(fromMonth, toMonth,	batchId);
									if(compensationList != null && compensationList.size() > 0){
										respJSON = EmployeePayrollUtil.YtdForMultipleMonth(compensationList, fromMonth, toMonth, CommonConstants.QUARTERLYC).toString();
									}
								}
								
								
								if(inputJSON.get(CommonConstants.QUARTERLY).toString().equalsIgnoreCase("Q3")){
									fromMonth = "Oct"+" "+fyList.get(0).getFromyear();
									toMonth = "Dec"+" "+fyList.get(0).getFromyear(); 
									List<EmployeePayrollDO> compensationList = employeePayrollService.retriveForReportQuarter(fromMonth, toMonth,	batchId);
									if(compensationList != null && compensationList.size() > 0){
										respJSON = EmployeePayrollUtil.YtdForMultipleMonth(compensationList, fromMonth, toMonth, CommonConstants.QUARTERLYC).toString();
									}
								}
								if(inputJSON.get(CommonConstants.QUARTERLY).toString().equalsIgnoreCase("Q4")){
									fromMonth = "Jan"+" "+fyList.get(0).getToyear();
									toMonth = "Mar"+" "+fyList.get(0).getToyear(); 
									List<EmployeePayrollDO> compensationList = employeePayrollService.retriveForReportQuarter(fromMonth, toMonth,	batchId);
									if(compensationList != null && compensationList.size() > 0){
										respJSON = EmployeePayrollUtil.YtdForMultipleMonth(compensationList, fromMonth, toMonth, CommonConstants.QUARTERLYC).toString();
									}
								}
							}
							
						}
					}else{
						if(inputJSON != null && !inputJSON.get(CommonConstants.FREQUENCY).toString().isEmpty()
								&& inputJSON.get(CommonConstants.FREQUENCY).toString().equalsIgnoreCase("Y")){
							String fromMonth = null;
							String toMonth = null;
							Long batchId = null;
							List<FyDO> fyList = new ArrayList<FyDO>();
							if(!inputJSON.get(CommonConstants.FYID).toString().isEmpty()){
								fyList = fyService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.FYID).toString()));
								if(fyList != null && fyList.size() > 0){
									if(!inputJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty()){
										batchId = Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString());
										if(batchId == 0){
											batchId = null;
										}
									}
									fromMonth = "Apr"+" "+fyList.get(0).getFromyear();
									toMonth = "Mar"+" "+fyList.get(0).getToyear(); 
									List<EmployeePayrollDO> compensationList = employeePayrollService.retriveForReportQuarter(fromMonth, toMonth,	batchId);
									if(compensationList != null && compensationList.size() > 0){
										respJSON = EmployeePayrollUtil.YtdForMultipleMonth(compensationList, fromMonth, toMonth, CommonConstants.YEARLYC).toString();
									}
								}
							}
						}
					}
				}
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}*/
}
