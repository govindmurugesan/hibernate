package com.spheresuite.erp.rs;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.CompanyDO;
import com.spheresuite.erp.service.CompanyService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.CompanyUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/company")
public class CompanyRS {

	String validation = null;
	static Logger logger = Logger.getLogger(CompanyRS.class.getName());

	@Autowired
	private CompanyService companyService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				CompanyDO companyDO = new CompanyDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
			 			companyDO.setName(inputJSON.get(CommonConstants.NAME).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.TANNO) != null && !inputJSON.get(CommonConstants.TANNO).toString().isEmpty()){
			 			companyDO.setTanno(inputJSON.get(CommonConstants.TANNO).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.CIN) != null && !inputJSON.get(CommonConstants.CIN).toString().isEmpty()){
			 			companyDO.setCin(inputJSON.get(CommonConstants.CIN).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.SERVICETAXNUMBER) != null && !inputJSON.get(CommonConstants.SERVICETAXNUMBER).toString().isEmpty()){
			 			companyDO.setServiceTaxNumber(inputJSON.get(CommonConstants.SERVICETAXNUMBER).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.GSTIN) != null && !inputJSON.get(CommonConstants.GSTIN).toString().isEmpty()){
			 			companyDO.setGsTin(inputJSON.get(CommonConstants.GSTIN).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.PANNO) != null && !inputJSON.get(CommonConstants.PANNO).toString().isEmpty()){
			 			companyDO.setPanno(inputJSON.get(CommonConstants.PANNO).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.ADDRESS1) != null && !inputJSON.get(CommonConstants.ADDRESS1).toString().isEmpty()){
			 			companyDO.setAddress1(inputJSON.get(CommonConstants.ADDRESS1).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.ADDRESS2) != null && !inputJSON.get(CommonConstants.ADDRESS2).toString().isEmpty()){
			 			companyDO.setAddress2(inputJSON.get(CommonConstants.ADDRESS2).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.CITY) != null && !inputJSON.get(CommonConstants.CITY).toString().isEmpty()){
			 			companyDO.setCity(inputJSON.get(CommonConstants.CITY).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.STATE) != null &&  !inputJSON.get(CommonConstants.STATE).toString().isEmpty()){
			 			companyDO.setState(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.STATE).toString() : null));
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.COUNTRY) != null &&  !inputJSON.get(CommonConstants.COUNTRY).toString().isEmpty()){
			 			companyDO.setCountry(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.COUNTRY).toString() : null));
			 		}
			 		if(inputJSON.get(CommonConstants.ZIP) != null &&  !inputJSON.get(CommonConstants.ZIP).toString().isEmpty()){
			 			companyDO.setZip(inputJSON != null ? inputJSON.get(CommonConstants.ZIP).toString() : null);
			 		}
			 		if(inputJSON.get(CommonConstants.MOBILE) != null &&  !inputJSON.get(CommonConstants.MOBILE).toString().isEmpty()){
			 			companyDO.setMobile(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.MOBILE).toString() : null));
			 		}
			 		if(inputJSON.get(CommonConstants.INDUSTRY) != null &&  !inputJSON.get(CommonConstants.INDUSTRY).toString().isEmpty()){
			 			companyDO.setIndustry(inputJSON != null ? inputJSON.get(CommonConstants.INDUSTRY).toString() : null);
			 		}
			 		if(inputJSON.get(CommonConstants.WEBSITE) != null &&  !inputJSON.get(CommonConstants.WEBSITE).toString().isEmpty()){
			 			companyDO.setWebsite(inputJSON != null ? inputJSON.get(CommonConstants.WEBSITE).toString() : null);
			 		}
			 		if(inputJSON.get(CommonConstants.CALLING_CODE) != null &&  !inputJSON.get(CommonConstants.CALLING_CODE).toString().isEmpty()){
			 			companyDO.setCallingCode(inputJSON != null ? inputJSON.get(CommonConstants.CALLING_CODE).toString() : null);
			 		}
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			companyDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			companyDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		companyDO.setUpdatedon(new Date());
			 		companyDO.setCreatedon(new Date());
			 	}
				companyService.persist(companyDO);
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Company Details Created");
				return CommonWebUtil.buildSuccessResponseId(companyDO.getId().toString()).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
	}
	
	
	
	@RequestMapping(value = "/retrieveById/{inputParams}", method = RequestMethod.GET)
		public @ResponseBody String retrieveById(@PathVariable String inputParams, Model model, HttpServletRequest request) {
			String respJSON = null;
			try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<CompanyDO> companyList = companyService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = CompanyUtil.getCompanyList(companyList).toString();
				}
				
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<CompanyDO> categoryList = companyService.retrieve();
				respJSON = CompanyUtil.getCompanyList(categoryList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				CompanyDO companyDO = new CompanyDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<CompanyDO> companyList = companyService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(companyList != null && companyList.size() > 0){
			 			companyDO = companyList.get(0);
			 			if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
				 			companyDO.setName(inputJSON.get(CommonConstants.NAME).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.TANNO) != null && !inputJSON.get(CommonConstants.TANNO).toString().isEmpty()){
				 			companyDO.setTanno(inputJSON.get(CommonConstants.TANNO).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.CIN) != null && !inputJSON.get(CommonConstants.CIN).toString().isEmpty()){
				 			companyDO.setCin(inputJSON.get(CommonConstants.CIN).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.SERVICETAXNUMBER) != null && !inputJSON.get(CommonConstants.SERVICETAXNUMBER).toString().isEmpty()){
				 			companyDO.setServiceTaxNumber(inputJSON.get(CommonConstants.SERVICETAXNUMBER).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.GSTIN) != null && !inputJSON.get(CommonConstants.GSTIN).toString().isEmpty()){
				 			companyDO.setGsTin(inputJSON.get(CommonConstants.GSTIN).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.PANNO) != null && !inputJSON.get(CommonConstants.PANNO).toString().isEmpty()){
				 			companyDO.setPanno(inputJSON.get(CommonConstants.PANNO).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.ADDRESS1) != null && !inputJSON.get(CommonConstants.ADDRESS1).toString().isEmpty()){
				 			companyDO.setAddress1(inputJSON.get(CommonConstants.ADDRESS1).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.ADDRESS2) != null && !inputJSON.get(CommonConstants.ADDRESS2).toString().isEmpty()){
				 			companyDO.setAddress2(inputJSON.get(CommonConstants.ADDRESS2).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.CITY) != null && !inputJSON.get(CommonConstants.CITY).toString().isEmpty()){
				 			companyDO.setCity(inputJSON.get(CommonConstants.CITY).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.STATE) != null &&  !inputJSON.get(CommonConstants.STATE).toString().isEmpty()){
				 			companyDO.setState(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.STATE).toString() : null));
				 		}
				 		
				 		if(inputJSON.get(CommonConstants.COUNTRY) != null &&  !inputJSON.get(CommonConstants.COUNTRY).toString().isEmpty()){
				 			companyDO.setCountry(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.COUNTRY).toString() : null));
				 		}
				 		if(inputJSON.get(CommonConstants.ZIP) != null &&  !inputJSON.get(CommonConstants.ZIP).toString().isEmpty()){
				 			companyDO.setZip(inputJSON != null ? inputJSON.get(CommonConstants.ZIP).toString() : null);
				 		}
				 		if(inputJSON.get(CommonConstants.MOBILE) != null &&  !inputJSON.get(CommonConstants.MOBILE).toString().isEmpty()){
				 			companyDO.setMobile(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.MOBILE).toString() : null));
				 		}
				 		if(inputJSON.get(CommonConstants.INDUSTRY) != null &&  !inputJSON.get(CommonConstants.INDUSTRY).toString().isEmpty()){
				 			companyDO.setIndustry(inputJSON != null ? inputJSON.get(CommonConstants.INDUSTRY).toString() : null);
				 		}
				 		if(inputJSON.get(CommonConstants.WEBSITE) != null &&  !inputJSON.get(CommonConstants.WEBSITE).toString().isEmpty()){
				 			companyDO.setWebsite(inputJSON != null ? inputJSON.get(CommonConstants.WEBSITE).toString() : null);
				 		}
				 		if(inputJSON.get(CommonConstants.CALLING_CODE) != null &&  !inputJSON.get(CommonConstants.CALLING_CODE).toString().isEmpty()){
				 			companyDO.setCallingCode(inputJSON != null ? inputJSON.get(CommonConstants.CALLING_CODE).toString() : null);
				 		}
				 		companyDO.setUpdatedon(new Date());
				 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			companyDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		companyService.update(companyDO);
				 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Company Updated");
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/updatepic", method = RequestMethod.POST)
	public @ResponseBody String updatepic(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				CompanyDO companyDO = new CompanyDO();
				/*JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("data").toString());*/
			 	if (request.getParameter("file") != null && request.getParameter("id") != null){
			 		List<CompanyDO> companyList = companyService.retrieveById(Long.parseLong(request.getParameter("id")));
			 		if(companyList != null && companyList.size() > 0){
			 			companyDO = companyList.get(0);
				 		companyDO.setPhoto(request.getParameter("file"));
				 		companyDO.setUpdatedon(new Date());
				 		companyService.update(companyDO);
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/updateicon", method = RequestMethod.POST)
	public @ResponseBody String updateicon(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				CompanyDO companyDO = new CompanyDO();
				/*JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("data").toString());*/
			 	if (request.getParameter("file") != null && request.getParameter("id") != null){
			 		List<CompanyDO> companyList = companyService.retrieveById(Long.parseLong(request.getParameter("id")));
			 		if(companyList != null && companyList.size() > 0){
			 			companyDO = companyList.get(0);
				 		companyDO.setCompanyIcon(request.getParameter("file"));
				 		companyDO.setUpdatedon(new Date());
				 		companyService.update(companyDO);
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
