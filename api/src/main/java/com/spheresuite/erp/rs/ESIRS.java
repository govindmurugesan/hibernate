package com.spheresuite.erp.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.ESIDO;
import com.spheresuite.erp.service.ESIService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.ESIUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/esi")
public class ESIRS {

	String validation = null;
	static Logger logger = Logger.getLogger(ESIRS.class.getName());

	@Autowired
	private ESIService esiService;
	
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				ESIDO esiDO = new ESIDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		
			 		if(inputJSON.get(CommonConstants.PERCENTAGE) != null && !inputJSON.get(CommonConstants.PERCENTAGE).toString().isEmpty()){
			 			esiDO.setEmployeePercentage(Double.parseDouble(inputJSON.get(CommonConstants.PERCENTAGE).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.EMPLOYERPERCENTAGE) != null && !inputJSON.get(CommonConstants.EMPLOYERPERCENTAGE).toString().isEmpty()){
			 			esiDO.setEmployerPercentage(Double.parseDouble(inputJSON.get(CommonConstants.EMPLOYERPERCENTAGE).toString()));
			 		}
			 		
			 		esiDO.setUpdatedon(new Date());
			 		esiDO.setCreatedon(new Date());
			 		esiDO.setStatus(CommonConstants.ACTIVE);
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			esiDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			esiDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 	}
				if(!esiService.persist(esiDO)){
					return CommonWebUtil.buildErrorResponse("ESI Percentage Already Added").toString();
				}
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "ESI Percentage Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<ESIDO> esilist = esiService.retrieve();
				respJSON = ESIUtil.getESIList(esilist).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				ESIDO esiDO = new ESIDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<ESIDO> esiList = esiService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(esiList != null && esiList.size() > 0){
			 			esiDO = esiList.get(0);
			 			if(inputJSON.get(CommonConstants.EMPLOYERPERCENTAGE) != null && !inputJSON.get(CommonConstants.EMPLOYERPERCENTAGE).toString().isEmpty()){
				 			esiDO.setEmployerPercentage(Double.parseDouble(inputJSON.get(CommonConstants.EMPLOYERPERCENTAGE).toString()));
				 		}
				 		if(inputJSON.get(CommonConstants.PERCENTAGE) != null && !inputJSON.get(CommonConstants.PERCENTAGE).toString().isEmpty()){
				 			esiDO.setEmployeePercentage(Double.parseDouble(inputJSON.get(CommonConstants.PERCENTAGE).toString()));
				 		}
				 		
				 		if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
				 			esiDO.setStatus((char) (inputJSON.get(CommonConstants.STATUS).toString().charAt(0)));
				 		}
				 		esiDO.setUpdatedon(new Date());
				 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			esiDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		if(!esiService.update(esiDO)){
							return CommonWebUtil.buildErrorResponse("ESI Percentage Already Added").toString();
						}
				 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "ESI Percentage Updated");
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/importEducationCess", method = RequestMethod.POST)
	public @ResponseBody String importEducationCess(Model model, HttpServletRequest request) {
		try {
			JSONObject inputJSON =  CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
			JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
			List<ESIDO> esiList = new ArrayList<ESIDO>();
			Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY);
			for (int i=0; i < fileData.size(); i++){
				JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
				ESIDO esiDO = new ESIDO();
				
				if(colName.get(CommonConstants.EMPLOYERPERCENTAGE) != null && !colName.get(CommonConstants.EMPLOYERPERCENTAGE).toString().isEmpty()){
		 			esiDO.setEmployerPercentage(Double.parseDouble(rowJSON.get(colName.get(CommonConstants.EMPLOYERPERCENTAGE)).toString()));
		 		}
		 		if(colName.get(CommonConstants.PERCENTAGE) != null && !colName.get(CommonConstants.PERCENTAGE).toString().isEmpty()){
		 			esiDO.setEmployeePercentage(Double.parseDouble(rowJSON.get(colName.get(CommonConstants.PERCENTAGE)).toString()));
		 		}
		 		if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
		 			esiDO.setUpdatedby(updatedBy.toString());
		 			esiDO.setCreatedby(updatedBy.toString());
		 		}
		 		esiDO.setUpdatedon(new Date());
		 		esiDO.setCreatedon(new Date());
		 		esiDO.setStatus(CommonConstants.ACTIVE);
		 		esiList.add(esiDO);
			}
			esiService.persistList(esiList);
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
