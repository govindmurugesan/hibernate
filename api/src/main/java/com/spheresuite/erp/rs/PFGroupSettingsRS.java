package com.spheresuite.erp.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.AllowanceSettingDO;
import com.spheresuite.erp.domainobject.PFGroupDO;
import com.spheresuite.erp.domainobject.PFGroupEarningsDO;
import com.spheresuite.erp.service.AllowanceSettingsService;
import com.spheresuite.erp.service.PFGroupEarningService;
import com.spheresuite.erp.service.PFGroupService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.PFGroupUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/pfgroupsettings")
public class PFGroupSettingsRS {

	String validation = null;
	static Logger logger = Logger.getLogger(PFGroupSettingsRS.class.getName());
	
	@Autowired
	private PFGroupService pfGroupService;
	
	@Autowired
	private PFGroupEarningService pfGroupEarningService;
	
	@Autowired
	private AllowanceSettingsService allowanceSettingsService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		PFGroupDO pfGrp = new PFGroupDO();
			 		if(inputJSON.get(CommonConstants.PFGROUPID) != null && !inputJSON.get(CommonConstants.PFGROUPID).toString().isEmpty() && inputJSON.get(CommonConstants.AMOUNT) != null && !inputJSON.get(CommonConstants.AMOUNT).toString().isEmpty()){
			 			List<PFGroupDO> pfGroupList = pfGroupService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.PFGROUPID).toString()));
				 		if(pfGroupList != null && pfGroupList.size() >0){
				 			pfGrp = pfGroupList.get(0);
				 			pfGrp.setMaxAmount(Double.parseDouble(inputJSON.get(CommonConstants.AMOUNT).toString()));
				 			if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 				pfGrp.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 		}
				 			pfGrp.setUpdatedon(new Date());
				 			pfGroupService.update(pfGrp);
				 			
				 		}
			 		}
			 		
			 		
			 		if (inputJSON.get(CommonConstants.ALLOWENCE_ID) != null && !inputJSON.get(CommonConstants.ALLOWENCE_ID).toString().isEmpty()){
				 		JSONArray resultJSONArray = new JSONArray();
				 		resultJSONArray.put(inputJSON.get(CommonConstants.ALLOWENCE_ID));
				 		String rec = (String) resultJSONArray.get(0).toString();
				 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
				 		for (int i = 0; i < resultJSONArray1.size(); i++) {
				 			PFGroupEarningsDO pfGroupEarningDO = new PFGroupEarningsDO();
					 		if(inputJSON.get(CommonConstants.PFGROUPID) != null && !inputJSON.get(CommonConstants.PFGROUPID).toString().isEmpty()){
					 			List<PFGroupDO> pfGroupList = pfGroupService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.PFGROUPID).toString()));
						 		if(pfGroupList != null && pfGroupList.size() >0){
						 			pfGroupEarningDO.setPfGroup(pfGroupList.get(0));
						 		}
					 		}
					 		
					 		if(resultJSONArray1.get(i) != null ){
						 		List<AllowanceSettingDO> allowanceSettingList = allowanceSettingsService.retrieveById(Long.parseLong(resultJSONArray1.get(i).toString()));
					 			if(allowanceSettingList != null && allowanceSettingList.size() > 0){
					 				pfGroupEarningDO.setAllowanceSettings(allowanceSettingList.get(0));
					 			}
					 		}
					 		pfGroupEarningDO.setStatus(CommonConstants.ACTIVE);
					 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			pfGroupEarningDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			pfGroupEarningDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 		}
					 		pfGroupEarningDO.setUpdatedon(new Date());
					 		pfGroupEarningDO.setCreatedon(new Date());
					 		
					 		if(!pfGroupEarningService.persist(pfGroupEarningDO)){
					 			//return CommonWebUtil.buildErrorResponse("Payroll Group Already Added").toString();
							}
				 		}
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.DEACTIVATEDEARNINGMENU) != null && !inputJSON.get(CommonConstants.DEACTIVATEDEARNINGMENU).toString().isEmpty()){
			 			JSONArray resultJSONArray = new JSONArray();
				 		resultJSONArray.put(inputJSON.get(CommonConstants.DEACTIVATEDEARNINGMENU));
				 		String rec = (String) resultJSONArray.get(0).toString();
				 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
				 		List<PFGroupEarningsDO> pfGroupDOlist = new ArrayList<PFGroupEarningsDO>();
				 		for (int i = 0; i < resultJSONArray1.size(); i++) {
				 			PFGroupEarningsDO pfGrpDO = new PFGroupEarningsDO();
				 			List<PFGroupEarningsDO> payrollGroupEarningList = pfGroupEarningService.retrieveByID(Long.parseLong(resultJSONArray1.get(i).toString()));
				 			if(payrollGroupEarningList != null && payrollGroupEarningList.size() > 0){
				 				pfGrpDO = payrollGroupEarningList.get(0);
				 				pfGrpDO.setStatus(CommonConstants.INACTIVE);
				 				pfGroupDOlist.add(pfGrpDO);
				 			}
				 		}
				 		pfGroupEarningService.updateList(pfGroupDOlist);
			 		}
			 	}
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "PF Group Settings Updated");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
			 		
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveByGroupId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<PFGroupDO> pfGroupList = pfGroupService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = PFGroupUtil.getPFGroupListDetails(pfGroupList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
}
