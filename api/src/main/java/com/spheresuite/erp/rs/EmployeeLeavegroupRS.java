package com.spheresuite.erp.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.EmployeeLeavegroupDO;
import com.spheresuite.erp.domainobject.LeaveGroupDO;
import com.spheresuite.erp.service.EmployeeLeavegroupService;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.LeaveGroupService;
import com.spheresuite.erp.service.PFGroupService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.EmployeeLeaveGroupUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/empleavegroup")
public class EmployeeLeavegroupRS {

	String validation = null;
	static Logger logger = Logger.getLogger(EmployeeLeavegroupRS.class.getName());

	@Autowired
	private EmployeeLeavegroupService employeeLeavegrpService;
	
	@Autowired
	private EmployeeService employeeService;
	
	@Autowired
	private LeaveGroupService leaveGroupService;
	
	@Autowired
	private PFGroupService pfGroupService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeLeavegroupDO employeeLeavegroupDO = new EmployeeLeavegroupDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.LEAVEGROUP_ID) != null && !inputJSON.get(CommonConstants.LEAVEGROUP_ID).toString().isEmpty()){
			 			List<LeaveGroupDO> leaveGroupList = leaveGroupService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.LEAVEGROUP_ID).toString()));
				 		if(leaveGroupList != null && leaveGroupList.size() > 0){
				 			employeeLeavegroupDO.setLeaveGroup(leaveGroupList.get(0));
				 		}
			 		}
			 		if(inputJSON.get(CommonConstants.EMPID).toString() != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
				 		if(employeeList != null && employeeList.size() > 0){
				 			employeeLeavegroupDO.setEmployee(employeeList.get(0));
				 		}
			 		}
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			employeeLeavegroupDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			employeeLeavegroupDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		employeeLeavegroupDO.setUpdatedon(new Date());
			 		employeeLeavegroupDO.setCreatedon(new Date());
			 	}
			 	employeeLeavegrpService.persist(employeeLeavegroupDO);
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Leave Group Updated");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retrieveByEmpId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByEmpId(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<EmployeeLeavegroupDO> employeeDetailList = employeeLeavegrpService.retrieveByEmpId(inputJSON.get(CommonConstants.ID).toString());
					respJSON = EmployeeLeaveGroupUtil.getEmployeeLeavegroupList(employeeDetailList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeLeavegroupDO employeeLeaveGrpDO = new EmployeeLeavegroupDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<EmployeeLeavegroupDO> employeeLeavegrplList = employeeLeavegrpService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		employeeLeaveGrpDO = employeeLeavegrplList.get(0);
			 		
			 		if(inputJSON.get(CommonConstants.LEAVEGROUP_ID) != null && !inputJSON.get(CommonConstants.LEAVEGROUP_ID).toString().isEmpty()){
			 			List<LeaveGroupDO> leaveGroupList = leaveGroupService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.LEAVEGROUP_ID).toString()));
				 		if(leaveGroupList != null && leaveGroupList.size() > 0){
				 			employeeLeaveGrpDO.setLeaveGroup(leaveGroupList.get(0));
				 		}
			 		}
			 		if(inputJSON.get(CommonConstants.EMPID).toString() != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
				 		if(employeeList != null && employeeList.size() > 0){
				 			employeeLeaveGrpDO.setEmployee(employeeList.get(0));
				 		}
			 		}
			 		employeeLeaveGrpDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			employeeLeaveGrpDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		employeeLeavegrpService.update(employeeLeaveGrpDO);
			 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Employee Leave Group Updated");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/importLeavegpInfo", method = RequestMethod.POST)
	public @ResponseBody String importCountry(Model model, HttpServletRequest request) {
		try {
				JSONObject inputJSON =  CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
				org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
				JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
				List<EmployeeLeavegroupDO> empLeavegrplist = new ArrayList<EmployeeLeavegroupDO>();
				Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY);
				for (int i=0; i < fileData.size(); i++){
					JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
					EmployeeLeavegroupDO EmployeeLeavegroup = new EmployeeLeavegroupDO();
					
					if(colName.get(CommonConstants.EMPID).toString() != null && !colName.get(CommonConstants.EMPID).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(rowJSON.get(colName.get(CommonConstants.EMPID)).toString());
				 		if(employeeList != null && employeeList.size() > 0){
				 			EmployeeLeavegroup.setEmployee(employeeList.get(0));
				 			if(colName.get(CommonConstants.LEAVEGROUP) != null && !colName.get(CommonConstants.LEAVEGROUP).toString().isEmpty()){
					 			List<LeaveGroupDO> leaveGroupList = leaveGroupService.retrieveByName(rowJSON.get(colName.get(CommonConstants.LEAVEGROUP)).toString());
						 		if(leaveGroupList != null && leaveGroupList.size() > 0){
						 			EmployeeLeavegroup.setLeaveGroup(leaveGroupList.get(0));
						 			if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
						 				EmployeeLeavegroup.setUpdatedby(updatedBy.toString());
						 				EmployeeLeavegroup.setCreatedby(updatedBy.toString());
							 		}
						 			EmployeeLeavegroup.setUpdatedon(new Date());
						 			EmployeeLeavegroup.setCreatedon(new Date());
							 		empLeavegrplist.add(EmployeeLeavegroup);
						 		}
					 		}
				 		}
			 		}
				}
				employeeLeavegrpService.persistList(empLeavegrplist);
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
