package com.spheresuite.erp.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.PFGroupDO;
import com.spheresuite.erp.service.PFGroupService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.PFGroupUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/pfgroup")
public class PFGroupRS {

	String validation = null;
	static Logger logger = Logger.getLogger(PFGroupRS.class.getName());
	
	@Autowired
	private PFGroupService pfGroupService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				PFGroupDO pfGroupDO = new PFGroupDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
			 			pfGroupDO.setName(inputJSON.get(CommonConstants.NAME).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.AMOUNT) != null && !inputJSON.get(CommonConstants.AMOUNT).toString().isEmpty()){
			 			pfGroupDO.setMaxAmount(Double.parseDouble(inputJSON.get(CommonConstants.AMOUNT).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.DESCRIPTION) != null && !inputJSON.get(CommonConstants.DESCRIPTION).toString().isEmpty()){
			 			pfGroupDO.setDescription(inputJSON.get(CommonConstants.DESCRIPTION).toString());
			 		}
			 		pfGroupDO.setStatus(CommonConstants.ACTIVE);
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			pfGroupDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			pfGroupDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		pfGroupDO.setUpdatedon(new Date());
			 		pfGroupDO.setCreatedon(new Date());
			 	}
				if(!pfGroupService.persist(pfGroupDO)){
		 			return CommonWebUtil.buildErrorResponse("PF Group Already Added").toString();
				}
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New PF Group Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<PFGroupDO> pfGroupList = pfGroupService.retrieveActive();
				//respJSON = payrollGroupList
				respJSON = PFGroupUtil.getPFGroupListDetails(pfGroupList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<PFGroupDO> pfGroupList = pfGroupService.retrieve();
				respJSON = PFGroupUtil.getPFGroupListDetails(pfGroupList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/importPFGroup", method = RequestMethod.POST)
	public @ResponseBody String importPFGroup(Model model, HttpServletRequest request) {
		try {
			JSONObject inputJSON =  CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
			JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
			List<PFGroupDO> pfGroupList = new ArrayList<PFGroupDO>();
			Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY);
			for (int i=0; i < fileData.size(); i++){
				JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
				PFGroupDO pfGroupDO = new PFGroupDO();
				
				if (colName.get(CommonConstants.NAME) != null && !colName.get(CommonConstants.NAME).toString().isEmpty()){
					pfGroupDO.setName(rowJSON.get(colName.get(CommonConstants.NAME)).toString());
		 		}
				if(colName.get(CommonConstants.DESCRIPTION) != null && !colName.get(CommonConstants.DESCRIPTION).toString().isEmpty()){
					pfGroupDO.setDescription(rowJSON.get(colName.get(CommonConstants.DESCRIPTION)).toString());
		 		}
				if(colName.get(CommonConstants.AMOUNT) != null && !colName.get(CommonConstants.AMOUNT).toString().isEmpty()){
		 			pfGroupDO.setMaxAmount(Double.parseDouble(rowJSON.get(colName.get(CommonConstants.AMOUNT)).toString()));
		 		}
		 		if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
		 			pfGroupDO.setUpdatedby(updatedBy.toString());
		 			pfGroupDO.setCreatedby(updatedBy.toString());
		 		}
		 		pfGroupDO.setUpdatedon(new Date());
		 		pfGroupDO.setCreatedon(new Date());
		 		pfGroupDO.setStatus(CommonConstants.ACTIVE);
		 		pfGroupList.add(pfGroupDO);
			}
			pfGroupService.persistList(pfGroupList);
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				PFGroupDO pfGroupDO = new PFGroupDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<PFGroupDO> pfGroupList = pfGroupService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(pfGroupList != null && pfGroupList.size() >0){
			 			pfGroupDO = pfGroupList.get(0);
				 		if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
				 			pfGroupDO.setName(inputJSON.get(CommonConstants.NAME).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.DESCRIPTION) != null && !inputJSON.get(CommonConstants.DESCRIPTION).toString().isEmpty()){
				 			pfGroupDO.setDescription(inputJSON.get(CommonConstants.DESCRIPTION).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.AMOUNT) != null && !inputJSON.get(CommonConstants.AMOUNT).toString().isEmpty()){
				 			pfGroupDO.setMaxAmount(Double.parseDouble(inputJSON.get(CommonConstants.AMOUNT).toString()));
				 		}
				 		if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
				 			pfGroupDO.setStatus((char) inputJSON.get(CommonConstants.STATUS).toString().charAt(0));
				 		}
				 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			pfGroupDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		pfGroupDO.setUpdatedon(new Date());
				 		if(!pfGroupService.update(pfGroupDO)){
				 			return CommonWebUtil.buildErrorResponse("PF Group Already Added").toString();
				 		}
				 		CommonUtil.persistRecentAcvtivity(inputJSON.get(CommonConstants.UPDATED_BY).toString(), "PF Group Updated");
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
