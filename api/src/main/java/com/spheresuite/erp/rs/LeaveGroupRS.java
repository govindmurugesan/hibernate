package com.spheresuite.erp.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.LeaveGroupDO;
import com.spheresuite.erp.service.LeaveGroupService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.LeaveGroupUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/leaveGroup")
public class LeaveGroupRS {

	String validation = null;
	static Logger logger = Logger.getLogger(LeaveGroupRS.class.getName());

	@Autowired
	private LeaveGroupService leaveGroupService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				LeaveGroupDO leaveGroupDO = new LeaveGroupDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if (inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
			 			leaveGroupDO.setLeaveGroup(inputJSON.get(CommonConstants.NAME).toString());
			 		}
			 		if (inputJSON.get(CommonConstants.DESCRIPTION) != null && !inputJSON.get(CommonConstants.DESCRIPTION).toString().isEmpty()){
			 			leaveGroupDO.setDescription(inputJSON.get(CommonConstants.DESCRIPTION).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			leaveGroupDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			leaveGroupDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		leaveGroupDO.setStatus(CommonConstants.ACTIVE);
			 		leaveGroupDO.setUpdatedon(new Date());
			 		leaveGroupDO.setCreatedon(new Date());
			 		if(!leaveGroupService.persist(leaveGroupDO)){
						return CommonWebUtil.buildErrorResponse("Leave Group Already Added").toString();
					}
			 		
				 	CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Leave Group Created");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<LeaveGroupDO> leaveGroupList = leaveGroupService.retrieve();
				respJSON = LeaveGroupUtil.getLeaveGroupList(leaveGroupList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<LeaveGroupDO> leaveGroupList = leaveGroupService.retrieveActive();
				respJSON = LeaveGroupUtil.getLeaveGroupList(leaveGroupList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				LeaveGroupDO leaveGroupDO = new LeaveGroupDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		
			 			List<LeaveGroupDO> leaveGroupList = leaveGroupService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
				 		if(leaveGroupList != null && leaveGroupList.size() > 0){
				 			leaveGroupDO = leaveGroupList.get(0);
				 			if (inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
				 				leaveGroupDO.setLeaveGroup(inputJSON.get(CommonConstants.NAME).toString());
					 		}
				 			if (inputJSON.get(CommonConstants.DESCRIPTION) != null && !inputJSON.get(CommonConstants.DESCRIPTION).toString().isEmpty()){
					 			leaveGroupDO.setDescription(inputJSON.get(CommonConstants.DESCRIPTION).toString());
					 		}
				 			leaveGroupDO.setUpdatedon(new Date());
					 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			leaveGroupDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 		}
					 		if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
					 			leaveGroupDO.setStatus((char) inputJSON.get(CommonConstants.STATUS).toString().charAt(0));
					 		}
					 		if(!leaveGroupService.update(leaveGroupDO)){
								return CommonWebUtil.buildErrorResponse("Leave Group  Already Added").toString();
							}
					 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Leave Group Updated");
				 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/importLeaveGroup", method = RequestMethod.POST)
	public @ResponseBody String importLeaveGroup(Model model, HttpServletRequest request) {
		try {
			JSONObject inputJSON =  CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
			JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
			List<LeaveGroupDO> leaveGrouplist = new ArrayList<LeaveGroupDO>();
			Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY);
			for (int i=0; i < fileData.size(); i++){
				JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
				LeaveGroupDO leaveGroupDO = new LeaveGroupDO();
				if (colName.get(CommonConstants.NAME) != null && !colName.get(CommonConstants.NAME).toString().isEmpty()){
					leaveGroupDO.setLeaveGroup(rowJSON.get(colName.get(CommonConstants.NAME)).toString());
		 		}
				if (colName.get(CommonConstants.DESCRIPTION) != null && !colName.get(CommonConstants.DESCRIPTION).toString().isEmpty()){
					leaveGroupDO.setDescription(rowJSON.get(colName.get(CommonConstants.DESCRIPTION)).toString());
		 		}
		 		if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
		 			leaveGroupDO.setUpdatedby(updatedBy.toString());
		 			leaveGroupDO.setCreatedby(updatedBy.toString());
		 		}
		 		leaveGroupDO.setUpdatedon(new Date());
		 		leaveGroupDO.setCreatedon(new Date());
		 		leaveGroupDO.setStatus(CommonConstants.ACTIVE);
		 		leaveGrouplist.add(leaveGroupDO);
			}
			leaveGroupService.persistList(leaveGrouplist);
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
