package com.spheresuite.erp.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.TimesheetTypeDO;
import com.spheresuite.erp.service.TimesheetTypeService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.TimesheetTypeUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/timesheetType")
public class TimesheetTypeRS {

	String validation = null;
	static Logger logger = Logger.getLogger(TimesheetTypeRS.class.getName());

	@Autowired
	private TimesheetTypeService timesheetTypeService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				TimesheetTypeDO timesheetTypeDO = new TimesheetTypeDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
			 			timesheetTypeDO.setName(inputJSON.get(CommonConstants.NAME).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			timesheetTypeDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			timesheetTypeDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		timesheetTypeDO.setUpdatedon(new Date());
			 		timesheetTypeDO.setCreatedon(new Date());
			 		
			 		timesheetTypeDO.setStatus(CommonConstants.ACTIVE);
			 	}
				if(!timesheetTypeService.persist(timesheetTypeDO)){
		 			return CommonWebUtil.buildErrorResponse("Timesheet Type Already Added").toString();
				}
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New TimeSheet Type Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<TimesheetTypeDO> timesheetTypeList = timesheetTypeService.retrieve();
				respJSON = TimesheetTypeUtil.getTimesheetTypeList(timesheetTypeList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				TimesheetTypeDO timesheetTypeDO = new TimesheetTypeDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<TimesheetTypeDO> timesheetTypeList = timesheetTypeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(timesheetTypeList != null && timesheetTypeList.size() > 0){
			 			timesheetTypeDO = timesheetTypeList.get(0);
			 			if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
				 			timesheetTypeDO.setName(inputJSON.get(CommonConstants.NAME).toString());
				 		}
				 		timesheetTypeDO.setUpdatedon(new Date());
				 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			timesheetTypeDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
				 			timesheetTypeDO.setStatus((char) inputJSON.get(CommonConstants.STATUS).toString().charAt(0));
				 		}
				 		if(!timesheetTypeService.update(timesheetTypeDO)){
				 			return CommonWebUtil.buildErrorResponse("TimeSheet Type Already Added").toString();
				 		}
				 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "TimeSheet Type Updated");
			 		}
			 		
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/importTimesheetType", method = RequestMethod.POST)
	public @ResponseBody String importTimesheetType(Model model, HttpServletRequest request) {
		try {
			JSONObject inputJSON =  CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
			JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
			List<TimesheetTypeDO> timesheetTypelist = new ArrayList<TimesheetTypeDO>();
			Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY);
			for (int i=0; i < fileData.size(); i++){
				JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
				TimesheetTypeDO timesheetType = new TimesheetTypeDO();
				
				if (colName.get(CommonConstants.NAME) != null && !colName.get(CommonConstants.NAME).toString().isEmpty()){
					timesheetType.setName(rowJSON.get(colName.get(CommonConstants.NAME)).toString());
		 		}
		 		if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
		 			timesheetType.setUpdatedby(updatedBy.toString());
		 			timesheetType.setCreatedby(updatedBy.toString());
		 		}
		 		timesheetType.setUpdatedon(new Date());
		 		timesheetType.setCreatedon(new Date());
		 		timesheetType.setStatus(CommonConstants.ACTIVE);
		 		timesheetTypelist.add(timesheetType);
			}
			timesheetTypeService.persistList(timesheetTypelist);
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
