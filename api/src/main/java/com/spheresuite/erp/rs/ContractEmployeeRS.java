package com.spheresuite.erp.rs;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.ContractEmployeeDO;
import com.spheresuite.erp.domainobject.DepartmentDO;
import com.spheresuite.erp.domainobject.DesignationDO;
import com.spheresuite.erp.domainobject.SuppliersDO;
import com.spheresuite.erp.service.ContractEmployeeService;
import com.spheresuite.erp.service.DepartmentService;
import com.spheresuite.erp.service.DesignationService;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.supplier.service.SupplierService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.ContractEmployeeUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/contractLabour")
public class ContractEmployeeRS {

	String validation = null;
	static Logger logger = Logger.getLogger(ContractEmployeeRS.class.getName());
	
	@Autowired
	private ContractEmployeeService contractLaboursService;
	@Autowired
	private SupplierService supplierService;
	
	@Autowired
	private EmployeeService employeeService;
	
	@Autowired
	private DepartmentService departmentService;
	
	@Autowired
	private DesignationService designationService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				ContractEmployeeDO contractLaboursDO = new ContractEmployeeDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
			 			contractLaboursDO.setName(inputJSON.get(CommonConstants.NAME).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.SUPPLIERID) != null && !inputJSON.get(CommonConstants.SUPPLIERID).toString().isEmpty()){
			 			List<SuppliersDO> suppliersList = supplierService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.SUPPLIERID).toString()));
			 			if(suppliersList != null && suppliersList.size() > 0){
			 				contractLaboursDO.setSupplier(suppliersList.get(0));
			 			}
		 			}
			 		
			 		if(inputJSON.get(CommonConstants.EMPID) != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			contractLaboursDO.setEmpId(inputJSON.get(CommonConstants.EMPID).toString());
		 			}
			 		
			 		
			 		if(inputJSON.get(CommonConstants.DESIGNATION) != null && !inputJSON.get(CommonConstants.DESIGNATION).toString().isEmpty()){
			 			List<DesignationDO> designationList = designationService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.DESIGNATION).toString()));
			 			if(designationList != null && designationList.size() > 0){
			 				contractLaboursDO.setDesignation(designationList.get(0));
			 			}
		 			}
			 		
			 		if(inputJSON.get(CommonConstants.DEPARTMENT) != null && !inputJSON.get(CommonConstants.DEPARTMENT).toString().isEmpty()){
			 			List<DepartmentDO> deptList = departmentService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.DEPARTMENT).toString()));
			 			if(deptList != null && deptList.size() > 0){
			 				contractLaboursDO.setDepartment(deptList.get(0));
			 			}
		 			}
			 		
			 		if(inputJSON.get(CommonConstants.DOB) != null && !inputJSON.get(CommonConstants.DOB).toString().isEmpty()){
			 			contractLaboursDO.setDob(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.DOB).toString()));
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.DOJ) != null && !inputJSON.get(CommonConstants.DOJ).toString().isEmpty()){
			 			contractLaboursDO.setDoj(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.DOJ).toString()));
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.FATHERNAME) != null && !inputJSON.get(CommonConstants.FATHERNAME).toString().isEmpty()){
			 			contractLaboursDO.setFatherName(inputJSON.get(CommonConstants.FATHERNAME).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.SPOUSENAME) != null && !inputJSON.get(CommonConstants.SPOUSENAME).toString().isEmpty()){
			 			contractLaboursDO.setSpouseName(inputJSON.get(CommonConstants.SPOUSENAME).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.GENDER) != null && !inputJSON.get(CommonConstants.GENDER).toString().isEmpty()){
			 			contractLaboursDO.setGender(inputJSON.get(CommonConstants.GENDER).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.MARITALSTATUS) != null && !inputJSON.get(CommonConstants.MARITALSTATUS).toString().isEmpty()){
			 			contractLaboursDO.setMaritalStatus(inputJSON.get(CommonConstants.MARITALSTATUS).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.CAT) != null && !inputJSON.get(CommonConstants.CAT).toString().isEmpty()){
			 			contractLaboursDO.setCat(inputJSON.get(CommonConstants.CAT).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.BLOODGROUP) != null && !inputJSON.get(CommonConstants.BLOODGROUP).toString().isEmpty()){
			 			contractLaboursDO.setBloodGroup(inputJSON.get(CommonConstants.BLOODGROUP).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.QUALIFICATION) != null && !inputJSON.get(CommonConstants.QUALIFICATION).toString().isEmpty()){
			 			contractLaboursDO.setQualification(inputJSON.get(CommonConstants.QUALIFICATION).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.WAGES) != null && !inputJSON.get(CommonConstants.WAGES).toString().isEmpty()){
			 			contractLaboursDO.setWages(Long.parseLong(inputJSON.get(CommonConstants.WAGES).toString()));
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.CONTACTNO) != null && !inputJSON.get(CommonConstants.CONTACTNO).toString().isEmpty()){
			 			contractLaboursDO.setContactNo(Long.parseLong(inputJSON.get(CommonConstants.CONTACTNO).toString()));
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.EMERGENCY_CONTACTNO) != null && !inputJSON.get(CommonConstants.EMERGENCY_CONTACTNO).toString().isEmpty()){
			 			contractLaboursDO.setEmergencyContactNo(Long.parseLong(inputJSON.get(CommonConstants.EMERGENCY_CONTACTNO).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.PREVIOUSEXPERIENCE) != null && !inputJSON.get(CommonConstants.PREVIOUSEXPERIENCE).toString().isEmpty()){
			 			contractLaboursDO.setPreviousExperience(Long.parseLong(inputJSON.get(CommonConstants.PREVIOUSEXPERIENCE).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.BANKNAME) != null && !inputJSON.get(CommonConstants.BANKNAME).toString().isEmpty()){
			 			contractLaboursDO.setBankName(inputJSON.get(CommonConstants.BANKNAME).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.IFSC) != null && !inputJSON.get(CommonConstants.IFSC).toString().isEmpty()){
			 			contractLaboursDO.setIfsc(inputJSON.get(CommonConstants.IFSC).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.BANKACNO) != null && !inputJSON.get(CommonConstants.BANKACNO).toString().isEmpty()){
			 			contractLaboursDO.setBankAcNo(Long.parseLong(inputJSON.get(CommonConstants.BANKACNO).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.AADHARNO) != null && !inputJSON.get(CommonConstants.AADHARNO).toString().isEmpty()){
			 			contractLaboursDO.setAadharNo(Long.parseLong(inputJSON.get(CommonConstants.AADHARNO).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.PAN_NO) != null && !inputJSON.get(CommonConstants.PAN_NO).toString().isEmpty()){
			 			contractLaboursDO.setPanNo(inputJSON.get(CommonConstants.PAN_NO).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.PRESENTADDRESS) != null && !inputJSON.get(CommonConstants.PRESENTADDRESS).toString().isEmpty()){
			 			contractLaboursDO.setPresentAddress(inputJSON.get(CommonConstants.PRESENTADDRESS).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.PERMANENTADDRESS) != null && !inputJSON.get(CommonConstants.PERMANENTADDRESS).toString().isEmpty()){
			 			contractLaboursDO.setPermanentAddress(inputJSON.get(CommonConstants.PERMANENTADDRESS).toString());
			 		}
			 		
			 		contractLaboursDO.setUpdatedon(new Date());
			 		contractLaboursDO.setCreatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			contractLaboursDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			contractLaboursDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 	}
				if(!contractLaboursService.persist(contractLaboursDO)){
					return CommonWebUtil.buildErrorResponse("Contract Labour Details Added").toString();
				}
				CommonUtil.persistRecentAcvtivity(inputJSON.get(CommonConstants.UPDATED_BY).toString(), "New Contract Labour Details Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	/*@RequestMapping(value = "/retrieveActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<AddressTypeDO> addressTypeList = addressTypeService.retrieveActive();
				respJSON = AddressTypeUtil.getAddressTypeList(addressTypeList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}*/
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<ContractEmployeeDO> contractLaboursList = contractLaboursService.retrieve();
				respJSON = ContractEmployeeUtil.getContractLabourList(contractLaboursList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				ContractEmployeeDO contractLaboursDO = new ContractEmployeeDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<ContractEmployeeDO> contractLaboursList = contractLaboursService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(contractLaboursList != null && contractLaboursList.size() > 0){
			 			contractLaboursDO = contractLaboursList.get(0);
			 			if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
				 			contractLaboursDO.setName(inputJSON.get(CommonConstants.NAME).toString());
				 		}
				 		
				 		if(inputJSON.get(CommonConstants.SUPPLIERID) != null && !inputJSON.get(CommonConstants.SUPPLIERID).toString().isEmpty()){
				 			List<SuppliersDO> suppliersList = supplierService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.SUPPLIERID).toString()));
				 			if(suppliersList != null && suppliersList.size() > 0){
				 				contractLaboursDO.setSupplier(suppliersList.get(0));
				 			}
			 			}
				 		
				 		if(inputJSON.get(CommonConstants.EMPID) != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
				 			contractLaboursDO.setEmpId(inputJSON.get(CommonConstants.EMPID).toString());
			 			}
				 		
				 		if(inputJSON.get(CommonConstants.DESIGNATION) != null && !inputJSON.get(CommonConstants.DESIGNATION).toString().isEmpty()){
				 			List<DesignationDO> designationList = designationService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.DESIGNATION).toString()));
				 			if(designationList != null && designationList.size() > 0){
				 				contractLaboursDO.setDesignation(designationList.get(0));
				 			}
			 			}
				 		
				 		if(inputJSON.get(CommonConstants.DEPARTMENT) != null && !inputJSON.get(CommonConstants.DEPARTMENT).toString().isEmpty()){
				 			List<DepartmentDO> deptList = departmentService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.DEPARTMENT).toString()));
				 			if(deptList != null && deptList.size() > 0){
				 				contractLaboursDO.setDepartment(deptList.get(0));
				 			}
			 			}
				 		
				 		if(inputJSON.get(CommonConstants.DOB) != null && !inputJSON.get(CommonConstants.DOB).toString().isEmpty()){
				 			contractLaboursDO.setDob(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.DOB).toString()));
				 		}
				 		
				 		if(inputJSON.get(CommonConstants.DOJ) != null && !inputJSON.get(CommonConstants.DOJ).toString().isEmpty()){
				 			contractLaboursDO.setDoj(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.DOJ).toString()));
				 		}
				 		
				 		if(inputJSON.get(CommonConstants.FATHERNAME) != null && !inputJSON.get(CommonConstants.FATHERNAME).toString().isEmpty()){
				 			contractLaboursDO.setFatherName(inputJSON.get(CommonConstants.FATHERNAME).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.SPOUSENAME) != null && !inputJSON.get(CommonConstants.SPOUSENAME).toString().isEmpty()){
				 			contractLaboursDO.setSpouseName(inputJSON.get(CommonConstants.SPOUSENAME).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.GENDER) != null && !inputJSON.get(CommonConstants.GENDER).toString().isEmpty()){
				 			contractLaboursDO.setGender(inputJSON.get(CommonConstants.GENDER).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.MARITALSTATUS) != null && !inputJSON.get(CommonConstants.MARITALSTATUS).toString().isEmpty()){
				 			contractLaboursDO.setMaritalStatus(inputJSON.get(CommonConstants.MARITALSTATUS).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.CAT) != null && !inputJSON.get(CommonConstants.CAT).toString().isEmpty()){
				 			contractLaboursDO.setCat(inputJSON.get(CommonConstants.CAT).toString());
				 		}
				 		
				 		if(inputJSON.get(CommonConstants.BLOODGROUP) != null && !inputJSON.get(CommonConstants.BLOODGROUP).toString().isEmpty()){
				 			contractLaboursDO.setBloodGroup(inputJSON.get(CommonConstants.BLOODGROUP).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.QUALIFICATION) != null && !inputJSON.get(CommonConstants.QUALIFICATION).toString().isEmpty()){
				 			contractLaboursDO.setQualification(inputJSON.get(CommonConstants.QUALIFICATION).toString());
				 		}
				 		
				 		if(inputJSON.get(CommonConstants.WAGES) != null && !inputJSON.get(CommonConstants.WAGES).toString().isEmpty()){
				 			contractLaboursDO.setWages(Long.parseLong(inputJSON.get(CommonConstants.WAGES).toString()));
				 		}
				 		
				 		if(inputJSON.get(CommonConstants.CONTACTNO) != null && !inputJSON.get(CommonConstants.CONTACTNO).toString().isEmpty()){
				 			contractLaboursDO.setContactNo(Long.parseLong(inputJSON.get(CommonConstants.CONTACTNO).toString()));
				 		}
				 		
				 		if(inputJSON.get(CommonConstants.EMERGENCY_CONTACTNO) != null && !inputJSON.get(CommonConstants.EMERGENCY_CONTACTNO).toString().isEmpty()){
				 			contractLaboursDO.setEmergencyContactNo(Long.parseLong(inputJSON.get(CommonConstants.EMERGENCY_CONTACTNO).toString()));
				 		}
				 		if(inputJSON.get(CommonConstants.PREVIOUSEXPERIENCE) != null && !inputJSON.get(CommonConstants.PREVIOUSEXPERIENCE).toString().isEmpty()){
				 			contractLaboursDO.setPreviousExperience(Long.parseLong(inputJSON.get(CommonConstants.PREVIOUSEXPERIENCE).toString()));
				 		}
				 		if(inputJSON.get(CommonConstants.BANKNAME) != null && !inputJSON.get(CommonConstants.BANKNAME).toString().isEmpty()){
				 			contractLaboursDO.setBankName(inputJSON.get(CommonConstants.BANKNAME).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.IFSC) != null && !inputJSON.get(CommonConstants.IFSC).toString().isEmpty()){
				 			contractLaboursDO.setIfsc(inputJSON.get(CommonConstants.IFSC).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.BANKACNO) != null && !inputJSON.get(CommonConstants.BANKACNO).toString().isEmpty()){
				 			contractLaboursDO.setBankAcNo(Long.parseLong(inputJSON.get(CommonConstants.BANKACNO).toString()));
				 		}
				 		if(inputJSON.get(CommonConstants.AADHARNO) != null && !inputJSON.get(CommonConstants.AADHARNO).toString().isEmpty()){
				 			contractLaboursDO.setAadharNo(Long.parseLong(inputJSON.get(CommonConstants.AADHARNO).toString()));
				 		}
				 		if(inputJSON.get(CommonConstants.PAN_NO) != null && !inputJSON.get(CommonConstants.PAN_NO).toString().isEmpty()){
				 			contractLaboursDO.setPanNo(inputJSON.get(CommonConstants.PAN_NO).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.PRESENTADDRESS) != null && !inputJSON.get(CommonConstants.PRESENTADDRESS).toString().isEmpty()){
				 			contractLaboursDO.setPresentAddress(inputJSON.get(CommonConstants.PRESENTADDRESS).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.PERMANENTADDRESS) != null && !inputJSON.get(CommonConstants.PERMANENTADDRESS).toString().isEmpty()){
				 			contractLaboursDO.setPermanentAddress(inputJSON.get(CommonConstants.PERMANENTADDRESS).toString());
				 		}				 		contractLaboursDO.setUpdatedon(new Date());
				 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			contractLaboursDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		if(!contractLaboursService.update(contractLaboursDO)){
							return CommonWebUtil.buildErrorResponse("Details Already Added").toString();
						}
				 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Contract Employee Updated");
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	/*@RequestMapping(value = "/importAddressType", method = RequestMethod.POST)
	public @ResponseBody String importAddressType(Model model, HttpServletRequest request) {
		try {
			JSONObject inputJSON =  CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
			JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
			List<AddressTypeDO> AddressTypeList = new ArrayList<AddressTypeDO>();
			Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY);
			for (int i=0; i < fileData.size(); i++){
				JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
				AddressTypeDO addressTypeDO = new AddressTypeDO();
				if (colName.get(CommonConstants.NAME) != null && !colName.get(CommonConstants.NAME).toString().isEmpty()){
					addressTypeDO.setName(rowJSON.get(colName.get(CommonConstants.NAME)).toString());
		 		}
		 		if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
		 			addressTypeDO.setUpdatedby(updatedBy.toString());
		 			addressTypeDO.setCreatedby(updatedBy.toString());
		 		}
		 		addressTypeDO.setUpdatedon(new Date());
		 		addressTypeDO.setCreatedon(new Date());
		 		addressTypeDO.setStatus(CommonConstants.ACTIVE);
		 		AddressTypeList.add(addressTypeDO);
			}
			addressTypeService.persistList(AddressTypeList);
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}*/
}
