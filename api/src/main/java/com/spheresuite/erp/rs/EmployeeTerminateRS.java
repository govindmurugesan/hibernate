package com.spheresuite.erp.rs;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.EmployeeTerminateDO;
import com.spheresuite.erp.domainobject.TerminateReasonTypeDO;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.EmployeeTerminateService;
import com.spheresuite.erp.service.TerminateReasonTypeService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.EmployeeTerminateUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/employeeterminate")
public class EmployeeTerminateRS {

	String validation = null;
	static Logger logger = Logger.getLogger(EmployeeTerminateRS.class.getName());
	@Autowired
	private EmployeeService employeeService;
	
	@Autowired
	private EmployeeTerminateService employeeTerminateService;

	@Autowired
	private TerminateReasonTypeService terminateReasonTypeService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeTerminateDO employeeTerminateDO = new EmployeeTerminateDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.EMPID).toString() != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
				 		if(employeeList != null && employeeList.size() > 0){
				 			employeeTerminateDO.setEmployee(employeeList.get(0));
				 			if(inputJSON.get(CommonConstants.TERMINATETYPEID).toString() != null && !inputJSON.get(CommonConstants.TERMINATETYPEID).toString().isEmpty()){
					 			List<TerminateReasonTypeDO> terminateReasonTypeList = terminateReasonTypeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.TERMINATETYPEID).toString()));
						 		if(terminateReasonTypeList != null && terminateReasonTypeList.size() > 0){
						 			employeeTerminateDO.setTerminateReason(terminateReasonTypeList.get(0));
						 			if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
							 			employeeTerminateDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
							 			employeeTerminateDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
							 			if(inputJSON.get(CommonConstants.COMMENT) != null && !inputJSON.get(CommonConstants.COMMENT).toString().isEmpty()){
								 			employeeTerminateDO.setComments(inputJSON.get(CommonConstants.COMMENT).toString());
								 		}
								 		if(inputJSON.get(CommonConstants.TERMINATEDATE) != null && !inputJSON.get(CommonConstants.TERMINATEDATE).toString().isEmpty()){
								 			employeeTerminateDO.setTerminateDate( CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.TERMINATEDATE).toString()));
								 		}
								 		if(inputJSON.get(CommonConstants.RESIGNEDDATE) != null && !inputJSON.get(CommonConstants.RESIGNEDDATE).toString().isEmpty()){
								 			employeeTerminateDO.setResignDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.RESIGNEDDATE).toString())); 
								 		}
								 		employeeTerminateDO.setUpdatedon(new Date());
								 		employeeTerminateDO.setCreatedon(new Date());
								 		List<EmployeeDO> employeeList2 = employeeService.retriveByEmpId(employeeTerminateDO.getEmployee().getEmpId().toString());
										if(employeeList2 != null && employeeList2.size() > 0){
											EmployeeDO employee = employeeList2.get(0);
											employee.setStatus(CommonConstants.TERMINATED);
											employeeService.update(employee);
										}
										CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Employee  Terminated");
						 			}
						 		}
					 		}
				 		}
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveByEmpId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<EmployeeTerminateDO> addressTypeList = employeeTerminateService.retrieveByEmpId(inputJSON.get(CommonConstants.ID).toString());
					respJSON = EmployeeTerminateUtil.getEmployeeTerminateList(addressTypeList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<EmployeeTerminateDO> addressTypeList = employeeTerminateService.retrieve();
				respJSON = EmployeeTerminateUtil.getEmployeeTerminateList(addressTypeList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeTerminateDO employeeTerminateDO = new EmployeeTerminateDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<EmployeeTerminateDO> addressTypeList = employeeTerminateService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		employeeTerminateDO = addressTypeList.get(0);
			 		if(inputJSON.get(CommonConstants.COMMENT) != null && !inputJSON.get(CommonConstants.COMMENT).toString().isEmpty()){
			 			employeeTerminateDO.setComments(inputJSON.get(CommonConstants.COMMENT).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.TERMINATEDATE) != null && !inputJSON.get(CommonConstants.TERMINATEDATE).toString().isEmpty()){
			 			employeeTerminateDO.setTerminateDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.TERMINATEDATE).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.RESIGNEDDATE) != null && !inputJSON.get(CommonConstants.RESIGNEDDATE).toString().isEmpty()){
			 			employeeTerminateDO.setResignDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.RESIGNEDDATE).toString())); 
			 		}
			 		if(inputJSON.get(CommonConstants.TERMINATETYPEID).toString() != null && !inputJSON.get(CommonConstants.TERMINATETYPEID).toString().isEmpty()){
			 			List<TerminateReasonTypeDO> terminateReasonTypeList = terminateReasonTypeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.TERMINATETYPEID).toString()));
				 		if(terminateReasonTypeList != null && terminateReasonTypeList.size() > 0){
				 			employeeTerminateDO.setTerminateReason(terminateReasonTypeList.get(0));
				 		}
			 		}
			 		if(inputJSON.get(CommonConstants.EMPID).toString() != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
				 		if(employeeList != null && employeeList.size() > 0){
				 			employeeTerminateDO.setEmployee(employeeList.get(0));
				 		}
			 		}
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			employeeTerminateDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		employeeTerminateDO.setUpdatedon(new Date());
			 		employeeTerminateService.update(employeeTerminateDO);
			 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Employee Updated");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public @ResponseBody String delete(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		employeeTerminateService.delete(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		return CommonWebUtil.buildSuccessResponse().toString();
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
