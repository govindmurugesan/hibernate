package com.spheresuite.erp.rs;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.AddressTypeDO;
import com.spheresuite.erp.domainobject.CountryDO;
import com.spheresuite.erp.domainobject.EmployeeAddressDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.StateDO;
import com.spheresuite.erp.service.AddressTypeService;
import com.spheresuite.erp.service.CountryService;
import com.spheresuite.erp.service.EmployeeAddressService;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.StateService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.EmployeeAddressUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/employeeaddress")
public class EmployeeAddressRS {

	String validation = null;
	static Logger logger = Logger.getLogger(EmployeeAddressRS.class.getName());

	@Autowired
	private EmployeeAddressService employeeAddressService;
	
	@Autowired
	private CountryService countryService;
	
	@Autowired
	private StateService stateService;
	
	@Autowired
	private AddressTypeService addressTypeService;
	
	@Autowired
	private EmployeeService employeeService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeAddressDO employeeAddressDO = new EmployeeAddressDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.COUNTRY_ID) != null && !inputJSON.get(CommonConstants.COUNTRY_ID).toString().isEmpty()){
				 		List<CountryDO> countryList = countryService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.COUNTRY_ID).toString()));
			 			if(countryList != null && countryList.size() > 0){
			 				employeeAddressDO.setCountry(countryList.get(0));
			 				if(inputJSON.get(CommonConstants.STATE_ID) != null && !inputJSON.get(CommonConstants.STATE_ID).toString().isEmpty()){
						 		List<StateDO> stateList = stateService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.STATE_ID).toString()));
						 		if(stateList != null && stateList.size() >0){
						 			employeeAddressDO.setState(stateList.get(0));
						 			if(inputJSON.get(CommonConstants.ADDRESSTYPEID) != null && !inputJSON.get(CommonConstants.ADDRESSTYPEID).toString().isEmpty()){
							 			List<AddressTypeDO> addressTypeList = addressTypeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ADDRESSTYPEID).toString()));
								 		if(addressTypeList != null && addressTypeList.size() > 0){
								 			employeeAddressDO.setAddressType(addressTypeList.get(0));
								 			if(inputJSON.get(CommonConstants.EMPID).toString() != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
									 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
										 		if(employeeList != null && employeeList.size() > 0){
										 			employeeAddressDO.setEmployee(employeeList.get(0));
										 			if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
											 			employeeAddressDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
											 			employeeAddressDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
											 			if(inputJSON.get(CommonConstants.CITY) != null && !inputJSON.get(CommonConstants.CITY).toString().isEmpty()){
												 			employeeAddressDO.setCity(inputJSON.get(CommonConstants.CITY).toString());
												 			if(inputJSON.get(CommonConstants.NOTE) != null && !inputJSON.get(CommonConstants.NOTE).toString().isEmpty()){
													 			employeeAddressDO.setNotes(inputJSON.get(CommonConstants.NOTE).toString());
													 		}
													 		if(inputJSON.get(CommonConstants.ZIP) != null && !inputJSON.get(CommonConstants.ZIP).toString().isEmpty()){
													 			employeeAddressDO.setZip(Long.parseLong(inputJSON.get(CommonConstants.ZIP).toString()));
													 		}
													 		if(inputJSON.get(CommonConstants.ADDRESS1) != null && !inputJSON.get(CommonConstants.ADDRESS1).toString().isEmpty()){
													 			employeeAddressDO.setAddress1(inputJSON.get(CommonConstants.ADDRESS1).toString());
													 		}
													 		if(inputJSON.get(CommonConstants.ADDRESS2) != null && !inputJSON.get(CommonConstants.ADDRESS2).toString().isEmpty()){
													 			employeeAddressDO.setAddress2(inputJSON.get(CommonConstants.ADDRESS2).toString());
													 		}
													 		employeeAddressDO.setUpdatedon(new Date());
													 		employeeAddressService.persist(employeeAddressDO);
															CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Emplyee Address Created");
											 			}
										 			}
										 			
										 		}
									 		}
								 		}
							 		}
						 		}
					 		}
			 			}
			 		}
			 	}
				
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveByEmpId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByEmpId(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<EmployeeAddressDO> employeeAddressList = employeeAddressService.retrieveByEmpId(inputJSON.get(CommonConstants.ID).toString());
					respJSON = EmployeeAddressUtil.getEmployeeAddressList(employeeAddressList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeAddressDO employeeAddressDO = new EmployeeAddressDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<EmployeeAddressDO> employeeAddressList = employeeAddressService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		employeeAddressDO = employeeAddressList.get(0);
			 		if(inputJSON.get(CommonConstants.CITY) != null && !inputJSON.get(CommonConstants.CITY).toString().isEmpty()){
			 			employeeAddressDO.setCity(inputJSON.get(CommonConstants.CITY).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.COUNTRY_ID) != null && !inputJSON.get(CommonConstants.COUNTRY_ID).toString().isEmpty()){
				 		List<CountryDO> countryList = countryService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.COUNTRY_ID).toString()));
			 			if(countryList != null && countryList.size() > 0){
			 				employeeAddressDO.setCountry(countryList.get(0));
			 			}
			 		}
			 		if(inputJSON.get(CommonConstants.STATE_ID) != null && !inputJSON.get(CommonConstants.STATE_ID).toString().isEmpty()){
				 		List<StateDO> stateList = stateService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.STATE_ID).toString()));
				 		if(stateList != null && stateList.size() >0){
				 			employeeAddressDO.setState(stateList.get(0));
				 		}
			 		}
			 		if(inputJSON.get(CommonConstants.ADDRESSTYPEID) != null && !inputJSON.get(CommonConstants.ADDRESSTYPEID).toString().isEmpty()){
			 			List<AddressTypeDO> addressTypeList = addressTypeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ADDRESSTYPEID).toString()));
				 		if(addressTypeList != null && addressTypeList.size() > 0){
				 			employeeAddressDO.setAddressType(addressTypeList.get(0));
				 		}
			 		}
			 		if(inputJSON.get(CommonConstants.EMPID).toString() != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
				 		if(employeeList != null && employeeList.size() > 0){
				 			employeeAddressDO.setEmployee(employeeList.get(0));
				 		}
			 		}
			 		if(inputJSON.get(CommonConstants.NOTE) != null && !inputJSON.get(CommonConstants.NOTE).toString().isEmpty()){
			 			employeeAddressDO.setNotes(inputJSON.get(CommonConstants.NOTE).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.ZIP) != null && !inputJSON.get(CommonConstants.ZIP).toString().isEmpty()){
			 			employeeAddressDO.setZip(Long.parseLong(inputJSON.get(CommonConstants.ZIP).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.ADDRESS1) != null && !inputJSON.get(CommonConstants.ADDRESS1).toString().isEmpty()){
			 			employeeAddressDO.setAddress1(inputJSON.get(CommonConstants.ADDRESS1).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.ADDRESS2) != null && !inputJSON.get(CommonConstants.ADDRESS2).toString().isEmpty()){
			 			employeeAddressDO.setAddress2(inputJSON.get(CommonConstants.ADDRESS2).toString());
			 		}
			 		employeeAddressDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			employeeAddressDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		employeeAddressService.update(employeeAddressDO);
			 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Allowance Type Updated");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
