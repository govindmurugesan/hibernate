package com.spheresuite.erp.rs;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.auth0.jwt.JWTVerifyException;
import com.spheresuite.erp.domainobject.HolidaysDO;
import com.spheresuite.erp.domainobject.NotificationSettingsDO;
import com.spheresuite.erp.domainobject.WorkLocationDO;
import com.spheresuite.erp.service.HolidaysService;
import com.spheresuite.erp.service.NotificationSettingsService;
import com.spheresuite.erp.service.WorkLocationService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.HolidaysUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/holidays")
public class HolidaysRS {

	String validation = null;
	static Logger logger = Logger.getLogger(HolidaysRS.class.getName());

	@Autowired
	private HolidaysService holidaysService;
	
	@Autowired
	private NotificationSettingsService notificationSettingsService;
	
	@Autowired
	private WorkLocationService worklocationService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		//inputJSON.get(CommonConstants.DESC).toString(), 
			 		List<HolidaysDO> holidayList = holidaysService.retrieveByHoliday(inputJSON.get(CommonConstants.YEAR).toString(), inputJSON.get(CommonConstants.DATE).toString());
			 		if(holidayList != null && holidayList.size() > 0){
			 			return CommonWebUtil.buildErrorResponse("Holiday Already Exist").toString();
			 		}else{
			 			HolidaysDO holidayDO = new HolidaysDO(); 
			 			if(inputJSON.get(CommonConstants.YEAR) != null && !inputJSON.get(CommonConstants.YEAR).toString().isEmpty()){
			 				holidayDO.setYear(inputJSON.get(CommonConstants.YEAR).toString());
			 			}
			 			if(inputJSON.get(CommonConstants.DATE) != null && !inputJSON.get(CommonConstants.DATE).toString().isEmpty()){
			 				holidayDO.setLeavedate(inputJSON.get(CommonConstants.DATE).toString());
			 			}
			 			if(inputJSON.get(CommonConstants.DESC) != null && !inputJSON.get(CommonConstants.DESC).toString().isEmpty()){
			 				holidayDO.setDescription(inputJSON.get(CommonConstants.DESC).toString());
			 			}
			 			//holidayDO.setStatus(CommonConstants.ACTIVE);
			 			if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
			 				holidayDO.setStatus((char)  inputJSON.get(CommonConstants.STATUS).toString().charAt(0));
			 			}
			 			if(inputJSON.get(CommonConstants.WORKLOCATION) != null && !inputJSON.get(CommonConstants.WORKLOCATION).toString().isEmpty()){
				 			List<WorkLocationDO> worklocationList = worklocationService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.WORKLOCATION).toString()));
				 			if(worklocationList != null && worklocationList.size() > 0){
				 				holidayDO.setWorklocation(worklocationList.get(0));
				 			}
			 			}
			 			if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 				holidayDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 				holidayDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
			 			holidayDO.setCreatedon(new Date());
				 		holidayDO.setUpdatedon(new Date());
			 			holidaysService.persist(holidayDO);
			 			CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Holiday created");
			 			CommonUtil.sendMailToReportingHeads(request,inputJSON,"holidays/manage","holiday","pending");
			 		}
			 	}
			}else{
		 		return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
		 	}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) throws InvalidKeyException, NoSuchAlgorithmException, IllegalStateException, SignatureException, IOException, JWTVerifyException {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<HolidaysDO> hoildayList = holidaysService.retrieve();
				respJSON = HolidaysUtil.getHoildaysList(hoildayList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && inputJSON.get(CommonConstants.ID) != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<HolidaysDO> holidayList = holidaysService.retrieveByHolidayForUpdate(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()),
			 				inputJSON.get(CommonConstants.YEAR).toString(), inputJSON.get(CommonConstants.DATE).toString());
			 		if(holidayList != null && holidayList.size() > 0){
			 			return CommonWebUtil.buildErrorResponse("Holiday Already Exist").toString();
			 		}
	 				HolidaysDO hoildayDO = new HolidaysDO(); 
	 				List<HolidaysDO> hoildayList = holidaysService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
	 				if(hoildayList != null && hoildayList.size() > 0){
	 					hoildayDO = hoildayList.get(0);
	 					if(inputJSON.get(CommonConstants.YEAR) != null && !inputJSON.get(CommonConstants.YEAR).toString().isEmpty()){
	 						hoildayDO.setYear(inputJSON.get(CommonConstants.YEAR).toString());
			 			}
			 			if(inputJSON.get(CommonConstants.DATE) != null && !inputJSON.get(CommonConstants.DATE).toString().isEmpty()){
			 				hoildayDO.setLeavedate(inputJSON.get(CommonConstants.DATE).toString());
			 			}
			 			if(inputJSON.get(CommonConstants.DESC) != null && !inputJSON.get(CommonConstants.DESC).toString().isEmpty()){
			 				hoildayDO.setDescription(inputJSON.get(CommonConstants.DESC).toString());
			 			}
			 			if(inputJSON.get(CommonConstants.WORKLOCATION) != null && !inputJSON.get(CommonConstants.WORKLOCATION).toString().isEmpty()){
				 			List<WorkLocationDO> worklocationList = worklocationService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.WORKLOCATION).toString()));
				 			if(worklocationList != null && worklocationList.size() > 0){
				 				hoildayDO.setWorklocation(worklocationList.get(0));
				 			}
			 			}
			 			if(inputJSON.get(CommonConstants.COMMENT) != null && !inputJSON.get(CommonConstants.COMMENT).toString().isEmpty()){
			 				hoildayDO.setComment(inputJSON.get(CommonConstants.COMMENT).toString());
			 			}
		 				hoildayDO.setUpdatedon(new Date());
		 				if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
		 					hoildayDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
		 				if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
		 					hoildayDO.setStatus((char)  inputJSON.get(CommonConstants.STATUS).toString().charAt(0));
			 			}
		 				holidaysService.update(hoildayDO);
		 				if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty() && inputJSON.get(CommonConstants.STATUS).toString().equalsIgnoreCase("a")){
		 					CommonUtil.sendMailToReportingHeads(request,inputJSON,"/holiday","holiday","approved");
			 			}
		 				if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty() && inputJSON.get(CommonConstants.STATUS).toString().equalsIgnoreCase("p")){
		 					CommonUtil.sendMailToReportingHeads(request,inputJSON,"/holidays/manage","holiday","pending");
			 			}
		 				if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty() && inputJSON.get(CommonConstants.STATUS).toString().equalsIgnoreCase("r")){
		 					CommonUtil.sendMailToReportingHeads(request,inputJSON,"/holidays/manage","holiday","reject");
			 			}
		 				
		 				
		 				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Holiday Updated");
	 				}
			 	}
		 	}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retrieveByApproverId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByApproverId(@PathVariable String inputParams, Model model, HttpServletRequest request)  {
		String respJSON = null;
		Boolean isApprover = false;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<NotificationSettingsDO> notificationSettingsList = notificationSettingsService.retrieve();
	 				if(notificationSettingsList != null && notificationSettingsList.size() >0){
	 					if(notificationSettingsList.get(0).getHolidays() != null){
	 						List<String> hoildaysList = Arrays.asList(notificationSettingsList.get(0).getHolidays().split(CommonConstants.COMMA));
	 						if(hoildaysList !=null && hoildaysList.size() > 0){
	 							for(String approver : hoildaysList ){
	 								if(approver.equalsIgnoreCase(inputJSON.get(CommonConstants.ID).toString())){
	 									isApprover = true;
	 									List<HolidaysDO> hoildayList = holidaysService.retrieve();
	 									respJSON = HolidaysUtil.getHoildaysList(hoildayList).toString();
	 								}
	 							}
	 							if(!isApprover){
	 								respJSON = HolidaysUtil.notApproverResponse().toString();
	 							}
	 						}
	 					}
	 				}
				}else{
					return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveByDate/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByDate(@PathVariable String inputParams, Model model, HttpServletRequest request)  {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && inputJSON.get(CommonConstants.YEAR) != null && !inputJSON.get(CommonConstants.YEAR).toString().isEmpty() && inputJSON.get(CommonConstants.MONTH) != null && !inputJSON.get(CommonConstants.MONTH).toString().isEmpty()){
					List<HolidaysDO> hoildayList = holidaysService.retrieveByDate(inputJSON.get(CommonConstants.YEAR).toString(), inputJSON.get(CommonConstants.MONTH).toString());
					respJSON = HolidaysUtil.getHoildaysList(hoildayList).toString();
				}else{
					return CommonWebUtil.buildErrorResponse("").toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
}
