package com.spheresuite.erp.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.CountryDO;
import com.spheresuite.erp.domainobject.PtDO;
import com.spheresuite.erp.domainobject.StateDO;
import com.spheresuite.erp.domainobject.WorkLocationDO;
import com.spheresuite.erp.service.CountryService;
import com.spheresuite.erp.service.PtService;
import com.spheresuite.erp.service.StateService;
import com.spheresuite.erp.service.WorkLocationService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.PtUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/pt")
public class PtRS {

	String validation = null;
	static Logger logger = Logger.getLogger(PtRS.class.getName());

	@Autowired
	private PtService ptService;
	
	@Autowired
	private CountryService countryService;
	
	@Autowired
	private StateService stateService;
	
	@Autowired
	private WorkLocationService worklocationService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				PtDO ptDO = new PtDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.COUNTRY_ID) != null && !inputJSON.get(CommonConstants.COUNTRY_ID).toString().isEmpty()){
			 			List<CountryDO> countryList = countryService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.COUNTRY_ID).toString()));
			 			if(countryList != null && countryList.size() > 0){
			 				ptDO.setCountry(countryList.get(0));
			 				if(inputJSON.get(CommonConstants.STATE_ID) != null && !inputJSON.get(CommonConstants.STATE_ID).toString().isEmpty()){
					 			List<StateDO> stateList = stateService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.STATE_ID).toString()));
					 			if(stateList != null && stateList.size() > 0){
					 				ptDO.setState(stateList.get(0));
					 				if(inputJSON.get(CommonConstants.WORKLOCATION) != null && !inputJSON.get(CommonConstants.WORKLOCATION).toString().isEmpty()){
							 			List<WorkLocationDO> worklocationList = worklocationService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.WORKLOCATION).toString()));
							 			if(worklocationList != null && worklocationList.size() > 0){
							 				ptDO.setWorklocation(worklocationList.get(0));
							 			}
						 			}
					 				if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
							 			ptDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
							 			ptDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
							 			if(inputJSON.get(CommonConstants.FROMAMOUNT) != null && !inputJSON.get(CommonConstants.FROMAMOUNT).toString().isEmpty()){
								 			ptDO.setFromamount(Long.parseLong(inputJSON.get(CommonConstants.FROMAMOUNT).toString()));
								 		}
								 		if(inputJSON.get(CommonConstants.TOAMOUNT) != null && !inputJSON.get(CommonConstants.TOAMOUNT).toString().isEmpty()){
								 			ptDO.setToamount(Long.parseLong(inputJSON.get(CommonConstants.TOAMOUNT).toString()));
								 		}
								 		if(inputJSON.get(CommonConstants.PTAMOUNT) != null && !inputJSON.get(CommonConstants.PTAMOUNT).toString().isEmpty()){
								 			ptDO.setPtamount(Long.parseLong(inputJSON.get(CommonConstants.PTAMOUNT).toString()));
								 		}
								 		ptDO.setUpdatedon(new Date());
								 		ptDO.setCreatedon(new Date());
								 		ptDO.setStatus(CommonConstants.ACTIVE);
								 		if(!ptService.persist(ptDO)){
											return CommonWebUtil.buildErrorResponse("Professional Tax Already Added").toString();
										}
										CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Professional Tax Slab Created");
							 		}

					 			}
				 			}
			 			}
		 			}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retriveByState/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveById(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<PtDO> ptDOList = ptService.retrieveByState(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = PtUtil.getPtList(ptDOList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<PtDO> ptDOList = ptService.retrieveActive();
				respJSON = PtUtil.getPtList(ptDOList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<PtDO> ptDOList = ptService.retrieve();
				respJSON = PtUtil.getPtList(ptDOList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				PtDO ptDO = new PtDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<PtDO> ptList = ptService.retrieveActiveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(ptList != null && ptList.size() > 0){
			 			ptDO = ptList.get(0);
				 		if(inputJSON.get(CommonConstants.FROMAMOUNT) != null && !inputJSON.get(CommonConstants.FROMAMOUNT).toString().isEmpty()){
				 			ptDO.setFromamount(Long.parseLong(inputJSON.get(CommonConstants.FROMAMOUNT).toString()));
				 		}
				 		if(inputJSON.get(CommonConstants.TOAMOUNT) != null && !inputJSON.get(CommonConstants.TOAMOUNT).toString().isEmpty()){
				 			ptDO.setToamount(Long.parseLong(inputJSON.get(CommonConstants.TOAMOUNT).toString()));
				 		}
				 		if(inputJSON.get(CommonConstants.PTAMOUNT) != null && !inputJSON.get(CommonConstants.PTAMOUNT).toString().isEmpty()){
				 			ptDO.setPtamount(Long.parseLong(inputJSON.get(CommonConstants.PTAMOUNT).toString()));
				 		}
				 		
				 		if(inputJSON.get(CommonConstants.COUNTRY_ID) != null && !inputJSON.get(CommonConstants.COUNTRY_ID).toString().isEmpty()){
				 			List<CountryDO> countryList = countryService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.COUNTRY_ID).toString()));
				 			if(countryList != null && countryList.size() > 0){
				 				ptDO.setCountry(countryList.get(0));
				 			}
			 			}
				 		if(inputJSON.get(CommonConstants.STATE_ID) != null && !inputJSON.get(CommonConstants.STATE_ID).toString().isEmpty()){
				 			List<StateDO> stateList = stateService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.STATE_ID).toString()));
				 			if(stateList != null && stateList.size() > 0){
				 				ptDO.setState(stateList.get(0));
				 			}
			 			}
				 		if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
				 			ptDO.setStatus((char) (inputJSON.get(CommonConstants.STATUS).toString().charAt(0)));
				 		}
				 		ptDO.setUpdatedon(new Date());
				 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			ptDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		if(!ptService.update(ptDO)){
							return CommonWebUtil.buildErrorResponse("Professional Tax Slab Already Added").toString();
						}
				 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Professional Tax Slab Updated");
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/importPT", method = RequestMethod.POST)
	public @ResponseBody String importPT(Model model, HttpServletRequest request) {
		try {
			JSONObject inputJSON =  CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
			JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
			List<PtDO> ptList = new ArrayList<PtDO>();
			Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY);
			for (int i=0; i < fileData.size(); i++){
				JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
				PtDO ptDO = new PtDO();
				if (colName.get(CommonConstants.COUNTRY_NAME) != null && !colName.get(CommonConstants.COUNTRY_NAME).toString().isEmpty()){
					List<CountryDO> countryList = countryService.retrieveByName(rowJSON.get(colName.get(CommonConstants.COUNTRY_NAME)).toString());
			 		if(countryList != null && countryList.size() > 0){
			 			ptDO.setCountry(countryList.get(0));
			 			
			 			if (colName.get(CommonConstants.STATE_NAME) != null && !colName.get(CommonConstants.STATE_NAME).toString().isEmpty()){
							List<StateDO> statusList = stateService.retrieveByName(rowJSON.get(colName.get(CommonConstants.STATE_NAME)).toString());
					 		if(statusList != null && statusList.size() > 0){
					 			ptDO.setState(statusList.get(0));
					 			
						 		if (colName.get(CommonConstants.FROMAMOUNT) != null && !colName.get(CommonConstants.FROMAMOUNT).toString().isEmpty()){
						 			ptDO.setFromamount(Long.parseLong(rowJSON.get(colName.get(CommonConstants.FROMAMOUNT)).toString()));
						 		}
						 		if (colName.get(CommonConstants.TOAMOUNT) != null && !colName.get(CommonConstants.TOAMOUNT).toString().isEmpty()){
						 			ptDO.setToamount(Long.parseLong(rowJSON.get(colName.get(CommonConstants.TOAMOUNT)).toString()));
						 		}
						 		if (colName.get(CommonConstants.PTAMOUNT) != null && !colName.get(CommonConstants.PTAMOUNT).toString().isEmpty()){
						 			ptDO.setPtamount(Long.parseLong(rowJSON.get(colName.get(CommonConstants.PTAMOUNT)).toString()));
							 		}
					 		if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
					 			ptDO.setUpdatedby(updatedBy.toString());
					 			ptDO.setCreatedby(updatedBy.toString());
					 		}
					 		ptDO.setUpdatedon(new Date());
					 		ptDO.setCreatedon(new Date());
					 		ptDO.setStatus(CommonConstants.ACTIVE);
					 		ptList.add(ptDO);
				 		}
			 		}
		 		}
			}
				ptService.persistList(ptList);
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
}
