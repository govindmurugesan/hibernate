package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.TimesheetDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class TimesheetDAO {
	static Logger logger = Logger.getLogger(TimesheetDAO.class.getName());

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private GenericDAOImpl<TimesheetDO> dao;
	
	@SuppressWarnings("unchecked")
	public boolean persist(TimesheetDO timesheetDO) {
		try {
			List<TimesheetDO> timesheetList = null;
			timesheetList =  this.sessionFactory.getCurrentSession().getNamedQuery(TimesheetDO.FIND_DUPLICATE_PERSIST)
					.setParameter(CommonConstants.ID, timesheetDO.getTimesheetType().getTimesheetTypeId())
					.setParameter(CommonConstants.DATE, timesheetDO.getDate())
					.setParameter(CommonConstants.WEEKENDDATE, timesheetDO.getWeeklyTimesheet().getDate())
					.list();
			if(timesheetList != null && timesheetList.size() > 0){
				return false;
			}else{
				dao.persist(timesheetDO);
				return true;
			}
			
		} catch (Exception eException) {
			return true;
		} finally {
		}
	}
	
	public List<TimesheetDO> retrieve() {
		List<TimesheetDO> timesheetList = null;
		try {
			timesheetList = dao.retrieve(TimesheetDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return timesheetList;
	}
	
	@SuppressWarnings("unchecked")
	public List<TimesheetDO> retrieveById(Long Id) {
		List<TimesheetDO> timesheetList = null;
		try {
			timesheetList =  this.sessionFactory.getCurrentSession().getNamedQuery(TimesheetDO.FIND_BY_ID)
								.setParameter(CommonConstants.ID, Id).list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return timesheetList;
	}
	
	@SuppressWarnings("unchecked")
	public boolean update(TimesheetDO timesheetDO) {
		try {
			List<TimesheetDO> timesheetList = null;
			timesheetList =  this.sessionFactory.getCurrentSession().getNamedQuery(TimesheetDO.FIND_DUPLICATE_UPDATE)
					.setParameter(CommonConstants.ID, timesheetDO.getTimesheetType().getTimesheetTypeId())
					.setParameter(CommonConstants.DATE, timesheetDO.getDate())
					.setParameter(CommonConstants.WEEKENDDATE, timesheetDO.getWeeklyTimesheet().getDate())
					.setParameter("Id", timesheetDO.getTimesheetId())
					.list();
			if(timesheetList != null && timesheetList.size() > 0){
				return false;
			}else{
				dao.merge(timesheetDO);
				return true;
			}
		} catch (Exception eException) {
			return true;
		} finally {
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<TimesheetDO> retrieveByWeekendDate(String date, String empId) {
		List<TimesheetDO> TimesheetList = null;
		try {
			TimesheetList = this.sessionFactory.getCurrentSession().getNamedQuery(TimesheetDO.FIND_BY_DATE)
									.setParameter(CommonConstants.WEEKENDDATE, date)
									.setParameter(CommonConstants.EMPID, empId)
									.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return TimesheetList;
	}
}
