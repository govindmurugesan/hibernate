package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.DesignationDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class DesignationDAO {
	static Logger logger = Logger.getLogger(DesignationDAO.class.getName());
	@Autowired
	private GenericDAOImpl<DesignationDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(DesignationDO designationDO)  {
		boolean persistStatus = true;
		try {
			List<DesignationDO> DesignationDOList = null;
			DesignationDOList = this.sessionFactory.getCurrentSession().getNamedQuery(DesignationDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, designationDO.getName())
					.list();
					if(DesignationDOList != null && DesignationDOList.size() > 0){
						persistStatus = false;
					}else{
						genericObject.persist(designationDO);
					}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	public List<DesignationDO> retrieve()  {
		List<DesignationDO> designationList = null;
		try {
			designationList = genericObject.retrieve(DesignationDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return designationList;
	}
	
	@SuppressWarnings("unchecked")
	public List<DesignationDO> retrieveActive()  {
		List<DesignationDO> designationList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(DesignationDO.FIND_BY_STATUS)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return designationList;
	}
	
	@SuppressWarnings("unchecked")
	public List<DesignationDO> retrieveById(Long id)  {
		List<DesignationDO> designationList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(DesignationDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return designationList;
	}
	
	@SuppressWarnings("unchecked")
	public List<DesignationDO> retrieveByName(String name)  {
		List<DesignationDO> designationList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(DesignationDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, name)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return designationList;
	}
	
	@SuppressWarnings("unchecked")
	public boolean update(DesignationDO designationDO)  {
		boolean updateStatus = true;
		try {
			List<DesignationDO> designationList = null;
			designationList = this.sessionFactory.getCurrentSession().getNamedQuery(DesignationDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, designationDO.getName())
					.setParameter(CommonConstants.ID, designationDO.getDesignationId())
					.list();
			if(designationList != null && designationList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(designationDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}
	
	public boolean persistList(List<DesignationDO> designationList){
		try {
			for (DesignationDO designation : designationList) {
				genericObject.persist(designation);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}
}
