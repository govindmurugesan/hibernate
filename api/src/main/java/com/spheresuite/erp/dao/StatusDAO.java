package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.StatusDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class StatusDAO {
	static Logger logger = Logger.getLogger(StatusDAO.class.getName());
	
	@Autowired
	private GenericDAOImpl<StatusDO> dao;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(StatusDO statusDO){
		boolean persistStatus = true;
		try {
			List<StatusDO> statusDOList = null;
			statusDOList = this.sessionFactory.getCurrentSession().getNamedQuery(StatusDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, statusDO.getName())
					.list();
					if(statusDOList != null && statusDOList.size() > 0){
						persistStatus =  false;
					}else{
						dao.persist(statusDO);
					}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	public List<StatusDO> retrieve(){
		List<StatusDO> statusDOList = null;
		try {
			statusDOList = dao.retrieve(StatusDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return statusDOList;
	}
	
	/*public List<StatusDO> retrieveActive(){
		List<StatusDO> addressTypeList = null;
		try {
			addressTypeList =  dao.retrieveActive(CommonConstants.ACTIVE, StatusDO.FIND_BY_STATUS, StatusDO.class);
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return addressTypeList;
	}*/
	
	@SuppressWarnings("unchecked")
	public List<StatusDO> retrieveById(Long id){
		 List<StatusDO> advanceTypeList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(StatusDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return advanceTypeList;
	}
	
	@SuppressWarnings("unchecked")
	public boolean update(StatusDO statusDO) {
		boolean updateStatus = true;
		try {
			List<StatusDO> StatusDOList = null;
			StatusDOList = this.sessionFactory.getCurrentSession().getNamedQuery(StatusDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, statusDO.getName())
					.setParameter(CommonConstants.ID, statusDO.getStatusId())
					.list();
			if(StatusDOList != null && StatusDOList.size() > 0){
				updateStatus = false;
			}else{
				dao.merge(statusDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}
	
	/*public boolean persistList(List<StatusDO> addressTypeList){
		try {
			for (StatusDO addressType : addressTypeList) {
				dao.persist(addressType);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}*/

}
