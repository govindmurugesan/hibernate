package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.HrRequestDocDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class HrRequestDocDAO {
	static Logger logger = Logger.getLogger(HrRequestDocDAO.class.getName());
	@Autowired
	private GenericDAOImpl<HrRequestDocDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public boolean delete(Long id)  {
		try {
			org.hibernate.Query query = this.sessionFactory.getCurrentSession().createQuery("delete from HrRequestDocDO e where e.itsavingId=:id")
					.setParameter(CommonConstants.ID, id);
			query.executeUpdate();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return true;
	}
	
	
	public HrRequestDocDO persist(HrRequestDocDO hrRequestDoc)  {
		try {
			genericObject.persist(hrRequestDoc);
		} catch (Exception eException) {
		} finally {
		}
		return hrRequestDoc;
	}
	
	
	public List<HrRequestDocDO> retrieve()  {
		List<HrRequestDocDO> OpportunitiesDocList = null;
		try {
			OpportunitiesDocList = genericObject.retrieve(HrRequestDocDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return OpportunitiesDocList;
	}
	
	@SuppressWarnings("unchecked")
	public List<HrRequestDocDO> retrieveByOppId(Long id)  {
		List<HrRequestDocDO> OpportunitiesDocList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(HrRequestDocDO.FIND_BY_ITSAVING_ID)
			.setParameter(CommonConstants.ID, id)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return OpportunitiesDocList;
	}
	
	public HrRequestDocDO update(HrRequestDocDO hrRequestDoc)  {
		try {
			genericObject.merge(hrRequestDoc);
		} catch (Exception eException) {
		} finally {
		}
		return hrRequestDoc;
	}

}
