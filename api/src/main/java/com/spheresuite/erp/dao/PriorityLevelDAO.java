package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.PriorityLevelDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class PriorityLevelDAO {
	static Logger logger = Logger.getLogger(PriorityLevelDAO.class.getName());
	
	@Autowired
	private GenericDAOImpl<PriorityLevelDO> dao;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(PriorityLevelDO priorityLevelDO){
		boolean persistStatus = true;
		try {
			List<PriorityLevelDO> priorityLevelDOList = null;
			priorityLevelDOList = this.sessionFactory.getCurrentSession().getNamedQuery(PriorityLevelDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, priorityLevelDO.getName())
					.list();
					if(priorityLevelDOList != null && priorityLevelDOList.size() > 0){
						persistStatus =  false;
					}else{
						dao.persist(priorityLevelDO);
					}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	public List<PriorityLevelDO> retrieve(){
		List<PriorityLevelDO> priorityLevelDOList = null;
		try {
			priorityLevelDOList = dao.retrieve(PriorityLevelDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return priorityLevelDOList;
	}
	
	/*public List<PriorityLevelDO> retrieveActive(){
		List<PriorityLevelDO> addressTypeList = null;
		try {
			addressTypeList =  dao.retrieveActive(CommonConstants.ACTIVE, PriorityLevelDO.FIND_BY_STATUS, PriorityLevelDO.class);
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return addressTypeList;
	}*/
	
	@SuppressWarnings("unchecked")
	public List<PriorityLevelDO> retrieveById(Long id){
		 List<PriorityLevelDO> advanceTypeList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(PriorityLevelDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return advanceTypeList;
	}
	
	@SuppressWarnings("unchecked")
	public boolean update(PriorityLevelDO priorityLevelDO) {
		boolean updateStatus = true;
		try {
			List<PriorityLevelDO> PriorityLevelDOList = null;
			PriorityLevelDOList = this.sessionFactory.getCurrentSession().getNamedQuery(PriorityLevelDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, priorityLevelDO.getName())
					.setParameter(CommonConstants.ID, priorityLevelDO.getPriorityLevelId())
					.list();
			if(PriorityLevelDOList != null && PriorityLevelDOList.size() > 0){
				updateStatus = false;
			}else{
				dao.merge(priorityLevelDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}
	
	/*public boolean persistList(List<PriorityLevelDO> addressTypeList){
		try {
			for (PriorityLevelDO addressType : addressTypeList) {
				dao.persist(addressType);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}*/

}
