package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.PayrollDeductionDetailsDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class PayrollDeductionDetailsDAO {
	static Logger logger = Logger.getLogger(PayrollDeductionDetailsDAO.class.getName());

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private GenericDAOImpl<PayrollDeductionDetailsDO> genericObject;
	
	public List<PayrollDeductionDetailsDO> persist(List<PayrollDeductionDetailsDO> payrollDeductionDetailsDO) {
		try {
			for (PayrollDeductionDetailsDO payrollDeductionDetailsDO2 : payrollDeductionDetailsDO) {
				genericObject.persist(payrollDeductionDetailsDO2);
			}
		} catch (Exception eException) {
			//throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			//SessionManager.closeEntityManager(em);
		}
		return payrollDeductionDetailsDO;
	}
	
	
	public List<PayrollDeductionDetailsDO> retrieve() {
		List<PayrollDeductionDetailsDO> payrollDeductionDetailsList = null;
		try {
			payrollDeductionDetailsList = genericObject.retrieve(PayrollDeductionDetailsDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return payrollDeductionDetailsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<PayrollDeductionDetailsDO> retrieveByEmpCompensationId(Long id) {
		List<PayrollDeductionDetailsDO> payrollDeductionDetailsList = null;
		try {
			payrollDeductionDetailsList = this.sessionFactory.getCurrentSession().getNamedQuery(PayrollDeductionDetailsDO.FIND_BY_ID)
										.setParameter(CommonConstants.ID, id)
										.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return payrollDeductionDetailsList;
	}
	
	public Double retrieveAllDeduction(Long id) {
		Double payrollDeductionDetailsList = null;
		try {
			payrollDeductionDetailsList = (Double) this.sessionFactory.getCurrentSession().getNamedQuery(PayrollDeductionDetailsDO.FIND_ALL_DEDUCTION)
										.setParameter(CommonConstants.ID, id)
										.uniqueResult();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return payrollDeductionDetailsList;
	}
	
	public PayrollDeductionDetailsDO update(PayrollDeductionDetailsDO payrollDeductionDetailsDO) {
		try {
			genericObject.merge(payrollDeductionDetailsDO);
		} catch (Exception eException) {
		} finally {
		}
		return payrollDeductionDetailsDO;
	}

	@SuppressWarnings("unchecked")
	public List<PayrollDeductionDetailsDO> retrieveByEmpCompensationIds(List<Long> ids) {
		List<PayrollDeductionDetailsDO> employeeEarningsList = null;
		try {
			employeeEarningsList = this.sessionFactory.getCurrentSession().getNamedQuery(PayrollDeductionDetailsDO.FIND_BY_IDS)
										.setParameter(CommonConstants.ID, ids)
										.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeEarningsList;
	}
	
	public boolean deletePayrollById(Long payrollId)  {
		try {
			org.hibernate.Query query = this.sessionFactory.getCurrentSession().createQuery("delete from PayrollDeductionDetailsDO e where e.payroll.payrollId=:id")
					.setParameter(CommonConstants.ID, payrollId);
			query.executeUpdate();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return true;
	}
}
