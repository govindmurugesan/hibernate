package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.InvoiceTermsConfigurationDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class InvoiceTermsConfigurationDAO {
	static Logger logger = Logger.getLogger(InvoiceTermsConfigurationDAO.class.getName());
	@Autowired
	private GenericDAOImpl<InvoiceTermsConfigurationDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public InvoiceTermsConfigurationDO persist(InvoiceTermsConfigurationDO invoiceTerms)  {
		try {
			genericObject.persist(invoiceTerms);
		} catch (Exception eException) {
		} finally {
		}
		return invoiceTerms;
	}
	
	
	public List<InvoiceTermsConfigurationDO> retrieve()  {
		List<InvoiceTermsConfigurationDO> invoicetermsList = null;
		try {
			invoicetermsList = genericObject.retrieve(InvoiceTermsConfigurationDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return invoicetermsList;
	}
	
	public List<InvoiceTermsConfigurationDO> retrieveActive()  {
		List<InvoiceTermsConfigurationDO> invoiceTermsList = null;
		try {
			
			return genericObject.retrieveActive(CommonConstants.ACTIVE, 
					InvoiceTermsConfigurationDO.FIND_BY_STATUS, InvoiceTermsConfigurationDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return invoiceTermsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<InvoiceTermsConfigurationDO> retrieveById(Long Id)  {
		List<InvoiceTermsConfigurationDO> termsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(InvoiceTermsConfigurationDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, Id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return termsList;
	}
	
	public InvoiceTermsConfigurationDO update(InvoiceTermsConfigurationDO invoicetermsDO)  {
		try {
			genericObject.merge(invoicetermsDO);
		} catch (Exception eException) {
		} finally {
		}
		return invoicetermsDO;
	}

}
