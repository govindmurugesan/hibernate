package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.PayrollBatchDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class PayrollBatchDAO {
	static Logger logger = Logger.getLogger(PayrollBatchDAO.class.getName());

	@Autowired
	private GenericDAOImpl<PayrollBatchDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(PayrollBatchDO batchDO) {
		boolean persistStatus = true;
		List<PayrollBatchDO> payrollBatchList = null;
		try {
			payrollBatchList = this.sessionFactory.getCurrentSession().getNamedQuery(PayrollBatchDO.FIND_BY_NAME)
			.setParameter(CommonConstants.NAME, batchDO.getName())
			.list();
			if(payrollBatchList != null && payrollBatchList.size() > 0){
				persistStatus = false;
			}else{
				genericObject.persist(batchDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	public List<PayrollBatchDO> retrieve() {
		List<PayrollBatchDO> batchList = null;
		try {
			batchList = genericObject.retrieve(PayrollBatchDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return batchList;
	}
	
	public List<PayrollBatchDO> retrieveActive() {
		List<PayrollBatchDO> batchList = null;
		try {
			batchList =  genericObject.retrieveActive(CommonConstants.ACTIVE, PayrollBatchDO.FIND_BY_STATUS, PayrollBatchDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return batchList;
	}
	
	@SuppressWarnings("unchecked")
	public List<PayrollBatchDO> retrieveById(Long id) {
		List<PayrollBatchDO> batchList = null;
		try {
			batchList =  this.sessionFactory.getCurrentSession().getNamedQuery(PayrollBatchDO.FIND_BY_ID)
							.setParameter(CommonConstants.ID, id).list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return batchList;
	}
	
	@SuppressWarnings("unchecked")
	public List<PayrollBatchDO> retrieveByUnitId(Long id) {
		List<PayrollBatchDO> batchList = null;
		try {
			batchList =  this.sessionFactory.getCurrentSession().getNamedQuery(PayrollBatchDO.FIND_BY_UNIT_ID)
							.setParameter(CommonConstants.ID, id)
							.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE).list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return batchList;
	}
	
	@SuppressWarnings("unchecked")
	public boolean update(PayrollBatchDO batchDO) {
		boolean updateStatus = true;
		try {
			List<PayrollBatchDO> payrollBatchList = null;
			payrollBatchList = this.sessionFactory.getCurrentSession().getNamedQuery(PayrollBatchDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, batchDO.getName())
					.setParameter(CommonConstants.ID, batchDO.getPayrollBatchId())
					.list();
			if(payrollBatchList != null && payrollBatchList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(batchDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}
	
	@SuppressWarnings("unchecked")
	public List<PayrollBatchDO> retrieveByName(String name) {
		List<PayrollBatchDO> payrollBatchList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(PayrollBatchDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, name)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return payrollBatchList;
	}
	
	@SuppressWarnings("unchecked")
	public List<PayrollBatchDO> retrieveByPayrollType(String type) {
		List<PayrollBatchDO> payrollBatchList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(PayrollBatchDO.FIND_BY_PAROLLTYPE)
					.setParameter(CommonConstants.TYPE, type)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return payrollBatchList;
	}
	
	
	public boolean persistList(List<PayrollBatchDO> payrollBatchList){
		try {
			for (PayrollBatchDO payrollBatch : payrollBatchList) {
				genericObject.persist(payrollBatch);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}

}
