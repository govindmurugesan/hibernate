package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.FyDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class FyDAO {
	static Logger logger = Logger.getLogger(FyDAO.class.getName());
	@Autowired
	private GenericDAOImpl<FyDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(FyDO fyDO)  {
		boolean persistStatus = true;
		try {
			List<FyDO> fyDOList = null;
			fyDOList = this.sessionFactory.getCurrentSession().getNamedQuery(FyDO.FIND_BY_NAME)
					.setParameter(CommonConstants.FROM, fyDO.getFromyear())
					.setParameter(CommonConstants.TO, fyDO.getToyear())
					.list();
					if(fyDOList != null && fyDOList.size() > 0){
						persistStatus = false;
					}else{
						genericObject.persist(fyDO);
					}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	public List<FyDO> retrieve()  {
		List<FyDO> fyList = null;
		try {
			fyList = genericObject.retrieve(FyDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return fyList;
	}
	
	public List<FyDO> retrieveActive()  {
		List<FyDO> fyList = null;
		try {
			return genericObject.retrieveActive(CommonConstants.ACTIVE, 
					FyDO.FIND_BY_STATUS, FyDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return fyList;
	}
	
	@SuppressWarnings("unchecked")
	public List<FyDO> retrieveById(Long Id)  {
		List<FyDO> fyList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(FyDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, Id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return fyList;
	}
	
	@SuppressWarnings("unchecked")
	public boolean update(FyDO fyDO)  {
		boolean updateStatus = true;
		try {
			List<FyDO> fyDOList = null;
			fyDOList = this.sessionFactory.getCurrentSession().getNamedQuery(FyDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.FROM, fyDO.getFromyear())
					.setParameter(CommonConstants.TO, fyDO.getToyear())
					.setParameter(CommonConstants.ID, fyDO.getFyId())
					.list();
			if(fyDOList != null && fyDOList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(fyDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}
	
	public boolean persistList(List<FyDO> fyDO){
		try {
			for (FyDO fy : fyDO) {
				genericObject.persist(fy);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}

}
