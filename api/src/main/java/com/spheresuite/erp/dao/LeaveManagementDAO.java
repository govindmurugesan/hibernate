package com.spheresuite.erp.dao;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.LeaveManagementDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class LeaveManagementDAO {
	static Logger logger = Logger.getLogger(LeaveManagementDAO.class.getName());
	@Autowired
	private GenericDAOImpl<LeaveManagementDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(LeaveManagementDO leaveManagmentDO) {
		boolean persistStatus = true;
		try {
			List<LeaveManagementDO> leaveList = null;
			leaveList = this.sessionFactory.getCurrentSession().getNamedQuery(LeaveManagementDO.FIND_FOR_ADD)
					.setParameter(CommonConstants.FROMDATE, leaveManagmentDO.getFromDate())
					.setParameter(CommonConstants.TODATE, leaveManagmentDO.getToDate())
					.setParameter(CommonConstants.TYPE, leaveManagmentDO.getLeaveType())
					.list();
					if(leaveList != null && leaveList.size() > 0){
						persistStatus = false;
					}else{
						genericObject.persist(leaveManagmentDO);
					}
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
		}
		return persistStatus;
	}
	
	public List<LeaveManagementDO> retrieve()  {
		List<LeaveManagementDO> leaveList = null;
		try {
			leaveList = genericObject.retrieve(LeaveManagementDO.class);
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
		}
		return leaveList;
	}
	
	
	
	@SuppressWarnings("unchecked")
	public List<LeaveManagementDO> retrieveCountByYear(Date fromDate, Date ToDate, Long leaveTypeId)  {
		List<LeaveManagementDO> requestList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeaveManagementDO.FIND_COUNT_FORYEAR)
			.setParameter(CommonConstants.FROMDATE, fromDate )
			.setParameter(CommonConstants.TODATE, ToDate)
			.setParameter(CommonConstants.TYPE, leaveTypeId)
			.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return requestList;
	}
	
	/*@SuppressWarnings("unchecked")
	public List<LeaveManagementDO> retrieveByDept(List<Long> ids)  {
		List<LeaveManagementDO> leaveList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeaveManagementDO.FIND_ALL_BY_DEPT)
			.setParameter(CommonConstants.ID, ids)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return leaveList;
	}
	*/
	
	@SuppressWarnings("unchecked")
	public List<LeaveManagementDO> retrieveById(Long Id)  {
		List<LeaveManagementDO> leaveList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeaveManagementDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, Id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return leaveList;
	}
	
	/*public LeaveManagementDO update(LeaveManagementDO leaveDetail)  {
		try {
			genericObject.merge(leaveDetail);
		} catch (Exception eException) {
		} finally {
		}
		return leaveDetail;
	}*/
	
	@SuppressWarnings("unchecked")
	public boolean update(LeaveManagementDO leaveManagmentDO) {
		boolean persistStatus = true;
		try {
			genericObject.merge(leaveManagmentDO);
			/*List<LeaveManagementDO> leaveList = null;
			leaveList = this.sessionFactory.getCurrentSession().getNamedQuery(LeaveManagementDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.FROMDATE, leaveManagmentDO.getFromDate())
					.setParameter(CommonConstants.TODATE, leaveManagmentDO.getToDate())
					.setParameter(CommonConstants.TYPE, leaveManagmentDO.getLeaveType().getLeaveTypeId())
					.setParameter(CommonConstants.ID, leaveManagmentDO.getLeaveManagementId())
					.list();
					if(leaveList != null && leaveList.size() > 0){
						persistStatus = false;
					}else{
						genericObject.merge(leaveManagmentDO);
					}*/
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
		}
		return persistStatus;
	}
	
	/*@SuppressWarnings("unchecked")
	public List<LeaveManagementDO> retrieveByEmpId(Long id)  {
		List<LeaveManagementDO> leaveList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(LeaveManagementDO.FINDBY_EMP_ID);
				q.setParameter(CommonConstants.ID, id);
				leaveList = (List<LeaveManagementDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return leaveList;
	}*/
	
	@SuppressWarnings("unchecked")
	public List<LeaveManagementDO> retriveActiveById(Long id)  {
		List<LeaveManagementDO> leaveList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeaveManagementDO.FIND_ACTIVE_BY_ID)
			.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
			.setParameter(CommonConstants.ID, id)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return leaveList;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeaveManagementDO> retrieveCountByType(Long type)  {
		List<LeaveManagementDO> leaveList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeaveManagementDO.FIND_COUNT_BY_TYPE)
			.setParameter(CommonConstants.TYPE, type)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return leaveList;
	}
	
	/*@SuppressWarnings("unchecked")
	public List<LeaveManagementDO> retrieveByTypeName(String name)  {
		List<LeaveManagementDO> leaveList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeaveManagementDO.FIND_BY_TYPENAME)
			.setParameter(CommonConstants.TYPE, name)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return leaveList;
	}*/
	
	
	
	public List<LeaveManagementDO> retrieveAllActive()  {
		List<LeaveManagementDO> leaveList = null;
		try {
			return genericObject.retrieveActive(CommonConstants.ACTIVE, 
			LeaveManagementDO.FIND_ACTIVE, LeaveManagementDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return leaveList;
	}
	
	public boolean persistList(List<LeaveManagementDO> leaveManagementList){
		try {
			for (LeaveManagementDO leaveManagement : leaveManagementList) {
				genericObject.persist(leaveManagement);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}
}
