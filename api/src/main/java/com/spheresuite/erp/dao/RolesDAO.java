package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.RolesDO;
import com.spheresuite.erp.util.CommonConstants;

@Repository
public class RolesDAO {
	
	static Logger logger = Logger.getLogger(RolesDAO.class.getName());
	
	@Autowired
	private GenericDAO<RolesDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(RolesDO rolesDO) {
		boolean persistStatus = true;
		try {
			List<RolesDO> roleList = null;
			roleList = this.sessionFactory.getCurrentSession().getNamedQuery(RolesDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, rolesDO.getName())
					.list();
					if(roleList != null && roleList.size() > 0){
						persistStatus = false;
					}else{
						genericObject.persist(rolesDO);
					}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	public List<RolesDO> retrieve() {
		List<RolesDO> rolesList = null;
		try {
			rolesList = genericObject.retrieve(RolesDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return rolesList;
	}

	@SuppressWarnings("unchecked")
	public List<RolesDO> retriveById(long id) {
		List<RolesDO> rolesList = null;
		try {
			rolesList =  this.sessionFactory.getCurrentSession().getNamedQuery(RolesDO.FIND_BY_ID)
							.setParameter(CommonConstants.ID, id).list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return rolesList;
	}
	
	
	@SuppressWarnings("unchecked")
	public boolean update(RolesDO roleDO) {
		boolean updateStatus = true;
		try {
			List<RolesDO> roleList = null;
			roleList = this.sessionFactory.getCurrentSession().getNamedQuery(RolesDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, roleDO.getName())
					.setParameter(CommonConstants.ID, roleDO.getRoleId())
					.list();
			if(roleList != null && roleList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(roleDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}

	public boolean delete(RolesDO rolesDO) {
		try {
			genericObject.remove(rolesDO);
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}
	
	@SuppressWarnings("unchecked")
	public List<RolesDO> retriveByName(String name) {
		List<RolesDO> rolesList = null;
		try {
			rolesList =  this.sessionFactory.getCurrentSession().getNamedQuery(RolesDO.FIND_BY_NAME)
							.setParameter(CommonConstants.NAME, name).list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return rolesList;
	}
	

}	
