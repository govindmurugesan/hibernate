package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.InsurancePaymentDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class InsurancePaymentDAO {
	static Logger logger = Logger.getLogger(InsurancePaymentDAO.class.getName());
	@Autowired
	private GenericDAOImpl<InsurancePaymentDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public InsurancePaymentDO persist(InsurancePaymentDO insurancePaymentDO) {
		try {
			genericObject.persist(insurancePaymentDO);
		} catch (Exception eException) {
		} finally {
		}
		return insurancePaymentDO;
	}
	
	@SuppressWarnings("unchecked")
	public List<InsurancePaymentDO> retrieveActiveByEmpId(String empId) {
		List<InsurancePaymentDO> employeeInsuranceList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(InsurancePaymentDO.FIND_ACTIVE_BY_EMPID)
					.setParameter(CommonConstants.EMPID, empId)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVESTRING)
					.list(); 
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeInsuranceList;
	}
	
	/*@SuppressWarnings("unchecked")
	public List<InsurancePaymentDO> retrieveByEmpIdWithDate(List<Long> empIds, String bonusMonth) {
		List<InsurancePaymentDO> employeeBonusList = null;
		try {
			if(empIds != null && empIds.size() > 0){
				return this.sessionFactory.getCurrentSession().getNamedQuery(InsurancePaymentDO.FIND_BY_EMPID_MONTH)
					.setParameter(CommonConstants.EMPID, empIds)
					.setParameter(CommonConstants.STATUS, 'a')
					.setParameter(CommonConstants.MONTHLY, bonusMonth)
					.list(); 
			}else{
				return this.sessionFactory.getCurrentSession().getNamedQuery(InsurancePaymentDO.FIND_BY_MONTH)
					.setParameter(CommonConstants.STATUS, 'a')
					.setParameter(CommonConstants.MONTHLY, bonusMonth)
					.list();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeBonusList;
	}*/
	
	/*@SuppressWarnings("unchecked")
	public List<InsurancePaymentDO> retrieveByEmpIdBetweenDate(List<Long> empIds, String fromMonth, String toMonth) {
		List<InsurancePaymentDO> employeeBonusList = null;
		try {
				if(empIds != null && empIds.size() > 0){
					return this.sessionFactory.getCurrentSession().getNamedQuery(InsurancePaymentDO.FIND_BY_EMPID_BETWEENMONTH)
						.setParameter(CommonConstants.EMPID, empIds)
						.setParameter(CommonConstants.STATUS, 'a')
						.setParameter("fromMonth", fromMonth)
						.setParameter("toMonth", toMonth)
						.list();
				}else{
					return this.sessionFactory.getCurrentSession().getNamedQuery(InsurancePaymentDO.FIND_BETWEENMONTH)
					.setParameter(CommonConstants.STATUS, 'a')
					.setParameter("fromMonth", fromMonth)
					.setParameter("toMonth", toMonth)
					.list();
				}
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeBonusList;
	}
	
	@SuppressWarnings("unchecked")
	public List<InsurancePaymentDO> retrieveByEmpIdAndMonth(String empId, String month) {
		List<InsurancePaymentDO> employeeBonusList = null;
		try {
				return this.sessionFactory.getCurrentSession().getNamedQuery(InsurancePaymentDO.FIND_BY_EMPID_DATE)
					.setParameter(CommonConstants.EMPID, empId)
					.setParameter(CommonConstants.MONTHLY, month)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
				
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeBonusList;
	}
	
	@SuppressWarnings("rawtypes")
	public Long retrieveByEmpIdBetweenMonth(String empId, String fromMonth, String toMonth) {
		Long employeeBonusList = null;
		try {
				List longList =  this.sessionFactory.getCurrentSession().getNamedQuery(InsurancePaymentDO.FIND_BY_EMPID_BETWEEN_DATE)
						.setParameter(CommonConstants.EMPID, empId)
						.setParameter("fromMonth", fromMonth)
						.setParameter("toMonth", toMonth)
						.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
						.list();
				
				if(longList !=null && longList.size() > 0){
					employeeBonusList = (Long) longList.get(0);
				}
				
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeBonusList;
	}
	
	@SuppressWarnings("unchecked")
	public List<InsurancePaymentDO> retrieveByEmpIdAndMonthWithOutStatus(Long empId, String month) {
		List<InsurancePaymentDO> employeeBonusList = null;
		try {
			
			return this.sessionFactory.getCurrentSession().getNamedQuery(InsurancePaymentDO.FIND_BY_EMPID_DATE)
					.setParameter(CommonConstants.EMPID, empId)
					.setParameter(CommonConstants.MONTHLY, month)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeBonusList;
	}*/
	
	@SuppressWarnings("unchecked")
	public List<InsurancePaymentDO> retrieve() {
		List<InsurancePaymentDO> insuracePaymentList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(InsurancePaymentDO.FIND_ALL)
					.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
		}
		return insuracePaymentList;
	}
	
	@SuppressWarnings("unchecked")
	public List<InsurancePaymentDO> retrieveById(Long id) {
		List<InsurancePaymentDO> insuracePaymentList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(InsurancePaymentDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return insuracePaymentList;
	}
	
	public InsurancePaymentDO update(InsurancePaymentDO insurancePaymentDO) {
		try {
			genericObject.merge(insurancePaymentDO);
		} catch (Exception eException) {
		} finally {
		}
		return insurancePaymentDO;
	}
	
	/*public boolean delete(Long id)  {
		try {
			org.hibernate.Query query = this.sessionFactory.getCurrentSession().createQuery("delete from InsurancePaymentDO e where e.empBonusId=:id")
					.setParameter(CommonConstants.ID, id);
			query.executeUpdate();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return true;
	}*/
	

}
