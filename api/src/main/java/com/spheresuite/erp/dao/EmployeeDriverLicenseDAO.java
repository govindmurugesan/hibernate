package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.EmployeeDriverLicenseDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class EmployeeDriverLicenseDAO {
	static Logger logger = Logger.getLogger(EmployeeDriverLicenseDAO.class.getName());
	@Autowired
	private GenericDAOImpl<EmployeeDriverLicenseDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public EmployeeDriverLicenseDO persist(EmployeeDriverLicenseDO employeeDriverLicenseDO) {
		try {
			genericObject.persist(employeeDriverLicenseDO);
		} catch (Exception eException) {
		} finally {
		}
		return employeeDriverLicenseDO;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeDriverLicenseDO> retrieveById(Long id){
		List<EmployeeDriverLicenseDO> employeeDriverLicenseList = null;
		
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeDriverLicenseDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeDriverLicenseList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeDriverLicenseDO> retrieveByEmpId(String empId) {
		List<EmployeeDriverLicenseDO> employeeDriverLicenseList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeDriverLicenseDO.FIND_BY_EMPID)
					.setParameter(CommonConstants.EMPID, empId)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeDriverLicenseList;
	}
	
	public EmployeeDriverLicenseDO update(EmployeeDriverLicenseDO employeeDriverLicenseDO) {
		try {
			genericObject.merge(employeeDriverLicenseDO);
		} catch (Exception eException) {
		} finally {
		}
		return employeeDriverLicenseDO;
	}

}
