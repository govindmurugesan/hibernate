package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.HrRequestTypeDO;
import com.spheresuite.erp.domainobject.OpportunityTypeDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class HrRequestTypeDAO {
	static Logger logger = Logger.getLogger(HrRequestTypeDAO.class.getName());
	@Autowired
	private GenericDAOImpl<HrRequestTypeDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(HrRequestTypeDO hrRequestType)  {
		boolean persistStatus = true;
		List<OpportunityTypeDO> projectTypeList = null;
		try {
			projectTypeList = this.sessionFactory.getCurrentSession().getNamedQuery(HrRequestTypeDO.FIND_BY_NAME)
			.setParameter(CommonConstants.NAME, hrRequestType.getRequestType())
			.list();
			if(projectTypeList != null && projectTypeList.size() > 0){
				persistStatus = false;
			}else{
				genericObject.persist(hrRequestType);
			}
			
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	
	public List<HrRequestTypeDO> retrieve()  {
		List<HrRequestTypeDO> requestTypeList = null;
		try {
			requestTypeList = genericObject.retrieve(HrRequestTypeDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return requestTypeList;
	}
	
	@SuppressWarnings("unchecked")
	public boolean update(HrRequestTypeDO requestType)  {
		boolean updateStatus = true;
		try {
			List<OpportunityTypeDO> projectTypeList = null;
			projectTypeList = this.sessionFactory.getCurrentSession().getNamedQuery(HrRequestTypeDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, requestType.getRequestType())
					.setParameter(CommonConstants.ID, requestType.getHrRequestTypeId())
					.list();
			if(projectTypeList != null && projectTypeList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(requestType);
			}
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}
	
	@SuppressWarnings("unchecked")
	public List<HrRequestTypeDO> retrieveById(Long Id)  {
		List<HrRequestTypeDO> hrRequestTypeList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(HrRequestTypeDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, Id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return hrRequestTypeList;
	}
	
	public boolean persistList(List<HrRequestTypeDO> hrRequestTypeList){
		try {
			for (HrRequestTypeDO hrRequestType : hrRequestTypeList) {
				genericObject.persist(hrRequestType);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}

}
