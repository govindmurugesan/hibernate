package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.AdvancePaymentDetailDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class AdvancePaymentDetailDAO {
	static Logger logger = Logger.getLogger(AdvancePaymentDetailDAO.class.getName());
	@Autowired
	private GenericDAOImpl<AdvancePaymentDetailDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public AdvancePaymentDetailDO persist(AdvancePaymentDetailDO advancePaymentDetailDO) {
		try {
			genericObject.persist(advancePaymentDetailDO);
		} catch (Exception eException) {
		} finally {
		}
		return advancePaymentDetailDO;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<AdvancePaymentDetailDO> getByAdvanceId(Long id) {
		List<AdvancePaymentDetailDO> advancePaymentDetailList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(AdvancePaymentDetailDO.FIND_BY_ADVANCE_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return advancePaymentDetailList;
	}
	
	@SuppressWarnings("unchecked")
	public List<AdvancePaymentDetailDO> getById(Long id) {
		List<AdvancePaymentDetailDO> advancePaymentDetailList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(AdvancePaymentDetailDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return advancePaymentDetailList;
	}
	
	
	
	@SuppressWarnings("unchecked")
	public List<AdvancePaymentDetailDO> getByMonthAndEmpId(String empId, String month) {
		List<AdvancePaymentDetailDO> advancePaymentDetailList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(AdvancePaymentDetailDO.FIND_BY_EMPID_MONTH)
					.setParameter(CommonConstants.EMPID, empId)
					.setParameter(CommonConstants.MONTH, month)
					.setParameter(CommonConstants.STATUS,CommonConstants.ACTIVESTRING)
					.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return advancePaymentDetailList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<AdvancePaymentDetailDO> getByStatusAndEmpId(String empId, String month) {
		List<AdvancePaymentDetailDO> advancePaymentDetailList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(AdvancePaymentDetailDO.FIND_BY_EMPID_MONTH)
					.setParameter(CommonConstants.EMPID, empId)
					.setParameter(CommonConstants.MONTH, month)
					.setParameter(CommonConstants.STATUS,CommonConstants.SSTATUS)
					.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return advancePaymentDetailList;
	}
	
	public AdvancePaymentDetailDO update(AdvancePaymentDetailDO AdvancePaymentDetailDO) {
		try {
			genericObject.merge(AdvancePaymentDetailDO);
		} catch (Exception eException) {
		} finally {
		}
		return AdvancePaymentDetailDO;
	}
	
	
	/*@SuppressWarnings("unchecked")
	public List<AdvancePaymentDetailDO> retrieveActiveByEmpId(String empId) {
		List<AdvancePaymentDetailDO> employeeAdvanceList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(AdvancePaymentDetailDO.FIND_ACTIVE_BY_EMPID)
					.setParameter(CommonConstants.EMPID, empId)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVESTRING)
					.list(); 
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeAdvanceList;
	}*/
	
	/*@SuppressWarnings("unchecked")
	public List<AdvancePaymentDetailDO> retrieveByEmpIdWithDate(List<Long> empIds, String bonusMonth) {
		List<AdvancePaymentDetailDO> employeeBonusList = null;
		try {
			if(empIds != null && empIds.size() > 0){
				return this.sessionFactory.getCurrentSession().getNamedQuery(AdvancePaymentDetailDO.FIND_BY_EMPID_MONTH)
					.setParameter(CommonConstants.EMPID, empIds)
					.setParameter(CommonConstants.STATUS, 'a')
					.setParameter(CommonConstants.MONTHLY, bonusMonth)
					.list(); 
			}else{
				return this.sessionFactory.getCurrentSession().getNamedQuery(AdvancePaymentDetailDO.FIND_BY_MONTH)
					.setParameter(CommonConstants.STATUS, 'a')
					.setParameter(CommonConstants.MONTHLY, bonusMonth)
					.list();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeBonusList;
	}*/
	
	/*@SuppressWarnings("unchecked")
	public List<AdvancePaymentDetailDO> retrieveByEmpIdBetweenDate(List<Long> empIds, String fromMonth, String toMonth) {
		List<AdvancePaymentDetailDO> employeeBonusList = null;
		try {
				if(empIds != null && empIds.size() > 0){
					return this.sessionFactory.getCurrentSession().getNamedQuery(AdvancePaymentDetailDO.FIND_BY_EMPID_BETWEENMONTH)
						.setParameter(CommonConstants.EMPID, empIds)
						.setParameter(CommonConstants.STATUS, 'a')
						.setParameter("fromMonth", fromMonth)
						.setParameter("toMonth", toMonth)
						.list();
				}else{
					return this.sessionFactory.getCurrentSession().getNamedQuery(AdvancePaymentDetailDO.FIND_BETWEENMONTH)
					.setParameter(CommonConstants.STATUS, 'a')
					.setParameter("fromMonth", fromMonth)
					.setParameter("toMonth", toMonth)
					.list();
				}
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeBonusList;
	}
	
	@SuppressWarnings("unchecked")
	public List<AdvancePaymentDetailDO> retrieveByEmpIdAndMonth(String empId, String month) {
		List<AdvancePaymentDetailDO> employeeBonusList = null;
		try {
				return this.sessionFactory.getCurrentSession().getNamedQuery(AdvancePaymentDetailDO.FIND_BY_EMPID_DATE)
					.setParameter(CommonConstants.EMPID, empId)
					.setParameter(CommonConstants.MONTHLY, month)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
				
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeBonusList;
	}
	
	@SuppressWarnings("rawtypes")
	public Long retrieveByEmpIdBetweenMonth(String empId, String fromMonth, String toMonth) {
		Long employeeBonusList = null;
		try {
				List longList =  this.sessionFactory.getCurrentSession().getNamedQuery(AdvancePaymentDetailDO.FIND_BY_EMPID_BETWEEN_DATE)
						.setParameter(CommonConstants.EMPID, empId)
						.setParameter("fromMonth", fromMonth)
						.setParameter("toMonth", toMonth)
						.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
						.list();
				
				if(longList !=null && longList.size() > 0){
					employeeBonusList = (Long) longList.get(0);
				}
				
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeBonusList;
	}
	
	@SuppressWarnings("unchecked")
	public List<AdvancePaymentDetailDO> retrieveByEmpIdAndMonthWithOutStatus(Long empId, String month) {
		List<AdvancePaymentDetailDO> employeeBonusList = null;
		try {
			
			return this.sessionFactory.getCurrentSession().getNamedQuery(AdvancePaymentDetailDO.FIND_BY_EMPID_DATE)
					.setParameter(CommonConstants.EMPID, empId)
					.setParameter(CommonConstants.MONTHLY, month)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeBonusList;
	}*/
	
	/*@SuppressWarnings("unchecked")
	public List<AdvancePaymentDetailDO> retrieve() {
		List<AdvancePaymentDetailDO> advancePaymentList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(AdvancePaymentDetailDO.FIND_ALL)
					.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
		}
		return advancePaymentList;
	}
	
	
	
	*/
	
	/*public boolean delete(Long id)  {
		try {
			org.hibernate.Query query = this.sessionFactory.getCurrentSession().createQuery("delete from AdvancePaymentDetailDO e where e.empBonusId=:id")
					.setParameter(CommonConstants.ID, id);
			query.executeUpdate();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return true;
	}*/
	

}
