package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.GeneralEarningDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class GeneralEarningDAO {
	static Logger logger = Logger.getLogger(GeneralEarningDAO.class.getName());
	@Autowired
	private GenericDAOImpl<GeneralEarningDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public Boolean persist(GeneralEarningDO generalEarningDO) {
		Boolean presistStatus = true;
		try {
			genericObject.persist(generalEarningDO);
					}
		catch (Exception eException) {
			presistStatus = false;
		} finally {
		}
		return presistStatus;

	}
	
	@SuppressWarnings("unchecked")
	public List<GeneralEarningDO> retrieveActiveByEmpId(String empId) {
		List<GeneralEarningDO> employeeloanList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(GeneralEarningDO.FIND_ACTIVE_BY_EMPID)
					.setParameter(CommonConstants.EMPID, empId)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVESTRING)
					.list(); 
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeloanList;
	}
	
	

	@SuppressWarnings("unchecked")
	public List<GeneralEarningDO> retrieveActiveByEmpIdAndMonth(String empId, String month) {
		List<GeneralEarningDO> employeeloanList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(GeneralEarningDO.FIND_ACTIVE_BY_EMPID_MONTH)
					.setParameter(CommonConstants.EMPID, empId)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVESTRING)
					.setParameter(CommonConstants.MONTH, month)
					.list(); 
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeloanList;
	}
	
	@SuppressWarnings("unchecked")
	public List<GeneralEarningDO> retrieve() {
		List<GeneralEarningDO> loanPaymentList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(GeneralEarningDO.FIND_ALL)
					.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
		}
		return loanPaymentList;
	}
	
	@SuppressWarnings("unchecked")
	public List<GeneralEarningDO> retrieveById(Long id) {
		List<GeneralEarningDO> loanPaymentList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(GeneralEarningDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return loanPaymentList;
	}
	
	public Boolean update(GeneralEarningDO loanPaymentDO) {
		Boolean updateStatus = true;
		try {
			genericObject.merge(loanPaymentDO);
		} catch (Exception eException) {
			updateStatus = false;
		} finally {
		}
		return updateStatus;
	}
	
	public boolean persistList(List<GeneralEarningDO> earningList){
		try {
			for (GeneralEarningDO earning : earningList) {
				genericObject.persist(earning);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}
	
	/*public boolean delete(Long id)  {
		try {
			org.hibernate.Query query = this.sessionFactory.getCurrentSession().createQuery("delete from LoanPaymentDO e where e.empBonusId=:id")
					.setParameter(CommonConstants.ID, id);
			query.executeUpdate();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return true;
	}*/
	

}
