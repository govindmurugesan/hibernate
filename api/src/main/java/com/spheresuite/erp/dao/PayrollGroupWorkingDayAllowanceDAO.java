package com.spheresuite.erp.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.PayrollGroupWorkingdaysAllowanceDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class PayrollGroupWorkingDayAllowanceDAO {
	@Autowired
	private GenericDAOImpl<PayrollGroupWorkingdaysAllowanceDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(PayrollGroupWorkingdaysAllowanceDO payrollGroupWorkingdaysAllowanceDO){
		List<PayrollGroupWorkingdaysAllowanceDO> payrollGroupWorkingdaysAllowanceList = null;
		boolean persistStatus = true;
		try {
			payrollGroupWorkingdaysAllowanceList = this.sessionFactory.getCurrentSession().getNamedQuery(PayrollGroupWorkingdaysAllowanceDO.FIND_FOR_ADD)
			.setParameter(CommonConstants.ALLOWENCE_ID, payrollGroupWorkingdaysAllowanceDO.getWorkingDayAllowance().getWorkingDayAllowanceId())
			.setParameter(CommonConstants.PAYROLLGROUPID, payrollGroupWorkingdaysAllowanceDO.getPayrollGroup().getPayrollGroupId())
			.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
			.list();
			if(payrollGroupWorkingdaysAllowanceList != null && payrollGroupWorkingdaysAllowanceList.size() > 0){
				persistStatus = false;
			}else{
				genericObject.persist(payrollGroupWorkingdaysAllowanceDO);
			}
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
		}
		return persistStatus;
	}
	
	@SuppressWarnings("unchecked")
	public List<PayrollGroupWorkingdaysAllowanceDO> retrieveByGroupID(Long id) {
		List<PayrollGroupWorkingdaysAllowanceDO> payrollGroupWorkingdaysAllowanceList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(PayrollGroupWorkingdaysAllowanceDO.FIND_BY_GROUP_ID)
					.setParameter(CommonConstants.ID, id)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return payrollGroupWorkingdaysAllowanceList;
	}
	
	
	
	@SuppressWarnings("unchecked")
	public List<PayrollGroupWorkingdaysAllowanceDO> retrieveByID(Long id) {
		List<PayrollGroupWorkingdaysAllowanceDO> payrollGroupWorkingdaysAllowanceList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(PayrollGroupWorkingdaysAllowanceDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return payrollGroupWorkingdaysAllowanceList;
	}
	
	public boolean updateList(List<PayrollGroupWorkingdaysAllowanceDO> payrollGroupWorkingdaysAllowanceList){
		try {
			for (PayrollGroupWorkingdaysAllowanceDO payrollGroupWorkingdays : payrollGroupWorkingdaysAllowanceList) {
				genericObject.merge(payrollGroupWorkingdays);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}
	
	/*public List<PayrollGroupDO> retrieve(){
		List<PayrollGroupDO> payrollGroupList = null;
		try {
			payrollGroupList = genericObject.retrieve(PayrollGroupDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return payrollGroupList;
	}
	
	public List<PayrollGroupDO> retrieveActive(){
		List<PayrollGroupDO> payrollGroupList = null;
		try {
			return genericObject.retrieveActive(CommonConstants.ACTIVE, PayrollGroupDO.FIND_BY_STATUS, PayrollGroupDO.class);
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return payrollGroupList;
	}
	
	
	*/
	/*@SuppressWarnings("unchecked")
	public boolean update(PayrollGroupDO payrollGroupDO){
		boolean updateStatus = true;
		try {
			List<PayrollGroupDO> payrollGroupList = null;
			payrollGroupList = this.sessionFactory.getCurrentSession().getNamedQuery(PayrollGroupDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, payrollGroupDO.getName())
					.setParameter(CommonConstants.ID, payrollGroupDO.getPayrollGroupId())
					.list();
			if(payrollGroupList != null && payrollGroupList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(payrollGroupDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}*/

}
