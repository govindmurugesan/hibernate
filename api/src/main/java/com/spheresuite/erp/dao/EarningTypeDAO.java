package com.spheresuite.erp.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.EarningTypeDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class EarningTypeDAO {
	@Autowired
	private GenericDAOImpl<EarningTypeDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(EarningTypeDO earningTypeDO){
		List<EarningTypeDO> earningTypeList = null;
		boolean persistStatus = true;
		try {
			earningTypeList = this.sessionFactory.getCurrentSession().getNamedQuery(EarningTypeDO.FIND_BY_NAME)
			.setParameter(CommonConstants.NAME, earningTypeDO.getName())
			.list();
			if(earningTypeList != null && earningTypeList.size() > 0){
				persistStatus = false;
			}else{
				genericObject.persist(earningTypeDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	public List<EarningTypeDO> retrieve(){
		List<EarningTypeDO> earningTypeList = null;
		try {
			earningTypeList = genericObject.retrieve(EarningTypeDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return earningTypeList;
	}
	
	public List<EarningTypeDO> retrieveActive(){
		List<EarningTypeDO> earningTypeList = null;
		try {
			return genericObject.retrieveActive(CommonConstants.ACTIVE, EarningTypeDO.FIND_BY_STATUS, EarningTypeDO.class);
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return earningTypeList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EarningTypeDO> retrieveById(Long id) {
		List<EarningTypeDO> earningTypeList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EarningTypeDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return earningTypeList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<EarningTypeDO> retrieveByName(String name) {
		List<EarningTypeDO> earningTypeList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EarningTypeDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, name)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return earningTypeList;
	}
	
	@SuppressWarnings("unchecked")
	public boolean update(EarningTypeDO allowanceTypeDO){
		boolean updateStatus = true;
		try {
			List<EarningTypeDO> earningTypeList = null;
			earningTypeList = this.sessionFactory.getCurrentSession().getNamedQuery(EarningTypeDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, allowanceTypeDO.getName())
					.setParameter(CommonConstants.ID, allowanceTypeDO.getEarningTypeId())
					.list();
			if(earningTypeList != null && earningTypeList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(allowanceTypeDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}
	
	public boolean persistList(List<EarningTypeDO> designationList){
		try {
			for (EarningTypeDO earningType : designationList) {
				genericObject.persist(earningType);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}

}
