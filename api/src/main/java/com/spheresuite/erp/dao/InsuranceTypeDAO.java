package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.InsuranceTypeDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class InsuranceTypeDAO {
	static Logger logger = Logger.getLogger(InsuranceTypeDAO.class.getName());
	
	@Autowired
	private GenericDAOImpl<InsuranceTypeDO> dao;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(InsuranceTypeDO insuranceTypeDO){
		boolean persistStatus = true;
		try {
			List<InsuranceTypeDO> InsuranceTypeDOList = null;
			InsuranceTypeDOList = this.sessionFactory.getCurrentSession().getNamedQuery(InsuranceTypeDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, insuranceTypeDO.getName())
					.list();
					if(InsuranceTypeDOList != null && InsuranceTypeDOList.size() > 0){
						persistStatus =  false;
					}else{
						dao.persist(insuranceTypeDO);
					}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	public List<InsuranceTypeDO> retrieve(){
		List<InsuranceTypeDO> insuranceTypeList = null;
		try {
			insuranceTypeList = dao.retrieve(InsuranceTypeDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return insuranceTypeList;
	}
	
	/*public List<InsuranceTypeDO> retrieveActive(){
		List<InsuranceTypeDO> addressTypeList = null;
		try {
			addressTypeList =  dao.retrieveActive(CommonConstants.ACTIVE, InsuranceTypeDO.FIND_BY_STATUS, InsuranceTypeDO.class);
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return addressTypeList;
	}*/
	
	@SuppressWarnings("unchecked")
	public List<InsuranceTypeDO> retrieveById(Long id){
		 List<InsuranceTypeDO> insuranceTypeList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(InsuranceTypeDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return insuranceTypeList;
	}
	
	@SuppressWarnings("unchecked")
	public boolean update(InsuranceTypeDO InsuranceTypeDO) {
		boolean updateStatus = true;
		try {
			List<InsuranceTypeDO> insuranceTypeList = null;
			insuranceTypeList = this.sessionFactory.getCurrentSession().getNamedQuery(InsuranceTypeDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, InsuranceTypeDO.getName())
					.setParameter(CommonConstants.ID, InsuranceTypeDO.getInsuranceTypeId())
					.list();
			if(insuranceTypeList != null && insuranceTypeList.size() > 0){
				updateStatus = false;
			}else{
				dao.merge(InsuranceTypeDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}
	
	/*public boolean persistList(List<InsuranceTypeDO> addressTypeList){
		try {
			for (InsuranceTypeDO addressType : addressTypeList) {
				dao.persist(addressType);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}*/

}
