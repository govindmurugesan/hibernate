package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.EmployeeFamilyDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class EmployeeFamilyDAO {
	static Logger logger = Logger.getLogger(EmployeeFamilyDAO.class.getName());
	@Autowired
	private GenericDAOImpl<EmployeeFamilyDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;

	
	public EmployeeFamilyDO persist(EmployeeFamilyDO employeeFamilyDO){
		try {
			genericObject.persist(employeeFamilyDO);
		} catch (Exception eException) {
		} finally {
		}
		return employeeFamilyDO;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeFamilyDO> retrieveByEmpId(String empId){
		List<EmployeeFamilyDO> employeeFamilyList = null;
		try {
			
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeFamilyDO.FIND_BY_EMPID)
			.setParameter(CommonConstants.EMPID, empId)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeFamilyList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeFamilyDO> retrieveById(Long id){
		List<EmployeeFamilyDO> employeeFamilyList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeFamilyDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeFamilyList;
	}
	
	public EmployeeFamilyDO update(EmployeeFamilyDO employeeFamilyDO){
		try {
			genericObject.merge(employeeFamilyDO);
		} catch (Exception eException) {
		} finally {
		}
		return employeeFamilyDO;
	}

}
