package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.ShiftDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class ShiftDAO {
	static Logger logger = Logger.getLogger(ShiftDAO.class.getName());
	
	@Autowired
	private GenericDAOImpl<ShiftDO> dao;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public boolean persist(ShiftDO ShiftDO){
		boolean persistStatus = true;
		try {
			dao.persist(ShiftDO);
			/*List<ShiftDO> ShiftDOList = null;
			ShiftDOList = this.sessionFactory.getCurrentSession().getNamedQuery(ShiftDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, ShiftDO.getName())
					.list();
					if(ShiftDOList != null && ShiftDOList.size() > 0){
						persistStatus =  false;
					}else{
						dao.persist(ShiftDO);
					}*/
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	public List<ShiftDO> retrieve(){
		List<ShiftDO> shiftList = null;
		try {
			shiftList = dao.retrieve(ShiftDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return shiftList;
	}
	
	public List<ShiftDO> retrieveActive(){
		List<ShiftDO> shiftList = null;
		try {
			shiftList =  dao.retrieveActive(CommonConstants.ACTIVE, ShiftDO.FIND_BY_STATUS, ShiftDO.class);
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return shiftList;
	}
	
	@SuppressWarnings("unchecked")
	public List<ShiftDO> retrieveById(Long id){
		 List<ShiftDO> addressTypeList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(ShiftDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return addressTypeList;
	}
	
	@SuppressWarnings("unchecked")
	public List<ShiftDO> retrieveByName(String name){
		 List<ShiftDO> addressTypeList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(ShiftDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, name)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return addressTypeList;
	}
	
	@SuppressWarnings("unchecked")
	public boolean update(ShiftDO shiftDO) {
		boolean updateStatus = true;
		try {
			List<ShiftDO> ShiftDOList = null;
			ShiftDOList = this.sessionFactory.getCurrentSession().getNamedQuery(ShiftDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, shiftDO.getName())
					.setParameter(CommonConstants.ID, shiftDO.getShiftId())
					.list();
			if(ShiftDOList != null && ShiftDOList.size() > 0){
				updateStatus = false;
			}else{
				dao.merge(shiftDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}
	
	public boolean persistList(List<ShiftDO> shiftList){
		try {
			for (ShiftDO shift : shiftList) {
				dao.persist(shift);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}

}
