package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.PtDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class PtDAO {
	static Logger logger = Logger.getLogger(PtDAO.class.getName());

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private GenericDAOImpl<PtDO> genericObject;
	
	@SuppressWarnings("unchecked")
	public boolean persist(PtDO ptDO) {
		boolean persistStatus = true;
		try {
			List<PtDO> ptDOList = null;
			ptDOList =  this.sessionFactory.getCurrentSession().getNamedQuery(PtDO.FIND_BY_DUPLICATE_PERSIST)
					.setParameter(CommonConstants.STATE_ID, ptDO.getState().getStateId())
					.setParameter(CommonConstants.COUNTRY_ID, ptDO.getCountry().getCountryId())
					.setParameter(CommonConstants.FROMAMOUNT, ptDO.getFromamount())
					.setParameter(CommonConstants.TOAMOUNT, ptDO.getToamount())
					.list();
			if(ptDOList != null && ptDOList.size() > 0){
				persistStatus = false;
			}else{
				genericObject.persist(ptDO);
			}
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
		}
		return persistStatus;
	}
	
	/*public List<PtDO> retrieve() {
		List<PtDO> ptDOList = null;
		try {
			ptDOList = genericObject.retrieve(PtDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return ptDOList;
	}*/
	
	@SuppressWarnings("unchecked")
	public List<PtDO> retrieve() {
		List<PtDO> ptDOList = null;
		try {
			ptDOList =  this.sessionFactory.getCurrentSession().getNamedQuery(PtDO.FIND_ALL)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return ptDOList;
	}
	
	public List<PtDO> retrieveActive() {
		List<PtDO> ptDOList = null;
		try {
			ptDOList =  genericObject.retrieveActive(CommonConstants.ACTIVE, PtDO.FIND_BY_STATUS, PtDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return ptDOList;
	}
	
	@SuppressWarnings("unchecked")
	public List<PtDO> retrieveActiveById(Long Id) {
		List<PtDO> ptDOList = null;
		try {
			ptDOList =  this.sessionFactory.getCurrentSession().getNamedQuery(PtDO.FIND_BY_ID)
							.setParameter(CommonConstants.ID, Id).list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return ptDOList;
	}
	
	@SuppressWarnings("unchecked")
	public List<PtDO> retrieveByState(Long stateId) {
		List<PtDO> ptDOList = null;
		try {
			ptDOList = this.sessionFactory.getCurrentSession().getNamedQuery(PtDO.FIND_BY_STATE)
										.setParameter(CommonConstants.ID, stateId)
										.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
										.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return ptDOList;
	}
	
	@SuppressWarnings("unchecked")
	public List<PtDO> retrieveByCtcAndState(Long ctc, Long stateId ) {
		List<PtDO> ptList = null;
		try {
			ptList = this.sessionFactory.getCurrentSession().getNamedQuery(PtDO.FIND_FOR_PAYROLL)
										.setParameter(CommonConstants.STATE_ID, stateId)
										.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
										.setParameter(CommonConstants.AMOUNT, ctc)
										.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return ptList;
	}
	
	@SuppressWarnings("unchecked")
	public boolean update(PtDO ptDO) {
		boolean updateStatus = true;
		try {
			List<PtDO> ptDOList = null;
			ptDOList =  this.sessionFactory.getCurrentSession().getNamedQuery(PtDO.FIND_BY_DUPLICATE_UPDATE)
					.setParameter(CommonConstants.STATE_ID, ptDO.getState().getStateId())
					.setParameter(CommonConstants.COUNTRY_ID, ptDO.getCountry().getCountryId())
					.setParameter(CommonConstants.FROMAMOUNT, ptDO.getFromamount())
					.setParameter(CommonConstants.TOAMOUNT, ptDO.getToamount())
					.setParameter(CommonConstants.PTAMOUNT, ptDO.getPtamount())
					.setParameter(CommonConstants.ID, ptDO.getPtId())
					.list();
			if(ptDOList != null && ptDOList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(ptDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}
	
	public boolean persistList(List<PtDO> ptList){
		try {
			for (PtDO pt : ptList) {
				genericObject.persist(pt);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}

}
