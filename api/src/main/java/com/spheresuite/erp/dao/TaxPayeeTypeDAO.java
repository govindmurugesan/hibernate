package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.TaxPayeeTypeDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class TaxPayeeTypeDAO {
	static Logger logger = Logger.getLogger(TaxPayeeTypeDAO.class.getName());

	@Autowired
	private GenericDAOImpl<TaxPayeeTypeDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(TaxPayeeTypeDO taxPayeeType) {
		boolean persistStatus = true;
		List<TaxPayeeTypeDO> taxPayeeTypeList = null;
		try {
			taxPayeeTypeList = this.sessionFactory.getCurrentSession().getNamedQuery(TaxPayeeTypeDO.FIND_BY_NAME)
			.setParameter(CommonConstants.NAME, taxPayeeType.getTaxpayeeType())
			.list();
			if(taxPayeeTypeList != null && taxPayeeTypeList.size() > 0){
				persistStatus = false;
			}else{
				genericObject.persist(taxPayeeType);
			}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	public List<TaxPayeeTypeDO> retrieve() {
		List<TaxPayeeTypeDO> requestTypeList = null;
		try {
			requestTypeList = genericObject.retrieve(TaxPayeeTypeDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return requestTypeList;
	}
	
	@SuppressWarnings("unchecked")
	public boolean update(TaxPayeeTypeDO requestType) {
		boolean updateStatus = true;
		try {
			List<TaxPayeeTypeDO> taxPayeeTypeList = null;
			taxPayeeTypeList = this.sessionFactory.getCurrentSession().getNamedQuery(TaxPayeeTypeDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, requestType.getTaxpayeeType())
					.setParameter(CommonConstants.ID, requestType.getTaxPayeeTypeId())
					.list();
			if(taxPayeeTypeList != null && taxPayeeTypeList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(requestType);
			}
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}
	
	@SuppressWarnings("unchecked")
	public List<TaxPayeeTypeDO> retrieveById(Long Id) {
		List<TaxPayeeTypeDO> taxPayeeTypeList = null;
		try {
			taxPayeeTypeList =  this.sessionFactory.getCurrentSession().getNamedQuery(TaxPayeeTypeDO.FIND_BY_ID)
									.setParameter(CommonConstants.ID, Id).list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return taxPayeeTypeList;
	}
	
	public List<TaxPayeeTypeDO> retrieveActive(char status) {
		List<TaxPayeeTypeDO> taxPayeeTypeList = null;
		try {
			taxPayeeTypeList =  genericObject.retrieveActive(CommonConstants.ACTIVE, TaxPayeeTypeDO.FIND_BY_STATUS, TaxPayeeTypeDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return taxPayeeTypeList;
	}
}
