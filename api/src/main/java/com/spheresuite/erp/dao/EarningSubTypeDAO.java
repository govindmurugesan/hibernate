package com.spheresuite.erp.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.EarningSubTypeDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class EarningSubTypeDAO {
	@Autowired
	private GenericDAOImpl<EarningSubTypeDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(EarningSubTypeDO EarningSubTypeDO){
		List<EarningSubTypeDO> earningTypeList = null;
		boolean persistStatus = true;
		try {
			earningTypeList = this.sessionFactory.getCurrentSession().getNamedQuery(EarningSubTypeDO.FIND_BY_NAME)
			.setParameter(CommonConstants.NAME, EarningSubTypeDO.getName())
			.list();
			if(earningTypeList != null && earningTypeList.size() > 0){
				persistStatus = false;
			}else{
				genericObject.persist(EarningSubTypeDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	public List<EarningSubTypeDO> retrieve(){
		List<EarningSubTypeDO> earningTypeList = null;
		try {
			earningTypeList = genericObject.retrieve(EarningSubTypeDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return earningTypeList;
	}
	
	public List<EarningSubTypeDO> retrieveActive(){
		List<EarningSubTypeDO> earningTypeList = null;
		try {
			return genericObject.retrieveActive(CommonConstants.ACTIVE, EarningSubTypeDO.FIND_BY_STATUS, EarningSubTypeDO.class);
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return earningTypeList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EarningSubTypeDO> retrieveById(Long id) {
		List<EarningSubTypeDO> earningTypeList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EarningSubTypeDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return earningTypeList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EarningSubTypeDO> retrieveByName(String name) {
		List<EarningSubTypeDO> earningTypeList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EarningSubTypeDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, name)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return earningTypeList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EarningSubTypeDO> retrieveByEarningId(Long id) {
		List<EarningSubTypeDO> earningTypeList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EarningSubTypeDO.FIND_BY_EARNINGTYPE_ID)
					.setParameter(CommonConstants.ID, id)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return earningTypeList;
	}
	
	@SuppressWarnings("unchecked")
	public boolean update(EarningSubTypeDO allowanceTypeDO){
		boolean updateStatus = true;
		try {
			List<EarningSubTypeDO> earningTypeList = null;
			earningTypeList = this.sessionFactory.getCurrentSession().getNamedQuery(EarningSubTypeDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, allowanceTypeDO.getName())
					.setParameter(CommonConstants.ID, allowanceTypeDO.getEarningSubTypeId())
					.list();
			if(earningTypeList != null && earningTypeList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(allowanceTypeDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}
	
	public boolean persistList(List<EarningSubTypeDO> designationList){
		try {
			for (EarningSubTypeDO earningType : designationList) {
				genericObject.persist(earningType);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}

}
