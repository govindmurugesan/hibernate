package com.spheresuite.erp.dao;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.EmployeeShiftDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class EmployeeShiftDAO {
	static Logger logger = Logger.getLogger(EmployeeShiftDAO.class.getName());
	
	@Autowired
	private GenericDAOImpl<EmployeeShiftDO> dao;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public boolean persist(EmployeeShiftDO EmployeeShiftDO){
		boolean persistStatus = true;
		try {
			dao.persist(EmployeeShiftDO);
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	public List<EmployeeShiftDO> retrieve(){
		List<EmployeeShiftDO> shiftList = null;
		try {
			shiftList = dao.retrieve(EmployeeShiftDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return shiftList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<EmployeeShiftDO> retrieveById(Long id){
		 List<EmployeeShiftDO> shiftList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeShiftDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return shiftList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeShiftDO> retrieveByDate(Date date) {
		List<EmployeeShiftDO> shiftList = null;
		try {
			shiftList = this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeShiftDO.FIND_BY_DATE)
										.setParameter(CommonConstants.DATE, date)
										.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return shiftList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeShiftDO> retrieveByEmpDate(String id, Date date) {
		List<EmployeeShiftDO> shiftList = null;
		try {
			shiftList = this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeShiftDO.FIND_BY_EMP_DATE)
										.setParameter(CommonConstants.ID, id)
										.setParameter(CommonConstants.DATE, date)
										.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return shiftList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeShiftDO> retrieveByDateValidate(Long id) {
		List<EmployeeShiftDO> shiftList = null;
		try {
			shiftList = this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeShiftDO.FIND_BY_DATE_VALIDATE)
										.setParameter(CommonConstants.ID, id)
										.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return shiftList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeShiftDO> retrieveByEmpId(String string){
		 List<EmployeeShiftDO> shiftList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeShiftDO.FIND_BY_EMP_ID)
					.setParameter(CommonConstants.ID, string)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return shiftList;
	}
	
	public boolean update(EmployeeShiftDO EmployeeShiftDO) {
		boolean updateStatus = true;
		try {
				dao.merge(EmployeeShiftDO);
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}
	
	public boolean persistList(List<EmployeeShiftDO> shiftList){
		try {
			for (EmployeeShiftDO shift : shiftList) {
				dao.persist(shift);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}

}
