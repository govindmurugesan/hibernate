package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.WorkingDayAllowanceDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class WorkingDayAllowanceDAO {
	static Logger logger = Logger.getLogger(WorkingDayAllowanceDAO.class.getName());
	
	@Autowired
	private GenericDAOImpl<WorkingDayAllowanceDO> dao;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public WorkingDayAllowanceDO persist(WorkingDayAllowanceDO workingDayAllowanceDO){
		try {
			dao.persist(workingDayAllowanceDO);
		} catch (Exception eException) {
		} finally {
		}
		return workingDayAllowanceDO;
	}
	
	@SuppressWarnings("unchecked")
	public List<WorkingDayAllowanceDO> retrieve(){
		List<WorkingDayAllowanceDO> workingDayAllowanceList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(WorkingDayAllowanceDO.FIND_ALL)
					.list();
		} catch (Exception eException) {
		} finally {
		}
		return workingDayAllowanceList;
	}
	
	public List<WorkingDayAllowanceDO> retrieveActive(){
		List<WorkingDayAllowanceDO> workingDayAllowanceList = null;
		try {
			workingDayAllowanceList =  dao.retrieveActive(CommonConstants.ACTIVE, WorkingDayAllowanceDO.FIND_BY_STATUS, WorkingDayAllowanceDO.class);
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return workingDayAllowanceList;
	}
	
	@SuppressWarnings("unchecked")
	public List<WorkingDayAllowanceDO> retrieveById(Long id){
		 List<WorkingDayAllowanceDO> workingDayAllowanceList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(WorkingDayAllowanceDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return workingDayAllowanceList;
	}
	
	public boolean update(WorkingDayAllowanceDO workingDayAllowanceDO) {
		boolean updateStatus = true;
		try {
			dao.merge(workingDayAllowanceDO);
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}
	
}
