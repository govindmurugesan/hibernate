package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.OffersDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class OffersDAO {
	static Logger logger = Logger.getLogger(OffersDAO.class.getName());

	@Autowired
	private GenericDAOImpl<OffersDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public OffersDO persist(OffersDO offersDO) {
		try {
			genericObject.persist(offersDO);
		} catch (Exception eException) {
			//throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			//SessionManager.closeEntityManager(em);
		}
		return offersDO;
	}
	
	public OffersDO update(OffersDO offersDO) {
		try {
			genericObject.merge(offersDO);
		} catch (Exception eException) {
		} finally {
		}
		return offersDO;
	}
	
	@SuppressWarnings("unchecked")
	public List<OffersDO> retrieve() {
		List<OffersDO> offersList = null;
		try {
			offersList = this.sessionFactory.getCurrentSession().getNamedQuery(OffersDO.FIND_ALL).list();
		} catch (Exception eException) {
		} finally {
		}
		return offersList;
	}
	
	public List<OffersDO> retrieveByStatus(char status) {
		List<OffersDO> offersList = null;
		try {
			offersList =  genericObject.retrieveActive(status, OffersDO.FIND_BY_STATUS, OffersDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return offersList;
	}
	
	@SuppressWarnings("unchecked")
	public List<OffersDO> retrieveByProjectId(Long id) {
		List<OffersDO> offersList = null;
		try {
			offersList = this.sessionFactory.getCurrentSession().getNamedQuery(OffersDO.FIND_BY_PROJECTID)
										.setParameter(CommonConstants.ID, id)
										.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return offersList;
	}
	
	@SuppressWarnings("unchecked")
	public List<OffersDO> retrieveById(Long Id) {
		List<OffersDO> offersList = null;
		try {
			offersList =  this.sessionFactory.getCurrentSession().getNamedQuery(OffersDO.FIND_BY_ID)
								.setParameter(CommonConstants.ID, Id).list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return offersList;
	}
}
