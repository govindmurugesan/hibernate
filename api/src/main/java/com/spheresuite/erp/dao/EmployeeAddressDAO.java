package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.EmployeeAddressDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class EmployeeAddressDAO {
	static Logger logger = Logger.getLogger(EmployeeAddressDAO.class.getName());
	@Autowired
	private GenericDAOImpl<EmployeeAddressDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public EmployeeAddressDO persist(EmployeeAddressDO employeeEducationDO) {
		try {
			genericObject.persist(employeeEducationDO);
		} catch (Exception eException) {
		} finally {
		}
		return employeeEducationDO;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeAddressDO> retrieveByEmpId(String empId) {
		List<EmployeeAddressDO> employeeAddressList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeAddressDO.FIND_BY_EMPID)
					.setParameter(CommonConstants.EMPID, empId)
					.list(); 
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeAddressList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeAddressDO> retrieveById(Long id) {
		List<EmployeeAddressDO> employeeAddressList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeAddressDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeAddressList;
	}
	
	public EmployeeAddressDO update(EmployeeAddressDO employeeEducationDO) {
		try {
			genericObject.merge(employeeEducationDO);
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return employeeEducationDO;
	}

}
