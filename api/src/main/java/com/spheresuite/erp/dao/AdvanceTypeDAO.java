package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.AdvanceTypeDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class AdvanceTypeDAO {
	static Logger logger = Logger.getLogger(AdvanceTypeDAO.class.getName());
	
	@Autowired
	private GenericDAOImpl<AdvanceTypeDO> dao;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(AdvanceTypeDO advanceTypeDO){
		boolean persistStatus = true;
		try {
			List<AdvanceTypeDO> advanceTypeDOList = null;
			advanceTypeDOList = this.sessionFactory.getCurrentSession().getNamedQuery(AdvanceTypeDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, advanceTypeDO.getName())
					.list();
					if(advanceTypeDOList != null && advanceTypeDOList.size() > 0){
						persistStatus =  false;
					}else{
						dao.persist(advanceTypeDO);
					}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	public List<AdvanceTypeDO> retrieve(){
		List<AdvanceTypeDO> advanceTypeDOList = null;
		try {
			advanceTypeDOList = dao.retrieve(AdvanceTypeDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return advanceTypeDOList;
	}
	
	/*public List<AdvanceTypeDO> retrieveActive(){
		List<AdvanceTypeDO> addressTypeList = null;
		try {
			addressTypeList =  dao.retrieveActive(CommonConstants.ACTIVE, AdvanceTypeDO.FIND_BY_STATUS, AdvanceTypeDO.class);
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return addressTypeList;
	}*/
	
	@SuppressWarnings("unchecked")
	public List<AdvanceTypeDO> retrieveById(Long id){
		 List<AdvanceTypeDO> advanceTypeList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(AdvanceTypeDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return advanceTypeList;
	}
	
	@SuppressWarnings("unchecked")
	public boolean update(AdvanceTypeDO advanceTypeDO) {
		boolean updateStatus = true;
		try {
			List<AdvanceTypeDO> AdvanceTypeDOList = null;
			AdvanceTypeDOList = this.sessionFactory.getCurrentSession().getNamedQuery(AdvanceTypeDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, advanceTypeDO.getName())
					.setParameter(CommonConstants.ID, advanceTypeDO.getAdvanceTypeId())
					.list();
			if(AdvanceTypeDOList != null && AdvanceTypeDOList.size() > 0){
				updateStatus = false;
			}else{
				dao.merge(advanceTypeDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}
	
	/*public boolean persistList(List<AdvanceTypeDO> addressTypeList){
		try {
			for (AdvanceTypeDO addressType : addressTypeList) {
				dao.persist(addressType);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}*/

}
