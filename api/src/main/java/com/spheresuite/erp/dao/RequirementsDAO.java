package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.RequirementsDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class RequirementsDAO {
	static Logger logger = Logger.getLogger(RequirementsDAO.class.getName());
	
	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private GenericDAOImpl<RequirementsDO> genericObject;
	
	public RequirementsDO persist(RequirementsDO requirementsDO) {
		try {
			genericObject.persist(requirementsDO);
		} catch (Exception eException) {
			//throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			//SessionManager.closeEntityManager(em);
		}
		return requirementsDO;
	}
	
	public RequirementsDO update(RequirementsDO requirementsDO) {
		try {
			genericObject.merge(requirementsDO);
		} catch (Exception eException) {
		} finally {
		}
		return requirementsDO;
	}
	
	public List<RequirementsDO> retrieve() {
		List<RequirementsDO> requirementsList = null;
		try {
			requirementsList = genericObject.retrieve(RequirementsDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return requirementsList;
	}
	
	public List<RequirementsDO> retrieveByStatus(char status) {
		List<RequirementsDO> requirementsList = null;
		try {
			requirementsList =  genericObject.retrieveActive(status, RequirementsDO.FIND_BY_STATUS, RequirementsDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return requirementsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<RequirementsDO> retrieveByProjectId(Long id) {
		List<RequirementsDO> requirementsList = null;
		try {
			requirementsList = this.sessionFactory.getCurrentSession().getNamedQuery(RequirementsDO.FIND_BY_PROJECTID)
										.setParameter(CommonConstants.ID, id)
										.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return requirementsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<RequirementsDO> retrieveByCustomerId(Long id) {
		List<RequirementsDO> requirementsList = null;
		try {
			requirementsList = this.sessionFactory.getCurrentSession().getNamedQuery(RequirementsDO.FIND_BY_CUSTOMERID)
										.setParameter(CommonConstants.ID, id)
										.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return requirementsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<RequirementsDO> retrieveById(Long Id) {
		List<RequirementsDO> requirementsList = null;
		try {
			requirementsList =  this.sessionFactory.getCurrentSession().getNamedQuery(RequirementsDO.FIND_BY_ID)
									.setParameter(CommonConstants.ID, Id).list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return requirementsList;
	}
}
