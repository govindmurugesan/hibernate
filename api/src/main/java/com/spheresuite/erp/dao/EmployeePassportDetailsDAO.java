package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.EmployeePassportDetailsDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class EmployeePassportDetailsDAO {
	static Logger logger = Logger.getLogger(EmployeePassportDetailsDAO.class.getName());
	@Autowired
	private GenericDAOImpl<EmployeePassportDetailsDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public EmployeePassportDetailsDO persist(EmployeePassportDetailsDO employeePassportDetailsDO) {
		try {
			genericObject.persist(employeePassportDetailsDO);
		} catch (Exception eException) {
		} finally {
		}
		return employeePassportDetailsDO;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeePassportDetailsDO> retrieveByEmpId(String empId) {
		List<EmployeePassportDetailsDO> employeePassportInfoList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeePassportDetailsDO.FIND_BY_EMPID)
					.setParameter(CommonConstants.EMPID, empId)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeePassportInfoList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeePassportDetailsDO> retrieveById(Long id){
		List<EmployeePassportDetailsDO> employeePassportInfoList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeePassportDetailsDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeePassportInfoList;
	}
	
	public EmployeePassportDetailsDO update(EmployeePassportDetailsDO employeePassportDetailsDO){
		try {
			genericObject.merge(employeePassportDetailsDO);
		} catch (Exception eException) {
		} finally {
		}
		return employeePassportDetailsDO;
	}

}
