package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.AddressTypeDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class AddressTypeDAO {
	static Logger logger = Logger.getLogger(AddressTypeDAO.class.getName());
	
	@Autowired
	private GenericDAOImpl<AddressTypeDO> dao;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(AddressTypeDO addressTypeDO){
		boolean persistStatus = true;
		try {
			List<AddressTypeDO> addressTypeDOList = null;
			addressTypeDOList = this.sessionFactory.getCurrentSession().getNamedQuery(AddressTypeDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, addressTypeDO.getName())
					.list();
					if(addressTypeDOList != null && addressTypeDOList.size() > 0){
						persistStatus =  false;
					}else{
						dao.persist(addressTypeDO);
					}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	public List<AddressTypeDO> retrieve(){
		List<AddressTypeDO> addressTypeList = null;
		try {
			addressTypeList = dao.retrieve(AddressTypeDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return addressTypeList;
	}
	
	public List<AddressTypeDO> retrieveActive(){
		List<AddressTypeDO> addressTypeList = null;
		try {
			addressTypeList =  dao.retrieveActive(CommonConstants.ACTIVE, AddressTypeDO.FIND_BY_STATUS, AddressTypeDO.class);
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return addressTypeList;
	}
	
	@SuppressWarnings("unchecked")
	public List<AddressTypeDO> retrieveById(Long id){
		 List<AddressTypeDO> addressTypeList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(AddressTypeDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return addressTypeList;
	}
	
	@SuppressWarnings("unchecked")
	public boolean update(AddressTypeDO addressTypeDO) {
		boolean updateStatus = true;
		try {
			List<AddressTypeDO> addressTypeDOList = null;
			addressTypeDOList = this.sessionFactory.getCurrentSession().getNamedQuery(AddressTypeDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, addressTypeDO.getName())
					.setParameter(CommonConstants.ID, addressTypeDO.getAddressTypeId())
					.list();
			if(addressTypeDOList != null && addressTypeDOList.size() > 0){
				updateStatus = false;
			}else{
				dao.merge(addressTypeDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}
	
	public boolean persistList(List<AddressTypeDO> addressTypeList){
		try {
			for (AddressTypeDO addressType : addressTypeList) {
				dao.persist(addressType);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}

}
