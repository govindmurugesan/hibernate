package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.LeadEmailDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class LeadEmailDAO {
	static Logger logger = Logger.getLogger(LeadEmailDAO.class.getName());
	@Autowired
	private GenericDAOImpl<LeadEmailDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	
	
	public LeadEmailDO persist(LeadEmailDO leadEmailDO)  {
		try {
			genericObject.persist(leadEmailDO);
		} catch (Exception eException) {
		} finally {
		}
		return leadEmailDO;
	}
	
	public boolean persistList(List<LeadEmailDO> leadEmailDO)  {
		try {
			for (LeadEmailDO leadEmailDO2 : leadEmailDO) {
				genericObject.persist(leadEmailDO2);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<LeadEmailDO> retrieveByLead(Long leadId)  {
		List<LeadEmailDO> leadEmailList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeadEmailDO.FIND_BY_LEAD_ID)
			.setParameter(CommonConstants.LEAD_ID, leadId)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return leadEmailList;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeadEmailDO> retrieveById(Long Id)  {
		List<LeadEmailDO> leadEmailList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeadEmailDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, Id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return leadEmailList;
	}
	

	public Boolean delete(String Id)  {
		boolean flag = false;
		try {
			this.sessionFactory.getCurrentSession().getNamedQuery(LeadEmailDO.DELETE_BY_ID)
			.setParameter(CommonConstants.ID, Id)
			.executeUpdate();
			flag = true;
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return flag;
	}
	
	
	public List<LeadEmailDO> retrieveAllMail()  {
		List<LeadEmailDO> leadEmailList = null;
		try {
			leadEmailList = genericObject.retrieve(LeadEmailDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return leadEmailList;
	}
	
}
