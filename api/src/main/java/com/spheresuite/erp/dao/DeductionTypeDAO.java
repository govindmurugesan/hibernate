package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.DeductionTypeDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class DeductionTypeDAO {
	static Logger logger = Logger.getLogger(DeductionTypeDAO.class.getName());
	
	@Autowired
	private GenericDAOImpl<DeductionTypeDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(DeductionTypeDO deductionTypeDO) {
		List<DeductionTypeDO> deductionTypeList = null;
		boolean persistStatus = true;
		try {
			deductionTypeList = this.sessionFactory.getCurrentSession().getNamedQuery(DeductionTypeDO.FIND_BY_NAME)
			.setParameter(CommonConstants.NAME, deductionTypeDO.getName())
			.list();
			if(deductionTypeList != null && deductionTypeList.size() > 0){
				persistStatus = false;
			}else{
				genericObject.persist(deductionTypeDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	public List<DeductionTypeDO> retrieve() {
		List<DeductionTypeDO> deductionTypeList = null;
		try {
			deductionTypeList = genericObject.retrieve(DeductionTypeDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return deductionTypeList;
	}
	
	public List<DeductionTypeDO> retrieveActive() {
		List<DeductionTypeDO> deductionTypeList = null;
		try {
			return genericObject.retrieveActive(CommonConstants.ACTIVE, DeductionTypeDO.FIND_BY_STATUS, DeductionTypeDO.class);
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return deductionTypeList;
	}
	
	@SuppressWarnings("unchecked")
	public List<DeductionTypeDO> retrieveById(Long id) {
		List<DeductionTypeDO> deductionTypeList = null;
		
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(DeductionTypeDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return deductionTypeList;
	}
	
	@SuppressWarnings("unchecked")
	public List<DeductionTypeDO> retrieveByName(String name) {
		List<DeductionTypeDO> deductionTypeList = null;
		
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(DeductionTypeDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, name)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return deductionTypeList;
	}
	
	@SuppressWarnings("unchecked")
	public boolean update(DeductionTypeDO deductionTypeDO) {
		boolean updateStatus = true;
		try {
			List<DeductionTypeDO> deductionTypeList = null;
			deductionTypeList = this.sessionFactory.getCurrentSession().getNamedQuery(DeductionTypeDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, deductionTypeDO.getName())
					.setParameter(CommonConstants.ID, deductionTypeDO.getDeductionTypeId())
					.list();
			if(deductionTypeList != null && deductionTypeList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(deductionTypeDO);
			}
			
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}
	
	public boolean persistList(List<DeductionTypeDO> deducationTypeList){
		try {
			for (DeductionTypeDO deducationType : deducationTypeList) {
				genericObject.persist(deducationType);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}

}
