package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.EmployeeBankInformationDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class EmployeeBankInformationDAO {
	static Logger logger = Logger.getLogger(EmployeeBankInformationDAO.class.getName());
	@Autowired
	private GenericDAOImpl<EmployeeBankInformationDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public EmployeeBankInformationDO persist(EmployeeBankInformationDO employeeBankInformationDO) {
		try {
			genericObject.persist(employeeBankInformationDO);
		} catch (Exception eException) {
		} finally {
		}
		return employeeBankInformationDO;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeBankInformationDO> retrieveByEmpId(String empId) {
		List<EmployeeBankInformationDO> employeeBankInformationList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeBankInformationDO.FIND_BY_EMPID)
					.setParameter(CommonConstants.EMPID, empId)
					.list(); 
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeBankInformationList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeBankInformationDO> retrieveById(Long id) {
		List<EmployeeBankInformationDO> employeeBankInformationList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeBankInformationDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeBankInformationList;
	}
	
	public EmployeeBankInformationDO update(EmployeeBankInformationDO employeeBankInformationDO) {
		try {
			genericObject.merge(employeeBankInformationDO);
		} catch (Exception eException) {
		} finally {
		}
		return employeeBankInformationDO;
	}
	
	public boolean persistList(List<EmployeeBankInformationDO> employeeBankInfoList){
		try {
			for (EmployeeBankInformationDO employeeBankInfo : employeeBankInfoList) {
				genericObject.persist(employeeBankInfo);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}

}
