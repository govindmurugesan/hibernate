package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.IssueTypeDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class IssueTypeDAO {
	static Logger logger = Logger.getLogger(IssueTypeDAO.class.getName());
	
	@Autowired
	private GenericDAOImpl<IssueTypeDO> dao;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(IssueTypeDO issueTypeDO){
		boolean persistStatus = true;
		try {
			List<IssueTypeDO> IssueTypeDOList = null;
			IssueTypeDOList = this.sessionFactory.getCurrentSession().getNamedQuery(IssueTypeDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, issueTypeDO.getName())
					.list();
					if(IssueTypeDOList != null && IssueTypeDOList.size() > 0){
						persistStatus =  false;
					}else{
						dao.persist(issueTypeDO);
					}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	public List<IssueTypeDO> retrieve(){
		List<IssueTypeDO> IssueTypeDOList = null;
		try {
			IssueTypeDOList = dao.retrieve(IssueTypeDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return IssueTypeDOList;
	}
	
	/*public List<IssueTypeDO> retrieveActive(){
		List<IssueTypeDO> addressTypeList = null;
		try {
			addressTypeList =  dao.retrieveActive(CommonConstants.ACTIVE, IssueTypeDO.FIND_BY_STATUS, IssueTypeDO.class);
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return addressTypeList;
	}*/
	
	@SuppressWarnings("unchecked")
	public List<IssueTypeDO> retrieveById(Long id){
		 List<IssueTypeDO> issueTypeList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(IssueTypeDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return issueTypeList;
	}
	
	@SuppressWarnings("unchecked")
	public boolean update(IssueTypeDO issueTypeDO) {
		boolean updateStatus = true;
		try {
			List<IssueTypeDO> IssueTypeDOList = null;
			IssueTypeDOList = this.sessionFactory.getCurrentSession().getNamedQuery(IssueTypeDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, issueTypeDO.getName())
					.setParameter(CommonConstants.ID, issueTypeDO.getIssueTypeId())
					.list();
			if(IssueTypeDOList != null && IssueTypeDOList.size() > 0){
				updateStatus = false;
			}else{
				dao.merge(issueTypeDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}
	
	/*public boolean persistList(List<IssueTypeDO> addressTypeList){
		try {
			for (IssueTypeDO addressType : addressTypeList) {
				dao.persist(addressType);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}*/

}
