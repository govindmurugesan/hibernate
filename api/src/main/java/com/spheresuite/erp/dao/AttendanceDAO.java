package com.spheresuite.erp.dao;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.AttendanceDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class AttendanceDAO {
	static Logger logger = Logger.getLogger(AttendanceDAO.class.getName());
	@Autowired
	private GenericDAOImpl<AttendanceDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public boolean persist(AttendanceDO attendanceDO)  {
		boolean persistStatus = true;
		try {
			genericObject.persist(attendanceDO);
			/*List<DesignationDO> DesignationDOList = null;
			DesignationDOList = this.sessionFactory.getCurrentSession().getNamedQuery(DesignationDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, designationDO.getName())
					.list();
					if(DesignationDOList != null && DesignationDOList.size() > 0){
						persistStatus = false;
					}else{
						genericObject.persist(designationDO);
					}*/
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
		}
		return persistStatus;
	}
	
	public boolean update(AttendanceDO attendanceDO)  {
		boolean persistStatus = true;
		try {
			genericObject.merge(attendanceDO);
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
		}
		return persistStatus;
	}
	
	public List<AttendanceDO> retrieve()  {
		List<AttendanceDO> attendanceList = null;
		try {
			attendanceList = genericObject.retrieve(AttendanceDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return attendanceList;
	}
	
	@SuppressWarnings("unchecked")
	public List<AttendanceDO> retrieveByEmpId(String id)  {
		List<AttendanceDO> attendanceList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(AttendanceDO.FIND_BY_EMPID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return attendanceList;
	}
	
	@SuppressWarnings("unchecked")
	public List<AttendanceDO> retrieveById(Long id)  {
		List<AttendanceDO> attendanceList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(AttendanceDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return attendanceList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> retrieveSpeceficField()  {
		List<Object[]> attendanceList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(AttendanceDO.FIND_SPECIFIC_FIELD)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return attendanceList;
	}
	
	@SuppressWarnings("unchecked")
	public List<AttendanceDO> retrieveByEmpIdAndDate(String id,Date fromDate, Date endDate)  {
		List<AttendanceDO> attendanceList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(AttendanceDO.FIND_BY_EMPID_TIME)
					.setParameter(CommonConstants.ID, id)
					.setParameter(CommonConstants.FROMDATE, fromDate)
					.setParameter(CommonConstants.ENDDATE, endDate)
					.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return attendanceList;
	}
	
	@SuppressWarnings("unchecked")
	public List<AttendanceDO> retrieveByEmpId_Date(String id,Date fromDate, Date endDate)  {
		List<AttendanceDO> attendanceList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(AttendanceDO.FIND_BY_EMPID_DATE)
					.setParameter(CommonConstants.ID, id)
					.setParameter(CommonConstants.FROMDATE, fromDate)
					.setParameter(CommonConstants.ENDDATE, endDate)
					.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return attendanceList;
	}
	
	public boolean persistList(List<AttendanceDO> attendanceList){
		try {
			for (AttendanceDO attendance : attendanceList) {
				genericObject.persist(attendance);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}
	
	@SuppressWarnings("unchecked")
	public List<AttendanceDO> retrieveByTeamEmp(List<String> empIds)  {
		List<AttendanceDO> attendanceList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(AttendanceDO.FIND_BY_TEAM)
					.setParameterList(CommonConstants.ID, empIds)
					.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return attendanceList;
	}
	
	/*public List<DesignationDO> retrieve()  {
		List<AttendanceDO> designationList = null;
		try {
			designationList = genericObject.retrieve(DesignationDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return designationList;
	}*/
	
	/*@SuppressWarnings("unchecked")
	public List<DesignationDO> retrieveActive()  {
		List<DesignationDO> designationList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(DesignationDO.FIND_BY_STATUS)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return designationList;
	}
	
	@SuppressWarnings("unchecked")
	public List<DesignationDO> retrieveById(Long id)  {
		List<DesignationDO> designationList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(DesignationDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return designationList;
	}
	
	@SuppressWarnings("unchecked")
	public List<DesignationDO> retrieveByName(String name)  {
		List<DesignationDO> designationList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(DesignationDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, name)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return designationList;
	}
	
	@SuppressWarnings("unchecked")
	public boolean update(DesignationDO designationDO)  {
		boolean updateStatus = true;
		try {
			List<DesignationDO> designationList = null;
			designationList = this.sessionFactory.getCurrentSession().getNamedQuery(DesignationDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, designationDO.getName())
					.setParameter(CommonConstants.ID, designationDO.getDesignationId())
					.list();
			if(designationList != null && designationList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(designationDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}
	
	public boolean persistList(List<DesignationDO> designationList){
		try {
			for (DesignationDO designation : designationList) {
				genericObject.persist(designation);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}*/
}
