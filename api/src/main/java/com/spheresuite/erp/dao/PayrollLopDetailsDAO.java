package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.PayrollLopDetailsDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class PayrollLopDetailsDAO {
	static Logger logger = Logger.getLogger(PayrollLopDetailsDO.class.getName());
	
	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private GenericDAOImpl<PayrollLopDetailsDO> genericObject;	
	
	public PayrollLopDetailsDO persist(PayrollLopDetailsDO payrollLopDetailsDO) {
		try {
			//for (PayrollEarningsDetailsDO payrollEarningsDetailsDO2 : payrollEarningsDetailsDO) {
				genericObject.persist(payrollLopDetailsDO);
			//}
		} catch (Exception eException) {
			//throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			//SessionManager.closeEntityManager(em);
		}
		return payrollLopDetailsDO;
	}
	
	
	public List<PayrollLopDetailsDO> retrieve() {
		List<PayrollLopDetailsDO> payrollEarningsDetailsList = null;
		try {
			payrollEarningsDetailsList = genericObject.retrieve(PayrollLopDetailsDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return payrollEarningsDetailsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<PayrollLopDetailsDO> retrieveByEmpId(Long id, String monthly) {
		List<PayrollLopDetailsDO> payrollEarningsDetailsList = null;
		try {
			payrollEarningsDetailsList = this.sessionFactory.getCurrentSession().getNamedQuery(PayrollLopDetailsDO.FIND_BY_EMPID)
										.setParameter(CommonConstants.ID, id)
										.setParameter(CommonConstants.MONTHLY, monthly)
										.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return payrollEarningsDetailsList;
	}
	
	/*public PayrollEarningsDetailsDO update(PayrollEarningsDetailsDO payrollEarningsDetailsDO) {
		try {
			genericObject.merge(payrollEarningsDetailsDO);
		} catch (Exception eException) {
		} finally {
		}
		return payrollEarningsDetailsDO;
	}*/
	
	@SuppressWarnings("unchecked")
	public List<PayrollLopDetailsDO> retrieveByBetweenMonth(Long id, String fromMonth, String toMonth) {
		List<PayrollLopDetailsDO> employeeEarningsList = null;
		try {
			employeeEarningsList = this.sessionFactory.getCurrentSession().getNamedQuery(PayrollLopDetailsDO.FIND_BY_EMPID_BETWEENDATES)
										.setParameter(CommonConstants.ID, id)
										.setParameter(CommonConstants.FROMDATE, fromMonth)
										.setParameter(CommonConstants.TODATE, toMonth)
										.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeEarningsList;
	}

}
