package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.LeaveGroupDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class LeaveGroupDAO {
	static Logger logger = Logger.getLogger(LeaveGroupDAO.class.getName());
	@Autowired
	private GenericDAOImpl<LeaveGroupDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(LeaveGroupDO leaveGroup) {
		boolean persistStatus = true;
		try {
			List<LeaveGroupDO> leaveGroupList = null;
			leaveGroupList = this.sessionFactory.getCurrentSession().getNamedQuery(LeaveGroupDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, leaveGroup.getLeaveGroup())
					.list();
					if(leaveGroupList != null && leaveGroupList.size() > 0){
						persistStatus = false;
					}else{
						genericObject.persist(leaveGroup);
					}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	public List<LeaveGroupDO> retrieve()  {
		List<LeaveGroupDO> leaveGroupList = null;
		try {
			leaveGroupList = genericObject.retrieve(LeaveGroupDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return leaveGroupList;
	}
	
	/*public LeaveGroupDO update(LeaveGroupDO requestType)  {
		try {
			genericObject.merge(requestType);
		} catch (Exception eException) {
		} finally {
		}
		return requestType;
	}*/
	
	@SuppressWarnings("unchecked")
	public boolean update(LeaveGroupDO leaveGroupDO) {
		boolean updateStatus = true;
		try {
			List<LeaveGroupDO> leaveGroupList = null;
			leaveGroupList = this.sessionFactory.getCurrentSession().getNamedQuery(LeaveGroupDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, leaveGroupDO.getLeaveGroup())
					.setParameter(CommonConstants.ID, leaveGroupDO.getLeaveGroupId())
					.list();
			if(leaveGroupList != null && leaveGroupList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(leaveGroupDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeaveGroupDO> retrieveById(Long Id)  {
		List<LeaveGroupDO> leaveGroupList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeaveGroupDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, Id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return leaveGroupList;
	}
	
	/*@SuppressWarnings("unchecked")
	public List<LeaveGroupDO> retrieveByType(String type)  {
		List<LeaveGroupDO> leaveTypeList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeaveGroupDO.FIND_BY_TYPE)
			.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
			.setParameter(CommonConstants.TYPE, type)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return leaveTypeList;
	}
	*/
	@SuppressWarnings("unchecked")
	public List<LeaveGroupDO> retrieveActive()  {
		List<LeaveGroupDO> leaveGroupList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeaveGroupDO.FIND_BY_STATUS)
			.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return leaveGroupList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<LeaveGroupDO> retrieveByName(String name)  {
		List<LeaveGroupDO> leaveTypeList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeaveGroupDO.FIND_BY_NAME)
			.setParameter(CommonConstants.NAME, name)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return leaveTypeList;
	}
	
	/*@SuppressWarnings("unchecked")
	public List<LeaveGroupDO> retrieveByTypeUpdate(String type, Long Id)  {
		List<LeaveGroupDO> leaveTypeList = null;
		List<Long> ids = new ArrayList<Long>();
		ids.add(Id);
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeaveGroupDO.FIND_BY_TYPE_FOR_UPDATE)
			.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
			.setParameter(CommonConstants.TYPE, type)
			.setParameter(CommonConstants.ID, ids)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return leaveTypeList;
	}
	*/
	
	public boolean persistList(List<LeaveGroupDO> leaveGroupList){
		try {
			for (LeaveGroupDO leaveGroup : leaveGroupList) {
				genericObject.persist(leaveGroup);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}

}
