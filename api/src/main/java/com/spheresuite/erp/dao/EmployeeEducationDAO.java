package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.EmployeeEducationDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class EmployeeEducationDAO {
	static Logger logger = Logger.getLogger(EmployeeEducationDAO.class.getName());
	@Autowired
	private GenericDAOImpl<EmployeeEducationDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public EmployeeEducationDO persist(EmployeeEducationDO employeeEducationDO) {
		try {
			genericObject.persist(employeeEducationDO);
		} catch (Exception eException) {
		} finally {
		}
		return employeeEducationDO;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeEducationDO> retrieveByEmpId(String empId){
		List<EmployeeEducationDO> employeeEducationList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeEducationDO.FIND_BY_EMPID)
			.setParameter(CommonConstants.EMPID, empId)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeEducationList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeEducationDO> retrieveById(Long id)  {
		List<EmployeeEducationDO> employeeEducationList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeEducationDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeEducationList;
	}
	
	public EmployeeEducationDO update(EmployeeEducationDO employeeEducationDO) {
		try {
			genericObject.merge(employeeEducationDO);
		} catch (Exception eException) {
		} finally {
		}
		return employeeEducationDO;
	}

}
