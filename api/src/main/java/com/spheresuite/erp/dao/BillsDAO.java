package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.BillsDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class BillsDAO {
	static Logger logger = Logger.getLogger(BillsDAO.class.getName());
	@Autowired
	private GenericDAOImpl<BillsDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public BillsDO persist(BillsDO billsDO) {
		try {
			genericObject.persist(billsDO);
		} catch (Exception eException) {
		} finally {
		}
		return billsDO;
	}
	
	
	public List<BillsDO> retrieve() {
		List<BillsDO> billsList = null;
		try {
			billsList = genericObject.retrieve(BillsDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return billsList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<BillsDO> retrieveBillNumber() {
		List<BillsDO> billsList = null;
		try {
			
			return this.sessionFactory.getCurrentSession().getNamedQuery("bills.findBillNumber")
					.setMaxResults(1)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return billsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<BillsDO> retrieveById(Long id) {
		List<BillsDO> proposal = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(BillsDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return proposal;
	}
	
	public BillsDO update(BillsDO billsDO) {
		try {
			genericObject.merge(billsDO);
		} catch (Exception eException) {
		} finally {
		}
		return billsDO;
	}
	
	public boolean persistList(List<BillsDO> billsDO) {
		try {
			for (BillsDO billsDO2 : billsDO) {
				genericObject.persist(billsDO2);
			}
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
		}
		return true;
	}

}
