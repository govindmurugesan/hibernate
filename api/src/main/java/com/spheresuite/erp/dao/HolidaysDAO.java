package com.spheresuite.erp.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.HolidaysDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class HolidaysDAO {
	static Logger logger = Logger.getLogger(HolidaysDAO.class.getName());
	@Autowired
	private GenericDAOImpl<HolidaysDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public HolidaysDO persist(HolidaysDO holidayDetail)  {
		try {
			genericObject.persist(holidayDetail);
		} catch (Exception eException) {
		} finally {
		}
		return holidayDetail;
	}
	
	public List<HolidaysDO> retrieve() {
		List<HolidaysDO> holidayList = null;
		try {
			holidayList = genericObject.retrieve(HolidaysDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return holidayList;
	}
	
	@SuppressWarnings("unchecked")
	public List<HolidaysDO> retrieveById(Long Id)  {
		List<HolidaysDO> holidayList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(HolidaysDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, Id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return holidayList;
	}
	
	@SuppressWarnings("unchecked")
	public List<HolidaysDO> retrieveByHoliday(String year, String date)  {
		List<HolidaysDO> holidayList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(HolidaysDO.FIND_BY_HOLIDAY)
			//.setParameter(CommonConstants.DESC, desc)
			.setParameter(CommonConstants.YEAR, year)
			.setParameter(CommonConstants.DATE, date)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return holidayList;
	}
	
	@SuppressWarnings("unchecked")
	public List<HolidaysDO> retrieveByHolidayForUpdate(Long id, String year, String date)  {
		List<HolidaysDO> holidayList = null;
		List<Long> ids = new ArrayList<Long>();
		ids.add(id);
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(HolidaysDO.FIND_BY_HOLIDAY_UPDATE)
			//.setParameter(CommonConstants.DESC, desc)
			.setParameter(CommonConstants.YEAR, year)
			.setParameter(CommonConstants.DATE, date)
			.setParameter(CommonConstants.ID, ids)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return holidayList;
	}
	
	public HolidaysDO update(HolidaysDO holidayDetail)  {
		try {
			genericObject.merge(holidayDetail);
		} catch (Exception eException) {
		} finally {
		}
		return holidayDetail;
	}
	
	@SuppressWarnings("unchecked")
	public List<HolidaysDO> retrieveByDate(String year, String date)  {
		List<HolidaysDO> holidayList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(HolidaysDO.FIND_BY_DATE)
			.setParameter(CommonConstants.YEAR, year)
			.setParameter(CommonConstants.DATE, date)
			.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return holidayList;
	}
	
	
	/*@SuppressWarnings("unchecked")
	public List<HolidaysDO> retrieveByDateAndWorkLocation(String year, String date, long workLocationId)  {
		List<HolidaysDO> holidayList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(HolidaysDO.FIND_BY_DATE_WORKLOCATION)
			.setParameter(CommonConstants.YEAR, year)
			.setParameter(CommonConstants.DATE, date)
			.setParameter(CommonConstants.ID, workLocationId)
			.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return holidayList;
	}*/

}
