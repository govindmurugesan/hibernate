package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.EmployeeLeavegroupDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class EmployeeLeavegroupDAO {
	static Logger logger = Logger.getLogger(EmployeeLeavegroupDAO.class.getName());
	@Autowired
	private GenericDAOImpl<EmployeeLeavegroupDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public boolean persist(EmployeeLeavegroupDO leavegroupDO) {
		boolean persistStatus = true;
		try {
			genericObject.persist(leavegroupDO);
			/*List<EmployeePFAndESIInfoDO> empList = null;
			empList = this.sessionFactory.getCurrentSession().getNamedQuery(EmployeePFAndESIInfoDO.FIND_BY_ACCOUNTID)
					.setParameter(CommonConstants.PFACCOUNTNO, employeeDO.getPFAccNo())
					.setParameter(CommonConstants.ESIACCOUNTNO, employeeDO.getESIAccNo())
					.list();
					if(empList != null && empList.size() > 0){
						persistStatus = false;
					}else{
						genericObject.persist(employeeDO);
					}*/
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
		}
		return persistStatus;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeLeavegroupDO> retrieveByEmpId(String empId) {
		List<EmployeeLeavegroupDO> employeeLeavegrpList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeLeavegroupDO.FIND_BY_EMPID)
					.setParameter(CommonConstants.EMPID, empId)
					.list(); 
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeLeavegrpList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeLeavegroupDO> retrieveById(Long id) {
		List<EmployeeLeavegroupDO> employeeLeavegrpList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeLeavegroupDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeLeavegrpList;
	}
	
	public EmployeeLeavegroupDO update(EmployeeLeavegroupDO employeeLeavegrp) {
		try {
			genericObject.merge(employeeLeavegrp);
		} catch (Exception eException) {
		} finally {
		}
		return employeeLeavegrp;
	}
	
	public boolean persistList(List<EmployeeLeavegroupDO> employeeLeavegroupList){
		try {
			for (EmployeeLeavegroupDO employeeLeavegroup : employeeLeavegroupList) {
				genericObject.persist(employeeLeavegroup);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}

}
