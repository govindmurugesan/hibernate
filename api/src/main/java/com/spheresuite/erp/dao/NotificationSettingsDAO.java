package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.NotificationSettingsDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class NotificationSettingsDAO {
	static Logger logger = Logger.getLogger(NotificationSettingsDAO.class.getName());

	@Autowired
	private GenericDAOImpl<NotificationSettingsDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public NotificationSettingsDO persist(NotificationSettingsDO notificationSettingsDO) {
		try {
			genericObject.persist(notificationSettingsDO);
		} catch (Exception eException) {
		} finally {
		}
		return notificationSettingsDO;
	}
	
	public NotificationSettingsDO update(NotificationSettingsDO notificationSettingsDO) {
		try {
			genericObject.merge(notificationSettingsDO);
		} catch (Exception eException) {
		} finally {
		}
		return notificationSettingsDO;
	}
	
	public List<NotificationSettingsDO> retrieve() {
		List<NotificationSettingsDO> notificarionSettingList = null;
		try {
			notificarionSettingList = genericObject.retrieve(NotificationSettingsDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return notificarionSettingList;
	}
	
	@SuppressWarnings("unchecked")
	public List<NotificationSettingsDO> retrieveById(Long Id) {
		List<NotificationSettingsDO> notificarionSettingList = null;
		try {
			notificarionSettingList =  this.sessionFactory.getCurrentSession().getNamedQuery(NotificationSettingsDO.FIND_BY_ID)
									.setParameter(CommonConstants.ID, Id).list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return notificarionSettingList;
	}
}
