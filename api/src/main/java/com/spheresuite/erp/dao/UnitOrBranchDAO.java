package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.UnitOrBranchDO;
import com.spheresuite.erp.util.CommonConstants;

@Repository
public class UnitOrBranchDAO {
	static Logger logger = Logger.getLogger(UnitOrBranchDAO.class.getName());
	
	@Autowired
	private GenericDAOImpl<UnitOrBranchDO> dao;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(UnitOrBranchDO dataObject) {
		boolean persistStatus = true;
		try {
			List<UnitOrBranchDO> unitOrBranchDOList = null;
			unitOrBranchDOList = this.sessionFactory.getCurrentSession().getNamedQuery(UnitOrBranchDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, dataObject.getUnitOrBranch())
					.list();
					if(unitOrBranchDOList != null && unitOrBranchDOList.size() > 0){
						persistStatus = false;
					}else{
						dao.persist(dataObject);
					}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	public List<UnitOrBranchDO> retrieve() {
		List<UnitOrBranchDO> unitOrBranchDOList = null;
		try {
			unitOrBranchDOList = dao.retrieve(UnitOrBranchDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return unitOrBranchDOList;
	}
	
	public List<UnitOrBranchDO> retrieveActive() {
		List<UnitOrBranchDO> unitOrBranchDOList = null;
		try {
			unitOrBranchDOList = dao.retrieveActive(CommonConstants.ACTIVE, UnitOrBranchDO.FIND_BY_STATUS, UnitOrBranchDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return unitOrBranchDOList;
	}
	
	@SuppressWarnings("unchecked")
	public List<UnitOrBranchDO> retrieveActiveById(Long Id) {
		List<UnitOrBranchDO> unitOrBranchDOList = null;
		try {
			unitOrBranchDOList =  this.sessionFactory.getCurrentSession().getNamedQuery(UnitOrBranchDO.FIND_BY_ID)
									.setParameter(CommonConstants.ID, Id).list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return unitOrBranchDOList;
	}
	
	@SuppressWarnings("unchecked")
	public List<UnitOrBranchDO> retrieveByCityAll(String city) {
		List<UnitOrBranchDO> unitOrBranchDOList = null;
		try {
			unitOrBranchDOList =  this.sessionFactory.getCurrentSession().getNamedQuery(UnitOrBranchDO.FIND_BY_CITY_ALL)
									.setParameter(CommonConstants.NAME, city).list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return unitOrBranchDOList;
	}
	
	@SuppressWarnings("unchecked")
	public boolean update(UnitOrBranchDO workLocationDO) {
		boolean updateStatus = true;
		try {
			List<UnitOrBranchDO> unitOrBranchDOList = null;
			unitOrBranchDOList = this.sessionFactory.getCurrentSession().getNamedQuery(UnitOrBranchDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, workLocationDO.getUnitOrBranch())
					.setParameter(CommonConstants.ID, workLocationDO.getUnitOrBranchId())
					.list();
			if(unitOrBranchDOList != null && unitOrBranchDOList.size() > 0){
				updateStatus = false;
			}else{
				dao.merge(workLocationDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}
	
	@SuppressWarnings("unchecked")
	public List<UnitOrBranchDO> retrieveActiveByName(String name) {
		List<UnitOrBranchDO> unitOrBranchDOList = null;
		try {
			unitOrBranchDOList =  this.sessionFactory.getCurrentSession().getNamedQuery(UnitOrBranchDO.FIND_BY_NAME)
									.setParameter(CommonConstants.NAME, name).list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return unitOrBranchDOList;
	}
	
	
	public boolean persistList(List<UnitOrBranchDO> unitOrBranchList){
		try {
			for (UnitOrBranchDO unitOrBranch : unitOrBranchList) {
				dao.persist(unitOrBranch);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}
}
