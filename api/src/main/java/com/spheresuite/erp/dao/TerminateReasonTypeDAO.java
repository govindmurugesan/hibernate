package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.TerminateReasonTypeDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class TerminateReasonTypeDAO {
	static Logger logger = Logger.getLogger(TerminateReasonTypeDAO.class.getName());

	@Autowired
	private GenericDAOImpl<TerminateReasonTypeDO> dao;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(TerminateReasonTypeDO terminateReasonTypeDO) {
		boolean persistStatus = true;
		try {
			List<TerminateReasonTypeDO> terminateReasonTypeDOList = null;
			terminateReasonTypeDOList = this.sessionFactory.getCurrentSession().getNamedQuery(TerminateReasonTypeDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, terminateReasonTypeDO.getName())
					.list();
					if(terminateReasonTypeDOList != null && terminateReasonTypeDOList.size() > 0){
						persistStatus = false;
					}else{
						dao.persist(terminateReasonTypeDO);
					}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	public List<TerminateReasonTypeDO> retrieve() {
		List<TerminateReasonTypeDO> terminateReasonTypeList = null;
		try {
			terminateReasonTypeList = dao.retrieve(TerminateReasonTypeDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return terminateReasonTypeList;
	}
	
	public List<TerminateReasonTypeDO> retrieveActive() {
		List<TerminateReasonTypeDO> terminateReasonTypeList = null;
		try {
			terminateReasonTypeList =  dao.retrieveActive(CommonConstants.ACTIVE, TerminateReasonTypeDO.FIND_BY_STATUS, TerminateReasonTypeDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return terminateReasonTypeList;
	}
	
	@SuppressWarnings("unchecked")
	public List<TerminateReasonTypeDO> retrieveById(Long id) {
		List<TerminateReasonTypeDO> terminateReasonTypeList = null;
		try {
			terminateReasonTypeList =  this.sessionFactory.getCurrentSession().getNamedQuery(TerminateReasonTypeDO.FIND_BY_ID)
										.setParameter(CommonConstants.ID, id).list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return terminateReasonTypeList;
	}
	
	@SuppressWarnings("unchecked")
	public boolean update(TerminateReasonTypeDO terminateReasonTypeDO) {
		boolean updateStatus = true;
		try {
			List<TerminateReasonTypeDO> terminateReasonTypeDOList = null;
			terminateReasonTypeDOList = this.sessionFactory.getCurrentSession().getNamedQuery(TerminateReasonTypeDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, terminateReasonTypeDO.getName())
					.setParameter(CommonConstants.ID, terminateReasonTypeDO.getTerminationReasonId())
					.list();
			if(terminateReasonTypeDOList != null && terminateReasonTypeDOList.size() > 0){
				updateStatus = false;
			}else{
				dao.merge(terminateReasonTypeDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}
	
	public boolean persistList(List<TerminateReasonTypeDO> terminateReasonTypList){
		try {
			for (TerminateReasonTypeDO terminateReasonType : terminateReasonTypList) {
				dao.persist(terminateReasonType);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}
}
