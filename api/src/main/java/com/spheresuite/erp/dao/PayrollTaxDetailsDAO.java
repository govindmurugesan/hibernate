package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.PayrollTaxDetailsDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class PayrollTaxDetailsDAO {
	static Logger logger = Logger.getLogger(PayrollTaxDetailsDAO.class.getName());

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private GenericDAOImpl<PayrollTaxDetailsDO> genericObject;
	
	public List<PayrollTaxDetailsDO> persist(List<PayrollTaxDetailsDO> payrollTaxDetailsDO) {
		try {
			for (PayrollTaxDetailsDO payrollTaxDetailsDO2 : payrollTaxDetailsDO) {
				genericObject.persist(payrollTaxDetailsDO2);
			}
		} catch (Exception eException) {
			//throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			//SessionManager.closeEntityManager(em);
		}
		return payrollTaxDetailsDO;
	}
	
	
	public List<PayrollTaxDetailsDO> retrieve() {
		List<PayrollTaxDetailsDO> payrollTaxDetailsList = null;
		try {
			payrollTaxDetailsList = genericObject.retrieve(PayrollTaxDetailsDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return payrollTaxDetailsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<PayrollTaxDetailsDO> retrieveByEmpCompensationId(Long id) {
		List<PayrollTaxDetailsDO> payrollTaxDetailsList = null;
		try {
			payrollTaxDetailsList = this.sessionFactory.getCurrentSession().getNamedQuery(PayrollTaxDetailsDO.FIND_BY_ID)
										.setParameter(CommonConstants.ID, id)
										.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return payrollTaxDetailsList;
	}
	
	public PayrollTaxDetailsDO update(PayrollTaxDetailsDO payrollTaxDetailsDO) {
		try {
			genericObject.merge(payrollTaxDetailsDO);
		} catch (Exception eException) {
		} finally {
		}
		return payrollTaxDetailsDO;
	}

	@SuppressWarnings("unchecked")
	public List<PayrollTaxDetailsDO> retrieveByEmpCompensationIds(List<Long> ids) {
		List<PayrollTaxDetailsDO> employeeEarningsList = null;
		try {
			employeeEarningsList = this.sessionFactory.getCurrentSession().getNamedQuery(PayrollTaxDetailsDO.FIND_BY_IDS)
										.setParameter(CommonConstants.ID, ids)
										.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeEarningsList;
	}
	
	public boolean deletePayrollById(Long payrollId)  {
		try {
			org.hibernate.Query query = this.sessionFactory.getCurrentSession().createQuery("delete from PayrollTaxDetailsDO e where e.payroll.payrollId=:id")
					.setParameter(CommonConstants.ID, payrollId);
			query.executeUpdate();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return true;
	}
}
