package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.WorkLocationDO;
import com.spheresuite.erp.util.CommonConstants;

@Repository
public class WorkLocationDAO {
	static Logger logger = Logger.getLogger(WorkLocationDAO.class.getName());
	
	@Autowired
	private GenericDAOImpl<WorkLocationDO> dao;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(WorkLocationDO dataObject) {
		boolean persistStatus = true;
		try {
			List<WorkLocationDO> workLocationDOList = null;
			workLocationDOList = this.sessionFactory.getCurrentSession().getNamedQuery(WorkLocationDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, dataObject.getWorklocation())
					.list();
					if(workLocationDOList != null && workLocationDOList.size() > 0){
						persistStatus = false;
					}else{
						dao.persist(dataObject);
					}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	/*public List<WorkLocationDO> retrieve() {
		List<WorkLocationDO> workLocationDOList = null;
		try {
			workLocationDOList = dao.retrieve(WorkLocationDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return workLocationDOList;
	}*/
	
	@SuppressWarnings("unchecked")
	public List<WorkLocationDO> retrieve() {
		List<WorkLocationDO> workLocationDOList = null;
		try {
			workLocationDOList =  this.sessionFactory.getCurrentSession().getNamedQuery(WorkLocationDO.FIND_ALL)
									.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return workLocationDOList;
	}
	
	public List<WorkLocationDO> retrieveActive() {
		List<WorkLocationDO> workLocationDOList = null;
		try {
			workLocationDOList = dao.retrieveActive(CommonConstants.ACTIVE, WorkLocationDO.FIND_BY_STATUS, WorkLocationDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return workLocationDOList;
	}
	
	@SuppressWarnings("unchecked")
	public List<WorkLocationDO> retrieveById(Long Id) {
		List<WorkLocationDO> workLocationDOList = null;
		try {
			workLocationDOList =  this.sessionFactory.getCurrentSession().getNamedQuery(WorkLocationDO.FIND_BY_ID)
									.setParameter(CommonConstants.ID, Id).list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return workLocationDOList;
	}
	
	@SuppressWarnings("unchecked")
	public List<WorkLocationDO> retrieveByStateId(Long Id) {
		List<WorkLocationDO> workLocationDOList = null;
		try {
			workLocationDOList =  this.sessionFactory.getCurrentSession().getNamedQuery(WorkLocationDO.FIND_BY_STATE_ID)
									.setParameter(CommonConstants.ID, Id).list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return workLocationDOList;
	}
	
	/*@SuppressWarnings("unchecked")
	public List<WorkLocationDO> retrieveByCityAll(String city) {
		List<WorkLocationDO> workLocationDOList = null;
		try {
			workLocationDOList =  this.sessionFactory.getCurrentSession().getNamedQuery(WorkLocationDO.FIND_BY_CITY_ALL)
									.setParameter(CommonConstants.NAME, city).list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return workLocationDOList;
	}*/
	
	@SuppressWarnings("unchecked")
	public boolean update(WorkLocationDO workLocationDO) {
		boolean updateStatus = true;
		try {
			List<WorkLocationDO> WorkLocationDOList = null;
			WorkLocationDOList = this.sessionFactory.getCurrentSession().getNamedQuery(WorkLocationDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, workLocationDO.getWorklocation())
					.setParameter(CommonConstants.ID, workLocationDO.getWorklocationId())
					.list();
			if(WorkLocationDOList != null && WorkLocationDOList.size() > 0){
				updateStatus = false;
			}else{
				dao.merge(workLocationDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}
	
	@SuppressWarnings("unchecked")
	public List<WorkLocationDO> retrieveByName(String name) {
		List<WorkLocationDO> WorkLocationDOList = null;
		try {
			WorkLocationDOList =  this.sessionFactory.getCurrentSession().getNamedQuery(WorkLocationDO.FIND_BY_NAME)
									.setParameter(CommonConstants.NAME, name).list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return WorkLocationDOList;
	}
	
	public boolean persistList(List<WorkLocationDO> workLocationList){
		try {
			for (WorkLocationDO workLocation : workLocationList) {
				dao.persist(workLocation);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}
}
