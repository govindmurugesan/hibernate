package com.spheresuite.erp.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.PFGroupEarningsDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class PFGroupEarningsDAO {
	@Autowired
	private GenericDAOImpl<PFGroupEarningsDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(PFGroupEarningsDO pfGroupEarningsDO){
		List<PFGroupEarningsDO> pfGroupEarningList = null;
		boolean persistStatus = true;
		try {
			pfGroupEarningList = this.sessionFactory.getCurrentSession().getNamedQuery(PFGroupEarningsDO.FIND_FOR_ADD)
			.setParameter(CommonConstants.ALLOWENCE_ID, pfGroupEarningsDO.getAllowanceSettings().getAllowanceSettingsId())
			.setParameter(CommonConstants.PFGROUPID, pfGroupEarningsDO.getPfGroup().getPfGroupId())
			.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
			.list();
			if(pfGroupEarningList != null && pfGroupEarningList.size() > 0){
				persistStatus = false;
			}else{
				genericObject.persist(pfGroupEarningsDO);
			}
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
		}
		return persistStatus;
	}
	
	@SuppressWarnings("unchecked")
	public List<PFGroupEarningsDO> retrieveByGroupID(Long id) {
		List<PFGroupEarningsDO> pfGroupEarningList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(PFGroupEarningsDO.FIND_BY_GROUP_ID)
					.setParameter(CommonConstants.ID, id)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return pfGroupEarningList;
	}
	
	
	
	@SuppressWarnings("unchecked")
	public List<PFGroupEarningsDO> retrieveByID(Long id) {
		List<PFGroupEarningsDO> pfGroupEarningList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(PFGroupEarningsDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return pfGroupEarningList;
	}
	
	public boolean updateList(List<PFGroupEarningsDO> pfGroupEarningList){
		try {
			for (PFGroupEarningsDO pfGroupEarning : pfGroupEarningList) {
				genericObject.merge(pfGroupEarning);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}

}
