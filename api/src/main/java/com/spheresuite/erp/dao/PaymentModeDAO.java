package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.PaymentModeDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class PaymentModeDAO {
	static Logger logger = Logger.getLogger(PaymentModeDAO.class.getName());

	@Autowired
	private GenericDAOImpl<PaymentModeDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public PaymentModeDO persist(PaymentModeDO paymentmodeDO) {
		try {
			genericObject.persist(paymentmodeDO);
		} catch (Exception eException) {
			//throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			//SessionManager.closeEntityManager(em);
		}
		return paymentmodeDO;
	}
	
	
	public List<PaymentModeDO> retrieve() {
		List<PaymentModeDO> paymentList = null;
		try {
			paymentList = genericObject.retrieve(PaymentModeDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return paymentList;
	}
	
	public List<PaymentModeDO> retrieveActive() {
		List<PaymentModeDO> paymentList = null;
		try {
			paymentList =  genericObject.retrieveActive(CommonConstants.ACTIVE, PaymentModeDO.FIND_BY_STATUS, PaymentModeDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return paymentList;
	}
	
	@SuppressWarnings("unchecked")
	public List<PaymentModeDO> retrieveById(Long Id) {
		List<PaymentModeDO> paymentList = null;
		try {
			paymentList =  this.sessionFactory.getCurrentSession().getNamedQuery(PaymentModeDO.FIND_BY_ID)
							.setParameter(CommonConstants.ID, Id).list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return paymentList;
	}
	
	public PaymentModeDO update(PaymentModeDO paymentDO) {
		try {
			genericObject.merge(paymentDO);
		} catch (Exception eException) {
		} finally {
		}
		return paymentDO;
	}

}
