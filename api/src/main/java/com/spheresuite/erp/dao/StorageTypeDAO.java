package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.StorageTypeDO;
import com.spheresuite.erp.domainobject.UserDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class StorageTypeDAO {
	static Logger logger = Logger.getLogger(StorageTypeDAO.class.getName());

	@Autowired
	private GenericDAOImpl<StorageTypeDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public StorageTypeDO persist(StorageTypeDO storageTypeDO) {
		try {
			genericObject.persist(storageTypeDO);
		} catch (Exception eException) {
			//throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			//SessionManager.closeEntityManager(em);
		}
		return storageTypeDO;
	}
	
	
	public List<StorageTypeDO> retrieve() {
		List<StorageTypeDO> storageTypeList = null;
		try {
			storageTypeList = genericObject.retrieve(StorageTypeDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return storageTypeList;
	}
	
	public List<StorageTypeDO> retrieveActive() {
		List<StorageTypeDO> storageTypeList = null;
		try {
			storageTypeList =  genericObject.retrieveActive(CommonConstants.ACTIVE, UserDO.FIND_BY_STATUS, StorageTypeDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return storageTypeList;
	}
	
	@SuppressWarnings("unchecked")
	public List<StorageTypeDO> retrieveById(Long id) {
		List<StorageTypeDO> storageTypeList = null;
		try {
			storageTypeList =  this.sessionFactory.getCurrentSession().getNamedQuery(StorageTypeDO.FIND_BY_ID)
									.setParameter(CommonConstants.ID, id).list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return storageTypeList;
	}
	
	public StorageTypeDO update(StorageTypeDO storageTypeDO) {
		try {
			genericObject.merge(storageTypeDO);
		} catch (Exception eException) {
			//throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			//SessionManager.closeEntityManager(em);
		}
		return storageTypeDO;
	}

}
