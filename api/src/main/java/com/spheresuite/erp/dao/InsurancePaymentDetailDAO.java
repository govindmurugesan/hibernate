package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.InsurancePaymentDetailDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class InsurancePaymentDetailDAO {
	static Logger logger = Logger.getLogger(InsurancePaymentDetailDAO.class.getName());
	@Autowired
	private GenericDAOImpl<InsurancePaymentDetailDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public InsurancePaymentDetailDO persist(InsurancePaymentDetailDO insurancePaymentDetailDO) {
		try {
			genericObject.persist(insurancePaymentDetailDO);
		} catch (Exception eException) {
		} finally {
		}
		return insurancePaymentDetailDO;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<InsurancePaymentDetailDO> getByAdvanceId(Long id) {
		List<InsurancePaymentDetailDO> insurancePaymentDetailList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(InsurancePaymentDetailDO.FIND_BY_ADVANCE_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return insurancePaymentDetailList;
	}
	
	@SuppressWarnings("unchecked")
	public List<InsurancePaymentDetailDO> getById(Long id) {
		List<InsurancePaymentDetailDO> insurancePaymentDetailList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(InsurancePaymentDetailDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return insurancePaymentDetailList;
	}
	
	
	
	@SuppressWarnings("unchecked")
	public List<InsurancePaymentDetailDO> getByMonthAndEmpId(String empId, String month) {
		List<InsurancePaymentDetailDO> insurancePaymentDetailList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(InsurancePaymentDetailDO.FIND_BY_EMPID_MONTH)
					.setParameter(CommonConstants.EMPID, empId)
					.setParameter(CommonConstants.MONTH, month)
					.setParameter(CommonConstants.STATUS,CommonConstants.ACTIVESTRING)
					.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return insurancePaymentDetailList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<InsurancePaymentDetailDO> getByStatusAndEmpId(String empId, String month) {
		List<InsurancePaymentDetailDO> insurancePaymentDetailList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(InsurancePaymentDetailDO.FIND_BY_EMPID_MONTH)
					.setParameter(CommonConstants.EMPID, empId)
					.setParameter(CommonConstants.MONTH, month)
					.setParameter(CommonConstants.STATUS,CommonConstants.SSTATUS)
					.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return insurancePaymentDetailList;
	}
	
	public InsurancePaymentDetailDO update(InsurancePaymentDetailDO InsurancePaymentDetailDO) {
		try {
			genericObject.merge(InsurancePaymentDetailDO);
		} catch (Exception eException) {
		} finally {
		}
		return InsurancePaymentDetailDO;
	}
	
	
	/*@SuppressWarnings("unchecked")
	public List<InsurancePaymentDetailDO> retrieveActiveByEmpId(String empId) {
		List<InsurancePaymentDetailDO> employeeAdvanceList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(InsurancePaymentDetailDO.FIND_ACTIVE_BY_EMPID)
					.setParameter(CommonConstants.EMPID, empId)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVESTRING)
					.list(); 
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeAdvanceList;
	}*/
	
	/*@SuppressWarnings("unchecked")
	public List<InsurancePaymentDetailDO> retrieveByEmpIdWithDate(List<Long> empIds, String bonusMonth) {
		List<InsurancePaymentDetailDO> employeeBonusList = null;
		try {
			if(empIds != null && empIds.size() > 0){
				return this.sessionFactory.getCurrentSession().getNamedQuery(InsurancePaymentDetailDO.FIND_BY_EMPID_MONTH)
					.setParameter(CommonConstants.EMPID, empIds)
					.setParameter(CommonConstants.STATUS, 'a')
					.setParameter(CommonConstants.MONTHLY, bonusMonth)
					.list(); 
			}else{
				return this.sessionFactory.getCurrentSession().getNamedQuery(InsurancePaymentDetailDO.FIND_BY_MONTH)
					.setParameter(CommonConstants.STATUS, 'a')
					.setParameter(CommonConstants.MONTHLY, bonusMonth)
					.list();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeBonusList;
	}*/
	
	/*@SuppressWarnings("unchecked")
	public List<InsurancePaymentDetailDO> retrieveByEmpIdBetweenDate(List<Long> empIds, String fromMonth, String toMonth) {
		List<InsurancePaymentDetailDO> employeeBonusList = null;
		try {
				if(empIds != null && empIds.size() > 0){
					return this.sessionFactory.getCurrentSession().getNamedQuery(InsurancePaymentDetailDO.FIND_BY_EMPID_BETWEENMONTH)
						.setParameter(CommonConstants.EMPID, empIds)
						.setParameter(CommonConstants.STATUS, 'a')
						.setParameter("fromMonth", fromMonth)
						.setParameter("toMonth", toMonth)
						.list();
				}else{
					return this.sessionFactory.getCurrentSession().getNamedQuery(InsurancePaymentDetailDO.FIND_BETWEENMONTH)
					.setParameter(CommonConstants.STATUS, 'a')
					.setParameter("fromMonth", fromMonth)
					.setParameter("toMonth", toMonth)
					.list();
				}
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeBonusList;
	}
	
	@SuppressWarnings("unchecked")
	public List<InsurancePaymentDetailDO> retrieveByEmpIdAndMonth(String empId, String month) {
		List<InsurancePaymentDetailDO> employeeBonusList = null;
		try {
				return this.sessionFactory.getCurrentSession().getNamedQuery(InsurancePaymentDetailDO.FIND_BY_EMPID_DATE)
					.setParameter(CommonConstants.EMPID, empId)
					.setParameter(CommonConstants.MONTHLY, month)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
				
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeBonusList;
	}
	
	@SuppressWarnings("rawtypes")
	public Long retrieveByEmpIdBetweenMonth(String empId, String fromMonth, String toMonth) {
		Long employeeBonusList = null;
		try {
				List longList =  this.sessionFactory.getCurrentSession().getNamedQuery(InsurancePaymentDetailDO.FIND_BY_EMPID_BETWEEN_DATE)
						.setParameter(CommonConstants.EMPID, empId)
						.setParameter("fromMonth", fromMonth)
						.setParameter("toMonth", toMonth)
						.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
						.list();
				
				if(longList !=null && longList.size() > 0){
					employeeBonusList = (Long) longList.get(0);
				}
				
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeBonusList;
	}
	
	@SuppressWarnings("unchecked")
	public List<InsurancePaymentDetailDO> retrieveByEmpIdAndMonthWithOutStatus(Long empId, String month) {
		List<InsurancePaymentDetailDO> employeeBonusList = null;
		try {
			
			return this.sessionFactory.getCurrentSession().getNamedQuery(InsurancePaymentDetailDO.FIND_BY_EMPID_DATE)
					.setParameter(CommonConstants.EMPID, empId)
					.setParameter(CommonConstants.MONTHLY, month)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeBonusList;
	}*/
	
	/*@SuppressWarnings("unchecked")
	public List<InsurancePaymentDetailDO> retrieve() {
		List<InsurancePaymentDetailDO> advancePaymentList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(InsurancePaymentDetailDO.FIND_ALL)
					.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
		}
		return advancePaymentList;
	}
	
	
	
	*/
	
	/*public boolean delete(Long id)  {
		try {
			org.hibernate.Query query = this.sessionFactory.getCurrentSession().createQuery("delete from InsurancePaymentDetailDO e where e.empBonusId=:id")
					.setParameter(CommonConstants.ID, id);
			query.executeUpdate();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return true;
	}*/
	

}
