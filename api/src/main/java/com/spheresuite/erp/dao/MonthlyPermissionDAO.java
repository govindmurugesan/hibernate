package com.spheresuite.erp.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.MonthlyPermissionDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class MonthlyPermissionDAO {
	@Autowired
	private GenericDAOImpl<MonthlyPermissionDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(MonthlyPermissionDO monthlyPermissionDO){
		List<MonthlyPermissionDO> earningTypeList = null;
		boolean persistStatus = true;
		try {
			earningTypeList = this.sessionFactory.getCurrentSession().getNamedQuery(MonthlyPermissionDO.FIND_BY_STATUS)
			.setParameter(CommonConstants.STATUS,CommonConstants.ACTIVE)
			.list();
			if(earningTypeList != null && earningTypeList.size() > 0){
				MonthlyPermissionDO monthlyPermissionUpdate = new MonthlyPermissionDO();
				monthlyPermissionUpdate = earningTypeList.get(0);
				monthlyPermissionUpdate.setStatus(CommonConstants.INACTIVE);
				genericObject.merge(monthlyPermissionUpdate);
				genericObject.persist(monthlyPermissionDO);
			}else{
				genericObject.persist(monthlyPermissionDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	public List<MonthlyPermissionDO> retrieveActive(){
		List<MonthlyPermissionDO> earningTypeList = null;
		try {
			return genericObject.retrieveActive(CommonConstants.ACTIVE, MonthlyPermissionDO.FIND_BY_STATUS, MonthlyPermissionDO.class);
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return earningTypeList;
	}
	
	@SuppressWarnings("unchecked")
	public List<MonthlyPermissionDO> retrieveById(Long id) {
		List<MonthlyPermissionDO> earningTypeList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(MonthlyPermissionDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return earningTypeList;
	}
	
	public boolean update(MonthlyPermissionDO allowanceTypeDO){
		boolean updateStatus = true;
		try {
				genericObject.merge(allowanceTypeDO);
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}
}
