package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.AttendanceAllowanceDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class AttendanceAllowanceDAO {
	static Logger logger = Logger.getLogger(AttendanceAllowanceDAO.class.getName());
	
	@Autowired
	private GenericDAOImpl<AttendanceAllowanceDO> dao;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(AttendanceAllowanceDO advanceTypeDO){
		boolean persistStatus = true;
		try {
			List<AttendanceAllowanceDO> advanceTypeDOList = null;
			advanceTypeDOList = this.sessionFactory.getCurrentSession().getNamedQuery(AttendanceAllowanceDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, advanceTypeDO.getName())
					.list();
					if(advanceTypeDOList != null && advanceTypeDOList.size() > 0){
						persistStatus =  false;
					}else{
						dao.persist(advanceTypeDO);
					}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	public List<AttendanceAllowanceDO> retrieve(){
		List<AttendanceAllowanceDO> advanceTypeDOList = null;
		try {
			advanceTypeDOList = dao.retrieve(AttendanceAllowanceDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return advanceTypeDOList;
	}
	
	/*public List<AttendanceAllowanceDO> retrieveActive(){
		List<AttendanceAllowanceDO> addressTypeList = null;
		try {
			addressTypeList =  dao.retrieveActive(CommonConstants.ACTIVE, AttendanceAllowanceDO.FIND_BY_STATUS, AttendanceAllowanceDO.class);
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return addressTypeList;
	}*/
	
	@SuppressWarnings("unchecked")
	public List<AttendanceAllowanceDO> retrieveById(Long id){
		 List<AttendanceAllowanceDO> advanceTypeList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(AttendanceAllowanceDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return advanceTypeList;
	}
	
	@SuppressWarnings("unchecked")
	public boolean update(AttendanceAllowanceDO advanceTypeDO) {
		boolean updateStatus = true;
		try {
			List<AttendanceAllowanceDO> AttendanceAllowanceDOList = null;
			AttendanceAllowanceDOList = this.sessionFactory.getCurrentSession().getNamedQuery(AttendanceAllowanceDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, advanceTypeDO.getName())
					.setParameter(CommonConstants.ID, advanceTypeDO.getAttendanceAllowanceId())
					.list();
			if(AttendanceAllowanceDOList != null && AttendanceAllowanceDOList.size() > 0){
				updateStatus = false;
			}else{
				dao.merge(advanceTypeDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}
	
	/*public boolean persistList(List<AttendanceAllowanceDO> addressTypeList){
		try {
			for (AttendanceAllowanceDO addressType : addressTypeList) {
				dao.persist(addressType);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}*/

}
