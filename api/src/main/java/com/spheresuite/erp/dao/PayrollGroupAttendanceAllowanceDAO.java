package com.spheresuite.erp.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.PayrollGroupAttendanceAllowanceDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class PayrollGroupAttendanceAllowanceDAO {
	@Autowired
	private GenericDAOImpl<PayrollGroupAttendanceAllowanceDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(PayrollGroupAttendanceAllowanceDO PayrollGroupAttendanceAllowanceDO){
		List<PayrollGroupAttendanceAllowanceDO> payrollGroupAttendanceAllowanceList = null;
		boolean persistStatus = true;
		try {
			payrollGroupAttendanceAllowanceList = this.sessionFactory.getCurrentSession().getNamedQuery(PayrollGroupAttendanceAllowanceDO.FIND_FOR_ADD)
			.setParameter(CommonConstants.ALLOWENCE_ID, PayrollGroupAttendanceAllowanceDO.getAttendanceAllowance().getAttendanceAllowanceId())
			.setParameter(CommonConstants.PAYROLLGROUPID, PayrollGroupAttendanceAllowanceDO.getPayrollGroup().getPayrollGroupId())
			.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
			.list();
			if(payrollGroupAttendanceAllowanceList != null && payrollGroupAttendanceAllowanceList.size() > 0){
				persistStatus = false;
			}else{
				genericObject.persist(PayrollGroupAttendanceAllowanceDO);
			}
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
		}
		return persistStatus;
	}
	
	@SuppressWarnings("unchecked")
	public List<PayrollGroupAttendanceAllowanceDO> retrieveByGroupID(Long id) {
		List<PayrollGroupAttendanceAllowanceDO> payrollGroupAttendanceAllowanceList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(PayrollGroupAttendanceAllowanceDO.FIND_BY_GROUP_ID)
					.setParameter(CommonConstants.ID, id)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return payrollGroupAttendanceAllowanceList;
	}
	
	
	
	@SuppressWarnings("unchecked")
	public List<PayrollGroupAttendanceAllowanceDO> retrieveByID(Long id) {
		List<PayrollGroupAttendanceAllowanceDO> payrollGroupWorkingdaysAllowanceList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(PayrollGroupAttendanceAllowanceDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return payrollGroupWorkingdaysAllowanceList;
	}
	
	public boolean updateList(List<PayrollGroupAttendanceAllowanceDO> payrollGroupWorkingdaysAllowanceList){
		try {
			for (PayrollGroupAttendanceAllowanceDO payrollGroupWorkingdays : payrollGroupWorkingdaysAllowanceList) {
				genericObject.merge(payrollGroupWorkingdays);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}
	

}
