package com.spheresuite.erp.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.util.CommonConstants;

@Repository
public class GenericDAOImpl<T> implements GenericDAO<T> {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}

	@Override
	public Object persist(T p) {
		try{
			return this.sessionFactory.getCurrentSession().save(p);
		}catch(Exception eException){
			
		}finally{
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return null;
		
	}

	@Override
	public void remove(T obj) {
		try{
			this.sessionFactory.getCurrentSession().delete(obj);
		}catch(Exception eException){
			
		}finally{
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		
		
	}

	/*@SuppressWarnings("unchecked")
	@Override
	public T findById(Long id, Class<?> obj) {
		return (T) this.sessionFactory.openSession().get(obj, id);
	}*/

	@SuppressWarnings("unchecked")
	@Override
	public T merge(T obj) {
		try{
			return (T) this.sessionFactory.getCurrentSession().merge(obj);
		}catch(Exception eException){
			
		}finally{
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return obj;
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<T> retrieve(final Class<T> type) { 
		try{
			return this.sessionFactory.getCurrentSession().createCriteria(type).list(); 
		}catch(Exception eException){
			eException.printStackTrace();
		}finally{
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return null;
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<T> retrieveActive(char status, String namedQuery, final Class<T> type) { 
		try{
			return this.sessionFactory.getCurrentSession().getNamedQuery(namedQuery)
					.setParameter(CommonConstants.STATUS, status)
					.list();
		}catch(Exception eException){
			
		}finally{
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return null;
		 
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<T> retrieveActiveByName(char status, String name, String namedQuery, final Class<T> type) { 
		try{
			return this.sessionFactory.getCurrentSession().getNamedQuery(namedQuery)
					.setParameter(CommonConstants.STATUS, status)
					.setParameter(CommonConstants.NAME, name)
					.list(); 
		}catch(Exception eException){
			
		}finally{
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return null;
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<T> retrieveByCountryId(Long countryId, String namedQuery, final Class<T> type) { 
		try{
			return this.sessionFactory.getCurrentSession().getNamedQuery(namedQuery)
					.setParameter(CommonConstants.COUNTRY_ID, countryId)
					.list(); 
		}catch(Exception eException){
			
		}finally{
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return null;
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<T> retrieveAllByDept(List<Long> ids, String namedQuery, final Class<T> type) { 
		try{
			return this.sessionFactory.getCurrentSession().getNamedQuery(namedQuery)
					.setParameter(CommonConstants.ID, ids)
					.list(); 
		}catch(Exception eException){
			
		}finally{
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return null;
		
	}

}
