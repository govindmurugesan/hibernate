package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.EmployeeOtherInformationDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class EmployeeOtherInformationDAO {
	static Logger logger = Logger.getLogger(EmployeeOtherInformationDAO.class.getName());
	@Autowired
	private GenericDAOImpl<EmployeeOtherInformationDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public EmployeeOtherInformationDO persist(EmployeeOtherInformationDO employeeOtherInformationDO){
		try {
			genericObject.persist(employeeOtherInformationDO);
		} catch (Exception eException) {
		} finally {
		}
		return employeeOtherInformationDO;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeOtherInformationDO> retrieveByEmpId(String empId){
		List<EmployeeOtherInformationDO> employeeOtherInformationList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeOtherInformationDO.FIND_BY_EMPID)
					.setParameter(CommonConstants.EMPID, empId)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeOtherInformationList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeOtherInformationDO> retrieveById(Long id){
		List<EmployeeOtherInformationDO> employeeOtherInformationList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeOtherInformationDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeOtherInformationList;
	}
	
	public EmployeeOtherInformationDO update(EmployeeOtherInformationDO employeeOtherInformationDO){
		try {
			genericObject.merge(employeeOtherInformationDO);
		} catch (Exception eException) {
		} finally {
		}
		return employeeOtherInformationDO;
	}

}
