package com.spheresuite.erp.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.PayrollGroupTaxDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class PayrollGroupTaxDAO {
	@Autowired
	private GenericDAOImpl<PayrollGroupTaxDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(PayrollGroupTaxDO payrollGroupTaxDO){
		List<PayrollGroupTaxDO> payrollGroupTaxList = null;
		boolean persistStatus = true;
		try {
			payrollGroupTaxList = this.sessionFactory.getCurrentSession().getNamedQuery(PayrollGroupTaxDO.FIND_FOR_ADD)
			.setParameter(CommonConstants.TAXID, payrollGroupTaxDO.getTaxSettings().getTaxSettingId())
			.setParameter(CommonConstants.PAYROLLGROUPID, payrollGroupTaxDO.getPayrollGroup().getPayrollGroupId())
			.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
			.list();
			if(payrollGroupTaxList != null && payrollGroupTaxList.size() > 0){
				persistStatus = false;
			}else{
				genericObject.persist(payrollGroupTaxDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	public boolean persistWithoutDuplicateCheck(PayrollGroupTaxDO payrollGroupTaxDO){
		boolean persistStatus = true;
		try {
				genericObject.persist(payrollGroupTaxDO);
		} catch (Exception eException) {
			persistStatus = false;
		} finally {
		}
		return persistStatus;
	}
	
	@SuppressWarnings("unchecked")
	public List<PayrollGroupTaxDO> retrieveByGroupID(Long id) {
		List<PayrollGroupTaxDO> payrollGroupTaxList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(PayrollGroupTaxDO.FIND_BY_GROUP_ID)
					.setParameter(CommonConstants.ID, id)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return payrollGroupTaxList;
	}
	
	@SuppressWarnings("unchecked")
	public List<PayrollGroupTaxDO> retrieveByID(Long id) {
		List<PayrollGroupTaxDO> payrollGroupTaxList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(PayrollGroupTaxDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return payrollGroupTaxList;
	}
	
	@SuppressWarnings("unchecked")
	public List<PayrollGroupTaxDO> retrieveByTDS(Long grpId) {
		List<PayrollGroupTaxDO> payrollGroupTaxList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(PayrollGroupTaxDO.FIND_BY_TDS)
					.setParameter(CommonConstants.APPLY, CommonConstants.APPLY)
					.setParameter(CommonConstants.ID, grpId)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return payrollGroupTaxList;
	}
	
	@SuppressWarnings("unchecked")
	public List<PayrollGroupTaxDO> retrieveByPt(Long grpId) {
		List<PayrollGroupTaxDO> payrollGroupTaxList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(PayrollGroupTaxDO.FIND_BY_PT)
					.setParameter(CommonConstants.APPLY, CommonConstants.APPLY)
					.setParameter(CommonConstants.ID, grpId)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return payrollGroupTaxList;
	}
	
	@SuppressWarnings("unchecked")
	public List<PayrollGroupTaxDO> retrieveByEducationCess(Long grpId) {
		List<PayrollGroupTaxDO> payrollGroupTaxList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(PayrollGroupTaxDO.FIND_BY_EDUCATIONCESS)
					.setParameter(CommonConstants.APPLY, CommonConstants.APPLY)
					.setParameter(CommonConstants.ID, grpId)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return payrollGroupTaxList;
	}
	
	
	
	public boolean updateList(List<PayrollGroupTaxDO> payrollGroupTaxList){
		try {
			for (PayrollGroupTaxDO payrollGrouptax : payrollGroupTaxList) {
				genericObject.merge(payrollGrouptax);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}
	
	public boolean update(PayrollGroupTaxDO payrollGroupDO){
		boolean updateStatus = true;
		try {
				genericObject.merge(payrollGroupDO);
			
		} catch (Exception eException) {
			updateStatus= false;
		} finally {
		}
		return updateStatus;
	}
	
	/*public List<PayrollGroupDO> retrieve(){
		List<PayrollGroupDO> payrollGroupList = null;
		try {
			payrollGroupList = genericObject.retrieve(PayrollGroupDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return payrollGroupList;
	}
	
	public List<PayrollGroupDO> retrieveActive(){
		List<PayrollGroupDO> payrollGroupList = null;
		try {
			return genericObject.retrieveActive(CommonConstants.ACTIVE, PayrollGroupDO.FIND_BY_STATUS, PayrollGroupDO.class);
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return payrollGroupList;
	}
	
	@SuppressWarnings("unchecked")
	public List<PayrollGroupDO> retrieveById(Long id) {
		List<PayrollGroupDO> payrollGroupList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(PayrollGroupDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return payrollGroupList;
	}
	*/
	/*@SuppressWarnings("unchecked")
	public boolean update(PayrollGroupDO payrollGroupDO){
		boolean updateStatus = true;
		try {
			List<PayrollGroupDO> payrollGroupList = null;
			payrollGroupList = this.sessionFactory.getCurrentSession().getNamedQuery(PayrollGroupDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, payrollGroupDO.getName())
					.setParameter(CommonConstants.ID, payrollGroupDO.getPayrollGroupId())
					.list();
			if(payrollGroupList != null && payrollGroupList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(payrollGroupDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}*/

}
