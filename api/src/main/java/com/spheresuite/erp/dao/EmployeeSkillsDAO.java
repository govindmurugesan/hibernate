package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.EmployeeSkillsDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class EmployeeSkillsDAO {
	static Logger logger = Logger.getLogger(EmployeeSkillsDAO.class.getName());
	@Autowired
	private GenericDAOImpl<EmployeeSkillsDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public EmployeeSkillsDO persist(EmployeeSkillsDO employeeSkillsDO)  {
		try {
			genericObject.persist(employeeSkillsDO);
		} catch (Exception eException) {
		} finally {
		}
		return employeeSkillsDO;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeSkillsDO> retrieveByEmpId(String empId)  {
		List<EmployeeSkillsDO> employeeSkillsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeSkillsDO.FIND_BY_EMPID)
			.setParameter(CommonConstants.EMPID, empId)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeSkillsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeSkillsDO> retrieveById(Long id)  {
		List<EmployeeSkillsDO> employeeSkillsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeSkillsDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeSkillsList;
	}
	
	public EmployeeSkillsDO update(EmployeeSkillsDO employeeSkillsDO)  {
		try {
			genericObject.merge(employeeSkillsDO);
		} catch (Exception eException) {
		} finally {
		}
		return employeeSkillsDO;
	}

}
