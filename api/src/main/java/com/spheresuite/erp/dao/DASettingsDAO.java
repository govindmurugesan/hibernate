package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.DASettingsDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class DASettingsDAO {
	static Logger logger = Logger.getLogger(DASettingsDAO.class.getName());
	@Autowired
	private GenericDAOImpl<DASettingsDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	
	@SuppressWarnings("unchecked")
	public boolean persist(DASettingsDO DAdo){
		List<DASettingsDO> DASettingsList = null;
		boolean persistStatus = true;
		try {
			DASettingsList = this.sessionFactory.getCurrentSession().getNamedQuery(DASettingsDO.FIND_BY_MONTH_YEAR)
					.setParameter(CommonConstants.MONTHLY, DAdo.getMonth())
					.setParameter(CommonConstants.YEAR, DAdo.getYear())
			.list();
			if(DASettingsList != null && DASettingsList.size() > 0){
				persistStatus = false;
			}else{
				genericObject.persist(DAdo);
			}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	
	
	public List<DASettingsDO> retrieve()  {
		List<DASettingsDO> DASettingsList = null;
		try {
			DASettingsList = genericObject.retrieve(DASettingsDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return DASettingsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<DASettingsDO> retrieveByMonthAndYear(String month,Long year)  {
		List<DASettingsDO> DASettingList = null;
		try {
			DASettingList = this.sessionFactory.getCurrentSession().getNamedQuery(DASettingsDO.FIND_BY_MONTH_YEAR)
					.setParameter(CommonConstants.MONTHLY, month)
					.setParameter(CommonConstants.YEAR, year)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return DASettingList;
	}
	
	
	
	@SuppressWarnings("unchecked")
	public List<DASettingsDO> retrieveByMonthAndYearUnit(String month,Long year, Long unitId)  {
		List<DASettingsDO> DASettingList = null;
		try {
			DASettingList = this.sessionFactory.getCurrentSession().getNamedQuery(DASettingsDO.FIND_BY_MONTH_YEAR_UNIT)
					.setParameter(CommonConstants.MONTHLY, month)
					.setParameter(CommonConstants.YEAR, year)
					.setParameter(CommonConstants.UNIT, unitId)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return DASettingList;
	}
	
	@SuppressWarnings("unchecked")
	public List<DASettingsDO> retrieveById(Long id)  {
		List<DASettingsDO> DASettingList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(DASettingsDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return DASettingList;
	}
	
	@SuppressWarnings("unchecked")
	public boolean update(DASettingsDO DAdo)  {
		boolean updateStatus = true;
		try {
			List<DASettingsDO> DASettingsList = null;
			DASettingsList = this.sessionFactory.getCurrentSession().getNamedQuery(DASettingsDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.MONTHLY, DAdo.getMonth())
					.setParameter(CommonConstants.YEAR, DAdo.getYear())
					.setParameter(CommonConstants.ID, DAdo.getDearnessallowanceId())
					.list();
			if(DASettingsList != null && DASettingsList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(DAdo);
			}
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}
}
