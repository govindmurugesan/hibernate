package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.WareHouseDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class WareHouseDAO {
	static Logger logger = Logger.getLogger(WareHouseDAO.class.getName());

	@Autowired
	private GenericDAOImpl<WareHouseDO> dao;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public WareHouseDO persist(WareHouseDO wareHouseDO) {
		try {
			dao.persist(wareHouseDO);
		} catch (Exception eException) {
			//throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			//SessionManager.closeEntityManager(em);
		}
		return wareHouseDO;
	}
	
	public WareHouseDO update(WareHouseDO wareHouseDO) {
		try {
			dao.merge(wareHouseDO);
		} catch (Exception eException) {
		} finally {
		}
		return wareHouseDO;
	}
	
	public List<WareHouseDO> retrieve() {
		List<WareHouseDO> wareHouseList = null;
		try {
			wareHouseList = dao.retrieve(WareHouseDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return wareHouseList;
	}
	
	@SuppressWarnings("unchecked")
	public List<WareHouseDO> retrieveById(Long Id) {
		List<WareHouseDO> wareHouseList = null;
		try {
			wareHouseList =  this.sessionFactory.getCurrentSession().getNamedQuery(WareHouseDO.FIND_BY_ID)
								.setParameter(CommonConstants.ID, Id).list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return wareHouseList;
	}
}
