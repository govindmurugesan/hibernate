package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.ContractEmployeeDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class ContractEmployeeDAO {
	static Logger logger = Logger.getLogger(ContractEmployeeDAO.class.getName());
	
	@Autowired
	private GenericDAOImpl<ContractEmployeeDO> dao;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(ContractEmployeeDO contractLaboursDO){
		boolean persistStatus = true;
		try {
			dao.persist(contractLaboursDO);
			/*List<ContractLaboursDO> ContractLaboursDOList = null;
			ContractLaboursDOList = this.sessionFactory.getCurrentSession().getNamedQuery(ContractLaboursDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, ContractLaboursDO.getName())
					.list();
					if(ContractLaboursDOList != null && ContractLaboursDOList.size() > 0){
						persistStatus =  false;
					}else{
						dao.persist(ContractLaboursDO);
					}*/
		} catch (Exception eException) {
		eException.printStackTrace();
		} finally {
		}
		return persistStatus;
	}
	
	public List<ContractEmployeeDO> retrieve(){
		List<ContractEmployeeDO> contractLaboursList = null;
		try {
			contractLaboursList = dao.retrieve(ContractEmployeeDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return contractLaboursList;
	}
	
	/*public List<ContractLaboursDO> retrieveActive(){
		List<ContractLaboursDO> addressTypeList = null;
		try {
			addressTypeList =  dao.retrieveActive(CommonConstants.ACTIVE, ContractLaboursDO.FIND_BY_STATUS, ContractLaboursDO.class);
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return addressTypeList;
	}*/
	
	@SuppressWarnings("unchecked")
	public List<ContractEmployeeDO> retrieveById(Long id){
		 List<ContractEmployeeDO> contractLaboursList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(ContractEmployeeDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return contractLaboursList;
	}
	
	@SuppressWarnings("unchecked")
	public boolean update(ContractEmployeeDO contractLaboursDO) {
		boolean updateStatus = true;
		try {
			dao.merge(contractLaboursDO);
			/*List<ContractLaboursDO> contractLaboursList = null;
			contractLaboursList = this.sessionFactory.getCurrentSession().getNamedQuery(ContractLaboursDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, ContractLaboursDO.getName())
					.setParameter(CommonConstants.ID, ContractLaboursDO.getAddressTypeId())
					.list();
			if(contractLaboursList != null && contractLaboursList.size() > 0){
				updateStatus = false;
			}else{
				dao.merge(ContractLaboursDO);
			}*/
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}
	
/*	public boolean persistList(List<ContractLaboursDO> addressTypeList){
		try {
			for (ContractLaboursDO addressType : addressTypeList) {
				dao.persist(addressType);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}
*/
}
