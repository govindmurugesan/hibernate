package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.EmployeeTravelDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class EmployeeTravelDAO {
	static Logger logger = Logger.getLogger(EmployeeTravelDAO.class.getName());
	
	@Autowired
	private GenericDAOImpl<EmployeeTravelDO> dao;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public boolean persist(EmployeeTravelDO employeeTravelDO){
		boolean persistStatus = true;
		try {
			dao.persist(employeeTravelDO);
			/*List<employeeTravelDO> employeeTravelDOList = null;
			employeeTravelDOList = this.sessionFactory.getCurrentSession().getNamedQuery(employeeTravelDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, employeeTravelDO.getName())
					.list();
					if(employeeTravelDOList != null && employeeTravelDOList.size() > 0){
						persistStatus =  false;
					}else{
						dao.persist(employeeTravelDO);
					}*/
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeTravelDO> retrieve(){
		List<EmployeeTravelDO> employeeTravelList = null;
		try {
			employeeTravelList = this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeTravelDO.FIND_ALL).list();
			//addressTypeList = dao.retrieve(employeeTravelDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return employeeTravelList;
	}
	
	/*public List<employeeTravelDO> retrieveActive(){
		List<employeeTravelDO> addressTypeList = null;
		try {
			addressTypeList =  dao.retrieveActive(CommonConstants.ACTIVE, employeeTravelDO.FIND_BY_STATUS, employeeTravelDO.class);
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return addressTypeList;
	}
	*/
	@SuppressWarnings("unchecked")
	public List<EmployeeTravelDO> retrieveById(Long id){
		 List<EmployeeTravelDO> empTravelList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeTravelDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return empTravelList;
	}
	
	
	
	
	@SuppressWarnings("unchecked")
	public List<EmployeeTravelDO> retrieveByReportingId(String id){
		 List<EmployeeTravelDO> empTravelList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeTravelDO.FIND_BY_REPORTINGID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return empTravelList;
	}
	
	
	
	@SuppressWarnings("unchecked")
	public List<EmployeeTravelDO> retrieveByEmpId(String id){
		 List<EmployeeTravelDO> empTravelList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeTravelDO.FIND_BY_EMPID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return empTravelList;
	}
	
	/*@SuppressWarnings("unchecked")
	public boolean update(EmployeeTravelDO employeeTravelDO) {
		boolean updateStatus = true;
		try {
			List<EmployeeTravelDO> employeeTravelDOList = null;
			employeeTravelDOList = this.sessionFactory.getCurrentSession().getNamedQuery(employeeTravelDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, employeeTravelDO.getName())
					.setParameter(CommonConstants.ID, employeeTravelDO.getAddressTypeId())
					.list();
			if(employeeTravelDOList != null && employeeTravelDOList.size() > 0){
				updateStatus = false;
			}else{
				dao.merge(employeeTravelDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}*/
	
	
	public Boolean update(EmployeeTravelDO employeeTravelDO) {
		Boolean updateStatus = false;
		try {
			dao.merge(employeeTravelDO);
			updateStatus = true;
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}
	
	/*public boolean persistList(List<employeeTravelDO> addressTypeList){
		try {
			for (employeeTravelDO addressType : addressTypeList) {
				dao.persist(addressType);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}*/

}
