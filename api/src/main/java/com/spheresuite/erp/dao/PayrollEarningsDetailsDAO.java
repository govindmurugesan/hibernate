package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.PayrollEarningsDetailsDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class PayrollEarningsDetailsDAO {
	static Logger logger = Logger.getLogger(PayrollEarningsDetailsDAO.class.getName());
	
	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private GenericDAOImpl<PayrollEarningsDetailsDO> genericObject;	
	
	public List<PayrollEarningsDetailsDO> persist(List<PayrollEarningsDetailsDO> payrollEarningsDetailsDO) {
		try {
			for (PayrollEarningsDetailsDO payrollEarningsDetailsDO2 : payrollEarningsDetailsDO) {
				genericObject.persist(payrollEarningsDetailsDO2);
			}
		} catch (Exception eException) {
			//throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			//SessionManager.closeEntityManager(em);
		}
		return payrollEarningsDetailsDO;
	}
	
	
	public List<PayrollEarningsDetailsDO> retrieve() {
		List<PayrollEarningsDetailsDO> payrollEarningsDetailsList = null;
		try {
			payrollEarningsDetailsList = genericObject.retrieve(PayrollEarningsDetailsDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return payrollEarningsDetailsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<PayrollEarningsDetailsDO> retrieveByEmpCompensationId(Long id) {
		List<PayrollEarningsDetailsDO> payrollEarningsDetailsList = null;
		try {
			payrollEarningsDetailsList = this.sessionFactory.getCurrentSession().getNamedQuery(PayrollEarningsDetailsDO.FIND_BY_ID)
										.setParameter(CommonConstants.ID, id)
										.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return payrollEarningsDetailsList;
	}
	
	public PayrollEarningsDetailsDO update(PayrollEarningsDetailsDO payrollEarningsDetailsDO) {
		try {
			genericObject.merge(payrollEarningsDetailsDO);
		} catch (Exception eException) {
		} finally {
		}
		return payrollEarningsDetailsDO;
	}
	
	@SuppressWarnings("unchecked")
	public List<PayrollEarningsDetailsDO> retrieveByEmpCompensationIds(List<Long> ids) {
		List<PayrollEarningsDetailsDO> employeeEarningsList = null;
		try {
			employeeEarningsList = this.sessionFactory.getCurrentSession().getNamedQuery(PayrollEarningsDetailsDO.FIND_BY_IDS)
										.setParameter(CommonConstants.ID, ids)
										.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeEarningsList;
	}
	
	public boolean deletePayrollById(Long payrollId)  {
		try {
			org.hibernate.Query query = this.sessionFactory.getCurrentSession().createQuery("delete from PayrollEarningsDetailsDO e where e.payroll.payrollId=:id")
					.setParameter(CommonConstants.ID, payrollId);
			query.executeUpdate();
			
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return true;
	}
	
}
