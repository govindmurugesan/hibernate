package com.spheresuite.erp.dao;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.EmployeePayrollDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class EmployeePayrollDAO {
	static Logger logger = Logger.getLogger(EmployeePayrollDAO.class.getName());
	@Autowired
	private GenericDAOImpl<EmployeePayrollDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public EmployeePayrollDO persist(EmployeePayrollDO employeePayrollDO) {
		try {
			genericObject.persist(employeePayrollDO);
		} catch (Exception eException) {
		} finally {
		}
		return employeePayrollDO;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeePayrollDO> retriveByMonthAndEmp(String month, String empId)  {
		List<EmployeePayrollDO> empPayrollList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeePayrollDO.FIND_BY_MONTH_EMP)
			.setParameter(CommonConstants.MONTHLY, month)
			.setParameter(CommonConstants.EMPID, empId)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return empPayrollList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<EmployeePayrollDO> retriveByWeekAndEmp(String fromDate, String todate, String empId)  {
		List<EmployeePayrollDO> empPayrollList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeePayrollDO.FIND_BY_WEEK_EMP)
			.setParameter(CommonConstants.FROMDATE, fromDate)
			.setParameter(CommonConstants.TODATE, todate)
			.setParameter(CommonConstants.EMPID, empId)
			.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return empPayrollList;
	}
	
	@SuppressWarnings("rawtypes")
	public Object[] retrieveByEmpIdBetweenDateSum(String empId, String fromMnth, String toMonth)  {
		Object[] empPayrollList = null;
		try {
			List result =  this.sessionFactory.getCurrentSession().getNamedQuery(EmployeePayrollDO.FIND_BY_EMP_BEWTEENDATE_SUM)
					.setParameter("fromMonth", fromMnth)
					.setParameter("toMonth", toMonth)
					.setParameter(CommonConstants.EMPID, empId)
					.setParameter(CommonConstants.STATUS,'p')
					.list();
			if(result != null && result.size() > 0){
				empPayrollList = (Object[]) result.get(0);
			}
		} catch (Exception eException) {
			return null;
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return empPayrollList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeePayrollDO>retrieveByEmpIdBetweenDate(String empId, String fromMnth, String toMonth)  {
		List<EmployeePayrollDO> empPayrollList = null;
		try {
			 return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeePayrollDO.FIND_BY_EMP_BEWTEENDATE)
				.setParameter("fromMonth", fromMnth)
				.setParameter("toMonth", toMonth)
				.setParameter(CommonConstants.EMPID, empId)
				.setParameter(CommonConstants.STATUS,'p')
				.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return empPayrollList;
	}
	
	@SuppressWarnings("rawtypes")
	public Object[] retrieveByEmpIdForMonthSum(String empId, String fromMnth)  {
		Object[] empPayrollList = null;
		try {
			List result =  this.sessionFactory.getCurrentSession().getNamedQuery(EmployeePayrollDO.FIND_BY_EMP_DATE_SUM)
					.setParameter("fromMonth", fromMnth)
					.setParameter(CommonConstants.EMPID, empId)
					.setParameter(CommonConstants.STATUS,'p')
					.list();
			
			if(result != null && result.size() > 0){
				empPayrollList = (Object[]) result.get(0);
			}
		} catch (Exception eException) {
			return null;
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return empPayrollList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<EmployeePayrollDO> retriveByMonthAndBatch(String month, Long payrollBatchId)  {
		List<EmployeePayrollDO> empPayrollList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeePayrollDO.FIND_BY_MONTH_BATCh)
			.setParameter(CommonConstants.MONTHLY, month)
			.setParameter(CommonConstants.PAYROLLBATCHID, payrollBatchId)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return empPayrollList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<EmployeePayrollDO> retriveForProcessPayroll(String month, Long payrollBatchId)  {
		List<EmployeePayrollDO> empPayrollList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeePayrollDO.FIND_FOR_PROCESS)
			.setParameter(CommonConstants.MONTHLY, month)
			.setParameter(CommonConstants.PAYROLLBATCHID, payrollBatchId)
			.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return empPayrollList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeePayrollDO> retriveForSubmitApprovel(String month, Long payrollBatchId)  {
		List<EmployeePayrollDO> empPayrollList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeePayrollDO.FIND_FOR_PROCESS)
			.setParameter(CommonConstants.MONTHLY, month)
			.setParameter(CommonConstants.PAYROLLBATCHID, payrollBatchId)
			.setParameter(CommonConstants.STATUS, CommonConstants.DRAFT)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return empPayrollList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeePayrollDO> retriveWeelyPayrollForSubmitApprovel(String fromDate,String toDate,Long payrollBatchId)  {
		List<EmployeePayrollDO> empPayrollList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeePayrollDO.FIND_WEEKLY_FOR_PROCESS)
			.setParameter(CommonConstants.FROMDATE, fromDate)
			.setParameter(CommonConstants.TODATE, toDate)
			.setParameter(CommonConstants.PAYROLLBATCHID, payrollBatchId)
			.setParameter(CommonConstants.STATUS, CommonConstants.DRAFT)
			.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return empPayrollList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<EmployeePayrollDO> retriveDailyPayrollForSubmitApprovel(String date,Long payrollBatchId)  {
		List<EmployeePayrollDO> empPayrollList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeePayrollDO.FIND_DAILY_FOR_PROCESS)
			.setParameter(CommonConstants.DATE, date)
			.setParameter(CommonConstants.PAYROLLBATCHID, payrollBatchId)
			.setParameter(CommonConstants.STATUS, CommonConstants.DRAFT)
			.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return empPayrollList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeePayrollDO> retriveWeelyPayrollForApprovel(String fromDate,String toDate,Long payrollBatchId)  {
		List<EmployeePayrollDO> empPayrollList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeePayrollDO.FIND_WEEKLY_FOR_PROCESS)
			.setParameter(CommonConstants.FROMDATE, fromDate)
			.setParameter(CommonConstants.TODATE, toDate)
			.setParameter(CommonConstants.PAYROLLBATCHID, payrollBatchId)
			.setParameter(CommonConstants.STATUS, CommonConstants.WSTATE)
			.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return empPayrollList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeePayrollDO> retriveWeelyPayrollForProcess(String fromDate,String toDate,Long payrollBatchId)  {
		List<EmployeePayrollDO> empPayrollList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeePayrollDO.FIND_WEEKLY_FOR_PROCESS)
			.setParameter(CommonConstants.FROMDATE, fromDate)
			.setParameter(CommonConstants.TODATE, toDate)
			.setParameter(CommonConstants.PAYROLLBATCHID, payrollBatchId)
			.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
			.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return empPayrollList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeePayrollDO> retriveDailyPayrollForProcess(String date,Long payrollBatchId)  {
		List<EmployeePayrollDO> empPayrollList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeePayrollDO.FIND_DAILY_FOR_PROCESS)
			.setParameter(CommonConstants.DATE, date)
			.setParameter(CommonConstants.PAYROLLBATCHID, payrollBatchId)
			.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
			.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return empPayrollList;
	}
	
	public EmployeePayrollDO update(EmployeePayrollDO employeePayrollDO)  {
		try {
			genericObject.merge(employeePayrollDO);
		} catch (Exception eException) {
		} finally {
		}
		return employeePayrollDO;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeePayrollDO> retrieveById(Long id)  {
		List<EmployeePayrollDO> empPayrollList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeePayrollDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return empPayrollList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<Object[]> retrieveForSummary()  {
		List<Object[]> empPayrollList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeePayrollDO.FIND_FOR_SUMMARY)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return empPayrollList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> retrieveForSummaryByBatchId(String month)  {
		List<Object[]> empPayrollList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeePayrollDO.FIND_FOR_SUMMARY_BY_BATCHID)
					.setParameter(CommonConstants.MONTHLY, month)
					//.setParameter(CommonConstants.STATUS, status)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return empPayrollList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeePayrollDO> retrieveByAllEmp(String payrollMonth, Long payrollbatchId, char status)  {
		List<EmployeePayrollDO> empPayrollList = null;
		try {
			 return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeePayrollDO.FIND_ALL_EMP)
			 	.setParameter(CommonConstants.PAYROLLMONTH, payrollMonth)
				.setParameter(CommonConstants.STATUS, status)
				.setParameter(CommonConstants.PAYROLLBATCHID, payrollbatchId)
				.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return empPayrollList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeePayrollDO> retrieveByEmpIdAndStatus(String empId)  {
		List<EmployeePayrollDO> empPayrollList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeePayrollDO.FIND_BY_EMP_STATUS)
					.setParameter(CommonConstants.STATUS, CommonConstants.PSTATE)
					.setParameter(CommonConstants.EMPID, empId)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return empPayrollList;
	}
	
	public boolean  deletePayrollById(Long payrollId)  {
		try {
			org.hibernate.Query query = this.sessionFactory.getCurrentSession().createQuery("delete from EmployeePayrollDO e where e.payrollId=:id")
					.setParameter(CommonConstants.ID, payrollId);
			query.executeUpdate();
			
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return true;
	}
	
	
	
	@SuppressWarnings("unchecked")
	public List<Object[]> retrieveForWeeklySummaryByBatchId(String month)  {
		List<Object[]> empPayrollList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeePayrollDO.FIND_BY_WEEKLY_PAYROLLSUMMARY_BATCHID)
					.setParameter(CommonConstants.MONTHLY, month)
					//.setParameter(CommonConstants.STATUS, status)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return empPayrollList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeePayrollDO> retriveDailyEmpPayroll(String empId, String date, Long BatchId)  {
		List<EmployeePayrollDO> empPayrollList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeePayrollDO.FIND_DAILY_PAYROLL)
			.setParameter(CommonConstants.PAYROLLBATCHID, BatchId)
			.setParameter(CommonConstants.DATE, date)
			.setParameter(CommonConstants.EMPID, empId)
			.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return empPayrollList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeePayrollDO> retrieveByPayrollMonth(String payrollMonth, char status)  {
		List<EmployeePayrollDO> empPayrollList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeePayrollDO.FIND_BY_PAYROLLMONTH)
			.setParameter(CommonConstants.PAYROLLMONTH, payrollMonth)
			.setParameter(CommonConstants.STATUS, status)
			.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return empPayrollList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<EmployeePayrollDO> retrieveByPayrollMonthByEmpID(String payrollMonth, char status, String empid)  {
		List<EmployeePayrollDO> empPayrollList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeePayrollDO.FIND_BY_PAYROLLMONTH_EMPID)
			.setParameter(CommonConstants.PAYROLLMONTH, payrollMonth)
			.setParameter(CommonConstants.STATUS, status)
			.setParameter(CommonConstants.EMPID, empid)
			.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return empPayrollList;
	}
	
	/*public EmployeePayrollDO persist(EmployeePayrollDO employeePayrollDO) {
		try {
			genericObject.persist(employeePayrollDO);
		} catch (Exception eException) {
		} finally {
		}
		return employeePayrollDO;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<EmployeePayrollDO> retrieve()  {
		List<EmployeePayrollDO> empPayrollList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeePayrollDO.FIND_ALL)
			.setParameter(CommonConstants.STATUS, 'p')
			.list();
		} catch (Exception eException) {
		} finally {
		}
		return empPayrollList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeePayrollDO> retrieveActive(Long empId)  {
		List<EmployeePayrollDO> empPayrollList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeePayrollDO.FIND_BY_STATUS)
			.setParameter(CommonConstants.STATUS, 'p')
			.setParameter(CommonConstants.EMPID, empId)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return empPayrollList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeePayrollDO> retrieveById(Long id)  {
		List<EmployeePayrollDO> empPayrollList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeePayrollDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return empPayrollList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeePayrollDO> retrieveByEmpId(Long empId)  {
		List<EmployeePayrollDO> empPayrollList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeePayrollDO.FIND_BY_EMP_ID)
					.setParameter(CommonConstants.STATUS, 'p')
					.setParameter(CommonConstants.EMPID, empId)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return empPayrollList;
	}
	
	
	
	
	
	
	@SuppressWarnings("unchecked")
	public List<EmployeePayrollDO> retrieveByAllEmp(String payrollMonth, Long payrollbatchId, char status)  {
		List<EmployeePayrollDO> empPayrollList = null;
		try {
			 return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeePayrollDO.FIND_ALL_EMP)
			 	.setParameter(CommonConstants.PAYROLLMONTH, payrollMonth)
				.setParameter(CommonConstants.STATUS, status)
				.setParameter(CommonConstants.PAYROLLBATCHID, payrollbatchId)
				.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return empPayrollList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeePayrollDO> retriveByMonth(String month, Long payrollBatchId)  {
		List<EmployeePayrollDO> empPayrollList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeePayrollDO.FIND_BY_MONTH)
			.setParameter(CommonConstants.MONTHLY, month)
			.setParameter(CommonConstants.PAYROLLBATCHID, payrollBatchId)
			.setParameter(CommonConstants.STATUS, CommonConstants.DRAFT)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return empPayrollList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeePayrollDO> retriveForReportMonth(String month, Long batchId)  {
		List<EmployeePayrollDO> empPayrollList = null;
		try {
			if(batchId == null){
				return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeePayrollDO.FIND_FOR_REPORT)
				.setParameter(CommonConstants.MONTHLY, month)
				.list();
			}
			if(batchId != null){
				
				return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeePayrollDO.FIND_FOR_REPORT_BATCH)
				.setParameter(CommonConstants.MONTHLY, month)
				.setParameter(CommonConstants.PAYROLLBATCHID, batchId)
				.list();
			}
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return empPayrollList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeePayrollDO> retriveForReportQuarter(String fromMonth,String toMonth, Long batchId)  {
		List<EmployeePayrollDO> empPayrollList = null;
		try {
			if(batchId == null){
				
				return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeePayrollDO.FIND_FOR_REPORT_BEWTEENDATE)
				.setParameter("fromMonth", fromMonth)
				.setParameter("toMonth", toMonth)
				.list();
			}
			if(batchId != null){
				return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeePayrollDO.FIND_FOR_REPORT_BEWTEENDATE_BATCH)
				.setParameter("fromMonth", fromMonth)
				.setParameter("toMonth", toMonth)
				.setParameter(CommonConstants.PAYROLLBATCHID, batchId)
				.list();
			}
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return empPayrollList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeePayrollDO> retriveByStatus(String month, Long payrollBatchId)  {
		List<EmployeePayrollDO> empPayrollList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeePayrollDO.FIND_BY_MONTH)
			.setParameter(CommonConstants.MONTHLY, month)
			.setParameter(CommonConstants.PAYROLLBATCHID, payrollBatchId)
			.setParameter(CommonConstants.STATUS, 'p')
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return empPayrollList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeePayrollDO> retriveByMonthAndEmp(String month, Long empId)  {
		List<EmployeePayrollDO> empPayrollList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeePayrollDO.FIND_BY_MONTH_EMP)
			.setParameter(CommonConstants.MONTHLY, month)
			.setParameter(CommonConstants.EMPID, empId)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return empPayrollList;
	}
	
	public EmployeePayrollDO update(EmployeePayrollDO employeePayrollDO)  {
		try {
			genericObject.merge(employeePayrollDO);
		} catch (Exception eException) {
		} finally {
		}
		return employeePayrollDO;
	}

	
	
	@SuppressWarnings("unchecked")
	public List<Object[]> retrieveForSummaryByBatchId(String month)  {
		List<Object[]> empPayrollList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeePayrollDO.FIND_FOR_SUMMARY_BY_BATCHID)
					.setParameter(CommonConstants.MONTHLY, month)
					//.setParameter(CommonConstants.STATUS, status)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return empPayrollList;
	}*/
}
