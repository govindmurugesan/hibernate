package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.EmployeeBonusDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class EmployeeBonusDAO {
	static Logger logger = Logger.getLogger(EmployeeBonusDAO.class.getName());
	@Autowired
	private GenericDAOImpl<EmployeeBonusDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public EmployeeBonusDO persist(EmployeeBonusDO employeeBonusDO) {
		try {
			genericObject.persist(employeeBonusDO);
		} catch (Exception eException) {
		} finally {
		}
		return employeeBonusDO;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeBonusDO> retrieveByEmpId(String empId) {
		List<EmployeeBonusDO> employeeBonusList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeBonusDO.FIND_BY_EMPID)
					.setParameter(CommonConstants.EMPID, empId)
					.list(); 
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeBonusList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeBonusDO> retrieveByEmpIdWithDate(List<Long> empIds, String bonusMonth) {
		List<EmployeeBonusDO> employeeBonusList = null;
		try {
			if(empIds != null && empIds.size() > 0){
				return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeBonusDO.FIND_BY_EMPID_MONTH)
					.setParameter(CommonConstants.EMPID, empIds)
					.setParameter(CommonConstants.STATUS, 'a')
					.setParameter(CommonConstants.MONTHLY, bonusMonth)
					.list(); 
			}else{
				return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeBonusDO.FIND_BY_MONTH)
					.setParameter(CommonConstants.STATUS, 'a')
					.setParameter(CommonConstants.MONTHLY, bonusMonth)
					.list();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeBonusList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeBonusDO> retrieveByEmpIdBetweenDate(List<Long> empIds, String fromMonth, String toMonth) {
		List<EmployeeBonusDO> employeeBonusList = null;
		try {
				if(empIds != null && empIds.size() > 0){
					return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeBonusDO.FIND_BY_EMPID_BETWEENMONTH)
						.setParameter(CommonConstants.EMPID, empIds)
						.setParameter(CommonConstants.STATUS, 'a')
						.setParameter("fromMonth", fromMonth)
						.setParameter("toMonth", toMonth)
						.list();
				}else{
					return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeBonusDO.FIND_BETWEENMONTH)
					.setParameter(CommonConstants.STATUS, 'a')
					.setParameter("fromMonth", fromMonth)
					.setParameter("toMonth", toMonth)
					.list();
				}
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeBonusList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeBonusDO> retrieveByEmpIdAndMonth(String empId, String month) {
		List<EmployeeBonusDO> employeeBonusList = null;
		try {
				return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeBonusDO.FIND_BY_EMPID_DATE)
					.setParameter(CommonConstants.EMPID, empId)
					.setParameter(CommonConstants.MONTHLY, month)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
				
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeBonusList;
	}
	
	@SuppressWarnings("rawtypes")
	public Long retrieveByEmpIdBetweenMonth(String empId, String fromMonth, String toMonth) {
		Long employeeBonusList = null;
		try {
				List longList =  this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeBonusDO.FIND_BY_EMPID_BETWEEN_DATE)
						.setParameter(CommonConstants.EMPID, empId)
						.setParameter("fromMonth", fromMonth)
						.setParameter("toMonth", toMonth)
						.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
						.list();
				
				if(longList !=null && longList.size() > 0){
					employeeBonusList = (Long) longList.get(0);
				}
				
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeBonusList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeBonusDO> retrieveByEmpIdAndMonthWithOutStatus(Long empId, String month) {
		List<EmployeeBonusDO> employeeBonusList = null;
		try {
			
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeBonusDO.FIND_BY_EMPID_DATE)
					.setParameter(CommonConstants.EMPID, empId)
					.setParameter(CommonConstants.MONTHLY, month)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeBonusList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeBonusDO> retrieve() {
		List<EmployeeBonusDO> employeeBonusList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeBonusDO.FIND_ALL)
					.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
		}
		return employeeBonusList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeBonusDO> retrieveById(Long id) {
		List<EmployeeBonusDO> employeeBonusList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeBonusDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeBonusList;
	}
	
	public EmployeeBonusDO update(EmployeeBonusDO employeeBonusDO) {
		try {
			genericObject.merge(employeeBonusDO);
		} catch (Exception eException) {
		} finally {
		}
		return employeeBonusDO;
	}
	
	public boolean delete(Long id)  {
		try {
			org.hibernate.Query query = this.sessionFactory.getCurrentSession().createQuery("delete from EmployeeBonusDO e where e.empBonusId=:id")
					.setParameter(CommonConstants.ID, id);
			query.executeUpdate();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return true;
	}
	

}
