	package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.PFDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class PFDAO {
	static Logger logger = Logger.getLogger(PFDAO.class.getName());
	@Autowired
	private GenericDAOImpl<PFDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public boolean persist(PFDO pfDO) {
		boolean persistStatus = true;
		try {
			genericObject.persist(pfDO);
			/*List<PFDO> pflist = null;
			pflist =  this.sessionFactory.getCurrentSession().getNamedQuery(PFDO.FIND_BY_SALARY)
					.setParameter(CommonConstants.FROMAMOUNT, pfDO.getSalaryFrom())
					.setParameter(CommonConstants.TOAMOUNT, pfDO.getSalaryTo())
					.list();
			if(pflist != null && pflist.size() > 0){
				persistStatus = false;
			}else{
				genericObject.persist(pfDO);
			}*/
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	public List<PFDO> retrieve()  {
		List<PFDO> pflist = null;
		try {
			pflist = genericObject.retrieve(PFDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return pflist;
	}
	
	@SuppressWarnings("unchecked")
	public List<PFDO> retrieveById(Long id)  {
		List<PFDO> pflist = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(PFDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return pflist;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<PFDO> retrieveActive()  {
		List<PFDO> pflist = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(PFDO.FIND_BY_STATUS)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return pflist;
	}
	
	/*@SuppressWarnings("unchecked")
	public List<PFDO> retrieveByDate(Date fromDate, Date toDate)  {
		List<PFDO> educationCessList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(PFDO.FIND_BY_DATE)
			.setParameter(CommonConstants.FROMDATE, fromDate)
			.setParameter(CommonConstants.TODATE, toDate)
			.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return educationCessList;
	}*/
	
/*	@SuppressWarnings("unchecked")
	public List<PFDO> retrieveByCurrentDate(Date date)  {
		List<EducationCessDO> educationCessList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EducationCessDO.FIND_BY_CURRENTDATE)
			.setParameter(CommonConstants.DATE, date)
			.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
			.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return educationCessList;
	}*/
	
	public boolean update(PFDO pfDO) {
		boolean updateStatus = true;
		try {
			genericObject.merge(pfDO);
			/*List<PFDO> pflist = null;
			pflist =  this.sessionFactory.getCurrentSession().getNamedQuery(PFDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.TOAMOUNT, pfDO.getSalaryTo())
					.setParameter(CommonConstants.FROMAMOUNT, pfDO.getSalaryFrom())
					.setParameter(CommonConstants.ID, pfDO.getPfId())
					.list();
			if(pflist != null && pflist.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(pfDO);
			}*/
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}
	
	public boolean persistList(List<PFDO> pflist){
		try {
			for (PFDO pf : pflist) {
				genericObject.persist(pf);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}

}
