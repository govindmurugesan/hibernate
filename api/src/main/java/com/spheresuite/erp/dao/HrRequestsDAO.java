package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.HrRequestsDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class HrRequestsDAO {
	static Logger logger = Logger.getLogger(HrRequestsDAO.class.getName());
	@Autowired
	private GenericDAOImpl<HrRequestsDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public HrRequestsDO persist(HrRequestsDO hrrequestDetail)  {
		try {
			genericObject.persist(hrrequestDetail);
		} catch (Exception eException) {
		} finally {
		}
		return hrrequestDetail;
	}
	
	@SuppressWarnings("unchecked")
	public List<HrRequestsDO> retrieve()  {
		List<HrRequestsDO> hrRequestsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(HrRequestsDO.FIND_ALL)
					.list();
		} catch (Exception eException) {
		} finally {
		}
		return hrRequestsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<HrRequestsDO> retrieveById(Long Id)  {
		List<HrRequestsDO> hrRequestList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(HrRequestsDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, Id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return hrRequestList;
	}
	
	public HrRequestsDO update(HrRequestsDO hrRequest)  {
		try {
			genericObject.merge(hrRequest);
		} catch (Exception eException) {
		} finally {
		}
		return hrRequest;
	}
	
	@SuppressWarnings("unchecked")
	public List<HrRequestsDO> retrieveByEmpId(String id)  {
		List<HrRequestsDO> requestList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(HrRequestsDO.FINDBY_EMP_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return requestList;
	}
}
