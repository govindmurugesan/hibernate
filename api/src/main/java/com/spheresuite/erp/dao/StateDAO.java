package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.StateDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class StateDAO {
	static Logger logger = Logger.getLogger(StateDAO.class.getName());
	
	@Autowired
	private GenericDAOImpl<StateDO> dao;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(StateDO stateDO) {
		boolean persistStatus = true;
		try {
			List<StateDO> stateDOList = null;
			stateDOList = this.sessionFactory.getCurrentSession().getNamedQuery(StateDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, stateDO.getStateName())
					.list();
					if(stateDOList != null && stateDOList.size() > 0){
						persistStatus = false;
					}else{
						dao.persist(stateDO);
					}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	/*public List<StateDO> retrieve() {
		List<StateDO> stateList = null;
		try {
			stateList = dao.retrieve(StateDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return stateList;
	}*/
	
	@SuppressWarnings("unchecked")
	public List<StateDO> retrieve() {
		List<StateDO> stateDO = null;
		try {
			stateDO =  this.sessionFactory.getCurrentSession().getNamedQuery(StateDO.FIND_ALL)
						.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return stateDO;
	}
	
	public List<StateDO> retrieveActive() {
		List<StateDO> stateList = null;
		try {
			stateList =  dao.retrieveActive(CommonConstants.ACTIVE, StateDO.FIND_BY_STATUS, StateDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return stateList;
	}
	
	@SuppressWarnings("unchecked")
	public List<StateDO> retrieveById(Long stateId) {
		List<StateDO> stateDO = null;
		try {
			stateDO =  this.sessionFactory.getCurrentSession().getNamedQuery(StateDO.FIND_BY_ID)
						.setParameter(CommonConstants.ID, stateId).list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return stateDO;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<StateDO> retrieveByName(String name) {
		List<StateDO> stateDO = null;
		try {
			stateDO =  this.sessionFactory.getCurrentSession().getNamedQuery(StateDO.FIND_BY_NAME)
						.setParameter(CommonConstants.NAME, name).list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return stateDO;
	}
	
	public List<StateDO> retrieveByCountryId(Long countryId) {
		List<StateDO> stateDO = null;
		try {
			stateDO =  dao.retrieveByCountryId(countryId, StateDO.FIND_BY_COUNTRY_ID, StateDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return stateDO;
	}
	
	@SuppressWarnings("unchecked")
	public boolean update(StateDO stateDO) {
		boolean updateStatus = true;
		try {
			List<StateDO> stateDOList = null;
			stateDOList = this.sessionFactory.getCurrentSession().getNamedQuery(StateDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, stateDO.getStateName())
					.setParameter(CommonConstants.ID, stateDO.getStateId())
					.list();
			if(stateDOList != null && stateDOList.size() > 0){
				updateStatus = false;
			}else{
				dao.merge(stateDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}
	
	@SuppressWarnings("unchecked")
	public List<StateDO> retrieveByActiveName(String name) {
		List<StateDO> stateDO = null;
		try {
			stateDO =  this.sessionFactory.getCurrentSession().getNamedQuery(StateDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, name)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return stateDO;
	}
	
	public boolean persistList(List<StateDO> stateDO){
		try {
			for (StateDO state : stateDO) {
				dao.persist(state);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}
}
