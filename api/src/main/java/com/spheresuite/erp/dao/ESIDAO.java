	package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.ESIDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class ESIDAO {
	static Logger logger = Logger.getLogger(ESIDAO.class.getName());
	@Autowired
	private GenericDAOImpl<ESIDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public boolean persist(ESIDO esiDO) {
		boolean persistStatus = true;
		try {
			genericObject.persist(esiDO);
			/*List<ESIDO> esilist = null;
			esilist =  this.sessionFactory.getCurrentSession().getNamedQuery(ESIDO.FIND_BY_SALARY)
					.setParameter(CommonConstants.FROMAMOUNT, esiDO.getSalaryFrom())
					.setParameter(CommonConstants.TOAMOUNT, esiDO.getSalaryTo())
					.list();
			if(esilist != null && esilist.size() > 0){
				persistStatus = false;
			}else{
				genericObject.persist(esiDO);
			}*/
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	public List<ESIDO> retrieve()  {
		List<ESIDO> esilist = null;
		try {
			esilist = genericObject.retrieve(ESIDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return esilist;
	}
	
	@SuppressWarnings("unchecked")
	public List<ESIDO> retrieveById(Long id)  {
		List<ESIDO> esilist = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(ESIDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return esilist;
	}
	
	@SuppressWarnings("unchecked")
	public List<ESIDO> retrieveActive()  {
		List<ESIDO> esilist = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(ESIDO.FIND_ACTIVE)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return esilist;
	}
	
	
	/*@SuppressWarnings("unchecked")
	public List<PFDO> retrieveByDate(Date fromDate, Date toDate)  {
		List<PFDO> educationCessList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(PFDO.FIND_BY_DATE)
			.setParameter(CommonConstants.FROMDATE, fromDate)
			.setParameter(CommonConstants.TODATE, toDate)
			.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return educationCessList;
	}*/
	
/*	@SuppressWarnings("unchecked")
	public List<PFDO> retrieveByCurrentDate(Date date)  {
		List<EducationCessDO> educationCessList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EducationCessDO.FIND_BY_CURRENTDATE)
			.setParameter(CommonConstants.DATE, date)
			.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
			.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return educationCessList;
	}*/
	
	public boolean update(ESIDO esiDO) {
		boolean updateStatus = true;
		try {
			genericObject.merge(esiDO);
			/*List<ESIDO> esilist = null;
			esilist =  this.sessionFactory.getCurrentSession().getNamedQuery(ESIDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.TOAMOUNT, esiDO.getSalaryTo())
					.setParameter(CommonConstants.FROMAMOUNT, esiDO.getSalaryFrom())
					.setParameter(CommonConstants.ID, esiDO.getEsiId())
					.list();
			if(esilist != null && esilist.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(esiDO);
			}*/
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}
	
	public boolean persistList(List<ESIDO> esilist){
		try {
			for (ESIDO esi : esilist) {
				genericObject.persist(esi);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}

}
