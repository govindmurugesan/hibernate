package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.EducationLevelDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class EducationLevelDAO {
	static Logger logger = Logger.getLogger(EducationLevelDAO.class.getName());
	@Autowired
	private GenericDAOImpl<EducationLevelDO> genericObject;
	@Autowired
	private SessionFactory sessionFactory;
	@SuppressWarnings("unchecked")
	public boolean persist(EducationLevelDO educationLevelDO) {
		boolean persistStatus = true;
		try {
			List<EducationLevelDO> educationLevelDOList = null;
			educationLevelDOList = this.sessionFactory.getCurrentSession().getNamedQuery(EducationLevelDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, educationLevelDO.getName())
					.list();
					if(educationLevelDOList != null && educationLevelDOList.size() > 0){
						persistStatus = false;
					}else{
						genericObject.persist(educationLevelDO);
					}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	
	public List<EducationLevelDO> retrieve() {
		List<EducationLevelDO> educationLevelList = null;
		try {
			educationLevelList = genericObject.retrieve(EducationLevelDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return educationLevelList;
	}
	
	public List<EducationLevelDO> retrieveActive() {
		List<EducationLevelDO> educationLevelList = null;
		try {
			return genericObject.retrieveActive(CommonConstants.ACTIVE, 
					EducationLevelDO.FIND_BY_STATUS, EducationLevelDO.class);
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return educationLevelList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EducationLevelDO> retrieveById(Long id) {
		 List<EducationLevelDO> educationLevelList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EducationLevelDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return educationLevelList;
	}
	
	@SuppressWarnings("unchecked")
	public boolean update(EducationLevelDO educationLevelDO) {
		boolean updateStatus = true;
		try {
			List<EducationLevelDO> educationLevelDOList = null;
			educationLevelDOList = this.sessionFactory.getCurrentSession().getNamedQuery(EducationLevelDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, educationLevelDO.getName())
					.setParameter(CommonConstants.ID, educationLevelDO.getEducationLevelId())
					.list();
			if(educationLevelDOList != null && educationLevelDOList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(educationLevelDO);
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return updateStatus;
	}
	
	public boolean persistList(List<EducationLevelDO> educationLevelList){
		try {
			for (EducationLevelDO educationLevel : educationLevelList) {
				genericObject.persist(educationLevel);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}

}
