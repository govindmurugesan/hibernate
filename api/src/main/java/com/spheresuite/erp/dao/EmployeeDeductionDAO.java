package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.EmployeeDeductionDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class EmployeeDeductionDAO {
	static Logger logger = Logger.getLogger(EmployeeDeductionDAO.class.getName());
	@Autowired
	private GenericDAOImpl<EmployeeDeductionDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public List<EmployeeDeductionDO> persist(List<EmployeeDeductionDO> employeeDeductionDO) {
		try {
				for (EmployeeDeductionDO employeeDeductionDO2 : employeeDeductionDO) {
					genericObject.persist(employeeDeductionDO2);
				}
		} catch (Exception eException) {
		} finally {
		}
		return employeeDeductionDO;
	}
	
	
	public List<EmployeeDeductionDO> retrieve() {
		List<EmployeeDeductionDO> employeeDeductionList = null;
		
		try {
			employeeDeductionList = genericObject.retrieve(EmployeeDeductionDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return employeeDeductionList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeDeductionDO> retrieveByEmpCompensationId(Long id) {
		List<EmployeeDeductionDO> employeeDeductionList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeDeductionDO.FIND_BY_ID)
			.setParameter(CommonConstants.ID, id)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeDeductionList;
	}
	
	public EmployeeDeductionDO update(EmployeeDeductionDO employeeDeductionDO) {
		try {
			genericObject.merge(employeeDeductionDO);
		} catch (Exception eException) {
		} finally {
		}
		return employeeDeductionDO;
	}

}
