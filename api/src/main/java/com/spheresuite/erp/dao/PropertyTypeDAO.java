package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.PropertyTypeDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class PropertyTypeDAO {
	static Logger logger = Logger.getLogger(PropertyTypeDAO.class.getName());

	@Autowired
	private GenericDAOImpl<PropertyTypeDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public PropertyTypeDO persist(PropertyTypeDO propertyTypeDO) {
		try {
			genericObject.persist(propertyTypeDO);
		} catch (Exception eException) {
			//throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			//SessionManager.closeEntityManager(em);
		}
		return propertyTypeDO;
	}
	
	
	public List<PropertyTypeDO> retrieve() {
		List<PropertyTypeDO> propertyTypeList = null;
		try {
			propertyTypeList = genericObject.retrieve(PropertyTypeDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return propertyTypeList;
	}
	
	public List<PropertyTypeDO> retrieveActive() {
		List<PropertyTypeDO> propertyTypeList = null;
		try {
			propertyTypeList =  genericObject.retrieveActive(CommonConstants.ACTIVE, PropertyTypeDO.FIND_BY_STATUS, PropertyTypeDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return propertyTypeList;
	}
	
	@SuppressWarnings("unchecked")
	public List<PropertyTypeDO> retrieveById(Long id) {
		List<PropertyTypeDO> propertyTypeList = null;
		try {
			propertyTypeList =  this.sessionFactory.getCurrentSession().getNamedQuery(PropertyTypeDO.FIND_BY_ID)
								.setParameter(CommonConstants.ID, id).list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return propertyTypeList;
	}
	
	public PropertyTypeDO update(PropertyTypeDO propertyTypeDO) {
		try {
			genericObject.merge(propertyTypeDO);
		} catch (Exception eException) {
		} finally {
		}
		return propertyTypeDO;
	}

}
