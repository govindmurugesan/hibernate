package com.spheresuite.erp.dao;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.EmployeeESIInfoDO;
import com.spheresuite.erp.domainobject.EmployeePFInfoDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class EmployeeESIInfoDAO {
	static Logger logger = Logger.getLogger(EmployeeESIInfoDAO.class.getName());
	@Autowired
	private GenericDAOImpl<EmployeeESIInfoDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public boolean persist(EmployeeESIInfoDO employeeDO) {
		boolean persistStatus = true;
		try {
			genericObject.persist(employeeDO);
			/*List<EmployeePFAndESIInfoDO> empList = null;
			empList = this.sessionFactory.getCurrentSession().getNamedQuery(EmployeePFAndESIInfoDO.FIND_BY_ACCOUNTID)
					.setParameter(CommonConstants.PFACCOUNTNO, employeeDO.getPFAccNo())
					.setParameter(CommonConstants.ESIACCOUNTNO, employeeDO.getESIAccNo())
					.list();
					if(empList != null && empList.size() > 0){
						persistStatus = false;
					}else{
						genericObject.persist(employeeDO);
					}*/
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
		}
		return persistStatus;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeESIInfoDO> retrieveByEmpId(String empId) {
		List<EmployeeESIInfoDO> employeeInformationList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeESIInfoDO.FIND_BY_EMPID)
					.setParameter(CommonConstants.EMPID, empId)
					.list(); 
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeInformationList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeESIInfoDO> retrieveByEmpIdAndDate(String empId, Date fromDate, Date toDate) {
		List<EmployeeESIInfoDO> employeeInformationList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeESIInfoDO.FIND_BY_EMPID_DATE)
					.setParameter(CommonConstants.EMPID, empId)
					.setParameter(CommonConstants.FROMDATE, fromDate)
					.setParameter(CommonConstants.TODATE, toDate)
					.list(); 
		} catch (Exception eException) {
			eException.printStackTrace();
			System.out.println("eException"+eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeInformationList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeESIInfoDO> retrieveById(Long id) {
		List<EmployeeESIInfoDO> employeeInformationList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeESIInfoDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeInformationList;
	}
	
	public EmployeeESIInfoDO update(EmployeeESIInfoDO employeeInfo) {
		try {
			genericObject.merge(employeeInfo);
		} catch (Exception eException) {
		} finally {
		}
		return employeeInfo;
	}
	
	public boolean persistList(List<EmployeeESIInfoDO> esiDetailDO){
		try {
			for (EmployeeESIInfoDO esi : esiDetailDO) {
				genericObject.persist(esi);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}


}
