package com.spheresuite.erp.dao;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.TaxSlabsSettingsDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class TaxSlabsSettingsDAO {
	static Logger logger = Logger.getLogger(TaxSlabsSettingsDAO.class.getName());

	@Autowired
	private GenericDAOImpl<TaxSlabsSettingsDO> dao;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(TaxSlabsSettingsDO taxSlabsSettings) {
		boolean persistStatus = true;
		try {
			dao.persist(taxSlabsSettings);
			/*List<TaxSlabsSettingsDO> taxSlabList = null;
			taxSlabList =  this.sessionFactory.getCurrentSession().getNamedQuery(TaxSlabsSettingsDO.FIND_FOR_ADD)
					.setParameter(CommonConstants.FROMDATE, taxSlabsSettings.getStartdate())
					.setParameter(CommonConstants.TODATE, taxSlabsSettings.getEnddate())
					.setParameter(CommonConstants.FROMAGE, taxSlabsSettings.getEmpAgeFrom())
					.setParameter(CommonConstants.TOAGE, taxSlabsSettings.getEmpAgeTo())
					.list();
			if(taxSlabList != null && taxSlabList.size() > 0){
				persistStatus = false;
			}else{
				dao.persist(taxSlabsSettings);
			}*/
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	/*public TaxSlabsSettingsDO update(TaxSlabsSettingsDO taxSlabsSettingsDO) {
		try {
			dao.merge(taxSlabsSettingsDO);
		} catch (Exception eException) {
		} finally {
		}
		return taxSlabsSettingsDO;
	}*/
	
	@SuppressWarnings("unchecked")
	public boolean update(TaxSlabsSettingsDO taxSlabsSettings) {
		boolean updateStatus = true;
		try {
			dao.merge(taxSlabsSettings);
			/*List<TaxSlabsSettingsDO> taxSlabList = null;
			taxSlabList =  this.sessionFactory.getCurrentSession().getNamedQuery(TaxSlabsSettingsDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.TODATE, taxSlabsSettings.getEnddate())
					.setParameter(CommonConstants.FROMDATE, taxSlabsSettings.getStartdate())
					.setParameter(CommonConstants.FROMAGE, taxSlabsSettings.getEmpAgeFrom())
					.setParameter(CommonConstants.TOAGE, taxSlabsSettings.getEmpAgeTo())
					.setParameter(CommonConstants.ID, taxSlabsSettings.getTaxSlabId())
					.list();
			if(taxSlabList != null && taxSlabList.size() > 0){
				updateStatus = false;
			}else{
				dao.merge(taxSlabsSettings);
			}*/
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}
	
	/*public List<TaxSlabsSettingsDO> retrieve() {
		List<TaxSlabsSettingsDO> taxSlabsSettingsList = null;
		try {
			taxSlabsSettingsList = dao.retrieve(TaxSlabsSettingsDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return taxSlabsSettingsList;
	}*/
	
	
	@SuppressWarnings("unchecked")
	public List<TaxSlabsSettingsDO> retrieve() {
		List<TaxSlabsSettingsDO> taxSlabsSettingsList = null;
		try {
			taxSlabsSettingsList =  this.sessionFactory.getCurrentSession().getNamedQuery(TaxSlabsSettingsDO.FIND_ALL)
									.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return taxSlabsSettingsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<TaxSlabsSettingsDO> retrieveById(Long Id) {
		List<TaxSlabsSettingsDO> taxSlabsSettingsList = null;
		try {
			taxSlabsSettingsList =  this.sessionFactory.getCurrentSession().getNamedQuery(TaxSlabsSettingsDO.FIND_BY_ID)
									.setParameter(CommonConstants.ID, Id).list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return taxSlabsSettingsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<TaxSlabsSettingsDO> retrieveByAgeDate(Long age, Date currentDate) {
		List<TaxSlabsSettingsDO> taxSlabsSettingsList = null;
		try {
			taxSlabsSettingsList =  this.sessionFactory.getCurrentSession().getNamedQuery(TaxSlabsSettingsDO.FIND_BY_AGE_DATE)
									.setParameter(CommonConstants.AGE, age)
									.setParameter(CommonConstants.DATE, currentDate)
									.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
									.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return taxSlabsSettingsList;
	}
	
	/*@SuppressWarnings("unchecked")
	public List<TaxSlabsSettingsDO> retrieveBytaxPayee(Long Id, String startDate, String endDate) {
		List<TaxSlabsSettingsDO> taxSlabsSettingsList = null;
		try {
			taxSlabsSettingsList = this.sessionFactory.getCurrentSession().getNamedQuery(TaxSlabsSettingsDO.FIND_BY_TAXPAYEEID)
									.setParameter(CommonConstants.ID, Id)
									.setParameter(CommonConstants.STARTDATE, startDate)
									.setParameter(CommonConstants.ENDDATE, endDate)
									.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
									.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return taxSlabsSettingsList;
	}*/
	
	/*public List<TaxSlabsSettingsDO> retrieveActive() {
		List<TaxSlabsSettingsDO> taxSlabsSettingsList = null;
		try {
			taxSlabsSettingsList =  dao.retrieveActive(CommonConstants.ACTIVE, TaxSlabsSettingsDO.FIND_ACTIVE, TaxSlabsSettingsDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return taxSlabsSettingsList;
	}*/
	
	public boolean persistList(List<TaxSlabsSettingsDO> taxSlabsSettingsList){
		try {
			for (TaxSlabsSettingsDO taxSlabsSettings : taxSlabsSettingsList) {
				dao.persist(taxSlabsSettings);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}
}
