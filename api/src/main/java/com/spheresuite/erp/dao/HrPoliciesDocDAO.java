package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.HrPoliciesDocDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class HrPoliciesDocDAO {
	static Logger logger = Logger.getLogger(HrPoliciesDocDAO.class.getName());
	@Autowired
	private GenericDAOImpl<HrPoliciesDocDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public boolean delete(Long id)  {
		boolean deleteStatus = true;
		try {
			org.hibernate.Query query = this.sessionFactory.getCurrentSession().createQuery("delete from HrPoliciesDocDO e where e.hrPoliceId=:id")
					.setParameter(CommonConstants.ID, id);
			query.executeUpdate();
			
		} catch (Exception eException) {
			deleteStatus = false;
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return deleteStatus;
	}
	
	
	public HrPoliciesDocDO persist(HrPoliciesDocDO hrpoliciesdoc)  {
		try {
			genericObject.persist(hrpoliciesdoc);
		} catch (Exception eException) {
		} finally {
		}
		return hrpoliciesdoc;
	}
	
	
	public List<HrPoliciesDocDO> retrieve()  {
		List<HrPoliciesDocDO> OpportunitiesDocList = null;
		try {
			OpportunitiesDocList = genericObject.retrieve(HrPoliciesDocDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return OpportunitiesDocList;
	}
	
	@SuppressWarnings("unchecked")
	public List<HrPoliciesDocDO> retrieveByPoliciesId(Long id)  {
		List<HrPoliciesDocDO> OpportunitiesDocList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(HrPoliciesDocDO.FIND_BY_POLICIE_ID)
			.setParameter(CommonConstants.ID, id)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return OpportunitiesDocList;
	}
	
	public HrPoliciesDocDO update(HrPoliciesDocDO hrpoliciesdoc)  {
		try {
			genericObject.merge(hrpoliciesdoc);
		} catch (Exception eException) {
		} finally {
		}
		return hrpoliciesdoc;
	}

}
