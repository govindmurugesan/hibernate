package com.spheresuite.erp.dao;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.EmployeeExpenseDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class EmployeeExpenseDAO {
	static Logger logger = Logger.getLogger(EmployeeExpenseDAO.class.getName());
	@Autowired
	private GenericDAOImpl<EmployeeExpenseDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public EmployeeExpenseDO persist(EmployeeExpenseDO employeeExpenseDO) {
		try {
			genericObject.persist(employeeExpenseDO);
		} catch (Exception eException) {
		} finally {
		}
		return employeeExpenseDO;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeExpenseDO> retrieveByEmpId(String empId) {
		List<EmployeeExpenseDO> employeeExpenseList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeExpenseDO.FIND_BY_EMPID)
					.setParameter(CommonConstants.EMPID, empId)
					.list(); 
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeExpenseList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeExpenseDO> retrieveById(Long id) {
		List<EmployeeExpenseDO> employeeExpenseList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeExpenseDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeExpenseList;
	}
	
	public EmployeeExpenseDO update(EmployeeExpenseDO employeeExpenseList) {
		try {
			genericObject.merge(employeeExpenseList);
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return employeeExpenseList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeExpenseDO> retrieve(){
		List<EmployeeExpenseDO> expenseList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeExpenseDO.FIND_ALL)
					.list();
		} catch (Exception eException) {
		} finally {
		}
		return expenseList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeExpenseDO> retrieveByReportingId(String empId) {
		List<EmployeeExpenseDO> expenseList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeExpenseDO.FIND_BY_REPORTINGID)
					.setParameter(CommonConstants.ID, empId)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return expenseList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeExpenseDO> retrieveByMonthAndEmpId(String empId, Date fromDate, Date ToDate, String status)  {
		List<EmployeeExpenseDO> requestList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeExpenseDO.FIND_BY_EMPID_MONTH)
			.setParameter(CommonConstants.FROMDATE, fromDate )
			.setParameter(CommonConstants.TODATE, ToDate)
			.setParameter(CommonConstants.ID, empId)
			.setParameter(CommonConstants.STATUS, status)
			.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return requestList;
	}
	
}
