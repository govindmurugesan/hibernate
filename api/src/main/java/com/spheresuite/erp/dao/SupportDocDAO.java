package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.SupportDocDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class SupportDocDAO {
	static Logger logger = Logger.getLogger(SupportDocDAO.class.getName());

	@Autowired
	private GenericDAOImpl<SupportDocDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public boolean delete(Long id) {
		boolean deleteStatus = true;
		try {
			this.sessionFactory.getCurrentSession().createQuery("delete from SupportDocDO e where e.support.supportId=:id")
										.setParameter(CommonConstants.ID, id)
										.executeUpdate();
		} catch (Exception eException) {
			deleteStatus = false;
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return deleteStatus;
	}
	
	
	public SupportDocDO persist(SupportDocDO supportDocDO) {
		try {
			if (supportDocDO != null) {
				genericObject.persist(supportDocDO);
			}
		} catch (Exception eException) {
			//throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			//SessionManager.closeEntityManager(em);
		}
		return supportDocDO;
	}
	
	
	public List<SupportDocDO> retrieve() {
		List<SupportDocDO> OpportunitiesDocList = null;
		try {
			OpportunitiesDocList = genericObject.retrieve(SupportDocDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return OpportunitiesDocList;
	}
	
	@SuppressWarnings("unchecked")
	public List<SupportDocDO> retrieveBySupportId(Long id) {
		List<SupportDocDO> OpportunitiesDocList = null;
		try {
			OpportunitiesDocList = this.sessionFactory.getCurrentSession().getNamedQuery(SupportDocDO.FIND_BY_SUPPORT_ID)
										.setParameter(CommonConstants.ID, id)
										.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return OpportunitiesDocList;
	}
	
	public SupportDocDO update(SupportDocDO supportDocDO) {
		try {
			genericObject.merge(supportDocDO);
		} catch (Exception eException) {
		} finally {
		}
		return supportDocDO;
	}

}
