package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.TimesheetTypeDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class TimesheetTypeDAO {
	static Logger logger = Logger.getLogger(TimesheetTypeDAO.class.getName());

	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private GenericDAOImpl<TimesheetTypeDO> dao;
	
	@SuppressWarnings("unchecked")
	public boolean persist(TimesheetTypeDO timesheetTypeDO) {
		List<TimesheetTypeDO> timesheetTypeList = null;
		boolean persistStatus = true;
		try {
			timesheetTypeList = this.sessionFactory.getCurrentSession().getNamedQuery(TimesheetTypeDO.FIND_BY_NAME)
			.setParameter(CommonConstants.NAME, timesheetTypeDO.getName())
			.list();
			if(timesheetTypeList != null && timesheetTypeList.size() > 0){
				persistStatus = false;
			}else{
				dao.persist(timesheetTypeDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	public List<TimesheetTypeDO> retrieve() {
		List<TimesheetTypeDO> timesheetTypeList = null;
		try {
			timesheetTypeList = dao.retrieve(TimesheetTypeDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return timesheetTypeList;
	}
	
	@SuppressWarnings("unchecked")
	public List<TimesheetTypeDO> retrieveById(Long Id) {
		List<TimesheetTypeDO> timesheetTypeList = null;
		try {
			timesheetTypeList = this.sessionFactory.getCurrentSession().getNamedQuery(TimesheetTypeDO.FIND_BY_ID)
									.setParameter(CommonConstants.ID, Id).list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return timesheetTypeList;
	}
	
	@SuppressWarnings("unchecked")
	public boolean update(TimesheetTypeDO timesheetTypeDO) {
		boolean updateStatus = true;
		try {
			List<TimesheetTypeDO> timesheetTypeList = null;
			timesheetTypeList = this.sessionFactory.getCurrentSession().getNamedQuery(TimesheetTypeDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, timesheetTypeDO.getName())
					.setParameter(CommonConstants.ID, timesheetTypeDO.getTimesheetTypeId())
					.list();
			if(timesheetTypeList != null && timesheetTypeList.size() > 0){
				updateStatus = false;
			}else{
				dao.merge(timesheetTypeDO);
			}
			
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}
	
	public boolean persistList(List<TimesheetTypeDO> timesheetTypeList){
		try {
			for (TimesheetTypeDO timesheetType : timesheetTypeList) {
				dao.persist(timesheetType);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}

}
