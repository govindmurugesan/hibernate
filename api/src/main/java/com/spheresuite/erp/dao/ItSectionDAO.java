package com.spheresuite.erp.dao;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.ItSectionDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class ItSectionDAO {
	static Logger logger = Logger.getLogger(ItSectionDAO.class.getName());
	@Autowired
	private GenericDAOImpl<ItSectionDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	
	@SuppressWarnings({ "unchecked", "static-access" })
	public boolean persist(ItSectionDO ItSectionDO)  {
		boolean persistStatus = true;
		try {
			//FIND_BY_DUPLICATE_PERSIST
			List<ItSectionDO> itSectionList = null;
			itSectionList = this.sessionFactory.getCurrentSession().getNamedQuery(ItSectionDO.FIND_BY_DUPLICATE_PERSIST)
					.setParameter(CommonConstants.FROMDATE, ItSectionDO.getFromYear())
					.setParameter(CommonConstants.TODATE, ItSectionDO.getToYear())
					.setParameter(CommonConstants.SECTION, ItSectionDO.getSection())
					.list();
			if(itSectionList != null && itSectionList.size() > 0){
				persistStatus = false;
			}else{
				genericObject.persist(ItSectionDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	
	public List<ItSectionDO> retrieve()  {
		List<ItSectionDO> itSectionList = null;
		try {
			itSectionList = genericObject.retrieve(ItSectionDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return itSectionList;
	}
	
	public List<ItSectionDO> retrieveActive()  {
		List<ItSectionDO> itSectionList = null;
		try {
			return genericObject.retrieveActive(CommonConstants.ACTIVE, 
					ItSectionDO.FIND_BY_STATUS, ItSectionDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return itSectionList;
	}
	
	@SuppressWarnings("unchecked")
	public List<ItSectionDO> retrieveById(Long id)  {
		List<ItSectionDO> itSectionList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(ItSectionDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return itSectionList;
	}
	
	
	
	@SuppressWarnings("unchecked")
	public List<ItSectionDO> retrieveBySectionName(String name)  {
		List<ItSectionDO> itSectionList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(ItSectionDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, name)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return itSectionList;
	}
	
	
	/*@SuppressWarnings("unchecked")
	public List<ItSectionDO> retrieveByDate(String fromDate, String toDate)  {
		List<ItSectionDO> itSavingsSettingsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(ItSectionDO.FIND_BY_DATE)
			.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
			.setParameter(CommonConstants.FROMDATE, fromDate)
			.setParameter(CommonConstants.TODATE, toDate)
			.list();
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return itSavingsSettingsList;
	}*/
	
	@SuppressWarnings("unchecked")
	public List<ItSectionDO> retrieveByCurrentDate(Date date)  {
		List<ItSectionDO> itSavingsSettingsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(ItSectionDO.FIND_BY_CURRENTDATE)
			.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
			.setParameter(CommonConstants.DATE, date)
			.list();
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return itSavingsSettingsList;
	}
	
	@SuppressWarnings({ "unchecked", "static-access" })
	public boolean update(ItSectionDO ItSectionDO)  {
		try {
			List<ItSectionDO> itSectionList = null;
			itSectionList = this.sessionFactory.getCurrentSession().getNamedQuery(ItSectionDO.FIND_BY_DUPLICATE_UPDATE)
					.setParameter(CommonConstants.FROMDATE, ItSectionDO.getFromYear())
					.setParameter(CommonConstants.TODATE, ItSectionDO.getToYear())
					.setParameter(CommonConstants.SECTION, ItSectionDO.getSection())
					.setParameter(CommonConstants.ID, ItSectionDO.getItSectionId())
					.list();
			if(itSectionList != null && itSectionList.size() > 0){
				return false;
			}else{
				genericObject.merge(ItSectionDO);
				return true;
			}
			
		} catch (Exception eException) {
			return true;
		} finally {
		}
	}
	
	public boolean persistList(List<ItSectionDO> itSectionList){
		try {
			for (ItSectionDO itSection : itSectionList) {
				genericObject.persist(itSection);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}

}
