package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.DepartmentDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class DepartmentDAO {
	static Logger logger = Logger.getLogger(DepartmentDAO.class.getName());
	
	@Autowired
	private GenericDAOImpl<DepartmentDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(DepartmentDO departmentDO) {
		boolean persistStatus = true;
		try {
			List<DepartmentDO> departmentDOList = null;
			departmentDOList = this.sessionFactory.getCurrentSession().getNamedQuery(DepartmentDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, departmentDO.getName())
					.list();
					if(departmentDOList != null && departmentDOList.size() > 0){
						persistStatus = false;
					}else{
						genericObject.persist(departmentDO);
					}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	public List<DepartmentDO> retrieve() {
		List<DepartmentDO> departmentList = null;
		try {
			departmentList = genericObject.retrieve(DepartmentDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return departmentList;
	}
	
	public List<DepartmentDO> retrieveActive() {
		List<DepartmentDO> departmentList = null;
		try {
			return genericObject.retrieveActive(CommonConstants.ACTIVE, 
					DepartmentDO.FIND_BY_STATUS, DepartmentDO.class);
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return departmentList;
	}
	
	@SuppressWarnings("unchecked")
	public List<DepartmentDO> retrieveById(Long Id) {
		List<DepartmentDO> departmentList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(DepartmentDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, Id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return departmentList;
	}
	
	@SuppressWarnings("unchecked")
	public List<DepartmentDO> retrieveByName(String name) {
		List<DepartmentDO> departmentList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(DepartmentDO.FIND_BY_NAME_ALL)
					.setParameter(CommonConstants.NAME, name)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return departmentList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Long> retrieveisManager(String isManager) {
		List<Long> departmentList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(DepartmentDO.FIND_IS_MANAGER)
					.setParameter("isManager", isManager)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list(); 
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return departmentList;
	}
	
	@SuppressWarnings("unchecked")
	public boolean update(DepartmentDO departmentDO) {
		boolean updateStatus = true;
		try {
			List<DepartmentDO> departmentDOList = null;
			departmentDOList = this.sessionFactory.getCurrentSession().getNamedQuery(DepartmentDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, departmentDO.getName())
					.setParameter(CommonConstants.ID, departmentDO.getDepartmentId())
					.list();
			if(departmentDOList != null && departmentDOList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(departmentDO);
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return updateStatus;
	}
	
	public boolean persistList(List<DepartmentDO> departmentList){
		try {
			for (DepartmentDO department : departmentList) {
				genericObject.persist(department);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}

}
