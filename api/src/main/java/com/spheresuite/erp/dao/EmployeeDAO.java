package com.spheresuite.erp.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class EmployeeDAO {
	static Logger logger = Logger.getLogger(EmployeeDAO.class.getName());
	@Autowired
	private GenericDAOImpl<EmployeeDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	
	
	@SuppressWarnings("unchecked")
	public boolean persist(EmployeeDO employeeDO) {
		boolean persistStatus = true;
		try {
			List<EmployeeDO> empList = null;
			empList = this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeDO.FIND_BY_EMAIL_EMPID)
					//.setParameter(CommonConstants.EMAIL, employeeDO.getCompanyemail())
						.setParameter(CommonConstants.ID, employeeDO.getEmpId())
					.list();
					if(empList != null && empList.size() > 0){
						persistStatus = false;
					}else{
						genericObject.persist(employeeDO);
					}
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
		}
		return persistStatus;
	}
	
	
	/*public EmployeeDO persist(EmployeeDO employeeDO) {
		try {
			genericObject.persist(employeeDO);
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
		}
		return employeeDO;
	}*/
	
/*	@SuppressWarnings("unchecked")
	public List<EmployeeDO> retrieve() {
		List<EmployeeDO> userList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeDO.FIND_ALL)
					.list();
		} catch (Exception eException) {
		} finally {
		}
		return userList;
	}*/
	
	public List<EmployeeDO> retrieve() {
		List<EmployeeDO> empList = null;
		try {
			empList = genericObject.retrieve(EmployeeDO.class);
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return empList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Object []> retrieveAll() {
		List<Object []> empList = null;
		try {
			empList = this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeDO.FIND_ALL_SPECIFICFIELD).list();
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return empList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Object []> retrieveNameId() {
		List<Object []> empList = null;
		try {
			empList = this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeDO.FIND_ONLY_EMPNAME_ID)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE).list();
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return empList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Object []> retrieveByUnit(Long unitId) {
		List<Object []> empList = null;
		try {
			empList = this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeDO.FIND_BY_UNIT)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.setParameter(CommonConstants.ID, unitId).list();
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return empList;
	}
	
	
	
	
	@SuppressWarnings("unchecked")
	public List<EmployeeDO> retrieveForId() {
		List<EmployeeDO> employeeList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeDO.FIND_LAST_ID)
					.list();
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return employeeList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeDO> retrieveActive() {
		List<EmployeeDO> userList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeDO.FIND_ACTIVE)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return userList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeDO> retrieveActiveNoCtc(List<String> empIds) {
		List<EmployeeDO> userList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeDO.FIND_EMP_NOCTC)
			.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
			.setParameterList(CommonConstants.ID, empIds)
			.list();
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return userList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeDO> isEmpActive(String empid) {
		List<EmployeeDO> userList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeDO.FIND_IS_ACTIVE)
			.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
			.setParameter(CommonConstants.ID, empid)
			.list();
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return userList;
	}
	/*
	@SuppressWarnings("unchecked")
	public List<EmployeeDO> retrieveById(String id) {
		List<EmployeeDO> user = null;
		try {
			user = this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeDO.FIND_EMPID)
					.setParameter(CommonConstants.ID, id).list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return user;
	}*/
	
	@SuppressWarnings("unchecked")
	public List<String> retrieveByDeptIds(List<Long> deptIds) {
		List<String> employeeList = null;
		try {
			return (List<String>) this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeDO.FIND_BY_DEPT)
			.setParameterList(CommonConstants.ID, deptIds)
			.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
			.list();
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeDO> retrieveByEmail(String email){
		List<EmployeeDO> user = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeDO.FIND_BY_EMAIL)
			.setParameter(CommonConstants.EMAIL, email)
			.list();
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return user;
	}
	
	/*public Boolean update(EmployeeDO employeeDO) {
		try {
			genericObject.merge(employeeDO);
		} catch (Exception eException) {
		} finally {
		}
		return employeeDO;
	}
	*/
	
	public boolean update(EmployeeDO employeeDO) {
		boolean updateStatus = true;
		try {
			genericObject.merge(employeeDO);
			/*List<EmployeeDO> empList = null;
			empList = this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeDO.FIND_DUPLICATE_UPDATE)
					.setParameter(CommonConstants.EMAIL, employeeDO.getCompanyemail())
					.setParameter(CommonConstants.ID, employeeDO.getEmpId())
					.list();
			if(empList != null && empList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(employeeDO);
			}*/
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return updateStatus;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<EmployeeDO> retrieveByEmpId(String id) {
		List<EmployeeDO> user = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeDO.FIND_EMPID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return user;
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> retrieveByEmpIdOnlyNames(String id) {
		List<Object[]> user = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeDO.FIND_EMPID_ONLYNAMES)
					.setParameter(CommonConstants.ID, id).list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return user;
	}
	
	/*@SuppressWarnings("unchecked")
	public List<EmployeeDO> retrieveEmpName(String fname,String mname,String lname) {
		List<EmployeeDO> user = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeDO.FIND_EMPFUllNAME)
					.setParameter(CommonConstants.FIRSTNAME, fname)
			.setParameter(CommonConstants.MIDDLENAME, mname)
			.setParameter(CommonConstants.LASTNAME, lname)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return user;
	}*/
	
	
	
	/*
	@SuppressWarnings("unchecked")
	public List<EmployeeDO> retrieveEmpId(String id) {
		List<EmployeeDO> user = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeDO.FIND_EMPID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return user;
	}
	*/
	
	
	@SuppressWarnings("unchecked")
	public String checkDuplicateUpdate(String empId, String companyEmail, Long Id) {
		List<EmployeeDO> employeeList = null;
		try {
			if(companyEmail != null && Id != null && empId != null){
				List<Long> ids = new ArrayList<Long>();
				ids.add(Id);
				employeeList =  this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeDO.FIND_DUPLICATE_UPDATE)
						.setParameter(CommonConstants.EMAIL, companyEmail)
						.setParameter(CommonConstants.EMPID, empId)
						.setParameter(CommonConstants.ID, ids)
						.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
						.list();
				for (EmployeeDO employeeDO : employeeList) {
					if(employeeDO.getCompanyemail() != null && employeeDO.getCompanyemail().equalsIgnoreCase(companyEmail)){
						return CommonConstants.COMPANYEMAIL;
					}else{
						return CommonConstants.EMPID;
					}
				}
			}else if(companyEmail != null && Id != null){
				List<Long> ids = new ArrayList<Long>();
				ids.add(Id);
				employeeList =  this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeDO.FIND_DUPLICATE_UPDATE_NOEMP)
						.setParameter(CommonConstants.EMAIL, companyEmail)
						.setParameter(CommonConstants.ID, ids)
						.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
						.list();
				for (EmployeeDO employeeDO : employeeList) {
					if(employeeDO.getCompanyemail() != null && employeeDO.getCompanyemail().equalsIgnoreCase(companyEmail)){
						return CommonConstants.COMPANYEMAIL;
					}
				}
			}
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return CommonConstants.NO_DUPLICATE;
	}
	
	@SuppressWarnings("unchecked")
	public String checkDuplicate(String empId, String companyEmail){
		List<EmployeeDO> employeeList = null;
		try {
			if(companyEmail != null && empId != null){
				employeeList =  this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeDO.FIND_DUPLICATE_UPDATE_NOEMP)
						.setParameter(CommonConstants.EMAIL, companyEmail)
						.setParameter(CommonConstants.EMPID, empId)
						.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
						.list();
				for (EmployeeDO employeeDO : employeeList) {
					if(employeeDO.getCompanyemail() != null && employeeDO.getCompanyemail().equalsIgnoreCase(companyEmail)){
						return CommonConstants.COMPANYEMAIL;
					}else{
						return CommonConstants.EMPID;
					}
				}
			}else if(companyEmail != null){
				
				employeeList =  this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeDO.FIND_DUPLICATE_UPDATE_NOEMP)
						.setParameter(CommonConstants.EMAIL, companyEmail)
						.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
						.list();
				for (EmployeeDO employeeDO : employeeList) {
					if(employeeDO.getCompanyemail() != null && employeeDO.getCompanyemail().equalsIgnoreCase(companyEmail)){
						return CommonConstants.COMPANYEMAIL;
					}
				}
			}
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return CommonConstants.NO_DUPLICATE;
	}
	
	public boolean persistList(List<EmployeeDO> empDO){
		try {
			for (EmployeeDO empDO2 : empDO) {
				genericObject.persist(empDO2);
			}
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
		}
		return true;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeDO> retrieveReportTo(String empId) {
		List<EmployeeDO> employeeList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeDO.FIND_BY_REPORT_TO)
					.setParameter(CommonConstants.REPORTTO, empId)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)					
					.list();
		} catch (Exception eException) {
		} finally {
		}
		return employeeList;
	}

}
