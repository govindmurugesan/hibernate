package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.crm.dao.LeadTypeDAO;
import com.spheresuite.erp.domainobject.GSTTaxSlabDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class GSTTaxSlabDAO {
	static Logger logger = Logger.getLogger(LeadTypeDAO.class.getName());
	@Autowired
	private GenericDAOImpl<GSTTaxSlabDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(GSTTaxSlabDO taxDetail)  {
		boolean persistStatus = true;
		try {
			List<GSTTaxSlabDO> gstTaxSlabList = null;
			try {
				gstTaxSlabList = this.sessionFactory.getCurrentSession().getNamedQuery(GSTTaxSlabDO.FIND_BY_NAME)
				.setParameter(CommonConstants.NAME, taxDetail.getPercentage())
				.list();
				if(gstTaxSlabList != null && gstTaxSlabList.size() > 0){
					persistStatus = false;
				}else{
					genericObject.persist(taxDetail);
				}
		} catch (Exception eException) {
		}
		} finally {
		}
		return persistStatus;
	}
	
	public List<GSTTaxSlabDO> retrieve()  {
		List<GSTTaxSlabDO> taxList = null;
		try {
			taxList = genericObject.retrieve(GSTTaxSlabDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return taxList;
	}
	
	public List<GSTTaxSlabDO> retrieveActive()  {
		List<GSTTaxSlabDO> taxList = null;
		try {
			return genericObject.retrieveActive(CommonConstants.ACTIVE, 
					GSTTaxSlabDO.FIND_BY_STATUS, GSTTaxSlabDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return taxList;
	}
	
	@SuppressWarnings("unchecked")
	public List<GSTTaxSlabDO> retrieveById(Long Id)  {
		List<GSTTaxSlabDO> taxList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(GSTTaxSlabDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, Id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return taxList;
	}
	
	@SuppressWarnings("unchecked")
	public boolean update(GSTTaxSlabDO taxList)  {
		boolean updateStatus = true;
		try {
			List<GSTTaxSlabDO> gstTaxSlabList = null;
			gstTaxSlabList = this.sessionFactory.getCurrentSession().getNamedQuery(GSTTaxSlabDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, taxList.getPercentage())
					.setParameter(CommonConstants.ID, taxList.getGstTaxSlabId())
					.list();
			if(gstTaxSlabList != null && gstTaxSlabList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(taxList);
			}
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}
	
	public boolean persistList(List<GSTTaxSlabDO> gstTaxSlabList){
		try {
			for (GSTTaxSlabDO gstTaxSlab : gstTaxSlabList) {
				genericObject.persist(gstTaxSlab);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}
}
