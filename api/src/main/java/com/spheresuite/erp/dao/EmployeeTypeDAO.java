package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.EmployeeTypeDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class EmployeeTypeDAO {
	static Logger logger = Logger.getLogger(EmployeeTypeDAO.class.getName());
	@Autowired
	private GenericDAOImpl<EmployeeTypeDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(EmployeeTypeDO employeeTypeDO)  {
		boolean persistStatus = true;
		try {
			List<EmployeeTypeDO> employeeTypeDOList = null;
			employeeTypeDOList = this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeTypeDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, employeeTypeDO.getName())
					.list();
					if(employeeTypeDOList != null && employeeTypeDOList.size() > 0){
						persistStatus = false;
					}else{
						genericObject.persist(employeeTypeDO);
					}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	public List<EmployeeTypeDO> retrieve()  {
		List<EmployeeTypeDO> employeeTypeList = null;
		try {
			employeeTypeList = genericObject.retrieve(EmployeeTypeDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return employeeTypeList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeTypeDO> retrieveActive()  {
		List<EmployeeTypeDO> employeeTypeList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeTypeDO.FIND_BY_STATUS)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeTypeList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeTypeDO> retrieveById(Long id)  {
		List<EmployeeTypeDO> employeeTypeList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeTypeDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeTypeList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeTypeDO> retrieveByName(String name)  {
		List<EmployeeTypeDO> employeeTypeList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeTypeDO.FIND_BY_NAME_ALL)
					.setParameter(CommonConstants.NAME, name)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeTypeList;
	}
	
	@SuppressWarnings("unchecked")
	public boolean update(EmployeeTypeDO employeeTypeDO)  {
		boolean updateStatus = true;
		try {
			List<EmployeeTypeDO> employeeTypeDOList = null;
			employeeTypeDOList = this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeTypeDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, employeeTypeDO.getName())
					.setParameter(CommonConstants.ID, employeeTypeDO.getEmpTypeId())
					.list();
			if(employeeTypeDOList != null && employeeTypeDOList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(employeeTypeDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}
	
	public boolean persistList(List<EmployeeTypeDO> employeeTypeList){
		try {
			for (EmployeeTypeDO employeeType : employeeTypeList) {
				genericObject.persist(employeeType);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}
}
