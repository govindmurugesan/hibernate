package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.AttachmentsDO;
@Repository
public class AttachmentsDAO {
	static Logger logger = Logger.getLogger(AttachmentsDAO.class.getName());
	
	@Autowired
	private GenericDAOImpl<AttachmentsDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public boolean delete() {
		try {
			org.hibernate.Query query = this.sessionFactory.getCurrentSession().createQuery("delete from AttachmentsDO e");
			query.executeUpdate();
			//genericObject.remove(new AttachmentsDO());
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return true;
	}
	
	
	public AttachmentsDO persist(AttachmentsDO attachmentsDO){
		try {
			genericObject.persist(attachmentsDO);
		} catch (Exception eException) {
		} finally {
		}
		return attachmentsDO;
	}
	
	
	public List<AttachmentsDO> retrieve() {
		List<AttachmentsDO> attachmentsList = null;
		try {
			attachmentsList = genericObject.retrieve(AttachmentsDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return attachmentsList;
	}
}
