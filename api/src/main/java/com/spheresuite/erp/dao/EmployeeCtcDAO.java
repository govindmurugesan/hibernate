package com.spheresuite.erp.dao;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.EmployeeCtcDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class EmployeeCtcDAO {
	static Logger logger = Logger.getLogger(EmployeeCtcDAO.class.getName());
	@Autowired
	private GenericDAOImpl<EmployeeCtcDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public EmployeeCtcDO persist(EmployeeCtcDO EmloyeeCompensationDO) {
		try {
			genericObject.persist(EmloyeeCompensationDO);
		} catch (Exception eException) {
		} finally {
		}
		return EmloyeeCompensationDO;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<EmployeeCtcDO> retrieve() {
		List<EmployeeCtcDO> empCompensationList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeCtcDO.FIND_BY_STATUS)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return empCompensationList;
	}
	
	public List<EmployeeCtcDO> retrieveAll() {
		List<EmployeeCtcDO> empList = null;
		try {
			empList = genericObject.retrieve(EmployeeCtcDO.class);
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return empList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<EmployeeCtcDO> retrieveActive(String empId) {
		List<EmployeeCtcDO> empCompensationList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeCtcDO.FIND_BY_STATUS_EMP_ID)
				.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
				.setParameter(CommonConstants.EMPID, empId)
				.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return empCompensationList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<EmployeeCtcDO> retrieveByEmpCtc(Long ctc, String empid) {
		List<EmployeeCtcDO> empCompensationList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeCtcDO.FIND_BY_EMPLOYEE_CTC)
				.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
				.setParameter(CommonConstants.EMPID, empid)
				.setParameter(CommonConstants.EMPCTC, ctc)
				.list();
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return empCompensationList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeCtcDO> retrieveEmpIdeWithDate(String empId, Date date) {
		List<EmployeeCtcDO> empCompensationList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeCtcDO.FIND_BY_EMPID_DATE)
			.setParameter(CommonConstants.DATE, date)
			.setParameter(CommonConstants.EMPID, empId)
			.list();
				
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return empCompensationList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeCtcDO> retrieveById(Long id){
		List<EmployeeCtcDO> empCtcList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeCtcDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return empCtcList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Long> retrieveByBatchId(Long id){
		List<Long> empCtcList = null;
		try {
				return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeCtcDO.FIND_BY_BATCH_ID)
				.setParameter(CommonConstants.ID, id)
				.list();
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return empCtcList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeCtcDO> retrieveByEmpId(String empId) {
		List<EmployeeCtcDO> empCompensationList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeCtcDO.FIND_BY_EMP_ID)
			.setParameter(CommonConstants.EMPID, empId)
			.list();
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return empCompensationList;
	}
	
	public EmployeeCtcDO update(EmployeeCtcDO EmloyeeCompensationDO)	 {
		try {
			genericObject.merge(EmloyeeCompensationDO);
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return EmloyeeCompensationDO;
	}
	
	
	public boolean persistList(List<EmployeeCtcDO> EmloyeeCompensationDO){
		try {
			for (EmployeeCtcDO EmloyeeCompensationDO2 : EmloyeeCompensationDO) {
				genericObject.persist(EmloyeeCompensationDO2);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<EmployeeCtcDO> retrieveBatchAndMonth(Long id, Date month) {
		List<EmployeeCtcDO> empCompensationList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeCtcDO.FIND_BY_BATCH_MONTH)
			.setParameter(CommonConstants.DATE, month)
			.setParameter(CommonConstants.ID, id)
			.list();
				
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return empCompensationList;
	}
	
	

	@SuppressWarnings("unchecked")
	public List<EmployeeCtcDO> retrieveWeeklyBatchWithDate(Long id, Date month) {
		List<EmployeeCtcDO> empCompensationList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeCtcDO.FIND_WEEKLY_BATCH_MONTH)
			.setParameter(CommonConstants.DATE, month)
			.setParameter(CommonConstants.ID, id)
			.setParameter(CommonConstants.TYPE, CommonConstants.WEEKLY)
			.list();
				
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return empCompensationList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeCtcDO> retrieveDailyBatchWithDate(Long id, Date date) {
		List<EmployeeCtcDO> empCompensationList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeCtcDO.FIND_WEEKLY_BATCH_MONTH)
			.setParameter(CommonConstants.DATE, date)
			.setParameter(CommonConstants.ID, id)
			.setParameter(CommonConstants.TYPE, CommonConstants.DAILY)
			.list();
				
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return empCompensationList;
	}

}
