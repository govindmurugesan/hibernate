package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.GeneralDeductionDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class GeneralDeductionDAO {
	static Logger logger = Logger.getLogger(GeneralDeductionDAO.class.getName());
	@Autowired
	private GenericDAOImpl<GeneralDeductionDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public GeneralDeductionDO persist(GeneralDeductionDO loanPaymentDO) {
		try {
			genericObject.persist(loanPaymentDO);
		} catch (Exception eException) {
		} finally {
		}
		return loanPaymentDO;
	}
	
	@SuppressWarnings("unchecked")
	public List<GeneralDeductionDO> retrieveActiveByEmpId(String empId) {
		List<GeneralDeductionDO> employeeloanList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(GeneralDeductionDO.FIND_ACTIVE_BY_EMPID)
					.setParameter(CommonConstants.EMPID, empId)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVESTRING)
					.list(); 
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeloanList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<GeneralDeductionDO> retrieve() {
		List<GeneralDeductionDO> loanPaymentList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(GeneralDeductionDO.FIND_ALL)
					.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
		}
		return loanPaymentList;
	}
	
	@SuppressWarnings("unchecked")
	public List<GeneralDeductionDO> retrieveById(Long id) {
		List<GeneralDeductionDO> loanPaymentList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(GeneralDeductionDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return loanPaymentList;
	}
	
	public GeneralDeductionDO update(GeneralDeductionDO loanPaymentDO) {
		try {
			genericObject.merge(loanPaymentDO);
		} catch (Exception eException) {
		} finally {
		}
		return loanPaymentDO;
	}
	
	/*public boolean delete(Long id)  {
		try {
			org.hibernate.Query query = this.sessionFactory.getCurrentSession().createQuery("delete from LoanPaymentDO e where e.empBonusId=:id")
					.setParameter(CommonConstants.ID, id);
			query.executeUpdate();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return true;
	}*/
	

}
