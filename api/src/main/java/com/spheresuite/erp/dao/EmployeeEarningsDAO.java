package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.EmployeeEarningsDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class EmployeeEarningsDAO {
	static Logger logger = Logger.getLogger(EmployeeEarningsDAO.class.getName());
	@Autowired
	private GenericDAOImpl<EmployeeEarningsDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public List<EmployeeEarningsDO> persist(List<EmployeeEarningsDO> employeeEarningsDO){
		try {
				for (EmployeeEarningsDO employeeEarningsDO2 : employeeEarningsDO) {
					genericObject.persist(employeeEarningsDO2);
				}
		} catch (Exception eException) {
		} finally {
		}
		return employeeEarningsDO;
	}
	
	
	public List<EmployeeEarningsDO> retrieve(){
		List<EmployeeEarningsDO> employeeEarningsList = null;
		try {
			employeeEarningsList = genericObject.retrieve(EmployeeEarningsDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return employeeEarningsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeEarningsDO> retrieveByEmpCompensationId(Long id){
		List<EmployeeEarningsDO> employeeEarningsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeEarningsDO.FIND_BY_ID)
			.setParameter(CommonConstants.ID, id)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeEarningsList;
	}
	
	@SuppressWarnings("rawtypes")
	public Long retrieveByEmpCompensationIds(List<Long> ids){
		Long employeeEarningsList = null;
		try {
			List list =  this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeEarningsDO.FIND_BY_IDS)
			.setParameter(CommonConstants.ID, ids)
			.list();
			
			if(list != null && list.size() > 0){
				employeeEarningsList = (Long) list.get(0);
			}
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeEarningsList;
	}
	
	public EmployeeEarningsDO update(EmployeeEarningsDO employeeEarningsDO){
		try {
			genericObject.merge(employeeEarningsDO);
		} catch (Exception eException) {
		} finally {
		}
		return employeeEarningsDO;
	}

}
