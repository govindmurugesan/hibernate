package com.spheresuite.erp.dao;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.ItSlabEffectiveDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class ItSlabEffectiveDAO {
	static Logger logger = Logger.getLogger(ItSlabEffectiveDAO.class.getName());
	@Autowired
	private GenericDAOImpl<ItSlabEffectiveDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	
	@SuppressWarnings({ "unchecked", "static-access" })
	public boolean persist(ItSlabEffectiveDO itSlabEffectiveDO)  {
		boolean persistStatus = true;
		try {
			//FIND_BY_DUPLICATE_PERSIST
			List<ItSlabEffectiveDO> effectiveList = null;
			effectiveList = this.sessionFactory.getCurrentSession().getNamedQuery(ItSlabEffectiveDO.FIND_BY_DUPLICATE_PERSIST)
					.setParameter(CommonConstants.FROM, itSlabEffectiveDO.getEffectivefromDate())
					.setParameter(CommonConstants.TO, itSlabEffectiveDO.getEffectiveToDate())
					.list();
			if(effectiveList != null && effectiveList.size() > 0){
				persistStatus = false;
			}else{
				genericObject.persist(itSlabEffectiveDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	
	public List<ItSlabEffectiveDO> retrieve()  {
		List<ItSlabEffectiveDO> effectiveList = null;
		try {
			effectiveList = genericObject.retrieve(ItSlabEffectiveDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return effectiveList;
	}
	
	/*public List<ItSlabEffectiveDO> retrieveActive()  {
		List<ItSlabEffectiveDO> effectiveList = null;
		try {
			return genericObject.retrieveActive(CommonConstants.ACTIVE, 
					ItSlabEffectiveDO.FIND_BY_STATUS, ItSlabEffectiveDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return effectiveList;
	}*/
	
	@SuppressWarnings("unchecked")
	public List<ItSlabEffectiveDO> retrieveById(Long id)  {
		List<ItSlabEffectiveDO> effectiveList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(ItSlabEffectiveDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return effectiveList;
	}
	
	
	
	@SuppressWarnings("unchecked")
	public List<ItSlabEffectiveDO> retrieveByDate(Date fromDate, Date toDate)  {
		List<ItSlabEffectiveDO> itSlabEffectiveList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(ItSlabEffectiveDO.FIND_BY_DATE)
			.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
			.setParameter(CommonConstants.FROM, fromDate)
			.setParameter(CommonConstants.TO, toDate)
			.list();
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return itSlabEffectiveList;
	}
	
	
	/**/
	
	/*@SuppressWarnings("unchecked")
	public List<ItSlabEffectiveDO> retrieveByCurrentDate(Date date)  {
		List<ItSlabEffectiveDO> itSavingsSettingsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(ItSlabEffectiveDO.FIND_BY_CURRENTDATE)
			.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
			.setParameter(CommonConstants.DATE, date)
			.list();
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return itSavingsSettingsList;
	}*/
	
	@SuppressWarnings({ "unchecked", "static-access" })
	public boolean update(ItSlabEffectiveDO itSlabEffectiveDO)  {
		try {
			List<ItSlabEffectiveDO> effectiveList = null;
			effectiveList = this.sessionFactory.getCurrentSession().getNamedQuery(ItSlabEffectiveDO.FIND_BY_DUPLICATE_UPDATE)
					.setParameter(CommonConstants.FROM, itSlabEffectiveDO.getEffectivefromDate())
					.setParameter(CommonConstants.TO, itSlabEffectiveDO.getEffectiveToDate())
					.setParameter(CommonConstants.ID, itSlabEffectiveDO.getItSlabEffectiveId())
					.list();
			if(effectiveList != null && effectiveList.size() > 0){
				return false;
			}else{
				genericObject.merge(itSlabEffectiveDO);
				return true;
			}
			
		} catch (Exception eException) {
			return true;
		} finally {
		}
	}
	
	

}
