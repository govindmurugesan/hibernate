package com.spheresuite.erp.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.PFGroupDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class PFGroupDAO {
	@Autowired
	private GenericDAOImpl<PFGroupDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(PFGroupDO pfGroupDO){
		List<PFGroupDO> pfList = null;
		boolean persistStatus = true;
		try {
			pfList = this.sessionFactory.getCurrentSession().getNamedQuery(PFGroupDO.FIND_BY_NAME)
			.setParameter(CommonConstants.NAME, pfGroupDO.getName())
			.list();
			if(pfList != null && pfList.size() > 0){
				persistStatus = false;
			}else{
				genericObject.persist(pfGroupDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	public List<PFGroupDO> retrieve(){
		List<PFGroupDO> pfList = null;
		try {
			pfList = genericObject.retrieve(PFGroupDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return pfList;
	}
	
	public List<PFGroupDO> retrieveActive(){
		List<PFGroupDO> pfGroupList = null;
		try {
			return genericObject.retrieveActive(CommonConstants.ACTIVE, PFGroupDO.FIND_BY_STATUS, PFGroupDO.class);
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return pfGroupList;
	}
	
	@SuppressWarnings("unchecked")
	public List<PFGroupDO> retrieveById(Long id) {
		List<PFGroupDO> pfGroupList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(PFGroupDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return pfGroupList;
	}
	
	@SuppressWarnings("unchecked")
	public List<PFGroupDO> retrieveByName(String name) {
		List<PFGroupDO> pfGroupList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(PFGroupDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, name)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return pfGroupList;
	}
	
	public boolean persistList(List<PFGroupDO> pfGroupList){
		try {
			for (PFGroupDO pfGroup : pfGroupList) {
				genericObject.persist(pfGroup);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}
	
	public boolean update(PFGroupDO pfGroupDO){
		boolean updateStatus = true;
		try {
			genericObject.merge(pfGroupDO);
			/*List<PayrollGroupDO> payrollGroupList = null;
			payrollGroupList = this.sessionFactory.getCurrentSession().getNamedQuery(PayrollGroupDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, payrollGroupDO.getName())
					.setParameter(CommonConstants.ID, payrollGroupDO.getPayrollGroupId())
					.list();
			if(payrollGroupList != null && payrollGroupList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(payrollGroupDO);
			}*/
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}

}
