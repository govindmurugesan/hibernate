package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.CompanyDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class CompanyDAO {
	static Logger logger = Logger.getLogger(CompanyDAO.class.getName());
	@Autowired
	private GenericDAOImpl<CompanyDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public CompanyDO persist(CompanyDO companyDO) {
		try {
			genericObject.persist(companyDO);
		} catch (Exception eException) {
		} finally {
		}
		return companyDO;
	}
	
	
	public List<CompanyDO> retrieve() {
		List<CompanyDO> companyList = null;
		try {
			companyList = genericObject.retrieve(CompanyDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return companyList;
	}
	
	@SuppressWarnings("unchecked")
	public List<CompanyDO> retrieveById(Long Id) {
		List<CompanyDO> companyList = null;
		
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(CompanyDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, Id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return companyList;
	}
	
	public CompanyDO update(CompanyDO companyDO) {
		try {
			genericObject.merge(companyDO);
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return companyDO;
	}

}
