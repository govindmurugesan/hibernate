	package com.spheresuite.erp.dao;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.ItSavingsDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class ItSavingsDAO {
	static Logger logger = Logger.getLogger(ItSavingsDAO.class.getName());
	@Autowired
	private GenericDAOImpl<ItSavingsDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public ItSavingsDO persist(ItSavingsDO itSavingsDO)  {
		try {
			genericObject.persist(itSavingsDO);
		} catch (Exception eException) {
		} finally {
		}
		return itSavingsDO;
	}
	
	public List<ItSavingsDO> retrieve()  {
		List<ItSavingsDO> itSavingsList = null;
		try {
			itSavingsList = genericObject.retrieve(ItSavingsDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return itSavingsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<ItSavingsDO> retrieveByEmpId(String empId)  {
		List<ItSavingsDO> itSavingsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(ItSavingsDO.FIND_BY_EMPID)
			.setParameter(CommonConstants.EMPID, empId)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return itSavingsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<ItSavingsDO> retrieveById(Long id)  {
		List<ItSavingsDO> itSavingsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(ItSavingsDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return itSavingsList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<ItSavingsDO> retrieveByEmpIdSectionIdAndDate(Date date, String empId, long sectionId)  {
		List<ItSavingsDO> itSavingsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(ItSavingsDO.FIND_BY_EMPID_DATE_SECTIONID)
					.setParameter(CommonConstants.EMPID, empId)
					.setParameter(CommonConstants.DATE, date)
					.setParameter(CommonConstants.SECTIONID, sectionId)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return itSavingsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<ItSavingsDO> retrieveByDate(Date fromDate, Date toDate)  {
		List<ItSavingsDO> itSavingsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(ItSavingsDO.FIND_BY_DATE)
			.setParameter(CommonConstants.FROMDATE, fromDate)
			.setParameter(CommonConstants.TODATE, toDate)
			.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return itSavingsList;
	}
	
	public ItSavingsDO update(ItSavingsDO itSavingsDO)  {
		try {
			genericObject.merge(itSavingsDO);
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return itSavingsDO;
	}

}
