package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.MenuDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class MenusDAO {
	
	static Logger logger = Logger.getLogger(MenusDAO.class.getName());
	
	@Autowired
	private SessionFactory sessionFactory;

		
	@SuppressWarnings("unchecked")
	public List<MenuDO> retrieveMainMenu()  {
		List<MenuDO> menuList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(MenuDO.FIND_MENU)
					.list();
		} catch (Exception eException) {
		} finally {
		}
		return menuList;
	}
	
	@SuppressWarnings("unchecked")
	public List<MenuDO> retrieveSuperMenu(Long menuId)  {
		List<MenuDO> menuList = null;
		try {
			
			return this.sessionFactory.getCurrentSession().getNamedQuery(MenuDO.FIND_SUPER_MENU)
			.setParameter(CommonConstants.SUPERMENU, menuId)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return menuList;
	}
	
	@SuppressWarnings("unchecked")
	public List<MenuDO> retrieveById(Long menuId)  {
		List<MenuDO> menuList = null;
		try {
			
			return this.sessionFactory.getCurrentSession().getNamedQuery(MenuDO.FIND_BY_ID)
			.setParameter(CommonConstants.ID, menuId)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return menuList;
	}
	
	@SuppressWarnings("unchecked")
	public List<MenuDO> retrieveByProductId(Long submenu)  {
		List<MenuDO> menuList = null;
		try {
			
			return this.sessionFactory.getCurrentSession().getNamedQuery(MenuDO.FIND_PRODUCT)
			.setParameter(CommonConstants.SUB_MENU, submenu)
			.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return menuList;
	}
}	
