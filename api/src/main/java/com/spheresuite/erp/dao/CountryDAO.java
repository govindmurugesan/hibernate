package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.CountryDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class CountryDAO {
	static Logger logger = Logger.getLogger(CountryDAO.class.getName());

	@Autowired
	private GenericDAOImpl<CountryDO> dao;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(CountryDO countryDO) {
		boolean persistStatus = true;
		try {
			List<CountryDO> countryList = null;
			countryList = this.sessionFactory.getCurrentSession().getNamedQuery(CountryDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, countryDO.getCountryName())
					.list();
					if(countryList != null && countryList.size() > 0){
						persistStatus = false;
					}else{
						dao.persist(countryDO);
					}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	
	public List<CountryDO> retrieve() {
		List<CountryDO> countryList = null;
		try {
			countryList = dao.retrieve(CountryDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return countryList;
	}
	
	public List<CountryDO> retrieveActive() {
		List<CountryDO> countryList = null;
		try {
			countryList =  dao.retrieveActive(CommonConstants.ACTIVE, CountryDO.FIND_BY_STATUS, CountryDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return countryList;
	}
	
	@SuppressWarnings("unchecked")
	public List<CountryDO> retrieveById(Long id) {
		List<CountryDO> countryList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(CountryDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return countryList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<CountryDO> retrieveByName(String name) {
		List<CountryDO> countryList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(CountryDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, name)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return countryList;
	}
	
	@SuppressWarnings("unchecked")
	public boolean update(CountryDO countryDO) {
		boolean updateStatus = true;
		try {
			List<CountryDO> countryDOList = null;
			countryDOList = this.sessionFactory.getCurrentSession().getNamedQuery(CountryDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, countryDO.getCountryName())
					.setParameter(CommonConstants.ID, countryDO.getCountryId())
					.list();
			if(countryDOList != null && countryDOList.size() > 0){
				updateStatus = false;
			}else{
				dao.merge(countryDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}
	
	public boolean persistList(List<CountryDO> countryDO){
		try {
			for (CountryDO country : countryDO) {
				dao.persist(country);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}

}
