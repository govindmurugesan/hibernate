package com.spheresuite.erp.dao;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.EmployeeLopDO;
import com.spheresuite.erp.util.CommonConstants;

@Repository
public class EmployeeLopDAO {
	static Logger logger = Logger.getLogger(EmployeeLopDAO.class.getName());
	@Autowired
	private GenericDAOImpl<EmployeeLopDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public EmployeeLopDO persist(EmployeeLopDO employeeLopDO) {
		try {
			genericObject.persist(employeeLopDO);
		} catch (Exception eException) {
		} finally {
		}
		return employeeLopDO;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeLopDO> retrieveByEmpId(String empId){
		List<EmployeeLopDO> employeeLopList = null;
		try {
			
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeLopDO.FIND_BY_EMPID)
			.setParameter(CommonConstants.EMPID, empId)
			.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeLopList;
	}
	
	public List<EmployeeLopDO> retrieve(){
		List<EmployeeLopDO> employeeLopList = null;
		try {
			employeeLopList = genericObject.retrieve(EmployeeLopDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return employeeLopList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeLopDO> retrieveById(Long id){
		List<EmployeeLopDO> employeeLopList = null;
		
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeLopDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeLopList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeLopDO> retrieveByEmpDate(String empId, Date startDate, Date endDate){
		List<EmployeeLopDO> employeeLopList = null;
		
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeLopDO.FIND_BY_EMPIDBYDATE)
					.setParameter(CommonConstants.EMPID, empId)
					.setParameter(CommonConstants.STARTDATE, startDate)
					.setParameter(CommonConstants.ENDDATE, endDate)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeLopList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeLopDO> retrieveByEmpDateForUpdate(Long id, String empId, Date startDate, Date endDate){
		List<EmployeeLopDO> employeeLopList = null;
		
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeLopDO.FIND_BY_EMPIDBYDATE_FORUPDATE)
					.setParameter(CommonConstants.EMPID, empId)
					.setParameter(CommonConstants.STARTDATE, startDate)
					.setParameter(CommonConstants.ENDDATE, endDate)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeLopList;
	}
	
	public EmployeeLopDO update(EmployeeLopDO employeeLopDO) {
		try {
			genericObject.merge(employeeLopDO);
		} catch (Exception eException) {
		} finally {
		}
		return employeeLopDO;
	}
	
	public boolean delete(Long id)  {
		try {
			org.hibernate.Query query = this.sessionFactory.getCurrentSession().createQuery("delete from EmployeeLopDO e where e.lopId=:id")
					.setParameter(CommonConstants.ID, id);
			query.executeUpdate();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return true;
	}
	
	public boolean deleteByDate(String empId, Date startDate, Date endDate)  {
		try {
			org.hibernate.Query query = this.sessionFactory.getCurrentSession().createQuery("delete from EmployeeLopDO e where e.employee.empId=:id AND e.startdate=:startDate AND e.enddate=:enddate")
					.setParameter(CommonConstants.ID, empId)
					.setParameter(CommonConstants.STARTDATE, startDate)
					.setParameter(CommonConstants.ENDDATE, endDate);
			query.executeUpdate();
		} catch (Exception eException) {
			System.out.println(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return true;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeLopDO> retrieveByEmpWithDate(List<String> empIds, String fromMonth){
		List<EmployeeLopDO> employeeLopList = null;
		try {
			if(empIds != null && empIds.size() > 0){
				org.hibernate.Query query = this.sessionFactory.getCurrentSession().createQuery("SELECT r FROM EmployeeLopDO r where SUBSTRING(r.startdate,1,7) =:fromMonth and r.employee.empId IN :empId and r.status =:status");
				query.setParameter(CommonConstants.EMPID, empIds);
				query.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE);
				query.setParameter("fromMonth", fromMonth);
				employeeLopList = (List<EmployeeLopDO>) query.list();
			}else{
				org.hibernate.Query query = this.sessionFactory.getCurrentSession().createQuery("SELECT r FROM EmployeeLopDO r where SUBSTRING(r.startdate,1,7) =:fromMonth and r.status =:status");
				
				query.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE);
				query.setParameter("fromMonth", fromMonth);
				employeeLopList = (List<EmployeeLopDO>) query.list();
			}
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeLopList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeLopDO> retrieveByEmpIdBetweenDate(List<String> empIds, String fromMonth, String toMonth){
		List<EmployeeLopDO> employeeLopList = null;
		try {
				logger.info("Entity Manager is not null");
				if(empIds != null && empIds.size() > 0){
					org.hibernate.Query q = this.sessionFactory.getCurrentSession().createQuery("SELECT r FROM EmployeeLopDO r where (SUBSTR(r.startdate,1,7) >=:fromMonth and SUBSTR(r.startdate,1,7) <=:toMonth) and r.employee.empId IN :empId and r.status =:status");
					q.setParameter(CommonConstants.EMPID, empIds);
					q.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE);
					q.setParameter("fromMonth", fromMonth);
					q.setParameter("toMonth", toMonth);
					employeeLopList = (List<EmployeeLopDO>) q.list();
				}else{
					org.hibernate.Query q = this.sessionFactory.getCurrentSession().createQuery("SELECT r FROM EmployeeLopDO r where (SUBSTR(r.startdate,1,7) >=:fromMonth and SUBSTR(r.startdate,1,7) <=:toMonth) and r.status =:status");
					q.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE);
					q.setParameter("fromMonth", fromMonth);
					q.setParameter("toMonth", toMonth);
					employeeLopList = (List<EmployeeLopDO>) q.list();
				}
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeLopList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeLopDO> retrieveByEmpIdDateStatus(String empId, Date startDate, Date endDate){
		List<EmployeeLopDO> employeeLopList = null;
		
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeLopDO.FIND_BY_EMPID_DATE_STATUS)
					.setParameter(CommonConstants.EMPID, empId)
					.setParameter(CommonConstants.STARTDATE, startDate)
					.setParameter(CommonConstants.ENDDATE, endDate)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeLopList;
	}
}
