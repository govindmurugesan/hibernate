	package com.spheresuite.erp.dao;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.EducationCessDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class EducationCessDAO {
	static Logger logger = Logger.getLogger(EducationCessDAO.class.getName());
	@Autowired
	private GenericDAOImpl<EducationCessDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(EducationCessDO educationCessDO) {
		boolean persistStatus = true;
		try {
			List<EducationCessDO> educationCess = null;
			educationCess =  this.sessionFactory.getCurrentSession().getNamedQuery(EducationCessDO.FIND_BY_FY)
					.setParameter(CommonConstants.FROMDATE, educationCessDO.getFromYear())
					.setParameter(CommonConstants.TODATE, educationCessDO.getToYear())
					.list();
			if(educationCess != null && educationCess.size() > 0){
				persistStatus = false;
			}else{
				genericObject.persist(educationCessDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	public List<EducationCessDO> retrieve()  {
		List<EducationCessDO> educationCessList = null;
		try {
			educationCessList = genericObject.retrieve(EducationCessDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return educationCessList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EducationCessDO> retrieveById(Long id)  {
		List<EducationCessDO> educationCessList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EducationCessDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return educationCessList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EducationCessDO> retrieveByDate(Date fromDate, Date toDate)  {
		List<EducationCessDO> educationCessList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EducationCessDO.FIND_BY_DATE)
			.setParameter(CommonConstants.FROMDATE, fromDate)
			.setParameter(CommonConstants.TODATE, toDate)
			.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return educationCessList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EducationCessDO> retrieveByCurrentDate(Date date)  {
		List<EducationCessDO> educationCessList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EducationCessDO.FIND_BY_CURRENTDATE)
			.setParameter(CommonConstants.DATE, date)
			.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
			.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return educationCessList;
	}
	
	@SuppressWarnings("unchecked")
	public boolean update(EducationCessDO educationCessDO) {
		boolean updateStatus = true;
		try {
			List<EducationCessDO> educationCessList = null;
			educationCessList =  this.sessionFactory.getCurrentSession().getNamedQuery(EducationCessDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.TODATE, educationCessDO.getToYear())
					.setParameter(CommonConstants.FROMDATE, educationCessDO.getFromYear())
					.setParameter(CommonConstants.ID, educationCessDO.getEducationCessId())
					.list();
			if(educationCessList != null && educationCessList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(educationCessDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}
	
	public boolean persistList(List<EducationCessDO> educationCessList){
		try {
			for (EducationCessDO educationCess : educationCessList) {
				genericObject.persist(educationCess);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}

}
