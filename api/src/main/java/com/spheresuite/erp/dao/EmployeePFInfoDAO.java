package com.spheresuite.erp.dao;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.EmployeePFInfoDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class EmployeePFInfoDAO {
	static Logger logger = Logger.getLogger(EmployeePFInfoDAO.class.getName());
	@Autowired
	private GenericDAOImpl<EmployeePFInfoDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public boolean persist(EmployeePFInfoDO employeeDO) {
		boolean persistStatus = true;
		try {
			genericObject.persist(employeeDO);
			/*List<EmployeePFAndESIInfoDO> empList = null;
			empList = this.sessionFactory.getCurrentSession().getNamedQuery(EmployeePFAndESIInfoDO.FIND_BY_ACCOUNTID)
					.setParameter(CommonConstants.PFACCOUNTNO, employeeDO.getPFAccNo())
					.setParameter(CommonConstants.ESIACCOUNTNO, employeeDO.getESIAccNo())
					.list();
					if(empList != null && empList.size() > 0){
						persistStatus = false;
					}else{
						genericObject.persist(employeeDO);
					}*/
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
		}
		return persistStatus;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeePFInfoDO> retrieveByEmpId(String empId) {
		List<EmployeePFInfoDO> employeeInformationList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeePFInfoDO.FIND_BY_EMPID)
					.setParameter(CommonConstants.EMPID, empId)
					.list(); 
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeInformationList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeePFInfoDO> retrieveByEmpIdAndDate(String empId, Date fromDate, Date toDate) {
		List<EmployeePFInfoDO> employeeInformationList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeePFInfoDO.FIND_BY_EMPID_DATE)
					.setParameter(CommonConstants.EMPID, empId)
					.setParameter(CommonConstants.FROMDATE, fromDate)
					.setParameter(CommonConstants.TODATE, toDate)
					.list(); 
		} catch (Exception eException) {
			eException.printStackTrace();
			System.out.println("eException"+eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeInformationList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeePFInfoDO> retrieveById(Long id) {
		List<EmployeePFInfoDO> employeeInformationList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeePFInfoDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeInformationList;
	}
	
	public EmployeePFInfoDO update(EmployeePFInfoDO employeeInfo) {
		try {
			genericObject.merge(employeeInfo);
		} catch (Exception eException) {
		} finally {
		}
		return employeeInfo;
	}
	
	public boolean persistList(List<EmployeePFInfoDO> pfDetailDO){
		try {
			for (EmployeePFInfoDO pf : pfDetailDO) {
				genericObject.persist(pf);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}

}
