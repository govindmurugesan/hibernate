package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.EmployeeContactInfoDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class EmployeeContactInfoDAO {
	static Logger logger = Logger.getLogger(EmployeeContactInfoDAO.class.getName());
	@Autowired
	private GenericDAOImpl<EmployeeContactInfoDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public EmployeeContactInfoDO persist(EmployeeContactInfoDO employeeContactInfoDO) {
		try {
			genericObject.persist(employeeContactInfoDO);
		} catch (Exception eException) {
		} finally {
		}
		return employeeContactInfoDO;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeContactInfoDO> retrieveByEmpId(String empId) {
		List<EmployeeContactInfoDO> employeeContactInfoList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeContactInfoDO.FIND_BY_EMPID)
					.setParameter(CommonConstants.EMPID, empId)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeContactInfoList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeContactInfoDO> retrieveById(Long id){
		List<EmployeeContactInfoDO> employeeContactInfoList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeContactInfoDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeContactInfoList;
	}
	
	public EmployeeContactInfoDO update(EmployeeContactInfoDO employeeContactInfoDO){
		try {
			genericObject.merge(employeeContactInfoDO);
		} catch (Exception eException) {
		} finally {
		}
		return employeeContactInfoDO;
	}
	
	public boolean persistList(List<EmployeeContactInfoDO> employeeContactInfoList){
		try {
			for (EmployeeContactInfoDO employeeContactInfo : employeeContactInfoList) {
				genericObject.persist(employeeContactInfo);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}

}
