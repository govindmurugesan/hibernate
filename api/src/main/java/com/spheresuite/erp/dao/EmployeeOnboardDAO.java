package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.EmployeeOnboardDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class EmployeeOnboardDAO {
	static Logger logger = Logger.getLogger(EmployeeOnboardDAO.class.getName());
	@Autowired
	private GenericDAOImpl<EmployeeOnboardDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(EmployeeOnboardDO empDetail) {
		boolean persistStatus = true;
		try {
			List<EmployeeOnboardDO> empList = null;
			empList = this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeOnboardDO.FIND_BY_EMAILID_STATUS)
					.setParameter(CommonConstants.EMAIL, empDetail.getPersonalemail())
					.list();
					if(empList != null && empList.size() > 0){
						persistStatus = false;
					}else{
						genericObject.persist(empDetail);
					}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	public List<EmployeeOnboardDO> retrieve() {
		List<EmployeeOnboardDO> employeeOnboardList = null;
		try {
			employeeOnboardList = genericObject.retrieve(EmployeeOnboardDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return employeeOnboardList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<EmployeeOnboardDO> retrieveById(Long id){
		List<EmployeeOnboardDO> employeeOnboardList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeOnboardDO.FIND_BY_ID)
			.setParameter(CommonConstants.ID, id)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeOnboardList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeOnboardDO> retrieveByEmail(String personalEmail){
		List<EmployeeOnboardDO> employeeOnboardList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeOnboardDO.FIND_BY_EMAIL)
			.setParameter(CommonConstants.PERSONALEMAIL, personalEmail)
			.setParameter(CommonConstants.STATUS, CommonConstants.RCHAR)
			.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeOnboardList;
	}
	
	/*@SuppressWarnings("unchecked")
	public List<EmployeeOnboardDO> retrieveByEmailWithoutStatus(String personalEmail){
		List<EmployeeOnboardDO> employeeOnboardList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeOnboardDO.FIND_BY_EMAILID_WITHOUT_STATUS)
			.setParameter(CommonConstants.PERSONALEMAIL, personalEmail)
			.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeOnboardList;
	}*/
	

	@SuppressWarnings("unchecked")
	public boolean update(EmployeeOnboardDO empDetails) {
		boolean updateStatus = true;
		try {
			List<EmployeeOnboardDO> empDetailsList = null;
			empDetailsList = this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeOnboardDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.EMAIL, empDetails.getPersonalemail())
					.setParameter(CommonConstants.ID, empDetails.getEmployeeOnboardId())
					.list();
			if(empDetailsList != null && empDetailsList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(empDetails);
			}
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}

	public boolean delete(Long id) {
		try {
			this.sessionFactory.getCurrentSession().createQuery("delete from EmployeeOnboardDO e where e.id=:id")
										.setParameter(CommonConstants.ID, id)
										.executeUpdate();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return true;
	}

}
