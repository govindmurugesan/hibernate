package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.UserDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class UserDAO {
	static Logger logger = Logger.getLogger(UserDAO.class.getName());
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private GenericDAOImpl<UserDO> genericObject;
	
	@SuppressWarnings("unchecked")
	public boolean persist(UserDO userDO) {
		boolean persistStatus = true;
		try {
			List<UserDO> userDOList = null;
			userDOList = this.sessionFactory.getCurrentSession().getNamedQuery(UserDO.FIND_BY_EMP_ID)
					.setParameter(CommonConstants.USERID, userDO.getEmployee().getEmpId())
					.list();
					if(userDOList != null && userDOList.size() > 0){
						persistStatus = false;
					}else{
						genericObject.persist(userDO);
					}
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
		}
		return persistStatus;
	}
	
	
	public List<UserDO> retrieve() {
		List<UserDO> userList = null;
		try {
			userList = genericObject.retrieve(UserDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return userList;
	}
	
	public List<UserDO> retrieveActive() {
		List<UserDO> userList = null;
		try {
			userList =  genericObject.retrieveActive(CommonConstants.ACTIVE, UserDO.FIND_BY_STATUS, UserDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return userList;
	}
	
	@SuppressWarnings("unchecked")
	public List<UserDO> retrieveById(Long id) {
		List<UserDO> user = null;
		try {
			user = this.sessionFactory.getCurrentSession().getNamedQuery(UserDO.FIND_BY_ID)
										.setParameter(CommonConstants.ID, id)
										.list();
		} catch (Exception eException) {
		} finally {
		}
		return user;
	}
	
	@SuppressWarnings("unchecked")
	public List<UserDO> retrieveByEmail(String personal, String secondary) {
		List<UserDO> user = null;
		try {
			user = this.sessionFactory.getCurrentSession().getNamedQuery(UserDO.FIND_BY_EMAIL_FOR_EMP)
										.setParameter(CommonConstants.PERSONAL, personal)
										.setParameter(CommonConstants.SECONDARY, secondary)
										.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return user;
	}
	
	@SuppressWarnings("unchecked")
	public List<UserDO> retrieveByEmailId(String email) {
		List<UserDO> user = null;
		try {
			user = this.sessionFactory.getCurrentSession().getNamedQuery(UserDO.FIND_BY_EMAIL)
					.setParameter(CommonConstants.EMAIL, email)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return user;
	}
	
	@SuppressWarnings("unchecked")
	public List<UserDO> retrieveByTempPassword(String tempPassword, String empId) {
		List<UserDO> user = null;
		try {
			user = this.sessionFactory.getCurrentSession().getNamedQuery(UserDO.FIND_BY_TEMPPASSWORD)
					.setParameter(CommonConstants.TEMP_PASSWORD, tempPassword)
					.setParameter(CommonConstants.EMPID, empId)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return user;
	}
	
	/*public UserDO update(UserDO userDO) {
		try {
			genericObject.merge(userDO);
		} catch (Exception eException) {
		} finally {
		}
		return userDO;
	}*/
	
	@SuppressWarnings("unchecked")
	public boolean update(UserDO userDO) {
		boolean updateStatus = true;
		try {
			//List<UserDO> userDOList = null;
			genericObject.merge(userDO);
			/*userDOList = this.sessionFactory.getCurrentSession().getNamedQuery(UserDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.EMAIL, userDO.getEmployee().getCompanyemail())
					.setParameter(CommonConstants.ID, userDO.getUserId())
					.list();
			if(userDOList != null && userDOList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(userDO);
			}*/
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
		}
		return updateStatus;
	}
	
	public boolean updatePassword(UserDO userDO) {
		boolean updateStatus = true;
		try {
				genericObject.merge(userDO);
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
		}
		return updateStatus;
	}
	
	@SuppressWarnings("unchecked")
	public List<UserDO> retrieveForLogin(String email,String password) {
		List<UserDO> user = null;
		try {
			user = this.sessionFactory.getCurrentSession().getNamedQuery(UserDO.FIND_FOR_LOGIN)
					.setParameter(CommonConstants.EMAIL, email)
					.setParameter(CommonConstants.PASSWORD, password)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return user;
	}
	
	@SuppressWarnings("unchecked")
	public List<UserDO> retrieveUserByUnitId(Long unitId) {
		List<UserDO> user = null;
		try {
			user = this.sessionFactory.getCurrentSession().getNamedQuery(UserDO.FIND_BY_UNITID)
					.setParameter(CommonConstants.UNIT, unitId)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return user;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<UserDO> retrieveByEmpId(String id) {
		List<UserDO> user = null;
		try {
			user = this.sessionFactory.getCurrentSession().getNamedQuery(UserDO.FIND_BY_EMP_ID)
										.setParameter(CommonConstants.USERID, id)
										.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return user;
	}
	
	@SuppressWarnings("unchecked")
	public List<UserDO> getSuperAdmins() {
		List<UserDO> userList = null;
		try {
			userList = this.sessionFactory.getCurrentSession().getNamedQuery(UserDO.FIND_SUPERADMIN)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.setParameter(CommonConstants.ADMIN, Long.parseLong(CommonConstants.ADMINID))
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return userList;
	}

}
