package com.spheresuite.erp.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.EmployeeCompOffDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class EmployeeCompOffDAO {
	@Autowired
	private GenericDAOImpl<EmployeeCompOffDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public boolean persist(EmployeeCompOffDO earningTypeDO){
		boolean persistStatus = true;
		try {
				genericObject.persist(earningTypeDO);
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	public boolean persist(List<EmployeeCompOffDO> earningTypeDO){
		boolean persistStatus = true;
		try {
			for (EmployeeCompOffDO employeeCompOffDO : earningTypeDO) {
				genericObject.persist(employeeCompOffDO);
			}
				
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeCompOffDO> retrieve(){
		List<EmployeeCompOffDO> earningTypeList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeCompOffDO.FIND_ALL)
					.list();
		} catch (Exception eException) {
		} finally {
		}
		return earningTypeList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<EmployeeCompOffDO> retrieveById(Long id) {
		List<EmployeeCompOffDO> earningTypeList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeCompOffDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return earningTypeList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeCompOffDO> retrieveByEmpId(String empId) {
		List<EmployeeCompOffDO> earningTypeList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeCompOffDO.FIND_BY_EMPID)
					.setParameter(CommonConstants.ID, empId)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return earningTypeList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<EmployeeCompOffDO> retrieveByReportingId(String empId) {
		List<EmployeeCompOffDO> earningTypeList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeCompOffDO.FIND_BY_REPORTINGID)
					.setParameter(CommonConstants.ID, empId)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return earningTypeList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeCompOffDO> retrieveByEmpIdValidCompOff(String empId, Date date) {
		List<EmployeeCompOffDO> earningTypeList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeCompOffDO.FIND_BY_EMPID_VALIDDATE)
					.setParameter(CommonConstants.ID, empId)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.setParameter(CommonConstants.EXPIRYDATE, date)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return earningTypeList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeCompOffDO> retrieveByEmpIdValidCompOffWithType(String empId, Date date, char type) {
		List<EmployeeCompOffDO> earningTypeList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeCompOffDO.FIND_BY_EMPID_VALIDDATEANDTYPE)
					.setParameter(CommonConstants.ID, empId)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.setParameter(CommonConstants.EXPIRYDATE, date)
					.setParameter(CommonConstants.TYPE, type)
					.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return earningTypeList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeCompOffDO> retrieveByEmpCompOffWithTypeExpiry(String empId, Date date, char type) {
		List<EmployeeCompOffDO> earningTypeList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeCompOffDO.FIND_BY_EMPID_EXPITYDATE_TYPE)
					.setParameter(CommonConstants.ID, empId)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.setParameter(CommonConstants.EXPIRYDATE, date)
					.setParameter(CommonConstants.TYPE, type)
					.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return earningTypeList;
	}
	
	public boolean update(EmployeeCompOffDO employeeCompOffDO){
		boolean updateStatus = true;
		try {
				genericObject.merge(employeeCompOffDO);
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}

}
