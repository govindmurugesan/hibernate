package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.ChequeManagementDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class ChequeManagementDAO {
	static Logger logger = Logger.getLogger(BillsDAO.class.getName());
	@Autowired
	private GenericDAOImpl<ChequeManagementDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public ChequeManagementDO persist(ChequeManagementDO chequeDetails) {
		try {
			genericObject.persist(chequeDetails);
		} catch (Exception eException) {
		} finally {
		}
		return chequeDetails;
	}
	
	
	public List<ChequeManagementDO> retrieve() {
		List<ChequeManagementDO> chequeList = null;
		
		try {
			chequeList = genericObject.retrieve(ChequeManagementDO.class);
		} catch (Exception eException) {
		} finally {
			
		}
		return chequeList;
	}
	
	@SuppressWarnings("unchecked")
	public List<ChequeManagementDO> retrieveById(Long id) {
		List<ChequeManagementDO> chequeList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(ChequeManagementDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return chequeList;
	}
	
	public ChequeManagementDO update(ChequeManagementDO chequeDO) {
		try {
			genericObject.merge(chequeDO);
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return chequeDO;
	}
	
	@SuppressWarnings("unchecked")
	public List<ChequeManagementDO> retrieveActive() {
		List<ChequeManagementDO> chequeList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(ChequeManagementDO.FIND_ACTIVE)
					.setParameter(CommonConstants.STATUS, CommonConstants.NOTCLEARED)
					.list();
			
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return chequeList;
	}

}
