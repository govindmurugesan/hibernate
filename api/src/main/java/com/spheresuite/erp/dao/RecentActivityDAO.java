package com.spheresuite.erp.dao;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.RecentActivityDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class RecentActivityDAO {
	static Logger logger = Logger.getLogger(RecentActivityDAO.class.getName());

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private GenericDAOImpl<RecentActivityDO> genericObject;
	
	public RecentActivityDO persist(RecentActivityDO recentActivityDO) {
		try {
			genericObject.persist(recentActivityDO);
		} catch (Exception eException) {
			//throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			//SessionManager.closeEntityManager(em);
		}
		return recentActivityDO;
	}
	
	
	public List<RecentActivityDO> retrieve() {
		List<RecentActivityDO> recentActivityList = null;
		try {
			recentActivityList = genericObject.retrieve(RecentActivityDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return recentActivityList;
	}
	
	@SuppressWarnings("unchecked")
	public List<RecentActivityDO> retrieveByEmpId(String id) {
		List<RecentActivityDO> recentActivityList = null;
		try {
			recentActivityList = this.sessionFactory.getCurrentSession().getNamedQuery(RecentActivityDO.FIND_BY_EMP_ID)
										.setParameter(CommonConstants.ID, id)
										.setMaxResults(10)
										.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return recentActivityList;
	}
	
	@SuppressWarnings("unchecked")
	public List<RecentActivityDO> retrieveByEmp(String id) {
		List<RecentActivityDO> recentActivityList = null;
		try {
			recentActivityList = this.sessionFactory.getCurrentSession().getNamedQuery(RecentActivityDO.FIND_BY_EMP_ID)
										.setParameter(CommonConstants.ID, id)
										.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return recentActivityList;
	}
	
	@SuppressWarnings("unchecked")
	public List<RecentActivityDO> retrieveByEmpDate(String id, Date date) {
		List<RecentActivityDO> recentActivityList = null;
		try {
			recentActivityList = this.sessionFactory.getCurrentSession().getNamedQuery(RecentActivityDO.FIND_BY_EMP_DATE)
										.setParameter(CommonConstants.ID, id)
										.setParameter(CommonConstants.DATE, date)
										.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return recentActivityList;
	}
	
	public RecentActivityDO update(RecentActivityDO recentActivityDO) {
		try {
			genericObject.merge(recentActivityDO);
		} catch (Exception eException) {
		} finally {
		}
		return recentActivityDO;
	}

}
