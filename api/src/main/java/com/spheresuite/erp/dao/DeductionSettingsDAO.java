package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.DeductionSettingDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class DeductionSettingsDAO {
	static Logger logger = Logger.getLogger(DeductionSettingsDAO.class.getName());
	@Autowired
	private GenericDAOImpl<DeductionSettingDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public DeductionSettingDO persist(DeductionSettingDO deductionSettingDO) {
		try {
			genericObject.persist(deductionSettingDO);
		} catch (Exception eException) {
		} finally {
		}
		return deductionSettingDO;
	}
	
	public DeductionSettingDO update(DeductionSettingDO deductionSettingDO) {
		try {
			genericObject.merge(deductionSettingDO);
		} catch (Exception eException) {
		} finally {
		}
		return deductionSettingDO;
	}
	
	public List<DeductionSettingDO> retrieve() {
		List<DeductionSettingDO> deductionSettingList = null;
		try {
			deductionSettingList = genericObject.retrieve(DeductionSettingDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return deductionSettingList;
	}
	
	@SuppressWarnings("unchecked")
	public List<DeductionSettingDO> retrieveById(Long Id) {
		List<DeductionSettingDO> deductionSettingList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(DeductionSettingDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, Id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return deductionSettingList;
	}
	
	@SuppressWarnings("unchecked")
	public List<DeductionSettingDO> retrieveActive(String name) {
		List<DeductionSettingDO> deductionSettingList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(DeductionSettingDO.FIND_ACTIVE)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.setParameter(CommonConstants.NAME, name)
					.list();
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return deductionSettingList;
	}
}
