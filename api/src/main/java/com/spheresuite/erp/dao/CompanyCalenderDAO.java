package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.CompanyCalenderDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class CompanyCalenderDAO {
	static Logger logger = Logger.getLogger(CompanyCalenderDAO.class.getName());
	@Autowired
	private GenericDAOImpl<CompanyCalenderDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	
	public List<CompanyCalenderDO> persist(List<CompanyCalenderDO> comapnyCalenderDO) {
		try {
			for (CompanyCalenderDO comapnyCalender : comapnyCalenderDO) {
				genericObject.persist(comapnyCalender);
			}
		} catch (Exception eException) {
			//throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			//SessionManager.closeEntityManager(em);
		}
		return comapnyCalenderDO;
	}
	
	public boolean deleteByMonth(String month, Long batchId)  {
		try {
			org.hibernate.Query query = this.sessionFactory.getCurrentSession().createQuery("delete from CompanyCalenderDO e where e.month=:monthly and e.payrollBatch.payrollBatchId =:id")
					.setParameter(CommonConstants.MONTHLY, month)
					.setParameter(CommonConstants.ID, batchId);
					query.executeUpdate();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return true;
	}
	
	@SuppressWarnings("unchecked")
	public List<CompanyCalenderDO> retrieveByMonth(String month, Long batchId)  {
		List<CompanyCalenderDO> calenderList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(CompanyCalenderDO.FINDBYMONTH)
					.setParameter(CommonConstants.MONTHLY, month)
					.setParameter(CommonConstants.ID, batchId)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return calenderList;
	}
	
	
	
/*	@SuppressWarnings("unchecked")
	public boolean persist(companyCalenderDO days)  {
		boolean persistStatus = true;
		try {
			List<DesignationDO> DesignationDOList = null;
			DesignationDOList = this.sessionFactory.getCurrentSession().getNamedQuery(DesignationDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, designationDO.getName())
					.list();
					if(DesignationDOList != null && DesignationDOList.size() > 0){
						persistStatus = false;
					}else{
						genericObject.persist(designationDO);
					}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}*/
	
	/*public List<DesignationDO> retrieve()  {
		List<DesignationDO> designationList = null;
		try {
			designationList = genericObject.retrieve(DesignationDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return designationList;
	}
	
	@SuppressWarnings("unchecked")
	public List<DesignationDO> retrieveActive()  {
		List<DesignationDO> designationList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(DesignationDO.FIND_BY_STATUS)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return designationList;
	}
	
	@SuppressWarnings("unchecked")
	public List<DesignationDO> retrieveById(Long id)  {
		List<DesignationDO> designationList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(DesignationDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return designationList;
	}
	
	@SuppressWarnings("unchecked")
	public List<DesignationDO> retrieveByName(String name)  {
		List<DesignationDO> designationList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(DesignationDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, name)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return designationList;
	}
	
	@SuppressWarnings("unchecked")
	public boolean update(DesignationDO designationDO)  {
		boolean updateStatus = true;
		try {
			List<DesignationDO> designationList = null;
			designationList = this.sessionFactory.getCurrentSession().getNamedQuery(DesignationDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, designationDO.getName())
					.setParameter(CommonConstants.ID, designationDO.getDesignationId())
					.list();
			if(designationList != null && designationList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(designationDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}*/
}
