package com.spheresuite.erp.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.ESIGroupEarningsDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class ESIGroupEarningsDAO {
	@Autowired
	private GenericDAOImpl<ESIGroupEarningsDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(ESIGroupEarningsDO esiGroupEarningsDO){
		List<ESIGroupEarningsDO> esiGroupEarningList = null;
		boolean persistStatus = true;
		try {
			esiGroupEarningList = this.sessionFactory.getCurrentSession().getNamedQuery(ESIGroupEarningsDO.FIND_FOR_ADD)
			.setParameter(CommonConstants.ALLOWENCE_ID, esiGroupEarningsDO.getAllowanceSettings().getAllowanceSettingsId())
			.setParameter(CommonConstants.ESIGROUPID, esiGroupEarningsDO.getEsiGroup().getEsiGroupId())
			.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
			.list();
			if(esiGroupEarningList != null && esiGroupEarningList.size() > 0){
				persistStatus = false;
			}else{
				genericObject.persist(esiGroupEarningsDO);
			}
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
		}
		return persistStatus;
	}
	
	@SuppressWarnings("unchecked")
	public List<ESIGroupEarningsDO> retrieveByGroupID(Long id) {
		List<ESIGroupEarningsDO> esiGroupEarningList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(ESIGroupEarningsDO.FIND_BY_GROUP_ID)
					.setParameter(CommonConstants.ID, id)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return esiGroupEarningList;
	}
	
	
	
	@SuppressWarnings("unchecked")
	public List<ESIGroupEarningsDO> retrieveByID(Long id) {
		List<ESIGroupEarningsDO> esiGroupEarningList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(ESIGroupEarningsDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return esiGroupEarningList;
	}
	
	public boolean updateList(List<ESIGroupEarningsDO> esiGroupEarningList){
		try {
			for (ESIGroupEarningsDO esiGroupEarning : esiGroupEarningList) {
				genericObject.merge(esiGroupEarning);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}

}
