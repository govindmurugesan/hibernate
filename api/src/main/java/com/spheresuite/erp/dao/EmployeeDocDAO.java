package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.EmployeeDocDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class EmployeeDocDAO {
	static Logger logger = Logger.getLogger(EmployeeDocDAO.class.getName());
	@Autowired
	private GenericDAOImpl<EmployeeDocDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public boolean delete(Long id){
		try {
			org.hibernate.Query query = this.sessionFactory.getCurrentSession().createQuery("delete from EmployeeDocDO e where e.opportunityId="+id);
			query.executeUpdate();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return true;
	}
	
	
	public EmployeeDocDO persist(EmployeeDocDO employeeDocDO){
		try {
			genericObject.persist(employeeDocDO);
		} catch (Exception eException) {
		} finally {
		}
		return employeeDocDO;
	}
	
	
	public List<EmployeeDocDO> retrieve() {
		List<EmployeeDocDO> employeeDocList = null;
		try {
			employeeDocList = genericObject.retrieve(EmployeeDocDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return employeeDocList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeDocDO> retrieveByEmpId(String id){
		List<EmployeeDocDO> employeeDocList = null;
		try {
			
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeDocDO.FIND_BY_EMP_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeDocList;
	}
	
	public EmployeeDocDO update(EmployeeDocDO employeeDocDO){
		try {
			genericObject.merge(employeeDocDO);
		} catch (Exception eException) {
		} finally {
		}
		return employeeDocDO;
	}

}
