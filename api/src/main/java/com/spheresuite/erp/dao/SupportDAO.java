package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.SupportDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class SupportDAO {
	static Logger logger = Logger.getLogger(SupportDAO.class.getName());
	@Autowired
	private GenericDAOImpl<SupportDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public SupportDO persist(SupportDO supportDO) {
		try {
			genericObject.persist(supportDO);
		} catch (Exception eException) {
		} finally {
		}
		return supportDO;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<SupportDO> retrieve(List<String> ids) {
		List<SupportDO> contactList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(SupportDO.FIND_ALL)
					.setParameterList(CommonConstants.ID, ids)
					.list(); 
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return contactList;
	}
	
	@SuppressWarnings("unchecked")
	public List<SupportDO> retrieveAll() {
		List<SupportDO> contactList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(SupportDO.FIND_ALL_DATA)
					.list(); 
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return contactList;
	}
	
	@SuppressWarnings("unchecked")
	public List<SupportDO> retrieveById(Long id) {
		List<SupportDO> contactList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(SupportDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list(); 
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return contactList;
	}
	
	@SuppressWarnings("unchecked")
	public List<SupportDO> retriveByEmailId(String email) {
		List<SupportDO> contactList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(SupportDO.FIND_BY_EMAIL_ID)
					.setParameter(CommonConstants.EMAIL, email)
					.list(); 
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return contactList;
	}
	
	@SuppressWarnings("unchecked")
	public List<SupportDO> retrieveByLeadId(Long id) {
		List<SupportDO> contactList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(SupportDO.FIND_BY_SUPPORT_ID)
					.setParameter(CommonConstants.ID, id)
					.list(); 
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return contactList;
	}
	
	public SupportDO update(SupportDO supportDO) {
		try {
			genericObject.merge(supportDO);
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return supportDO;
	}
	
	public boolean update(List<SupportDO> supportDO) {
		try {
			for (SupportDO supportDO2 : supportDO) {
				genericObject.merge(supportDO2);
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return true;
	}
	
	public boolean persistList(List<SupportDO> supportDO) {
		try {
			for (SupportDO supportDO2 : supportDO) {
				genericObject.persist(supportDO2);
			}
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
		}
		return true;
	}
}
