package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.EmployeeTaxDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class EmployeeTaxDAO {
	static Logger logger = Logger.getLogger(EmployeeTaxDAO.class.getName());
	@Autowired
	private GenericDAOImpl<EmployeeTaxDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public List<EmployeeTaxDO> persist(List<EmployeeTaxDO> employeetaxDO)  {
		try {
			for (EmployeeTaxDO employeetaxDO2 : employeetaxDO) {
				genericObject.persist(employeetaxDO2);
			}
		} catch (Exception eException) {
		} finally {
		}
		return employeetaxDO;
	}
	
	
	public List<EmployeeTaxDO> retrieve()  {
		List<EmployeeTaxDO> employeeDeductionList = null;
		try {
			employeeDeductionList = genericObject.retrieve(EmployeeTaxDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return employeeDeductionList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeTaxDO> retrieveByEmpCompensationId(Long id)  {
		List<EmployeeTaxDO> employeeDeductionList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeTaxDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeDeductionList;
	}
	
	public EmployeeTaxDO update(EmployeeTaxDO employeetaxDO)  {
		try {
			genericObject.merge(employeetaxDO);
		} catch (Exception eException) {
		} finally {
		}
		return employeetaxDO;
	}

}
