package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.SalutationDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class SalutationDAO {
	static Logger logger = Logger.getLogger(SalutationDAO.class.getName());

	@Autowired
	private GenericDAOImpl<SalutationDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(SalutationDO salutationDO) {
		boolean persistStatus = true;
		try {
			List<SalutationDO> salutationDOList = null;
			salutationDOList = this.sessionFactory.getCurrentSession().getNamedQuery(SalutationDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, salutationDO.getName())
					.list();
					if(salutationDOList != null && salutationDOList.size() > 0){
						persistStatus = false;
					}else{
						genericObject.persist(salutationDO);
					}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	public List<SalutationDO> retrieve() {
		List<SalutationDO> salutationList = null;
		try {
			salutationList = genericObject.retrieve(SalutationDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return salutationList;
	}
	
	public List<SalutationDO> retrieveActive() {
		List<SalutationDO> salutationList = null;
		try {
			salutationList =  genericObject.retrieveActive(CommonConstants.ACTIVE, SalutationDO.FIND_BY_STATUS, SalutationDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return salutationList;
	}
	
	@SuppressWarnings("unchecked")
	public List<SalutationDO> retrieveById(Long Id) {
		List<SalutationDO> salutationList = null;
		try {
			salutationList =  this.sessionFactory.getCurrentSession().getNamedQuery(SalutationDO.FIND_BY_ID)
								.setParameter(CommonConstants.ID, Id).list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return salutationList;
	}
	
	@SuppressWarnings("unchecked")
	public List<SalutationDO> retrieveByName(String name) {
		List<SalutationDO> salutationList = null;
		try {
			salutationList =  this.sessionFactory.getCurrentSession().getNamedQuery(SalutationDO.FIND_BY_NAME)
								.setParameter(CommonConstants.NAME, name).list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return salutationList;
	}
	
	@SuppressWarnings("unchecked")
	public boolean update(SalutationDO salutationDO) {
		boolean updateStatus = true;
		try {
			List<SalutationDO> salutationDOList = null;
			salutationDOList = this.sessionFactory.getCurrentSession().getNamedQuery(SalutationDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, salutationDO.getName())
					.setParameter(CommonConstants.ID, salutationDO.getSalutationId())
					.list();
			if(salutationDOList != null && salutationDOList.size() > 0){
				updateStatus =  false;
			}else{
				genericObject.merge(salutationDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}

	public boolean persistList(List<SalutationDO> salutationList){
		try {
			for (SalutationDO salutation : salutationList) {
				genericObject.persist(salutation);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}
}
