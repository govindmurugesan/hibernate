package com.spheresuite.erp.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.LeaveGroupSettingsDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class LeaveGroupSettingsDAO {
	@Autowired
	private GenericDAOImpl<LeaveGroupSettingsDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(LeaveGroupSettingsDO leaveGroupSettingsDO){
		List<LeaveGroupSettingsDO> leaveGroupSettingsList = null;
		boolean persistStatus = true;
		try {
			if(leaveGroupSettingsDO.getLeaveManagement() != null){
				leaveGroupSettingsList = this.sessionFactory.getCurrentSession().getNamedQuery(LeaveGroupSettingsDO.FIND_FOR_ADD)
				.setParameter(CommonConstants.ID, leaveGroupSettingsDO.getLeaveManagement().getLeaveManagementId())
				.setParameter(CommonConstants.LEAVEGROUP_ID, leaveGroupSettingsDO.getLeaveGroup().getLeaveGroupId())
				.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
				.list();
			}
			if(leaveGroupSettingsList != null && leaveGroupSettingsList.size() > 0){
				persistStatus = false;
			}else{
				genericObject.persist(leaveGroupSettingsDO);
			}
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
		}
		return persistStatus;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeaveGroupSettingsDO> retrieveByGroupIdWithStatus(Long id) {
		List<LeaveGroupSettingsDO> leaveGroupSettingsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeaveGroupSettingsDO.FIND_BY_GROUP_ID_STATUS)
					.setParameter(CommonConstants.ID, id)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return leaveGroupSettingsList;
	}
	
/*	@SuppressWarnings("unchecked")
	public List<LeaveGroupSettingsDO> retrieveByGroupID(Long id) {
		List<LeaveGroupSettingsDO> leaveGroupSettingsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeaveGroupSettingsDO.FIND_BY_GROUP_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return leaveGroupSettingsList;
	}*/
	
	@SuppressWarnings("unchecked")
	public List<LeaveGroupSettingsDO> retrieveByGroupID(Long id) {
		List<LeaveGroupSettingsDO> leaveGroupSettingsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeaveGroupSettingsDO.FIND_BY_GROUP_ID)
					.setParameter(CommonConstants.ID, id)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
			/*leaveGroupSettingsList = this.sessionFactory.getCurrentSession().getNamedQuery(LeaveGroupSettingsDO.FIND_BY_GROUP_ID)
					.setParameter(CommonConstants.ID, id)
					.list();*/
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return leaveGroupSettingsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeaveGroupSettingsDO> retrieveByID(Long id) {
		List<LeaveGroupSettingsDO> leaveGroupSettingsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeaveGroupSettingsDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return leaveGroupSettingsList;
	}
	
	public boolean updateList(List<LeaveGroupSettingsDO> leaveGroupSettingsList){
		try {
			for (LeaveGroupSettingsDO leaveGroupSettings : leaveGroupSettingsList) {
				genericObject.merge(leaveGroupSettings);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}
	
	public boolean update(LeaveGroupSettingsDO leaveGroupList){
		boolean updatStatus = true;
		try {
				genericObject.merge(leaveGroupList);
		} catch (Exception eException) {
			updatStatus = false;
		} finally {
		}
		return updatStatus;
	}
	
	
	
	@SuppressWarnings("unchecked")
	public List<LeaveGroupSettingsDO> retrieveAvailableLeavesByGrpId(Long grpId, Date currentDate) {
		List<LeaveGroupSettingsDO> leaveGroupSettingsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeaveGroupSettingsDO.FIND_BY_GROUP_DATE)
					.setParameter(CommonConstants.ID, grpId)
					.setParameter(CommonConstants.DATE, currentDate)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return leaveGroupSettingsList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<LeaveGroupSettingsDO> retrieveAvailableLeavesByGrpIdAndDate(Long grpId, Date fromDate,  Date toDate) {
		List<LeaveGroupSettingsDO> leaveGroupSettingsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeaveGroupSettingsDO.FIND_BY_GROUP_FROMTODATE)
					.setParameter(CommonConstants.ID, grpId)
					//.setParameter(CommonConstants.FROMDATE, fromDate)
					//.setParameter(CommonConstants.TODATE, toDate)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return leaveGroupSettingsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeaveGroupSettingsDO> retrieveByGrpAndLeaveId(Long grpId, Long leaveId) {
		List<LeaveGroupSettingsDO> leaveGroupSettingsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeaveGroupSettingsDO.FIND_BY_GROUP_LEAVE_ID)
					.setParameter(CommonConstants.ID, leaveId)
					.setParameter(CommonConstants.LEAVEGROUP_ID, grpId)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return leaveGroupSettingsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeaveGroupSettingsDO> retrieveByGrpAndCompensation(Long grpId, String type) {
		List<LeaveGroupSettingsDO> leaveGroupSettingsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeaveGroupSettingsDO.FIND_BY_GROUP_COMPENSATION)
					.setParameter(CommonConstants.LEAVEGROUP_ID, grpId)
					.setParameter(CommonConstants.TYPE, type)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return leaveGroupSettingsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeaveGroupSettingsDO> retrieveByGrpAndPermission(Long grpId, String type) {
		List<LeaveGroupSettingsDO> leaveGroupSettingsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeaveGroupSettingsDO.FIND_BY_GROUP_PERMISSION)
					.setParameter(CommonConstants.LEAVEGROUP_ID, grpId)
					.setParameter(CommonConstants.TYPE, type)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return leaveGroupSettingsList;
	}
	
	
	/*public List<PayrollGroupDO> retrieve(){
		List<PayrollGroupDO> payrollGroupList = null;
		try {
			payrollGroupList = genericObject.retrieve(PayrollGroupDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return payrollGroupList;
	}
	
	public List<PayrollGroupDO> retrieveActive(){
		List<PayrollGroupDO> payrollGroupList = null;
		try {
			return genericObject.retrieveActive(CommonConstants.ACTIVE, PayrollGroupDO.FIND_BY_STATUS, PayrollGroupDO.class);
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return payrollGroupList;
	}
	
	
	*/
	/*@SuppressWarnings("unchecked")
	public boolean update(PayrollGroupDO payrollGroupDO){
		boolean updateStatus = true;
		try {
			List<PayrollGroupDO> payrollGroupList = null;
			payrollGroupList = this.sessionFactory.getCurrentSession().getNamedQuery(PayrollGroupDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, payrollGroupDO.getName())
					.setParameter(CommonConstants.ID, payrollGroupDO.getPayrollGroupId())
					.list();
			if(payrollGroupList != null && payrollGroupList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(payrollGroupDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}*/

}
