package com.spheresuite.erp.dao;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.LeaveRequestsDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class LeaveRequestsDAO {
	static Logger logger = Logger.getLogger(LeaveRequestsDAO.class.getName());
	@Autowired
	private GenericDAOImpl<LeaveRequestsDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public LeaveRequestsDO persist(LeaveRequestsDO leaverequestDetail)  {
		try {
			genericObject.persist(leaverequestDetail);
		} catch (Exception eException) {
		} finally {
		}
		return leaverequestDetail;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeaveRequestsDO> retrieve()  {
		List<LeaveRequestsDO> leaveRequestsList = null;
		try {
		//	leaveRequestsList = genericObject.retrieve(LeaveRequestsDO.class);
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeaveRequestsDO.FIND_ALL)
					.list();
		} catch (Exception eException) {
		} finally {
		}
		return leaveRequestsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeaveRequestsDO> retrieveById(Long Id)  {
		List<LeaveRequestsDO> leaveRequestList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeaveRequestsDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, Id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return leaveRequestList;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeaveRequestsDO> retrieveAllPermissionForEmpForMonth(String empId, int year, int month)  {
		List<LeaveRequestsDO> leaveRequestList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeaveRequestsDO.FIND_ALL_PERMISSION_FORMONTH)
					.setParameter(CommonConstants.ID, empId)
					.setParameter(CommonConstants.YEAR, year)
					.setParameter(CommonConstants.MONTH, month)
					.setParameter(CommonConstants.TYPE, "p")
					.setParameter(CommonConstants.STATUS, 'a')
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return leaveRequestList;
	}
	
	public LeaveRequestsDO update(LeaveRequestsDO leaveRequest)  {
		try {
			genericObject.merge(leaveRequest);
		} catch (Exception eException) {
		} finally {
		}
		return leaveRequest;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeaveRequestsDO> retrieveByEmpId(String id)  {
		List<LeaveRequestsDO> requestList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeaveRequestsDO.FINDBY_EMP_ID)
			.setParameter(CommonConstants.ID, id)
			.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return requestList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<LeaveRequestsDO> retriveByEmpIdStatus(String id)  {
		List<LeaveRequestsDO> requestList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeaveRequestsDO.FINDBY_EMP_ID_STATUS)
			.setParameter(CommonConstants.ID, id)
			.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
			.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return requestList;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeaveRequestsDO> retriveByEmpIdStatusType(String id, Long type)  {
		List<LeaveRequestsDO> requestList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeaveRequestsDO.FINDBY_EMP_ID_STATUS_TYPE)
			.setParameter(CommonConstants.ID, id)
			.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
				.setParameter(CommonConstants.TYPE, type)
			.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return requestList;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeaveRequestsDO> retrieveByReportingTo(String id)  {
		List<LeaveRequestsDO> requestList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeaveRequestsDO.FINDBY_REPORTING_ID)
			.setParameter(CommonConstants.ID, id)
			.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return requestList;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeaveRequestsDO> retrieveAvailableLeavesByEmpId(String id, Date leaveTypeFromDate, Date leaveTypeToDate, Long type)  {
		List<LeaveRequestsDO> requestList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeaveRequestsDO.FIND_LEAVE_BY_EMP_ID)
			.setParameter(CommonConstants.ID, id)
			.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
			.setParameter(CommonConstants.LEAVETYPETODATE, leaveTypeToDate )
			.setParameter(CommonConstants.LEAVETYPEFROMDATE, leaveTypeFromDate)
			.setParameter(CommonConstants.TYPE, type)
			.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return requestList;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeaveRequestsDO> retrieveLeavesByYear(String empId, Date fromDate, Date ToDate, Long leaveTypeId)  {
		List<LeaveRequestsDO> requestList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeaveRequestsDO.FINDFORYEAR)
			.setParameter(CommonConstants.FROMDATE, fromDate )
			.setParameter(CommonConstants.TODATE, ToDate)
			.setParameter(CommonConstants.ID, empId)
			.setParameter(CommonConstants.TYPE, leaveTypeId)
			.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return requestList;
	}
	

	@SuppressWarnings("unchecked")
	public List<LeaveRequestsDO> retrivebyEmpLeaveperiod(String empId, Date fromDate, Date ToDate, Date leaveTypeFrom, Date leaveTypeTo, long grpId, long leaveType)  {
		List<LeaveRequestsDO> requestList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeaveRequestsDO.FIND_EMPREPORT)
			.setParameter(CommonConstants.FROMDATE, fromDate )
			.setParameter(CommonConstants.TODATE, ToDate)
			.setParameter(CommonConstants.ID, empId)
				.setParameter(CommonConstants.LEAVEGROUP, grpId)
			.setParameter(CommonConstants.LEAVETYPEFROMDATE, leaveTypeFrom)
			.setParameter(CommonConstants.LEAVETYPETODATE, leaveTypeTo)
			.setParameter(CommonConstants.TYPE, leaveType)
				.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
			.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return requestList;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeaveRequestsDO> retrieveLeavesByFromDate(String empId, Date fromDate)  {
		List<LeaveRequestsDO> requestList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeaveRequestsDO.FINDBYFROMDATE)
			.setParameter(CommonConstants.FROMDATE, fromDate )
			.setParameter(CommonConstants.ID, empId)
			.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
			.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return requestList;
	}
	
	

	@SuppressWarnings("unchecked")
	public List<LeaveRequestsDO> retrieveLeavesByFromDateWithType(String empId, Date fromDate, Long type)  {
		List<LeaveRequestsDO> requestList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeaveRequestsDO.FIND_FROMDATEWITHTYPE)
			.setParameter(CommonConstants.FROMDATE, fromDate )
			.setParameter(CommonConstants.ID, empId)
			.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
			.setParameter(CommonConstants.TYPE, type)
			.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return requestList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<LeaveRequestsDO> retrieveLeavesByFromToDate(String empId, Date fromDate, Date ToDate)  {
		List<LeaveRequestsDO> requestList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeaveRequestsDO.FINDBYFROMTODATE)
			.setParameter(CommonConstants.FROMDATE, fromDate)
			.setParameter(CommonConstants.TODATE, ToDate)
			.setParameter(CommonConstants.ID, empId)
			.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
			.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return requestList;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeaveRequestsDO> retrieveLeavesByFromToDateType(String empId, Date fromDate, Date ToDate, Long type)  {
		List<LeaveRequestsDO> requestList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeaveRequestsDO.FINDBYFROMTODATETYPE)
			.setParameter(CommonConstants.FROMDATE, fromDate)
			.setParameter(CommonConstants.TODATE, ToDate)
			.setParameter(CommonConstants.ID, empId)
			.setParameter(CommonConstants.TYPE, type)
			.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
			.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return requestList;
	}
	
	
	
	@SuppressWarnings("unchecked")
	public List<LeaveRequestsDO> retrievePermissionByYear(String empId, Date fromDate, Date ToDate, String leaveTypeId)  {
		List<LeaveRequestsDO> requestList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeaveRequestsDO.FIND_PERMISSION)
			.setParameter(CommonConstants.FROMDATE, fromDate )
			.setParameter(CommonConstants.TODATE, ToDate)
			.setParameter(CommonConstants.ID, empId)
			.setParameter(CommonConstants.TYPE, leaveTypeId)
			.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
			.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return requestList;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeaveRequestsDO> retrievePermissionByMonth(String empId, Date fromDate, Date ToDate, String leaveTypeId)  {
		List<LeaveRequestsDO> requestList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeaveRequestsDO.FIND_FINDPERMISSIONMONTHDETAILS)
			.setParameter(CommonConstants.FROMDATE, fromDate )
			.setParameter(CommonConstants.TODATE, ToDate)
			.setParameter(CommonConstants.ID, empId)
			.setParameter(CommonConstants.TYPE, leaveTypeId)
			.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return requestList;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeaveRequestsDO> retrievePermissionByEmpID(String empId, String leaveTypeId)  {
		List<LeaveRequestsDO> requestList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeaveRequestsDO.FIND_PERMISSION_BYEMPID)
			.setParameter(CommonConstants.ID, empId)
			.setParameter(CommonConstants.TYPE, leaveTypeId)
			.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
			.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return requestList;
	}
	
	
	
	@SuppressWarnings("unchecked")
	public List<LeaveRequestsDO> retrieveReportByEmpId(String empId, Long leaveTypeId)  {
		List<LeaveRequestsDO> requestList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeaveRequestsDO.FINDREPORTBY_EMPID)
			.setParameter(CommonConstants.ID, empId)
			.setParameter(CommonConstants.TYPE, leaveTypeId)
			.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return requestList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<LeaveRequestsDO> retrieveLeavesForPayroll(String empId, Date fromDate, Date ToDate)  {
		List<LeaveRequestsDO> requestList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeaveRequestsDO.FIND_FORPAYROLL)
			.setParameter(CommonConstants.FROMDATE, fromDate)
			.setParameter(CommonConstants.TODATE, ToDate)
			.setParameter(CommonConstants.ID, empId)
			.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return requestList;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeaveRequestsDO> empCompoffValidity(String id, Date fromDate, Date toDate, String compOffType)  {
		List<LeaveRequestsDO> requestList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeaveRequestsDO.FIND_EMPCOMPOFFVALIDITY)
			.setParameter(CommonConstants.ID, id)
			.setParameter(CommonConstants.FROMDATE, fromDate )
			.setParameter(CommonConstants.TODATE, toDate)
			.setParameter(CommonConstants.TYPE, compOffType)
			.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return requestList;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeaveRequestsDO> empCompoffValidityWithoutExpiry(String id, Date fromDate, String compOffType)  {
		List<LeaveRequestsDO> requestList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeaveRequestsDO.FIND_EMPCOMPOFFVALIDITYWITHOUTEXPIRY)
			.setParameter(CommonConstants.ID, id)
			.setParameter(CommonConstants.FROMDATE, fromDate )
			.setParameter(CommonConstants.TYPE, compOffType)
			.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return requestList;
	}

	public boolean deleteLeaveRequestByEmpId(Date fromDate, Date toDate, String empId) {
		boolean deleteStatus = true;
		try {
			this.sessionFactory.getCurrentSession().createQuery("delete from LeaveRequestsDO e where e.fromDate =:fromDate and e.toDate =:toDate and e.employee.empId=:id")
										.setParameter(CommonConstants.FROMDATE, fromDate)
										.setParameter(CommonConstants.TODATE, toDate)
										.setParameter(CommonConstants.ID, empId)
										.executeUpdate();
		} catch (Exception eException) {
			deleteStatus = false;
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return deleteStatus;
	}
}
