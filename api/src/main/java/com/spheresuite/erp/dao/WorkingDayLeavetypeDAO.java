package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.WorkingDayLeavetypeDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class WorkingDayLeavetypeDAO {
	static Logger logger = Logger.getLogger(WorkingDayLeavetypeDAO.class.getName());
	
	@Autowired
	private GenericDAOImpl<WorkingDayLeavetypeDO> dao;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public boolean persist(WorkingDayLeavetypeDO WorkingDayLeavetypeDO){
		boolean persistStatus = true;
		try {
			dao.persist(WorkingDayLeavetypeDO);
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	public List<WorkingDayLeavetypeDO> persistList(List<WorkingDayLeavetypeDO> workingLeaveTypeList)  {
		try {
			if(workingLeaveTypeList != null && workingLeaveTypeList.size() > 0){
				org.hibernate.Query query = this.sessionFactory.getCurrentSession().createQuery("delete from WorkingDayLeavetypeDO e where e.workingDayAllowance.workingDayAllowanceId=:id")
					.setParameter(CommonConstants.ID, workingLeaveTypeList.get(0).getWorkingDayAllowance().getWorkingDayAllowanceId());
				query.executeUpdate();
			}
			for (WorkingDayLeavetypeDO workingDayLeavetypeDO : workingLeaveTypeList) {
				dao.persist(workingDayLeavetypeDO);
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return workingLeaveTypeList;
	}
	
	
	
	@SuppressWarnings("unchecked")
	public List<WorkingDayLeavetypeDO> retrieveByWorkingDayId(Long id){
		 List<WorkingDayLeavetypeDO> workingDayAllowanceList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(WorkingDayLeavetypeDO.FIND_BY_WORKING_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return workingDayAllowanceList;
	}
	
	/*@SuppressWarnings("unchecked")
	public List<WorkingDayLeavetypeDO> retrieve(){
		List<WorkingDayLeavetypeDO> workingDayAllowanceList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(WorkingDayLeavetypeDO.FIND_ALL)
					.list();
		} catch (Exception eException) {
		} finally {
		}
		return workingDayAllowanceList;
	}
	
	public List<WorkingDayLeavetypeDO> retrieveActive(){
		List<WorkingDayLeavetypeDO> workingDayAllowanceList = null;
		try {
			workingDayAllowanceList =  dao.retrieveActive(CommonConstants.ACTIVE, WorkingDayLeavetypeDO.FIND_BY_STATUS, WorkingDayLeavetypeDO.class);
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return workingDayAllowanceList;
	}
	
	
	
	public boolean update(WorkingDayLeavetypeDO WorkingDayLeavetypeDO) {
		boolean updateStatus = true;
		try {
			dao.merge(WorkingDayLeavetypeDO);
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}*/
	
}
