package com.spheresuite.erp.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.ESIGroupDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class ESIGroupDAO {
	@Autowired
	private GenericDAOImpl<ESIGroupDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(ESIGroupDO esiGroupDO){
		List<ESIGroupDO> pfList = null;
		boolean persistStatus = true;
		try {
			pfList = this.sessionFactory.getCurrentSession().getNamedQuery(ESIGroupDO.FIND_BY_NAME)
			.setParameter(CommonConstants.NAME, esiGroupDO.getName())
			.list();
			if(pfList != null && pfList.size() > 0){
				persistStatus = false;
			}else{
				genericObject.persist(esiGroupDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	public List<ESIGroupDO> retrieve(){
		List<ESIGroupDO> esiList = null;
		try {
			esiList = genericObject.retrieve(ESIGroupDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return esiList;
	}
	
	public List<ESIGroupDO> retrieveActive(){
		List<ESIGroupDO> esiList = null;
		try {
			return genericObject.retrieveActive(CommonConstants.ACTIVE, ESIGroupDO.FIND_BY_STATUS, ESIGroupDO.class);
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return esiList;
	}
	
	@SuppressWarnings("unchecked")
	public List<ESIGroupDO> retrieveById(Long id) {
		List<ESIGroupDO> esiList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(ESIGroupDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return esiList;
	}
	
	@SuppressWarnings("unchecked")
	public List<ESIGroupDO> retrieveByName(String name) {
		List<ESIGroupDO> esiList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(ESIGroupDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, name)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return esiList;
	}
	
	public boolean persistList(List<ESIGroupDO> esiGroupList){
		try {
			for (ESIGroupDO esiGroup : esiGroupList) {
				genericObject.persist(esiGroup);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}
	
	public boolean update(ESIGroupDO esiGroupDO){
		boolean updateStatus = true;
		try {
			genericObject.merge(esiGroupDO);
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}
	

}
