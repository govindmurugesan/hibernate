package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.EmployeeCertificationDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class EmployeeCertificationDAO {
	static Logger logger = Logger.getLogger(EmployeeCertificationDAO.class.getName());
	@Autowired
	private GenericDAOImpl<EmployeeCertificationDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public EmployeeCertificationDO persist(EmployeeCertificationDO employeeCertificationDO) {
		try {
			genericObject.persist(employeeCertificationDO);
		} catch (Exception eException) {
		} finally {
		}
		return employeeCertificationDO;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeCertificationDO> retrieveByEmpId(String empId) {
		List<EmployeeCertificationDO> employeeCertificationList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeCertificationDO.FIND_BY_EMPID)
					.setParameter(CommonConstants.EMPID, empId)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeCertificationList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeCertificationDO> retrieveById(Long id) {
		List<EmployeeCertificationDO> employeeCertificationList = null;
		
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeCertificationDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeCertificationList;
	}
	
	public EmployeeCertificationDO update(EmployeeCertificationDO employeeCertificationDO) {
		try {
			genericObject.merge(employeeCertificationDO);
		} catch (Exception eException) {
		} finally {
		}
		return employeeCertificationDO;
	}
}
