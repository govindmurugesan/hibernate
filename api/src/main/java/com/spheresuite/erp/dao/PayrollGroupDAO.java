package com.spheresuite.erp.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.PayrollGroupDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class PayrollGroupDAO {
	@Autowired
	private GenericDAOImpl<PayrollGroupDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(PayrollGroupDO payrollGroupDO){
		List<PayrollGroupDO> payrollGroupList = null;
		boolean persistStatus = true;
		try {
			payrollGroupList = this.sessionFactory.getCurrentSession().getNamedQuery(PayrollGroupDO.FIND_BY_NAME)
			.setParameter(CommonConstants.NAME, payrollGroupDO.getName())
			.list();
			if(payrollGroupList != null && payrollGroupList.size() > 0){
				persistStatus = false;
			}else{
				genericObject.persist(payrollGroupDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	public List<PayrollGroupDO> retrieve(){
		List<PayrollGroupDO> payrollGroupList = null;
		try {
			payrollGroupList = genericObject.retrieve(PayrollGroupDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return payrollGroupList;
	}
	
	public List<PayrollGroupDO> retrieveActive(){
		List<PayrollGroupDO> payrollGroupList = null;
		try {
			return genericObject.retrieveActive(CommonConstants.ACTIVE, PayrollGroupDO.FIND_BY_STATUS, PayrollGroupDO.class);
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return payrollGroupList;
	}
	
	@SuppressWarnings("unchecked")
	public List<PayrollGroupDO> retrieveById(Long id) {
		List<PayrollGroupDO> payrollGroupList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(PayrollGroupDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return payrollGroupList;
	}
	
	@SuppressWarnings("unchecked")
	public List<PayrollGroupDO> retrieveByName(String name) {
		List<PayrollGroupDO> payrollGroupList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(PayrollGroupDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, name)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return payrollGroupList;
	}
	
	public boolean persistList(List<PayrollGroupDO> payrollGroupList){
		try {
			for (PayrollGroupDO payrollGroup : payrollGroupList) {
				genericObject.persist(payrollGroup);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}
	
	@SuppressWarnings("unchecked")
	public boolean update(PayrollGroupDO payrollGroupDO){
		boolean updateStatus = true;
		try {
			List<PayrollGroupDO> payrollGroupList = null;
			payrollGroupList = this.sessionFactory.getCurrentSession().getNamedQuery(PayrollGroupDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, payrollGroupDO.getName())
					.setParameter(CommonConstants.ID, payrollGroupDO.getPayrollGroupId())
					.list();
			if(payrollGroupList != null && payrollGroupList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(payrollGroupDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}

}
