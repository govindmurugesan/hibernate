package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.AdvancePaymentDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class AdvancePaymentDAO {
	static Logger logger = Logger.getLogger(AdvancePaymentDAO.class.getName());
	@Autowired
	private GenericDAOImpl<AdvancePaymentDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public AdvancePaymentDO persist(AdvancePaymentDO advancePaymentDO) {
		try {
			genericObject.persist(advancePaymentDO);
		} catch (Exception eException) {
		} finally {
		}
		return advancePaymentDO;
	}
	
	@SuppressWarnings("unchecked")
	public List<AdvancePaymentDO> retrieveActiveByEmpId(String empId) {
		List<AdvancePaymentDO> employeeAdvanceList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(AdvancePaymentDO.FIND_ACTIVE_BY_EMPID)
					.setParameter(CommonConstants.EMPID, empId)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVESTRING)
					.list(); 
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeAdvanceList;
	}
	
	/*@SuppressWarnings("unchecked")
	public List<AdvancePaymentDO> retrieveByEmpIdWithDate(List<Long> empIds, String bonusMonth) {
		List<AdvancePaymentDO> employeeBonusList = null;
		try {
			if(empIds != null && empIds.size() > 0){
				return this.sessionFactory.getCurrentSession().getNamedQuery(AdvancePaymentDO.FIND_BY_EMPID_MONTH)
					.setParameter(CommonConstants.EMPID, empIds)
					.setParameter(CommonConstants.STATUS, 'a')
					.setParameter(CommonConstants.MONTHLY, bonusMonth)
					.list(); 
			}else{
				return this.sessionFactory.getCurrentSession().getNamedQuery(AdvancePaymentDO.FIND_BY_MONTH)
					.setParameter(CommonConstants.STATUS, 'a')
					.setParameter(CommonConstants.MONTHLY, bonusMonth)
					.list();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeBonusList;
	}*/
	
	/*@SuppressWarnings("unchecked")
	public List<AdvancePaymentDO> retrieveByEmpIdBetweenDate(List<Long> empIds, String fromMonth, String toMonth) {
		List<AdvancePaymentDO> employeeBonusList = null;
		try {
				if(empIds != null && empIds.size() > 0){
					return this.sessionFactory.getCurrentSession().getNamedQuery(AdvancePaymentDO.FIND_BY_EMPID_BETWEENMONTH)
						.setParameter(CommonConstants.EMPID, empIds)
						.setParameter(CommonConstants.STATUS, 'a')
						.setParameter("fromMonth", fromMonth)
						.setParameter("toMonth", toMonth)
						.list();
				}else{
					return this.sessionFactory.getCurrentSession().getNamedQuery(AdvancePaymentDO.FIND_BETWEENMONTH)
					.setParameter(CommonConstants.STATUS, 'a')
					.setParameter("fromMonth", fromMonth)
					.setParameter("toMonth", toMonth)
					.list();
				}
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeBonusList;
	}
	
	@SuppressWarnings("unchecked")
	public List<AdvancePaymentDO> retrieveByEmpIdAndMonth(String empId, String month) {
		List<AdvancePaymentDO> employeeBonusList = null;
		try {
				return this.sessionFactory.getCurrentSession().getNamedQuery(AdvancePaymentDO.FIND_BY_EMPID_DATE)
					.setParameter(CommonConstants.EMPID, empId)
					.setParameter(CommonConstants.MONTHLY, month)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
				
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeBonusList;
	}
	
	@SuppressWarnings("rawtypes")
	public Long retrieveByEmpIdBetweenMonth(String empId, String fromMonth, String toMonth) {
		Long employeeBonusList = null;
		try {
				List longList =  this.sessionFactory.getCurrentSession().getNamedQuery(AdvancePaymentDO.FIND_BY_EMPID_BETWEEN_DATE)
						.setParameter(CommonConstants.EMPID, empId)
						.setParameter("fromMonth", fromMonth)
						.setParameter("toMonth", toMonth)
						.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
						.list();
				
				if(longList !=null && longList.size() > 0){
					employeeBonusList = (Long) longList.get(0);
				}
				
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeBonusList;
	}
	
	@SuppressWarnings("unchecked")
	public List<AdvancePaymentDO> retrieveByEmpIdAndMonthWithOutStatus(Long empId, String month) {
		List<AdvancePaymentDO> employeeBonusList = null;
		try {
			
			return this.sessionFactory.getCurrentSession().getNamedQuery(AdvancePaymentDO.FIND_BY_EMPID_DATE)
					.setParameter(CommonConstants.EMPID, empId)
					.setParameter(CommonConstants.MONTHLY, month)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeBonusList;
	}*/
	
	@SuppressWarnings("unchecked")
	public List<AdvancePaymentDO> retrieve() {
		List<AdvancePaymentDO> advancePaymentList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(AdvancePaymentDO.FIND_ALL)
					.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
		}
		return advancePaymentList;
	}
	
	@SuppressWarnings("unchecked")
	public List<AdvancePaymentDO> retrieveById(Long id) {
		List<AdvancePaymentDO> advancePaymentList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(AdvancePaymentDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return advancePaymentList;
	}
	
	public AdvancePaymentDO update(AdvancePaymentDO advancePaymentDO) {
		try {
			genericObject.merge(advancePaymentDO);
		} catch (Exception eException) {
		} finally {
		}
		return advancePaymentDO;
	}
	
	/*public boolean delete(Long id)  {
		try {
			org.hibernate.Query query = this.sessionFactory.getCurrentSession().createQuery("delete from AdvancePaymentDO e where e.empBonusId=:id")
					.setParameter(CommonConstants.ID, id);
			query.executeUpdate();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return true;
	}*/
	

}
