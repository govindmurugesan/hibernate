package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.EmailSettingsDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class EmailSettingsDAO {
	static Logger logger = Logger.getLogger(EmailSettingsDAO.class.getName());
	
	@Autowired
	private GenericDAOImpl<EmailSettingsDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(EmailSettingsDO emailSettingsDO) {
		boolean persistStatus = true;
		try {
			List<EmailSettingsDO> emailSettingsDOList = null;
			emailSettingsDOList = this.sessionFactory.getCurrentSession().getNamedQuery(EmailSettingsDO.FIND_BY_NAME)
					.setParameter(CommonConstants.MAIL_TYPE, emailSettingsDO.getMailtype())
					.list();
					if(emailSettingsDOList != null && emailSettingsDOList.size() > 0){
						persistStatus = false;
					}else{
						genericObject.persist(emailSettingsDO);
					}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	
	public List<EmailSettingsDO> retrieve() {
		List<EmailSettingsDO> emailSettingsList = null;
		try {
			emailSettingsList = genericObject.retrieve(EmailSettingsDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return emailSettingsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmailSettingsDO> retrieveActive() {
		List<EmailSettingsDO> EmailSettingsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmailSettingsDO.FIND_BY_STATUS)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return EmailSettingsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmailSettingsDO> retrieveById(Long Id) {
		List<EmailSettingsDO> EmailSettingsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmailSettingsDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, Id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return EmailSettingsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmailSettingsDO> retrieveByMailType(String mailtype) {
		List<EmailSettingsDO> EmailSettingsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmailSettingsDO.FIND_BY_MAILTYPE)
					.setParameter(CommonConstants.MAILTYPE, mailtype)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list(); 
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return EmailSettingsList;
	}
	
	@SuppressWarnings("unchecked")
	public boolean update(EmailSettingsDO emailSettingsDO) {
		try {
			List<EmailSettingsDO> emailSettingsDOList = null;
			emailSettingsDOList = this.sessionFactory.getCurrentSession().getNamedQuery(EmailSettingsDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.MAIL_TYPE, emailSettingsDO.getMailtype())
					.setParameter(CommonConstants.ID, emailSettingsDO.getEmailSettingId())
					.list();
			if(emailSettingsDOList != null && emailSettingsDOList.size() > 0){
				return false;
			}else{
				genericObject.merge(emailSettingsDO);
				return true;
			}
		} catch (Exception eException) {
			return true;
		} finally {
		}
	}
	
	public boolean persistList(List<EmailSettingsDO> emailSettingsList){
		try {
			for (EmailSettingsDO emailSettings : emailSettingsList) {
				genericObject.persist(emailSettings);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}
}
