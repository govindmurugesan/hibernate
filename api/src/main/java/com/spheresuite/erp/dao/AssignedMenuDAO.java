package com.spheresuite.erp.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.AssignedMenuDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class AssignedMenuDAO {
	
	@Autowired
	private GenericDAOImpl<AssignedMenuDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;

		
	public AssignedMenuDO persist(AssignedMenuDO assignedMenu){
		try {
			genericObject.persist(assignedMenu);
		} catch (Exception eException) {
		} finally {
		}
		return assignedMenu;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<AssignedMenuDO> retrieveByRoleId(Long roleId) { 
		try{
			return this.sessionFactory.getCurrentSession().getNamedQuery(AssignedMenuDO.FIND_BY_ROLE_ID)
					.setParameter(CommonConstants.ROLE_ID, roleId)
					.list(); 
		}catch(Exception eException){
			
		}finally{
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return null;
		
	}
	
	
	
	@SuppressWarnings("unchecked")
	public List<AssignedMenuDO> retrieveById(Long id){
		List<AssignedMenuDO> assignedMenuList = null;
		
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(AssignedMenuDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return assignedMenuList;
	}
	
	public List<AssignedMenuDO> update(List<AssignedMenuDO> assignedMenuList){
		try {
			org.hibernate.Query query = this.sessionFactory.getCurrentSession().createQuery("delete from AssignedMenuDO e where e.role.roleId=:id")
					.setParameter(CommonConstants.ID, assignedMenuList.get(0).getRole().getRoleId());
			query.executeUpdate();
				for (AssignedMenuDO assignedMenuDO : assignedMenuList) {
					genericObject.persist(assignedMenuDO);
				}
				//TransactionManager.commitTransaction();
			//}
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return assignedMenuList;
	}
	
	@SuppressWarnings("unchecked")
	public List<AssignedMenuDO> retrieveProductByRoleId(Long id){
		List<AssignedMenuDO> assignedMenuList = null;
		
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(AssignedMenuDO.FIND_BY_PRODUCTID_ROLEID)
					.setParameter(CommonConstants.ID, Long.parseLong("1"))
					.setParameter(CommonConstants.ROLE_ID, id)
					.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return assignedMenuList;
	}
	
	@SuppressWarnings("unchecked")
	public List<AssignedMenuDO> retrieveMenuByRoleId(Long roleId, Long productId){
		List<AssignedMenuDO> assignedMenuList = null;
		
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(AssignedMenuDO.FIND_BY_PRODUCTID_ROLEID)
					.setParameter(CommonConstants.ID, productId)
					.setParameter(CommonConstants.ROLE_ID, roleId)
					.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return assignedMenuList;
	}
	
	@SuppressWarnings("unchecked")
	public List<AssignedMenuDO> retrieveMenuBySupermenuRoleId(Long roleId, Long productId){
		List<AssignedMenuDO> assignedMenuList = null;
		
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(AssignedMenuDO.FIND_BY_SUPERMENU_ROLEID)
					.setParameter(CommonConstants.ID, productId)
					.setParameter(CommonConstants.ROLE_ID, roleId)
					.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return assignedMenuList;
	}
	
	
	
	
}	
