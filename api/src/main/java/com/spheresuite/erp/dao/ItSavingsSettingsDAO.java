package com.spheresuite.erp.dao;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.ItSavingsSettingsDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class ItSavingsSettingsDAO {
	static Logger logger = Logger.getLogger(ItSavingsSettingsDAO.class.getName());
	@Autowired
	private GenericDAOImpl<ItSavingsSettingsDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(ItSavingsSettingsDO itSavingsSettingsDO)  {
		boolean persistStatus = true;
		try {
			List<ItSavingsSettingsDO> itSavingsList = null;
			itSavingsList = this.sessionFactory.getCurrentSession().getNamedQuery(ItSavingsSettingsDO.FIND_BY_DUPLICATE_PERSIST)
					.setParameter(CommonConstants.FROMDATE, itSavingsSettingsDO.getFromYear())
					.setParameter(CommonConstants.TODATE, itSavingsSettingsDO.getToYear())
					.setParameter(CommonConstants.SECTION, itSavingsSettingsDO.getItSection().getItSectionId())
					.setParameter(CommonConstants.NAME, itSavingsSettingsDO.getName())
					.list();
			if(itSavingsList != null && itSavingsList.size() > 0){
				persistStatus = false;
			}else{
				genericObject.persist(itSavingsSettingsDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	public List<ItSavingsSettingsDO> retrieve()  {
		List<ItSavingsSettingsDO> itSavingsSettingsList = null;
		try {
			itSavingsSettingsList = genericObject.retrieve(ItSavingsSettingsDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return itSavingsSettingsList;
	}
	
	public List<ItSavingsSettingsDO> retrieveActive()  {
		List<ItSavingsSettingsDO> itSavingsSettingsList = null;
		try {
			return genericObject.retrieveActive(CommonConstants.ACTIVE, 
					ItSavingsSettingsDO.FIND_BY_STATUS, ItSavingsSettingsDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return itSavingsSettingsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<ItSavingsSettingsDO> retrieveById(Long id)  {
		List<ItSavingsSettingsDO> itSavingsSettingsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(ItSavingsSettingsDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return itSavingsSettingsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<ItSavingsSettingsDO> retrieveByDate(Date fromDate, Date toDate)  {
		List<ItSavingsSettingsDO> itSavingsSettingsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(ItSavingsSettingsDO.FIND_BY_DATE)
			.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
			.setParameter(CommonConstants.FROMDATE, fromDate)
			.setParameter(CommonConstants.TODATE, toDate)
			.list();
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return itSavingsSettingsList;
	}
	
	@SuppressWarnings("unchecked")
	public boolean update(ItSavingsSettingsDO itSavingsSettingsDO)  {
		boolean updateStatus = true;
		try {
			List<ItSavingsSettingsDO> itSavingsList = null;
			itSavingsList = this.sessionFactory.getCurrentSession().getNamedQuery(ItSavingsSettingsDO.FIND_BY_DUPLICATE_UPDATE)
					.setParameter(CommonConstants.FROMDATE, itSavingsSettingsDO.getFromYear())
					.setParameter(CommonConstants.TODATE, itSavingsSettingsDO.getToYear())
					.setParameter(CommonConstants.SECTION, itSavingsSettingsDO.getItSection().getItSectionId())
					.setParameter(CommonConstants.NAME, itSavingsSettingsDO.getName())
					.setParameter(CommonConstants.ID, itSavingsSettingsDO.getItSavingsSettingsId())
					.list();
			if(itSavingsList != null && itSavingsList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(itSavingsSettingsDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}
	
	public boolean persistList(List<ItSavingsSettingsDO> itSavingsSettingsList){
		try {
			for (ItSavingsSettingsDO itSavingsSettings : itSavingsSettingsList) {
				genericObject.persist(itSavingsSettings);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}
}
