package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.EmployeeTerminateDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class EmployeeTerminateDAO {
	static Logger logger = Logger.getLogger(EmployeeTerminateDAO.class.getName());
	@Autowired
	private GenericDAOImpl<EmployeeTerminateDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public EmployeeTerminateDO persist(EmployeeTerminateDO employeeTerminateDO)  {
		try {
			genericObject.persist(employeeTerminateDO);
		} catch (Exception eException) {
		} finally {
		}
		return employeeTerminateDO;
	}
	
	
	public List<EmployeeTerminateDO> retrieve()  {
		List<EmployeeTerminateDO> employeeTerminateList = null;
		try {
			employeeTerminateList = genericObject.retrieve(EmployeeTerminateDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return employeeTerminateList;
	}
	
	public List<EmployeeTerminateDO> delete(Long id)  {
		List<EmployeeTerminateDO> employeeTerminateList = null;
		try {
			org.hibernate.Query query = this.sessionFactory.getCurrentSession().createQuery("delete from EmployeeTerminateDO e where e.empTerminateId=:id")
					.setParameter(CommonConstants.ID, id);
			query.executeUpdate();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeTerminateList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeTerminateDO> retrieveByEmpId(String empId)  {
		List<EmployeeTerminateDO> employeeTerminateList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeTerminateDO.FIND_BY_EMPID)
			.setParameter(CommonConstants.EMPID, empId)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeTerminateList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeTerminateDO> retrieveById(Long id)  {
		List<EmployeeTerminateDO> employeeTerminateList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeTerminateDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeTerminateList;
	}
	
	public EmployeeTerminateDO update(EmployeeTerminateDO employeeTerminateDO)  {
		try {
			genericObject.merge(employeeTerminateDO);
		} catch (Exception eException) {
		} finally {
		}
		return employeeTerminateDO;
	}

}
