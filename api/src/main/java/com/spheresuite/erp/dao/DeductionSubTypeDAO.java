package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.DeductionSubTypeDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class DeductionSubTypeDAO {
	static Logger logger = Logger.getLogger(DeductionSubTypeDAO.class.getName());
	
	@Autowired
	private GenericDAOImpl<DeductionSubTypeDO> dao;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(DeductionSubTypeDO loanTypeDO){
		boolean persistStatus = true;
		try {
			List<DeductionSubTypeDO> LoanTypeDOList = null;
			LoanTypeDOList = this.sessionFactory.getCurrentSession().getNamedQuery(DeductionSubTypeDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, loanTypeDO.getName())
					.list();
					if(LoanTypeDOList != null && LoanTypeDOList.size() > 0){
						persistStatus =  false;
					}else{
						dao.persist(loanTypeDO);
					}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	public List<DeductionSubTypeDO> retrieve(){
		List<DeductionSubTypeDO> loanTypeDOList = null;
		try {
			loanTypeDOList = dao.retrieve(DeductionSubTypeDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return loanTypeDOList;
	}
	
	/*public List<LoanTypeDO> retrieveActive(){
		List<LoanTypeDO> addressTypeList = null;
		try {
			addressTypeList =  dao.retrieveActive(CommonConstants.ACTIVE, LoanTypeDO.FIND_BY_STATUS, LoanTypeDO.class);
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return addressTypeList;
	}*/
	
	@SuppressWarnings("unchecked")
	public List<DeductionSubTypeDO> retrieveById(Long id){
		 List<DeductionSubTypeDO> loanTypeList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(DeductionSubTypeDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return loanTypeList;
	}
	
	@SuppressWarnings("unchecked")
	public List<DeductionSubTypeDO> retrieveByName(String name){
		 List<DeductionSubTypeDO> loanTypeList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(DeductionSubTypeDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, name)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return loanTypeList;
	}
	
	@SuppressWarnings("unchecked")
	public List<DeductionSubTypeDO> retrieveByDeductionTypeId(Long id){
		 List<DeductionSubTypeDO> loanTypeList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(DeductionSubTypeDO.FIND_BY_DEDUCTIONTYPEID)
					.setParameter(CommonConstants.ID, id)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVESTRING)
					.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return loanTypeList;
	}
	
	@SuppressWarnings("unchecked")
	public boolean update(DeductionSubTypeDO loanTypeDO) {
		boolean updateStatus = true;
		try {
			List<DeductionSubTypeDO> LoanTypeDOList = null;
			LoanTypeDOList = this.sessionFactory.getCurrentSession().getNamedQuery(DeductionSubTypeDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, loanTypeDO.getName())
					.setParameter(CommonConstants.ID, loanTypeDO.getDeductionSubTypeId())
					.list();
			if(LoanTypeDOList != null && LoanTypeDOList.size() > 0){
				updateStatus = false;
			}else{
				dao.merge(loanTypeDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}
	
	/*public boolean persistList(List<LoanTypeDO> addressTypeList){
		try {
			for (LoanTypeDO addressType : addressTypeList) {
				dao.persist(addressType);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}*/

}
