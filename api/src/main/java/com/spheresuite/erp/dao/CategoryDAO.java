package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.CategoryDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class CategoryDAO {
	static Logger logger = Logger.getLogger(CategoryDAO.class.getName());
	@Autowired
	private GenericDAOImpl<CategoryDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(CategoryDO categoryDO) {
		boolean persistStatus = true;
		List<CategoryDO> categoryList = null;
		try {
			categoryList = this.sessionFactory.getCurrentSession().getNamedQuery(CategoryDO.FIND_BY_NAME)
			.setParameter(CommonConstants.NAME, categoryDO.getName())
			.list();
			if(categoryList != null && categoryList.size() > 0){
				persistStatus = false;
			}else{
				genericObject.persist(categoryDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return  persistStatus;
	}
	
	public List<CategoryDO> retrieve() {
		List<CategoryDO> categoryList = null;
		try {
			categoryList = genericObject.retrieve(CategoryDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return categoryList;
	}
	
	public List<CategoryDO> retrieveActive() {
		List<CategoryDO> categoryList = null;
		try {
			return genericObject.retrieveActive(CommonConstants.ACTIVE,CategoryDO.FIND_BY_STATUS, CategoryDO.class);
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return categoryList;
	}
	
	@SuppressWarnings("unchecked")
	public List<CategoryDO> retrieveById(Long id) {
		List<CategoryDO> categoryList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(CategoryDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return categoryList;
	}
	
	@SuppressWarnings("unchecked")
	public boolean update(CategoryDO categoryDO) {
		boolean updateStatus = true;
		try {
			List<CategoryDO> categoryList = null;
			categoryList = this.sessionFactory.getCurrentSession().getNamedQuery(CategoryDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, categoryDO.getName())
					.setParameter(CommonConstants.ID, categoryDO.getCategoryId())
					.list();
			if(categoryList != null && categoryList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(categoryDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}
	
	public boolean persistList(List<CategoryDO> categoryDOList){
		try {
			for (CategoryDO categoryDO : categoryDOList) {
				genericObject.persist(categoryDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}

}
