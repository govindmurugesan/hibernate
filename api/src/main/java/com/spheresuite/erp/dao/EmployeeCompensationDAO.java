package com.spheresuite.erp.dao;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.EmployeeCompensationDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class EmployeeCompensationDAO {
	static Logger logger = Logger.getLogger(EmployeeCompensationDAO.class.getName());
	@Autowired
	private GenericDAOImpl<EmployeeCompensationDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;

	
	public EmployeeCompensationDO persist(EmployeeCompensationDO EmloyeeCompensationDO) {
		try {
			genericObject.persist(EmloyeeCompensationDO);
		} catch (Exception eException) {
		} finally {
		}
		return EmloyeeCompensationDO;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeCompensationDO> retrieveByEmpIdAndStatus(String empId){
		List<EmployeeCompensationDO> empCompensationList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeCompensationDO.FIND_BY_EMPID_STATUS)
					.setParameter(CommonConstants.EMPID, empId)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return empCompensationList;
	}
	
	
	
	public EmployeeCompensationDO update(EmployeeCompensationDO EmloyeeCompensationDO) {
		try {
			genericObject.merge(EmloyeeCompensationDO);
		} catch (Exception eException) {
		} finally {
		}
		return EmloyeeCompensationDO;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeCompensationDO> retrieveActive(){
		List<EmployeeCompensationDO> empCompensationList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeCompensationDO.FIND_BY_STATUS)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return empCompensationList;
	} 
	
	@SuppressWarnings("unchecked")
	public List<EmployeeCompensationDO> retrieveById(Long id) {
		List<EmployeeCompensationDO> empCompensationList = null;
		
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeCompensationDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return empCompensationList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeCompensationDO> retrieveWithComponsesationDateAndEmpID(String empId, Date month) {
		List<EmployeeCompensationDO> empCompensationList = null;
		
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeCompensationDO.FIND_BY_EMPID_MONTH)
					.setParameter(CommonConstants.ID, empId)
					.setParameter(CommonConstants.DATE, month)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return empCompensationList;
	}
	
	public boolean persistList(List<EmployeeCompensationDO> compensationList){
		try {
			for (EmployeeCompensationDO compensation : compensationList) {
				genericObject.persist(compensation);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}
	
	/*@SuppressWarnings("unchecked")
	public List<EmployeeCompensationDO> retrieveWithComponsesationDateAndPayrollBatch(Long BatchId, Date month) {
		List<EmployeeCompensationDO> empCompensationList = null;
		
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeCompensationDO.FIND_FOR_PAYROLL)
					.setParameter(CommonConstants.ID, BatchId)
					.setParameter(CommonConstants.DATE, month)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return empCompensationList;
	}*/
	
	/*public List<EmployeeCompensationDO> retrieve(){
		List<EmployeeCompensationDO> empCompensationList = null;
		try {
			empCompensationList = genericObject.retrieve(EmployeeCompensationDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return empCompensationList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeCompensationDO> retrieveActive(Long empId){
		List<EmployeeCompensationDO> empCompensationList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeCompensationDO.FIND_BY_STATUS)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.setParameter(CommonConstants.EMPID, empId)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return empCompensationList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeCompensationDO> retrieveById(Long id) {
		List<EmployeeCompensationDO> empCompensationList = null;
		
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeCompensationDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return empCompensationList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeCompensationDO> retrieveByEmpId(String empId){
		List<EmployeeCompensationDO> empCompensationList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeCompensationDO.FIND_BY_EMP_ID)
					.setParameter(CommonConstants.EMPID, empId)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return empCompensationList;
	}
	
	public EmployeeCompensationDO update(EmployeeCompensationDO EmloyeeCompensationDO) {
		try {
			genericObject.merge(EmloyeeCompensationDO);
		} catch (Exception eException) {
		} finally {
		}
		return EmloyeeCompensationDO;
	}*/

}
