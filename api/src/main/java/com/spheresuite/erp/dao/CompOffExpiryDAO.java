package com.spheresuite.erp.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.CompOffExpiryDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class CompOffExpiryDAO {
	@Autowired
	private GenericDAOImpl<CompOffExpiryDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public boolean persist(CompOffExpiryDO earningTypeDO){
		try {
				genericObject.persist(earningTypeDO);
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}
	
	public List<CompOffExpiryDO> retrieve(){
		List<CompOffExpiryDO> earningTypeList = null;
		try {
			earningTypeList = genericObject.retrieve(CompOffExpiryDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return earningTypeList;
	}
	
	@SuppressWarnings("unchecked")
	public List<CompOffExpiryDO> retrieveById(Long id) {
		List<CompOffExpiryDO> earningTypeList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(CompOffExpiryDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return earningTypeList;
	}
	
	public boolean update(CompOffExpiryDO earningTypeDO){
		try {
				genericObject.merge(earningTypeDO);
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}
}
