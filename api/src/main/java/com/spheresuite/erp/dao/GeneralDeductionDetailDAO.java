package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.GeneralDeductionDetailDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class GeneralDeductionDetailDAO {
	static Logger logger = Logger.getLogger(GeneralDeductionDetailDAO.class.getName());
	@Autowired
	private GenericDAOImpl<GeneralDeductionDetailDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public GeneralDeductionDetailDO persist(GeneralDeductionDetailDO loanPaymentDetailDO) {
		try {
			genericObject.persist(loanPaymentDetailDO);
		} catch (Exception eException) {
		} finally {
		}
		return loanPaymentDetailDO;
	}
	
	public List<GeneralDeductionDetailDO> retrieve(){
		List<GeneralDeductionDetailDO> deductionDetailList = null;
		try {
			deductionDetailList = genericObject.retrieve(GeneralDeductionDetailDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return deductionDetailList;
	}
	
	@SuppressWarnings("unchecked")
	public List<GeneralDeductionDetailDO> getByAdvanceId(Long id) {
		List<GeneralDeductionDetailDO> loanPaymentDetailList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(GeneralDeductionDetailDO.FIND_BY_LOAN_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return loanPaymentDetailList;
	}
	
	@SuppressWarnings("unchecked")
	public List<GeneralDeductionDetailDO> getById(Long id) {
		List<GeneralDeductionDetailDO> loanPaymentDetailList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(GeneralDeductionDetailDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return loanPaymentDetailList;
	}
	
	
	
	@SuppressWarnings("unchecked")
	public List<GeneralDeductionDetailDO> getByMonthAndEmpId(String empId, String month) {
		List<GeneralDeductionDetailDO> loanPaymentDetailList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(GeneralDeductionDetailDO.FIND_BY_EMPID_MONTH)
					.setParameter(CommonConstants.EMPID, empId)
					.setParameter(CommonConstants.MONTH, month)
					.setParameter(CommonConstants.STATUS,CommonConstants.ACTIVESTRING)
					.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return loanPaymentDetailList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<GeneralDeductionDetailDO> getByStatusAndEmpId(String empId, String month) {
		List<GeneralDeductionDetailDO> loanPaymentDetailList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(GeneralDeductionDetailDO.FIND_BY_EMPID_MONTH)
					.setParameter(CommonConstants.EMPID, empId)
					.setParameter(CommonConstants.MONTH, month)
					.setParameter(CommonConstants.STATUS,CommonConstants.SSTATUS)
					.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return loanPaymentDetailList;
	}
	
	public GeneralDeductionDetailDO update(GeneralDeductionDetailDO LoanPaymentDetailDO) {
		try {
			genericObject.merge(LoanPaymentDetailDO);
		} catch (Exception eException) {
		} finally {
		}
		return LoanPaymentDetailDO;
	}
	

}
