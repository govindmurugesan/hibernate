package com.spheresuite.erp.dao;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.EmployeeEncashLeaveDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class EmployeeEncashLeaveDAO {
	static Logger logger = Logger.getLogger(EmployeeEncashLeaveDAO.class.getName());
	@Autowired
	private GenericDAOImpl<EmployeeEncashLeaveDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(EmployeeEncashLeaveDO leavegroupDO) {
		boolean persistStatus = true;
		try {
			List<EmployeeEncashLeaveDO> employeeLeavegrpList =  this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeEncashLeaveDO.FIND_BY_EMP_STATUS)
						.setParameter(CommonConstants.EMPID, leavegroupDO.getEmployee().getEmpId())
						.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
						.list(); 
			if(employeeLeavegrpList != null && employeeLeavegrpList.size() > 0){
				EmployeeEncashLeaveDO leavegroupDOstatus = employeeLeavegrpList.get(0);
				leavegroupDOstatus.setStatus('i');
				genericObject.merge(leavegroupDOstatus);
			}
			leavegroupDO.setEncashleaveid(null);
			genericObject.persist(leavegroupDO);
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
		}
		return persistStatus;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeEncashLeaveDO> retrieveByEmpId(String empId) {
		List<EmployeeEncashLeaveDO> employeeLeavegrpList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeEncashLeaveDO.FIND_BY_EMP_STATUS)
					.setParameter(CommonConstants.EMPID, empId)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list(); 
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeLeavegrpList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> retrieveAvailableLeavesByEmpId(String id, Date encashLeaveFromDate, Date encashLeaveToDate)  {
		List<Object[]> requestList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeEncashLeaveDO.FIND_LEAVE_BY_EMP_ID)
			.setParameter(CommonConstants.ID, id)
			.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
			.setParameter(CommonConstants.FROMDATE, encashLeaveFromDate )
			.setParameter(CommonConstants.TODATE, encashLeaveToDate)
			.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return requestList;
	}
	
	/*public EmployeeEncashLeaveDO update(EmployeeEncashLeaveDO employeeLeavegrp) {
		try {
			genericObject.merge(employeeLeavegrp);
		} catch (Exception eException) {
		} finally {
		}
		return employeeLeavegrp;
	}*/

}
