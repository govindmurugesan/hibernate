package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.ItSavingsDocDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class ItSavingDocDAO {
	static Logger logger = Logger.getLogger(ItSavingDocDAO.class.getName());
	@Autowired
	private GenericDAOImpl<ItSavingsDocDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public boolean delete(Long id)  {
		try {
			org.hibernate.Query query = this.sessionFactory.getCurrentSession().createQuery("delete from ItSavingsDocDO e where e.itsavingId=:id")
					.setParameter(CommonConstants.ID, id);
			query.executeUpdate();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return true;
	}
	
	
	public ItSavingsDocDO persist(ItSavingsDocDO itsavingdoc)  {
		try {
			genericObject.persist(itsavingdoc);
		} catch (Exception eException) {
		} finally {
		}
		return itsavingdoc;
	}
	
	
	public List<ItSavingsDocDO> retrieve()  {
		List<ItSavingsDocDO> OpportunitiesDocList = null;
		try {
			OpportunitiesDocList = genericObject.retrieve(ItSavingsDocDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return OpportunitiesDocList;
	}
	
	@SuppressWarnings("unchecked")
	public List<ItSavingsDocDO> retrieveByOppId(Long id)  {
		List<ItSavingsDocDO> OpportunitiesDocList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(ItSavingsDocDO.FIND_BY_ITSAVING_ID)
				.setParameter(CommonConstants.ID, id)
				.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return OpportunitiesDocList;
	}
	
	public ItSavingsDocDO update(ItSavingsDocDO itsavingdoc)  {
		try {
			genericObject.merge(itsavingdoc);
		} catch (Exception eException) {
		} finally {
		}
		return itsavingdoc;
	}

}
