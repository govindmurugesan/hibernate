package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.EarnedLeavesDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class EarnedLeavesDAO {
	static Logger logger = Logger.getLogger(EarnedLeavesDAO.class.getName());
	@Autowired
	private GenericDAOImpl<EarnedLeavesDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	/*public boolean persist(EarnedLeavesDO employeeDO) {
		boolean persistStatus = true;
		try {
			genericObject.persist(employeeDO);
			List<EmployeePFAndESIInfoDO> empList = null;
			empList = this.sessionFactory.getCurrentSession().getNamedQuery(EmployeePFAndESIInfoDO.FIND_BY_ACCOUNTID)
					.setParameter(CommonConstants.PFACCOUNTNO, employeeDO.getPFAccNo())
					.setParameter(CommonConstants.ESIACCOUNTNO, employeeDO.getESIAccNo())
					.list();
					if(empList != null && empList.size() > 0){
						persistStatus = false;
					}else{
						genericObject.persist(employeeDO);
					}
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
		}
		return persistStatus;
	}
	
	
	
	@SuppressWarnings("unchecked")
	public List<EarnedLeavesDO> retrieveById(Long id) {
		List<EarnedLeavesDO> employeeInformationList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EarnedLeavesDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeInformationList;
	}
	
	public EarnedLeavesDO update(EarnedLeavesDO employeeInfo) {
		try {
			genericObject.merge(employeeInfo);
		} catch (Exception eException) {
		} finally {
		}
		return employeeInfo;
	}*/
	
	@SuppressWarnings("unchecked")
	public List<EarnedLeavesDO> retrieveByEmpId(String empId) {
		List<EarnedLeavesDO> employeeInformationList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EarnedLeavesDO.FIND_BY_EMPID)
					.setParameter(CommonConstants.EMPID, empId)
					.list(); 
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return employeeInformationList;
	}
	
	public List<EarnedLeavesDO> retrieve() {
		List<EarnedLeavesDO> earnedLeaveList = null;
		try {
			earnedLeaveList = genericObject.retrieve(EarnedLeavesDO.class);
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return earnedLeaveList;
	}
	
	
	public boolean persistList(List<EarnedLeavesDO> leaveDetailDO){
		try {
			for (EarnedLeavesDO leaveDetail : leaveDetailDO) {
				genericObject.persist(leaveDetail);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}

}
