package com.spheresuite.erp.dao;

import java.util.List;

public interface GenericDAO<T> {

	public Object persist(final T obj);
	public void remove(final T obj);
	//public T findById(final Long id, Class<?> obj);
	public T merge(final T obj);
	public List<T> retrieve(Class<T> type);
	public List<T> retrieveActive(char status, String namedQuery, Class<T> type);
	public List<T> retrieveActiveByName(char status,String name, String namedQuery, Class<T> type);
	public List<T> retrieveByCountryId(Long countryId, String namedQuery, Class<T> type);
	public List<T> retrieveAllByDept(List<Long> ids, String namedQuery, Class<T> type);
}
