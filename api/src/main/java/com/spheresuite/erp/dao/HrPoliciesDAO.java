package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.HrPoliciesDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class HrPoliciesDAO {
	static Logger logger = Logger.getLogger(HrPoliciesDAO.class.getName());
	@Autowired
	private GenericDAOImpl<HrPoliciesDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public HrPoliciesDO persist(HrPoliciesDO hrPoliciesDOList)  {
		try {
			genericObject.persist(hrPoliciesDOList);
		} catch (Exception eException) {
		} finally {
		}
		return hrPoliciesDOList;
	}
	@SuppressWarnings("unchecked")
	public List<HrPoliciesDO> retrieve() {
		List<HrPoliciesDO> hrPoliciesList = null;
		try {
			//hrPoliciesList = genericObject.retrieve(HrPoliciesDO.class);
			return this.sessionFactory.getCurrentSession().getNamedQuery(HrPoliciesDO.FIND_ALL)
					.list();
		} catch (Exception eException) {
		} finally {
		}
		return hrPoliciesList;
	}
	
	@SuppressWarnings("unchecked")
	public List<HrPoliciesDO> retrieveById(Long Id)  {
		List<HrPoliciesDO> hrPoliciesList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(HrPoliciesDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, Id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return hrPoliciesList;
	}
	
	@SuppressWarnings("unchecked")
	public List<HrPoliciesDO> retrieveByRoleId(Long Id)  {
		List<HrPoliciesDO> hrPoliciesList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(HrPoliciesDO.FIND_BY_ROLEID)
			.setParameter(CommonConstants.ID, Id)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return hrPoliciesList;
	}
	
	public HrPoliciesDO update(HrPoliciesDO hrPoliciesDO)  {
		try {
			genericObject.merge(hrPoliciesDO);
		} catch (Exception eException) {
		} finally {
		}
		return hrPoliciesDO;
	}

}
