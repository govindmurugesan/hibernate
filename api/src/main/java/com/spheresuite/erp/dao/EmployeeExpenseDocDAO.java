package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.EmployeeExpenseDocDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class EmployeeExpenseDocDAO {
	static Logger logger = Logger.getLogger(EmployeeExpenseDocDAO.class.getName());

	@Autowired
	private GenericDAOImpl<EmployeeExpenseDocDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public boolean delete(Long id) {
		boolean deleteStatus = true;
		try {
			this.sessionFactory.getCurrentSession().createQuery("delete from EmployeeExpenseDocDO e where e.empExpense.empExpenseId=:id")
										.setParameter(CommonConstants.ID, id)
										.executeUpdate();
		} catch (Exception eException) {
			deleteStatus = false;
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return deleteStatus;
	}
	
	
	public EmployeeExpenseDocDO persist(EmployeeExpenseDocDO employeeExpenseDocDO) {
		try {
			if (employeeExpenseDocDO != null) {
				genericObject.persist(employeeExpenseDocDO);
			}
		} catch (Exception eException) {
			//throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			//SessionManager.closeEntityManager(em);
		}
		return employeeExpenseDocDO;
	}
	
	
	public List<EmployeeExpenseDocDO> retrieve() {
		List<EmployeeExpenseDocDO> empExpeseDocList = null;
		try {
			empExpeseDocList = genericObject.retrieve(EmployeeExpenseDocDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return empExpeseDocList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeExpenseDocDO> retrieveByExpenseId(Long id) {
		List<EmployeeExpenseDocDO> empExpeseDocList = null;
		try {
			empExpeseDocList = this.sessionFactory.getCurrentSession().getNamedQuery(EmployeeExpenseDocDO.FIND_BY_EXPENSE_ID)
										.setParameter(CommonConstants.ID, id)
										.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return empExpeseDocList;
	}
	
	public EmployeeExpenseDocDO update(EmployeeExpenseDocDO employeeExpenseDocDO) {
		try {
			genericObject.merge(employeeExpenseDocDO);
		} catch (Exception eException) {
		} finally {
		}
		return employeeExpenseDocDO;
	}

}
