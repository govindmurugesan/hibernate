package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.WeeklyTimesheetDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class WeeklyTimesheetDAO {
	static Logger logger = Logger.getLogger(WeeklyTimesheetDAO.class.getName());
	
	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private GenericDAOImpl<WeeklyTimesheetDO> genericObject;
	
	public WeeklyTimesheetDO persist(WeeklyTimesheetDO weeklyTimesheetDO) {
		try {
			genericObject.persist(weeklyTimesheetDO);
		} catch (Exception eException) {
		} finally {
		}
		return weeklyTimesheetDO;
	}
	
	public List<WeeklyTimesheetDO> retrieve() {
		List<WeeklyTimesheetDO> weeklyTimesheetList = null;
		try {
			weeklyTimesheetList = genericObject.retrieve(WeeklyTimesheetDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return weeklyTimesheetList;
	}
	
	public List<WeeklyTimesheetDO> retrieveByDept(List<Long> ids) {
		List<WeeklyTimesheetDO> weeklyTimesheetList = null;
		try {
			weeklyTimesheetList =  genericObject.retrieveAllByDept(ids, WeeklyTimesheetDO.FIND_ALL_BY_DEPT, WeeklyTimesheetDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return weeklyTimesheetList;
	}
	
	public List<WeeklyTimesheetDO> retrieveSubmittedTimesheet() {
		List<WeeklyTimesheetDO> weeklyTimesheetList = null;
		try {
			weeklyTimesheetList =  genericObject.retrieveActive(CommonConstants.SUBMITTED, WeeklyTimesheetDO.FIND_SUBMITTED, WeeklyTimesheetDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return weeklyTimesheetList;
	}
	
	@SuppressWarnings("unchecked")
	public List<WeeklyTimesheetDO> retrieveById(Long Id) {
		List<WeeklyTimesheetDO> weeklyTimesheetList = null;
		try {
			weeklyTimesheetList =  this.sessionFactory.getCurrentSession().getNamedQuery(WeeklyTimesheetDO.FIND_BY_ID)
									.setParameter(CommonConstants.ID, Id).list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return weeklyTimesheetList;
	}
	
	@SuppressWarnings("unchecked")
	public List<WeeklyTimesheetDO> retrieveByDate(String date, long empid) {
		List<WeeklyTimesheetDO> weeklyTimesheetList = null;
		try {
			weeklyTimesheetList = this.sessionFactory.getCurrentSession().getNamedQuery(WeeklyTimesheetDO.FIND_BY_DATE)
										.setParameter(CommonConstants.WEEKENDDATE, date)
										.setParameter(CommonConstants.EMPID, empid)
										.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return weeklyTimesheetList;
	}
	
	@SuppressWarnings("unchecked")
	public List<WeeklyTimesheetDO> retrieveByEmpId(String empid) {
		List<WeeklyTimesheetDO> weeklyTimesheetList = null;
		try {
			weeklyTimesheetList = this.sessionFactory.getCurrentSession().getNamedQuery(WeeklyTimesheetDO.FIND_BY_EMPID)
										.setParameter(CommonConstants.EMPID, empid)
										.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return weeklyTimesheetList;
	}
	
	public WeeklyTimesheetDO update(WeeklyTimesheetDO weeklyTimesheetDO) {
		try {
			genericObject.merge(weeklyTimesheetDO);
		} catch (Exception eException) {
		} finally {
		}
		return weeklyTimesheetDO;
	}

}
