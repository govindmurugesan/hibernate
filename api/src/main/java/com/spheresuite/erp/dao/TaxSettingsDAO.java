package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.TaxSettingsDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class TaxSettingsDAO {
	static Logger logger = Logger.getLogger(TaxSettingsDAO.class.getName());

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private GenericDAOImpl<TaxSettingsDO> dao;
	
	public TaxSettingsDO persist(TaxSettingsDO taxSettingDO) {
		try {
			dao.persist(taxSettingDO);
		} catch (Exception eException) {
			//throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			//SessionManager.closeEntityManager(em);
		}
		return taxSettingDO;
	}
	
	public TaxSettingsDO update(TaxSettingsDO taxSettingDO) {
		try {
			dao.merge(taxSettingDO);
		} catch (Exception eException) {
			//throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			//SessionManager.closeEntityManager(em);
		}
		return taxSettingDO;
	}
	
	public List<TaxSettingsDO> retrieve() {
		List<TaxSettingsDO> taxSettingList = null;
		try {
			taxSettingList = dao.retrieve(TaxSettingsDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return taxSettingList;
	}
	
	@SuppressWarnings("unchecked")
	public List<TaxSettingsDO> retrieveById(Long Id) {
		List<TaxSettingsDO> taxSettingList = null;
		try {
			taxSettingList =  this.sessionFactory.getCurrentSession().getNamedQuery(TaxSettingsDO.FIND_BY_ID)
								.setParameter(CommonConstants.ID, Id).list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return taxSettingList;
	}
	
	public List<TaxSettingsDO> retrieveActive(String name) {
		List<TaxSettingsDO> taxSettingList = null;
		try {
			taxSettingList =  dao.retrieveActiveByName(CommonConstants.ACTIVE,name, TaxSettingsDO.FIND_ACTIVE, TaxSettingsDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return taxSettingList;
	}
}
