package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.ExpenseTypeDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class ExpenseTypeDAO {
	static Logger logger = Logger.getLogger(ExpenseTypeDAO.class.getName());
	
	@Autowired
	private GenericDAOImpl<ExpenseTypeDO> dao;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(ExpenseTypeDO expenseTypeDO){
		boolean persistStatus = true;
		try {
			List<ExpenseTypeDO> expenseTypeDOList = null;
			expenseTypeDOList = this.sessionFactory.getCurrentSession().getNamedQuery(ExpenseTypeDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, expenseTypeDO.getName())
					.list();
					if(expenseTypeDOList != null && expenseTypeDOList.size() > 0){
						persistStatus =  false;
					}else{
						dao.persist(expenseTypeDO);
					}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	public List<ExpenseTypeDO> retrieve(){
		List<ExpenseTypeDO> expenseTypeDOList = null;
		try {
			expenseTypeDOList = dao.retrieve(ExpenseTypeDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return expenseTypeDOList;
	}
	
	public List<ExpenseTypeDO> retrieveActive(){
		List<ExpenseTypeDO> expenseTypeDOList = null;
		try {
			expenseTypeDOList =  dao.retrieveActive(CommonConstants.ACTIVE, ExpenseTypeDO.FIND_BY_STATUS, ExpenseTypeDO.class);
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return expenseTypeDOList;
	}
	
	@SuppressWarnings("unchecked")
	public List<ExpenseTypeDO> retrieveById(Long id){
		 List<ExpenseTypeDO> expenseTypeDOList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(ExpenseTypeDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return expenseTypeDOList;
	}
	
	@SuppressWarnings("unchecked")
	public boolean update(ExpenseTypeDO ExpenseTypeDO) {
		boolean updateStatus = true;
		try {
			List<ExpenseTypeDO> expenseTypeDOList = null;
			expenseTypeDOList = this.sessionFactory.getCurrentSession().getNamedQuery(ExpenseTypeDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, ExpenseTypeDO.getName())
					.setParameter(CommonConstants.ID, ExpenseTypeDO.getExpenseTypeId())
					.list();
			if(expenseTypeDOList != null && expenseTypeDOList.size() > 0){
				updateStatus = false;
			}else{
				dao.merge(ExpenseTypeDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}
	
	public boolean persistList(List<ExpenseTypeDO> expenseTypeDOList){
		try {
			for (ExpenseTypeDO expensesType : expenseTypeDOList) {
				dao.persist(expensesType);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}

}
