package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.domainobject.AllowanceSettingDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class AllowanceSettingsDAO {
	static Logger logger = Logger.getLogger(AllowanceSettingsDAO.class.getName());
	@Autowired
	private GenericDAOImpl<AllowanceSettingDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public AllowanceSettingDO persist(AllowanceSettingDO allowanceSettingDO){
		try {
				genericObject.persist(allowanceSettingDO);
			} catch (Exception eException) {
			} finally {
				this.sessionFactory.getCurrentSession().flush();
				this.sessionFactory.getCurrentSession().clear();
			}
		return allowanceSettingDO;
	}
	
	public AllowanceSettingDO update(AllowanceSettingDO allowanceSettingDO){
		try {
			genericObject.merge(allowanceSettingDO);
		} catch (Exception eException) {
		} finally {
		}
		return allowanceSettingDO;
	}
	
	public List<AllowanceSettingDO> retrieve() {
		List<AllowanceSettingDO> allowanceSettingList = null;
		try {
			allowanceSettingList = genericObject.retrieve(AllowanceSettingDO.class);
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
		}
		return allowanceSettingList;
	}
	
	@SuppressWarnings("unchecked")
	public List<AllowanceSettingDO> retrieveById(Long Id){
		List<AllowanceSettingDO> allowanceSettingList = null;
		
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(AllowanceSettingDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, Id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return allowanceSettingList;
	}
	
	public List<AllowanceSettingDO> retrieveActive(String name){
		List<AllowanceSettingDO> allowanceSettingList = null;
		try {
			return genericObject.retrieveActiveByName(CommonConstants.ACTIVE,name, 
					"allowancesetting.findActive", AllowanceSettingDO.class);
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return allowanceSettingList;
	}
}
