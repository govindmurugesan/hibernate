package com.spheresuite.erp.procurement.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.Greige_QuotatioDetailsDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class Geige_QuotationDetailsDAO {
	@Autowired
	private GenericDAOImpl<Greige_QuotatioDetailsDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public boolean persist(Greige_QuotatioDetailsDO detailsDO){
		//List<EPMasterSortDO> earningTypeList = null;
		boolean persistStatus = true;
		try {
			genericObject.persist(detailsDO);
			/*earningTypeList = this.sessionFactory.getCurrentSession().getNamedQuery(EPMasterSortDO.FIND_BY_NAME)
			.setParameter(CommonConstants.NAME, earningTypeDO.getName())
			.list();
			if(earningTypeList != null && earningTypeList.size() > 0){
				persistStatus = false;
			}else{
				genericObject.persist(earningTypeDO);
			}*/
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<Greige_QuotatioDetailsDO> retrieveByQuoteId(Long id) {
		List<Greige_QuotatioDetailsDO> DetailsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(Greige_QuotatioDetailsDO.FIND_BY_QUOTID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return DetailsList;
	}
	
	public boolean delete(Long id)  {
		try {
			org.hibernate.Query query = this.sessionFactory.getCurrentSession().createQuery("delete from Greige_QuotatioDetailsDO e where e.quotation.quoteId=:id")
					.setParameter(CommonConstants.ID, id);
			query.executeUpdate();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return true;
	}
	
	
	
	
/*	
	public List<Greige_CreateQuotationDO> retrieve(){
		List<Greige_CreateQuotationDO> DetailsList = null;
		try {
			DetailsList = genericObject.retrieve(Greige_CreateQuotationDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return DetailsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Greige_CreateQuotationDO> retrieveById(Long id) {
		List<Greige_CreateQuotationDO> DetailsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(Greige_CreateQuotationDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return DetailsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Greige_CreateQuotationDO> retrieveQuoteBySortId(Long id) {
		List<Greige_CreateQuotationDO> DetailsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(Greige_CreateQuotationDO.FIND_BY_SORTID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return DetailsList;
	}
	
	
	
	@SuppressWarnings("unchecked")
	public List<Greige_CreateQuotationDO> retrieveDraftQuote() {
		List<Greige_CreateQuotationDO> DetailsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(Greige_CreateQuotationDO.FIND_QUOTEBYSTATUS)
					.setParameter(CommonConstants.STATUS, CommonConstants.DSTATUS)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return DetailsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Greige_CreateQuotationDO> retrieveRequestQuote() {
		List<Greige_CreateQuotationDO> DetailsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(Greige_CreateQuotationDO.FIND_QUOTEBYSTATUS)
					.setParameter(CommonConstants.STATUS, CommonConstants.RSTATUS)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return DetailsList;
	}
	
	
	
	
	public boolean update(Greige_CreateQuotationDO DetailsList){
		boolean updateStatus = true;
		try {
			genericObject.merge(DetailsList);
			List<EPMasterSortDO> sortList = null;
			earningTypeList = this.sessionFactory.getCurrentSession().getNamedQuery(EPMasterSortDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, allowanceTypeDO.getName())
					.setParameter(CommonConstants.ID, allowanceTypeDO.getEarningTypeId())
					.list();
			if(earningTypeList != null && earningTypeList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(sortList);
			}
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}
	
	public boolean delete(Long id)  {
		try {
			org.hibernate.Query query = this.sessionFactory.getCurrentSession().createQuery("delete from Greige_CreateQuotationDO e where e.quoteId=:id")
					.setParameter(CommonConstants.ID, id);
			query.executeUpdate();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return true;
	}*/
	
	

}
