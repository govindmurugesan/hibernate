package com.spheresuite.erp.procurement.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.Greige_Spplier_QuoteDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class Greige_Supplier_QuoteDAO {
	static Logger logger = Logger.getLogger(Greige_Supplier_QuoteDAO.class.getName());
	@Autowired
	private GenericDAOImpl<Greige_Spplier_QuoteDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public boolean persist(Greige_Spplier_QuoteDO quoteDetails){
		//List<EPMasterSortDO> earningTypeList = null;
		boolean persistStatus = true;
		try {
			genericObject.persist(quoteDetails);
			/*earningTypeList = this.sessionFactory.getCurrentSession().getNamedQuery(EPMasterSortDO.FIND_BY_NAME)
			.setParameter(CommonConstants.NAME, earningTypeDO.getName())
			.list();
			if(earningTypeList != null && earningTypeList.size() > 0){
				persistStatus = false;
			}else{
				genericObject.persist(earningTypeDO);
			}*/
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return persistStatus;
	}
	
	public List<Greige_Spplier_QuoteDO> retrieve(){
		List<Greige_Spplier_QuoteDO> quoteDetailsList = null;
		try {
			quoteDetailsList = genericObject.retrieve(Greige_Spplier_QuoteDO.class);
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return quoteDetailsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Greige_Spplier_QuoteDO> retrieveById(Long id) {
		List<Greige_Spplier_QuoteDO> quoteDetailsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(Greige_Spplier_QuoteDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return quoteDetailsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Greige_Spplier_QuoteDO> retriveByQuoteIdAndSupplierId(Long id, Long quoteId, String supplierId) {
		List<Greige_Spplier_QuoteDO> quoteDetailsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(Greige_Spplier_QuoteDO.FIND_BY_QUOTEID_SUPPLIERID)
					.setParameter(CommonConstants.ID, id)
					.setParameter(CommonConstants.QUOT_ID, quoteId)
					.setParameter(CommonConstants.SUPPLIERID, supplierId)
					
					.list();
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return quoteDetailsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Greige_Spplier_QuoteDO> retriveByQuoteId(Long id) {
		List<Greige_Spplier_QuoteDO> quoteDetailsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(Greige_Spplier_QuoteDO.FIND_BY_QUOTEID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return quoteDetailsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Greige_Spplier_QuoteDO> retriveByQuoteSubmittedId(Long id) {
		List<Greige_Spplier_QuoteDO> quoteDetailsList = null;
		try {
			quoteDetailsList = this.sessionFactory.getCurrentSession().getNamedQuery(Greige_Spplier_QuoteDO.FIND_BY_QUOTE_SUBMITTED)
					.setParameter(CommonConstants.ID, id)
					.setParameter(CommonConstants.STATUS, "s")
					.list();
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return quoteDetailsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Greige_Spplier_QuoteDO> retriveByQuoteApprove(Long id) {
		List<Greige_Spplier_QuoteDO> quoteDetailsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(Greige_Spplier_QuoteDO.FIND_BY_QUOTE_APPROVE)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return quoteDetailsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Greige_Spplier_QuoteDO> retrieveByStatus(String status) {
		List<Greige_Spplier_QuoteDO> quoteDetailsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(Greige_Spplier_QuoteDO.FIND_BY_STATUS)
					.setParameter(CommonConstants.STATUS, status)
					.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return quoteDetailsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Greige_Spplier_QuoteDO> retrieveBySpplierId(String spplierId, String type) {
		List<Greige_Spplier_QuoteDO> quoteDetailsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(Greige_Spplier_QuoteDO.FIND_BY_SPPLIERID)
					.setParameter(CommonConstants.ID, spplierId)
					.setParameter(CommonConstants.TYPE, type)
					.list();
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return quoteDetailsList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<Greige_Spplier_QuoteDO> retrieveForApproval() {
		List<Greige_Spplier_QuoteDO> quoteDetailsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(Greige_Spplier_QuoteDO.FIND_BY_STATUS)
					.setParameter(CommonConstants.STATUS, CommonConstants.RSTATUS)
					.list();
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return quoteDetailsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Greige_Spplier_QuoteDO> retriveBySupplierAcceptCost(String accept) {
		List<Greige_Spplier_QuoteDO> quoteDetailsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(Greige_Spplier_QuoteDO.FIND_BY_ACCEPT_COST)
					.setParameter(CommonConstants.ACCEPT_COST, accept)
					.list();
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return quoteDetailsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Greige_Spplier_QuoteDO> retriveForApproval() {
		List<Greige_Spplier_QuoteDO> DetailsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(Greige_Spplier_QuoteDO.FIND_QUOTEBYAPPROVESTATUS)
					.setParameter(CommonConstants.STATUS, CommonConstants.SSTATUS)
						.setParameter(CommonConstants.APPROVEDSTATUS, CommonConstants.ASTATUS)
					.list();
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return DetailsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Greige_Spplier_QuoteDO> retrieveByQuoteReceived() {
		List<Greige_Spplier_QuoteDO> DetailsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(Greige_Spplier_QuoteDO.FIND_QUOTEBYRECEIVED)
					.setParameter(CommonConstants.STATUS, CommonConstants.SSTATUS)
					.list();
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return DetailsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Greige_Spplier_QuoteDO> retrieveSupplierAlert() {
		List<Greige_Spplier_QuoteDO> DetailsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(Greige_Spplier_QuoteDO.FIND_QUOTEALERT)
					.setParameter(CommonConstants.STATUS, CommonConstants.SSTATUS)
					.list();
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return DetailsList;
	}
	
	public boolean update(Greige_Spplier_QuoteDO quoteDetailsList){
		boolean updateStatus = true;
		try {
			genericObject.merge(quoteDetailsList);
			/*List<EPMasterSortDO> sortList = null;
			earningTypeList = this.sessionFactory.getCurrentSession().getNamedQuery(EPMasterSortDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, allowanceTypeDO.getName())
					.setParameter(CommonConstants.ID, allowanceTypeDO.getEarningTypeId())
					.list();
			if(earningTypeList != null && earningTypeList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(sortList);
			}*/
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return updateStatus;
	}
	
	

}
