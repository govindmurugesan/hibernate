package com.spheresuite.erp.procurement.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.Greige_PendingOrdersDO;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
@Repository
public class Greige_PendingOrdersDAO {
	
	static Logger logger = Logger.getLogger(Greige_PendingOrdersDAO.class.getName());
	
	@Autowired
	private GenericDAOImpl<Greige_PendingOrdersDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public boolean persist(Greige_PendingOrdersDO detailsDO){
		//List<EPMasterSortDO> earningTypeList = null;
		boolean persistStatus = true;
		try {
			genericObject.persist(detailsDO);
			/*earningTypeList = this.sessionFactory.getCurrentSession().getNamedQuery(EPMasterSortDO.FIND_BY_NAME)
			.setParameter(CommonConstants.NAME, earningTypeDO.getName())
			.list();
			if(earningTypeList != null && earningTypeList.size() > 0){
				persistStatus = false;
			}else{
				genericObject.persist(earningTypeDO);
			}*/
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return persistStatus;
	}
	
	public List<Greige_PendingOrdersDO> retrieve(){
		List<Greige_PendingOrdersDO> DetailsList = null;
		try {
			DetailsList = genericObject.retrieve(Greige_PendingOrdersDO.class);
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return DetailsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Greige_PendingOrdersDO> retrieveAll(){
		List<Greige_PendingOrdersDO> DetailsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(Greige_PendingOrdersDO.FIND_ALL)
					.list();
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return DetailsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Greige_PendingOrdersDO> retrieveWithStatus(){
		List<Greige_PendingOrdersDO> DetailsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(Greige_PendingOrdersDO.FIND_STATUS)
					.list();
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return DetailsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Greige_PendingOrdersDO> retrieveById(Long id) {
		List<Greige_PendingOrdersDO> DetailsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(Greige_PendingOrdersDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return DetailsList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<Greige_PendingOrdersDO> retrieveBySortId(Long id) {
		List<Greige_PendingOrdersDO> DetailsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(Greige_PendingOrdersDO.FIND_BY_SORTID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return DetailsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Greige_PendingOrdersDO> retrieveByGreigeReqDate(String date) {
		List<Greige_PendingOrdersDO> DetailsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(Greige_PendingOrdersDO.FIND_BY_GREIGE_REQ_DATE)
					.setParameter(CommonConstants.DATE, CommonUtil.convertStringToSqlDate(date))
					.list();
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return DetailsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Greige_PendingOrdersDO> retrieveByGreigReqDate(String date, Long id) {
		List<Greige_PendingOrdersDO> DetailsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(Greige_PendingOrdersDO.FIND_BY_GREIG_REQ_DATE)
					.setParameter(CommonConstants.DATE, CommonUtil.convertStringToSqlDate(date))
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return DetailsList;
	}
	
	public boolean update(Greige_PendingOrdersDO DetailsList){
		boolean updateStatus = true;
		try {
			genericObject.merge(DetailsList);
			/*List<EPMasterSortDO> sortList = null;
			earningTypeList = this.sessionFactory.getCurrentSession().getNamedQuery(EPMasterSortDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, allowanceTypeDO.getName())
					.setParameter(CommonConstants.ID, allowanceTypeDO.getEarningTypeId())
					.list();
			if(earningTypeList != null && earningTypeList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(sortList);
			}*/
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return updateStatus;
	}
	
	@SuppressWarnings("unchecked")
	public List<Greige_PendingOrdersDO> retrieveOcnsCount(){
		List<Greige_PendingOrdersDO> DetailsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(Greige_PendingOrdersDO.FIND_COUNT)
					.list();
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return DetailsList;
	}
	
	

}
