package com.spheresuite.erp.procurement.web.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.Greige_CreateQuotationDO;
import com.spheresuite.erp.domainobject.Greige_PendingOrdersDO;
import com.spheresuite.erp.domainobject.Greige_QuotatioDetailsDO;
import com.spheresuite.erp.procurement.service.Greige_CreateQuotationService;
import com.spheresuite.erp.procurement.service.Greige_PendingOrdersService;
import com.spheresuite.erp.procurement.service.Greige_QuotationDetailService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;

@Component
public class Greige_PendingOrdersUtil {
	
	private Greige_PendingOrdersUtil() {}
	
	@Autowired
	private  Greige_CreateQuotationService tgreige_CreateQuotationService;
	private static Greige_CreateQuotationService greige_CreateQuotationService;
	
	@Autowired
	private  Greige_QuotationDetailService tgreige_QuotationDetailService;
	private static Greige_QuotationDetailService greige_QuotationDetailService;
	
	@Autowired
	private  Greige_PendingOrdersService tgreige_PendingOrdersService;
	private static Greige_PendingOrdersService greige_PendingOrdersService;
	
	@PostConstruct
	public void init() {
		greige_CreateQuotationService = tgreige_CreateQuotationService;
		greige_QuotationDetailService = tgreige_QuotationDetailService;
		greige_PendingOrdersService = tgreige_PendingOrdersService;
	}
	
	public static JSONObject getOrderList(List<Greige_PendingOrdersDO> orderDetailList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (Greige_PendingOrdersDO orderDetail : orderDetailList) {
				resultJSONArray.put(getOrderDetailObject(orderDetail));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getOrderDetailObject(Greige_PendingOrdersDO sortDetail)throws JSONException{
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(sortDetail.getPendingOrderId()));
		if(sortDetail.getSortName() != null){
			result.put(CommonConstants.SORT, String.valueOf(sortDetail.getSortName().getSortName()));
			result.put(CommonConstants.SORT_ID, String.valueOf(sortDetail.getSortName().getSortId()));
			
			
			if(sortDetail.getSortName().getGrCons() != null){
				result.put(CommonConstants.GRCONS, String.valueOf(sortDetail.getSortName().getGrCons()));
			}else{
				result.put(CommonConstants.GRCONS, "");
			}
			if(sortDetail.getSortName().getGw() != null){
				result.put(CommonConstants.GW, String.valueOf(sortDetail.getSortName().getGw()));
			}else{
				result.put(CommonConstants.GW, "");
			}
			if(sortDetail.getSortName().getWave() != null){
				result.put(CommonConstants.WAVE, String.valueOf(sortDetail.getSortName().getWave()));
			}else{
				result.put(CommonConstants.WAVE, "");
			}
			if(sortDetail.getSortName().getTc() != null){
				result.put(CommonConstants.TC, String.valueOf(sortDetail.getSortName().getTc()));
			}else{
				result.put(CommonConstants.TC, "");
			}
			if(sortDetail.getSortName().getYc() != null){
				result.put(CommonConstants.YC, String.valueOf(sortDetail.getSortName().getYc()));
			}else{
				result.put(CommonConstants.YC, "");
			}
			if(sortDetail.getSortName().getGrCons() != null){
				result.put(CommonConstants.GRCONS, String.valueOf(sortDetail.getSortName().getGrCons()));
			}else{
				result.put(CommonConstants.GRCONS, "");
			}
			
		}else{
			result.put(CommonConstants.SORT, "");
			result.put(CommonConstants.SORT_ID, "");
		}
		if(sortDetail.getOcn() != null){
			result.put(CommonConstants.OCN, String.valueOf(sortDetail.getOcn().trim()));
		}else{
			result.put(CommonConstants.OCN, "");
		}
		if(sortDetail.getPlanNo() != null){
			result.put(CommonConstants.PLANNO, String.valueOf(sortDetail.getPlanNo()));
		}else{
			result.put(CommonConstants.PLANNO, "");
		}
		if(sortDetail.getGreigReq() != null){
			result.put(CommonConstants.GREIGREG, String.valueOf(sortDetail.getGreigReq()));
		}else{
			result.put(CommonConstants.GREIGREG, "");
		}
		if(sortDetail.getStatus() != null){
			result.put(CommonConstants.STATUS, String.valueOf(sortDetail.getStatus()));
		}else{
			result.put(CommonConstants.STATUS, "");
		}
		/*if(sortDetail.getGrCons() != null){
			result.put(CommonConstants.GRCONS, String.valueOf(sortDetail.getGrCons()));
		}else{
			result.put(CommonConstants.GRCONS, "");
		}*/
		if(sortDetail.getShippingDate() != null){
			result.put(CommonConstants.SHIPPINGDATE, String.valueOf(sortDetail.getShippingDate()));
		}else{
			result.put(CommonConstants.SHIPPINGDATE, "");
		}
		if(sortDetail.getGreigReqDate() != null){
			result.put(CommonConstants.GREIGREQDATE, String.valueOf(sortDetail.getGreigReqDate()));
		}else{
			result.put(CommonConstants.GREIGREQDATE, "");
		}
		/*if(sortDetail.getPendigDays() != null){
			result.put(CommonConstants.PENDINGDAYS, String.valueOf(sortDetail.getPendigDays()));
		}else{
			result.put(CommonConstants.PENDINGDAYS, "");
		}*/
		
		
		/*LocalDate todaydate = LocalDate.now();
		System.out.println("todaydate"+todaydate);
		LocalDate dateAfter = sortDetail.getGreigReqDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate(); ;
		long daysBetween = DAYS.between(todaydate, dateAfter);*/
		Date today = new Date();
		Date dueDate = sortDetail.getCreatedon();
		 long diff = today.getTime() - dueDate.getTime();;
		 result.put(CommonConstants.PENDINGDAYS, String.valueOf(TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)));
		
		/*if(sortDetail.getSortName() != null && sortDetail.getSortDO().getGrCons() != null){
			result.put(CommonConstants.GRCONS, String.valueOf(sortDetail.getSortDO().getGrCons()));
		}else{
			result.put(CommonConstants.GRCONS, "");
		}
		if(sortDetail.getSortDO() != null && sortDetail.getSortDO().getGw() != null){
			result.put(CommonConstants.GW, String.valueOf(sortDetail.getSortDO().getGw()));
		}else{
			result.put(CommonConstants.GW, "");
		}
		if(sortDetail.getSortDO() != null && sortDetail.getSortDO().getTc() != null){
			result.put(CommonConstants.TC, String.valueOf(sortDetail.getSortDO().getTc()));
		}else{
			result.put(CommonConstants.TC, "");
		}
		if(sortDetail.getSortDO() != null && sortDetail.getSortDO().getYc() != null){
			result.put(CommonConstants.YC, String.valueOf(sortDetail.getSortDO().getYc()));
		}else{
			result.put(CommonConstants.YC, "");
		}
		if(sortDetail.getWeave() != null){
			result.put(CommonConstants.WAVE, String.valueOf(sortDetail.getWeave()));
		}else{
			result.put(CommonConstants.WAVE, "");
		}*/
		if(sortDetail.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(sortDetail.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(sortDetail.getUpdatedon())));
		return result;
	}
	
	
	public static JSONObject getOrderListWithQuotationDetails(List<Greige_PendingOrdersDO> orderDetailList, Long sortID) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (Greige_PendingOrdersDO orderDetail : orderDetailList) {
				resultJSONArray.put(getOrderListWithQuotationDetailsObject(orderDetail, sortID));
			}
			JSONArray resultJSONArray2 = new JSONArray();
			if(sortID != null){
				List<Greige_CreateQuotationDO> quotationList = greige_CreateQuotationService.retrieveQuoteBySortId(sortID);
				if(quotationList != null && quotationList.size() > 0){
					for (Greige_CreateQuotationDO qtDetail : quotationList) {
						resultJSONArray2.put(getEPMasterSortDetailObject(qtDetail));
					}
				}
			}
			resultJSON.put(CommonConstants.QUOTATIONDETAIL, resultJSONArray2);
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getOrderListWithQuotationDetailsObject(Greige_PendingOrdersDO sortDetail, Long sortID)throws JSONException{
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(sortDetail.getPendingOrderId()));
		if(sortDetail.getSortName() != null){
			result.put(CommonConstants.SORT, String.valueOf(sortDetail.getSortName().getSortName()));
			result.put(CommonConstants.SORT_ID, String.valueOf(sortDetail.getSortName().getSortId()));
		}else{
			result.put(CommonConstants.SORT, "");
			result.put(CommonConstants.SORT_ID, "");
		}
		if(sortDetail.getOcn() != null){
			result.put(CommonConstants.OCN, String.valueOf(sortDetail.getOcn().trim()));
		}else{
			result.put(CommonConstants.OCN, "");
		}
		if(sortDetail.getPlanNo() != null){
			result.put(CommonConstants.PLANNO, String.valueOf(sortDetail.getPlanNo()));
		}else{
			result.put(CommonConstants.PLANNO, "");
		}
		if(sortDetail.getGreigReq() != null){
			result.put(CommonConstants.GREIGREG, String.valueOf(sortDetail.getGreigReq()));
		}else{
			result.put(CommonConstants.GREIGREG, "");
		}
		if(sortDetail.getShippingDate() != null){
			result.put(CommonConstants.SHIPPINGDATE, String.valueOf(sortDetail.getShippingDate()));
		}else{
			result.put(CommonConstants.SHIPPINGDATE, "");
		}
		if(sortDetail.getGreigReqDate() != null){
			result.put(CommonConstants.GREIGREQDATE, String.valueOf(sortDetail.getGreigReqDate()));
		}else{
			result.put(CommonConstants.GREIGREQDATE, "");
		}
		if(sortDetail.getStatus() != null){
			result.put(CommonConstants.STATUS, String.valueOf(sortDetail.getStatus()));
		}else{
			result.put(CommonConstants.STATUS, "");
		}
		Date today = new Date();
		Date dueDate = sortDetail.getCreatedon();
		long diff = today.getTime() - dueDate.getTime();
		result.put(CommonConstants.PENDINGDAYS, String.valueOf(TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)));
	
		/*if(sortDetail.getPendigDays() != null){
			result.put(CommonConstants.PENDINGDAYS, String.valueOf(sortDetail.getPendigDays()));
		}else{
			result.put(CommonConstants.PENDINGDAYS, "");
		}*/
		if(sortDetail.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(sortDetail.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(sortDetail.getUpdatedon())));
		
		return result;
	}
	
	public static JSONObject getEPMasterSortDetailObject(Greige_CreateQuotationDO quotationDetail)throws JSONException{
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(quotationDetail.getQuoteId()));
		
		if(quotationDetail.getSortName() != null){
			result.put(CommonConstants.SORT, String.valueOf(quotationDetail.getSortName().getSortName()));
			result.put(CommonConstants.SORT_ID, String.valueOf(quotationDetail.getSortName().getSortId()));
		}else{
			result.put(CommonConstants.SORT, "");
			result.put(CommonConstants.SORT_ID, "");
		}
		if(quotationDetail.getGrCons()!= null){
			result.put(CommonConstants.GRCONS, String.valueOf(quotationDetail.getGrCons()));
		}else{
			result.put(CommonConstants.GRCONS, "");
		}
		if(quotationDetail.getGreigeReqDueDate() != null){
			result.put(CommonConstants.GREIGREQDUODATE, String.valueOf(quotationDetail.getGreigeReqDueDate()));
		}else{
			result.put(CommonConstants.GREIGREQDUODATE, "");
		}
		if(quotationDetail.getGreigReqDate() != null){
			result.put(CommonConstants.GREIGREQDATE, String.valueOf(quotationDetail.getGreigReqDate()));
		}else{
			result.put(CommonConstants.GREIGREQDATE, "");
		}
		if(quotationDetail.getGw() != null){
			result.put(CommonConstants.GW, String.valueOf(quotationDetail.getGw()));
		}else{
			result.put(CommonConstants.GW, "");
		}
		if(quotationDetail.getRequiredMeter() != null){
			result.put(CommonConstants.REQUIREDMETER, String.valueOf(quotationDetail.getRequiredMeter()));
		}else{
			result.put(CommonConstants.REQUIREDMETER, "");
		}
		if(quotationDetail.getTargetPrice() != null){
			result.put(CommonConstants.TARGETPRICE, String.valueOf(quotationDetail.getTargetPrice()));
		}else{
			result.put(CommonConstants.TARGETPRICE, "");
		}
		if(quotationDetail.getTc() != null){
			result.put(CommonConstants.TC, String.valueOf(quotationDetail.getTc()));
		}else{
			result.put(CommonConstants.TC, "");
		}
		if(quotationDetail.getTc() != null){
			result.put(CommonConstants.YC, String.valueOf(quotationDetail.getYc()));
		}else{
			result.put(CommonConstants.YC, "");
		}
		if(quotationDetail.getWave() != null){
			result.put(CommonConstants.WAVE, String.valueOf(quotationDetail.getWave()));
		}else{
			result.put(CommonConstants.WAVE, "");
		}
		
		/*if(quotationDetail.getGreigePendingOrder() != null){
			result.put(CommonConstants.PENDINGORDERID, String.valueOf(quotationDetail.getGreigePendingOrder().getPendingOrderId()));
		}else{
			result.put(CommonConstants.PENDINGORDERID, "");
		}*/
		/*if(quotationDetail.getGreigePendingOrder() != null &&  quotationDetail.getGreigePendingOrder().getOcn() != null){
			result.put(CommonConstants.OCN, String.valueOf(quotationDetail.getGreigePendingOrder().getOcn()));
		}else{
			result.put(CommonConstants.OCN, "");
		}*/
		List<Greige_QuotatioDetailsDO> orderList = greige_QuotationDetailService.retrieveByQuoteId(Long.parseLong(quotationDetail.getQuoteId().toString()));
		
		if(orderList != null && orderList.size()>0){
			JSONArray resultJSONArray = new JSONArray();
			for(int i= 0; i < orderList.size(); i++){
				resultJSONArray.put(getOcnDetail(orderList.get(i)));
			}
			result.put(CommonConstants.OCNDETAILS, resultJSONArray);
		}
		
		
		if(quotationDetail.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(quotationDetail.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(quotationDetail.getUpdatedon())));
		return result;
	}
	
	public static JSONObject getOcnDetail(Greige_QuotatioDetailsDO  order)throws JSONException{
		JSONObject result = new JSONObject();
		result.put(CommonConstants.OCN, String.valueOf(order.getOcn().getOcn().trim()));
		return result;
	}
	
	public static JSONObject getOrderListWithQuotationDetailsGreigeReqDate(List<Greige_PendingOrdersDO> orderDetailList, String date) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			List<Long> sortList = new ArrayList<Long>();
			for (Greige_PendingOrdersDO orderDetail : orderDetailList) {
				resultJSONArray.put(getOrderListWithQuotationDetailsGreigeReqDateObject(orderDetail,date));
				sortList.add(orderDetail.getSortName().getSortId());
			}
			JSONArray resultJSONArray2 = new JSONArray();
			if(sortList != null && sortList.size() > 0){
				List<Greige_CreateQuotationDO> quotationList = greige_CreateQuotationService.retrieveQuoteBySortList(sortList);
				if(quotationList != null && quotationList.size() > 0){
					for (Greige_CreateQuotationDO qtDetail : quotationList) {
						resultJSONArray2.put(getEPMasterSortDetailObject(qtDetail));
					}
				}
			}
			/*JSONArray resultJSONArray2 = new JSONArray();
			if(sortID != null){
				List<Greige_CreateQuotationDO> quotationList = greige_CreateQuotationService.retrieveQuoteBySortId(sortID);
				if(quotationList != null && quotationList.size() > 0){
					for (Greige_CreateQuotationDO qtDetail : quotationList) {
						resultJSONArray2.put(getEPMasterSortDetailObject(qtDetail));
					}
				}
			}*/
			resultJSON.put(CommonConstants.QUOTATIONDETAIL, resultJSONArray2);
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getOrderListWithQuotationDetailsGreigeReqDateObject(Greige_PendingOrdersDO sortDetail, String date)throws JSONException{
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(sortDetail.getPendingOrderId()));
		if(sortDetail.getSortName() != null){
			result.put(CommonConstants.SORT, String.valueOf(sortDetail.getSortName().getSortName()));
			result.put(CommonConstants.SORT_ID, String.valueOf(sortDetail.getSortName().getSortId()));
		}else{
			result.put(CommonConstants.SORT, "");
			result.put(CommonConstants.SORT_ID, "");
		}
		
		/*List<Greige_PendingOrdersDO> greige_PendingOrdersList = greige_PendingOrdersService.retrieveByGreigReqDate(date, sortDetail.getSortName().getSortId());
		if(greige_PendingOrdersList != null && greige_PendingOrdersList.size() > 0){
			String ocn =  "";
			String id = "";
			for(Greige_PendingOrdersDO greige_PendingOrdersDO : greige_PendingOrdersList){
				ocn += greige_PendingOrdersDO.getOcn() + ",";
			}
			if(ocn != null && ocn.length()>0){
				result.put(CommonConstants.OCN, ocn);
			}else{
				result.put(CommonConstants.OCN, "");
			}
		}else{
			result.put(CommonConstants.OCN, "");
		}*/
		
		/*if(sortDetail.getPlanNo() != null){
			result.put(CommonConstants.PLANNO, String.valueOf(sortDetail.getPlanNo()));
		}else{
			result.put(CommonConstants.PLANNO, "");
		}*/
		if(sortDetail.getOcn() != null){
			result.put(CommonConstants.OCN, String.valueOf(sortDetail.getOcn().trim()));
		}else{
			result.put(CommonConstants.OCN, "");
		}
		if(sortDetail.getGreigReq() != null){
			result.put(CommonConstants.GREIGREG, String.valueOf(sortDetail.getGreigReq()));
		}else{
			result.put(CommonConstants.GREIGREG, "");
		}
		/*if(sortDetail.getShippingDate() != null){
			result.put(CommonConstants.SHIPPINGDATE, String.valueOf(sortDetail.getShippingDate()));
		}else{
			result.put(CommonConstants.SHIPPINGDATE, "");
		}*/
		if(sortDetail.getGreigReqDate() != null){
			result.put(CommonConstants.GREIGREQDATE, String.valueOf(sortDetail.getGreigReqDate()));
		}else{
			result.put(CommonConstants.GREIGREQDATE, "");
		}
		
		Date today = new Date();
		Date dueDate = sortDetail.getCreatedon();
		long diff = today.getTime() - dueDate.getTime();
		result.put(CommonConstants.PENDINGDAYS, String.valueOf(TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)));
	
		/*if(sortDetail.getPendigDays() != null){
			result.put(CommonConstants.PENDINGDAYS, String.valueOf(sortDetail.getPendigDays()));
		}else{
			result.put(CommonConstants.PENDINGDAYS, "");
		}*/
		if(sortDetail.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(sortDetail.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(sortDetail.getUpdatedon())));
		
		return result;
	}
	
	public static JSONObject getOrderCountList(List<Greige_PendingOrdersDO> orderDetailList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			if(orderDetailList != null && orderDetailList.size() > 0){
				resultJSON.put(CommonConstants.PENDING_OCN_COUNT, orderDetailList.get(0));
			} else {
				resultJSON.put(CommonConstants.PENDING_OCN_COUNT, orderDetailList.get(0));
			}
			List<Greige_CreateQuotationDO> quoteReqCount = greige_CreateQuotationService.retrieveReqCount();
			if(quoteReqCount != null && quoteReqCount.size() > 0){
				resultJSON.put(CommonConstants.QUOTE_REQUESTED_COUNT, quoteReqCount.size());
			}else {
				resultJSON.put(CommonConstants.QUOTE_REQUESTED_COUNT, quoteReqCount.size());
			}
			List<Greige_CreateQuotationDO> quoteEditCount = greige_CreateQuotationService.retrieveEditCount();
			if(quoteEditCount != null && quoteEditCount.size() > 0){
				resultJSON.put(CommonConstants.QUOTE_EDIT_COUNT, quoteEditCount.size());
			}else {
				resultJSON.put(CommonConstants.QUOTE_EDIT_COUNT, quoteEditCount.size());
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
}
