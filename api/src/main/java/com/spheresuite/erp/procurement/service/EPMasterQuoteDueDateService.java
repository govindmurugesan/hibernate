package com.spheresuite.erp.procurement.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.domainobject.EPMasterQuoteDueDateDO;
import com.spheresuite.erp.procurement.dao.EPMasterQuoteDueDateDAO;

@Service
@Transactional
public class EPMasterQuoteDueDateService {

	@Autowired
	private EPMasterQuoteDueDateDAO epMasterQuoteDueDateDAO;
	
	@Transactional
	public boolean persist(EPMasterQuoteDueDateDO quoteDueDO){
		return epMasterQuoteDueDateDAO.persist(quoteDueDO);
	}

	@Transactional
	public List<EPMasterQuoteDueDateDO> retrieveActive(){
		return epMasterQuoteDueDateDAO.retrieveActive();
	}
	
	@Transactional
	public List<EPMasterQuoteDueDateDO> retrieveById(Long id){
		return epMasterQuoteDueDateDAO.retrieveById(id);
	}
	
	@Transactional
	public List<EPMasterQuoteDueDateDO> retrieve(){
		return epMasterQuoteDueDateDAO.retrieveActive();
	}
	
	@Transactional
	public boolean update(EPMasterQuoteDueDateDO quoteDueDO){
		return epMasterQuoteDueDateDAO.update(quoteDueDO);
	}
	
	@Transactional
	public boolean persistList(List<EPMasterQuoteDueDateDO> quoteDueDO) {
		return epMasterQuoteDueDateDAO.persistList(quoteDueDO);
	}
}
