package com.spheresuite.erp.procurement.rs;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.Greige_NotificationSettingsDO;
import com.spheresuite.erp.procurement.service.Greige_NotificationSettingsService;
import com.spheresuite.erp.procurement.web.util.Greige_NotificationSettingsUtil;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/greigenotificationsettings")
public class Greige_NotificationSettingsRS {

	String validation = null;
	static Logger logger = Logger.getLogger(Greige_NotificationSettingsRS.class.getName());

	@Autowired
	private Greige_NotificationSettingsService greigeNotificationSettingsService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				Greige_NotificationSettingsDO greigeNotificationSettingsDO = new Greige_NotificationSettingsDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.APPROVER) != null && !inputJSON.get(CommonConstants.APPROVER).toString().isEmpty()){
			 			greigeNotificationSettingsDO.setApprover(inputJSON.get(CommonConstants.APPROVER).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			greigeNotificationSettingsDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			greigeNotificationSettingsDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		greigeNotificationSettingsDO.setUpdatedon(new Date());
			 		greigeNotificationSettingsDO.setCreatedon(new Date());
			 	}
			 	greigeNotificationSettingsService.persist(greigeNotificationSettingsDO);
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Notification Settings Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<Greige_NotificationSettingsDO> notificationSettingsList = greigeNotificationSettingsService.retrieve();
				respJSON = Greige_NotificationSettingsUtil.getNotificationSettingsList(notificationSettingsList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				Greige_NotificationSettingsDO greigeNotificationSettingsDO = new Greige_NotificationSettingsDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		if(!inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 			List<Greige_NotificationSettingsDO> greigeNotificationSettingsList = greigeNotificationSettingsService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 				if(greigeNotificationSettingsList != null && greigeNotificationSettingsList.size() >0){
			 					greigeNotificationSettingsDO = greigeNotificationSettingsList.get(0);
				 				if(inputJSON.get(CommonConstants.APPROVER) != null && !inputJSON.get(CommonConstants.APPROVER).toString().isEmpty()){
						 			greigeNotificationSettingsDO.setApprover(inputJSON.get(CommonConstants.APPROVER).toString());
						 		}
						 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
						 			greigeNotificationSettingsDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
						 		}
						 		greigeNotificationSettingsDO.setUpdatedon(new Date());
						 		greigeNotificationSettingsService.update(greigeNotificationSettingsDO);
						 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Notification Settings Updated");
			 				}
			 			}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
