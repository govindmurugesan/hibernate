package com.spheresuite.erp.procurement.rs;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.EPMasterSortDO;
import com.spheresuite.erp.procurement.service.EPMasterSortService;
import com.spheresuite.erp.procurement.web.util.EPMasterSortUtil;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/mastersort")
public class EPMasterSortRS {

	String validation = null;
	static Logger logger = Logger.getLogger(EPMasterSortRS.class.getName());
	@Autowired
	private EPMasterSortService epMasterSortService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EPMasterSortDO epMasterSortDO = new EPMasterSortDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.SORT) != null && !inputJSON.get(CommonConstants.SORT).toString().isEmpty()){
			 			epMasterSortDO.setSortName(inputJSON.get(CommonConstants.SORT).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.GRCONS) != null && !inputJSON.get(CommonConstants.GRCONS).toString().isEmpty()){
			 			epMasterSortDO.setGrCons(inputJSON.get(CommonConstants.GRCONS).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.TC) != null && !inputJSON.get(CommonConstants.TC).toString().isEmpty()){
			 			epMasterSortDO.setTc(inputJSON.get(CommonConstants.TC).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.YC) != null && !inputJSON.get(CommonConstants.YC).toString().isEmpty()){
			 			epMasterSortDO.setYc(inputJSON.get(CommonConstants.YC).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.GW) != null && !inputJSON.get(CommonConstants.GW).toString().isEmpty()){
			 			epMasterSortDO.setGw(inputJSON.get(CommonConstants.GW).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.WAVE) != null && !inputJSON.get(CommonConstants.WAVE).toString().isEmpty()){
			 			epMasterSortDO.setWave(inputJSON.get(CommonConstants.WAVE).toString());
			 		}
			 		epMasterSortDO.setStatus(CommonConstants.ACTIVE);
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			epMasterSortDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			epMasterSortDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		epMasterSortDO.setUpdatedon(new Date());
			 		epMasterSortDO.setCreatedon(new Date());
			 	}
				if(!epMasterSortService.persist(epMasterSortDO)){
		 			return CommonWebUtil.buildErrorResponse("Sort Already Added").toString();
				}
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Sort Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<EPMasterSortDO> cagtegoryList = epMasterSortService.retrieveActive();
				respJSON = EPMasterSortUtil.getEPMasterSortList(cagtegoryList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<EPMasterSortDO> categoryList = epMasterSortService.retrieve();
				respJSON = EPMasterSortUtil.getEPMasterSortList(categoryList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EPMasterSortDO sortDO = new EPMasterSortDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<EPMasterSortDO> sortList = epMasterSortService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(sortList != null && sortList.size() >0){
			 			sortDO = sortList.get(0);
			 			if(inputJSON.get(CommonConstants.SORT) != null && !inputJSON.get(CommonConstants.SORT).toString().isEmpty()){
			 				sortDO.setSortName(inputJSON.get(CommonConstants.SORT).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.TC) != null && !inputJSON.get(CommonConstants.TC).toString().isEmpty()){
				 			sortDO.setTc(inputJSON.get(CommonConstants.TC).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.YC) != null && !inputJSON.get(CommonConstants.YC).toString().isEmpty()){
				 			sortDO.setYc(inputJSON.get(CommonConstants.YC).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.GW) != null && !inputJSON.get(CommonConstants.GW).toString().isEmpty()){
				 			sortDO.setGw(inputJSON.get(CommonConstants.GW).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.WAVE) != null && !inputJSON.get(CommonConstants.WAVE).toString().isEmpty()){
				 			sortDO.setWave(inputJSON.get(CommonConstants.WAVE).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
				 			sortDO.setStatus((char) inputJSON.get(CommonConstants.STATUS).toString().charAt(0));
				 		}
				 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			sortDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		sortDO.setUpdatedon(new Date());
				 		if(!epMasterSortService.update(sortDO)){
				 			return CommonWebUtil.buildErrorResponse("Sort Already Added").toString();
				 		}
				 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Sort Updated");
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrievewithocn", method = RequestMethod.GET)
	public @ResponseBody String retrievewithocn(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<EPMasterSortDO> shortList = epMasterSortService.retrieve();
				respJSON = EPMasterSortUtil.getEPMasterSortListWithOCN(shortList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	
	/*@RequestMapping(value = "/importEarningType", method = RequestMethod.POST)
	public @ResponseBody String importEarningType(Model model, HttpServletRequest request) {
		try {
			JSONObject inputJSON =  CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
			JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
			List<EarningTypeDO> earningTypelist = new ArrayList<EarningTypeDO>();
			Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY);
			for (int i=0; i < fileData.size(); i++){
				JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
				EarningTypeDO earningType = new EarningTypeDO();
				
				if (colName.get(CommonConstants.NAME) != null && !colName.get(CommonConstants.NAME).toString().isEmpty()){
					earningType.setName(rowJSON.get(colName.get(CommonConstants.NAME)).toString());
		 		}
				
				if (colName.get(CommonConstants.DISPLAYNAME) != null && !colName.get(CommonConstants.DISPLAYNAME).toString().isEmpty()){
					earningType.setDisplayname(rowJSON.get(colName.get(CommonConstants.DISPLAYNAME)).toString());
		 		}
		 		if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
		 			earningType.setUpdatedby(updatedBy.toString());
		 			earningType.setCreatedby(updatedBy.toString());
		 		}
		 		earningType.setUpdatedon(new Date());
		 		earningType.setCreatedon(new Date());
		 		earningType.setStatus(CommonConstants.ACTIVE);
		 		earningTypelist.add(earningType);
			}
			earningTypeService.persistList(earningTypelist);
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}*/
}
