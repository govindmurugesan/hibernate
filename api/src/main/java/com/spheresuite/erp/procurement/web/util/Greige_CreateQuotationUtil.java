package com.spheresuite.erp.procurement.web.util;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.Greige_CreateQuotationDO;
import com.spheresuite.erp.domainobject.Greige_QuotatioDetailsDO;
import com.spheresuite.erp.procurement.service.Greige_QuotationDetailService;
import com.spheresuite.erp.procurement.service.Greige_Supplier_QuoteService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;

@Component
public class Greige_CreateQuotationUtil {
	
	private Greige_CreateQuotationUtil() {}
	
	@Autowired
	private  Greige_Supplier_QuoteService tgreige_Spplier_QuoteService;
	private static Greige_Supplier_QuoteService greige_Spplier_QuoteService;
	
	@Autowired
	private  Greige_QuotationDetailService tgreige_QuotationDetailService;
	private static Greige_QuotationDetailService greige_QuotationDetailService;
	
	@PostConstruct
	public void init() {
		greige_Spplier_QuoteService = tgreige_Spplier_QuoteService;
		greige_QuotationDetailService = tgreige_QuotationDetailService;
		
	}
	
	public static JSONObject getQuotationList(List<Greige_CreateQuotationDO> quotationDetailList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (Greige_CreateQuotationDO quotationDetail : quotationDetailList) {
				resultJSONArray.put(getQuotationDetailObject(quotationDetail));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getQuotationDetailObject(Greige_CreateQuotationDO quotationDetail)throws JSONException{
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(quotationDetail.getQuoteId()));
		
		if(quotationDetail.getSortName() != null){
			result.put(CommonConstants.SORT, String.valueOf(quotationDetail.getSortName().getSortName()));
			result.put(CommonConstants.SORT_ID, String.valueOf(quotationDetail.getSortName().getSortId()));
		}else{
			result.put(CommonConstants.SORT, "");
			result.put(CommonConstants.SORT_ID, "");
		}
		if(quotationDetail.getGrCons()!= null){
			result.put(CommonConstants.GRCONS, String.valueOf(quotationDetail.getGrCons()));
		}else{
			result.put(CommonConstants.GRCONS, "");
		}
		if(quotationDetail.getQuoteType() != null){
			result.put(CommonConstants.TYPE, String.valueOf(quotationDetail.getQuoteType()));
		}else{
			result.put(CommonConstants.TYPE, "");
		}
		if(quotationDetail.getGreigeReqDueDate() != null){
			result.put(CommonConstants.GREIGREQDUODATE, String.valueOf(CommonUtil.convertDateToStringWithtime(quotationDetail.getGreigeReqDueDate())));
		}else{
			result.put(CommonConstants.GREIGREQDUODATE, "");
		}
		if(quotationDetail.getQuoteDue() != null){
			result.put(CommonConstants.GREIGREQDUEHOURSID, String.valueOf(quotationDetail.getQuoteDue().getQuoteDueDateId()));
			result.put(CommonConstants.GREIGREQDUEHOURS, String.valueOf(quotationDetail.getQuoteDue().getHours()));
		}else{
			result.put(CommonConstants.GREIGREQDUEHOURSID, "");
			result.put(CommonConstants.GREIGREQDUEHOURS, "");
		}
		if(quotationDetail.getGreigReqDate() != null){
			result.put(CommonConstants.GREIGREQDATE, String.valueOf(quotationDetail.getGreigReqDate()));
		}else{
			result.put(CommonConstants.GREIGREQDATE, "");
		}
		if(quotationDetail.getGw() != null){
			result.put(CommonConstants.GW, String.valueOf(quotationDetail.getGw()));
		}else{
			result.put(CommonConstants.GW, "");
		}
		if(quotationDetail.getRequiredMeter() != null){
			result.put(CommonConstants.REQUIREDMETER, String.valueOf(quotationDetail.getRequiredMeter()));
		}else{
			result.put(CommonConstants.REQUIREDMETER, "");
		}
		if(quotationDetail.getPendingRequiredMeter() != null){
			result.put(CommonConstants.PENDINGQTY, String.valueOf(quotationDetail.getPendingRequiredMeter()));
		}else{
			result.put(CommonConstants.PENDINGQTY, "");
		}
		if(quotationDetail.getTargetPrice() != null){
			result.put(CommonConstants.TARGETPRICE, String.valueOf(quotationDetail.getTargetPrice()));
		}else{
			result.put(CommonConstants.TARGETPRICE, "");
		}
		if(quotationDetail.getTc() != null){
			result.put(CommonConstants.TC, String.valueOf(quotationDetail.getTc()));
		}else{
			result.put(CommonConstants.TC, "");
		}
		
		
		if(quotationDetail.getYc() != null && quotationDetail.getYc() != null){
			result.put(CommonConstants.YC, String.valueOf(quotationDetail.getYc()));
		}else{
			result.put(CommonConstants.YC, "");
		}
		
		if(quotationDetail.getWave() != null){
			result.put(CommonConstants.WAVE, String.valueOf(quotationDetail.getWave()));
		}else{
			result.put(CommonConstants.WAVE, "");
		}
		
		if(quotationDetail.getStatus() != null){
			result.put(CommonConstants.STATUS, String.valueOf(quotationDetail.getStatus()));
		}else{
			result.put(CommonConstants.STATUS, "");
		}
		List<Greige_QuotatioDetailsDO> orderList = greige_QuotationDetailService.retrieveByQuoteId(Long.parseLong(quotationDetail.getQuoteId().toString()));
		
		if(orderList != null && orderList.size()>0){
			JSONArray resultJSONArray = new JSONArray();
			for(int i= 0; i < orderList.size(); i++){
				resultJSONArray.put(getOcnDetail(orderList.get(i)));
			}
			result.put(CommonConstants.OCNDETAILS, resultJSONArray);
		}
		if(quotationDetail.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(quotationDetail.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(quotationDetail.getUpdatedon())));
		return result;
	}
	
	public static JSONObject getOcnDetail(Greige_QuotatioDetailsDO  order)throws JSONException{
		JSONObject result = new JSONObject();
		if(order.getOcn() != null && order.getOcn().getOcn() != null)result.put(CommonConstants.OCN, String.valueOf(order.getOcn().getOcn().trim()));
		
		return result;
	}
	
	/*public static JSONObject getQuotationListWithSupplierQuotation(List<Greige_CreateQuotationDO> quotationDetailList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (Greige_CreateQuotationDO quotationDetail : quotationDetailList) {
				resultJSONArray.put(getQuotationDetailDetailObject(quotationDetail));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getQuotationDetailDetailObject(Greige_CreateQuotationDO quotationDetail)throws JSONException{
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(quotationDetail.getQuoteId()));
		
		if(quotationDetail.getSortName() != null){
			result.put(CommonConstants.SORT, String.valueOf(quotationDetail.getSortName().getSortName()));
			result.put(CommonConstants.SORT_ID, String.valueOf(quotationDetail.getSortName().getSortId()));
		}else{
			result.put(CommonConstants.SORT, "");
			result.put(CommonConstants.SORT_ID, "");
		}
		if(quotationDetail.getGrCons()!= null){
			result.put(CommonConstants.GRCONS, String.valueOf(quotationDetail.getGrCons()));
		}else{
			result.put(CommonConstants.GRCONS, "");
		}
		if(quotationDetail.getGreigeReqDueDate() != null){
			result.put(CommonConstants.GREIGREQDUODATE, String.valueOf(quotationDetail.getGreigeReqDueDate()));
		}else{
			result.put(CommonConstants.GREIGREQDUODATE, "");
		}
		if(quotationDetail.getGreigReqDate() != null){
			result.put(CommonConstants.GREIGREQDATE, String.valueOf(quotationDetail.getGreigReqDate()));
		}else{
			result.put(CommonConstants.GREIGREQDATE, "");
		}
		if(quotationDetail.getGw() != null){
			result.put(CommonConstants.GW, String.valueOf(quotationDetail.getGw()));
		}else{
			result.put(CommonConstants.GW, "");
		}
		if(quotationDetail.getRequiredMeter() != null){
			result.put(CommonConstants.REQUIREDMETER, String.valueOf(quotationDetail.getRequiredMeter()));
		}else{
			result.put(CommonConstants.REQUIREDMETER, "");
		}
		if(quotationDetail.getTargetPrice() != null){
			result.put(CommonConstants.TARGETPRICE, String.valueOf(quotationDetail.getTargetPrice()));
		}else{
			result.put(CommonConstants.TARGETPRICE, "");
		}
		if(quotationDetail.getTc() != null){
			result.put(CommonConstants.TC, String.valueOf(quotationDetail.getTc()));
		}else{
			result.put(CommonConstants.TC, "");
		}
		
		if(quotationDetail.getYc() != null && quotationDetail.getYc() != null){
			result.put(CommonConstants.YC, String.valueOf(quotationDetail.getYc()));
		}else{
			result.put(CommonConstants.YC, "");
		}
		
		if(quotationDetail.getWave() != null){
			result.put(CommonConstants.WAVE, String.valueOf(quotationDetail.getWave()));
		}else{
			result.put(CommonConstants.WAVE, "");
		}
		if(quotationDetail.getGreigeReqDueDate() != null){
			result.put(CommonConstants.DUEDATE, String.valueOf(quotationDetail.getGreigeReqDueDate()));
		}else{
			result.put(CommonConstants.DUEDATE, "");
		}
		if(quotationDetail.getQuoteType() != null){
			result.put(CommonConstants.TYPE, String.valueOf(quotationDetail.getQuoteType()));
		}else{
			result.put(CommonConstants.TYPE, "");
		}
		List<Greige_Spplier_QuoteDO> quotationDetailsList = greige_Spplier_QuoteService.retrieveByQuoteId(Long.parseLong(quotationDetail.getQuoteId().toString()));
 		if(quotationDetailsList != null && quotationDetailsList.size() >0){
 			if(quotationDetailsList.get(0).getExmillrate() != null){
 				result.put(CommonConstants.RATE, String.valueOf(quotationDetailsList.get(0).getExmillrate()));
	 		}else{
	 			result.put(CommonConstants.RATE, String.valueOf(""));
	 		}
 			
 			if(quotationDetailsList.get(0).getFinalizedrate() != null){
 				result.put(CommonConstants.FINALIZEDRATE, String.valueOf(quotationDetailsList.get(0).getFinalizedrate()));
	 		}else{
	 			result.put(CommonConstants.FINALIZEDRATE, String.valueOf(""));
	 		}
 			
 			if(quotationDetailsList.get(0).getStatus() != null){
 				result.put(CommonConstants.STATUS, String.valueOf(quotationDetailsList.get(0).getStatus()));
	 		}else{
	 			result.put(CommonConstants.STATUS, String.valueOf(""));
	 		}
 		}else{
 			result.put(CommonConstants.STATUS, CommonConstants.PEDING); 
 		}
		
		if(quotationDetail.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(quotationDetail.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(quotationDetail.getUpdatedon())));
		return result;
	}*/
	
	public static JSONObject getPendingOrderDetails(List<Greige_QuotatioDetailsDO> quotationDetailList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (Greige_QuotatioDetailsDO quotationDetail : quotationDetailList) {
				resultJSONArray.put(getPendingOrderDetailObject(quotationDetail));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getPendingOrderDetailObject(Greige_QuotatioDetailsDO quotationDetail)throws JSONException{
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(quotationDetail.getQuoteDetailId()));
		if(quotationDetail.getOcn() != null){
			if(quotationDetail.getOcn().getGreigReq() != null){
				result.put(CommonConstants.GREIGREG, String.valueOf(quotationDetail.getOcn().getGreigReq()));
			}else{
				result.put(CommonConstants.GREIGREG, String.valueOf(""));
			}
			
			/*if(quotationDetail.getOcn().getPendigDays() != null){
				result.put(CommonConstants.PENDINGDAYS, String.valueOf(quotationDetail.getOcn().getPendigDays()));
			}else{
				result.put(CommonConstants.PENDINGDAYS, String.valueOf(""));
			}*/
			
			
			
			if(quotationDetail.getOcn().getPlanNo() != null){
				result.put(CommonConstants.PLANNO, String.valueOf(quotationDetail.getOcn().getPlanNo()));
			}else{
				result.put(CommonConstants.PLANNO, String.valueOf(""));
			}
			
			if(quotationDetail.getOcn().getGreigReqDate() != null){
				result.put(CommonConstants.GREIGREQDATE, String.valueOf(quotationDetail.getOcn().getGreigReqDate()));
				Date today = new Date();
				Date dueDate = quotationDetail.getOcn().getCreatedon();
				 long diff = today.getTime() - dueDate.getTime();
			result.put(CommonConstants.PENDINGDAYS, String.valueOf(TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)));
			}else{
				result.put(CommonConstants.GREIGREQDATE, String.valueOf(""));
			}
			
			if(quotationDetail.getOcn().getShippingDate()!= null){
				result.put(CommonConstants.SHIPPINGDATE, String.valueOf(quotationDetail.getOcn().getShippingDate()));
			}else{
				result.put(CommonConstants.SHIPPINGDATE, String.valueOf(""));
			}
			
			if(quotationDetail.getOcn().getOcn() != null){
				result.put(CommonConstants.OCN, String.valueOf(quotationDetail.getOcn().getOcn()));
			}else{
				result.put(CommonConstants.OCN, String.valueOf(""));
			}
		}
		
		
		
		return result;
	}
	
	
	
}
