package com.spheresuite.erp.procurement.web.util;

import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.Greige_NotificationSettingsDO;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
@Component
public class Greige_NotificationSettingsUtil {
	
	private Greige_NotificationSettingsUtil() {}
	
	@Autowired
	private  EmployeeService temployeeService;
	private static EmployeeService employeeService;
	
	@PostConstruct
	public void init() {
		employeeService = temployeeService;
	}
	
	
	public static JSONObject getNotificationSettingsList(List<Greige_NotificationSettingsDO> notificationSettingsList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (Greige_NotificationSettingsDO notificationSettingsDO : notificationSettingsList) {
				resultJSONArray.put(getNotificationSettingsDetailObject(notificationSettingsDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getNotificationSettingsDetailObject(Greige_NotificationSettingsDO notificationSettingsDO)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(notificationSettingsDO.getGreigeNotificationSettingId() != null ? notificationSettingsDO.getGreigeNotificationSettingId() : ""));
		JSONArray approverListObj = new JSONArray();
		if(notificationSettingsDO.getApprover() != null){
			List<String> approverList = Arrays.asList(notificationSettingsDO.getApprover().split(CommonConstants.COMMA));
			if(approverList !=null && approverList.size() > 0){
				for(String approver : approverList ){
					List<EmployeeDO> empList = employeeService.retriveByEmpId(approver.toString());
					if(empList != null && empList.size() > 0){
							JSONObject quoteApprover = new JSONObject();
							quoteApprover.put(CommonConstants.EMPID, String.valueOf(empList.get(0).getEmpId()));
							quoteApprover.put(CommonConstants.FIRSTNAME, String.valueOf(empList.get(0).getFirstname()));
							if(empList.get(0).getMiddlename() != null) quoteApprover.put(CommonConstants.MIDDLENAME, String.valueOf(empList.get(0).getMiddlename()));
							if(empList.get(0).getLastname() != null) quoteApprover.put(CommonConstants.LASTNAME, String.valueOf(empList.get(0).getLastname()));
							approverListObj.put(quoteApprover);
					}
				}
			}
		}
		result.put(CommonConstants.APPROVER, approverListObj);
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(notificationSettingsDO.getUpdatedon())));
		if(notificationSettingsDO.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(notificationSettingsDO.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		return result;
	}
}
