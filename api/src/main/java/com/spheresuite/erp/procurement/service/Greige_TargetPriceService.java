package com.spheresuite.erp.procurement.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.domainobject.Greige_TargetPriceDO;
import com.spheresuite.erp.procurement.dao.Greige_TargetPriceDAO;

@Service
@Transactional
public class Greige_TargetPriceService {

	@Autowired
	private Greige_TargetPriceDAO greigeTargetPriceDAO;
	
	@Transactional
	public boolean persist(Greige_TargetPriceDO detailsDO){
		return greigeTargetPriceDAO.persist(detailsDO);
	}

/*	@Transactional
	public List<Greige_SortDetailsDO> retrieveActive(){
		return greigeTargetPriceDAO.retrieveActive();
	}*/
	
	@Transactional
	public List<Greige_TargetPriceDO> retrieveById(Long id){
		return greigeTargetPriceDAO.retrieveById(id);
	}
	
	@Transactional
	public List<Greige_TargetPriceDO> retrieve(){
		return greigeTargetPriceDAO.retrieve();
	}
	
	@Transactional
	public List<Greige_TargetPriceDO> retrieveAll(){
		return greigeTargetPriceDAO.retrieveAll();
	}
	
	@Transactional
	public boolean update(Greige_TargetPriceDO detailsDO){
		return greigeTargetPriceDAO.update(detailsDO);
	}
	
	@Transactional
	public List<Greige_TargetPriceDO> retrieveBySortId(Long id){
		return greigeTargetPriceDAO.retrieveBySortId(id);
	}
	
	@Transactional
	public List<Greige_TargetPriceDO> retrieveBySupplierId(Long id){
		return greigeTargetPriceDAO.retrieveBySupplierId(id);
	}
	
	@Transactional
	public List<Greige_TargetPriceDO> retrieveByQuoteId(Long id){
		return greigeTargetPriceDAO.retrieveByQuoteId(id);
	}
	
	@Transactional
	public List<Greige_TargetPriceDO> retrieveBySupplierSortQuoteId(Long sortId, Long supplierId, String quoteId){
		return greigeTargetPriceDAO.retrieveBySupplierSortQuoteId(sortId,supplierId,quoteId);
	}
}
	
