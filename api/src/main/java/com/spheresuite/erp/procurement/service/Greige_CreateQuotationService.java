package com.spheresuite.erp.procurement.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.domainobject.Greige_CreateQuotationDO;
import com.spheresuite.erp.procurement.dao.Geige_CreateQuotationDAO;

@Service
@Transactional
public class Greige_CreateQuotationService {

	@Autowired
	private Geige_CreateQuotationDAO greigeCreateQuotationDAO;
	
	@Transactional
	public  Greige_CreateQuotationDO persist(Greige_CreateQuotationDO detailsDO){
		return greigeCreateQuotationDAO.persist(detailsDO);
	}

/*	@Transactional
	public List<Greige_SortDetailsDO> retrieveActive(){
		return greigeSortDetailsDAO.retrieveActive();
	}*/
	
	@Transactional
	public List<Greige_CreateQuotationDO> retrieveById(Long id){
		return greigeCreateQuotationDAO.retrieveById(id);
	}
	
	@Transactional
	public List<Greige_CreateQuotationDO> retrieve(){
		return greigeCreateQuotationDAO.retrieve();
	}
	
	@Transactional
	public List<Greige_CreateQuotationDO> retrieveDraftQuote(){
		return greigeCreateQuotationDAO.retrieveDraftQuote();
	}
	
	@Transactional
	public boolean update(Greige_CreateQuotationDO detailsDO){
		return greigeCreateQuotationDAO.update(detailsDO);
	}
	
	@Transactional
	public boolean delete(Long id){
		return greigeCreateQuotationDAO.delete(id);
	}
	
	@Transactional
	public List<Greige_CreateQuotationDO> retrieveRequestQuote(){
		return greigeCreateQuotationDAO.retrieveRequestQuote();
	}
	
	@Transactional
	public List<Greige_CreateQuotationDO> retrieveQuoteBySortId(Long id){
		return greigeCreateQuotationDAO.retrieveQuoteBySortId(id);
	}
	
	@Transactional
	public List<Greige_CreateQuotationDO> retrieveQuoteBySortList(List<Long> ids){
		return greigeCreateQuotationDAO.retrieveQuoteBySortList(ids);
	}
	
	@Transactional
	public List<Greige_CreateQuotationDO> retrieveByQuoteDueDate(Date date,String type){
		return greigeCreateQuotationDAO.retrieveByQuoteDueDate(date, type);
	}
	
	@Transactional
	public List<Greige_CreateQuotationDO> retrieveByQuoteType(String type){
		return greigeCreateQuotationDAO.retrieveByQuoteType(type);
	}
	
	@Transactional
	public List<Greige_CreateQuotationDO> retriveForApproval(){
		return greigeCreateQuotationDAO.retriveForApproval();
	}
	
	@Transactional
	public List<Greige_CreateQuotationDO> retrieveReqCount(){
		return greigeCreateQuotationDAO.retriveReqCount();
	}
	
	@Transactional
	public List<Greige_CreateQuotationDO> retrieveEditCount(){
		return greigeCreateQuotationDAO.retriveEditCount();
	}
	
	
	
}
	
