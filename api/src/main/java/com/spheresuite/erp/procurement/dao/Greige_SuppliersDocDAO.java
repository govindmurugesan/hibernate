package com.spheresuite.erp.procurement.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.Greige_SuppliersDocDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class Greige_SuppliersDocDAO {
	static Logger logger = Logger.getLogger(Greige_SuppliersDocDAO.class.getName());

	@Autowired
	private GenericDAOImpl<Greige_SuppliersDocDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public boolean delete(Long id) {
		boolean deleteStatus = true;
		try {
			this.sessionFactory.getCurrentSession().createQuery("delete from Greige_SuppliersDocDO e where e.suppliers.supplierQuoteId=:id")
										.setParameter(CommonConstants.ID, id)
										.executeUpdate();
		} catch (Exception eException) {
			deleteStatus = false;
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return deleteStatus;
	}
	
	
	public Greige_SuppliersDocDO persist(Greige_SuppliersDocDO suppliersDocDO) {
		try {
			if (suppliersDocDO != null) {
				genericObject.persist(suppliersDocDO);
			}
		} catch (Exception eException) {
			//throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			//SessionManager.closeEntityManager(em);
		}
		return suppliersDocDO;
	}
	
	
	public List<Greige_SuppliersDocDO> retrieve() {
		List<Greige_SuppliersDocDO> OpportunitiesDocList = null;
		try {
			OpportunitiesDocList = genericObject.retrieve(Greige_SuppliersDocDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return OpportunitiesDocList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Greige_SuppliersDocDO> retrieveByOppId(Long id) {
		List<Greige_SuppliersDocDO> OpportunitiesDocList = null;
		try {
			OpportunitiesDocList = this.sessionFactory.getCurrentSession().getNamedQuery(Greige_SuppliersDocDO.FIND_BY_SUPPLIERS_QUOTE_ID)
										.setParameter(CommonConstants.ID, id)
										.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return OpportunitiesDocList;
	}
	
	public Greige_SuppliersDocDO update(Greige_SuppliersDocDO suppliersDocDO) {
		try {
			genericObject.merge(suppliersDocDO);
		} catch (Exception eException) {
		} finally {
		}
		return suppliersDocDO;
	}

}
