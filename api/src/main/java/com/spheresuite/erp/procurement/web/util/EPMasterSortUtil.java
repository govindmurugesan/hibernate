package com.spheresuite.erp.procurement.web.util;

import java.util.List;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.EPMasterSortDO;
import com.spheresuite.erp.domainobject.Greige_PendingOrdersDO;
import com.spheresuite.erp.procurement.service.Greige_PendingOrdersService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;

@Component
public class EPMasterSortUtil {
	
	private EPMasterSortUtil() {}
	
	
	@Autowired
	private  Greige_PendingOrdersService tgreige_PendingDetailsService;
	private static Greige_PendingOrdersService greige_PendingDetailsService;
	
	@PostConstruct
	public void init() {
		greige_PendingDetailsService = tgreige_PendingDetailsService;
	}
	
	public static JSONObject getEPMasterSortList(List<EPMasterSortDO> sortList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EPMasterSortDO sort : sortList) {
				resultJSONArray.put(getEPMasterSortDetailObject(sort));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getEPMasterSortDetailObject(EPMasterSortDO sort)throws JSONException{
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(sort.getSortId()));
		if(sort.getSortName() != null){
			result.put(CommonConstants.SORT, String.valueOf(sort.getSortName()));
		}else{
			result.put(CommonConstants.SORT, "");
		}
		if(sort.getGrCons() != null){
			result.put(CommonConstants.GRCONS, String.valueOf(sort.getGrCons()));
		}else{
			result.put(CommonConstants.GRCONS, "");
		}
		if(sort.getGw() != null){
			result.put(CommonConstants.GW, String.valueOf(sort.getGw()));
		}else{
			result.put(CommonConstants.GW, "");
		}
		if(sort.getWave() != null){
			result.put(CommonConstants.WAVE, String.valueOf(sort.getWave()));
		}else{
			result.put(CommonConstants.WAVE, "");
		}
		if(sort.getTc() != null){
			result.put(CommonConstants.TC, String.valueOf(sort.getTc()));
		}else{
			result.put(CommonConstants.TC, "");
		}
		if(sort.getYc() != null){
			result.put(CommonConstants.YC, String.valueOf(sort.getYc()));
		}else{
			result.put(CommonConstants.YC, "");
		}
		if(sort.getGrCons() != null){
			result.put(CommonConstants.GRCONS, String.valueOf(sort.getGrCons()));
		}else{
			result.put(CommonConstants.GRCONS, "");
		}
		result.put(CommonConstants.STATUS, String.valueOf(sort.getStatus()));
		if(sort.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(sort.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(sort.getUpdatedon())));
		return result;
	}
	
	public static JSONObject getEPMasterSortListWithOCN(List<EPMasterSortDO> sortList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EPMasterSortDO sort : sortList) {
				resultJSONArray.put(getEPMasterSortDetailObjectWithOCN(sort));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getEPMasterSortDetailObjectWithOCN(EPMasterSortDO sort)throws JSONException{
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(sort.getSortId()));
		if(sort.getSortName() != null){
			result.put(CommonConstants.SORT, String.valueOf(sort.getSortName()));
		}else{
			result.put(CommonConstants.SORT, "");
		}
		if(sort.getGrCons() != null){
			result.put(CommonConstants.GRCONS, String.valueOf(sort.getGrCons()));
		}else{
			result.put(CommonConstants.GRCONS, "");
		}
		if(sort.getGw() != null){
			result.put(CommonConstants.GW, String.valueOf(sort.getGw()));
		}else{
			result.put(CommonConstants.GW, "");
		}
		if(sort.getWave() != null){
			result.put(CommonConstants.WAVE, String.valueOf(sort.getWave()));
		}else{
			result.put(CommonConstants.WAVE, "");
		}
		if(sort.getTc() != null){
			result.put(CommonConstants.TC, String.valueOf(sort.getTc()));
		}else{
			result.put(CommonConstants.TC, "");
		}
		if(sort.getYc() != null){
			result.put(CommonConstants.YC, String.valueOf(sort.getYc()));
		}else{
			result.put(CommonConstants.YC, "");
		}
		if(sort.getGrCons() != null){
			result.put(CommonConstants.GRCONS, String.valueOf(sort.getGrCons()));
		}else{
			result.put(CommonConstants.GRCONS, "");
		}
		
		List<Greige_PendingOrdersDO> orderList = greige_PendingDetailsService.retrieveBySortId(Long.parseLong(sort.getSortId().toString()));
		
		if(orderList != null && orderList.size()>0){
			JSONArray resultJSONArray = new JSONArray();
			result.put(CommonConstants.OCNCOUNT, String.valueOf(orderList.size()));
			for(int i= 0; i < orderList.size(); i++){
				resultJSONArray.put(getOcnDetail(orderList.get(i)));
						
				//resultJSON.put("ocn"+i, String.valueOf(orderList.get(i).getOcn()));
			}
			result.put(CommonConstants.OCNDETAILS, resultJSONArray);
		}
		
		result.put(CommonConstants.STATUS, String.valueOf(sort.getStatus()));
		if(sort.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(sort.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(sort.getUpdatedon())));
		return result;
	}
	
	public static JSONObject getOcnDetail(Greige_PendingOrdersDO  order)throws JSONException{
		JSONObject result = new JSONObject();
		result.put(CommonConstants.OCN, String.valueOf(order.getOcn()));
		//result.put(CommonConstants.COUNT, String.valueOf(order.getPendigDays()));
		return result;
	}
}
