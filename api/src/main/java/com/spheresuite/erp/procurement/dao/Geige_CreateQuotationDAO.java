package com.spheresuite.erp.procurement.dao;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.Greige_CreateQuotationDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class Geige_CreateQuotationDAO {
	
	static Logger logger = Logger.getLogger(Geige_CreateQuotationDAO.class.getName());
	
	@Autowired
	private GenericDAOImpl<Greige_CreateQuotationDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public  Greige_CreateQuotationDO persist(Greige_CreateQuotationDO detailsDO){
		//List<Greige_CreateQuotationDO> quotationList = null;
		///boolean persistStatus = true;
		try {
			genericObject.persist(detailsDO);
			/*earningTypeList = this.sessionFactory.getCurrentSession().getNamedQuery(EPMasterSortDO.FIND_BY_NAME)
			.setParameter(CommonConstants.NAME, earningTypeDO.getName())
			.list();
			if(earningTypeList != null && earningTypeList.size() > 0){
				persistStatus = false;
			}else{
				genericObject.persist(earningTypeDO);
			}*/
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return detailsDO;
	}
	
	public List<Greige_CreateQuotationDO> retrieve(){
		List<Greige_CreateQuotationDO> DetailsList = null;
		try {
			DetailsList = genericObject.retrieve(Greige_CreateQuotationDO.class);
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return DetailsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Greige_CreateQuotationDO> retrieveById(Long id) {
		List<Greige_CreateQuotationDO> DetailsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(Greige_CreateQuotationDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return DetailsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Greige_CreateQuotationDO> retrieveQuoteBySortId(Long id) {
		List<Greige_CreateQuotationDO> DetailsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(Greige_CreateQuotationDO.FIND_BY_SORTID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return DetailsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Greige_CreateQuotationDO> retrieveQuoteBySortList(List<Long> ids) {
		List<Greige_CreateQuotationDO> DetailsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(Greige_CreateQuotationDO.FIND_BY_SORTIDS)
					.setParameterList(CommonConstants.ID, ids)
					.list();
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return DetailsList;
	}
	
	
	
	@SuppressWarnings("unchecked")
	public List<Greige_CreateQuotationDO> retrieveDraftQuote() {
		List<Greige_CreateQuotationDO> DetailsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(Greige_CreateQuotationDO.FIND_QUOTEBYSTATUS)
					.setParameter(CommonConstants.STATUS, CommonConstants.DSTATUS)
					.list();
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return DetailsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Greige_CreateQuotationDO> retrieveRequestQuote() {
		List<Greige_CreateQuotationDO> DetailsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(Greige_CreateQuotationDO.FIND_QUOTEBYSTATUS)
					.setParameter(CommonConstants.STATUS, CommonConstants.RSTATUS)
					.list();
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return DetailsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Greige_CreateQuotationDO> retriveForApproval() {
		List<Greige_CreateQuotationDO> DetailsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(Greige_CreateQuotationDO.FIND_QUOTEBYAPPROVESTATUS)
					.setParameter(CommonConstants.STATUS, CommonConstants.SSTATUS)
						.setParameter(CommonConstants.APPROVEDSTATUS, CommonConstants.ASTATUS)
					.list();
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return DetailsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Greige_CreateQuotationDO> retriveReqCount() {
		List<Greige_CreateQuotationDO> DetailsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(Greige_CreateQuotationDO.FIND_QUOTE_REQ_COUNT)
					.list();
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return DetailsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Greige_CreateQuotationDO> retriveEditCount() {
		List<Greige_CreateQuotationDO> DetailsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(Greige_CreateQuotationDO.FIND_QUOTE_DRAFT_COUNT)
					.list();
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return DetailsList;
	}
	
	public boolean update(Greige_CreateQuotationDO DetailsList){
		boolean updateStatus = true;
		try {
			genericObject.merge(DetailsList);
			/*List<EPMasterSortDO> sortList = null;
			earningTypeList = this.sessionFactory.getCurrentSession().getNamedQuery(EPMasterSortDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, allowanceTypeDO.getName())
					.setParameter(CommonConstants.ID, allowanceTypeDO.getEarningTypeId())
					.list();
			if(earningTypeList != null && earningTypeList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(sortList);
			}*/
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
		}
		return updateStatus;
	}
	
	public boolean delete(Long id)  {
		try {
			org.hibernate.Query query = this.sessionFactory.getCurrentSession().createQuery("delete from Greige_CreateQuotationDO e where e.quoteId=:id")
					.setParameter(CommonConstants.ID, id);
			query.executeUpdate();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return true;
	}
	
	@SuppressWarnings("unchecked")
	public List<Greige_CreateQuotationDO> retrieveByQuoteDueDate(Date date, String type) {
		List<Greige_CreateQuotationDO> DetailsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(Greige_CreateQuotationDO.FIND_BY_QUOTDUEDATE)
					//.setParameter(CommonConstants.DATE, date)
					.setParameter(CommonConstants.TYPE, type)
					
					.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return DetailsList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<Greige_CreateQuotationDO> retrieveByQuoteType(String type) {
		List<Greige_CreateQuotationDO> DetailsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(Greige_CreateQuotationDO.FIND_BY_QUOTTYPE)
					.setParameter(CommonConstants.TYPE, type)
					.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return DetailsList;
	}
	

}
