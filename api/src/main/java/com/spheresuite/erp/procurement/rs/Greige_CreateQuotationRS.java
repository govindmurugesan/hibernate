package com.spheresuite.erp.procurement.rs;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.EPMasterQuoteDueDateDO;
import com.spheresuite.erp.domainobject.EPMasterSortDO;
import com.spheresuite.erp.domainobject.Greige_CreateQuotationDO;
import com.spheresuite.erp.domainobject.Greige_PendingOrdersDO;
import com.spheresuite.erp.domainobject.Greige_QuotatioDetailsDO;
import com.spheresuite.erp.domainobject.Greige_Spplier_QuoteDO;
import com.spheresuite.erp.domainobject.SuppliersDO;
import com.spheresuite.erp.procurement.service.EPMasterQuoteDueDateService;
import com.spheresuite.erp.procurement.service.EPMasterSortService;
import com.spheresuite.erp.procurement.service.Greige_CreateQuotationService;
import com.spheresuite.erp.procurement.service.Greige_PendingOrdersService;
import com.spheresuite.erp.procurement.service.Greige_QuotationDetailService;
import com.spheresuite.erp.procurement.service.Greige_Supplier_QuoteService;
import com.spheresuite.erp.procurement.web.util.Greige_CreateQuotationUtil;
import com.spheresuite.erp.procurement.web.util.Greige_SupplierQuoteUtil;
import com.spheresuite.erp.supplier.service.SupplierService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/greige_createQuotation")
public class Greige_CreateQuotationRS {

	String validation = null;
	static Logger logger = Logger.getLogger(Greige_CreateQuotationRS.class.getName());
	
	@Autowired
	private Greige_CreateQuotationService geige_createQuotationService;
	
	@Autowired
	private Greige_QuotationDetailService greige_QuotationDetailService;
	
	@Autowired
	private Greige_Supplier_QuoteService  greigeSupplierQuoteService;
	
	@Autowired
	private EPMasterSortService epMasterSortService;
	
	@Autowired
	private EPMasterQuoteDueDateService epMasterQuoteDueService;
	
	@Autowired
	private Greige_PendingOrdersService greige_PendingOrdersService;
	
	@Autowired
	private SupplierService supplierService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		Greige_CreateQuotationDO quotDetailsDO = new Greige_CreateQuotationDO();
			 		if(inputJSON.get(CommonConstants.SORT_ID) != null && !inputJSON.get(CommonConstants.SORT_ID).toString().isEmpty()){
			 			List <EPMasterSortDO> sortInfo = epMasterSortService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.SORT_ID).toString()));
			 			if(sortInfo != null && sortInfo.size() > 0 ){
			 				quotDetailsDO.setSortName(sortInfo.get(0));
			 				quotDetailsDO.setYc(sortInfo.get(0).getYc());
			 				quotDetailsDO.setTc(sortInfo.get(0).getTc());
			 				quotDetailsDO.setWave(sortInfo.get(0).getWave());
			 				quotDetailsDO.setGrCons(sortInfo.get(0).getGrCons());
			 				quotDetailsDO.setGw(sortInfo.get(0).getGw());
			 			}
		 			}
			 		/*if(inputJSON.get(CommonConstants.YC) != null && !inputJSON.get(CommonConstants.YC).toString().isEmpty()){
			 			quotDetailsDO.setYc(inputJSON.get(CommonConstants.YC).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.TC) != null && !inputJSON.get(CommonConstants.TC).toString().isEmpty()){
			 			quotDetailsDO.setTc(inputJSON.get(CommonConstants.TC).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.WAVE) != null && !inputJSON.get(CommonConstants.WAVE).toString().isEmpty()){
			 			quotDetailsDO.setWave(inputJSON.get(CommonConstants.WAVE).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.GRCONS) != null && !inputJSON.get(CommonConstants.GRCONS).toString().isEmpty()){
			 			quotDetailsDO.setGrCons(inputJSON.get(CommonConstants.GRCONS).toString());
			 		}*/
			 		if(inputJSON.get(CommonConstants.TYPE) != null && !inputJSON.get(CommonConstants.TYPE).toString().isEmpty()){
			 			quotDetailsDO.setQuoteType(inputJSON.get(CommonConstants.TYPE).toString());
			 		}
			 		/*if(inputJSON.get(CommonConstants.GW) != null && !inputJSON.get(CommonConstants.GW).toString().isEmpty()){
			 			quotDetailsDO.setGw(inputJSON.get(CommonConstants.GW).toString());
			 		}*/
			 		if(inputJSON.get(CommonConstants.REQUIREDMETER) != null && !inputJSON.get(CommonConstants.REQUIREDMETER).toString().isEmpty()){
			 			quotDetailsDO.setRequiredMeter(inputJSON.get(CommonConstants.REQUIREDMETER).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.TARGETPRICE) != null && !inputJSON.get(CommonConstants.TARGETPRICE).toString().isEmpty()){
			 			quotDetailsDO.setTargetPrice(Long.parseLong(inputJSON.get(CommonConstants.TARGETPRICE).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.GREIGREQDATE) != null && !inputJSON.get(CommonConstants.GREIGREQDATE).toString().isEmpty()){
			 			quotDetailsDO.setGreigReqDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.GREIGREQDATE).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.GREIGREQDUEHOURSID) != null && !inputJSON.get(CommonConstants.GREIGREQDUEHOURSID).toString().isEmpty()){
			 			List <EPMasterQuoteDueDateDO> hoursList = epMasterQuoteDueService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.GREIGREQDUEHOURSID).toString()));
			 			if(hoursList != null && hoursList.size() > 0 ){
			 				quotDetailsDO.setQuoteDue(hoursList.get(0));
			 				Calendar cal = Calendar.getInstance(); // creates calendar
				 		    cal.setTime(new Date()); // sets calendar time/date
				 		    cal.add(Calendar.HOUR_OF_DAY, Integer.parseInt(hoursList.get(0).getHours())); // adds one hour
				 		    cal.getTime();
				 		   quotDetailsDO.setGreigeReqDueDate(cal.getTime());
			 			}
			 		}
			 		/*if(inputJSON.get(CommonConstants.GREIGREQDUODATE) != null && !inputJSON.get(CommonConstants.GREIGREQDUODATE).toString().isEmpty()){
			 			quotDetailsDO.setGreigeReqDueDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.GREIGREQDUODATE).toString()));
			 		}*/
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			quotDetailsDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			quotDetailsDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		quotDetailsDO.setStatus(CommonConstants.DSTATUS);
			 		quotDetailsDO.setUpdatedon(new Date());
			 		quotDetailsDO.setCreatedon(new Date());
			 		Greige_CreateQuotationDO quotationList = geige_createQuotationService.persist(quotDetailsDO);
			 		
			 		if(quotationList != null && quotationList.getQuoteId() != null){
			 		
			 			if (inputJSON.get(CommonConstants.PENDINGORDERID) != null && !inputJSON.get(CommonConstants.PENDINGORDERID).toString().isEmpty()){
					 		JSONArray resultJSONArray = new JSONArray();
					 		resultJSONArray.put(inputJSON.get(CommonConstants.PENDINGORDERID));
					 		String rec = (String) resultJSONArray.get(0).toString();
					 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
					 		for (int i = 0; i < resultJSONArray1.size(); i++) {
					 			if(resultJSONArray1.get(i) != null && !resultJSONArray1.get(i).toString().isEmpty()){
					 				Greige_QuotatioDetailsDO greQuotDetailsDO = new Greige_QuotatioDetailsDO();
					 				if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 					greQuotDetailsDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 					greQuotDetailsDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
							 		}
					 				greQuotDetailsDO.setUpdatedon(new Date());
					 				greQuotDetailsDO.setCreatedon(new Date());
					 				greQuotDetailsDO.setQuotation(quotationList);
					 				if(resultJSONArray1.get(i)!=null && !resultJSONArray1.get(i).toString().isEmpty()){
						 				List <Greige_PendingOrdersDO> pendingOrder = greige_PendingOrdersService.retrieveById(Long.parseLong(resultJSONArray1.get(i).toString()));
							 			if(pendingOrder != null && pendingOrder.size() > 0 ){
							 				greQuotDetailsDO.setOcn(pendingOrder.get(0));
							 				Greige_PendingOrdersDO sortDetailDO = new Greige_PendingOrdersDO();
							 				sortDetailDO = pendingOrder.get(0);
							 				sortDetailDO.setStatus("s");
							 				sortDetailDO.setUpdatedon(new Date());
							 				if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
							 					sortDetailDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
									 		}
							 				if(!greige_PendingOrdersService.update(sortDetailDO)){
									 			return CommonWebUtil.buildErrorResponse("Pending Orders Already Updated").toString();
									 		}
							 			}
					 				}
						 			greige_QuotationDetailService.persist(greQuotDetailsDO);
						 		}
					 		}
				 		}
			 		}
			 		
			 		
			 	}
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Quotation Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
/*	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<Greige_CreateQuotationDO> quotationList = geige_createQuotationService.retrieve();
				respJSON = Greige_CreateQuotationUtil.getQuotationList(quotationList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}*/
	
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public @ResponseBody String delete(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<Greige_QuotatioDetailsDO> quoteDetList = greige_QuotationDetailService.retrieveByQuoteId(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(quoteDetList != null && quoteDetList.size() > 0){
			 			for(Greige_QuotatioDetailsDO qt: quoteDetList){
				 			List <Greige_PendingOrdersDO> pendingOrder = greige_PendingOrdersService.retrieveById(qt.getOcn().getPendingOrderId());
				 			if(pendingOrder != null && pendingOrder.size() > 0 ){
				 				Greige_PendingOrdersDO sortDetailDO = new Greige_PendingOrdersDO();
				 				sortDetailDO = pendingOrder.get(0);
				 				sortDetailDO.setStatus(null);
				 				sortDetailDO.setUpdatedon(new Date());
				 				if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 					sortDetailDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
						 		}
				 				if(!greige_PendingOrdersService.update(sortDetailDO)){
						 			return CommonWebUtil.buildErrorResponse("Pending Orders Already Updated").toString();
						 		}
				 			}
			 			}
				 		if(greige_QuotationDetailService.delete(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()))){
				 			if(geige_createQuotationService.delete(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()))){
				 				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Quotation Deleted");
				 			}else{
				 				return CommonWebUtil.buildAuthenticationErrorResponse("").toString();
				 			}
				 		}else{
				 			return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
				 		}
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				Greige_CreateQuotationDO quotationDO = new Greige_CreateQuotationDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<Greige_CreateQuotationDO> quotationDetailsList = geige_createQuotationService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(quotationDetailsList != null && quotationDetailsList.size() >0){
			 			quotationDO = quotationDetailsList.get(0);
			 			if(inputJSON.get(CommonConstants.SORT_ID) != null && !inputJSON.get(CommonConstants.SORT_ID).toString().isEmpty()){
				 			List <EPMasterSortDO> sortInfo = epMasterSortService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.SORT_ID).toString()));
				 			if(sortInfo != null && sortInfo.size() > 0 ){
				 				quotationDO.setSortName(sortInfo.get(0));
				 			}
				 		}
				 		if(inputJSON.get(CommonConstants.YC) != null && !inputJSON.get(CommonConstants.YC).toString().isEmpty()){
				 			quotationDO.setYc(inputJSON.get(CommonConstants.YC).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.TC) != null && !inputJSON.get(CommonConstants.TC).toString().isEmpty()){
				 			quotationDO.setTc(inputJSON.get(CommonConstants.TC).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.WAVE) != null && !inputJSON.get(CommonConstants.WAVE).toString().isEmpty()){
				 			quotationDO.setWave(inputJSON.get(CommonConstants.WAVE).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.TYPE) != null && !inputJSON.get(CommonConstants.TYPE).toString().isEmpty()){
				 			quotationDO.setQuoteType(inputJSON.get(CommonConstants.TYPE).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.GRCONS) != null && !inputJSON.get(CommonConstants.GRCONS).toString().isEmpty()){
				 			quotationDO.setGrCons(inputJSON.get(CommonConstants.GRCONS).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.GW)  != null && !inputJSON.get(CommonConstants.GW).toString().isEmpty()){
				 			quotationDO.setGw(inputJSON.get(CommonConstants.GW).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.REQUIREDMETER) != null && !inputJSON.get(CommonConstants.REQUIREDMETER).toString().isEmpty()){
				 			quotationDO.setRequiredMeter(inputJSON.get(CommonConstants.REQUIREDMETER).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.TARGETPRICE) != null && !inputJSON.get(CommonConstants.TARGETPRICE).toString().isEmpty()){
				 			quotationDO.setTargetPrice(Long.parseLong(inputJSON.get(CommonConstants.TARGETPRICE).toString()));
				 		}
				 		if(inputJSON.get(CommonConstants.GREIGREQDATE)  != null && !inputJSON.get(CommonConstants.GREIGREQDATE).toString().isEmpty()){
				 			quotationDO.setGreigReqDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.GREIGREQDATE).toString()));
				 		}
				 		if(inputJSON.get(CommonConstants.GREIGREQDUODATE)  != null && !inputJSON.get(CommonConstants.GREIGREQDUODATE).toString().isEmpty()){
				 			//quotationDO.setGreigeReqDueDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.GREIGREQDUODATE).toString()));
				 			List <EPMasterQuoteDueDateDO> hoursList = epMasterQuoteDueService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.GREIGREQDUODATE).toString()));
				 			if(hoursList != null && hoursList.size() > 0 ){
				 				quotationDO.setQuoteDue(hoursList.get(0));
				 				Calendar cal = Calendar.getInstance(); // creates calendar
					 		    cal.setTime(new Date()); // sets calendar time/date
					 		    cal.add(Calendar.HOUR_OF_DAY, Integer.parseInt(hoursList.get(0).getHours())); // adds one hour
					 		    cal.getTime();
					 		    quotationDO.setGreigeReqDueDate(cal.getTime());
				 			}
				 		}
				 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			quotationDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		quotationDO.setUpdatedon(new Date());
				 		if(!geige_createQuotationService.update(quotationDO)){
				 			return CommonWebUtil.buildErrorResponse("Quotation Already Updated").toString();
				 		}
				 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Quotation Updated");
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/submitForQuote", method = RequestMethod.POST)
	public @ResponseBody String submitForQuote(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
				List<Greige_CreateQuotationDO> quotationList = geige_createQuotationService.retrieveDraftQuote();
				if(quotationList != null && quotationList.size() > 0){
					for(Greige_CreateQuotationDO quote : quotationList){
						Greige_CreateQuotationDO quotationDO = new Greige_CreateQuotationDO();
						quotationDO = quote;
						quotationDO.setStatus(CommonConstants.RSTATUS);
						quotationDO.setUpdatedon(new Date());
						if(geige_createQuotationService.update(quotationDO)){
							//if (inputJSON.get(CommonConstants.SUPPLIERID) != null && !inputJSON.get(CommonConstants.SUPPLIERID).toString().isEmpty()){
						 		//JSONArray resultJSONArray = new JSONArray();
						 		/*resultJSONArray.put(inputJSON.get(CommonConstants.SUPPLIERID));
						 		String rec = (String) resultJSONArray.get(0).toString();
						 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);*/
							List<SuppliersDO> supplierList = supplierService.retrieveAll();
							if(supplierList != null && supplierList.size() > 0){
						 		for (int i = 0; i < supplierList.size(); i++) {
						 			if(supplierList.get(i) != null && !supplierList.get(i).toString().isEmpty()){
						 				Greige_Spplier_QuoteDO spplierQuoteDO = new Greige_Spplier_QuoteDO();
						 				if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
						 					spplierQuoteDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
						 					spplierQuoteDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
								 		}
						 				spplierQuoteDO.setStatus(CommonConstants.RSTATUS);
						 				//spplierQuoteDO.setTargetprice(quotationDO.getTargetPrice());
						 				spplierQuoteDO.setUpdatedon(new Date());
						 				spplierQuoteDO.setCreatedon(new Date());
						 				spplierQuoteDO.setQuotation(quotationDO);
						 				spplierQuoteDO.setSupplierId(supplierList.get(i).getSupplierId().toString());
						 				greigeSupplierQuoteService.persist(spplierQuoteDO);
							 		}
						 		}
					 		}
						}
						CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Quotation Submitted");
					}
					return CommonWebUtil.buildSuccessResponseId("").toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	/*
	@RequestMapping(value = "/retrieveQuoteForSupplier/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String retrieveQuoteFrorSupplier(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<Greige_CreateQuotationDO> quotationList = geige_createQuotationService.retrieveRequestQuote();
				respJSON = Greige_CreateQuotationUtil.getQuotationListWithSupplierQuotation(quotationList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}*/
	
	@RequestMapping(value = "/retriveOCNByQuoteId/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String retriveById(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<Greige_QuotatioDetailsDO> quotatuionList = greige_QuotationDetailService.retrieveByQuoteId(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = Greige_CreateQuotationUtil.getPendingOrderDetails(quotatuionList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retriveByDate/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String retriveByDate(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.DATE).toString().isEmpty() && !inputJSON.get(CommonConstants.TYPE).toString().isEmpty()){
					List<Greige_CreateQuotationDO> quotatuionList = geige_createQuotationService.retrieveByQuoteDueDate(CommonUtil.convertStringToSqlDate((inputJSON.get(CommonConstants.DATE).toString())), inputJSON.get(CommonConstants.TYPE).toString());
					respJSON = Greige_CreateQuotationUtil.getQuotationList(quotatuionList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retriveByType/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String retriveByType(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.TYPE).toString().isEmpty()){
					List<Greige_CreateQuotationDO> quotatuionList = geige_createQuotationService.retrieveByQuoteType((inputJSON.get(CommonConstants.TYPE).toString()));
					respJSON = Greige_CreateQuotationUtil.getQuotationList(quotatuionList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/updateStatus/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String updateStatus(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				List<Greige_CreateQuotationDO> quotationList = geige_createQuotationService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));;
				if(quotationList != null && quotationList.size() > 0){
						Greige_CreateQuotationDO quotationDO = new Greige_CreateQuotationDO();
						quotationDO = quotationList.get(0);
						if(inputJSON.get(CommonConstants.QUOTSTATUS) != null && !inputJSON.get(CommonConstants.QUOTSTATUS).toString().isEmpty()){
							if(inputJSON.get(CommonConstants.QUOTSTATUS).toString().equalsIgnoreCase("a") ||
									inputJSON.get(CommonConstants.QUOTSTATUS).toString().equalsIgnoreCase("s")){
								quotationDO.setStatus(inputJSON.get(CommonConstants.QUOTSTATUS).toString());
							}
				 		}
						quotationDO.setUpdatedon(new Date());
						if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			quotationDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
						if(geige_createQuotationService.update(quotationDO)){
							if(inputJSON.get(CommonConstants.SUPPLIERID) != null && !inputJSON.get(CommonConstants.SUPPLIERID).toString().isEmpty()){
								//List<Greige_Spplier_QuoteDO> supplierquotationDetailsList = greigeSupplierQuoteService.retriveByQuoteIdAndSupplierId(Long.parseLong(inputJSON.get(CommonConstants.QUOT_ID).toString()),inputJSON.get(CommonConstants.SUPPLIERID).toString());
								List<Greige_Spplier_QuoteDO> supplierquotationDetailsList = greigeSupplierQuoteService.retriveByQuoteIdAndSupplierId(Long.parseLong(inputJSON.get(CommonConstants.SUPPLIER_QUOTE_ID).toString()),Long.parseLong(inputJSON.get(CommonConstants.QUOT_ID).toString()),inputJSON.get(CommonConstants.SUPPLIERID).toString());
								if(supplierquotationDetailsList != null && supplierquotationDetailsList.size() >0){
						 			Greige_Spplier_QuoteDO supplierInfo = new Greige_Spplier_QuoteDO();
						 			supplierInfo = supplierquotationDetailsList.get(0);
						 			if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
						 				supplierInfo.setStatus(inputJSON.get(CommonConstants.STATUS).toString());
							 		}
						 			if(inputJSON.get(CommonConstants.QUOTSTATUS) != null && !inputJSON.get(CommonConstants.QUOTSTATUS).toString().isEmpty()){
						 				supplierInfo.setStatus(inputJSON.get(CommonConstants.QUOTSTATUS).toString());
							 		}
						 			if(inputJSON.get(CommonConstants.SELECTED) != null && !inputJSON.get(CommonConstants.SELECTED).toString().isEmpty()){
						 				supplierInfo.setSubmitted(inputJSON.get(CommonConstants.SELECTED).toString());
							 		}
						 			if(inputJSON.get(CommonConstants.FINALIZEDRATE) != null && !inputJSON.get(CommonConstants.FINALIZEDRATE).toString().isEmpty()){
						 				supplierInfo.setFinalizedrate(Long.parseLong(inputJSON.get(CommonConstants.FINALIZEDRATE).toString()));
							 		}
							 		if(inputJSON.get(CommonConstants.TARGETPRICE) != null && !inputJSON.get(CommonConstants.TARGETPRICE).toString().isEmpty()){
							 			supplierInfo.setTargetprice(Long.parseLong(inputJSON.get(CommonConstants.TARGETPRICE).toString()));
							 		}
						 			if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
						 				supplierInfo.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
							 		}
						 			supplierInfo.setUpdatedon(new Date());
						 			if(!greigeSupplierQuoteService.update(supplierInfo)){
							 			return CommonWebUtil.buildErrorResponse("").toString();
							 		}
						 		}
							}
						}else{
							return CommonWebUtil.buildAuthenticationErrorResponse("").toString();
						}
					return CommonWebUtil.buildSuccessResponseId("").toString();
				}
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Quotation Status Updated");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
			
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	
	@RequestMapping(value = "/retriveForApproval", method = RequestMethod.GET)
	public @ResponseBody String retriveForApproval(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
					/*List<Greige_CreateQuotationDO> quotatuionList = geige_createQuotationService.retriveForApproval();
					respJSON = Greige_CreateQuotationUtil.getQuotationList(quotatuionList).toString();*/
				List<Greige_Spplier_QuoteDO> quotatuionList = greigeSupplierQuoteService.retriveForApproval();
				respJSON = Greige_SupplierQuoteUtil.getQuotationList(quotatuionList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveByQuotePrepared", method = RequestMethod.GET)
	public @ResponseBody String retrieveByQuotePrepared(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<Greige_CreateQuotationDO> quotatuionList = geige_createQuotationService.retrieveDraftQuote();
				respJSON = Greige_CreateQuotationUtil.getQuotationList(quotatuionList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveByQuoteSent", method = RequestMethod.GET)
	public @ResponseBody String retrieveByQuoteSent(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<Greige_CreateQuotationDO> quotatuionList = geige_createQuotationService.retrieveRequestQuote();
				respJSON = Greige_CreateQuotationUtil.getQuotationList(quotatuionList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveByQuoteReceived", method = RequestMethod.GET)
	public @ResponseBody String retrieveByQuoteReceived(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<Greige_Spplier_QuoteDO> quotatuionList = greigeSupplierQuoteService.retrieveByQuoteReceived();
				respJSON = Greige_SupplierQuoteUtil.getQuotationList(quotatuionList).toString();
				/*List<Greige_CreateQuotationDO> quotatuionList = geige_createQuotationService.retrieveByQuoteReceived();
				respJSON = Greige_CreateQuotationUtil.getQuotationList(quotatuionList).toString();*/
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/sendMailToApprover", method = RequestMethod.GET)
	public @ResponseBody String sendMailToApprover(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = new JSONObject();
				CommonUtil.sendMailToReportingHeads(request,inputJSON,"greige_approval","greigeApproval","submit");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/sendMailToSuppliers", method = RequestMethod.GET)
	public @ResponseBody String sendMailToSuppliers(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = new JSONObject();
				CommonUtil.sendMailToReportingHeads(request,inputJSON,"","suppliers","submit");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/persistList", method = RequestMethod.POST)
	public @ResponseBody String addList(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				
				JSONObject inputJSONObj = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
				//JSONObject inputJSONObj = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
				if (inputJSONObj.get(CommonConstants.OCNDETAILS) != null && !inputJSONObj.get(CommonConstants.OCNDETAILS).toString().isEmpty()){
			 		JSONArray resultOCNDetailsJSONArray = new JSONArray();
			 		resultOCNDetailsJSONArray.put(inputJSONObj.get(CommonConstants.OCNDETAILS));
			 		String recOcn = (String) resultOCNDetailsJSONArray.get(0).toString();
			 		org.json.simple.JSONArray resultOCNDetailsJSONArray1 = CommonWebUtil.getInputParamsArray(recOcn);
			 		for (int j = 0; j < resultOCNDetailsJSONArray1.size(); j++) {
			 			JSONObject inputJSON = (JSONObject) resultOCNDetailsJSONArray1.get(j);
					 	if (inputJSON != null){
					 		Greige_CreateQuotationDO quotDetailsDO = new Greige_CreateQuotationDO();
					 		if(inputJSON.get(CommonConstants.SORT_ID) != null && !inputJSON.get(CommonConstants.SORT_ID).toString().isEmpty()){
					 			List <EPMasterSortDO> sortInfo = epMasterSortService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.SORT_ID).toString()));
					 			if(sortInfo != null && sortInfo.size() > 0 ){
					 				quotDetailsDO.setSortName(sortInfo.get(0));
					 				quotDetailsDO.setYc(sortInfo.get(0).getYc());
					 				quotDetailsDO.setTc(sortInfo.get(0).getTc());
					 				quotDetailsDO.setWave(sortInfo.get(0).getWave());
					 				quotDetailsDO.setGrCons(sortInfo.get(0).getGrCons());
					 				quotDetailsDO.setGw(sortInfo.get(0).getGw());
					 			}
				 			}
					 		if(inputJSON.get(CommonConstants.TYPE) != null && !inputJSON.get(CommonConstants.TYPE).toString().isEmpty()){
					 			quotDetailsDO.setQuoteType(inputJSON.get(CommonConstants.TYPE).toString());
					 		}
					 		if(inputJSON.get(CommonConstants.REQUIREDMETER) != null && !inputJSON.get(CommonConstants.REQUIREDMETER).toString().isEmpty()){
					 			quotDetailsDO.setRequiredMeter(inputJSON.get(CommonConstants.REQUIREDMETER).toString());
					 		}
					 		if(inputJSON.get(CommonConstants.TARGETPRICE) != null && !inputJSON.get(CommonConstants.TARGETPRICE).toString().isEmpty()){
					 			quotDetailsDO.setTargetPrice(Long.parseLong(inputJSON.get(CommonConstants.TARGETPRICE).toString()));
					 		}
					 		if(inputJSON.get(CommonConstants.GREIGREQDATE) != null && !inputJSON.get(CommonConstants.GREIGREQDATE).toString().isEmpty()){
					 			quotDetailsDO.setGreigReqDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.GREIGREQDATE).toString()));
					 		}
					 		if(inputJSON.get(CommonConstants.GREIGREQDUEHOURSID) != null && !inputJSON.get(CommonConstants.GREIGREQDUEHOURSID).toString().isEmpty()){
					 			List <EPMasterQuoteDueDateDO> hoursList = epMasterQuoteDueService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.GREIGREQDUEHOURSID).toString()));
					 			if(hoursList != null && hoursList.size() > 0 ){
					 				quotDetailsDO.setQuoteDue(hoursList.get(0));
					 				Calendar cal = Calendar.getInstance(); // creates calendar
						 		    cal.setTime(new Date()); // sets calendar time/date
						 		    cal.add(Calendar.HOUR_OF_DAY, Integer.parseInt(hoursList.get(0).getHours())); // adds one hour
						 		    cal.getTime();
						 		   quotDetailsDO.setGreigeReqDueDate(cal.getTime());
					 			}
					 		}
					 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			quotDetailsDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			quotDetailsDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 		}
					 		quotDetailsDO.setStatus(CommonConstants.DSTATUS);
					 		quotDetailsDO.setUpdatedon(new Date());
					 		quotDetailsDO.setCreatedon(new Date());
					 		Greige_CreateQuotationDO quotationList = geige_createQuotationService.persist(quotDetailsDO);
					 		
					 		if(quotationList != null && quotationList.getQuoteId() != null){
					 		
					 			if (inputJSON.get(CommonConstants.PENDINGORDERID) != null && !inputJSON.get(CommonConstants.PENDINGORDERID).toString().isEmpty()){
							 		JSONArray resultJSONArray = new JSONArray();
							 		resultJSONArray.put(inputJSON.get(CommonConstants.PENDINGORDERID));
							 		String rec = (String) resultJSONArray.get(0).toString();
							 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
							 		for (int i = 0; i < resultJSONArray1.size(); i++) {
							 			if(resultJSONArray1.get(i) != null && !resultJSONArray1.get(i).toString().isEmpty()){
							 				Greige_QuotatioDetailsDO greQuotDetailsDO = new Greige_QuotatioDetailsDO();
							 				if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
							 					greQuotDetailsDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
							 					greQuotDetailsDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
									 		}
							 				greQuotDetailsDO.setUpdatedon(new Date());
							 				greQuotDetailsDO.setCreatedon(new Date());
							 				greQuotDetailsDO.setQuotation(quotationList);
							 				if(resultJSONArray1.get(i)!=null && !resultJSONArray1.get(i).toString().isEmpty()){
								 				List <Greige_PendingOrdersDO> pendingOrder = greige_PendingOrdersService.retrieveById(Long.parseLong(resultJSONArray1.get(i).toString()));
									 			if(pendingOrder != null && pendingOrder.size() > 0 ){
									 				greQuotDetailsDO.setOcn(pendingOrder.get(0));
									 				Greige_PendingOrdersDO sortDetailDO = new Greige_PendingOrdersDO();
									 				sortDetailDO = pendingOrder.get(0);
									 				sortDetailDO.setStatus("s");
									 				sortDetailDO.setUpdatedon(new Date());
									 				if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
									 					sortDetailDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
											 		}
									 				if(!greige_PendingOrdersService.update(sortDetailDO)){
											 			return CommonWebUtil.buildErrorResponse("Pending Orders Already Updated").toString();
											 		}
									 			}
							 				}
								 			greige_QuotationDetailService.persist(greQuotDetailsDO);
								 		}
							 		}
						 		}
					 		}
					 		
					 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Quotation Created");
					 	}
			 		}
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
