package com.spheresuite.erp.procurement.rs;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.Greige_CreateQuotationDO;
import com.spheresuite.erp.domainobject.Greige_Spplier_QuoteDO;
import com.spheresuite.erp.domainobject.Greige_SuppliersDocDO;
import com.spheresuite.erp.domainobject.Greige_TargetPriceDO;
import com.spheresuite.erp.procurement.service.Greige_CreateQuotationService;
import com.spheresuite.erp.procurement.service.Greige_Supplier_QuoteService;
import com.spheresuite.erp.procurement.service.Greige_SuppliersDocService;
import com.spheresuite.erp.procurement.service.Greige_TargetPriceService;
import com.spheresuite.erp.procurement.web.util.Greige_SupplierQuoteUtil;
import com.spheresuite.erp.supplier.service.SupplierService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/greige_spplier_quote")
public class Greige_Supplier_QuoteRS {

	String validation = null;
	static Logger logger = Logger.getLogger(Greige_Supplier_QuoteRS.class.getName());
	
	@Autowired
	private Greige_Supplier_QuoteService  geige_SpplierQuoteService;
	
	@Autowired
	private Greige_CreateQuotationService greige_createQuotationService;
	
	@Autowired
	private SupplierService suppliersService;
	
	@Autowired
	private Greige_SuppliersDocService supplierDocService;
	
	@Autowired
	private Greige_TargetPriceService greigeTargetPriceService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				Greige_Spplier_QuoteDO supplierQuotDetailsDO = new Greige_Spplier_QuoteDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.QUOT_ID) != null && !inputJSON.get(CommonConstants.QUOT_ID).toString().isEmpty()){
			 			List <Greige_CreateQuotationDO> quotInfo = greige_createQuotationService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.QUOT_ID).toString()));
			 			if(quotInfo != null && quotInfo.size() > 0 ){
			 				supplierQuotDetailsDO.setQuotation(quotInfo.get(0));
			 				supplierQuotDetailsDO.setTargetprice(quotInfo.get(0).getTargetPrice());
			 			}
			 		}
			 		if(inputJSON.get(CommonConstants.EXMILLRATE) != null && !inputJSON.get(CommonConstants.EXMILLRATE).toString().isEmpty()){
			 			supplierQuotDetailsDO.setExmillrate(inputJSON.get(CommonConstants.EXMILLRATE).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.FINALIZEDRATE) != null && !inputJSON.get(CommonConstants.FINALIZEDRATE).toString().isEmpty()){
			 			supplierQuotDetailsDO.setFinalizedrate(Long.parseLong(inputJSON.get(CommonConstants.FINALIZEDRATE).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.TARGETPRICE) != null && !inputJSON.get(CommonConstants.TARGETPRICE).toString().isEmpty()){
			 			supplierQuotDetailsDO.setFinalizedrate(Long.parseLong(inputJSON.get(CommonConstants.TARGETPRICE).toString()));
			 			supplierQuotDetailsDO.setTargetprice(Long.parseLong(inputJSON.get(CommonConstants.TARGETPRICE).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.GRCONS) != null && !inputJSON.get(CommonConstants.GRCONS).toString().isEmpty()){
			 			supplierQuotDetailsDO.setGrcons(inputJSON.get(CommonConstants.GRCONS).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.GW) != null && !inputJSON.get(CommonConstants.GW).toString().isEmpty()){
			 			supplierQuotDetailsDO.setGw(inputJSON.get(CommonConstants.GW).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.DENT) != null && !inputJSON.get(CommonConstants.DENT).toString().isEmpty()){
			 			supplierQuotDetailsDO.setDent(inputJSON.get(CommonConstants.DENT).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.QTYM) != null && !inputJSON.get(CommonConstants.QTYM).toString().isEmpty()){
			 			supplierQuotDetailsDO.setQtym(inputJSON.get(CommonConstants.QTYM).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.REEDCOUNT) != null && !inputJSON.get(CommonConstants.REEDCOUNT).toString().isEmpty()){
			 			supplierQuotDetailsDO.setReedcount(inputJSON.get(CommonConstants.REEDCOUNT).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.COMMENT) != null && !inputJSON.get(CommonConstants.COMMENT).toString().isEmpty()){
			 			supplierQuotDetailsDO.setRemarks(inputJSON.get(CommonConstants.COMMENT).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.SUPPLIERID) != null && !inputJSON.get(CommonConstants.SUPPLIERID).toString().isEmpty()){
			 			supplierQuotDetailsDO.setSupplierId(inputJSON.get(CommonConstants.SUPPLIERID).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
			 			supplierQuotDetailsDO.setStatus(inputJSON.get(CommonConstants.STATUS).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			supplierQuotDetailsDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			supplierQuotDetailsDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		supplierQuotDetailsDO.setUpdatedon(new Date());
			 		supplierQuotDetailsDO.setCreatedon(new Date());
			 	}
				if(!geige_SpplierQuoteService.persist(supplierQuotDetailsDO)){
		 			return CommonWebUtil.buildErrorResponse("Deatils Already Added").toString();
				}
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Deatils Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<Greige_Spplier_QuoteDO> supplierQuoteList = geige_SpplierQuoteService.retrieve();
				respJSON = Greige_SupplierQuoteUtil.getQuotationList(supplierQuoteList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				Greige_Spplier_QuoteDO quotationDO = new Greige_Spplier_QuoteDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<Greige_Spplier_QuoteDO> quotationDetailsList = geige_SpplierQuoteService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(quotationDetailsList != null && quotationDetailsList.size() >0){
			 			quotationDO = quotationDetailsList.get(0);
			 			
			 			/*if(inputJSON.get(CommonConstants.QUOT_ID) != null && !inputJSON.get(CommonConstants.QUOT_ID).toString().isEmpty()){
				 			List <Greige_CreateQuotationDO> quotInfo = greige_createQuotationService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.QUOT_ID).toString()));
				 			if(quotInfo != null && quotInfo.size() > 0 ){
				 				quotationDO.setQuotation(quotInfo.get(0));
				 			}
				 		}*/
				 		if(inputJSON.get(CommonConstants.EXMILLRATE) != null && !inputJSON.get(CommonConstants.EXMILLRATE).toString().isEmpty()){
				 			quotationDO.setExmillrate(inputJSON.get(CommonConstants.EXMILLRATE).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.FINALIZEDRATE) != null && !inputJSON.get(CommonConstants.FINALIZEDRATE).toString().isEmpty()){
				 			quotationDO.setFinalizedrate(Long.parseLong(inputJSON.get(CommonConstants.FINALIZEDRATE).toString()));
				 		}
				 		if(inputJSON.get(CommonConstants.TARGETPRICE) != null && !inputJSON.get(CommonConstants.TARGETPRICE).toString().isEmpty()){
				 			quotationDO.setTargetprice(Long.parseLong(inputJSON.get(CommonConstants.TARGETPRICE).toString()));
				 		}
				 		if(inputJSON.get(CommonConstants.GRCONS) != null && !inputJSON.get(CommonConstants.GRCONS).toString().isEmpty()){
				 			quotationDO.setGrcons(inputJSON.get(CommonConstants.GRCONS).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.GW) != null && !inputJSON.get(CommonConstants.GW).toString().isEmpty()){
				 			quotationDO.setGw(inputJSON.get(CommonConstants.GW).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.DENT) != null && !inputJSON.get(CommonConstants.DENT).toString().isEmpty()){
				 			quotationDO.setDent(inputJSON.get(CommonConstants.DENT).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.QTYM) != null && !inputJSON.get(CommonConstants.QTYM).toString().isEmpty()){
				 			quotationDO.setQtym(inputJSON.get(CommonConstants.QTYM).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.REEDCOUNT) != null && !inputJSON.get(CommonConstants.REEDCOUNT).toString().isEmpty()){
				 			quotationDO.setReedcount(inputJSON.get(CommonConstants.REEDCOUNT).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.COMMENT) != null && !inputJSON.get(CommonConstants.COMMENT).toString().isEmpty()){
				 			quotationDO.setRemarks(inputJSON.get(CommonConstants.COMMENT).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.SUPPLIERID) != null && !inputJSON.get(CommonConstants.SUPPLIERID).toString().isEmpty()){
				 			quotationDO.setSupplierId(inputJSON.get(CommonConstants.SUPPLIERID).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
				 			quotationDO.setStatus(inputJSON.get(CommonConstants.STATUS).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			quotationDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		quotationDO.setUpdatedon(new Date());
				 		if(!geige_SpplierQuoteService.update(quotationDO)){
				 			return CommonWebUtil.buildErrorResponse("Quotation Already Added").toString();
				 		}
				 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Quotation Updated");
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retrieveQuoteForSupplier/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String retrieveQuoteFrorSupplier(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON.get(CommonConstants.SUPPLIERID).toString() != null && !inputJSON.get(CommonConstants.SUPPLIERID).toString().isEmpty() && inputJSON.get(CommonConstants.TYPE).toString() != null && !inputJSON.get(CommonConstants.TYPE).toString().isEmpty()){
					List<Greige_Spplier_QuoteDO> quotationList = geige_SpplierQuoteService.retrieveBySpplierId(inputJSON.get(CommonConstants.SUPPLIERID).toString(), inputJSON.get(CommonConstants.TYPE).toString());
					respJSON =  Greige_SupplierQuoteUtil.getQuotationList(quotationList).toString();
				}
				
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	
	
	@RequestMapping(value = "/submitQuote/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String submitForQuote(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		//String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				List<Greige_Spplier_QuoteDO> supplierQuoteList = geige_SpplierQuoteService.retrieveByStatus(CommonConstants.QSTATUS);
				if(supplierQuoteList != null && supplierQuoteList.size() > 0){
					for(Greige_Spplier_QuoteDO supplierQuote : supplierQuoteList){
						Greige_Spplier_QuoteDO supplierInfo =  new Greige_Spplier_QuoteDO();
						supplierInfo = supplierQuote;
						supplierInfo.setStatus(CommonConstants.SSTATUS);
						supplierInfo.setUpdatedon(new Date());
						if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
							supplierInfo.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
						if(!geige_SpplierQuoteService.update(supplierInfo)){
							 CommonWebUtil.buildErrorResponse("").toString();
						}else{
							CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Quotation Updated");
						}
					}
				}else{
					
				}
				
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retriveByQuoteIdAndSupplierId/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String retriveByQuoteIdAndSupplierId(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<Greige_Spplier_QuoteDO> quotList = geige_SpplierQuoteService.retriveByQuoteIdAndSupplierId(Long.parseLong(inputJSON.get(CommonConstants.SUPPLIER_QUOTE_ID).toString()), Long.parseLong(inputJSON.get(CommonConstants.QUOT_ID).toString()), inputJSON.get(CommonConstants.SUPPLIERID).toString());
					respJSON = Greige_SupplierQuoteUtil.getQuotationList(quotList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/updateFinalizedRate", method = RequestMethod.POST)
	public @ResponseBody String updateFinalizedRate(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				Greige_Spplier_QuoteDO quotationDO = new Greige_Spplier_QuoteDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<Greige_Spplier_QuoteDO> quotationDetailsList = geige_SpplierQuoteService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(quotationDetailsList != null && quotationDetailsList.size() >0){
			 			quotationDO = quotationDetailsList.get(0);
			 			
				 		if(inputJSON.get(CommonConstants.FINALIZEDRATE) != null && !inputJSON.get(CommonConstants.FINALIZEDRATE).toString().isEmpty()){
				 			quotationDO.setFinalizedrate(Long.parseLong(inputJSON.get(CommonConstants.FINALIZEDRATE).toString()));
				 		}
				 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			quotationDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		quotationDO.setUpdatedon(new Date());
				 		if(!geige_SpplierQuoteService.update(quotationDO)){
				 			return CommonWebUtil.buildErrorResponse("Quotation Already Added").toString();
				 		}
				 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Quotation Updated");
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/updateTargetRate", method = RequestMethod.POST)
	public @ResponseBody String updateTargetRate(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				Greige_Spplier_QuoteDO quotationDO = new Greige_Spplier_QuoteDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<Greige_Spplier_QuoteDO> quotationDetailsList = geige_SpplierQuoteService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(quotationDetailsList != null && quotationDetailsList.size() >0){
			 			quotationDO = quotationDetailsList.get(0);
			 			
				 		if(inputJSON.get(CommonConstants.TARGETPRICE) != null && !inputJSON.get(CommonConstants.TARGETPRICE).toString().isEmpty()){
				 			quotationDO.setTargetprice(Long.parseLong(inputJSON.get(CommonConstants.TARGETPRICE).toString()));
				 		}
				 		if(inputJSON.get(CommonConstants.TARGETPRICE) != null && !inputJSON.get(CommonConstants.TARGETPRICE).toString().isEmpty()){
				 			quotationDO.setFinalizedrate(Long.parseLong(inputJSON.get(CommonConstants.TARGETPRICE).toString()));
				 		}
				 		if(inputJSON.get(CommonConstants.TARGETQTY) != null && !inputJSON.get(CommonConstants.TARGETQTY).toString().isEmpty()){
				 			Long qty = Long.parseLong(inputJSON.get(CommonConstants.TARGETQTY).toString());
			 				quotationDO.setTargetQty(qty.toString());
				 		}
				 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			quotationDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		quotationDO.setUpdatedon(new Date());
				 		if(!geige_SpplierQuoteService.update(quotationDO)){
				 			return CommonWebUtil.buildErrorResponse("Quotation Already Added").toString();
				 		}
				 		//List<SuppliersDO> DetailsList = suppliersService.retrieveById(Long.parseLong(quotationDO.getSupplierId()));
				 		if(quotationDO != null){
					 		Greige_TargetPriceDO greigeTargetPriceDO = new Greige_TargetPriceDO();
					 		if(inputJSON.get(CommonConstants.TARGETPRICE) != null && !inputJSON.get(CommonConstants.TARGETPRICE).toString().isEmpty()){
					 			greigeTargetPriceDO.setTargetPrice(inputJSON.get(CommonConstants.TARGETPRICE).toString());
					 		}
					 		if(inputJSON.get(CommonConstants.TARGETQTY) != null && !inputJSON.get(CommonConstants.TARGETQTY).toString().isEmpty()){
					 			//greigeTargetPriceDO.setTargetQty(inputJSON.get(CommonConstants.TARGETQTY).toString());
					 			Long qty = Long.parseLong(inputJSON.get(CommonConstants.TARGETQTY).toString());
				 				greigeTargetPriceDO.setTargetQty(qty.toString());
					 		}
					 		greigeTargetPriceDO.setQuoteId(quotationDO.getQuotation().getQuoteId().toString());
					 		greigeTargetPriceDO.setSupplierId(quotationDO.getSupplierId());
					 		greigeTargetPriceDO.setSortName(quotationDO.getQuotation().getSortName());
					 		greigeTargetPriceDO.setCreatedon(new Date());
					 		greigeTargetPriceDO.setUpdatedon(new Date());
					 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			greigeTargetPriceDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			greigeTargetPriceDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 		}
					 		greigeTargetPriceService.persist(greigeTargetPriceDO);
				 		}
				 		
				 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Quotation Updated");
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	/*
	@RequestMapping(value = "/submitForApproval/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String submitForApproval(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				List<Greige_Spplier_QuoteDO> supplierQuoteList = geige_SpplierQuoteService.retrieveForApproval();
				if(supplierQuoteList != null && supplierQuoteList.size() >0){
					List<Greige_CreateQuotationDO> quotationDetailsList = greige_createQuotationService.retrieveById(supplierQuoteList.get(0).getQuotation().getQuoteId());
			 		if(quotationDetailsList != null && quotationDetailsList.size() >0){
			 			Greige_CreateQuotationDO qtn = new Greige_CreateQuotationDO();
			 			qtn =  quotationDetailsList.get(0);
			 			qtn.setStatus(CommonConstants.SSTATUS);
			 			qtn.setUpdatedon(new Date());
			 			if(!greige_createQuotationService.update(qtn)){
				 			return CommonWebUtil.buildErrorResponse("Quotation Already Added").toString();
				 		}
				 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Quotation Updated");
			 		}
					
				}
				respJSON = Greige_Spplier_QuoteUtil.getQuotationList(supplierQuoteList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}*/
	
	
	@RequestMapping(value = "/retriveByQuoteId/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String retriveByQuoteId(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.QUOT_ID).toString().isEmpty()){
					List<Greige_Spplier_QuoteDO> quotList = geige_SpplierQuoteService.retriveByQuoteId(Long.parseLong(inputJSON.get(CommonConstants.QUOT_ID).toString()));
					respJSON = Greige_SupplierQuoteUtil.getQuotationList(quotList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retriveByQuoteSubmittedId/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String retriveByQuoteSubmittedId(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.QUOT_ID).toString().isEmpty()){
					List<Greige_Spplier_QuoteDO> quotList = geige_SpplierQuoteService.retriveByQuoteSubmittedId(Long.parseLong(inputJSON.get(CommonConstants.QUOT_ID).toString()));
					if(quotList != null && quotList.size() > 0){
						respJSON = Greige_SupplierQuoteUtil.getQuotationList(quotList).toString();
					}else {
						return CommonWebUtil.buildErrorResponse("").toString();
					}
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retriveByQuoteApprove/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String retriveByQuoteApprove(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.QUOT_ID).toString().isEmpty()){
					List<Greige_Spplier_QuoteDO> quotList = geige_SpplierQuoteService.retriveByQuoteApprove(Long.parseLong(inputJSON.get(CommonConstants.QUOT_ID).toString()));
					respJSON = Greige_SupplierQuoteUtil.getQuotationList(quotList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/updateAcceptCost", method = RequestMethod.POST)
	public @ResponseBody String updateAcceptCost(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				Greige_Spplier_QuoteDO quotationDO = new Greige_Spplier_QuoteDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<Greige_Spplier_QuoteDO> quotationDetailsList = geige_SpplierQuoteService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(quotationDetailsList != null && quotationDetailsList.size() >0){
			 			quotationDO = quotationDetailsList.get(0);
			 			
				 		if(inputJSON.get(CommonConstants.ACCEPT_COST) != null && !inputJSON.get(CommonConstants.ACCEPT_COST).toString().isEmpty()){
				 			quotationDO.setAcceptCost(inputJSON.get(CommonConstants.ACCEPT_COST).toString());
				 		}else{
				 			quotationDO.setAcceptCost(null);
				 		}
				 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			quotationDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		quotationDO.setUpdatedon(new Date());
				 		if(!geige_SpplierQuoteService.update(quotationDO)){
				 			return CommonWebUtil.buildErrorResponse("Quotation Already Added").toString();
				 		}
				 		if(quotationDO != null){
				 			Greige_CreateQuotationDO quotation = new Greige_CreateQuotationDO();
				 			List<Greige_CreateQuotationDO> quotationDetailsList1 = greige_createQuotationService.retrieveById(Long.parseLong(quotationDO.getQuotation().getQuoteId().toString()));
					 		if(quotationDetailsList1 != null && quotationDetailsList1.size() >0){
					 			quotation = quotationDetailsList1.get(0);
					 			Long pendingQty = 0l;
					 			if(inputJSON.get(CommonConstants.TARGETQTY) != null && !inputJSON.get(CommonConstants.TARGETQTY).toString().isEmpty()){
					 				pendingQty = Long.parseLong(quotation.getRequiredMeter()) - Long.parseLong(inputJSON.get(CommonConstants.TARGETQTY).toString());
					 				quotation.setPendingRequiredMeter(pendingQty.toString());
						 			if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
						 				quotation.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
							 		}
						 			quotation.setUpdatedon(new Date());
							 		if(!greige_createQuotationService.update(quotation)){
							 			return CommonWebUtil.buildErrorResponse("Quotation Already Updated").toString();
							 		}
					 			}
					 		}
				 		}
				 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Quotation Updated");
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retriveBySupplierAcceptCost/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String retriveBySupplierAcceptCost(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ACCEPT_COST).toString().isEmpty()){
					List<Greige_Spplier_QuoteDO> quotList = geige_SpplierQuoteService.retriveBySupplierAcceptCost(inputJSON.get(CommonConstants.ACCEPT_COST).toString());
					respJSON = Greige_SupplierQuoteUtil.getQuotationList(quotList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveSupplierAlert", method = RequestMethod.GET)
	public @ResponseBody String retrieveSupplierAlert(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<Greige_Spplier_QuoteDO> supplierQuoteList = geige_SpplierQuoteService.retrieveSupplierAlert();
				respJSON = Greige_SupplierQuoteUtil.getQuotationList(supplierQuoteList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/updatepic", method = RequestMethod.POST)
	public @ResponseBody String updatepic(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				Greige_SuppliersDocDO suppliersDocDO = new Greige_SuppliersDocDO();
				/*JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("data").toString());*/
			 	if (request.getParameter("file") != null && request.getParameter("id") != null){
			 		List<Greige_Spplier_QuoteDO> supplierQuoteList = geige_SpplierQuoteService.retrieveById(Long.parseLong(request.getParameter("id").toString()));
			 		if(supplierQuoteList != null && supplierQuoteList.size() > 0){
				 		suppliersDocDO.setSuppliers(supplierQuoteList.get(0));
				 		suppliersDocDO.setSupplierId(request.getParameter("supplierId"));
				 		suppliersDocDO.setPhoto(request.getParameter("file"));
				 		suppliersDocDO.setFileName(request.getParameter("name"));
				 		suppliersDocDO.setUpdatedon(new Date());
				 		suppliersDocDO.setCreatedon(new Date());
				 		if(request.getParameter(CommonConstants.UPDATED_BY) != null && !request.getParameter(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			suppliersDocDO.setUpdatedBy(request.getParameter(CommonConstants.UPDATED_BY).toString());
				 			suppliersDocDO.setCreatedBy(request.getParameter(CommonConstants.UPDATED_BY).toString());
				 		}
				 		supplierDocService.persist(suppliersDocDO);
			 		}else{
			 			return CommonWebUtil.buildErrorResponse("").toString();
			 		}
 			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrievepic", method = RequestMethod.POST)
	public @ResponseBody String retrievepic(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				//Greige_SuppliersDocDO suppliersDocDO = new Greige_SuppliersDocDO();
				/*JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("data").toString());*/
			 	/*if (request.getParameter("file") != null && request.getParameter("id") != null){
			 		List<Greige_Spplier_QuoteDO> supplierQuoteList = geige_SpplierQuoteService.retrieveById(Long.parseLong(request.getParameter("id").toString()));
			 		if(supplierQuoteList != null && supplierQuoteList.size() > 0){
				 		suppliersDocDO.setSuppliers(supplierQuoteList.get(0));
				 		suppliersDocDO.setSupplierId(request.getParameter("supplierId"));
				 		suppliersDocDO.setPhoto(request.getParameter("file"));
				 		suppliersDocDO.setFileName(request.getParameter("name"));
				 		suppliersDocDO.setUpdatedon(new Date());
				 		suppliersDocDO.setCreatedon(new Date());
				 		if(request.getParameter(CommonConstants.UPDATED_BY) != null && !request.getParameter(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			suppliersDocDO.setUpdatedBy(request.getParameter(CommonConstants.UPDATED_BY).toString());
				 			suppliersDocDO.setCreatedBy(request.getParameter(CommonConstants.UPDATED_BY).toString());
				 		}
				 		supplierDocService.persist(suppliersDocDO);
			 		}else{
			 			return CommonWebUtil.buildErrorResponse("").toString();
			 		}
 			 	}*/
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/persistSupplier", method = RequestMethod.POST)
	public @ResponseBody String addNewQuote(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				Greige_Spplier_QuoteDO supplierQuotDetailsDO = new Greige_Spplier_QuoteDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.QUOT_ID) != null && !inputJSON.get(CommonConstants.QUOT_ID).toString().isEmpty()){
			 			List <Greige_CreateQuotationDO> quotInfo = greige_createQuotationService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.QUOT_ID).toString()));
			 			if(quotInfo != null && quotInfo.size() > 0 ){
			 				supplierQuotDetailsDO.setQuotation(quotInfo.get(0));
			 				supplierQuotDetailsDO.setTargetprice(quotInfo.get(0).getTargetPrice());
			 			}
			 		}
			 		if(inputJSON.get(CommonConstants.EXMILLRATE) != null && !inputJSON.get(CommonConstants.EXMILLRATE).toString().isEmpty()){
			 			supplierQuotDetailsDO.setExmillrate(inputJSON.get(CommonConstants.EXMILLRATE).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.FINALIZEDRATE) != null && !inputJSON.get(CommonConstants.FINALIZEDRATE).toString().isEmpty()){
			 			supplierQuotDetailsDO.setFinalizedrate(Long.parseLong(inputJSON.get(CommonConstants.FINALIZEDRATE).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.TARGETPRICE) != null && !inputJSON.get(CommonConstants.TARGETPRICE).toString().isEmpty()){
			 			supplierQuotDetailsDO.setFinalizedrate(Long.parseLong(inputJSON.get(CommonConstants.TARGETPRICE).toString()));
			 			supplierQuotDetailsDO.setTargetprice(Long.parseLong(inputJSON.get(CommonConstants.TARGETPRICE).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.GRCONS) != null && !inputJSON.get(CommonConstants.GRCONS).toString().isEmpty()){
			 			supplierQuotDetailsDO.setGrcons(inputJSON.get(CommonConstants.GRCONS).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.GW) != null && !inputJSON.get(CommonConstants.GW).toString().isEmpty()){
			 			supplierQuotDetailsDO.setGw(inputJSON.get(CommonConstants.GW).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.DENT) != null && !inputJSON.get(CommonConstants.DENT).toString().isEmpty()){
			 			supplierQuotDetailsDO.setDent(inputJSON.get(CommonConstants.DENT).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.QTYM) != null && !inputJSON.get(CommonConstants.QTYM).toString().isEmpty()){
			 			supplierQuotDetailsDO.setQtym(inputJSON.get(CommonConstants.QTYM).toString());
			 			supplierQuotDetailsDO.setTargetQty(inputJSON.get(CommonConstants.QTYM).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.REEDCOUNT) != null && !inputJSON.get(CommonConstants.REEDCOUNT).toString().isEmpty()){
			 			supplierQuotDetailsDO.setReedcount(inputJSON.get(CommonConstants.REEDCOUNT).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.COMMENT) != null && !inputJSON.get(CommonConstants.COMMENT).toString().isEmpty()){
			 			supplierQuotDetailsDO.setRemarks(inputJSON.get(CommonConstants.COMMENT).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.SUPPLIERID) != null && !inputJSON.get(CommonConstants.SUPPLIERID).toString().isEmpty()){
			 			supplierQuotDetailsDO.setSupplierId(inputJSON.get(CommonConstants.SUPPLIERID).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
			 			supplierQuotDetailsDO.setStatus(inputJSON.get(CommonConstants.STATUS).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.ACCEPT_COST) != null && !inputJSON.get(CommonConstants.ACCEPT_COST).toString().isEmpty()){
			 			supplierQuotDetailsDO.setAcceptCost(inputJSON.get(CommonConstants.ACCEPT_COST).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			supplierQuotDetailsDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			supplierQuotDetailsDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		supplierQuotDetailsDO.setUpdatedon(new Date());
			 		supplierQuotDetailsDO.setCreatedon(new Date());
			 	}
				if(!geige_SpplierQuoteService.persist(supplierQuotDetailsDO)){
		 			return CommonWebUtil.buildErrorResponse("Deatils Already Added").toString();
				}
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Deatils Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
}
