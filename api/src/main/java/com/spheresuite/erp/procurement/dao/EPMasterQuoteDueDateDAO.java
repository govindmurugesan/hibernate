package com.spheresuite.erp.procurement.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.EPMasterQuoteDueDateDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class EPMasterQuoteDueDateDAO {
	static Logger logger = Logger.getLogger(EPMasterQuoteDueDateDAO.class.getName());
	@Autowired
	private GenericDAOImpl<EPMasterQuoteDueDateDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public boolean persist(EPMasterQuoteDueDateDO quoteDueDO){
		boolean persistStatus = true;
		try {
			genericObject.persist(quoteDueDO);
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return persistStatus;
	}
	
	public List<EPMasterQuoteDueDateDO> retrieve(){
		List<EPMasterQuoteDueDateDO> dueList = null;
		try {
			dueList = genericObject.retrieve(EPMasterQuoteDueDateDO.class);
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
		}
		return dueList;
	}
	
	public List<EPMasterQuoteDueDateDO> retrieveActive(){
		List<EPMasterQuoteDueDateDO> dueList = null;
		try {
			return genericObject.retrieveActive(CommonConstants.ACTIVE, EPMasterQuoteDueDateDO.FIND_BY_STATUS, EPMasterQuoteDueDateDO.class);
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return dueList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EPMasterQuoteDueDateDO> retrieveById(Long id) {
		List<EPMasterQuoteDueDateDO> dueList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EPMasterQuoteDueDateDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return dueList;
	}
	
	public boolean update(EPMasterQuoteDueDateDO dueList){
		boolean updateStatus = true;
		try {
			genericObject.merge(dueList);
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return updateStatus;
	}
	
	public boolean persistList(List<EPMasterQuoteDueDateDO> dueList){
		try {
			for (EPMasterQuoteDueDateDO due : dueList) {
				genericObject.persist(due);
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return true;
	}

}
