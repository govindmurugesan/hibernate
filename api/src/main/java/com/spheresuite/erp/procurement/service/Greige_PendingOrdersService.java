package com.spheresuite.erp.procurement.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.domainobject.Greige_PendingOrdersDO;
import com.spheresuite.erp.procurement.dao.Greige_PendingOrdersDAO;

@Service
@Transactional
public class Greige_PendingOrdersService {

	@Autowired
	private Greige_PendingOrdersDAO greigeSortDetailsDAO;
	
	@Transactional
	public boolean persist(Greige_PendingOrdersDO detailsDO){
		return greigeSortDetailsDAO.persist(detailsDO);
	}

/*	@Transactional
	public List<Greige_SortDetailsDO> retrieveActive(){
		return greigeSortDetailsDAO.retrieveActive();
	}*/
	
	@Transactional
	public List<Greige_PendingOrdersDO> retrieveById(Long id){
		return greigeSortDetailsDAO.retrieveById(id);
	}
	
	@Transactional
	public List<Greige_PendingOrdersDO> retrieve(){
		return greigeSortDetailsDAO.retrieve();
	}
	
	@Transactional
	public List<Greige_PendingOrdersDO> retrieveAll(){
		return greigeSortDetailsDAO.retrieveAll();
	}
	
	@Transactional
	public List<Greige_PendingOrdersDO> retrieveWithStatus(){
		return greigeSortDetailsDAO.retrieveWithStatus();
	}
	
	@Transactional
	public boolean update(Greige_PendingOrdersDO detailsDO){
		return greigeSortDetailsDAO.update(detailsDO);
	}
	
	@Transactional
	public List<Greige_PendingOrdersDO> retrieveBySortId(Long id){
		return greigeSortDetailsDAO.retrieveBySortId(id);
	}
	
	@Transactional
	public List<Greige_PendingOrdersDO> retrieveByGreigeReqDate(String date){
		return greigeSortDetailsDAO.retrieveByGreigeReqDate(date);
	}
	
	@Transactional
	public List<Greige_PendingOrdersDO> retrieveByGreigReqDate(String date, Long id){
		return greigeSortDetailsDAO.retrieveByGreigReqDate(date, id);
	}
	
	@Transactional
	public List<Greige_PendingOrdersDO> retrieveOcnsCount(){
		return greigeSortDetailsDAO.retrieveOcnsCount();
	}
}
	
