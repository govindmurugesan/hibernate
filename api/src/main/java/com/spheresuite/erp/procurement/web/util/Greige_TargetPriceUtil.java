package com.spheresuite.erp.procurement.web.util;

import java.util.List;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.Greige_TargetPriceDO;
import com.spheresuite.erp.domainobject.SuppliersDO;
import com.spheresuite.erp.supplier.service.SupplierService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;

@Component
public class Greige_TargetPriceUtil {
	
	private Greige_TargetPriceUtil() {}
	
	@Autowired
	private  SupplierService tsupplierService;
	private static SupplierService supplierService;	
	
	@PostConstruct
	public void init() {
		supplierService = tsupplierService;
	}
	
	public static JSONObject getOrderList(List<Greige_TargetPriceDO> orderDetailList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (Greige_TargetPriceDO orderDetail : orderDetailList) {
				resultJSONArray.put(getOrderDetailObject(orderDetail));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getOrderDetailObject(Greige_TargetPriceDO targetPriceDetail)throws JSONException{
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(targetPriceDetail.getTargetPriceId()));
		if(targetPriceDetail.getSortName() != null){
			result.put(CommonConstants.SORT, String.valueOf(targetPriceDetail.getSortName().getSortName()));
			result.put(CommonConstants.SORT_ID, String.valueOf(targetPriceDetail.getSortName().getSortId()));
		}else{
			result.put(CommonConstants.SORT, "");
			result.put(CommonConstants.SORT_ID, "");
		}
		if(targetPriceDetail.getQuoteId() != null){
			result.put(CommonConstants.QUOT_ID, String.valueOf(targetPriceDetail.getQuoteId()));
		}else{
			result.put(CommonConstants.QUOT_ID, "");
		}
		if(targetPriceDetail.getSupplierId() != null){
			List<SuppliersDO> supplierList = supplierService.retrieveById(Long.parseLong(targetPriceDetail.getSupplierId()));
 			if(supplierList != null && supplierList.size() > 0){
 				result.put(CommonConstants.SUPPLIERID, String.valueOf(supplierList.get(0).getSupplierId()));
 				result.put(CommonConstants.SUPPLIERNAME, String.valueOf(supplierList.get(0).getName()));
 			}
			
		}else{
			result.put(CommonConstants.SUPPLIERID, "");
			result.put(CommonConstants.SUPPLIERNAME, "");
		}
		if(targetPriceDetail.getTargetPrice() != null){
			result.put(CommonConstants.TARGETPRICE, String.valueOf(targetPriceDetail.getTargetPrice()));
		}else{
			result.put(CommonConstants.TARGETPRICE, "");
		}
		if(targetPriceDetail.getTargetQty() != null){
			result.put(CommonConstants.TARGETQTY, String.valueOf(targetPriceDetail.getTargetQty()));
		}else{
			result.put(CommonConstants.TARGETQTY, "");
		}
		if(targetPriceDetail.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(targetPriceDetail.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(targetPriceDetail.getUpdatedon())));
		return result;
	}
}
