package com.spheresuite.erp.procurement.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.EPMasterSortDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class EPMasterSortDAO {
	@Autowired
	private GenericDAOImpl<EPMasterSortDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public boolean persist(EPMasterSortDO sortDO){
		//List<EPMasterSortDO> earningTypeList = null;
		boolean persistStatus = true;
		try {
			genericObject.persist(sortDO);
			/*earningTypeList = this.sessionFactory.getCurrentSession().getNamedQuery(EPMasterSortDO.FIND_BY_NAME)
			.setParameter(CommonConstants.NAME, earningTypeDO.getName())
			.list();
			if(earningTypeList != null && earningTypeList.size() > 0){
				persistStatus = false;
			}else{
				genericObject.persist(earningTypeDO);
			}*/
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	public List<EPMasterSortDO> retrieve(){
		List<EPMasterSortDO> sortList = null;
		try {
			sortList = genericObject.retrieve(EPMasterSortDO.class);
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
		}
		return sortList;
	}
	
	public List<EPMasterSortDO> retrieveActive(){
		List<EPMasterSortDO> sortList = null;
		try {
			return genericObject.retrieveActive(CommonConstants.ACTIVE, EPMasterSortDO.FIND_BY_STATUS, EPMasterSortDO.class);
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return sortList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EPMasterSortDO> retrieveById(Long id) {
		List<EPMasterSortDO> sortList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EPMasterSortDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return sortList;
	}
	
	public boolean update(EPMasterSortDO sortList){
		boolean updateStatus = true;
		try {
			genericObject.merge(sortList);
			/*List<EPMasterSortDO> sortList = null;
			earningTypeList = this.sessionFactory.getCurrentSession().getNamedQuery(EPMasterSortDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, allowanceTypeDO.getName())
					.setParameter(CommonConstants.ID, allowanceTypeDO.getEarningTypeId())
					.list();
			if(earningTypeList != null && earningTypeList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(sortList);
			}*/
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}
	
	public boolean persistList(List<EPMasterSortDO> sortList){
		try {
			for (EPMasterSortDO sort : sortList) {
				genericObject.persist(sort);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}

}
