package com.spheresuite.erp.procurement.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.domainobject.EPMasterSortDO;
import com.spheresuite.erp.procurement.dao.EPMasterSortDAO;

@Service
@Transactional
public class EPMasterSortService {

	@Autowired
	private EPMasterSortDAO epMasterSortDAO;
	
	@Transactional
	public boolean persist(EPMasterSortDO sortDO){
		return epMasterSortDAO.persist(sortDO);
	}

	@Transactional
	public List<EPMasterSortDO> retrieveActive(){
		return epMasterSortDAO.retrieveActive();
	}
	
	@Transactional
	public List<EPMasterSortDO> retrieveById(Long id){
		return epMasterSortDAO.retrieveById(id);
	}
	
	@Transactional
	public List<EPMasterSortDO> retrieve(){
		return epMasterSortDAO.retrieve();
	}
	
	@Transactional
	public boolean update(EPMasterSortDO sortDO){
		return epMasterSortDAO.update(sortDO);
	}
	
	@Transactional
	public boolean persistList(List<EPMasterSortDO> sortDO) {
		return epMasterSortDAO.persistList(sortDO);
	}
}
