package com.spheresuite.erp.procurement.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.domainobject.Greige_QuotatioDetailsDO;
import com.spheresuite.erp.procurement.dao.Geige_QuotationDetailsDAO;

@Service
@Transactional
public class Greige_QuotationDetailService {

	@Autowired
	private Geige_QuotationDetailsDAO geige_QuotationDetailsDAO;
	
	@Transactional
	public boolean persist(Greige_QuotatioDetailsDO detailsDO){
		return geige_QuotationDetailsDAO.persist(detailsDO);
	}
	
	@Transactional
	public List<Greige_QuotatioDetailsDO> retrieveByQuoteId(Long id){
		return geige_QuotationDetailsDAO.retrieveByQuoteId(id);
	}
	
	@Transactional
	public boolean delete(Long id){
		return geige_QuotationDetailsDAO.delete(id);
	}
	

/*	@Transactional
	public List<Greige_SortDetailsDO> retrieveActive(){
		return greigeSortDetailsDAO.retrieveActive();
	}*/
	
/*	@Transactional
	public List<Greige_CreateQuotationDO> retrieveById(Long id){
		return greigeCreateQuotationDAO.retrieveById(id);
	}
	
	@Transactional
	public List<Greige_CreateQuotationDO> retrieve(){
		return greigeCreateQuotationDAO.retrieve();
	}
	
	@Transactional
	public List<Greige_CreateQuotationDO> retrieveDraftQuote(){
		return greigeCreateQuotationDAO.retrieveDraftQuote();
	}
	
	@Transactional
	public boolean update(Greige_CreateQuotationDO detailsDO){
		return greigeCreateQuotationDAO.update(detailsDO);
	}
	
	@Transactional
	public boolean delete(Long id){
		return greigeCreateQuotationDAO.delete(id);
	}
	
	@Transactional
	public List<Greige_CreateQuotationDO> retrieveRequestQuote(){
		return greigeCreateQuotationDAO.retrieveRequestQuote();
	}
	
	@Transactional
	public List<Greige_CreateQuotationDO> retrieveQuoteBySortId(Long id){
		return greigeCreateQuotationDAO.retrieveQuoteBySortId(id);
	}*/
	
	
	
}
	
