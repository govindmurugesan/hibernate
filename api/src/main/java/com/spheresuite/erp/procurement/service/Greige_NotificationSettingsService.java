package com.spheresuite.erp.procurement.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.domainobject.Greige_NotificationSettingsDO;
import com.spheresuite.erp.procurement.dao.Greige_NotificationSettingsDAO;
@Service
@Transactional
public class Greige_NotificationSettingsService {
	static Logger logger = Logger.getLogger(Greige_NotificationSettingsService.class.getName());
	@Autowired
	private Greige_NotificationSettingsDAO greigeNotificationSettingsDAO;

	@Transactional
	public Greige_NotificationSettingsDO persist(Greige_NotificationSettingsDO notificationSettingsDO) {
		return greigeNotificationSettingsDAO.persist(notificationSettingsDO);
	}

	@Transactional
	public Greige_NotificationSettingsDO update(Greige_NotificationSettingsDO notificationSettingsDO) {
		return greigeNotificationSettingsDAO.update(notificationSettingsDO);
	}

	@Transactional
	public List<Greige_NotificationSettingsDO> retrieveById(Long Id) {
		return greigeNotificationSettingsDAO.retrieveById(Id);
	}

	@Transactional
	public List<Greige_NotificationSettingsDO> retrieve() {
		return greigeNotificationSettingsDAO.retrieve();
	}
}
