package com.spheresuite.erp.procurement.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.domainobject.Greige_SuppliersDocDO;
import com.spheresuite.erp.procurement.dao.Greige_SuppliersDocDAO;
@Service
@Transactional
public class Greige_SuppliersDocService {
	static Logger logger = Logger.getLogger(Greige_SuppliersDocService.class.getName());
	@Autowired
	private Greige_SuppliersDocDAO suppliersDocDAO;

	@Transactional
	public Greige_SuppliersDocDO persist(Greige_SuppliersDocDO suppliersDocDO) {
		return suppliersDocDAO.persist(suppliersDocDO);
	}

	@Transactional
	public List<Greige_SuppliersDocDO> retrieveByOppId(Long id) {
		return suppliersDocDAO.retrieveByOppId(id);
	}

	@Transactional
	public boolean delete(Long id) {
		return suppliersDocDAO.delete(id);
	}

	@Transactional
	public List<Greige_SuppliersDocDO> retrieve() {
		return suppliersDocDAO.retrieve();
	}

	@Transactional
	public Greige_SuppliersDocDO update(Greige_SuppliersDocDO suppliersDocDO) {
		return suppliersDocDAO.update(suppliersDocDO);
	}
}
