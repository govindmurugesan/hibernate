package com.spheresuite.erp.procurement.web.util;

import java.util.List;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.Greige_QuotatioDetailsDO;
import com.spheresuite.erp.domainobject.Greige_Spplier_QuoteDO;
import com.spheresuite.erp.domainobject.SuppliersDO;
import com.spheresuite.erp.procurement.service.Greige_QuotationDetailService;
import com.spheresuite.erp.procurement.service.Greige_Supplier_QuoteService;
import com.spheresuite.erp.supplier.service.SupplierService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;

@Component
public class Greige_SupplierQuoteUtil {
	
	private Greige_SupplierQuoteUtil() {}
	
	
	@Autowired
	private  Greige_QuotationDetailService tgreige_QuotationDetailService;
	private static Greige_QuotationDetailService greige_QuotationDetailService;
	
	@Autowired
	private  Greige_Supplier_QuoteService tgreige_Supplier_QuoteService;
	private static Greige_Supplier_QuoteService greige_Supplier_QuoteService;
	
	@Autowired
	private  SupplierService tSupplierService;
	private static SupplierService supplierService;
	
	@PostConstruct
	public void init() {
		greige_QuotationDetailService = tgreige_QuotationDetailService;
		supplierService = tSupplierService;
		greige_Supplier_QuoteService = tgreige_Supplier_QuoteService;
		
	}
	
	public static JSONObject getQuotationList(List<Greige_Spplier_QuoteDO> quotationDetailList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (Greige_Spplier_QuoteDO quotationDetail : quotationDetailList) {
				resultJSONArray.put(getQuoteSortDetailObject(quotationDetail));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getQuoteSortDetailObject(Greige_Spplier_QuoteDO quotationDetail)throws JSONException{
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(quotationDetail.getSupplierQuoteId()));
		
		if(quotationDetail.getQuotation() != null){
			result.put(CommonConstants.QUOT_ID, String.valueOf(quotationDetail.getQuotation().getQuoteId()));
		}else{
			result.put(CommonConstants.QUOT_ID, "");
		}
		if(quotationDetail.getGrcons()!= null){
			result.put(CommonConstants.GRCONS, String.valueOf(quotationDetail.getGrcons()));
		}else{
			result.put(CommonConstants.GRCONS, "");
		}
		if(quotationDetail.getGw() != null){
			result.put(CommonConstants.GW, String.valueOf(quotationDetail.getGw()));
		}else{
			result.put(CommonConstants.GW, "");
		}
		if(quotationDetail.getExmillrate() != null){
			result.put(CommonConstants.EXMILLRATE, String.valueOf(quotationDetail.getExmillrate()));
		}else{
			result.put(CommonConstants.EXMILLRATE, "");
		}
		if(quotationDetail.getFinalizedrate() != null){
			result.put(CommonConstants.FINALIZEDRATE, String.valueOf(quotationDetail.getFinalizedrate()));
		}else{
			result.put(CommonConstants.FINALIZEDRATE, "");
		}
		if(quotationDetail.getTargetprice() != null){
			result.put(CommonConstants.TARGETPRICE, String.valueOf(quotationDetail.getTargetprice()));
		}else{
			result.put(CommonConstants.TARGETPRICE, "");
		}
		if(quotationDetail.getTargetQty() != null){
			result.put(CommonConstants.TARGETQTY, String.valueOf(quotationDetail.getTargetQty()));
		}else{
			result.put(CommonConstants.TARGETQTY, "");
		}
		if(quotationDetail.getFinalizedQty() != null){
			result.put(CommonConstants.FINALIZEDQTY, String.valueOf(quotationDetail.getFinalizedQty()));
		}else{
			result.put(CommonConstants.FINALIZEDQTY, "");
		}
		if(quotationDetail.getSubmitted() != null){
			result.put(CommonConstants.SELECTED, String.valueOf(quotationDetail.getSubmitted()));
		}else{
			result.put(CommonConstants.SELECTED, "");
		}
		if(quotationDetail.getDent() != null){
			result.put(CommonConstants.DENT, String.valueOf(quotationDetail.getDent()));
		}else{
			result.put(CommonConstants.DENT, "");
		}
		if(quotationDetail.getQtym() != null){
			result.put(CommonConstants.QTYM, String.valueOf(quotationDetail.getQtym()));
		}else{
			result.put(CommonConstants.QTYM, "");
		}
		if(quotationDetail.getReedcount() != null){
			result.put(CommonConstants.REEDCOUNT, String.valueOf(quotationDetail.getReedcount()));
		}else{
			result.put(CommonConstants.REEDCOUNT, "");
		}
		if(quotationDetail.getRemarks() != null){
			result.put(CommonConstants.COMMENT, String.valueOf(quotationDetail.getRemarks()));
		}else{
			result.put(CommonConstants.COMMENT, "");
		}
		
		if(quotationDetail.getSupplierId() != null){
			result.put(CommonConstants.SUPPLIERID, String.valueOf(quotationDetail.getSupplierId()));
			List<SuppliersDO> supplier = supplierService.retrieveById(Long.parseLong(quotationDetail.getSupplierId()));
			if(supplier != null && supplier.size() > 0 && supplier.get(0).getName() != null){
				result.put(CommonConstants.NAME, String.valueOf(supplier.get(0).getName()));
			}else{
				result.put(CommonConstants.NAME, String.valueOf(""));
			}
		}else{
			result.put(CommonConstants.SUPPLIERID, "");
			result.put(CommonConstants.NAME, String.valueOf(""));
		}
		if(quotationDetail.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(quotationDetail.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		if(quotationDetail.getStatus() != null){
			result.put(CommonConstants.STATUS, quotationDetail.getStatus()); 
		}else{
			result.put(CommonConstants.STATUS, ""); 
		}
		
		if(quotationDetail.getAcceptCost() != null){
			result.put(CommonConstants.ACCEPT_COST, quotationDetail.getAcceptCost()); 
		}else{
			result.put(CommonConstants.ACCEPT_COST, ""); 
		}
		
		JSONObject quotation = new JSONObject();
		if(quotationDetail.getQuotation() != null){
			if(quotationDetail.getQuotation().getSortName() != null){
				quotation.put(CommonConstants.SORT, String.valueOf(quotationDetail.getQuotation().getSortName().getSortName()));
				quotation.put(CommonConstants.SORT_ID, String.valueOf(quotationDetail.getQuotation().getSortName().getSortId()));
			}else{
				quotation.put(CommonConstants.SORT, "");
				quotation.put(CommonConstants.SORT_ID, "");
			}
			if(quotationDetail.getQuotation().getGrCons()!= null){
				quotation.put(CommonConstants.GRCONS, String.valueOf(quotationDetail.getQuotation().getGrCons()));
			}else{
				quotation.put(CommonConstants.GRCONS, "");
			}
			if(quotationDetail.getQuotation().getQuoteType() != null){
				quotation.put(CommonConstants.TYPE, String.valueOf(quotationDetail.getQuotation().getQuoteType()));
			}else{
				quotation.put(CommonConstants.TYPE, "");
			}
			if(quotationDetail.getQuotation().getGreigeReqDueDate() != null){
				quotation.put(CommonConstants.GREIGREQDUODATE, String.valueOf(CommonUtil.convertDateToStringWithtime(quotationDetail.getQuotation().getGreigeReqDueDate())));
			}else{
				quotation.put(CommonConstants.GREIGREQDUODATE, "");
			}
			if(quotationDetail.getQuotation().getQuoteDue() != null){
				result.put(CommonConstants.GREIGREQDUEHOURSID, String.valueOf(quotationDetail.getQuotation().getQuoteDue().getQuoteDueDateId()));
				result.put(CommonConstants.GREIGREQDUEHOURS, String.valueOf(quotationDetail.getQuotation().getQuoteDue().getHours()));
			}else{
				result.put(CommonConstants.GREIGREQDUEHOURSID, "");
				result.put(CommonConstants.GREIGREQDUEHOURS, "");
			}
			if(quotationDetail.getQuotation().getGreigReqDate() != null){
				quotation.put(CommonConstants.GREIGREQDATE, String.valueOf(quotationDetail.getQuotation().getGreigReqDate()));
			}else{
				quotation.put(CommonConstants.GREIGREQDATE, "");
			}
			if(quotationDetail.getQuotation().getGw() != null){
				quotation.put(CommonConstants.GW, String.valueOf(quotationDetail.getQuotation().getGw()));
			}else{
				quotation.put(CommonConstants.GW, "");
			}
			if(quotationDetail.getQuotation().getRequiredMeter() != null){
				quotation.put(CommonConstants.REQUIREDMETER, String.valueOf(quotationDetail.getQuotation().getRequiredMeter()));
			}else{
				quotation.put(CommonConstants.REQUIREDMETER, "");
			}
			if(quotationDetail.getQuotation().getPendingRequiredMeter() != null){
				quotation.put(CommonConstants.PENDINGQTY, String.valueOf(quotationDetail.getQuotation().getPendingRequiredMeter()));
			}else{
				quotation.put(CommonConstants.PENDINGQTY, "");
			}
			if(quotationDetail.getQuotation().getTc() != null){
				quotation.put(CommonConstants.TC, String.valueOf(quotationDetail.getQuotation().getTc()));
			}else{
				quotation.put(CommonConstants.TC, "");
			}
			
			
			if(quotationDetail.getQuotation().getYc() != null && quotationDetail.getQuotation().getYc() != null){
				quotation.put(CommonConstants.YC, String.valueOf(quotationDetail.getQuotation().getYc()));
			}else{
				quotation.put(CommonConstants.YC, "");
			}
			
			if(quotationDetail.getQuotation().getWave() != null){
				quotation.put(CommonConstants.WAVE, String.valueOf(quotationDetail.getQuotation().getWave()));
			}else{
				quotation.put(CommonConstants.WAVE, "");
			}
			
			List<Greige_QuotatioDetailsDO> orderList = greige_QuotationDetailService.retrieveByQuoteId(Long.parseLong(quotationDetail.getQuotation().getQuoteId().toString()));
			
			if(orderList != null && orderList.size()>0){
				JSONArray resultJSONArray = new JSONArray();
				for(int i= 0; i < orderList.size(); i++){
					resultJSONArray.put(getOcnDetail(orderList.get(i)));
				}
				quotation.put(CommonConstants.OCNDETAILS, resultJSONArray);
			}
			
		}
		List<Greige_Spplier_QuoteDO> previousOrderList = greige_Supplier_QuoteService.retrieveByStatus("a");
		JSONObject previousQuotation = new JSONObject();
		if(previousOrderList != null && previousOrderList.size()>0){
			JSONArray resultJSONArray1 = new JSONArray();
			for(int i= 0; i < previousOrderList.size(); i++){
				resultJSONArray1.put(getPreviousOrderDetails(previousOrderList.get(i)));
			}
			previousQuotation.put(CommonConstants.RESULTS, resultJSONArray1);
		}
		result.put(CommonConstants.PREVIOUS_QUOTATION_DETAILS, previousQuotation); 
		result.put(CommonConstants.QUOTATIONDETAIL, quotation); 
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(quotationDetail.getUpdatedon())));
		return result;
	}
		
		public static JSONObject getOcnDetail(Greige_QuotatioDetailsDO  order)throws JSONException{
			JSONObject result = new JSONObject();
			if(order.getOcn() != null && order.getOcn().getOcn() != null) result.put(CommonConstants.OCN, String.valueOf(order.getOcn().getOcn().trim()));
			if(order.getOcn() != null && order.getOcn().getShippingDate() != null) result.put(CommonConstants.SHIPPINGDATE, String.valueOf(order.getOcn().getShippingDate()));
			if(order.getOcn() != null && order.getOcn().getGreigReqDate() != null) result.put(CommonConstants.GREIGREQDATE, String.valueOf(order.getOcn().getGreigReqDate()));
			if(order.getOcn() != null && order.getOcn().getGreigReq() != null) result.put(CommonConstants.REQUIREDMETER, String.valueOf(order.getOcn().getGreigReq()));
			if(order.getOcn() != null && order.getOcn().getPendingOrderId() != null) result.put(CommonConstants.PENDINGORDERID, String.valueOf(order.getOcn().getPendingOrderId()));
			return result;
		}
		
		public static JSONObject getPreviousOrderDetails(Greige_Spplier_QuoteDO  previousOrder)throws JSONException{
			JSONObject result = new JSONObject();
			result.put(CommonConstants.ID, String.valueOf(previousOrder.getSupplierQuoteId()));
			
			if(previousOrder.getQuotation() != null){
				result.put(CommonConstants.QUOT_ID, String.valueOf(previousOrder.getQuotation().getQuoteId()));
			}else{
				result.put(CommonConstants.QUOT_ID, "");
			}
			if(previousOrder.getGrcons()!= null){
				result.put(CommonConstants.GRCONS, String.valueOf(previousOrder.getGrcons()));
			}else{
				result.put(CommonConstants.GRCONS, "");
			}
			if(previousOrder.getGw() != null){
				result.put(CommonConstants.GW, String.valueOf(previousOrder.getGw()));
			}else{
				result.put(CommonConstants.GW, "");
			}
			if(previousOrder.getExmillrate() != null){
				result.put(CommonConstants.EXMILLRATE, String.valueOf(previousOrder.getExmillrate()));
			}else{
				result.put(CommonConstants.EXMILLRATE, "");
			}
			if(previousOrder.getFinalizedrate() != null){
				result.put(CommonConstants.FINALIZEDRATE, String.valueOf(previousOrder.getFinalizedrate()));
			}else{
				result.put(CommonConstants.FINALIZEDRATE, "");
			}
			if(previousOrder.getTargetprice() != null){
				result.put(CommonConstants.TARGETPRICE, String.valueOf(previousOrder.getTargetprice()));
			}else{
				result.put(CommonConstants.TARGETPRICE, "");
			}
			if(previousOrder.getSubmitted() != null){
				result.put(CommonConstants.SELECTED, String.valueOf(previousOrder.getSubmitted()));
			}else{
				result.put(CommonConstants.SELECTED, "");
			}
			if(previousOrder.getDent() != null){
				result.put(CommonConstants.DENT, String.valueOf(previousOrder.getDent()));
			}else{
				result.put(CommonConstants.DENT, "");
			}
			if(previousOrder.getQtym() != null){
				result.put(CommonConstants.QTYM, String.valueOf(previousOrder.getQtym()));
			}else{
				result.put(CommonConstants.QTYM, "");
			}
			if(previousOrder.getReedcount() != null){
				result.put(CommonConstants.REEDCOUNT, String.valueOf(previousOrder.getReedcount()));
			}else{
				result.put(CommonConstants.REEDCOUNT, "");
			}
			if(previousOrder.getRemarks() != null){
				result.put(CommonConstants.COMMENT, String.valueOf(previousOrder.getRemarks()));
			}else{
				result.put(CommonConstants.COMMENT, "");
			}
			
			if(previousOrder.getSupplierId() != null){
				result.put(CommonConstants.SUPPLIERID, String.valueOf(previousOrder.getSupplierId()));
				List<SuppliersDO> supplier = supplierService.retrieveById(Long.parseLong(previousOrder.getSupplierId()));
				if(supplier != null && supplier.size() > 0 && supplier.get(0).getName() != null){
					result.put(CommonConstants.NAME, String.valueOf(supplier.get(0).getName()));
				}else{
					result.put(CommonConstants.NAME, String.valueOf(""));
				}
			}else{
				result.put(CommonConstants.SUPPLIERID, "");
				result.put(CommonConstants.NAME, String.valueOf(""));
			}
			if(previousOrder.getUpdatedby() != null){
				String empName = CommonUtil.getEmpObject(previousOrder.getUpdatedby());
				result.put(CommonConstants.UPDATED_BY, empName); 
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
			if(previousOrder.getStatus() != null){
				result.put(CommonConstants.STATUS, previousOrder.getStatus()); 
			}else{
				result.put(CommonConstants.STATUS, ""); 
			}
			
			if(previousOrder.getAcceptCost() != null){
				result.put(CommonConstants.ACCEPT_COST, previousOrder.getAcceptCost()); 
			}else{
				result.put(CommonConstants.ACCEPT_COST, ""); 
			}
			if(previousOrder.getQuotation().getSortName() != null){
				result.put(CommonConstants.SORT, String.valueOf(previousOrder.getQuotation().getSortName().getSortName()));
				result.put(CommonConstants.SORT_ID, String.valueOf(previousOrder.getQuotation().getSortName().getSortId()));
			}else{
				result.put(CommonConstants.SORT, "");
				result.put(CommonConstants.SORT_ID, "");
			}
			result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(previousOrder.getUpdatedon())));
			return result;
		}
}
