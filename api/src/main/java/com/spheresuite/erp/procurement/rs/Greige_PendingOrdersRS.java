package com.spheresuite.erp.procurement.rs;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.EPMasterSortDO;
import com.spheresuite.erp.domainobject.Greige_PendingOrdersDO;
import com.spheresuite.erp.procurement.service.EPMasterSortService;
import com.spheresuite.erp.procurement.service.Greige_PendingOrdersService;
import com.spheresuite.erp.procurement.web.util.Greige_PendingOrdersUtil;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/greige_pendingorders")
public class Greige_PendingOrdersRS {

	String validation = null;
	static Logger logger = Logger.getLogger(Greige_PendingOrdersRS.class.getName());
	
	@Autowired
	private Greige_PendingOrdersService greige_PendingDetailsService;
	
	@Autowired
	private EPMasterSortService epMasterSortService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				Greige_PendingOrdersDO SortDetailsDO = new Greige_PendingOrdersDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.SORT_ID) != null && !inputJSON.get(CommonConstants.SORT_ID).toString().isEmpty()){
			 			List <EPMasterSortDO> sortInfo = epMasterSortService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.SORT_ID).toString()));
			 			if(sortInfo != null && sortInfo.size() > 0 ){
			 				SortDetailsDO.setSortName(sortInfo.get(0));
			 			}
			 		}
			 		if(inputJSON.get(CommonConstants.OCN) != null && !inputJSON.get(CommonConstants.OCN).toString().isEmpty()){
			 			SortDetailsDO.setOcn(inputJSON.get(CommonConstants.OCN).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.PLANNO) != null && !inputJSON.get(CommonConstants.PLANNO).toString().isEmpty()){
			 			SortDetailsDO.setPlanNo(inputJSON.get(CommonConstants.PLANNO).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.GREIGREG) != null && !inputJSON.get(CommonConstants.GREIGREG).toString().isEmpty()){
			 			SortDetailsDO.setGreigReq(inputJSON.get(CommonConstants.GREIGREG).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
			 			SortDetailsDO.setStatus(inputJSON.get(CommonConstants.STATUS).toString());
			 		}
			 		/*if(inputJSON.get(CommonConstants.GRCONS).toString() != null && !inputJSON.get(CommonConstants.GRCONS).toString().isEmpty()){
			 			SortDetailsDO.setGreigReq(inputJSON.get(CommonConstants.GRCONS).toString());
			 		}*/
			 		/*if(inputJSON.get(CommonConstants.PENDINGDAYS) != null && !inputJSON.get(CommonConstants.PENDINGDAYS).toString().isEmpty()){
			 			SortDetailsDO.setPendigDays(Long.parseLong(inputJSON.get(CommonConstants.PENDINGDAYS).toString()));
			 		}*/
			 		if(inputJSON.get(CommonConstants.SHIPPINGDATE)!= null && !inputJSON.get(CommonConstants.SHIPPINGDATE).toString().isEmpty()){
			 			SortDetailsDO.setShippingDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.SHIPPINGDATE).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.GREIGREQDATE) != null && !inputJSON.get(CommonConstants.GREIGREQDATE).toString().isEmpty()){
			 			SortDetailsDO.setGreigReqDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.GREIGREQDATE).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			SortDetailsDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			SortDetailsDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		SortDetailsDO.setUpdatedon(new Date());
			 		SortDetailsDO.setCreatedon(new Date());
			 	}
				if(!greige_PendingDetailsService.persist(SortDetailsDO)){
		 			return CommonWebUtil.buildErrorResponse("Order Deatils Already Added").toString();
				}
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Order Details Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<Greige_PendingOrdersDO> sortDetailList = greige_PendingDetailsService.retrieveAll();
				respJSON = Greige_PendingOrdersUtil.getOrderList(sortDetailList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveWithStatus", method = RequestMethod.GET)
	public @ResponseBody String retrieveWithStatus(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<Greige_PendingOrdersDO> sortDetailList = greige_PendingDetailsService.retrieveWithStatus();
				respJSON = Greige_PendingOrdersUtil.getOrderList(sortDetailList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retriveBySortId/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String retriveById(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<Greige_PendingOrdersDO> ordereList = greige_PendingDetailsService.retrieveBySortId(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = Greige_PendingOrdersUtil.getOrderListWithQuotationDetails(ordereList,Long.parseLong(inputJSON.get(CommonConstants.ID).toString())).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retriveByGreigeReqDate/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String retriveByGreigeReqDate(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.GREIGREQDATE).toString().isEmpty()){
					List<Greige_PendingOrdersDO> ordereList = greige_PendingDetailsService.retrieveByGreigeReqDate(inputJSON.get(CommonConstants.GREIGREQDATE).toString());
					respJSON = Greige_PendingOrdersUtil.getOrderListWithQuotationDetailsGreigeReqDate(ordereList,inputJSON.get(CommonConstants.GREIGREQDATE).toString()).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				Greige_PendingOrdersDO sortDetailDO = new Greige_PendingOrdersDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<Greige_PendingOrdersDO> sortDetailsList = greige_PendingDetailsService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(sortDetailsList != null && sortDetailsList.size() >0){
			 			sortDetailDO = sortDetailsList.get(0);
			 			if(inputJSON.get(CommonConstants.SORT_ID) != null && !inputJSON.get(CommonConstants.SORT_ID).toString().isEmpty()){
				 			List <EPMasterSortDO> sortInfo = epMasterSortService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.SORT_ID).toString()));
				 			if(sortInfo != null && sortInfo.size() > 0 ){
				 				sortDetailDO.setSortName(sortInfo.get(0));
				 			}
				 		}
				 		if(inputJSON.get(CommonConstants.OCN) != null && !inputJSON.get(CommonConstants.OCN).toString().isEmpty()){
				 			sortDetailDO.setOcn(inputJSON.get(CommonConstants.OCN).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.PLANNO) != null && !inputJSON.get(CommonConstants.PLANNO).toString().isEmpty()){
				 			sortDetailDO.setPlanNo(inputJSON.get(CommonConstants.PLANNO).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.GREIGREG) != null && !inputJSON.get(CommonConstants.GREIGREG).toString().isEmpty()){
				 			sortDetailDO.setGreigReq(inputJSON.get(CommonConstants.GREIGREG).toString());
				 		}
				 	/*	if(inputJSON.get(CommonConstants.GRCONS).toString() != null && !inputJSON.get(CommonConstants.GRCONS).toString().isEmpty()){
				 			sortDetailDO.setGreigReq(inputJSON.get(CommonConstants.GRCONS).toString());
				 		}*/
				 		if(inputJSON.get(CommonConstants.SHIPPINGDATE)!= null && !inputJSON.get(CommonConstants.SHIPPINGDATE).toString().isEmpty()){
				 			sortDetailDO.setShippingDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.SHIPPINGDATE).toString()));
				 		}
				 		/*if(inputJSON.get(CommonConstants.PENDINGDAYS) != null && !inputJSON.get(CommonConstants.PENDINGDAYS).toString().isEmpty()){
				 			sortDetailDO.setPendigDays(Long.parseLong(inputJSON.get(CommonConstants.PENDINGDAYS).toString()));
				 		}*/
				 		if(inputJSON.get(CommonConstants.GREIGREQDATE)!= null && !inputJSON.get(CommonConstants.GREIGREQDATE).toString().isEmpty()){
				 			sortDetailDO.setGreigReqDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.GREIGREQDATE).toString()));
				 		}
				 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			sortDetailDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
				 			sortDetailDO.setStatus(inputJSON.get(CommonConstants.STATUS).toString());
				 		}
				 		sortDetailDO.setUpdatedon(new Date());
				 		if(!greige_PendingDetailsService.update(sortDetailDO)){
				 			return CommonWebUtil.buildErrorResponse("SortDetails Already Added").toString();
				 		}
				 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "SortDetails Updated");
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveOcnsCount", method = RequestMethod.GET)
	public @ResponseBody String retrieveOcnsCount(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<Greige_PendingOrdersDO> sortDetailList = greige_PendingDetailsService.retrieveOcnsCount();
				respJSON = Greige_PendingOrdersUtil.getOrderCountList(sortDetailList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
}
