package com.spheresuite.erp.procurement.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.EPMasterQuoteDueDateDO;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;

@Component
public class EPMasterQuoteDueDateUtil {
	
	private EPMasterQuoteDueDateUtil() {}
	
	
	public static JSONObject getEPMasterDueList(List<EPMasterQuoteDueDateDO> quoteDueDOList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EPMasterQuoteDueDateDO quoteDueDO : quoteDueDOList) {
				resultJSONArray.put(getEPMasterQuoteDueDetailObject(quoteDueDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getEPMasterQuoteDueDetailObject(EPMasterQuoteDueDateDO quoteDueDO)throws JSONException{
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(quoteDueDO.getQuoteDueDateId()));
		if(quoteDueDO.getHours() != null){
			result.put(CommonConstants.HOURS, String.valueOf(quoteDueDO.getHours()));
		}else{
			result.put(CommonConstants.HOURS, "");
		}
		result.put(CommonConstants.STATUS, String.valueOf(quoteDueDO.getStatus()));
		if(quoteDueDO.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(quoteDueDO.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(quoteDueDO.getUpdatedon())));
		return result;
	}
	
	public static JSONObject getEPMasterQuoteDueDetailObjectWithOCN(EPMasterQuoteDueDateDO quoteDueDO)throws JSONException{
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(quoteDueDO.getQuoteDueDateId()));
		if(quoteDueDO.getHours() != null){
			result.put(CommonConstants.HOURS, String.valueOf(quoteDueDO.getHours()));
		}else{
			result.put(CommonConstants.HOURS, "");
		}
		
		result.put(CommonConstants.STATUS, String.valueOf(quoteDueDO.getStatus()));
		if(quoteDueDO.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(quoteDueDO.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(quoteDueDO.getUpdatedon())));
		return result;
	}
	
}
