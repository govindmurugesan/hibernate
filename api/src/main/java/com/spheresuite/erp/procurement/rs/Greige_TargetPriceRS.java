package com.spheresuite.erp.procurement.rs;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.EPMasterSortDO;
import com.spheresuite.erp.domainobject.Greige_TargetPriceDO;
import com.spheresuite.erp.procurement.service.EPMasterSortService;
import com.spheresuite.erp.procurement.service.Greige_TargetPriceService;
import com.spheresuite.erp.procurement.web.util.Greige_TargetPriceUtil;
import com.spheresuite.erp.supplier.service.SupplierService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/greige_targetprice")
public class Greige_TargetPriceRS {

	String validation = null;
	static Logger logger = Logger.getLogger(Greige_TargetPriceRS.class.getName());
	
	@Autowired
	private Greige_TargetPriceService greige_TargetPriceService;
	
	@Autowired
	private EPMasterSortService epMasterSortService;
	
	@Autowired
	private SupplierService supplierService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				Greige_TargetPriceDO greigeTargetPriceDO = new Greige_TargetPriceDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.SORT_ID) != null && !inputJSON.get(CommonConstants.SORT_ID).toString().isEmpty()){
			 			List <EPMasterSortDO> sortInfo = epMasterSortService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.SORT_ID).toString()));
			 			if(sortInfo != null && sortInfo.size() > 0 ){
			 				greigeTargetPriceDO.setSortName(sortInfo.get(0));
			 			}
			 		}
			 		if(inputJSON.get(CommonConstants.QUOT_ID) != null && !inputJSON.get(CommonConstants.QUOT_ID).toString().isEmpty()){
			 			greigeTargetPriceDO.setQuoteId(inputJSON.get(CommonConstants.QUOT_ID).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.SUPPLIERID) != null && !inputJSON.get(CommonConstants.SUPPLIERID).toString().isEmpty()){
			 			greigeTargetPriceDO.setSupplierId(inputJSON.get(CommonConstants.SUPPLIERID).toString());
			 		}
			 		/*if(inputJSON.get(CommonConstants.SUPPLIERID) != null && !inputJSON.get(CommonConstants.SUPPLIERID).toString().isEmpty()){
			 			List<SuppliersDO> supplierList = supplierService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.SUPPLIERID).toString()));
			 			if(supplierList != null && supplierList.size() > 0){
			 				greigeTargetPriceDO.setSupplier(supplierList.get(0));
			 			}
			 		}*/
			 		if(inputJSON.get(CommonConstants.TARGETPRICE) != null && !inputJSON.get(CommonConstants.TARGETPRICE).toString().isEmpty()){
			 			greigeTargetPriceDO.setTargetPrice(inputJSON.get(CommonConstants.TARGETPRICE).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.TARGETQTY) != null && !inputJSON.get(CommonConstants.TARGETQTY).toString().isEmpty()){
			 			greigeTargetPriceDO.setTargetQty(inputJSON.get(CommonConstants.TARGETQTY).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			greigeTargetPriceDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			greigeTargetPriceDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		greigeTargetPriceDO.setUpdatedon(new Date());
			 		greigeTargetPriceDO.setCreatedon(new Date());
			 	}
				greige_TargetPriceService.persist(greigeTargetPriceDO);
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Target Price Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<Greige_TargetPriceDO> sortDetailList = greige_TargetPriceService.retrieveAll();
				respJSON = Greige_TargetPriceUtil.getOrderList(sortDetailList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retriveBySortId/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String retriveById(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<Greige_TargetPriceDO> ordereList = greige_TargetPriceService.retrieveBySortId(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = Greige_TargetPriceUtil.getOrderList(ordereList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retriveBySupplierId/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String retriveBySupplierId(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<Greige_TargetPriceDO> ordereList = greige_TargetPriceService.retrieveBySupplierId(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = Greige_TargetPriceUtil.getOrderList(ordereList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retriveByQuoteId/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String retriveByQuoteId(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<Greige_TargetPriceDO> ordereList = greige_TargetPriceService.retrieveByQuoteId(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = Greige_TargetPriceUtil.getOrderList(ordereList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retriveBySupplierSortQuoteId/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String retriveBySupplierSortQuoteId(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<Greige_TargetPriceDO> ordereList = greige_TargetPriceService.retrieveBySupplierSortQuoteId(Long.parseLong(inputJSON.get(CommonConstants.SORT_ID).toString()),
															Long.parseLong(inputJSON.get(CommonConstants.SUPPLIERID).toString()), inputJSON.get(CommonConstants.QUOT_ID).toString());
					respJSON = Greige_TargetPriceUtil.getOrderList(ordereList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
}
