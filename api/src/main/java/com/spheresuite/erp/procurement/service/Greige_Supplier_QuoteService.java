package com.spheresuite.erp.procurement.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.domainobject.Greige_Spplier_QuoteDO;
import com.spheresuite.erp.procurement.dao.Greige_Supplier_QuoteDAO;

@Service
@Transactional
public class Greige_Supplier_QuoteService {

	@Autowired
	private Greige_Supplier_QuoteDAO greigeSpplierQuoteDAO;
	
	@Transactional
	public boolean persist(Greige_Spplier_QuoteDO detailsDO){
		return greigeSpplierQuoteDAO.persist(detailsDO);
	}

/*	@Transactional
	public List<Greige_SortDetailsDO> retrieveActive(){
		return greigeSortDetailsDAO.retrieveActive();
	}*/
	
	@Transactional
	public List<Greige_Spplier_QuoteDO> retrieveById(Long id){
		return greigeSpplierQuoteDAO.retrieveById(id);
	}
	
	@Transactional
	public List<Greige_Spplier_QuoteDO> retriveByQuoteIdAndSupplierId(Long id, Long quoteId, String supplierId){
		return greigeSpplierQuoteDAO.retriveByQuoteIdAndSupplierId(id, quoteId, supplierId);
	}
	
	@Transactional
	public List<Greige_Spplier_QuoteDO> retrieve(){
		return greigeSpplierQuoteDAO.retrieve();
	}
	
	@Transactional
	public boolean update(Greige_Spplier_QuoteDO detailsDO){
		return greigeSpplierQuoteDAO.update(detailsDO);
	}
	
	@Transactional
	public List<Greige_Spplier_QuoteDO> retrieveByStatus(String status){
		return greigeSpplierQuoteDAO.retrieveByStatus(status);
	}
	
	@Transactional
	public List<Greige_Spplier_QuoteDO> retrieveForApproval(){
		return greigeSpplierQuoteDAO.retrieveForApproval();
	}
	
	@Transactional
	public List<Greige_Spplier_QuoteDO> retrieveBySpplierId(String spplierId, String type){
		return greigeSpplierQuoteDAO.retrieveBySpplierId(spplierId, type);
	}
	
	@Transactional
	public List<Greige_Spplier_QuoteDO> retriveByQuoteId(Long id){
		return greigeSpplierQuoteDAO.retriveByQuoteId(id);
	}
	
	@Transactional
	public List<Greige_Spplier_QuoteDO> retriveByQuoteSubmittedId(Long id){
		return greigeSpplierQuoteDAO.retriveByQuoteSubmittedId(id);
	}
	
	@Transactional
	public List<Greige_Spplier_QuoteDO> retriveByQuoteApprove(Long id){
		return greigeSpplierQuoteDAO.retriveByQuoteApprove(id);
	}
	
	@Transactional
	public List<Greige_Spplier_QuoteDO> retriveBySupplierAcceptCost(String accept){
		return greigeSpplierQuoteDAO.retriveBySupplierAcceptCost(accept);
	}
	
	@Transactional
	public List<Greige_Spplier_QuoteDO> retriveForApproval(){
		return greigeSpplierQuoteDAO.retriveForApproval();
	}
	
	@Transactional
	public List<Greige_Spplier_QuoteDO> retrieveByQuoteReceived(){
		return greigeSpplierQuoteDAO.retrieveByQuoteReceived();
	}
	
	@Transactional
	public List<Greige_Spplier_QuoteDO> retrieveSupplierAlert(){
		return greigeSpplierQuoteDAO.retrieveSupplierAlert();
	}
}
	
