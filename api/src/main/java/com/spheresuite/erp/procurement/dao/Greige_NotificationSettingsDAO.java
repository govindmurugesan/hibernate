package com.spheresuite.erp.procurement.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.Greige_NotificationSettingsDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class Greige_NotificationSettingsDAO {
	static Logger logger = Logger.getLogger(Greige_NotificationSettingsDAO.class.getName());

	@Autowired
	private GenericDAOImpl<Greige_NotificationSettingsDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public Greige_NotificationSettingsDO persist(Greige_NotificationSettingsDO notificationSettingsDO) {
		try {
			genericObject.persist(notificationSettingsDO);
		} catch (Exception eException) {
		} finally {
		}
		return notificationSettingsDO;
	}
	
	public Greige_NotificationSettingsDO update(Greige_NotificationSettingsDO notificationSettingsDO) {
		try {
			genericObject.merge(notificationSettingsDO);
		} catch (Exception eException) {
		} finally {
		}
		return notificationSettingsDO;
	}
	
	@SuppressWarnings("unchecked")
	public List<Greige_NotificationSettingsDO> retrieve() {
		List<Greige_NotificationSettingsDO> notificarionSettingList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(Greige_NotificationSettingsDO.FIND_ALL)
										.list();
		} catch (Exception eException) {
			System.out.println(eException.getMessage());
			logger.info(eException.getMessage());
		} finally {
		}
		return notificarionSettingList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Greige_NotificationSettingsDO> retrieveById(Long Id) {
		List<Greige_NotificationSettingsDO> notificarionSettingList = null;
		try {
			notificarionSettingList =  this.sessionFactory.getCurrentSession().getNamedQuery(Greige_NotificationSettingsDO.FIND_BY_ID)
									.setParameter(CommonConstants.ID, Id).list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return notificarionSettingList;
	}
}
