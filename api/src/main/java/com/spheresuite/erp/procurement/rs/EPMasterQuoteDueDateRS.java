package com.spheresuite.erp.procurement.rs;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.EPMasterQuoteDueDateDO;
import com.spheresuite.erp.procurement.service.EPMasterQuoteDueDateService;
import com.spheresuite.erp.procurement.web.util.EPMasterQuoteDueDateUtil;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/masterquoteduedate")
public class EPMasterQuoteDueDateRS {

	String validation = null;
	static Logger logger = Logger.getLogger(EPMasterQuoteDueDateRS.class.getName());
	@Autowired
	private EPMasterQuoteDueDateService epMasterquotedueService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EPMasterQuoteDueDateDO epMasterSortDO = new EPMasterQuoteDueDateDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.HOURS) != null && !inputJSON.get(CommonConstants.HOURS).toString().isEmpty()){
			 			epMasterSortDO.setHours(inputJSON.get(CommonConstants.HOURS).toString());
			 		}
			 		epMasterSortDO.setStatus(CommonConstants.ACTIVE);
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			epMasterSortDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			epMasterSortDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		epMasterSortDO.setUpdatedon(new Date());
			 		epMasterSortDO.setCreatedon(new Date());
			 	}
				if(!epMasterquotedueService.persist(epMasterSortDO)){
		 			return CommonWebUtil.buildErrorResponse("Quote Due Hours Already Added").toString();
				}
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Quote Due Hours Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<EPMasterQuoteDueDateDO> cagtegoryList = epMasterquotedueService.retrieveActive();
				respJSON = EPMasterQuoteDueDateUtil.getEPMasterDueList(cagtegoryList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<EPMasterQuoteDueDateDO> categoryList = epMasterquotedueService.retrieve();
				respJSON = EPMasterQuoteDueDateUtil.getEPMasterDueList(categoryList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EPMasterQuoteDueDateDO sortDO = new EPMasterQuoteDueDateDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<EPMasterQuoteDueDateDO> sortList = epMasterquotedueService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(sortList != null && sortList.size() >0){
			 			sortDO = sortList.get(0);
			 			if(inputJSON.get(CommonConstants.HOURS) != null && !inputJSON.get(CommonConstants.HOURS).toString().isEmpty()){
			 				sortDO.setHours(inputJSON.get(CommonConstants.HOURS).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
				 			sortDO.setStatus((char) inputJSON.get(CommonConstants.STATUS).toString().charAt(0));
				 		}
				 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			sortDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		sortDO.setUpdatedon(new Date());
				 		if(!epMasterquotedueService.update(sortDO)){
				 			return CommonWebUtil.buildErrorResponse("Quote Due Hours Already Added").toString();
				 		}
				 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Quote Due Hours Updated");
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
}
