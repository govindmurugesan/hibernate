package com.spheresuite.erp.procurement.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.Greige_TargetPriceDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class Greige_TargetPriceDAO {
	@Autowired
	private GenericDAOImpl<Greige_TargetPriceDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public boolean persist(Greige_TargetPriceDO detailsDO){
		//List<EPMasterSortDO> earningTypeList = null;
		boolean persistStatus = true;
		try {
			genericObject.persist(detailsDO);
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	public List<Greige_TargetPriceDO> retrieve(){
		List<Greige_TargetPriceDO> DetailsList = null;
		try {
			DetailsList = genericObject.retrieve(Greige_TargetPriceDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return DetailsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Greige_TargetPriceDO> retrieveAll(){
		List<Greige_TargetPriceDO> DetailsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(Greige_TargetPriceDO.FIND_ALL)
					.list();
		} catch (Exception eException) {
		} finally {
		}
		return DetailsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Greige_TargetPriceDO> retrieveById(Long id) {
		List<Greige_TargetPriceDO> DetailsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(Greige_TargetPriceDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return DetailsList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<Greige_TargetPriceDO> retrieveBySortId(Long id) {
		List<Greige_TargetPriceDO> DetailsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(Greige_TargetPriceDO.FIND_BY_SORTID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return DetailsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Greige_TargetPriceDO> retrieveBySupplierId(Long id) {
		List<Greige_TargetPriceDO> DetailsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(Greige_TargetPriceDO.FIND_BY_SUPPLIER_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return DetailsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Greige_TargetPriceDO> retrieveByQuoteId(Long id) {
		List<Greige_TargetPriceDO> DetailsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(Greige_TargetPriceDO.FIND_BY_QUOTE_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return DetailsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Greige_TargetPriceDO> retrieveBySupplierSortQuoteId(Long sortId, Long supplierId, String quoteId) {
		List<Greige_TargetPriceDO> DetailsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(Greige_TargetPriceDO.FIND_BY_SUPPLIER_SORT_QUOTE_ID)
					.setParameter(CommonConstants.SORT_ID, sortId)
					.setParameter(CommonConstants.SUPPLIERID, supplierId)
					.setParameter(CommonConstants.QUOT_ID, quoteId)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return DetailsList;
	}
	
	public boolean update(Greige_TargetPriceDO DetailsList){
		boolean updateStatus = true;
		try {
			genericObject.merge(DetailsList);
			/*List<EPMasterSortDO> sortList = null;
			earningTypeList = this.sessionFactory.getCurrentSession().getNamedQuery(EPMasterSortDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, allowanceTypeDO.getName())
					.setParameter(CommonConstants.ID, allowanceTypeDO.getEarningTypeId())
					.list();
			if(earningTypeList != null && earningTypeList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(sortList);
			}*/
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}
	
	

}
