package com.spheresuite.erp.util;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.security.Key;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.spheresuite.erp.domainobject.CustomerAccessDO;
import com.spheresuite.erp.domainobject.SupplierAccessDO;
import com.spheresuite.erp.domainobject.UserDO;
import com.spheresuite.erp.service.DepartmentService;

public class JwtTokenGenerator {
	
	@Autowired
	private DepartmentService departmentService;
	
	public JwtTokenGenerator() {
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
    }
	
	public String createJWT(List<UserDO> userInfo){
		SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
		 
		long nowMillis = System.currentTimeMillis();
		Date dateNow = new Date(nowMillis);
		 
		//We will sign our JWT with our ApiKey secret
		byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary("ths is secret");
		Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());
		 //Let's set the JWT Claims
		List<Long> deptId = new ArrayList<Long>();
		if(userInfo.get(0).getEmployee().getEmpId() != null){
			deptId = departmentService.retrieveisManager(userInfo.get(0).getEmployee().getEmpId().toString());
		}
		JwtBuilder builder = Jwts.builder().setId(deptId.toString())
	                            .setIssuedAt(dateNow)
	                            .setSubject(userInfo.get(0).getEmployee().getEmpId().toString())
	                            .setIssuer(userInfo.get(0).getEmployee().getCompanyemail().toString())
	                            .signWith(signatureAlgorithm, signingKey);
		int ttlMillis = 9000000;
		if (ttlMillis >= 0) {
		    long expMillis = nowMillis + ttlMillis;
		    Date exp = new Date(expMillis);
		    builder.setExpiration(exp);
		}
		String token = builder.compact();
		return token;
	}
	
	public String createSupplierJWT(List<SupplierAccessDO> userInfo){
		SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
		 
		long nowMillis = System.currentTimeMillis();
		Date dateNow = new Date(nowMillis);
		 
		//We will sign our JWT with our ApiKey secret
		byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary("ths is secret");
		Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());
		 //Let's set the JWT Claims
		List<Long> deptId = new ArrayList<Long>();
		if(userInfo.get(0).getContact().getSupplierContactId() != null){
			deptId = departmentService.retrieveisManager(userInfo.get(0).getContact().getSupplierContactId().toString());
		}
		JwtBuilder builder = Jwts.builder().setId(deptId.toString())
	                            .setIssuedAt(dateNow)
	                            .setSubject(userInfo.get(0).getContact().getSupplierContactId().toString())
	                            .setIssuer(userInfo.get(0).getContact().getEmail().toString())
	                            .signWith(signatureAlgorithm, signingKey);
		int ttlMillis = 9000000;
		if (ttlMillis >= 0) {
		    long expMillis = nowMillis + ttlMillis;
		    Date exp = new Date(expMillis);
		    builder.setExpiration(exp);
		}
		String token = builder.compact();
		return token;
	}
	
	public String createCustomerJWT(List<CustomerAccessDO> userInfo){
		SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
		 
		long nowMillis = System.currentTimeMillis();
		Date dateNow = new Date(nowMillis);
		 
		//We will sign our JWT with our ApiKey secret
		byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary("ths is secret");
		Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());
		 //Let's set the JWT Claims
		List<Long> deptId = new ArrayList<Long>();
		if(userInfo.get(0).getContact().getContactId() != null){
			deptId = departmentService.retrieveisManager(userInfo.get(0).getContact().getContactId().toString());
		}
		JwtBuilder builder = Jwts.builder().setId(deptId.toString())
	                            .setIssuedAt(dateNow)
	                            .setSubject(userInfo.get(0).getContact().getContactId().toString())
	                            .setIssuer(userInfo.get(0).getContact().getPrimaryemail().toString())
	                            .signWith(signatureAlgorithm, signingKey);
		int ttlMillis = 9000000;
		if (ttlMillis >= 0) {
		    long expMillis = nowMillis + ttlMillis;
		    Date exp = new Date(expMillis);
		    builder.setExpiration(exp);
		}
		String token = builder.compact();
		return token;
	}
}
