package com.spheresuite.erp.util;

public class CommonConstants {

	private CommonConstants() {

	}
	
	public static final String ID = "id";
	public static final String IDS = "ids";
	
	public static final String INPUT_PARAMS = "inputParam";
	
	public static final String CREATED_ON = "createdon";
	
	public static final String UPDATED_ON = "updatedon";
	
	public static final String SUCCESS_FLAG = "successflag";
	
	public static final String NAME = "name";
	
	public static final String TEMP_PASSWORD="temppassword";

	public static final String ISDELETED="idDeleted";
	
	public static final String DESCRIPTION = "description";
	
	public static final String SESSION = "session";

	public static final String SUB_MENU="submenu";
	
	public static final String URL="url";
	
	public static final String CODE="code";
	
	public static final String CALLING_CODE="callingCode";
	
	public static final String CURRENCY_TYPE="currencyType";
	
	public static final String PROPOSAL_NAME="proposalName";
	
	public static final String PROPOSAL_DATE="proposalDate";
	
	public static final String COUNTRY_ID="countryId";
	
	public static final String STATE_ID="stateId";
	
	public static final String ARTICLE="article";
	
	public static final String SIZE="size";
	
	//Server Settings
	public static final String MYSQL_DRIVER = "com.mysql.jdbc.Driver";
	
	public static final String MSSQL_DRIVER = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
	
	public static final String MYSQL_URL_BASE = "jdbc:mysql://";
	
	public static final String MSSQL_URL_BASE = "jdbc:sqlserver://";
	
	public static final String COLON = ":";
	
	public static final String EQUAL = "=";
	
	public static final String SEMI_COLON = ";";
	
	public static final String SLASH = "/";
	
	
	
	public static final String CITY="city";
	
	public static final String COUNTRY_NAME="countryName";
	
	public static final String STATE_NAME="stateName";
	
	public static final String DESC="desc";
	
	public static final String ROLE_NAME="roleName";
	
	public static final String ROLE_ID="roleId";
	
	public static final String EMAIL="email";
	
	public static final String CONTACTNAME="contactName";
	
	public static final String TANNO="tanno";
	public static final String PANNO="panno";
	public static final String ADDRESS1="address1";
	public static final String ADDRESS2="address2";
	public static final String STATE="state";
	public static final String COUNTRY="country";
	public static final String ZIP="zip";
	public static final String MOBILE="mobile";
	public static final String INDUSTRY="industry";
	public static final String WEBSITE="website";
	public static final String CREATEDON="createdon";
	public static final String UPDATEDON="updatedon";
	public static final String UPDATEDONDISPLAY="updatedondisplay";
	public static final String CREATEDONDISPLAY="cteatedondisplay";
	public static final String EMAIL_SEEN="emailseen";
	public static final String HAS_ATTACHMENT="hasAttachment";
	public static final String FLAG_MAIL="flagMail";
	public static final String REASON="reason";
	
	public static final String WORKLOCATION="workLocation";
	public static final String EMPID="empId";
	public static final String SHIFT_CODE="shiftCode";
	public static final String SHIFT_NAME="shiftName";
	public static final String EMPLOYEEID="employeeId";
	public static final String FIRSTNAME="firstName";
	public static final String FOLDERS="folders";
	public static final String LASTNAME="lastName";
	public static final String MIDDLENAME="middleName";
	public static final String PERSONALCONTACT="personalContact";
	public static final String PRIMARYCONTACT="primaryContact";
	public static final String COMPANYEMAIL="companyEmail";
	public static final String PERSONALEMAIL="personalEmail";
	public static final String AADHAR="aadhar";
	public static final String DATEOFBIRTH="dateOfBirth";
	public static final String EMPTYPE="empType";
	public static final String REPORTTO="reportTo";
	public static final String DEPT="dept";
	public static final String DEPARTMENT="department";
	public static final String TITLE="title";
	public static final String TITLE_ID ="titleId";
	public static final String JOBDESC="jobDesc";
	public static final String STARTDATE="startDate";
	public static final String GENDER="gender";
	public static final String SOURCE="source";
	public static final String PHONE="phone";
	public static final String COMMENT="comment";
	public static final String COMMENT2="comment2";
	public static final String ADDRESS="address";
	public static final String CUSTOMERID="customerId";
	public static final String PROJECTNAME="projectName";
	public static final String DISPLAYNAME="displayName";
	public static final String OTCHECK="otCheck";
	public static final String MIN_HR_OT="minHrOT";
	public static final String PROJECTCODE="projectCode";
	public static final String ENDDATE="enddate";
	public static final String LEADTYPE="leadType";
	public static final String SALUTATION="salutation";
	public static final String PROJECTDURATION="projectDuration";
	public static final String SECONDARYEMAIL="secondaryEmail";
	public static final String PRIMARYEMAIL="primaryEmail";
	public static final String DESIGNATION="designation";
	public static final String MOBILE1="mobile1";
	public static final String MOBILE2="mobile2";
	public static final String PHONE1="phone1";
	public static final String PHONE2="phone2";
	public static final String PHONE3="phone3";
	public static final String PHONE4="phone4";
	public static final String FAX="fax";
	public static final String NOTE="note";
	public static final String VENDOR="vendor";
	public static final String REQUIREMENT="requirement";
	public static final String CONTACTTYPE="contacttype";
	public static final String MENU="menu";
	public static final String MAIL_SMTP_HOST = "mail.smtp.host";
	
	public static final String MAIL_SMTP_PORT = "mail.smtp.port";
	
	public static final String ACCEPT_COST = "acceptCost";
	
	public static final String MAIL_SMTP_AUTH = "mail.smtp.auth";
	
	public static final String MAIL_SMTP_STARTTLS = "mail.smtp.starttls.enable";
	
	public static final String TRUE = "true";
	
	public static final String FALSE = "false";
	
	public static final String SUCCESS_MSG = "The e-mail was sent successfully";
	
	public static final String FAILURE_MSG = "Exception occured, kindly try after some time";
	
	public static final String INPUT_ERROR = "Invalid input error occurred, kindly chec your input";
	
	public static final String STATUS = "status";
	
	public static final String ERRORS = "errors";
	
	public static final String RESULTS = "results";
	
	public static final String RESPONSE = "response";
	
	public static final String COMMA = ",";
	
	public static final String HOST = "host";
	
	public static final String PORT = "port";
	
	public static final String FROMEMAIL = "FromEmail";
	
	public static final String PASSWORD = "password";
	
	public static final String USER_DOMAIN = "userDomain";
	
	public static final String TLS_ENABLED = "tlsEnabled";
	
	public static final String EMAIL_TYPE = "emailType";
	
	public static final String TOEMAIL = "ToEmail";
	
	public static final String CCEMAIL = "CcEmail";
	
	public static final String BCCMAIL = "BccEmail";
	
	public static final String EMAILBODY = "EmailBody";
	
	public static final String AUTHENTICATE="authenticate";
	
	public static final String LABEL="label";
	
	public static final String RETURNURL = "ReturnURL";
	
	public static final String REQUESTTYPE= "requestType";
	
	public static final String HTMLCONTENT= "htmlContent";
	
	public static final String ACCESS_CONTROL_ALLOW_ORIGIN  = "Access-Control-Allow-Origin";
	
	public static final String CACHE_CONTROL   = "Cache-Control";
	
	public static final String NO_CACHE   = "no-cache";
	
	public static final String  STAR = "*";
	
	public static final String EMPTY = "";
	
	public static final String AJAX = "ajax";
	
	public static final String TEXT_HTML = "text/html";
	
	public static final String REG_EXP = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w-_]+\\.)+[\\w]+[\\w]$";

	public static final String NEWPASSWORD="newpassword";

	public static final String WORKLOCATION_CITY="workLocationCity";
	
	public static final String EMPTYPE_NAME="emptypeName";
	
	public static final String DEPT_NAME="deptName";
	
	public static final String STATUS_NAME="statusName";
	
	public static final String STATUS_ID="statusId";
	
	public static final String ISSUE_SUMMARY="issueSummary";
	
	public static final String ISSUE_DETAILS="issueDetails";
	
	public static final String ISSUE_TYPE="issueType";
	
	public static final String ISSUE_TYPE_NAME="issueTypeName";
	
	public static final String PRIORITY_LEVEL_ID="priorityLevelId";
	
	public static final String PRIORITY_LEVEL_NAME="priorityLevelName";
	
	public static final String INDUSTRY_NAME="industryName";
	
	public static final String LEAD_TYPE_NAME="leadTypeName";
	
	public static final String FOLLOW_UP_DATE="followUpDate";
	
	public static final String LEAD_DATE="leadDate";
	
	public static final String CREATED_BY="createdBy";
	
	public static final String CREATED_BY_ID ="createdById";
	
	public static final String UPDATED_BY="updatedBy";
	
	public static final String SALUTATION_NAME="salutationName";
	
	public static String CONTACTTYPE_NAME="contactTypeName";
	
	public static String CONTACT_ID="contactId";
	
	public static String SUBJECT="subject";
	public static String CONTENT="content";
	public static String ATTACH_FILE="attachFile";
	public static String SENTDATE="sentDate";
	public static String HOST_POP = "imap.gmail.com";
	public static String POP_PORT = "993";
	public static String USERNAME = "username";//"pramesh@saptalabs.com"; //username for the mail you want to read
	public static String EMAILPASSWORD = "ramesh123"; //password
	public static String saveDirectory = "D:/software";
	public static final String MAX_COUNT="maxCount";
	public static final String START_iNDEX="startIndex";
	public static final String COUNT = "count";
	public static final String REPORTTO_NAME = "reportToName";
	public static final String TYPE= "type";
	public static final String TYPE_ID = "typeId";
	public static final String TYPE_ID_1 = "typeId1";
	
	public static final String FILENAME= "fileName";
	public static final String FILE_TYPE= "fileType";
	public static final String TYPENAME = "typeName";
	public static final String PROJECTTYPE="projectType";
	public static final String PROJECT_TYPE_NAME="projectTypeName";
	public static final String LEAD_ID="leadId";
	public static final String NOTES="notes";
	public static final String MOM="mom";
	public static final String  LEAD_NAME="leadName";
	public static final String  BODY="body";
	public static final String TIME = "time";
	public static final String DATE = "date";
	public static final String EXPIRED = "expired";
	public static final String DATEDISPLAY = "datedisplay";
	public static final String TASKDETAIL = "taskDetail";
	public static final String CONTACTLIST = "contactList";
	public static final String ICON = "icon";
	public static final String ACTIVEICON="activeicon";
	public static final String EMPNAME= "empName";
	public static final String DURATION= "duration";
	public static final String CURRENCYTYPE= "currencyType";
	public static final String RATE= "rate";
	public static final String PROJECTID= "projectId";
	public static final String PAYMENTTERM = "paymentTerm";
	public static final String PAYMENTTERM_NAME = "PaymentTermName";
	public static final String EXPENSE="expense";
	public static final String EXPENSE_TERM="expenseTerm";
	public static final String SCOPE="scope";
	public static final String TERM="term";
	public static final String INVOICETERM="invoiceTerm";
	public static final String INVOICETERM_LIST = "invoiceTermList";
	public static final String PERCENTAGE = "percentage";
	public static final String CUSTOMERNAME="cutomerName";
	public static final String EMAIL_NOT_REGISTER = "Entered email is not registered";
	public static final String EMPID_NOT_REGISTER = "Entered employee id is not registered";
	public static final String EMAIL_NOT_SENT = "Mail not sent";
	public static final char   PENDING_STATUS = 'P';
	public static final String FIELD_LIST="fieldList";
	public static final String KEY="key";
	public static final String VALUE="value";
	public static final String FILE="file";
	public static final String BILLNUMBER="billNumber";
	public static final String BILLDATE="billDate";
	public static final String DUEDATE="dueDate";
	public static final String FROMDATE="fromDate";
	public static final String FROMDATEDISPLAY="fromDateDisplay";
	public static final String TODATE="toDate";
	public static final String ISEDIT="isEdit";
	public static final String TODATEDISPLAY="toDateDisplay";
	public static final String PROPOSAL_DETAILS="proposalDetails";
	public static final String PROPOSAL_ID="proposalId";
	public static final String MOM_ID="momId";
	public static final String QUANTITY ="quantity";
	public static final String INVOICETERM_TYPE="invoiceTermType";
	public static final String HOT="hot";
	public static final String HOTID="hotId";
	public static final String COLDID="coldId";
	public static final String WARMID="warmId";
	public static final String WARM="warm";
	public static final String COLD="cold";
	public static final String PAIDDATE="paidDate";
	public static final String CURRENCYTYPE_NAME ="currencyTypeName";
	public static final String CUSTOMERLIST="totalCustomer";
	public static final String NEWCUSTOMER="newCustomer";
	public static final String SKIP="skip";
	
	public static final String TOKEN = "token";
	
	public static final String ERROR_CODE = "error_code";
	
	public static final String AUTHENTICATION_ERROECODE = "1000";
	public static final String Q1="q1";
	public static final String Q2="q2";
	public static final String Q3="q3";
	public static final String Q4="q4";
	public static final String ENV="env";
	//public static final String APIURL="api_url";
	public static final String UIURL="ui_url";
	public static final String HTTP="http://";
	public static final String HTTPS="https://";
	public static final String PHOTO="photo";
	public static final String HRPOLICY="hrPolicy";
	public static final String SALESPOLICY="salesPolicy";
	public static final char ACTIVE='a';
	public static final String USERID="userid";
	public static final String PERSONAL="personal";
	public static final String SECONDARY="secondary";
	public static final String CURRENT_TIME="currentTime";
	public static final String CURRENT_DATE="currentDate";
	public static final String SUPERMENU="supermenu";
	public static final String L="L";
	public static final String C="C";
	public static final char PEDING='p';
	public static final String IMAPS="imaps";
	public static final String EXCHANGE_IMAP="imap";
	public static final String POP3="pop3";
	public static final String ATTACHMENT="attachment";
	public static final String CONTENT_TYPE="contentType";
	public static String EXCHANGE_PORT = "143";	
	public static String EXCHANGE_HOST_IMAP = "mail.premiermills.com";
	public static String GMAIL_PORT = "993";
	public static String GMAIL_HOST_POP = "imap.gmail.com";
	public static String POP3_HOST = "mail.premiermills.com";
	public static String INBOX="INBOX";
	public static String SENT_MAIL="[Gmail]/Sent Mail";
	public static String GMAIL="gmail";
	public static String TO="to";
	public static String NO_DUPLICATE="noDuplicate";
	public static String EMPID_FOUND="Employee Id Already Found";
	public static String COMPANYEMAIL_FOUND="Company Email Already Found";
	public static final String DEFAULT_LIST="defaultList";
	public static final String SUB_SUB_MENU="subSubMenu";
	public static final String TRANSFER_FORM="transferFrom";
	public static final String TRANSFER_TO="transferTo";
	public static final String TRANSFER_TYPE="transferType";
	public static final String FILEDATA = "fileData";
	public static final String COLUMFIELDS = "columFields";
	
	public static final char STATUS_OPEN = 'O';
	public static final String HOURS = "hours";
	public static final String LEAVEDAY = "leaveday";
	public static final String DAYS = "days";
	public static final String NO_OF_SALARY_DAYS = "noOfSalaryDays";
	public static final char INACTIVE = 'i';
	
	public static final String LEAVETYPEFROMDATE = "leaveTypeFromDate";
	public static final String LEAVETYPETODATE = "leaveTypeToDate";
	public static final String SATURDAY = "saturday";
	public static final String SUNDAY = "sunday";
	public static final String USEDLEAVES = "usedLeaves";
	public static final String UID = "uId";
	public static final String  YEAR = "year";	
	public static final String WEEKENDDATE = "weekenddate";
	public static final char DRAFT = 'd';
	public static final String TIMESHEETLIST = "timeSheetList";
	public static final String TOTAl = "total";
	public static final char SUBMITTED = 's';
	public static final String EMPCTC = "empctc";
	public static final String CTC_TYPE = "ctcType";
	public static final String EMPCTCID = "empctcId";
	public static final String TAXABLE = "taxable";
	public static final String MAXLIMIT = "maxLimit";
	public static final String SECTION = "section";
	public static final String SECTIONID = "sectionId";
	public static final String ALLOWANCE_TYPE_ID="allowanceTypeId";
	public static final String ALLOWANCETYPE_NAME="allowanceTypeName";
	public static final String DEDUCTION_TYPE_ID="deductionTypeId";
	public static final String DEDUCTIONTYPE_NAME="deductionTypeName";
	public static final String PREPOST_TAX="prepostTax";
	public static final String FIXEDAMOUNT="fixedAmount";
	public static final String PERCENTAGEAMOUNT="percentageAmount";
	public static final String BASICGROSS_PAY="basicGrossPay";
	public static final String WAREHOUSEID="wareHouseId";
	public static final String PROPERTYTYPEID="propertyTypeId";
	public static final String STORAGETYPEID="storageTypeId";
	public static final String PROPERTYTYPE_NAME="propertyTypeName";
	public static final String STORAGETYPE_NAME="storageTypeName";
	public static final String MINEXP="minExp";
	public static final String MAXEXP="maxExp";
	public static final String COST="cost";
	public static final String NOTICE_PEROID="noticePeriod";
	public static final String NOOFPOSITION="noofPosititon";
	public static final String JOININGDATE="joiningDate";
	public static final String REQUIREMENT_ID="requirementId";
	public static final String OFFERDATE="offerDate";
	public static final String APPROVER_ID="approverId";
	public static final String APPROVER="approver";
	public static final String SUPERADMIN="Super Admin";
	public static final String SENDMAIL="noreply@saptalabs.com";
	public static final String SENDPASSWORD="spheresuite@789";
	public static final String CONTACT_NAME="contactName";
	public static final String CONTACT_EMAIL="contactEmail";
	public static final String CONTACT_MOBILE="contactMobile";
	public static final String EMP_NAME="empName";
	public static final String PROBOBILITY="probability";
	public static final String UPDATED_BY_NAME="updatedByName";
	public static final String OPPORTUNITY_ID="opportunityId";
	public static final String OPPORTUNITY_NAME="opportunityName";
	public static final String SALESSTAGE="salesStage";
	public static final String SALESSTAGE_NAME="salesStageName";
	public static final String NUMBER="number";
	public static final String AMOUNT = "amount";
	public static final String INVOICE_AMOUNT = "invoiceAmount";
	public static final String SALES_PERCENTAGE = "salesPercentage";
	public static final String PAYTO = "payto";
	public static final String CLEARED = "c";
	public static final String PAYMENTMODE = "paymentmode";
	public static final String CHEQUENUMBER = "chequenumber";
	public static final String NOTCLEARED = "n";
	public static final String AMOUNTTYPE = "amountype";
	public static final String ORDERNUMBER = "ordernumber";
	public static final String GST="gst";
	public static final String GST_PERCENTAGE="gstPercentage";
	public static final String BACSICPAY="50";
	public static final String MONTHLY="monthly";
	public static final String YTD="ytd";
	public static final String NETMONTHLY="netMonth";
	public static final String NETYEAR="netYear";
	public static final String EARNINGMONTHLY="earningMonth";
	public static final String EARNINGYEAR="earningYear";
	public static final String DEDUCTIONMONTHLY="deductionMonth";
	public static final String DEDUCTIONYEAR="deductionYear";
	public static final String DEDUCTIONLIST="employeeCompensationDeductionList";
	public static final String EARNINGLIST="employeeCompensationEarningList";
	public static final String EARNINGID="allowanceId";
	public static final String DEDUCTIONID="deductionId";
	public static final String EFFECTIVETO="effectiveTo";
	public static final String EFFECTIVEFROM="effectiveFrom";
	public static final String BACSICPAYLABEL="basicPay";
	public static final String EARNINGNAME="earningName";
	public static final String DEDUCTIONNAME="deductionName";
	public static final String PAYROLLMONTH="payrollMonth";
	public static final String PAYROLLLIST="payrollList";
	public static final String PAYROLLBATCHID="payrollBatchId";
	public static final String PAYROLLBATCHNAME="payrollBatchName";
	public static final String PAYROLLTYPE="payrollType";
	public static final String EDUCATIONLEVELID="educationLevelId";
	public static final String EDUCATIONLEVELNAME="educationLevelName";
	public static final String UNIVERSITY="university";
	public static final String AREA="area";
	public static final String FATHERNAME="fatherName";
	public static final String MOTHERNAME="motherName";
	public static final String ADDRESSTYPEID="addressTypeId";
	public static final String ADDRESSTYPENAME="addressTypeName";
	public static final String TERMINATETYPEID="terminateTypeId";
	public static final String TERMINATEDATE="terminateDate";
	public static final String TERMINATEREASON="terminateReason";
	public static final String BANKNAME="bankName";
	public static final String IFSCCODE="ifscCode";
	public static final String BRANCHNAME="branchName";
	public static final String ACCOUNTNAME="accountName";
	public static final String ACCOUNTNUMBER="accountNumber";
	public static final String VOTERID="voterId";
	public static final String RATIONCARD="rationCard";
	public static final String MARITALSTATUS="maritalStatus";
	public static final String SPOUSENAME="spouseName";
	public static final String CHILDREN="children";
	public static final String BLOODGROUP="bloodGroup";
	public static final String LICENSENUMBER="licenseNumber";
	public static final String DATEISSUED="dateIssued";
	public static final String EXPIRYDATE="expiryDate";
	public static final String DATEISSUEDNAME="dateIssuedName";
	public static final String EXPIRYDATENAME="expiryDateName";
	public static final String HOMEPHONE="homePhone";
	public static final String CELLPHONE="cellPhone";
	public static final String WORKPHONE="workPhone";
	public static final String OTHEREMAIL="otherEmail";
	public static final String BONUSMONTH="bonusMonth";
	public static final String PAIDON="paidOn";
	public static final String TAXMONTHLY="taxMonth";
	public static final String TAXYEAR="taxYear";
	public static final String TAXLIST="employeeCompensationTaxList";
	public static final String TAXID="taxId";
	public static final String TAXNAME="taxName";
	public static final String BONUSYEAR="bonusYear";
	public static final String HRREQUEST="hrRequest";
	public static final String PAYROLL="payroll";
	public static final String LOSSOFPAYMONTHLY="lossOfPayMonth";
	public static final String LOSSOFPAYYEAR="lossOfPayYear";
	public static final String SETTINGNAME="settingName";
	public static final String ITSAVINGSETTINGID="itSavingSettingId";
	public static final String ITSAVINGSETTINGNAME="itSavingSettingName";
	
	public static final String CC = "cc";
	
	public static final String FROM = "from";
	
	public static final String BCC = "bcc";
	
	public static final String PAYROLLSTATUS = "payrollStatus";
	public static final char NEW = 'n';
	public static final String FROMTIME = "fromTime";
	public static final String TOTIME = "toTime";
	public static final String MAX_HOUR_EXCUSE = "maxHrExcuse";
	public static final String NO_OF_EXCUSE = "noOfExcuse";
	public static final String FROMTIMEDISPLAY="fromTimeDisplay";
	public static final String TOTIMEDISPLAY="toTimeDisplay";
	public static final String FROMAMOUNT="fromAmount";
	public static final String TOAMOUNT="toAmount";
	public static final String TAXPAYEETYPE= "taxpayeeType";
	public static final String PTAMOUNT="ptAmount";
	public static final String FYFROM="fyFrom";
	public static final String FYTO="fyTo";
	public static final String TAXPAYEEID="taxpayeeId";
	public static final String TAXPAYEENAME="taxpayeeName";
	public static final String ENGAGED="Engaged";
	
	// Email Settings
	public static final String MAIL_TYPE="mailType";
	public static final String SMTP_PORT="smtpPort";
	public static final String SMTP_HOST="smtpHost";
	public static final String IMAP_HOST="imapHost";
	
	// Attendance Settings
	public static final String DATABASE_TYPE="databaseType";
	public static final String DATABASE_NAME="databaseName";
		
	public static final String DEPTMNGR_ID ="deptMngrId";
	public static final String DEPTMNGR_NAME = "deptMngrName";
	public static final String ADMIN = "adminId";
	public static final String ADMINID ="100101";
	public static final String REPORTTO_EMAIL ="reportingEmail";
	public static final String DEPHEAD_EMAIL ="deptHeadEmail";
	public static final String REQUESTBY = "requestBy";
	
	public static final String RESIGNEDDATE="resignedDate";
	public static final String QUARTERLY="quarterly";
	public static final String FYID="fyid";
	public static final String FREQUENCY="frequency";
	public static final String FREQUENCY_NAME="frequencyName";
	public static final String PERIOD="period";
	
	public static final String MONTHLYC="Monthly";
	public static final String QUARTERLYC="Quarterly";
	public static final String YEARLYC="Yearly";
	public static final String MAILTYPE="mailType";
	
	public static final String SUPPLIERNAME="supplierName";
	public static final String WORKDESCRIPTION="workDescription";
	public static final String CUSTOMERCONTACT="customerContact";
	public static final String SUPPLIERCONTACT="supplierContact";
	
	public static final String CATEGORYID = "categoryId";
	public static final String CATEGORY_NAME = "categoryName";
	public static final String SUBCATEGORY = "subcategory";
	public static final String SUBCATEGORY_NAME = "subcategoryName";
	public static final String ISSUEDBY = "issuedBy";
	public static final String NO_ALERT = "noOfAlert";
	public static final String NEXTRENEWAL ="nextRenewal";
	public static final String LEADTIME = "leadTime";
	public static final String RECURRENCE = "recurrence";
	
	
	public static final String VEHICLENO = "vehicleNo";
	public static final String UNITNAME = "unitName";
	public static final String GOODSIN = "goodsIn";
	public static final String GOODSINCOMMENT = "goodsinComments";
	public static final String GOODSOUT = "goodsOut";
	public static final String GOODSOUTCOMMENT = "goodsoutComments";
	public static final String EXITON = "exitOn";
	public static final String ENTEREDON = "enterOn";
	public static final String PURPOSE = "purpose";
	
	//ams
	
	public static final String UNIT = "unit";
	public static final String ASSETNUMBER = "assetNo";
	public static final String ASSETID ="assetId";
	public static final String PCName = "pcName";
	public static final String FLOOR = "floor";
	public static final String DOMAIN = "domain";
	public static final String PCUNDER = "pcunder";
	public static final String PURCHASEDATE = "purchaseDate";
	public static final String MAKE = "make";
	public static final String MONITOR = "monitor";
	public static final String CPUTYPE = "cpuType";
	public static final String CPUSPEED = "cpuSpeed";
	public static final String FDD = "fdd";
	public static final String RAM = "ram";
	public static final String HDD = "hdd";
	public static final String CDD = "cdd";
	public static final String MSOFFICE = "msOffice";
	public static final String MAILID = "mailID";
	public static final String OSMODEL = "osModel";
	public static final String INTERNET = "internet";
	public static final String USB = "usb";
	public static final String IPADDRESS = "ipAddress";
	public static final String FLOOR_NAME = "floorName";
	public static final String DOMAIN_NAME = "domainName";
	public static final String MAKE_NAME = "makeName";
	public static final String CPUTYPE_NAME = "cputypeName";
	public static final String CPUSPEED_NAME = "cpuSpeedName";
	public static final String RAM_NAME = "ramName";
	public static final String MSOFFICE_NAME = "msofficeName";
	public static final String HDD_NAME = "hddName";
	public static final String MAILID_NAME = "mailIDName";
	public static final String OS_NAME = "osName";
	
	
	
	public static final String IDPROOF = "idProof";
	public static final String COMPANY = "company";
	public static final String PONO = "poNo";
	public static final String PERSONTOMEET = "personToMeet";
	
	public static final String CIN="cin";
	public static final String SERVICETAXNUMBER="serviceTaxNumber";
	public static final String GSTIN="gsTin";
	
	public static final char TERMINATED='t';
	public static final String PAYROLLGROUPID = "payrollGroupId";
	public static final String ALLOWENCE_ID ="allowenceId";
	public static final String DEACTIVATEDEARNINGMENU = "deactivatedEarningMenu";
	public static final String DEACTIVATEDDEDUCTIONMENU = "deactivatedDeductionMenu";
	public static final String DEACTIVATEDTAXMENU = "deactivatedTaxMenu";
	public static final String PRODUCT_ID = "productId";
	public static final String PAYROLLGROUPNAME = "payrollGroupName";
	public static final String BASIC = "Basic";
	public static final String LEAVES = "leaves";
	public static final String LEAVEMANAGEMENTID = "leaveManagementId";
	public static final String LEAVETYPEID = "leaveTypeId";
	public static final String HOLIDAYS = "holidays";
	public static final String DAYS_ALLOWED = "daysAllowed";
	public static final String CANCELREQUEST = "cancelRequest";
	public static final String BONUSID = "bonusId";
	public static final String DAPOINTS = "dapoints";
	public static final String DADEDUCTIONPOINT = "daDeductionPoints";
	public static final String ADDDA = "addDA";
	public static final String ADD_DA = "add DA";
	public static final String SUBDA = "subDA";
	public static final String DEARNESSALLOWEANCE = "Dearness Allowance";
	public static final String DEARNESSALLOWEANCEID = "dearnessallowanceId";
	public static final String ADDDEARNESSALLOWEANCE = "add dearness allowance";
	public static final String SUBDEARNESSALLOWEANCE = "sub dearness allowance";
	public static final String MENUID = "menuId";
	
	public static final char PSTATE='p';
	public static final char WSTATE='w';
	
	 
	public static final String PCUNDERID = "pcUnderid";
	public static final String CDD_DVDID = "cdd_dvdId";
	public static final String LEAVEGROUP = "leaveGroup";
	public static final String LEAVEGROUP_ID = "leaveGroupId";
	public static final String DEACTIVATELEAVEMENU = "deactivatedLeaveMenu";
	public static final String DATEOFEXPIRY = "dateOfExpiry";
	public static final String PLACEOFISSUE = "placeOfIssue";
	public static final String CARRYFORWORD = "carryForward";
	public static final String FROMAGE = "fromAge";
	public static final String TOAGE = "toAge";
	public static final String TDSID = "tdsId";
	public static final String APPLY = "apply";
	public static final String PT_ID = "ptId";
	public static final String EDUCATIONCESS = "educationCess";
	public static final String AGE = "age";
	public static final String NUMBEROFLEAVETAKEN  = "noOfLeaveTaken";
	public static final String TIMEIN  = "timeIn";
	public static final String TIMEINMINUTES  = "timeInMinutes";
	public static final String TIMEINSECONDS  = "timeInSeconds";
	public static final String TIMEOUT  = "timeOut";
	public static final String TIMEOUTMINUTES  = "timeOutMinutes";
	public static final String TIMEOUTSECONDS = "timeOutSeconds";
	public static final String MINUTES = "minutes";
	public static final String SECONDS = "seconds";
	public static final String CONDITION = "condition";
	public static final String COSTEFFECTIVE = "costEffective";
	public static final String TIMEOUTDATE  = "timeOutDate";
	public static final String TIMEOUTDATEDISPLAY  = "timeOutDateDisplay";
	public static final String TIMEINDATE  = "timeInDate";
	public static final String TIMEINDATEDISPLAY  = "timeInDateDisplay";
	public static final String PFGROUPID  = "pfGroupId";
	public static final String PFGROUPNAME  = "pfGroupName";
	public static final String ESIGROUPID  = "esiGroupId";
	public static final String ESIGROUPNAME  = "esiGroupName";
	public static final String PFACCOUNTNO  = "pfAccountNo";
	public static final String ESIACCOUNTNO  = "esiAccountNo";
	public static final String SORT  = "sort";
	public static final String YC  = "yc";
	public static final String TC  = "tc";
	public static final String GRCONS  = "grCons";
	public static final String GRCON  = "grCon";
	public static final String BUDGETCON  = "budgetCon";
	public static final String VALUELOSS  = "valueLoss";
	public static final String CUTTINGLOSS  = "cuttingLoss";
	public static final String MULOSS  = "muLoss";
	public static final String BUDGETCOST  = "budgetCost";
	public static final String FABRICTYPE  = "fabrictype";
	public static final String FABRICTYPELIST  = "fabrictypeList";
	public static final String FABRIC_COSTING  = "fabricCosting";
	public static final String FABRICTYPE_NAME  = "fabricTypeName";
	public static final String OCNDETAILS_ID  = "ocnDetailsId";
	public static final String GW  = "gw";
	public static final String ASTATUS = "a";
	public static final String SORT_ID  = "sortId";
	public static final String WAVE  = "wave";
	public static final String OCN  = "ocn";
	public static final String OCN_NUMBER  = "ocnNumber";
	public static final String CPM  = "cpm";
	public static final String CPM_AMOUNT  = "cpmAmount";
	public static final String GREIGE_FABRIC_AMOUNT  = "greigeFabricAmount";
	public static final String DYEING_COST_AMOUNT  = "dyeingCostAmount";
	public static final String FINISHING_COST_AMOUNT  = "finishingCostAmount";
	public static final String TRIMS_AMOUNT  = "trimsAmount";
	public static final String SAM_AMOUNT  = "samAmount";
	public static final String REJECTION_AMOUNT  = "rejectionAmount";
	public static final String GREIGE_FABRICM  = "greigeFabricm";
	public static final String GREIGE_FABRIC  = "greigeFabric";
	public static final String DYEING_COST  = "dyeingCost";
	public static final String FINISHING_COST  = "finishingCost";
	public static final String TRIMS  = "trims";
	public static final String SAM  = "sam";
	public static final String REJECTION  = "rejection";
	public static final String EXPORT_INCENTIVE  = "exportIncentive";
	public static final String BUDGET_GREIGE_FABRIC  = "budgetGreigeFabric";
	public static final String BUDGET_DYEING_COST  = "budgetDyeingCost";
	public static final String BUDGET_FINISHING_COST  = "budgetFinishingCost";
	public static final String BUDGET_TRIMS  = "budgetTrims";
	public static final String BUDGET_SAM  = "budgetSam";
	public static final String BUDGET_REJECTION  = "budgetRejection";
	public static final String GREIGE_FABRIC_QTY  = "greigeFabricQty";
	public static final String DYEING_COST_QTY  = "dyeingCostQty";
	public static final String FINISHING_COST_QTY  = "finishingCostQty";
	public static final String TRIMS_QTY  = "trimsQty";
	public static final String SAM_QTY  = "samQty";
	public static final String REJECTION_QTY  = "rejectionQty";
	public static final String PLANNO  = "planNo";
	public static final String GREIGREG  = "greigReq";
	public static final String GREIGREQDATE  = "greigReqDate";
	public static final String SHIPPINGDATE  = "shippingDate";
	public static final String PENDINGDAYS  = "pendingDays";
	public static final String TARGETPRICE = "targetPrice";
	public static final String TARGETQTY = "targetQty";
	public static final String REQUIREDMETER = "requiredMeter";
	public static final String POSSIBLEMETER = "possibleMeter";
	public static final String GREIGREQDUODATE  = "greigReqDuoDate";
	public static final String GREIGREQDUEHOURSID = "greigReqDueHoursId";
	public static final String GREIGREQDUEHOURS = "greigReqDueHours";
	public static final String PENDINGORDERID  = "pendingOrderId";
	public static final String QUOT_ID  = "quotId";
	public static final String SUPPLIER_QUOTE_ID  = "supplierQuoteId";
	public static final String EXMILLRATE  = "exmillrate";
	public static final String FINALIZEDRATE = "finalizedrate";
	public static final String FINALIZEDQTY = "finalizedQty";
	public static final String PENDINGQTY = "pendingQty";
	public static final String QTYM = "qtym";
	public static final String REEDCOUNT = "reedcount";
	public static final String SUPPLIERID = "supplierId";
	public static final String OCNCOUNT = "ocnCount";
	public static final String OCNDETAILS = "ocnDetails";
	public static final String DSTATUS = "d";
	public static final String RSTATUS = "r";
	public static final String QSTATUS = "q";
	public static final String SSTATUS = "s";
	public static final String QUOTATIONDETAIL  = "quotationDetails";
	public static final String DENT  = "dent";
	public static final String P_STATE = "p";
	public static final String QUOTSTATUS = "quotStatus";
	public static final String APPROVEDSTATUS = "approvedStatus";
	public static final String LEADSTATUSID = "leadStatusId";
	public static final String LEADTYPEID = "leadTypeId";
	public static final String SELECTED = "selected";
	public static final String ENDTIME="endTime";
	public static final String STARTTIME="startTime";
	
	public static final String REGISTERED_ADDRESS="registeredAddr";
	public static final String COMMUNICATION_ADDRESS="communicationAddr";
	public static final String ENTITY_OF_STATUS="entityStatus";
	public static final String NAME_OF_DIRECTORS="nameOfDirectors";
	public static final String GST_REG_NO="gstRegNo";
	public static final String CST_REG_NO="cstRegNo";
	public static final String CENTRAL_EXCISE_REG_NO="centralExciseRegNo";
	public static final String ESI_REG_NO="esiRegNo";
	public static final String PF_REG_NO="pfRegNo";
	public static final String TYPE_OF_ACCOUNT="typeOfAccount";
	public static final String LIST_OF_PRODUCTS="listOfProducts";
	public static final String SPECIALIZED_IN="specializedIn";
	
	public static final String PERMISSIONFROM="permissionFrom";
	public static final String PERMISSIONFROMDISPLAY="permissionFromDisplay";
	public static final String PERMISSIONTO="permissionTo";
	public static final String PERMISSIONTODISPLAY="permissionToDisplay";
	public static final String ACTIVESTRING = "a";
	public static final String NOOFINSTALLMENT = "noOfInstallment";
	public static final String INSTALLMENTAOUNT = "installAmount";
	public static final String INACTIVESTRING = "i";
	public static final String ADVANCEDETAIL = "advanceDetail";
	public static final String MONTH = "month";
	public static final String PENDINGAMOUNT = "pendingAmount";
	public static final String LOANDETAIL = "loanDetail";
	public static final String INSURANCEDETAIL = "insuranceDetail";
	public static final String WEEKLY = "Weekly";
	public static final String PAYROLLWEEKFROM = "payrollWeekFrom";
	public static final String PAYROLLWEEKTO = "payrollWeekTo";
	public static final String DAILY = "Daily";
	public static final String PAYROLLDATE = "payrollDate";
	
	
	public static final String PENDING_OCN_COUNT = "pendingOcnCount";
	
	public static final String QUOTE_REQUESTED_COUNT = "quoteRequestedCount";
	
	public static final String QUOTE_EDIT_COUNT = "quoteEditCount";
	
	public static final String PREVIOUS_QUOTATION_DETAILS = "previousQuotationDetails";
	
	public static final String PARTICIPANTS = "participants";

	public static final String EVENT = "event";
	
	public static final String EVENTNAME = "eventName";
	
	public static final String EVENTID = "eventId";
	
	public static final String TEAM = "team";
	
	public static final String FOLLOW_UPS = "followUps";
	
	public static final String SAMPLE = "sample";
	
	public static final String PRICING = "pricing";
	
	public static final String INDEX = "index";
	
	public static final String MOM_NOTES_LIST = "momNotesList";
	
	public static final String MOM_CONTACTS_LIST = "momContactsList";
	
	public static final String PERMISSIONHOURSPERMONTH = "permissionHoursPerMonth";
	
	public static final String PERMISSIONHOURSPERDAY = "permissionHoursPerDay";
	
	public static final String NUMBEROFPERMISSION = "numberOfPermission";
	
	public static final String ATTENDANCETYPE = "attendanceType";
	
	public static final String NUMBEROFDAYS="numberOfDays";
	public static final String VALIDUPTO="validUpto";
	public static final String APPLYDATE="applyDate";
	public static final String COMPOFFTYPE="compOffType";
	
	public static final String CFTYPE = "cf";
	public static final String CHTYPE = "ch";
	
	public static final String COMPOFF="compOff";
	public static final String PERMISSION="permission";
	public static final String CAT="cat";
	public static final String QUALIFICATION="qualification";
	public static final String PERMANENTADDRESS="permanentAddress";
	public static final String PRESENTADDRESS="presentAddress";
	public static final String PREVIOUSEXPERIENCE="previousExperience";
	public static final String EMERGENCY_CONTACTNO="emergencyContactNo";
	public static final String WAGES="wages";
	public static final String CONTACTNO="contactNo";
	public static final String BANKACNO="bankAcNo";
	public static final String IFSC="ifsc";
	public static final String AADHARNO="aadharNo";
	public static final String PAN_NO="panNo";
	public static final String DOB="dob";
	public static final String DOJ="doj";
	public static final String EARNINGTYPEID="earningTypeId";
	public static final String WORKALLOWANCEID="workAllowanceId";
	public static final String DEACTIVATEDWORKALLOWANCEMENU="deactivatedWorkAllowanceMenu";
	public static final String WORKALLOWANCELIST="workAllowanceList";
	public static final String ATTENDANCEALLOWANCELIST="attendanceAllowanceList";
	
	public static final String PRESENTDAYS="presentdays";
	public static final char RCHAR = 'r';
	public static final String EXPORTLIST= "exportList";
	public static final String ATTENDANCEALLOWANCEID="attendanceAllowanceId";
	public static final String DEACTIVATEDATTENDANCEALLOWANCEMENU="deactivatedAttendanceAllowanceMenu";
	
	public static final String TDSAMOUNT = "tdsAmount";
	public static final String PFTYPE = "pfType";
	public static final String PFAMOUNT = "pfAmount";
	public static final String PFFROM = "pfFrom";
	public static final String PFTO = "pfTo";
	public static final String ESITYPE = "esiType";
	public static final String ESIAMOUNT = "esiAmount";
	public static final String ESIFROM = "esiFrom";
	public static final String ESITO = "esiTo";
	public static final String UAN = "uan";
	public static final String EMPLOYERPERCENTAGE = "employerPercentage";
	public static final String LOPTYPE = "lopType";
	public static final String MULTIPLYBY = "multiplyby";
	public static final String DIVIDEDBY = "dividedBy";
	public static final String EARNEDLEAVE = "earnedLeave";
	public static final String MANAGERID = "managerId";
	public static final String MANAGERNAME = "managerName";
	public static final String AVAILEDLEAVES = "availedLeaves";
	public static final String AVAILABLELEAVES = "availableLeaves";
	public static final String LEAVETYPE = "leaveType";
	public static final String TYPEID = "typeId";
	public static final String PERMISSION_CHECKED = "permissionChecked";
	public static final String COMPENSATION_CHECKED = "compensationChecked";
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
