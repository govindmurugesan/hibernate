package com.spheresuite.erp.crm.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.ContactDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class ContactDAO {
	static Logger logger = Logger.getLogger(ContactDAO.class.getName());
	@Autowired
	private GenericDAOImpl<ContactDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public ContactDO persist(ContactDO contactDO) {
		try {
			genericObject.persist(contactDO);
		} catch (Exception eException) {
		} finally {
		}
		return contactDO;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<ContactDO> retrieve(List<String> ids) {
		List<ContactDO> contactList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(ContactDO.FIND_ALL)
					.setParameterList(CommonConstants.ID, ids)
					.setParameter(CommonConstants.STATUS, "n")
					.list(); 
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return contactList;
	}
	
	@SuppressWarnings("unchecked")
	public List<ContactDO> retrieveAll() {
		List<ContactDO> contactList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(ContactDO.FIND_ALL_DATA)
					.setParameter(CommonConstants.STATUS, "n")
					.list(); 
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return contactList;
	}
	
	@SuppressWarnings("unchecked")
	public List<ContactDO> retrieveById(Long id) {
		List<ContactDO> contactList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(ContactDO.FIND_BY_ID)
					.setParameter(CommonConstants.STATUS, "n")
					.setParameter(CommonConstants.ID, id)
					.list(); 
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return contactList;
	}
	
	@SuppressWarnings("unchecked")
	public List<ContactDO> retriveByEmailId(String email) {
		List<ContactDO> contactList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(ContactDO.FIND_BY_EMAIL_ID)
					.setParameter(CommonConstants.STATUS, "n")
					.setParameter(CommonConstants.EMAIL, email)
					.list(); 
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return contactList;
	}
	
	@SuppressWarnings("unchecked")
	public List<ContactDO> retrieveByLeadId(Long id) {
		List<ContactDO> contactList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(ContactDO.FIND_BY_LEADID)
					.setParameter(CommonConstants.STATUS, "n")
					.setParameter(CommonConstants.ID, id)
					.list(); 
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return contactList;
	}
	
	public ContactDO update(ContactDO contactDO) {
		try {
			genericObject.merge(contactDO);
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return contactDO;
	}
	
	public boolean update(List<ContactDO> contactDO) {
		try {
			for (ContactDO contactDO2 : contactDO) {
				genericObject.merge(contactDO2);
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return true;
	}
	
	public boolean persistList(List<ContactDO> contactDO) {
		try {
			for (ContactDO contactDO2 : contactDO) {
				genericObject.persist(contactDO2);
			}
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
		}
		return true;
	}
}
