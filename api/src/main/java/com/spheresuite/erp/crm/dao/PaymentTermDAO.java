package com.spheresuite.erp.crm.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.OpportunityTypeDO;
import com.spheresuite.erp.domainobject.PaymentTermDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class PaymentTermDAO {
	static Logger logger = Logger.getLogger(PaymentTermDAO.class.getName());

	@Autowired
	private GenericDAOImpl<PaymentTermDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(PaymentTermDO paymentTermDO) {
		boolean persistStatus = true;
		List<PaymentTermDO> paymentTerm = null;
		try {
			paymentTerm = this.sessionFactory.getCurrentSession().getNamedQuery(PaymentTermDO.FIND_BY_NAME)
			.setParameter(CommonConstants.NAME, paymentTermDO.getName())
			.list();
			if(paymentTerm != null && paymentTerm.size() > 0){
				persistStatus = false;
			}else{
				genericObject.persist(paymentTermDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	public List<PaymentTermDO> retrieve() {
		List<PaymentTermDO> paymentTermList = null;
		try {
			paymentTermList = genericObject.retrieve(PaymentTermDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return paymentTermList;
	}
	
	public List<PaymentTermDO> retrieveActive() {
		List<PaymentTermDO> paymentTermList = null;
		try {
			paymentTermList =  genericObject.retrieveActive(CommonConstants.ACTIVE, PaymentTermDO.FIND_BY_STATUS, PaymentTermDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return paymentTermList;
	}
	
	@SuppressWarnings("unchecked")
	public List<PaymentTermDO> retrieveById(Long Id) {
		List<PaymentTermDO> paymentTermList = null;
		try {
			paymentTermList =  this.sessionFactory.getCurrentSession().getNamedQuery(PaymentTermDO.FIND_BY_ID)
								.setParameter(CommonConstants.ID, Id).list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return paymentTermList;
	}
	
	@SuppressWarnings("unchecked")
	public boolean update(PaymentTermDO paymentTermDO) {
		boolean updateStatus = true;
		try {
			List<PaymentTermDO> paymentTermList = null;
			paymentTermList = this.sessionFactory.getCurrentSession().getNamedQuery(OpportunityTypeDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, paymentTermDO.getName())
					.setParameter(CommonConstants.ID, paymentTermDO.getPaymentTermId())
					.list();
			if(paymentTermList != null && paymentTermList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(paymentTermDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}
	
	public boolean persistList(List<PaymentTermDO> paymentTermList){
		try {
			for (PaymentTermDO paymentTermDO : paymentTermList) {
				genericObject.persist(paymentTermDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}

}
