package com.spheresuite.erp.crm.rs;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.crm.service.AssignmentService;
import com.spheresuite.erp.crm.service.LeadService;
import com.spheresuite.erp.crm.service.OpportunitiesDocService;
import com.spheresuite.erp.crm.service.OpportunitiesService;
import com.spheresuite.erp.crm.service.PaymentTermService;
import com.spheresuite.erp.crm.service.ProjectService;
import com.spheresuite.erp.crm.web.util.ProjectUtil;
import com.spheresuite.erp.domainobject.AssignmentDO;
import com.spheresuite.erp.domainobject.LeadDO;
import com.spheresuite.erp.domainobject.OpportunitiesDO;
import com.spheresuite.erp.domainobject.OpportunitiesDocDO;
import com.spheresuite.erp.domainobject.PaymentTermDO;
import com.spheresuite.erp.domainobject.ProjectDO;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.AssignmentUtil;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/assignment")
public class AssignmentRS {

	String validation = null;
	static Logger logger = Logger.getLogger(AssignmentRS.class.getName());

	@Autowired
	private AssignmentService assignmentService;
	
	@Autowired
	private ProjectService projectService;
	
	@Autowired
	private OpportunitiesDocService opportunitiesDocService;
	
	@Autowired
	private OpportunitiesService opportunitiesService;
	
	@Autowired
	private LeadService leadService;
	
	@Autowired
	private PaymentTermService paymentTermService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		AssignmentDO assignmentDO = new AssignmentDO();
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		assignmentDO.setEmpId(inputJSON.get(CommonConstants.EMPID).toString());
			 		/*if(!inputJSON.get(CommonConstants.OPPORTUNITY_ID).toString().isEmpty()){
			 			assignmentDO.setOpportunityId(Long.parseLong(inputJSON.get(CommonConstants.OPPORTUNITY_ID).toString()));
			 		}*/
			 		if(inputJSON.get(CommonConstants.OPPORTUNITY_ID) != null &&  !inputJSON.get(CommonConstants.OPPORTUNITY_ID).toString().isEmpty()){
			 			List<OpportunitiesDO> oppList = opportunitiesService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.OPPORTUNITY_ID).toString()));
			 			if(oppList != null && oppList.size() > 0){
			 				assignmentDO.setOpportunity(oppList.get(0));
			 			}
			 		}
			 		if(inputJSON.get(CommonConstants.CUSTOMERID) != null && !inputJSON.get(CommonConstants.CUSTOMERID).toString().isEmpty()){
			 			List<LeadDO> leadList = leadService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.CUSTOMERID).toString()));
			 			if(leadList != null && leadList.size() > 0){
			 				assignmentDO.setCustomer(leadList.get(0));
			 			}
			 		}
			 		if(inputJSON.get(CommonConstants.PROJECTID) != null && !inputJSON.get(CommonConstants.PROJECTID).toString().isEmpty()){
			 			List<ProjectDO> projectList = projectService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.PROJECTID).toString()));
			 			if(projectList != null && projectList.size() > 0){
			 				assignmentDO.setProject(projectList.get(0));
			 			}
			 		}
			 		if(inputJSON.get(CommonConstants.PAYMENTTERM) != null && !inputJSON.get(CommonConstants.PAYMENTTERM).toString().isEmpty()){
			 			List<PaymentTermDO> paymentTermList = paymentTermService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.PAYMENTTERM).toString()));
			 			if(paymentTermList != null && paymentTermList.size() > 0){
			 				assignmentDO.setPaymentTerms(paymentTermList.get(0));
			 			}
			 		}
			 		/*if(!inputJSON.get(CommonConstants.CUSTOMERID).toString().isEmpty()){
			 			assignmentDO.setCustomerId(Long.parseLong(inputJSON.get(CommonConstants.CUSTOMERID).toString()));
			 		}*/
			 		/*if(!inputJSON.get(CommonConstants.PROJECTID).toString().isEmpty()){
			 			assignmentDO.setProjectId(Long.parseLong(inputJSON.get(CommonConstants.PROJECTID).toString()));
			 		}*/
			 		assignmentDO.setOrderNumber(!inputJSON.get(CommonConstants.ORDERNUMBER).toString().isEmpty() ? inputJSON.get(CommonConstants.ORDERNUMBER).toString() : "");
			 		assignmentDO.setProjectType(!inputJSON.get(CommonConstants.PROJECTTYPE).toString().isEmpty() ? inputJSON.get(CommonConstants.PROJECTTYPE).toString() : "");
			 		assignmentDO.setStartdate(!inputJSON.get(CommonConstants.STARTDATE).toString().isEmpty() ? inputJSON.get(CommonConstants.STARTDATE).toString() : "");
			 		assignmentDO.setEnddate(!inputJSON.get(CommonConstants.ENDDATE).toString().isEmpty() ? inputJSON.get(CommonConstants.ENDDATE).toString() : "");
			 		/*if(!inputJSON.get(CommonConstants.AMOUNTTYPE).toString().isEmpty()){
			 			assignmentDO.setAmountType(Long.parseLong(inputJSON.get(CommonConstants.AMOUNTTYPE).toString()));
			 		}*/
			 		if(!inputJSON.get(CommonConstants.AMOUNT).toString().isEmpty()){
			 			assignmentDO.setAmount(Long.parseLong(inputJSON.get(CommonConstants.AMOUNT).toString()));
			 		}
			 		/*if(!inputJSON.get(CommonConstants.PAYMENTTERM).toString().isEmpty()){
			 			assignmentDO.setPaymentTerms(Long.parseLong(inputJSON.get(CommonConstants.PAYMENTTERM).toString()));
			 		}*/
			 		
			 		assignmentDO.setComments(!inputJSON.get(CommonConstants.COMMENT).toString().isEmpty() ? inputJSON.get(CommonConstants.COMMENT).toString() : "");
			 		assignmentDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			assignmentDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			
			 		}
			 		
			 		if(!inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
			 			assignmentDO.setStatus(inputJSON.get(CommonConstants.STATUS).toString());
			 		}
			 	}
				assignmentService.persist(assignmentDO);
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Assignment Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponseId(assignmentDO.getAssignementId().toString()).toString();
	}
	
	/*@RequestMapping(value = "/retrieveActive/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveById(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<ProjectDO> ProjectList = projectService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = ProjectUtil.getProjectList(ProjectList).toString();
				}
			
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}*/
	
	/*@RequestMapping(value = "/retrieveByOpportunityId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByOpportunityId(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<ProjectDO> ProjectList = projectService.retrieveByOpportunityId(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = ProjectUtil.getProjectList(ProjectList).toString();
				}
			
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}*/
	@RequestMapping(value = "/retrieveByEmp/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByEmp(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<AssignmentDO> assignmnetList = assignmentService.retrieveByEmp(inputJSON.get(CommonConstants.ID).toString());
					respJSON = AssignmentUtil.getAllowanceList(assignmnetList).toString();
				}
			
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveById/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveById(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<AssignmentDO> assignmnetList = assignmentService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = AssignmentUtil.getAllowanceList(assignmnetList).toString();
				}
			
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}

	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<AssignmentDO> assignmnetList = assignmentService.retrieve();
				respJSON = AssignmentUtil.getAllowanceList(assignmnetList).toString();
				
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				AssignmentDO assignmentDO = new AssignmentDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<AssignmentDO> assignmentList = assignmentService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		assignmentDO = assignmentList.get(0);
			 		assignmentDO.setEmpId(inputJSON.get(CommonConstants.EMPID).toString());
			 		if(inputJSON.get(CommonConstants.OPPORTUNITY_ID) != null &&  !inputJSON.get(CommonConstants.OPPORTUNITY_ID).toString().isEmpty()){
			 			List<OpportunitiesDO> oppList = opportunitiesService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.OPPORTUNITY_ID).toString()));
			 			if(oppList != null && oppList.size() > 0){
			 				assignmentDO.setOpportunity(oppList.get(0));
			 			}
			 		}
			 		if(inputJSON.get(CommonConstants.CUSTOMERID) != null && !inputJSON.get(CommonConstants.CUSTOMERID).toString().isEmpty()){
			 			List<LeadDO> leadList = leadService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.CUSTOMERID).toString()));
			 			if(leadList != null && leadList.size() > 0){
			 				assignmentDO.setCustomer(leadList.get(0));
			 			}
			 		}
			 		if(inputJSON.get(CommonConstants.PROJECTID) != null && !inputJSON.get(CommonConstants.PROJECTID).toString().isEmpty()){
			 			List<ProjectDO> projectList = projectService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.PROJECTID).toString()));
			 			if(projectList != null && projectList.size() > 0){
			 				assignmentDO.setProject(projectList.get(0));
			 			}
			 		}
			 		if(inputJSON.get(CommonConstants.PAYMENTTERM) != null && !inputJSON.get(CommonConstants.PAYMENTTERM).toString().isEmpty()){
			 			List<PaymentTermDO> paymentTermList = paymentTermService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.PAYMENTTERM).toString()));
			 			if(paymentTermList != null && paymentTermList.size() > 0){
			 				assignmentDO.setPaymentTerms(paymentTermList.get(0));
			 			}
			 		}/*
			 		if(!inputJSON.get(CommonConstants.OPPORTUNITY_ID).toString().isEmpty()){
			 			assignmentDO.setOpportunityId(Long.parseLong(inputJSON.get(CommonConstants.OPPORTUNITY_ID).toString()));
			 		}
			 		if(!inputJSON.get(CommonConstants.CUSTOMERID).toString().isEmpty()){
			 			assignmentDO.setCustomerId(Long.parseLong(inputJSON.get(CommonConstants.CUSTOMERID).toString()));
			 		}*/
			 		
			 		if(!inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
			 			assignmentDO.setStatus(inputJSON.get(CommonConstants.STATUS).toString());
			 		}
			 		/*if(!inputJSON.get(CommonConstants.PROJECTID).toString().isEmpty()){
			 			assignmentDO.setProjectId(Long.parseLong(inputJSON.get(CommonConstants.PROJECTID).toString()));
			 		}*/
			 		assignmentDO.setOrderNumber(!inputJSON.get(CommonConstants.ORDERNUMBER).toString().isEmpty() ? inputJSON.get(CommonConstants.ORDERNUMBER).toString() : "");
			 		assignmentDO.setProjectType(!inputJSON.get(CommonConstants.PROJECTTYPE).toString().isEmpty() ? inputJSON.get(CommonConstants.PROJECTTYPE).toString() : "");
			 		assignmentDO.setStartdate(!inputJSON.get(CommonConstants.STARTDATE).toString().isEmpty() ? inputJSON.get(CommonConstants.STARTDATE).toString() : "");
			 		assignmentDO.setEnddate(!inputJSON.get(CommonConstants.ENDDATE).toString().isEmpty() ? inputJSON.get(CommonConstants.ENDDATE).toString() : "");
			 		/*if(!inputJSON.get(CommonConstants.AMOUNTTYPE).toString().isEmpty()){
			 			assignmentDO.setAmountType(Long.parseLong(inputJSON.get(CommonConstants.AMOUNTTYPE).toString()));
			 		}*/
			 		if(!inputJSON.get(CommonConstants.AMOUNT).toString().isEmpty()){
			 			assignmentDO.setAmount(Long.parseLong(inputJSON.get(CommonConstants.AMOUNT).toString()));
			 		}
			 		/*if(!inputJSON.get(CommonConstants.PAYMENTTERM).toString().isEmpty()){
			 			assignmentDO.setPaymentTerms(Long.parseLong(inputJSON.get(CommonConstants.PAYMENTTERM).toString()));
			 		}*/
			 		
			 		assignmentDO.setComments(!inputJSON.get(CommonConstants.COMMENT).toString().isEmpty() ? inputJSON.get(CommonConstants.COMMENT).toString() : "");
			 		assignmentDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			assignmentDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			
			 		}
			 		assignmentService.update(assignmentDO);
			 		//opportunitiesDocService.delete(projectDO.getId());
			 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Assignment Updated");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retrieveByProjectId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByProjectId(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					//List<ProjectDO> ProjectList = projectService.retrieveByProjectId(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					List<ProjectDO> ProjectDetail = projectService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = ProjectUtil.getProjectList(ProjectDetail).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/updatepic", method = RequestMethod.POST)
	public @ResponseBody String updatepic(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				OpportunitiesDocDO opportunitiesDocDO = new OpportunitiesDocDO();
				/*JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("data").toString());*/
			 	if (request.getParameter("file") != null && request.getParameter("id") != null){
			 		List<OpportunitiesDO> oppList = opportunitiesService.retrieveById(Long.parseLong(request.getParameter("id").toString()));
			 		opportunitiesDocDO.setOpportunity(oppList.get(0));
			 		opportunitiesDocDO.setPhoto(request.getParameter("file"));
			 		opportunitiesDocDO.setFileName(request.getParameter("name"));
			 		opportunitiesDocService.update(opportunitiesDocDO);
 			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
