package com.spheresuite.erp.crm.web.util;

import java.util.List;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.crm.service.OpportunitiesService;
import com.spheresuite.erp.domainobject.ProjectDO;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.GSTTaxSlabService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
@Component
public class ProjectUtil {
	
	private ProjectUtil() {}
	
	@Autowired
	private OpportunitiesService opportunitiesServiceTemp;
	
	@Autowired
	private GSTTaxSlabService gSTTaxSlabServiceTemp;
	
	@Autowired
	private EmployeeService employeeServiceTemp;
	
	private static EmployeeService employeeService;
	
	private static OpportunitiesService opportunitiesService;
	
	private static GSTTaxSlabService gSTTaxSlabService;
	
	@PostConstruct
	public void init() {
		employeeService = this.employeeServiceTemp;
		opportunitiesService = this.opportunitiesServiceTemp;
		gSTTaxSlabService = this.gSTTaxSlabServiceTemp;
	}
	
	public static JSONObject getProjectList(List<ProjectDO> projectList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (ProjectDO project : projectList) {
				resultJSONArray.put(getProjectDetailObject(project));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getProjectDetailObject(ProjectDO project)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(project.getProjectId()));
		
		if(project.getOpportunity() != null){
			result.put(CommonConstants.OPPORTUNITY_ID, String.valueOf(project.getOpportunity().getOpportunityId()));
			result.put(CommonConstants.OPPORTUNITY_NAME, String.valueOf(project.getOpportunity().getProjectname()));
		}else{
			result.put(CommonConstants.OPPORTUNITY_ID, "");
			result.put(CommonConstants.OPPORTUNITY_NAME, "");
		}
		if(project.getProjectname() != null){
			result.put(CommonConstants.PROJECTNAME, String.valueOf(project.getProjectname()));
		}else{
			result.put(CommonConstants.PROJECTNAME,"");
		}
		if(project.getDisplayname() != null){
			result.put(CommonConstants.DISPLAYNAME, String.valueOf(project.getDisplayname()));
		}else{
			result.put(CommonConstants.DISPLAYNAME,"");
		}
		if(project.getStartdate() != null){
			result.put(CommonConstants.STARTDATE, String.valueOf(project.getStartdate()));
		}else{
			result.put(CommonConstants.STARTDATE,"");
		}
		if(project.getEnddate() != null){
			result.put(CommonConstants.ENDDATE, String.valueOf(project.getEnddate()));
		}else{
			result.put(CommonConstants.ENDDATE,"");
		}
		/*if(project.getUpdatedby() != null){
			result.put(CommonConstants.UPDATED_BY, String.valueOf(project.getUpdatedby()));
		}else{
			result.put(CommonConstants.UPDATED_BY,"");
		}*/
		if(project.getProjectduration() != null){
			result.put(CommonConstants.PROJECTDURATION, String.valueOf(project.getProjectduration()));
		}else{
			result.put(CommonConstants.PROJECTDURATION,"");
		}
		if(project.getComment() != null){
			result.put(CommonConstants.COMMENT, String.valueOf(project.getComment()));
		}else{
			result.put(CommonConstants.COMMENT,"");
		}
		if(project.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(project.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		if(project.getUpdatedon() != null){
			result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(project.getUpdatedon())));
		}else{
			result.put(CommonConstants.UPDATEDON,"");
		}
		
		if(project.getAmount() != null){
			result.put(CommonConstants.COST, String.valueOf(project.getAmount()));
		}else{
			result.put(CommonConstants.COST, "");
		}
		
		if(project.getProbability() != null){
			result.put(CommonConstants.PROBOBILITY, String.valueOf(project.getProbability()));
		}else{
			result.put(CommonConstants.PROBOBILITY, "");
		}
		
		if(project.getGstTaxSlab() != null){
			result.put(CommonConstants.GST, String.valueOf(project.getGstTaxSlab().getGstTaxSlabId()));
			result.put(CommonConstants.GST_PERCENTAGE, String.valueOf(project.getGstTaxSlab().getPercentage()));
		}else{
			result.put(CommonConstants.GST, "");
			result.put(CommonConstants.GST_PERCENTAGE, "");
		}
		
		return result;
	}
}
