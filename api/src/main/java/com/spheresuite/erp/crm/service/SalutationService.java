package com.spheresuite.erp.crm.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.dao.SalutationDAO;
import com.spheresuite.erp.domainobject.SalutationDO;

@Service
@Transactional
public class SalutationService {
	static Logger logger = Logger.getLogger(SalutationService.class.getName());
	
	@Autowired
	private SalutationDAO salutationDAO;
	
	@Transactional
	public boolean persist(SalutationDO salutationDO) {
		return salutationDAO.persist(salutationDO);
	}

	@Transactional
	public List<SalutationDO> retrieveActive() {
		return salutationDAO.retrieveActive();
	}
	
	@Transactional
	public List<SalutationDO> retrieveById(Long Id) {
		return salutationDAO.retrieveById(Id);
	}
	
	@Transactional
	public List<SalutationDO> retrieve() {
		return salutationDAO.retrieve();
	}
	
	@Transactional
	public boolean update(SalutationDO salutationDO) {
		return salutationDAO.update(salutationDO);
	}
	
	@Transactional
	public boolean persistList(List<SalutationDO> salutationDO) {
		return salutationDAO.persistList(salutationDO);
	}
	
	@Transactional
	public List<SalutationDO> retrieveByName(String name) {
		return salutationDAO.retrieveByName(name);
	}
}
