package com.spheresuite.erp.crm.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.crm.dao.ContactDAO;
import com.spheresuite.erp.domainobject.ContactDO;
@Service
@Transactional
public class ContactService {
	static Logger logger = Logger.getLogger(ContactService.class.getName());
	
	@Autowired
	private ContactDAO contactDAO;
	
	@Transactional
	public ContactDO persist(ContactDO contactDO)  {
		return contactDAO.persist(contactDO);
	}

	@Transactional
	public List<ContactDO> retriveById(Long id)  {
		return contactDAO.retrieveById(id);
	}
	
	@Transactional
	public List<ContactDO> retriveByEmailId(String email)  {
		return contactDAO.retriveByEmailId(email);
	}

	@Transactional
	public List<ContactDO> retriveByLeadId(Long id)  {
		return contactDAO.retrieveByLeadId(id);
	}

	@Transactional
	public List<ContactDO> retrieve(List<String> ids)  {
		return contactDAO.retrieve(ids);
	}

	@Transactional
	public List<ContactDO> retrieveAll()  {
		return contactDAO.retrieveAll();
	}

	@Transactional
	public ContactDO update(ContactDO contactDO)  {
		return contactDAO.update(contactDO);
	}

	@Transactional
	public boolean update(List<ContactDO> contactDO)  {
		return contactDAO.update(contactDO);
	}

	@Transactional
	public boolean persistList(List<ContactDO> contactDO)  {
		return contactDAO.persistList(contactDO);
	}
}
