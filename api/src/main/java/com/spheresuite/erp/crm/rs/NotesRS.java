package com.spheresuite.erp.crm.rs;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.crm.service.LeadService;
import com.spheresuite.erp.crm.service.NotesService;
import com.spheresuite.erp.crm.web.util.NotesUtil;
import com.spheresuite.erp.domainobject.LeadDO;
import com.spheresuite.erp.domainobject.NotesDO;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/notes")
public class NotesRS {

	String validation = null;
	static Logger logger = Logger.getLogger(NotesRS.class.getName());

	@Autowired
	private NotesService notesService;
	
	@Autowired
	private LeadService leadService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				NotesDO notesDO = new NotesDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.LEAD_ID) != null && !inputJSON.get(CommonConstants.LEAD_ID).toString().isEmpty()){
			 			List<LeadDO> leadList = leadService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.LEAD_ID).toString()));
			 			if(leadList != null && leadList.size() > 0){
			 				notesDO.setLead(leadList.get(0));
			 			}
			 		}
			 		/*if(!inputJSON.get(CommonConstants.LEAD_ID).toString().isEmpty()){
			 			notesDO.setLeadId(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.LEAD_ID).toString() : ""));
			 		}*/
			 		notesDO.setNote(inputJSON.get(CommonConstants.BODY) != null && !inputJSON.get(CommonConstants.BODY).toString().isEmpty() ? inputJSON.get(CommonConstants.BODY).toString() : "");
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			notesDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			notesDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			notesDO.setEmpId((inputJSON.get(CommonConstants.UPDATED_BY).toString()));
			 		}
			 		notesDO.setCreatedon(new Date());
			 		notesDO.setUpdatedon(new Date());
			 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Note Created");
			 	}
				notesService.persist(notesDO);
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<NotesDO> notesList = notesService.retrieve();
				respJSON = NotesUtil.getNotesList(notesList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveByLeadId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByLeadid(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
			 	if (inputJSON != null){
			 		if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 			List<NotesDO> notesList = notesService.retrieveByLeadId(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
						respJSON = NotesUtil.getNotesList(notesList).toString();
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				NotesDO notesDO = new NotesDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		if(!inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 			List<NotesDO> notesList = notesService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 			if(notesList != null && notesList.size() > 0){
			 				notesDO = notesList.get(0);
			 				notesDO.setNote(inputJSON.get(CommonConstants.BODY).toString() != null && !inputJSON.get(CommonConstants.BODY).toString().isEmpty() ? inputJSON.get(CommonConstants.BODY).toString() : "");
			 				if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			notesDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 		}
					 		notesDO.setUpdatedon(new Date());
					 		notesService.update(notesDO);
					 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Note Updated");
			 			}
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
