package com.spheresuite.erp.crm.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.crm.dao.LogActivityDAO;
import com.spheresuite.erp.domainobject.LogActivityDO;
@Service
@Transactional
public class LogActivityService {
	static Logger logger = Logger.getLogger(LogActivityService.class.getName());
	@Autowired
	private LogActivityDAO logActivityDAO;
	
	@Transactional
	public LogActivityDO persist(LogActivityDO logDO) {
		return logActivityDAO.persist(logDO);
	}

	@Transactional
	public LogActivityDO update(LogActivityDO logDO) {
		return logActivityDAO.update(logDO);
	}

	@Transactional
	public List<LogActivityDO> retrieveById(Long Id) {
		return logActivityDAO.retrieveById(Id);
	}

	@Transactional
	public List<LogActivityDO> retrieveByLeadId(Long Id) {
		return logActivityDAO.retrieveByLeadId(Id);
	}


	@Transactional
	public List<LogActivityDO> retrieve(String type) {
		return logActivityDAO.retrieve(type);
	}
}
