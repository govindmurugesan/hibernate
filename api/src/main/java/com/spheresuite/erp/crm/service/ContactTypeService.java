package com.spheresuite.erp.crm.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.crm.dao.ContactTypeDAO;
import com.spheresuite.erp.domainobject.ContactTypeDO;
@Service
@Transactional
public class ContactTypeService {
	static Logger logger = Logger.getLogger(ContactTypeService.class.getName());

	@Autowired
	private ContactTypeDAO contactTypeDAO;

	@Transactional
	public boolean persist(ContactTypeDO countryDO)  {
		return contactTypeDAO.persist(countryDO);
	}

	@Transactional
	public List<ContactTypeDO> retrieveActive()  {
		return contactTypeDAO.retrieveActive();
	}

	@Transactional
	public List<ContactTypeDO> retrieveById(Long id)  {
		return contactTypeDAO.retrieveById(id);
	}
	
	@Transactional
	public List<ContactTypeDO> retrieveByName(String name)  {
		return contactTypeDAO.retrieveByName(name);
	}

	@Transactional
	public List<ContactTypeDO> retrieve()  {
		return contactTypeDAO.retrieve();
	}

	@Transactional
	public boolean update(ContactTypeDO countryDO)  {
		return contactTypeDAO.update(countryDO);
	}
	
	@Transactional
	public boolean persistList(List<ContactTypeDO> countryDO) {
		return contactTypeDAO.persistList(countryDO);
	}
}
