package com.spheresuite.erp.crm.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.crm.dao.MinutesOfMeetingDAO;
import com.spheresuite.erp.domainobject.MinutesOfMeetingDO;
@Service
@Transactional
public class MinutesOfMeetingService {
	static Logger logger = Logger.getLogger(MinutesOfMeetingService.class.getName());
	@Autowired
	private MinutesOfMeetingDAO momDAO;

	@Transactional
	public MinutesOfMeetingDO persist(MinutesOfMeetingDO mom) {
		return momDAO.persist(mom);
	}

	@Transactional
	public List<MinutesOfMeetingDO> retrieveById(Long momId) {
		return momDAO.retrieveById(momId);
	}
	
	@Transactional
	public List<MinutesOfMeetingDO> retrieveByCustomerId(Long custId) {
		return momDAO.retrieveByCustomerId(custId);
	}
	
	@Transactional
	public List<MinutesOfMeetingDO> retrieveByEvent(Long custId) {
		return momDAO.retrieveByEvent(custId);
	}
	
	@Transactional
	public List<MinutesOfMeetingDO> retrieveByEmpMom(Long eventId, List<String> id) {
		return momDAO.retrieveByEmpMom(eventId, id);
	}
	
	@Transactional
	public List<MinutesOfMeetingDO> retrieveByName(String name) {
		return momDAO.retrieveByName(name);
	}
	
	@Transactional
	public List<MinutesOfMeetingDO> retrieve() {
		return momDAO.retrieve();
	}
	
	@Transactional
	public MinutesOfMeetingDO update(MinutesOfMeetingDO mom) {
		return momDAO.update(mom);
	}

}
