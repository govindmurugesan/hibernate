package com.spheresuite.erp.crm.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.crm.dao.AssignmentDAO;
import com.spheresuite.erp.domainobject.AssignmentDO;

@Service
@Transactional
public class AssignmentService {
	static Logger logger = Logger.getLogger(AssignmentService.class.getName());
	
	@Autowired
	private AssignmentDAO assignmentDAO;
	@Transactional
	public AssignmentDO persist(AssignmentDO assignmentDO)  {
		return assignmentDAO.persist(assignmentDO);
	}
	@Transactional
	public List<AssignmentDO> retrieveActive()  {
		return assignmentDAO.retrieveActive();
	}
	@Transactional
	public List<AssignmentDO> retrieveById(Long Id)  {
		return assignmentDAO.retrieveById(Id);
	}
	@Transactional
	public List<AssignmentDO> retrieveByEmp(String Id)  {
		return assignmentDAO.retrieveByEmp(Id);
	}
	@Transactional
	public List<AssignmentDO> retrieve()  {
		return assignmentDAO.retrieve();
	}
	@Transactional
	public AssignmentDO update(AssignmentDO assignmentDO)  {
		return assignmentDAO.update(assignmentDO);
	}
}
