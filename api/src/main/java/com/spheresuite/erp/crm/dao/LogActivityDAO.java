package com.spheresuite.erp.crm.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.LogActivityDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class LogActivityDAO {
	static Logger logger = Logger.getLogger(LogActivityDAO.class.getName());
	@Autowired
	private GenericDAOImpl<LogActivityDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public LogActivityDO persist(LogActivityDO logDo)  {
		try {
			genericObject.persist(logDo);
		} catch (Exception eException) {
		} finally {
		}
		return logDo;
	}
	
	public LogActivityDO update(LogActivityDO logDo)  {
		try {
			genericObject.merge(logDo);
		} catch (Exception eException) {
		} finally {
		}
		return logDo;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<LogActivityDO> retrieve(String type)  {
		List<LogActivityDO> logList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LogActivityDO.FIND_ALL)
			.setParameter(CommonConstants.TYPE, type)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return logList;
	}
	
	@SuppressWarnings("unchecked")
	public List<LogActivityDO> retrieveById(Long Id)  {
		List<LogActivityDO> logList = null;
		try {
			logList =  this.sessionFactory.getCurrentSession().getNamedQuery(LogActivityDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, Id).list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return logList;
	}
	
	@SuppressWarnings("unchecked")
	public List<LogActivityDO> retrieveByLeadId(Long Id)  {
		List<LogActivityDO> logList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LogActivityDO.FIND_BY_LEAD_ID)
			.setParameter(CommonConstants.ID, Id)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return logList;
	}
}
