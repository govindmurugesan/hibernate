package com.spheresuite.erp.crm.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.crm.dao.SalesTargetDAO;
import com.spheresuite.erp.domainobject.SalesTargetDO;

@Service
@Transactional
public class SalesTargetService {
	static Logger logger = Logger.getLogger(SalesTargetService.class.getName());
	
	@Autowired
	private SalesTargetDAO salesTargetDAO;
	
	@Transactional
	public SalesTargetDO persist(SalesTargetDO salesTargetDO) {
		return salesTargetDAO.persist(salesTargetDO);
	}

	@Transactional
	public List<SalesTargetDO> retriveById(Long id) {
		return salesTargetDAO.retrieveById(id);
	}
	
	@Transactional
	public List<SalesTargetDO> retrieveByEmpId(String empId) {
		return salesTargetDAO.retrieveByEmpId(empId);
	}
	
	@Transactional
	public List<SalesTargetDO> retrieveByYear(Long fromDate, Long toDate) {
		return salesTargetDAO.retrieveByYear(fromDate, toDate);
	}
	
	@Transactional
	public List<SalesTargetDO> retrieveByYearForEmp(Long fromDate, Long toDate, String empId) {
		return salesTargetDAO.retrieveByYearForEmp(fromDate, toDate, empId);
	}
	
	@Transactional
	public List<SalesTargetDO> retrieve(List<String> id) {
		return salesTargetDAO.retrieve(id);
	}
	
	@Transactional
	public List<SalesTargetDO> retrieveAll() {
		return salesTargetDAO.retrieveAll();
	}
	
	@Transactional
	public SalesTargetDO update(SalesTargetDO salesTargetDO) {
		return salesTargetDAO.update(salesTargetDO);
	}
}
