package com.spheresuite.erp.crm.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.crm.dao.ProjectDAO;
import com.spheresuite.erp.domainobject.ProjectDO;

@Service
@Transactional
public class ProjectService {
	static Logger logger = Logger.getLogger(ProjectService.class.getName());
	
	@Autowired
	private ProjectDAO projectDAO;
	
	@Transactional
	public ProjectDO persist(ProjectDO projectDO) {
		return projectDAO.persist(projectDO);
	}

	@Transactional
	public List<ProjectDO> retrieveById(Long Id) {
		return projectDAO.retrieveById(Id);
	}
	
	@Transactional
	public List<ProjectDO> retrieveByOpportunityId(Long Id) {
		return projectDAO.retrieveByOpportunityId(Id);
	}
	
	@Transactional
	public List<ProjectDO> retrieve(List<String> Id) {
		return projectDAO.retrieve(Id);
	}
	
	@Transactional
	public List<ProjectDO> retrieveAll() {
		return projectDAO.retrieveAll();
	}
	
	@Transactional
	public ProjectDO update(ProjectDO projectDO) {
		return projectDAO.update(projectDO);
	}
	
	@Transactional
	public boolean persistList(List<ProjectDO> projectDO) {
		return projectDAO.persistList(projectDO);
	}
}
