package com.spheresuite.erp.crm.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.ProposalDocumentsDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class ProposalDocumentsDAO {
	static Logger logger = Logger.getLogger(ProposalDocumentsDAO.class.getName());

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private GenericDAOImpl<ProposalDocumentsDO> genericObject;
	
	public List<ProposalDocumentsDO> persistList(List<ProposalDocumentsDO> proposalDocumentList) {
		try {
			for (ProposalDocumentsDO proposalDocumnetDO : proposalDocumentList) {
				genericObject.persist(proposalDocumnetDO);
			}
		} catch (Exception eException) {
			//throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			//SessionManager.closeEntityManager(em);
		}
		return proposalDocumentList;
	}
	
	public ProposalDocumentsDO persist(ProposalDocumentsDO proposalDocumnetDO) {
		try {
				genericObject.persist(proposalDocumnetDO);
		} catch (Exception eException) {
			//throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			//SessionManager.closeEntityManager(em);
		}
		return proposalDocumnetDO;
	}
	
	@SuppressWarnings("unchecked")
	public List<ProposalDocumentsDO> retrieveById(Long proposalId) {
		List<ProposalDocumentsDO> proposalList = null;
		try {
			proposalList = this.sessionFactory.getCurrentSession().getNamedQuery(ProposalDocumentsDO.FIND_BY_PROPOSAL_ID)
										.setParameter(CommonConstants.PROPOSAL_DETAILS, proposalId)
										.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return proposalList;
	}
	
	public boolean delete(Long id) {
		boolean deleteStatus = true;
		try {
			this.sessionFactory.getCurrentSession().createQuery("delete from ProposalDocumentsDO e where e.proposal.proposalId=:id")
										.setParameter(CommonConstants.ID, id)
										.executeUpdate();
		} catch (Exception eException) {
			deleteStatus = false;
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return deleteStatus;
	}
}
