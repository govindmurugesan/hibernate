package com.spheresuite.erp.crm.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.crm.dao.OpportunitiesDAO;
import com.spheresuite.erp.domainobject.OpportunitiesDO;
@Service
@Transactional
public class OpportunitiesService {
	static Logger logger = Logger.getLogger(OpportunitiesService.class.getName());
	@Autowired
	private OpportunitiesDAO opportunitiesDAO;
	
	@Transactional
	public OpportunitiesDO persist(OpportunitiesDO projectDO) {
		return opportunitiesDAO.persist(projectDO);
	}

	@Transactional
	public List<OpportunitiesDO> retrieveById(Long Id) {
		return opportunitiesDAO.retrieveById(Id);
	}

	@Transactional
	public List<OpportunitiesDO> retrieveByContactId(Long Id) {
		return opportunitiesDAO.retrieveByContactId(Id);
	}

	@Transactional
	public Long retrieveBySalesStageId(Long Id) {
		return opportunitiesDAO.retrieveBySalesStageId(Id);
	}

	@Transactional
	public List<OpportunitiesDO> retrieveByCustomerId(Long Id) {
		return opportunitiesDAO.retrieveByCustomerId(Id);
	}

	@Transactional
	public List<OpportunitiesDO> retrieve(List<String> Id) {
		return opportunitiesDAO.retrieve(Id);
	}

	@Transactional
	public List<OpportunitiesDO> retrieveAll() {
		return opportunitiesDAO.retrieveAll();
	}

	@Transactional
	public Long retrieveForCustomer(List<Long> ids) {
		return opportunitiesDAO.retrieveForCustomer(ids);
	}

	@Transactional
	public OpportunitiesDO update(OpportunitiesDO projectDO) {
		return opportunitiesDAO.update(projectDO);
	}

	@Transactional
	public boolean persistList(List<OpportunitiesDO> projectDO) {
		return opportunitiesDAO.persistList(projectDO);
	}
}
