package com.spheresuite.erp.crm.web.util;

import java.util.List;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.crm.service.LeadService;
import com.spheresuite.erp.domainobject.ScheduleDO;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;

@Component
public class ScheduleUtil {
	
	private ScheduleUtil() {}
	
	@Autowired
	private EmployeeService employeeServiceTemp;
	
	@Autowired
	private LeadService leadServiceTemp;
	
	private static EmployeeService employeeService;
	
	private static LeadService leadService;
	
	@PostConstruct
	public void init() {
		employeeService = this.employeeServiceTemp;
		leadService = this.leadServiceTemp;
	}
	
	public static JSONObject getScheduleList(List<ScheduleDO> scheduleList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (ScheduleDO schedule : scheduleList) {
				resultJSONArray.put(getScheduleDetailObject(schedule));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	@SuppressWarnings("deprecation")
	public static JSONObject getScheduleDetailObject(ScheduleDO schedule)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(schedule.getScheduleId() != null ? schedule.getScheduleId() : ""));
		result.put(CommonConstants.DESC, String.valueOf(schedule.getSubject() != null ? schedule.getSubject() : ""));
		result.put(CommonConstants.CONTACTLIST, String.valueOf(schedule.getContactList() != null ? schedule.getContactList() : ""));
		result.put(CommonConstants.TIME, String.valueOf(schedule.getLogDate() != null ? (schedule.getLogDate().getHours() + " : " + schedule.getLogDate().getMinutes()) : ""));
		result.put(CommonConstants.DATE, String.valueOf(schedule.getLogDate() != null ? CommonUtil.convertDateToStringWithtime(schedule.getLogDate()) : ""));
		result.put(CommonConstants.DATEDISPLAY, String.valueOf(schedule.getLogDate() != null ? schedule.getLogDate().toString().substring(0, 10).trim() : ""));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(schedule.getUpdatedon())));
		/*if(schedule.getUpdatedBy() != null){
			List<UserDO> userList = userService.retriveById(Long.parseLong(schedule.getUpdatedBy()));
			if(userList != null && userList.size() > 0){
				result.put(CommonConstants.UPDATED_BY, String.valueOf(userList.get(0).getName())); 
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
			List<EmployeeDO> empList = employeeService.retriveByEmpId(Long.parseLong(schedule.getUpdatedBy()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}*/
		
		
		if(schedule.getUpdatedBy() != null){
			String empName = CommonUtil.getEmpObject(schedule.getUpdatedBy());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.LEAD_ID, String.valueOf(schedule.getLead() != null ? schedule.getLead().getLeadId() : ""));
		result.put(CommonConstants.BODY, String.valueOf(schedule.getDescription() != null ? schedule.getDescription() : ""));
		result.put(CommonConstants.SUBJECT, String.valueOf(schedule.getSubject() != null ? schedule.getSubject() : ""));
		if(schedule.getLead() != null){
			result.put(CommonConstants.LEAD_NAME, String.valueOf(schedule.getLead().getName() != null ? schedule.getLead().getName() : ""));
		}else{
			result.put(CommonConstants.LEAD_NAME,"");
		}
		return result;
	}
}
