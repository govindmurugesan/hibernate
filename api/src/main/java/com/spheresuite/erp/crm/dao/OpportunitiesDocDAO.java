package com.spheresuite.erp.crm.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.OpportunitiesDocDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class OpportunitiesDocDAO {
	static Logger logger = Logger.getLogger(OpportunitiesDocDAO.class.getName());

	@Autowired
	private GenericDAOImpl<OpportunitiesDocDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public boolean delete(Long id) {
		boolean deleteStatus = true;
		try {
			this.sessionFactory.getCurrentSession().createQuery("delete from OpportunitiesDocDO e where e.opportunity.opportunityId=:id")
										.setParameter(CommonConstants.ID, id)
										.executeUpdate();
		} catch (Exception eException) {
			deleteStatus = false;
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return deleteStatus;
	}
	
	
	public OpportunitiesDocDO persist(OpportunitiesDocDO opportunitiesDocDO) {
		try {
			if (opportunitiesDocDO != null) {
				genericObject.persist(opportunitiesDocDO);
			}
		} catch (Exception eException) {
			//throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			//SessionManager.closeEntityManager(em);
		}
		return opportunitiesDocDO;
	}
	
	
	public List<OpportunitiesDocDO> retrieve() {
		List<OpportunitiesDocDO> OpportunitiesDocList = null;
		try {
			OpportunitiesDocList = genericObject.retrieve(OpportunitiesDocDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return OpportunitiesDocList;
	}
	
	@SuppressWarnings("unchecked")
	public List<OpportunitiesDocDO> retrieveByOppId(Long id) {
		List<OpportunitiesDocDO> OpportunitiesDocList = null;
		try {
			OpportunitiesDocList = this.sessionFactory.getCurrentSession().getNamedQuery(OpportunitiesDocDO.FIND_BY_OPP_ID)
										.setParameter(CommonConstants.ID, id)
										.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return OpportunitiesDocList;
	}
	
	public OpportunitiesDocDO update(OpportunitiesDocDO opportunitiesDocDO) {
		try {
			genericObject.merge(opportunitiesDocDO);
		} catch (Exception eException) {
		} finally {
		}
		return opportunitiesDocDO;
	}

}
