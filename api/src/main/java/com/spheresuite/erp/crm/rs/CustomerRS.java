package com.spheresuite.erp.crm.rs;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.crm.service.CustomerService;
import com.spheresuite.erp.crm.service.IndustryService;
import com.spheresuite.erp.crm.service.LeadService;
import com.spheresuite.erp.crm.web.util.CustomerUtil;
import com.spheresuite.erp.crm.web.util.LeadUtil;
import com.spheresuite.erp.domainobject.CustomerDO;
import com.spheresuite.erp.domainobject.IndustryDO;
import com.spheresuite.erp.domainobject.LeadDO;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/customer")
public class CustomerRS {

	String validation = null;
	static Logger logger = Logger.getLogger(CustomerRS.class.getName());

	@Autowired
	private CustomerService customerService;
	
	@Autowired
	private LeadService leadService;
	
	@Autowired
	private IndustryService industryService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				CustomerDO customerDO = new CustomerDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		customerDO.setCustomerId(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.ID).toString() : ""));
			 		customerDO.setName(inputJSON != null ? inputJSON.get(CommonConstants.NAME).toString() : "");
			 		customerDO.setStatus(inputJSON != null ? inputJSON.get(CommonConstants.STATUS).toString() : "");
			 		customerDO.setSourceId(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.SOURCE).toString() : ""));
			 		//customerDO.setIndustry(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.INDUSTRY).toString() : ""));
			 		if(inputJSON.get(CommonConstants.INDUSTRY) != null && !inputJSON.get(CommonConstants.INDUSTRY).toString().isEmpty()){
			 			List<IndustryDO> industryList = industryService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.INDUSTRY).toString()));
			 			if(industryList != null && industryList.size() > 0){
			 				customerDO.setIndustry(industryList.get(0));
			 			}
			 		}
			 		customerDO.setMobile(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.MOBILE).toString() : ""));
			 		customerDO.setPone(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.PHONE).toString() : ""));
			 		customerDO.setEmail(inputJSON != null ? inputJSON.get(CommonConstants.EMAIL).toString() : "");
			 		customerDO.setComment(inputJSON != null ? inputJSON.get(CommonConstants.COMMENT).toString() : "");
			 		customerDO.setAddress(inputJSON != null ? inputJSON.get(CommonConstants.ADDRESS).toString() : "");
			 		customerDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			customerDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			customerDO.setEmpId(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 	}
				customerService.persist(customerDO);
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Customer Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retrieveActive{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveById(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<CustomerDO> customerList = customerService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = CustomerUtil.getCustomerList(customerList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<CustomerDO> categoryList = customerService.retrieve();
				respJSON = CustomerUtil.getCustomerList(categoryList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveByActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveByActive(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<CustomerDO> categoryList = customerService.retrieve();
				respJSON = CustomerUtil.getCustomerList(categoryList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				CustomerDO customerDO = new CustomerDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<CustomerDO> customerList = customerService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		customerDO = customerList.get(0);
			 		customerDO.setCustomerId(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.ID).toString() : ""));
			 		customerDO.setName(inputJSON != null ? inputJSON.get(CommonConstants.NAME).toString() : "");
			 		customerDO.setStatus(inputJSON != null ? inputJSON.get(CommonConstants.STATUS).toString() : "");
			 		customerDO.setSourceId(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.SOURCE).toString() : ""));
			 		//customerDO.setIndustryId(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.INDUSTRY).toString() : ""));
			 		if(inputJSON.get(CommonConstants.INDUSTRY) != null && !inputJSON.get(CommonConstants.INDUSTRY).toString().isEmpty()){
			 			List<IndustryDO> industryList = industryService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.INDUSTRY).toString()));
			 			if(industryList != null && industryList.size() > 0){
			 				customerDO.setIndustry(industryList.get(0));
			 			}
			 		}
			 		customerDO.setMobile(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.MOBILE).toString() : ""));
			 		customerDO.setPone(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.PHONE).toString() : ""));
			 		customerDO.setEmail(inputJSON != null ? inputJSON.get(CommonConstants.EMAIL).toString() : "");
			 		customerDO.setComment(inputJSON != null ? inputJSON.get(CommonConstants.COMMENT).toString() : "");
			 		customerDO.setAddress(inputJSON != null ? inputJSON.get(CommonConstants.ADDRESS).toString() : "");
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			customerDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		customerDO.setUpdatedon(new Date());
			 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Customer Updated");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@SuppressWarnings("deprecation")
	@RequestMapping(value = "/retrieveBetweenDate/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String retrieveBetweenDate(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.FROMDATE).toString().isEmpty() &&
						!inputJSON.get(CommonConstants.TODATE).toString().isEmpty()){
					Date frmDate = new Date(inputJSON.get(CommonConstants.FROMDATE).toString()); 
					Date endDate = new Date(inputJSON.get(CommonConstants.TODATE).toString());  
				    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");  
				    String fromDate = formatter.format(frmDate); 
				    String toDate = formatter.format(endDate);
					fromDate = fromDate + " 00:00:00";
					toDate = toDate + " 23:59:59";
					Date formattedDate = CommonUtil.convertStringToDateTimeFmt(fromDate);
					Date formattedDate1 = CommonUtil.convertStringToDateTimeFmt(toDate);
					List<LeadDO> newcustomerList = leadService.retrieveCustomerBetweenDate(formattedDate,formattedDate1);
					List<LeadDO> customerList = leadService.retrieveAll("C");
					respJSON = LeadUtil.getCustomerListForChart(customerList,newcustomerList).toString();
				}else{
					List<LeadDO> newcustomerList = new ArrayList<LeadDO>();
					List<LeadDO> customerList = leadService.retrieveAll("C");
					respJSON = LeadUtil.getCustomerListForChart(customerList,newcustomerList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@SuppressWarnings("deprecation")
	@RequestMapping(value = "/retrieveBetweenDateByEmp/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String retrieveBetweenDateByEmp(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.FROMDATE).toString().isEmpty() &&
						!inputJSON.get(CommonConstants.TODATE).toString().isEmpty()){
					/*String fromDate = inputJSON.get(CommonConstants.FROMDATE).toString();
					String toDate = inputJSON.get(CommonConstants.TODATE).toString();*/
					Date frmDate = new Date(inputJSON.get(CommonConstants.FROMDATE).toString()); 
					Date endDate = new Date(inputJSON.get(CommonConstants.TODATE).toString());  
				    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");  
				    String fromDate = formatter.format(frmDate); 
				    String toDate = formatter.format(endDate);
					String empId = "";
					if(inputJSON.get(CommonConstants.EMPID) != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
						empId = inputJSON.get(CommonConstants.EMPID).toString();
					}
					fromDate = fromDate + " 00:00:00";
					toDate = toDate + " 23:59:59";
					Date formattedDate = CommonUtil.convertStringToDateTimeFmt(fromDate);
					Date formattedDate1 = CommonUtil.convertStringToDateTimeFmt(toDate);
					if(!empId.isEmpty() && empId.length() > 0){
						List<LeadDO> newcustomerList = leadService.retrieveCustomerBetweenDateByEmp(formattedDate,formattedDate1, empId);
						List<LeadDO> customerList = leadService.retrieveByEmp("C",empId);
						respJSON = LeadUtil.getCustomerListForChart(customerList,newcustomerList).toString();
					}
				}else if(inputJSON.get(CommonConstants.EMPID) != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
					List<LeadDO> newcustomerList = new ArrayList<LeadDO>();
					List<LeadDO> customerList = leadService.retrieveByEmp("C",inputJSON.get(CommonConstants.EMPID).toString());
					respJSON = LeadUtil.getCustomerListForChart(customerList,newcustomerList).toString();
				}else{
					List<LeadDO> newcustomerList = new ArrayList<LeadDO>();
					List<LeadDO> customerList = leadService.retrieveAll("C");
					respJSON = LeadUtil.getCustomerListForChart(customerList,newcustomerList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
}
