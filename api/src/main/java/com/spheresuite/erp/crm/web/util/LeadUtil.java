package com.spheresuite.erp.crm.web.util;

import java.util.List;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.crm.service.LeadTypeService;
import com.spheresuite.erp.crm.service.NotesService;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.LeadDO;
import com.spheresuite.erp.domainobject.LeadTypeDO;
import com.spheresuite.erp.domainobject.NotesDO;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
@Component
public class LeadUtil {
	
	private LeadUtil() {}
	@Autowired
	private EmployeeService temployeeService;
	private static EmployeeService employeeService;
	
	@Autowired
	private LeadTypeService tleadTypeService;
	private static LeadTypeService leadTypeService;
	
	@Autowired
	private NotesService tnotesService;
	private static NotesService notesService;
	
	@PostConstruct
	public void init() {
		employeeService = temployeeService;
		leadTypeService = tleadTypeService;
		notesService = tnotesService;
	}
	
	public static JSONObject getLeadList(List<LeadDO> leadList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (LeadDO lead : leadList) {
				resultJSONArray.put(getLeadDetailObject(lead));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getLeadList(Long totalCustomer) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			JSONObject result = new JSONObject();
			result.put(CommonConstants.CUSTOMERLIST, String.valueOf(totalCustomer));
			resultJSONArray.put(result);
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getLeadListWithOutPic(List<LeadDO> leadList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (LeadDO lead : leadList) {
				resultJSONArray.put(getLeadDetailObjectWithOutPic(lead));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getLeadLimitData(List<LeadDO> leadList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (LeadDO lead : leadList) {
				resultJSONArray.put(getLeadLimitDataDetailObject(lead));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getLeadListForChart(List<LeadDO> leadList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			int hot=0, warm=0, cold=0;
			for (LeadDO leadDO : leadList) {
				if(leadDO.getLeadType() != null){
					List<LeadTypeDO> leadTypeList = leadTypeService.retrieveById(leadDO.getLeadType().getLeadTypeId());
					if(leadTypeList != null && leadTypeList.size() > 0){
						if(leadTypeList.get(0).getName().equalsIgnoreCase("Hot")){
							hot = hot+1;
						}else if(leadTypeList.get(0).getName().equalsIgnoreCase("Warm")){
							warm = warm+1;
						}else if(leadTypeList.get(0).getName().equalsIgnoreCase("Cold")){
							cold = cold+1;
						}
					}
				}
			}
			
			JSONArray resultJSONArray = new JSONArray();
			resultJSONArray.put(getLeadTypeDetails(hot, cold, warm));			
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getLeadTypeDetails(int hot, int cold, int warm)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.HOT, String.valueOf(hot));
		result.put(CommonConstants.WARM, String.valueOf(warm));
		result.put(CommonConstants.COLD, String.valueOf(cold));
		return result;
	}
	
	public static JSONObject getLeadDetailObjectWithOutPic(LeadDO lead)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(lead.getLeadId()));
		result.put(CommonConstants.NAME, String.valueOf(lead.getName()));
		if(lead.getStatus() !=null){
			result.put(CommonConstants.STATUS, String.valueOf(lead.getStatus().getLeadStatusId()));
		}else{
			result.put(CommonConstants.STATUS, "");
		}
		
		if(lead.getSource() !=null){
			result.put(CommonConstants.SOURCE, String.valueOf(lead.getSource()));
		}else{
			result.put(CommonConstants.SOURCE, "");
		}
		if(lead.getIndustry() !=null){
			result.put(CommonConstants.INDUSTRY, String.valueOf(lead.getIndustry().getIndustryId()));
		}else{
			result.put(CommonConstants.INDUSTRY, "");
		}
		
		if(lead.getMobile() !=null){
			result.put(CommonConstants.MOBILE, String.valueOf(lead.getMobile()));
		}else{
			result.put(CommonConstants.MOBILE, "");
		}
		if(lead.getPhone() !=null){
			result.put(CommonConstants.PHONE, String.valueOf(lead.getPhone()));
		}else{
			result.put(CommonConstants.PHONE, "");
		}
		if(lead.getEmail() !=null){
			result.put(CommonConstants.EMAIL, String.valueOf(lead.getEmail()));
		}else{
			result.put(CommonConstants.EMAIL, "");
		}
		if(lead.getAddress() !=null){
			result.put(CommonConstants.ADDRESS, String.valueOf(lead.getAddress()));
		}else{
			result.put(CommonConstants.ADDRESS, "");
		}
		if(lead.getComment() !=null){
			result.put(CommonConstants.COMMENT, String.valueOf(lead.getComment()));
		}else{
			result.put(CommonConstants.COMMENT, "");
		}
		
		if(lead.getLeadType() !=null){
			result.put(CommonConstants.LEADTYPE, String.valueOf(lead.getLeadType().getLeadTypeId()));
		}else{
			result.put(CommonConstants.LEADTYPE, "");
		}
		if(lead.getTransferfrom() != null){
			result.put(CommonConstants.TRANSFER_FORM, String.valueOf(lead.getTransferfrom()));
		}else{
			result.put(CommonConstants.TRANSFER_FORM, "");
		}
		
		if(lead.getTransferFromDate() != null){
			result.put(CommonConstants.FROMDATE, String.valueOf(CommonUtil.convertDateToYearWithOutTime(lead.getTransferFromDate())));
		}else{
			result.put(CommonConstants.FROMDATE, "");
		}
		
		if(lead.getTransferToDate() != null){
			result.put(CommonConstants.TODATE, String.valueOf(CommonUtil.convertDateToYearWithOutTime(lead.getTransferToDate())));
		}else{
			result.put(CommonConstants.TODATE, "");
		}
		
		if(lead.getTransferType() != null){
			result.put(CommonConstants.TRANSFER_TYPE, String.valueOf(lead.getTransferType()));
		}else{
			result.put(CommonConstants.TRANSFER_TYPE, "");
		}
		
		if(lead.getCity() != null){
			result.put(CommonConstants.CITY, String.valueOf(lead.getCity()));
		}else{
			result.put(CommonConstants.CITY, "");
		}
		
		if(lead.getFollowupdate() != null){
			result.put(CommonConstants.FOLLOW_UP_DATE, String.valueOf(CommonUtil.convertDateToStringWithtime(lead.getFollowupdate())));
		}else{
			result.put(CommonConstants.FOLLOW_UP_DATE, "");
		}
		
		if(lead.getLeaddate() != null){
			result.put(CommonConstants.LEAD_DATE, String.valueOf(CommonUtil.convertDateToYearWithOutTime(lead.getLeaddate())));
		}else{
			result.put(CommonConstants.LEAD_DATE, "");
		}
		
		if(lead.getState() !=null){
			result.put(CommonConstants.STATE, String.valueOf(lead.getState().getStateId()));
			result.put(CommonConstants.STATE_NAME, String.valueOf(lead.getState().getStateName()));
		}else{
			result.put(CommonConstants.STATE, "");
			result.put(CommonConstants.STATE_NAME, "");
		}
		
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(lead.getUpdatedon())));
		if(lead.getUpdatedby() != null){
			List<EmployeeDO> empList = employeeService.retrieveEmpId(lead.getUpdatedby().toString());
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		
		if(lead.getLeadType() !=null){
				result.put(CommonConstants.LEAD_TYPE_NAME, String.valueOf(lead.getLeadType().getName()));
		}else{
			result.put(CommonConstants.LEAD_TYPE_NAME, "");
		}
			
		if(lead.getIndustry() !=null){
			result.put(CommonConstants.INDUSTRY_NAME, String.valueOf(lead.getIndustry().getName()));
		}else{
			result.put(CommonConstants.INDUSTRY_NAME, "");
		}
		
		if(lead.getStatus() !=null){
			result.put(CommonConstants.STATUS_NAME, String.valueOf(lead.getStatus().getName()));
		}else{
			result.put(CommonConstants.STATUS_NAME, "");
		}
		List<NotesDO> notesList = notesService.retrieveByLeadId(lead.getLeadId());
		if(notesList != null && notesList.size() >0){
			result.put(CommonConstants.NOTES,notesList.get(0).getNote());
		}else{
			result.put(CommonConstants.NOTES,"");
		}
		return result;
	}
	
	public static JSONObject getLeadDetailObject(LeadDO lead)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(lead.getLeadId()));
		result.put(CommonConstants.NAME, String.valueOf(lead.getName()));
		if(lead.getStatus() !=null){
			result.put(CommonConstants.STATUS, String.valueOf(lead.getStatus().getLeadStatusId()));
		}else{
			result.put(CommonConstants.STATUS, "");
		}
		
		if(lead.getSource() !=null){
			result.put(CommonConstants.SOURCE, String.valueOf(lead.getSource()));
		}else{
			result.put(CommonConstants.SOURCE, "");
		}
		if(lead.getIndustry() !=null){
			result.put(CommonConstants.INDUSTRY, String.valueOf(lead.getIndustry().getIndustryId()));
		}else{
			result.put(CommonConstants.INDUSTRY, "");
		}
		
		if(lead.getMobile() !=null){
			result.put(CommonConstants.MOBILE, String.valueOf(lead.getMobile()));
		}else{
			result.put(CommonConstants.MOBILE, "");
		}
		if(lead.getPhone() !=null){
			result.put(CommonConstants.PHONE, String.valueOf(lead.getPhone()));
		}else{
			result.put(CommonConstants.PHONE, "");
		}
		if(lead.getEmail() !=null){
			result.put(CommonConstants.EMAIL, String.valueOf(lead.getEmail()));
		}else{
			result.put(CommonConstants.EMAIL, "");
		}
		if(lead.getAddress() !=null){
			result.put(CommonConstants.ADDRESS, String.valueOf(lead.getAddress()));
		}else{
			result.put(CommonConstants.ADDRESS, "");
		}
		if(lead.getComment() !=null){
			result.put(CommonConstants.COMMENT, String.valueOf(lead.getComment()));
		}else{
			result.put(CommonConstants.COMMENT, "");
		}
		
		if(lead.getLeadType() !=null){
			result.put(CommonConstants.LEADTYPE, String.valueOf(lead.getLeadType().getLeadTypeId()));
		}else{
			result.put(CommonConstants.LEADTYPE, "");
		}
		if(lead.getTransferfrom() != null){
			result.put(CommonConstants.TRANSFER_FORM, String.valueOf(lead.getTransferfrom()));
		}else{
			result.put(CommonConstants.TRANSFER_FORM, "");
		}
		
		if(lead.getCity() != null){
			result.put(CommonConstants.CITY, String.valueOf(lead.getCity()));
		}else{
			result.put(CommonConstants.CITY, "");
		}
		
		if(lead.getFollowupdate() != null){
			result.put(CommonConstants.FOLLOW_UP_DATE, String.valueOf(CommonUtil.convertDateToStringWithtime(lead.getFollowupdate())));
		}else{
			result.put(CommonConstants.FOLLOW_UP_DATE, "");
		}
		
		if(lead.getLeaddate() != null){
			result.put(CommonConstants.LEAD_DATE, lead.getLeaddate());
		}else{
			result.put(CommonConstants.LEAD_DATE, "");
		}
		
		if(lead.getState() !=null){
			//List<StateDO> stateList = stateService.retrieveById(lead.getState().getStateId());
			result.put(CommonConstants.STATE, String.valueOf(lead.getState().getStateId()));
			result.put(CommonConstants.STATE_NAME, String.valueOf(lead.getState().getStateName()));
		}else{
			result.put(CommonConstants.STATE, "");
			result.put(CommonConstants.STATE_NAME, "");
		}
		
		if(lead.getTransferFromDate() != null){
			result.put(CommonConstants.FROMDATE, String.valueOf(CommonUtil.convertDateToYearWithOutTime(lead.getTransferFromDate())));
		}else{
			result.put(CommonConstants.FROMDATE, "");
		}
		
		if(lead.getTransferToDate() != null){
			result.put(CommonConstants.TODATE, String.valueOf(CommonUtil.convertDateToYearWithOutTime(lead.getTransferToDate())));
		}else{
			result.put(CommonConstants.TODATE, "");
		}
		
		if(lead.getTransferType() != null){
			result.put(CommonConstants.TRANSFER_TYPE, String.valueOf(lead.getTransferType()));
		}else{
			result.put(CommonConstants.TRANSFER_TYPE, "");
		}
		
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(lead.getUpdatedon())));
		if(lead.getUpdatedby() != null){
			List<EmployeeDO> empList = employeeService.retrieveEmpId(lead.getUpdatedby().toString());
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}		
		if(lead.getLeadType() !=null){
			result.put(CommonConstants.LEAD_TYPE_NAME, String.valueOf(lead.getLeadType().getName()));
		}else{
			result.put(CommonConstants.LEAD_TYPE_NAME, "");
		}
		if(lead.getIndustry() !=null){
			result.put(CommonConstants.INDUSTRY_NAME, String.valueOf(lead.getIndustry().getName()));
		}else{
			result.put(CommonConstants.INDUSTRY_NAME, "");
		}
		
		if(lead.getStatus() !=null){
			result.put(CommonConstants.STATUS_NAME, String.valueOf(lead.getStatus().getName()));
		}else{
			result.put(CommonConstants.STATUS_NAME, "");
		}
		
		if(lead.getPhoto() != null){
			result.put(CommonConstants.PHOTO,lead.getPhoto());
		}else{
			result.put(CommonConstants.PHOTO,"");
		}
		
		List<NotesDO> notesList = notesService.retrieveByLeadId(lead.getLeadId());
		if(notesList != null && notesList.size() >0){
			result.put(CommonConstants.NOTES,notesList.get(0).getNote());
		}else{
			result.put(CommonConstants.NOTES,"");
		}
		
		return result;
	}
	
	public static JSONObject getLeadLimitDataDetailObject(LeadDO lead)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(lead.getLeadId()));
		result.put(CommonConstants.NAME, String.valueOf(lead.getName()));
		result.put(CommonConstants.EMPID, String.valueOf(lead.getEmpId()));
		result.put(CommonConstants.TYPE, String.valueOf(lead.getType()));
		return result;
	}
	
	public static JSONObject getCustomerListForChart(List<LeadDO> customerList, List<LeadDO> newcustomerList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			int newCustomer = 0, totalCustomer = 0;
			totalCustomer = (customerList.size() - newcustomerList.size());
			newCustomer  = newcustomerList.size();
			JSONArray resultJSONArray = new JSONArray();
			JSONObject result = new JSONObject();
			result.put(CommonConstants.NEWCUSTOMER, String.valueOf(newCustomer));
			result.put(CommonConstants.CUSTOMERLIST, String.valueOf(totalCustomer));
			resultJSONArray.put(result);			
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
}
