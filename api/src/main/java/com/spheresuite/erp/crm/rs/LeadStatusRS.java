package com.spheresuite.erp.crm.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.crm.service.LeadStatusService;
import com.spheresuite.erp.crm.web.util.LeadStatusUtil;
import com.spheresuite.erp.domainobject.LeadStatusDO;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/leadstatus")
public class LeadStatusRS {

	String validation = null;
	static Logger logger = Logger.getLogger(LeadStatusRS.class.getName());

	@Autowired
	private LeadStatusService leadStatusService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				LeadStatusDO leadStatusDO = new LeadStatusDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
			 			leadStatusDO.setName(inputJSON.get(CommonConstants.NAME).toString());
			 		}
			 		leadStatusDO.setStatus(CommonConstants.ACTIVE);
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			leadStatusDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			leadStatusDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		leadStatusDO.setUpdatedon(new Date());
			 		leadStatusDO.setCreatedon(new Date());
			 	}
				if(!leadStatusService.persist(leadStatusDO)){
					return CommonWebUtil.buildErrorResponse("Lead Status Already Added").toString();
				}
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Lead Status Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<LeadStatusDO> leadStatusList = leadStatusService.retrieveActive();
				respJSON = LeadStatusUtil.getLeadStatusList(leadStatusList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<LeadStatusDO> leadStatusList = leadStatusService.retrieve();
				respJSON = LeadStatusUtil.getLeadStatusList(leadStatusList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				LeadStatusDO leadStatusDO = new LeadStatusDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<LeadStatusDO> leadStatusList = leadStatusService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(leadStatusList != null && leadStatusList.size() >0 ){
				 		leadStatusDO = leadStatusList.get(0);
				 		if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
				 			leadStatusDO.setName(inputJSON.get(CommonConstants.NAME).toString());
				 		}
				 		leadStatusDO.setUpdatedon(new Date());
				 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			leadStatusDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
				 			leadStatusDO.setStatus((char) inputJSON.get(CommonConstants.STATUS).toString().charAt(0));
				 		}
				 		if(!leadStatusService.update(leadStatusDO)){
				 			return CommonWebUtil.buildErrorResponse("Lead Status Already Added").toString();
				 		}
				 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Lead Status Updated");
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/importLeadStatus", method = RequestMethod.POST)
	public @ResponseBody String importLeadStatus(Model model, HttpServletRequest request) {
		try {
			JSONObject inputJSON =  CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
			JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
			List<LeadStatusDO> leadStatusList = new ArrayList<LeadStatusDO>();
			Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY);
			for (int i=0; i < fileData.size(); i++){
				JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
				LeadStatusDO leadStatusDO = new LeadStatusDO();
				
				if (colName.get(CommonConstants.NAME) != null && !colName.get(CommonConstants.NAME).toString().isEmpty()){
					leadStatusDO.setName(rowJSON.get(colName.get(CommonConstants.NAME)).toString());
		 		}
		 		if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
		 			leadStatusDO.setUpdatedby(updatedBy.toString());
		 			leadStatusDO.setCreatedby(updatedBy.toString());
		 		}
		 		leadStatusDO.setUpdatedon(new Date());
		 		leadStatusDO.setCreatedon(new Date());
		 		leadStatusDO.setStatus(CommonConstants.ACTIVE);
		 		leadStatusList.add(leadStatusDO);
			}
			leadStatusService.persistList(leadStatusList);
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
