package com.spheresuite.erp.crm.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.ProposalDetailsDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class ProposalDetailsDAO {
	static Logger logger = Logger.getLogger(ProposalDetailsDAO.class.getName());

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private GenericDAOImpl<ProposalDetailsDO> genericObject;
	
	public List<ProposalDetailsDO> persist(List<ProposalDetailsDO> proposalDetailsList) {
		try {
			if(proposalDetailsList != null && proposalDetailsList.size() > 0){
				this.sessionFactory.getCurrentSession().createQuery("delete from ProposalDetailsDO e where e.proposalId =:proposalId")
								.setParameter(CommonConstants.PROPOSAL_ID, proposalDetailsList.get(0).getProposalId())
								.executeUpdate();
			}
			for (ProposalDetailsDO proposalDetailsDO : proposalDetailsList) {
				genericObject.persist(proposalDetailsDO);
			}
		} catch (Exception eException) {
			//throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return proposalDetailsList;
	}
	
	public boolean delete(Long proposalId) {
		try {
			this.sessionFactory.getCurrentSession().createQuery("delete from ProposalDetailsDO e where e.proposalId =:proposalId")
							.setParameter(CommonConstants.PROPOSAL_ID, proposalId)
							.executeUpdate();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return true;
	}
	
	@SuppressWarnings("unchecked")
	public List<ProposalDetailsDO> retrieveById(Long proposalId) {
		List<ProposalDetailsDO> proposalList = null;
		try {
			proposalList = this.sessionFactory.getCurrentSession().getNamedQuery(ProposalDetailsDO.FIND_BY_PROPOSAL_ID)
										.setParameter(CommonConstants.PROPOSAL_ID, proposalId)
										.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return proposalList;
	}
}
