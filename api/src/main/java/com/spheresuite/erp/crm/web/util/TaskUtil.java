package com.spheresuite.erp.crm.web.util;

import java.util.List;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.crm.service.LeadService;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.TaskDO;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;

@Component
public class TaskUtil {
	
	private TaskUtil() {}
	
	@Autowired
	private EmployeeService employeeServiceTemp;
	
	@Autowired
	private LeadService leadServiceTemp;
	
	private static EmployeeService employeeService;
	
	private static LeadService leadService;
	
	@PostConstruct
	public void init() {
		employeeService = this.employeeServiceTemp;
		leadService = this.leadServiceTemp;
	}
	
	public static JSONObject getTaskList(List<TaskDO> taskList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (TaskDO task : taskList) {
				resultJSONArray.put(getCategoryDetailObject(task));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getCategoryDetailObject(TaskDO task)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(task.getTaskId() != null ? task.getTaskId() : ""));
		result.put(CommonConstants.TASKDETAIL, String.valueOf(task.getTaskDetails() != null ? task.getTaskDetails() : ""));
		result.put(CommonConstants.DATE, String.valueOf(task.getTaskDate() != null ? CommonUtil.convertDateToStringWithtime(task.getTaskDate()) : ""));
		result.put(CommonConstants.TIME, String.valueOf(task.getTaskTime() != null ? task.getTaskTime() : ""));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(task.getUpdatedon())));
		result.put(CommonConstants.STATUS, String.valueOf(task.getStatus()));
		if(task.getUpdatedBy() != null){
			/*List<UserDO> userList = userService.retriveById(Long.parseLong(task.getUpdatedBy()));
			if(userList != null && userList.size() > 0){
				result.put(CommonConstants.UPDATED_BY, String.valueOf(userList.get(0).getName())); 
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}*/
			
			List<EmployeeDO> empList = employeeService.retrieveEmpId(task.getUpdatedBy().toString());
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
			
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.LEAD_ID, String.valueOf(task.getLead() != null ? task.getLead().getLeadId() : ""));
		
		if(task.getLead() != null){
			result.put(CommonConstants.LEAD_NAME, String.valueOf(task.getLead().getName()));
		}else{
			result.put(CommonConstants.LEAD_NAME,"");
		}
		return result;
	}
}
