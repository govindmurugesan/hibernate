package com.spheresuite.erp.crm.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.crm.dao.CustomerAccessDAO;
import com.spheresuite.erp.domainobject.CustomerAccessDO;

@Service
@Transactional
public class CustomerAccessService {
	static Logger logger = Logger.getLogger(CustomerAccessService.class.getName());
	
	@Autowired
	private CustomerAccessDAO customerAccessDAO;
	
	@Transactional
	public Boolean persist(CustomerAccessDO customerAccessDO) {
		return customerAccessDAO.persist(customerAccessDO);
	}

	@Transactional
	public List<CustomerAccessDO> retrieveActive() {
		return customerAccessDAO.retrieveActive();
	}
	
	@Transactional
	public List<CustomerAccessDO> retrieveByEmail(String personal, String secondary) {
		return customerAccessDAO.retrieveByEmail(personal, secondary);
	}
	
	@Transactional
	public List<CustomerAccessDO> retriveById(Long id) {
		return customerAccessDAO.retrieveById(id);
	}
	
	@Transactional
	public List<CustomerAccessDO> retrieveByTempPassword(String tempPassword, String empId) {
		return customerAccessDAO.retrieveByTempPassword(tempPassword, empId);
	}
	
	@Transactional
	public List<CustomerAccessDO> retrieveByEmailId(String email) {
		return customerAccessDAO.retrieveByEmailId(email);
	}
	
	@Transactional
	public List<CustomerAccessDO> retrieve() {
		return customerAccessDAO.retrieve();
	}
	
	@Transactional
	public Boolean update(CustomerAccessDO customerAccessDO) {
		return customerAccessDAO.update(customerAccessDO);
	}
	
	@Transactional
	public Boolean updatePassword(CustomerAccessDO customerAccessDO) {
		return customerAccessDAO.updatePassword(customerAccessDO);
	}
	
	@Transactional
	public List<CustomerAccessDO> retrieveForLogin(String email, String pasword) {
		return customerAccessDAO.retrieveForLogin(email, pasword);
	}
	
	@Transactional
	public List<CustomerAccessDO> retriveByCustomerId(String id) {
		return customerAccessDAO.retriveByCustomerId(id);
	}
	
	@Transactional
	public List<CustomerAccessDO> getSuperAdmins() {
		return customerAccessDAO.getSuperAdmins();
	}
}
