package com.spheresuite.erp.crm.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.crm.dao.SalesPoliciesDAO;
import com.spheresuite.erp.domainobject.SalesPoliciesDO;

@Service
@Transactional
public class SalesPoliciesService {
	static Logger logger = Logger.getLogger(SalesPoliciesService.class.getName());
	
	@Autowired
	private SalesPoliciesDAO salesPoliciesDAO;
	
	@Transactional
	public SalesPoliciesDO persist(SalesPoliciesDO salesPoliciesDO) {
		return salesPoliciesDAO.persist(salesPoliciesDO);
	}

	@Transactional
	public List<SalesPoliciesDO> retrieveById(Long Id) {
		return salesPoliciesDAO.retrieveById(Id);
	}
	
	@Transactional
	public List<SalesPoliciesDO> retrieve(List<String> id) {
		return salesPoliciesDAO.retrieve(id);
	}
	
	@Transactional
	public List<SalesPoliciesDO> retrieveAll() {
		return salesPoliciesDAO.retrieveAll();
	}
	
	@Transactional
	public SalesPoliciesDO update(SalesPoliciesDO salesPoliciesDO) {
		return salesPoliciesDAO.update(salesPoliciesDO);
	}
}
