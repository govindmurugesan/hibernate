package com.spheresuite.erp.crm.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.MinutesOfMeetingDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class MinutesOfMeetingDAO {
	static Logger logger = Logger.getLogger(MinutesOfMeetingDAO.class.getName());
	@Autowired
	private GenericDAOImpl<MinutesOfMeetingDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public MinutesOfMeetingDO persist(MinutesOfMeetingDO momDO) {
		try {
			genericObject.persist(momDO);
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return momDO;
	}
	
	@SuppressWarnings("unchecked")
	public List<MinutesOfMeetingDO> retrieve() {
		List<MinutesOfMeetingDO> momList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(MinutesOfMeetingDO.FIND_ALL)
					.list(); 
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return momList;
	}
	
	@SuppressWarnings("unchecked")
	public List<MinutesOfMeetingDO> retrieveById(Long id) {
		List<MinutesOfMeetingDO> momList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(MinutesOfMeetingDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list(); 
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return momList;
	}
	
	@SuppressWarnings("unchecked")
	public List<MinutesOfMeetingDO> retrieveByCustomerId(Long id) {
		List<MinutesOfMeetingDO> momList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(MinutesOfMeetingDO.FIND_BY_LEADID)
					.setParameter(CommonConstants.ID, id)
					.list(); 
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return momList;
	}
	
	@SuppressWarnings("unchecked")
	public List<MinutesOfMeetingDO> retrieveByEvent(Long id) {
		List<MinutesOfMeetingDO> momList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(MinutesOfMeetingDO.FIND_BY_EVENT)
					.setParameter(CommonConstants.ID, id)
					.list(); 
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return momList;
	}
	
	@SuppressWarnings("unchecked")
	public List<MinutesOfMeetingDO> retrieveByEmpMom(Long eventId, List<String> Id)  {
		List<MinutesOfMeetingDO> momList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(MinutesOfMeetingDO.FIND_BY_EMP_MOM)
			.setParameter(CommonConstants.EVENTID, eventId)
			.setParameterList(CommonConstants.ID, Id)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return momList;
	}
	
	@SuppressWarnings("unchecked")
	public List<MinutesOfMeetingDO> retrieveByName(String name) {
		List<MinutesOfMeetingDO> momList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(MinutesOfMeetingDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, name)
					.list(); 
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return momList;
	}
	
	public MinutesOfMeetingDO update(MinutesOfMeetingDO momDO) {
		try {
			genericObject.merge(momDO);
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return momDO;
	}
	
	public boolean update(List<MinutesOfMeetingDO> momDO) {
		try {
			for (MinutesOfMeetingDO momDO2 : momDO) {
				genericObject.merge(momDO2);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}
	
	public boolean persistList(List<MinutesOfMeetingDO> momDO) {
		try {
			for (MinutesOfMeetingDO momDO2 : momDO) {
				genericObject.persist(momDO2);
			}
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
		}
		return true;
	}
}
