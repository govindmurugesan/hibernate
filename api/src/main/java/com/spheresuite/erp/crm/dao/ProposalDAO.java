package com.spheresuite.erp.crm.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.ProposalDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class ProposalDAO {
	static Logger logger = Logger.getLogger(ProposalDAO.class.getName());

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private GenericDAOImpl<ProposalDO> genericObject;
	
	public ProposalDO persist(ProposalDO proposalDO) {
		try {
			genericObject.persist(proposalDO);
		} catch (Exception eException) {
			//throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			//SessionManager.closeEntityManager(em);
		}
		return proposalDO;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<ProposalDO> retrieve(List<String> id) {
		List<ProposalDO> proposalList = null;
		try {
			proposalList = this.sessionFactory.getCurrentSession().getNamedQuery(ProposalDO.FIND_ALL)
										.setParameterList(CommonConstants.ID, id)
										.list();
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return proposalList;
	}
	
	@SuppressWarnings("unchecked")
	public List<ProposalDO> retrieveCustomerId(Long id) {
		List<ProposalDO> proposalList = null;
		try {
			proposalList = this.sessionFactory.getCurrentSession().getNamedQuery(ProposalDO.FIND_ALL_BY_CUSTOMERID)
										.setParameter(CommonConstants.ID, id)
										.list();
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return proposalList;
	}
	
	public List<ProposalDO> retrieveAll() {
		List<ProposalDO> proposalList = null;
		try {
			proposalList = genericObject.retrieve(ProposalDO.class);
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return proposalList;
	}
	
	@SuppressWarnings("unchecked")
	public List<ProposalDO> retrieveById(Long id) {
		List<ProposalDO> proposal = null;
		try {
			proposal =  this.sessionFactory.getCurrentSession().getNamedQuery(ProposalDO.FIND_BY_ID)
							.setParameter(CommonConstants.ID, id).list();
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return proposal;
	}
	
	public ProposalDO update(ProposalDO proposalDO) {
		try {
			genericObject.merge(proposalDO);
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return proposalDO;
	}

}
