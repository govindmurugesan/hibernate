package com.spheresuite.erp.crm.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.crm.dao.ProposalDefaultDAO;
import com.spheresuite.erp.domainobject.ProposalDefaultDO;

@Service
@Transactional
public class ProposalDefaultService {
	static Logger logger = Logger.getLogger(ProposalDefaultService.class.getName());
	
	@Autowired
	private ProposalDefaultDAO proposalDefaultDAO;
	
	@Transactional
	public List<ProposalDefaultDO> persist(List<ProposalDefaultDO> proposalDetailsList) {
		return proposalDefaultDAO.persist(proposalDetailsList);
	}
	
	@Transactional
	public boolean delete(Long proposalId) {
		return proposalDefaultDAO.delete(proposalId);
	}

	@Transactional
	public List<ProposalDefaultDO> retrieveById(Long proposalId) {
		return proposalDefaultDAO.retrieveById(proposalId);
	}
}
