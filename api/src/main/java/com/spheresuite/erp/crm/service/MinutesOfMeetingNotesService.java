package com.spheresuite.erp.crm.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.crm.dao.MinutesOfMeetingNotesDAO;
import com.spheresuite.erp.domainobject.MinutesOfMeetingNotesDO;
@Service
@Transactional
public class MinutesOfMeetingNotesService {
	static Logger logger = Logger.getLogger(MinutesOfMeetingNotesService.class.getName());
	@Autowired
	private MinutesOfMeetingNotesDAO momNotesDAO;

	@Transactional
	public List<MinutesOfMeetingNotesDO> persist(List<MinutesOfMeetingNotesDO> momList) {
		return momNotesDAO.persist(momList);
	}

	@Transactional
	public List<MinutesOfMeetingNotesDO> retrieveById(Long momId) {
		return momNotesDAO.retrieveById(momId);
	}

	@Transactional
	public List<MinutesOfMeetingNotesDO> retrieveForExport(Long momId, String pricing, String followups, String Samples) {
		return momNotesDAO.retrieveForExport(momId, pricing, followups, Samples) ;
	}
	
	
	
	@Transactional
	public boolean delete(Long momId) {
		return momNotesDAO.delete(momId);
	}
}
