package com.spheresuite.erp.crm.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.crm.dao.NotesDAO;
import com.spheresuite.erp.domainobject.NotesDO;
@Service
@Transactional
public class NotesService {
	static Logger logger = Logger.getLogger(NotesService.class.getName());
	@Autowired
	private NotesDAO notesDAO;
	
	@Transactional
	public NotesDO persist(NotesDO notesDO) {
		return notesDAO.persist(notesDO);
	}

	@Transactional
	public NotesDO update(NotesDO notesDO) {
		return notesDAO.update(notesDO);
	}

	@Transactional
	public List<NotesDO> retrieveById(Long Id) {
		return notesDAO.retrieveById(Id);
	}

	@Transactional
	public List<NotesDO> retrieveByLeadId(Long Id) {
		return notesDAO.retrieveByLeadId(Id);
	}

	@Transactional
	public List<NotesDO> retrieveByLeadIdAndNote(Long leadId,String note) {
		return notesDAO.retrieveByLeadIdAndNote(leadId,note);
	}
	
	@Transactional
	public List<NotesDO> retrieveByLeadRecentNote(Long Id) {
		return notesDAO.retrieveByLeadRecentNote(Id);
	}


	@Transactional
	public List<NotesDO> retrieve() {
		return notesDAO.retrieve();
	}
}
