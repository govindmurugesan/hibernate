package com.spheresuite.erp.crm.dao;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.LeadDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class LeadDAO {
	static Logger logger = Logger.getLogger(LeadDAO.class.getName());
	@Autowired
	private GenericDAOImpl<LeadDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public LeadDO persist(LeadDO LeadDO)  {
		try {
			genericObject.persist(LeadDO);
		} catch (Exception eException) {
		} finally {
		}
		return LeadDO;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<LeadDO> retrieve(String type, List<String> Id)  {
		List<LeadDO> leadList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeadDO.FIND_ALL)
			.setParameter(CommonConstants.TYPE, type)
			.setParameterList(CommonConstants.ID, Id)
			.setParameter(CommonConstants.STATUS, "n")
			.setMaxResults(1)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return leadList;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeadDO> retrieveLatestLead(String empId, String type)  {
		List<LeadDO> leadList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeadDO.FIND_ALL_LATEST_LEAD)
			.setParameter(CommonConstants.TYPE, type)
			.setParameter(CommonConstants.EMPID, empId)
			.setParameter(CommonConstants.STATUS, "n")
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return leadList;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeadDO> retrieveAll(String type)  {
		List<LeadDO> leadList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeadDO.FIND_ALL_DATE)
			.setParameter(CommonConstants.TYPE, type)
			.setParameter(CommonConstants.STATUS, "n")
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return leadList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Long> retrieveForCustomerIds()  {
		List<Long> projectList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeadDO.FIND_ALL_CUST_ID)
			.setParameter(CommonConstants.TYPE, "C")
			.setParameter(CommonConstants.STATUS, "n")
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return projectList;
	}
	
	public Long retrieveAllForChart(String type)  {
		Long leadList = null;
		try {
			
			return (Long) this.sessionFactory.getCurrentSession().getNamedQuery(LeadDO.FIND_SUM_CUSTOMER)
			.setParameter(CommonConstants.TYPE, type)
			.setParameter(CommonConstants.STATUS, "n")
			.uniqueResult();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return leadList;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeadDO> retrieveByEmp(String type, String empId)  {
		List<LeadDO> leadList = null;
		try {
			
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeadDO.FIND_BY_EMP)
			.setParameter(CommonConstants.TYPE, type)
			.setParameter(CommonConstants.EMPID, empId)
			.setParameter(CommonConstants.STATUS, "n")
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return leadList;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeadDO> retrieve(List<Long> Id)  {
		List<LeadDO> leadList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeadDO.FIND)
			.setParameter(CommonConstants.ID, Id)
			.setParameter(CommonConstants.STATUS, "n")
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return leadList;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeadDO> retrieveAll()  {
		List<LeadDO> leadList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeadDO.FIND_ALL_LEAD)
			.setParameter(CommonConstants.STATUS, "n")
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return leadList;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeadDO> retrieveById(Long Id)  {
		List<LeadDO> leadList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeadDO.FIND_BY_ID)
			.setParameter(CommonConstants.STATUS, "n")
			.setParameter(CommonConstants.ID, Id)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return leadList;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeadDO> retrieveBetweenDate(Date fromDate, Date toDate)  {
		List<LeadDO> leadList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeadDO.FIND_BY_DATE)
			.setParameter(CommonConstants.FROMDATE, fromDate)
			.setParameter(CommonConstants.TODATE, toDate)
			.setParameter(CommonConstants.STATUS, "n")
			.setParameter(CommonConstants.TYPE, CommonConstants.L)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return leadList;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeadDO> retrieveBetweenDateByEmp(Date fromDate, Date toDate, String empId)  {
		List<LeadDO> leadList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeadDO.FIND_BY_DATE_BY_EMP)
			.setParameter(CommonConstants.FROMDATE, fromDate)
			.setParameter(CommonConstants.TODATE, toDate)
			.setParameter(CommonConstants.TYPE, CommonConstants.L)
			.setParameter(CommonConstants.EMPID, Long.parseLong(empId))
			.setParameter(CommonConstants.STATUS, "n")
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return leadList;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeadDO> retrieveCustomerBetweenDateByEmp(Date fromDate, Date toDate, String empId)  {
		List<LeadDO> leadList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeadDO.FIND_BY_DATE_BY_EMP_FOR_CUS)
			.setParameter(CommonConstants.FROMDATE, fromDate)
			.setParameter(CommonConstants.TODATE, toDate)
			.setParameter(CommonConstants.TYPE, CommonConstants.C)
			.setParameter(CommonConstants.EMPID, Long.parseLong(empId))
			.setParameter(CommonConstants.STATUS, "n")
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return leadList;
	}
	
	public LeadDO update(LeadDO LeadDO)  {
		try {
			genericObject.merge(LeadDO);
		} catch (Exception eException) {
		} finally {
		}
		return LeadDO;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeadDO> retrieveCustomerBetweenDate(Date fromDate, Date toDate)  {
		List<LeadDO> leadList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeadDO.FIND_CUSTOMER_BY_DATE)
			.setParameter(CommonConstants.FROMDATE, fromDate)
			.setParameter(CommonConstants.TODATE, toDate)
			.setParameter(CommonConstants.TYPE, CommonConstants.C)
			.setParameter(CommonConstants.STATUS, "n")
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return leadList;
	}
	

	public boolean persistList(List<LeadDO> leadDO)  {
		try {
			for (LeadDO leadDO2 : leadDO) {
				genericObject.persist(leadDO2);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	public List<LeadDO> retrieveLeadByEmp(String empId, String type)  {
		List<LeadDO> leadList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeadDO.FIND_BY_EMP)
			.setParameter(CommonConstants.TYPE, type)
			.setParameter(CommonConstants.EMPID, empId)
			.setParameter(CommonConstants.STATUS, "n")
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return leadList;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeadDO> retrieveByLeadName(String name)  {
		List<LeadDO> leadList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeadDO.FIND_BY_LEAD_NAME)
			.setParameter(CommonConstants.NAME, name)
			.setParameter(CommonConstants.STATUS, "n")
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return leadList;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeadDO> retrieveLeadByPermenantEmp(String empId, String type)  {
		List<LeadDO> leadList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeadDO.FIND_BY_PERMANANT_EMP)
			.setParameter(CommonConstants.TYPE, type)
			.setParameter(CommonConstants.EMPID, empId)
			.setParameter(CommonConstants.STATUS, "n")
			.setParameter(CommonConstants.TRANSFER_TYPE, "T")
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return leadList;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeadDO> retrieveLeadByTransferType(String transferType, String type)  {
		List<LeadDO> leadList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeadDO.FIND_BY_TRANSFERTYPE)
			.setParameter(CommonConstants.TYPE, type)
			.setParameter(CommonConstants.STATUS, "n")
			.setParameter(CommonConstants.TRANSFER_TYPE, transferType)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return leadList;

	}
	
	@SuppressWarnings("unchecked")
	public List<LeadDO> retrieveLeadByAllTransferType(String type)  {
		List<LeadDO> leadList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeadDO.FIND_BY_ALL_TRANSFERTYPE)
			.setParameter(CommonConstants.TYPE, type)
			.setParameter(CommonConstants.STATUS, "n")
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return leadList;

	}
	
	@SuppressWarnings("unchecked")
	public List<LeadDO> retrieveTransferLeadById(String transferType, String type, String empID)  {
		List<LeadDO> leadList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeadDO.FIND_BY_TRANSFERTYPE_BY_ID)
			.setParameter(CommonConstants.TYPE, type)
			.setParameter(CommonConstants.STATUS, "n")
			.setParameter(CommonConstants.TRANSFER_TYPE, transferType)
			.setParameter(CommonConstants.EMPID, empID)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return leadList;

	}
	
	@SuppressWarnings("unchecked")
	public List<LeadDO> retrieveCurrentFollowupLeads(String empID)  {
		List<LeadDO> leadList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeadDO.FIND_BY_CURRENT_FOLLOWUP_LEADS)
			.setParameter(CommonConstants.STATUS, "n")
			.setParameter(CommonConstants.EMPID, empID)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return leadList;

	}
	
	@SuppressWarnings("unchecked")
	public List<LeadDO> retrieveFollowupLeadByDate(String empID, Date date)  {
		List<LeadDO> leadList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeadDO.FIND_BY_DATE_FOLLOWUP_LEADS)
					.setParameter(CommonConstants.DATE, date)
					.setParameter(CommonConstants.STATUS, "n")
					.setParameter(CommonConstants.EMPID, empID)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return leadList;

	}

	public boolean update(List<LeadDO> LeadList)  {
		try {
			for (LeadDO leadDO : LeadList) {
				genericObject.merge(leadDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}
}
