package com.spheresuite.erp.crm.rs;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.crm.service.LeadService;
import com.spheresuite.erp.crm.service.LogActivityService;
import com.spheresuite.erp.crm.web.util.LogActivityUtil;
import com.spheresuite.erp.domainobject.LeadDO;
import com.spheresuite.erp.domainobject.LogActivityDO;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/logactivity")
public class LogActivityRS {

	String validation = null;
	static Logger logger = Logger.getLogger(LogActivityRS.class.getName());

	@Autowired
	private LogActivityService logActivityService;
	
	@Autowired
	private LeadService leadService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				LogActivityDO logDO = new LogActivityDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.LEAD_ID) != null && !inputJSON.get(CommonConstants.LEAD_ID).toString().isEmpty()){
			 			List<LeadDO> leadList = leadService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.LEAD_ID).toString()));
			 			if(leadList != null && leadList.size() > 0){
			 				logDO.setLead(leadList.get(0));
			 			}
			 		}
			 		/*if(!inputJSON.get(CommonConstants.LEAD_ID).toString().isEmpty()){
			 			logDO.setLeadId(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.LEAD_ID).toString() : ""));
			 		}*/
			 		logDO.setStatus(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty() ? inputJSON.get(CommonConstants.STATUS).toString() : "");
			 		logDO.setType(inputJSON.get(CommonConstants.TYPE) != null && !inputJSON.get(CommonConstants.TYPE).toString().isEmpty() ? inputJSON.get(CommonConstants.TYPE).toString() : "");
			 		logDO.setLogTime(inputJSON.get(CommonConstants.TIME) != null && !inputJSON.get(CommonConstants.TIME).toString().isEmpty() ? inputJSON.get(CommonConstants.TIME).toString() : "");
			 		logDO.setLogDate(inputJSON.get(CommonConstants.DATE) != null && !inputJSON.get(CommonConstants.DATE).toString().isEmpty() ? inputJSON.get(CommonConstants.DATE).toString() : "");
			 		logDO.setLogDetails(inputJSON.get(CommonConstants.BODY) != null && !inputJSON.get(CommonConstants.BODY).toString().isEmpty() ? inputJSON.get(CommonConstants.BODY).toString() : "");
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			logDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			logDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			logDO.setEmpId((inputJSON.get(CommonConstants.UPDATED_BY).toString()));
			 		}
			 		logDO.setCreatedon(new Date());
			 		logDO.setUpdatedon(new Date());
			 		
			 	}
				logActivityService.persist(logDO);
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Log Created For Lead");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieve/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieve(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
			 	if (inputJSON != null){
			 		if (inputJSON != null && !inputJSON.get(CommonConstants.TYPE).toString().isEmpty()){
			 			List<LogActivityDO> logList = logActivityService.retrieve(inputJSON.get(CommonConstants.TYPE).toString());
			 			respJSON = LogActivityUtil.getLogList(logList).toString();
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveByLeadId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByLeadid(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
			 	if (inputJSON != null){
			 		if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 			List<LogActivityDO> logList = logActivityService.retrieveByLeadId(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
						respJSON = LogActivityUtil.getLogList(logList).toString();
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				LogActivityDO logDO = new LogActivityDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		if(!inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 			List<LogActivityDO> logActivityList = logActivityService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 			if(logActivityList != null && logActivityList.size() > 0){
			 				logDO = logActivityList.get(0);
					 		logDO.setStatus(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty() ? inputJSON.get(CommonConstants.STATUS).toString() : "");
					 		logDO.setType(inputJSON.get(CommonConstants.TYPE) != null && !inputJSON.get(CommonConstants.TYPE).toString().isEmpty() ? inputJSON.get(CommonConstants.TYPE).toString() : "");
					 		logDO.setLogTime(inputJSON.get(CommonConstants.TIME) != null && !inputJSON.get(CommonConstants.TIME).toString().isEmpty() ? inputJSON.get(CommonConstants.TIME).toString() : "");
					 		logDO.setLogDate(inputJSON.get(CommonConstants.DATE) != null && !inputJSON.get(CommonConstants.DATE).toString().isEmpty() ? inputJSON.get(CommonConstants.DATE).toString() : "");
					 		logDO.setLogDetails(inputJSON.get(CommonConstants.BODY) != null && !inputJSON.get(CommonConstants.BODY).toString().isEmpty() ? inputJSON.get(CommonConstants.BODY).toString() : "");
					 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			logDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			logDO.setEmpId(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			logDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 		}
					 		logDO.setCreatedon(new Date());
					 		logDO.setUpdatedon(new Date());
					 		logActivityService.update(logDO);
					 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Log Updated For Lead");
			 			}
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
 		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
