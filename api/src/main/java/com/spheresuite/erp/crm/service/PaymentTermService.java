package com.spheresuite.erp.crm.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.crm.dao.PaymentTermDAO;
import com.spheresuite.erp.domainobject.PaymentTermDO;

@Service
@Transactional
public class PaymentTermService {
	static Logger logger = Logger.getLogger(PaymentTermService.class.getName());
	
	@Autowired
	private PaymentTermDAO paymentTermDAO;
	
	@Transactional
	public boolean persist(PaymentTermDO PaymentTermDO) {
		return paymentTermDAO.persist(PaymentTermDO);
	}

	@Transactional
	public List<PaymentTermDO> retrieveActive() {
		return paymentTermDAO.retrieveActive();
	}
	
	@Transactional
	public List<PaymentTermDO> retrieveById(Long Id) {
		return paymentTermDAO.retrieveById(Id);
	}
	
	@Transactional
	public List<PaymentTermDO> retrieve() {
		return paymentTermDAO.retrieve();
	}
	
	@Transactional
	public boolean update(PaymentTermDO PaymentTermDO) {
		return paymentTermDAO.update(PaymentTermDO);
	}
	
	@Transactional
	public boolean persistList(List<PaymentTermDO> paymentTermDO) {
		return paymentTermDAO.persistList(paymentTermDO);
	}
}
