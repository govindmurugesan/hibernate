package com.spheresuite.erp.crm.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.LeadStatusDO;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
@Component
public class LeadStatusUtil {
	
	private LeadStatusUtil() {}
	
	public static JSONObject getLeadStatusList(List<LeadStatusDO> leadStatusList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (LeadStatusDO leadStatus : leadStatusList) {
				resultJSONArray.put(getEmployeeTypeDetailObject(leadStatus));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getEmployeeTypeDetailObject(LeadStatusDO leadStatus)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(leadStatus.getLeadStatusId()));
		result.put(CommonConstants.NAME, String.valueOf(leadStatus.getName()));
		result.put(CommonConstants.STATUS, String.valueOf(leadStatus.getStatus()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(leadStatus.getUpdatedon())));
		if(leadStatus.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(leadStatus.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		return result;
	}
}
