package com.spheresuite.erp.crm.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.AssignmentDO;
import com.spheresuite.erp.domainobject.EmailSettingsDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class AssignmentDAO {
	static Logger logger = Logger.getLogger(AssignmentDAO.class.getName());
	@Autowired
	private GenericDAOImpl<AssignmentDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public AssignmentDO persist(AssignmentDO assignmentDO)  {
		try {
			genericObject.persist(assignmentDO);
		} catch (Exception eException) {
		} finally {
		}
		return assignmentDO;
	}
	
	
	public List<AssignmentDO> retrieve()  {
		List<AssignmentDO> assignmentList = null;
		try {
			assignmentList = genericObject.retrieve(AssignmentDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return assignmentList;
	}
	
	@SuppressWarnings("unchecked")
	public List<AssignmentDO> retrieveActive()  {
		List<AssignmentDO> assignmentList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EmailSettingsDO.FIND_BY_STATUS)
			.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
			.list(); 
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return assignmentList;
	}
	
	@SuppressWarnings("unchecked")
	public List<AssignmentDO> retrieveById(Long Id)  {
		List<AssignmentDO> assignmemtList = null;
		
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(AssignmentDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, Id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return assignmemtList;
	}
	
	@SuppressWarnings("unchecked")
	public List<AssignmentDO> retrieveByEmp(String Id)  {
		List<AssignmentDO> assignmemtList = null;
		try {
			
			return this.sessionFactory.getCurrentSession().getNamedQuery(AssignmentDO.FIND_BY_EMPID)
			.setParameter(CommonConstants.ID, Id)
			.setParameter(CommonConstants.STATUS, CommonConstants.ENGAGED)
			.list(); 
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return assignmemtList;
	}
	
	public AssignmentDO update(AssignmentDO assignmentDetails)  {
		try {
			genericObject.merge(assignmentDetails);
		} catch (Exception eException) {
		} finally {
		}
		return assignmentDetails;
	}

}
