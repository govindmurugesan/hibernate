package com.spheresuite.erp.crm.web.util;

import java.util.List;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.InvoiceTermsConfigurationDO;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
@Component
public class InvoiceTermsConfigurationUtil {
	
	private InvoiceTermsConfigurationUtil() {}
	@Autowired
	private EmployeeService temployeeService;
	
	private static EmployeeService employeeService;
	
	@PostConstruct
	public void init() {
		employeeService = temployeeService;
	}
	public static JSONObject getInvoiceTermsList(List<InvoiceTermsConfigurationDO> invoiceTermsList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (InvoiceTermsConfigurationDO invoiceterm : invoiceTermsList) {
				resultJSONArray.put(getInvoiceTermsDetailObject(invoiceterm));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getInvoiceTermsDetailObject(InvoiceTermsConfigurationDO invoiceterms)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(invoiceterms.getId()));
		result.put(CommonConstants.NAME, String.valueOf(invoiceterms.getName()));
		result.put(CommonConstants.STATUS, String.valueOf(invoiceterms.getStatus()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(invoiceterms.getUpdatedon())));
		if(invoiceterms.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(invoiceterms.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		
		
		
		/*if(invoiceterms.getUpdatedby() != null){
			List<EmployeeDO> empList = employeeService.retrieveEmpId(Long.parseLong(invoiceterms.getUpdatedby()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}*/
		return result;
	}
}
