package com.spheresuite.erp.crm.web.util;

import java.util.List;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.crm.service.ContactService;
import com.spheresuite.erp.crm.service.MinutesOfMeetingNotesService;
import com.spheresuite.erp.domainobject.ContactDO;
import com.spheresuite.erp.domainobject.MinutesOfMeetingDO;
import com.spheresuite.erp.domainobject.MinutesOfMeetingNotesDO;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;

@Component
public class MinutesOfMeetingsUtil {
	
	private MinutesOfMeetingsUtil() {}
	
	@Autowired
	private ContactService contactServiceTemp;
	
	private static ContactService contactService;
	
	@Autowired
	private MinutesOfMeetingNotesService momNotesServiceTemp;
	
	private static MinutesOfMeetingNotesService momNotesService;
	
	@PostConstruct
	public void init() {
		contactService = this.contactServiceTemp;
		momNotesService = this.momNotesServiceTemp;
	}
	
	public static JSONObject getMOMList(List<MinutesOfMeetingDO> momList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (MinutesOfMeetingDO mom : momList) {
				resultJSONArray.put(getMOMDetailObject(mom));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getMOMDetailObject(MinutesOfMeetingDO mom)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(mom.getMomId() != null ? mom.getMomId() : ""));
		result.put(CommonConstants.EMPID, String.valueOf(mom.getEmpId() != null ? mom.getEmpId() : ""));
		result.put(CommonConstants.COMPANY, String.valueOf(mom.getCompany()));
		result.put(CommonConstants.PARTICIPANTS, String.valueOf(mom.getParticipants() != null ? mom.getParticipants() : ""));
		result.put(CommonConstants.EVENT, String.valueOf(mom.getEvent() != null ? mom.getEvent() : ""));
		result.put(CommonConstants.TEAM, String.valueOf(mom.getTeam() != null ? mom.getTeam() : ""));
		result.put(CommonConstants.DATE, String.valueOf(mom.getMomDate() != null ? mom.getMomDate() : ""));
		result.put(CommonConstants.TIME, String.valueOf(mom.getMomTime() != null ? mom.getMomTime() : ""));
		if(mom.getLead() != null){
			result.put(CommonConstants.CUSTOMERNAME, String.valueOf(mom.getLead().getName()));
			result.put(CommonConstants.CUSTOMERID, String.valueOf(mom.getLead().getLeadId()));
			result.put(CommonConstants.TYPE, String.valueOf(mom.getLead().getType()));
		} else {
			result.put(CommonConstants.CUSTOMERNAME, "");
			result.put(CommonConstants.CUSTOMERID, "");
			result.put(CommonConstants.TYPE, "");
		}
		if(mom.getEvents() != null){
			result.put(CommonConstants.EVENTNAME, String.valueOf(mom.getEvents().getName()));
			result.put(CommonConstants.EVENTID, String.valueOf(mom.getEvents().getEventId()));
		} else {
			result.put(CommonConstants.EVENTNAME, "");
			result.put(CommonConstants.EVENTID, "");
		}
		JSONArray resultMoMNotes = new JSONArray();
		List<MinutesOfMeetingNotesDO> momNotesList = momNotesService.retrieveById(mom.getMomId());
		if(momNotesList != null && momNotesList.size() > 0){
			for (MinutesOfMeetingNotesDO invoiceTerm : momNotesList) {
				resultMoMNotes.put(getMinutesOfMeetingNotesObject(invoiceTerm));
			}
			result.put(CommonConstants.MOM_NOTES_LIST, resultMoMNotes);
		}else{
			result.put(CommonConstants.MOM_NOTES_LIST, "");
		}
		
		JSONArray resultMoMContacts = new JSONArray();
		List<ContactDO> contactsList = contactService.retriveByLeadId(mom.getLead().getLeadId());
		if(contactsList != null && contactsList.size() > 0){
			for (ContactDO contactDO : contactsList) {
				resultMoMContacts.put(getMoMContactDetailObject(contactDO));
			}
			result.put(CommonConstants.MOM_CONTACTS_LIST, resultMoMContacts);
		}else{
			result.put(CommonConstants.MOM_CONTACTS_LIST, "");
		}
		
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(mom.getUpdatedon())));
		if(mom.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(mom.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		return result;
	}
	
	public static JSONObject getMinutesOfMeetingNotesObject(MinutesOfMeetingNotesDO momNotes)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.DESCRIPTION, String.valueOf(momNotes.getDescription()));
		result.put(CommonConstants.SAMPLE, String.valueOf(momNotes.getSample()));
		result.put(CommonConstants.PRICING, String.valueOf(momNotes.getPricing()));
		result.put(CommonConstants.FOLLOW_UPS, String.valueOf(momNotes.getFollowups()));
		result.put(CommonConstants.INDEX, String.valueOf(momNotes.getIndexValue()));
		return result;
	}
	
	public static JSONObject getMoMContactDetailObject(ContactDO contact)throws JSONException {
		JSONObject result = new JSONObject();
		String name="";
		if(contact.getFirstname() !=null){
			name = name+" "+contact.getFirstname();
		}else{
			result.put(CommonConstants.CONTACT_NAME, name);
		}
		if(contact.getMiddlename() !=null){
			name = name+" "+contact.getMiddlename();
		}else{
			result.put(CommonConstants.CONTACT_NAME, name);
		}
		if(contact.getLastname() !=null){
			name = name+" "+contact.getLastname();
		}else{
			result.put(CommonConstants.CONTACT_NAME, name);
		}
		result.put(CommonConstants.CONTACT_NAME, name);
		
		if(contact.getPrimaryemail() !=null){
			result.put(CommonConstants.EMAIL, String.valueOf(contact.getPrimaryemail()));
		}else{
			result.put(CommonConstants.EMAIL, "");
		}
		
		if(contact.getMobile1() !=null){
			result.put(CommonConstants.MOBILE, String.valueOf(contact.getMobile1()));
		}else{
			result.put(CommonConstants.MOBILE, "");
		}
		result.put(CommonConstants.TYPE, "old");
		return result;
	}
	
	
	
	/*public static JSONObject getMOMListForReport(List<MinutesOfMeetingDO> momList, List<MinutesOfMeetingNotesDO> momNotesList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (MinutesOfMeetingDO mom : momList) {
				resultJSONArray.put(getMOMDetailObjectForReport(mom));
			}
			
			for (MinutesOfMeetingNotesDO momNotes : momNotesList) {
				resultJSONArray.put(getMinutesOfMeetingNotesObjectForReport(momNotes));
			}
			
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}*/
	
	public static JSONObject getMOMListForReport(JSONArray dataJSONArray) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			//JSONArray resultJSONArray = new JSONArray();
			/*for (MinutesOfMeetingDO mom : momList) {
				resultJSONArray.put(getMOMDetailObjectForReport(mom));
			}
			
			for (MinutesOfMeetingNotesDO momNotes : momNotesList) {
				resultJSONArray.put(getMinutesOfMeetingNotesObjectForReport(momNotes));
			}*/
			
			resultJSON.put(CommonConstants.RESULTS, dataJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	/*public static JSONObject getMOMDetailObjectForReport(MinutesOfMeetingDO mom)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(mom.getMomId() != null ? mom.getMomId() : ""));
	//	result.put(CommonConstants.EMPID, String.valueOf(mom.getEmpId() != null ? mom.getEmpId() : ""));
		result.put(CommonConstants.COMPANY, String.valueOf(mom.getCompany()));
		result.put(CommonConstants.PARTICIPANTS, String.valueOf(mom.getParticipants() != null ? mom.getParticipants() : ""));
		result.put(CommonConstants.EVENT, String.valueOf(mom.getEvent() != null ? mom.getEvent() : ""));
		result.put(CommonConstants.TEAM, String.valueOf(mom.getTeam() != null ? mom.getTeam() : ""));
		result.put(CommonConstants.DATE, String.valueOf(mom.getMomDate() != null ? mom.getMomDate() : ""));
		result.put(CommonConstants.TIME, String.valueOf(mom.getMomTime() != null ? mom.getMomTime() : ""));
		if(mom.getLead() != null){
			result.put(CommonConstants.CUSTOMERNAME, String.valueOf(mom.getLead().getName()));
			result.put(CommonConstants.CUSTOMERID, String.valueOf(mom.getLead().getLeadId()));
			result.put(CommonConstants.TYPE, String.valueOf(mom.getLead().getType()));
		} else {
			result.put(CommonConstants.CUSTOMERNAME, "");
			result.put(CommonConstants.CUSTOMERID, "");
			result.put(CommonConstants.TYPE, "");
		}
		if(mom.getEvents() != null){
			result.put(CommonConstants.EVENTNAME, String.valueOf(mom.getEvents().getName()));
			result.put(CommonConstants.EVENTID, String.valueOf(mom.getEvents().getEventId()));
		} else {
			result.put(CommonConstants.EVENTNAME, "");
			result.put(CommonConstants.EVENTID, "");
		}
		JSONArray resultMoMNotes = new JSONArray();
		List<MinutesOfMeetingNotesDO> momNotesList = momNotesService.retrieveById(mom.getMomId());
		if(momNotesList != null && momNotesList.size() > 0){
			for (MinutesOfMeetingNotesDO invoiceTerm : momNotesList) {
				resultMoMNotes.put(getMinutesOfMeetingNotesObject(invoiceTerm));
			}
			result.put(CommonConstants.MOM_NOTES_LIST, resultMoMNotes);
		}else{
			result.put(CommonConstants.MOM_NOTES_LIST, "");
		}
		
		return result;
	}
	
	public static JSONObject getMinutesOfMeetingNotesObjectForReport(MinutesOfMeetingNotesDO momNotes)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.DESCRIPTION, String.valueOf(momNotes.getDescription()));
		result.put(CommonConstants.SAMPLE, String.valueOf(momNotes.getSample()));
		result.put(CommonConstants.PRICING, String.valueOf(momNotes.getPricing()));
		result.put(CommonConstants.FOLLOW_UPS, String.valueOf(momNotes.getFollowups()));
		result.put(CommonConstants.INDEX, String.valueOf(momNotes.getIndexValue()));
		result.put(CommonConstants.NOTES, String.valueOf(momNotes.getDescription()));
		result.put(CommonConstants.COMPANY, String.valueOf(momNotes.getMom().getCompany()));
		result.put(CommonConstants.PARTICIPANTS, String.valueOf(momNotes.getMom().getParticipants() != null ? momNotes.getMom().getParticipants() : ""));
		result.put(CommonConstants.EVENT, String.valueOf(momNotes.getMom().getEvent() != null ? momNotes.getMom().getEvent() : ""));
		result.put(CommonConstants.TEAM, String.valueOf(momNotes.getMom().getTeam() != null ? momNotes.getMom().getTeam() : ""));
		result.put(CommonConstants.DATE, String.valueOf(momNotes.getMom().getMomDate() != null ? momNotes.getMom().getMomDate() : ""));
		result.put(CommonConstants.TIME, String.valueOf(momNotes.getMom().getMomTime() != null ? momNotes.getMom().getMomTime() : ""));
		if(momNotes.getMom().getLead() != null){
			result.put(CommonConstants.CUSTOMERNAME, String.valueOf(momNotes.getMom().getLead().getName()));
			result.put(CommonConstants.CUSTOMERID, String.valueOf(momNotes.getMom().getLead().getLeadId()));
			result.put(CommonConstants.TYPE, String.valueOf(momNotes.getMom().getLead().getType()));
		} else {
			result.put(CommonConstants.CUSTOMERNAME, "");
			result.put(CommonConstants.CUSTOMERID, "");
			result.put(CommonConstants.TYPE, "");
		}
		return result;
	}*/
}
