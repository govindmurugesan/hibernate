package com.spheresuite.erp.crm.web.util;

import java.util.List;
import java.util.Random;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.AssignedMenuDO;
import com.spheresuite.erp.domainobject.CustomerAccessDO;
import com.spheresuite.erp.service.AssignedMenuService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;

@Component
public class CustomerAccessUtil {
	
	private CustomerAccessUtil() {}
	
	@Autowired
	private AssignedMenuService assignedMenuServiceTemp;
	
	private static AssignedMenuService assignedMenuService;
	
	@PostConstruct
	public void init() {
		assignedMenuService = this.assignedMenuServiceTemp;
	}
	
	public static JSONObject getCustomerAccessList(List<CustomerAccessDO> customerAccessList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (CustomerAccessDO customerAccess : customerAccessList) {
				resultJSONArray.put(getCustomerAccessDetailObject(customerAccess));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getCustomerAccessDetailObject(CustomerAccessDO customerAccess)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(customerAccess.getCustomerAccessId() != null ? customerAccess.getCustomerAccessId() : ""));
		result.put(CommonConstants.EMPID, String.valueOf(customerAccess.getContact().getContactId() != null ? customerAccess.getContact().getContactId() : ""));
		result.put(CommonConstants.STATUS, String.valueOf(customerAccess.getStatus()));
		result.put(CommonConstants.EMAIL, String.valueOf(customerAccess.getContact().getPrimaryemail() != null ? customerAccess.getContact().getPrimaryemail() : ""));
		result.put(CommonConstants.ROLE_ID, String.valueOf(customerAccess.getRole().getRoleId() != null ? customerAccess.getRole().getRoleId() : ""));
	//	result.put(CommonConstants.PASSWORD, String.valueOf(customerAccess.getPassword() != null ? customerAccess.getPassword(): ""));
		result.put(CommonConstants.TEMP_PASSWORD, String.valueOf(customerAccess.getPassword() != null ? customerAccess.getPassword() : ""));
		result.put(CommonConstants.ISDELETED, String.valueOf(customerAccess.getIsDeleted() != null ? customerAccess.getIsDeleted() : ""));
		result.put(CommonConstants.ROLE_NAME, String.valueOf(customerAccess.getRole().getName() != null ? customerAccess.getRole().getName() : ""));
		result.put(CommonConstants.TYPE, "S");
		if(customerAccess.getContact() != null){
			String empName = customerAccess.getContact().getFirstname()+" "+ customerAccess.getContact().getMiddlename() + " "+ customerAccess.getContact().getLastname();
			result.put(CommonConstants.NAME, empName); 
		}else{
			result.put(CommonConstants.NAME, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(customerAccess.getUpdatedon())));
		if(customerAccess.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(customerAccess.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		return result;
	}
	
	public static JSONObject getCustomerForLogin(List<CustomerAccessDO> customerAccessList, String token) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (CustomerAccessDO customerAccess : customerAccessList) {
				resultJSONArray.put(getUserForLoginDetailObject(customerAccess));
			}
			resultJSON.put(CommonConstants.TOKEN, token);
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getUserForLoginDetailObject(CustomerAccessDO customerAccess)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(customerAccess.getContact().getContactId()));
		result.put(CommonConstants.EMAIL, String.valueOf(customerAccess.getContact().getPrimaryemail()));
		result.put(CommonConstants.TYPE, "S");
		if(customerAccess.getRole().getRoleId() != null){
			result.put(CommonConstants.ROLE_ID, String.valueOf(customerAccess.getRole().getRoleId()));
			result.put(CommonConstants.ROLE_NAME, String.valueOf(customerAccess.getRole().getName()));
		}
		if(customerAccess.getContact() != null){
			String empName = customerAccess.getContact().getFirstname()+" "+ customerAccess.getContact().getMiddlename() + " "+ customerAccess.getContact().getLastname();
			result.put(CommonConstants.NAME, empName); 
		}else{
			result.put(CommonConstants.NAME, ""); 
		}
		List<AssignedMenuDO> list = assignedMenuService.retrieveByRoleId(customerAccess.getRole().getRoleId());
		JSONArray resultJSONArray = new JSONArray();
		if(list.size() > 0){
			for (AssignedMenuDO l : list) {
				resultJSONArray.put(getMenuObject(l));
			}
		}
		result.put(CommonConstants.MENU, resultJSONArray);
		return result;
	}
	
	public static JSONObject getMenuObject(AssignedMenuDO menu)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.SUB_MENU, String.valueOf(menu.getMenu().getMenuId()));
		return result;
	}
	
	public static JSONObject generateUrl(String email, String tempPassword ) {
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.EMAIL, email);
			resultJSON.put(CommonConstants.TEMP_PASSWORD, tempPassword);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return resultJSON;
	}
	
	public static char[] geek_Password(int len)
    {
        // A strong password has Cap_chars, Lower_chars,
        // numeric value and symbols. So we are using all of
        // them to generate our password
        String Capital_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String Small_chars = "abcdefghijklmnopqrstuvwxyz";
        String numbers = "0123456789";
                String symbols = "!@#$%^&*_=+-/.?<>)";
 
 
        String values = Capital_chars + Small_chars +
                        numbers + symbols;
 
        // Using random method
        Random rndm_method = new Random();
 
        char[] password = new char[len];
 
        for (int i = 0; i < len; i++)
        {
            // Use of charAt() method : to get character value
            // Use of nextInt() as it is scanning the value as int
            password[i] =
              values.charAt(rndm_method.nextInt(values.length()));
 
        }
        return password;
    }
}
