package com.spheresuite.erp.crm.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.crm.service.ContactService;
import com.spheresuite.erp.crm.service.EventsService;
import com.spheresuite.erp.crm.service.LeadService;
import com.spheresuite.erp.crm.service.MinutesOfMeetingNotesService;
import com.spheresuite.erp.crm.service.MinutesOfMeetingService;
import com.spheresuite.erp.crm.web.util.MinutesOfMeetingsUtil;
import com.spheresuite.erp.domainobject.ContactDO;
import com.spheresuite.erp.domainobject.EventsDO;
import com.spheresuite.erp.domainobject.LeadDO;
import com.spheresuite.erp.domainobject.MinutesOfMeetingDO;
import com.spheresuite.erp.domainobject.MinutesOfMeetingNotesDO;
import com.spheresuite.erp.domainobject.RolesDO;
import com.spheresuite.erp.domainobject.UserDO;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.RolesService;
import com.spheresuite.erp.service.UserService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/mom")
public class MinutesOfMeetingRS {

	String validation = null;
	static Logger logger = Logger.getLogger(MinutesOfMeetingRS.class.getName());

	@Autowired
	private MinutesOfMeetingService momService;
	
	@Autowired
	private MinutesOfMeetingNotesService momNotesService;
	
	@Autowired
	private EmployeeService employeeService;
	
	@Autowired
	private LeadService leadService;
	
	@Autowired
	private ContactService contactService;
	
	@Autowired
	private EventsService eventsService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private RolesService rolesService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				MinutesOfMeetingDO momDO = new MinutesOfMeetingDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		momDO.setCompany(inputJSON.get(CommonConstants.COMPANY) != null && !inputJSON.get(CommonConstants.COMPANY).toString().isEmpty() ? inputJSON.get(CommonConstants.COMPANY).toString().toUpperCase() : "");
			 		momDO.setEvent(inputJSON.get(CommonConstants.EVENT) != null && !inputJSON.get(CommonConstants.EVENT).toString().isEmpty() ? inputJSON.get(CommonConstants.EVENT).toString() : "");
			 		momDO.setParticipants(inputJSON.get(CommonConstants.PARTICIPANTS) != null && !inputJSON.get(CommonConstants.PARTICIPANTS).toString().isEmpty() ? inputJSON.get(CommonConstants.PARTICIPANTS).toString() : "");
			 		momDO.setTeam(inputJSON.get(CommonConstants.TEAM) != null && !inputJSON.get(CommonConstants.TEAM).toString().isEmpty() ? inputJSON.get(CommonConstants.TEAM).toString() : "");
			 		if(inputJSON.get(CommonConstants.DATE) != null && !inputJSON.get(CommonConstants.DATE).toString().isEmpty()){
			 			momDO.setMomDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.DATE).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.TIME) != null && !inputJSON.get(CommonConstants.TIME).toString().isEmpty() ){
			 			momDO.setMomTime(inputJSON.get(CommonConstants.TIME).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.CUSTOMERID) != null && !inputJSON.get(CommonConstants.CUSTOMERID).toString().isEmpty()){
			 			List<LeadDO> leadList = leadService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.CUSTOMERID).toString()));
			 			if(leadList != null && leadList.size() > 0){
			 				momDO.setLead(leadList.get(0));
			 				if(inputJSON.get(CommonConstants.MOM_CONTACTS_LIST) != null && !inputJSON.get(CommonConstants.MOM_CONTACTS_LIST).toString().isEmpty()){
					 			List<ContactDO> contactDOList = new ArrayList<ContactDO>(); 
						 		JSONArray resultJSONArray = new JSONArray();
					 			resultJSONArray.put(inputJSON.get(CommonConstants.MOM_CONTACTS_LIST));
						 		String rec = (String) resultJSONArray.get(0).toString();
						 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
						 		for (int i = 0; i < resultJSONArray1.size(); i++) {
					 				ContactDO contactDO = new ContactDO();
					 				JSONObject inputJSON1 = (JSONObject) resultJSONArray1.get(i);
					 				if(inputJSON1.get(CommonConstants.TYPE) != null && !inputJSON1.get(CommonConstants.TYPE).toString().isEmpty()){
					 					if(inputJSON1.get(CommonConstants.TYPE).toString().equalsIgnoreCase("new")){
							 				if(inputJSON1.get(CommonConstants.CONTACT_NAME) != null && !inputJSON1.get(CommonConstants.CONTACT_NAME).toString().isEmpty()){
							 					String names[] = inputJSON1.get(CommonConstants.CONTACT_NAME).toString().split(" ");
							 					if (names.length == 3){
							 						contactDO.setFirstname(names[0]);
							 						contactDO.setMiddlename(names[1]);
							 						contactDO.setLastname(names[2]);
							 					} else if(names.length == 2){
							 						contactDO.setFirstname(names[0]);
							 						contactDO.setLastname(names[1]);
							 					} else{
							 						contactDO.setFirstname(names[0]);
							 					}
									 		}
							 				if(inputJSON1.get(CommonConstants.EMAIL) != null && !inputJSON1.get(CommonConstants.EMAIL).toString().isEmpty()){
						 						contactDO.setPrimaryemail(inputJSON1.get(CommonConstants.EMAIL).toString());
									 		}
							 				if(inputJSON1.get(CommonConstants.MOBILE) != null && !inputJSON1.get(CommonConstants.MOBILE).toString().isEmpty()){
							 					contactDO.setMobile1(inputJSON1.get(CommonConstants.MOBILE).toString());
									 		}
							 				contactDO.setUpdatedon(new Date());
							 				contactDO.setLead(leadList.get(0));
							 				contactDO.setCreatedon(new Date());
							 				contactDO.setIsDeleted("n");
							 				if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
							 					contactDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
							 					contactDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
							 					contactDO.setEmpId(inputJSON.get(CommonConstants.UPDATED_BY).toString());
									 		}
							 				contactDOList.add(contactDO);
					 					}
					 				}
						 		}
						 		if(contactDOList != null && contactDOList.size() > 0){
						 			contactService.persistList(contactDOList);
						 		}
			 				}
			 			}
			 		}else{
			 			LeadDO lead =  new LeadDO();
			 			lead.setName(inputJSON.get(CommonConstants.COMPANY) != null && !inputJSON.get(CommonConstants.COMPANY).toString().isEmpty() ? inputJSON.get(CommonConstants.COMPANY).toString().toUpperCase() : "");
			 			lead.setUpdatedon(new Date());
			 			lead.setCreatedon(new Date());
				 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			lead.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 			lead.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 			lead.setEmpId(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.TYPE) != null && !inputJSON.get(CommonConstants.TYPE).toString().isEmpty()){
				 			lead.setType(inputJSON.get(CommonConstants.TYPE).toString());
				 		}
				 		lead.setIsDeleted("n");
			 			leadService.persist(lead);
			 			if(lead.getLeadId() != null){
			 				if(inputJSON.get(CommonConstants.MOM_CONTACTS_LIST) != null && !inputJSON.get(CommonConstants.MOM_CONTACTS_LIST).toString().isEmpty()){
					 			List<ContactDO> contactDOList = new ArrayList<ContactDO>(); 
						 		JSONArray resultJSONArray = new JSONArray();
					 			resultJSONArray.put(inputJSON.get(CommonConstants.MOM_CONTACTS_LIST));
						 		String rec = (String) resultJSONArray.get(0).toString();
						 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
						 		for (int i = 0; i < resultJSONArray1.size(); i++) {
					 				ContactDO contactDO = new ContactDO();
					 				JSONObject inputJSON1 = (JSONObject) resultJSONArray1.get(i);
					 				if(inputJSON1.get(CommonConstants.TYPE) != null && !inputJSON1.get(CommonConstants.TYPE).toString().isEmpty()){
					 					if(inputJSON1.get(CommonConstants.TYPE).toString().equalsIgnoreCase("new")){
							 				if(inputJSON1.get(CommonConstants.CONTACT_NAME) != null && !inputJSON1.get(CommonConstants.CONTACT_NAME).toString().isEmpty()){
							 					String names[] = inputJSON1.get(CommonConstants.CONTACT_NAME).toString().split(" ");
							 					if (names.length == 3){
							 						contactDO.setFirstname(names[0]);
							 						contactDO.setMiddlename(names[1]);
							 						contactDO.setLastname(names[2]);
							 					} else if(names.length == 2){
							 						contactDO.setFirstname(names[0]);
							 						contactDO.setLastname(names[1]);
							 					} else{
							 						contactDO.setFirstname(names[0]);
							 					}
									 		}
							 				if(inputJSON1.get(CommonConstants.EMAIL) != null && !inputJSON1.get(CommonConstants.EMAIL).toString().isEmpty()){
						 						contactDO.setPrimaryemail(inputJSON1.get(CommonConstants.EMAIL).toString());
									 		}
							 				if(inputJSON1.get(CommonConstants.MOBILE) != null && !inputJSON1.get(CommonConstants.MOBILE).toString().isEmpty()){
							 					contactDO.setMobile1(inputJSON1.get(CommonConstants.MOBILE).toString());
									 		}
							 				contactDO.setUpdatedon(new Date());
							 				contactDO.setLead(lead);
							 				contactDO.setCreatedon(new Date());
							 				contactDO.setIsDeleted("n");
							 				if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
							 					contactDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
							 					contactDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
							 					contactDO.setEmpId(inputJSON.get(CommonConstants.UPDATED_BY).toString());
									 		}
							 				contactDOList.add(contactDO);
					 					}
					 				}
						 		}
						 		if(contactDOList != null && contactDOList.size() > 0){
						 			contactService.persistList(contactDOList);
						 		}
			 				}
			 			}
			 			momDO.setLead(lead);
			 		}
			 		if(inputJSON.get(CommonConstants.EVENTID) != null && !inputJSON.get(CommonConstants.EVENTID).toString().isEmpty()){
			 			List<EventsDO> eventsList = eventsService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.EVENTID).toString()));
			 			if(eventsList != null && eventsList.size() > 0){
			 				momDO.setEvents(eventsList.get(0));
			 			}
			 		}
			 		momDO.setUpdatedon(new Date());
			 		momDO.setCreatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			momDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			momDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			momDO.setEmpId(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		momDO = momService.persist(momDO);
			 		if(inputJSON.get(CommonConstants.MOM_NOTES_LIST) != null && !inputJSON.get(CommonConstants.MOM_NOTES_LIST).toString().isEmpty()){
			 			List<MinutesOfMeetingNotesDO> momNotesDOList = new ArrayList<MinutesOfMeetingNotesDO>(); 
				 		JSONArray resultJSONArray = new JSONArray();
			 			resultJSONArray.put(inputJSON.get(CommonConstants.MOM_NOTES_LIST));
				 		String rec = (String) resultJSONArray.get(0).toString();
				 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
				 		for (int i = 0; i < resultJSONArray1.size(); i++) {
				 			MinutesOfMeetingNotesDO momNotesDO = new MinutesOfMeetingNotesDO(); 
				 			JSONObject inputJSON1 = (JSONObject) resultJSONArray1.get(i);
				 			//momNotesDO.setDescription(inputJSON1.get(CommonConstants.DESCRIPTION)!=null && !inputJSON1.get(CommonConstants.DESCRIPTION).toString().isEmpty()?inputJSON1.get(CommonConstants.DESCRIPTION).toString():"");
				 			momNotesDO.setFollowups(inputJSON1.get(CommonConstants.FOLLOW_UPS)!=null && !inputJSON1.get(CommonConstants.FOLLOW_UPS).toString().isEmpty()?inputJSON1.get(CommonConstants.FOLLOW_UPS).toString():"");
				 			momNotesDO.setSample(inputJSON1.get(CommonConstants.SAMPLE)!=null && !inputJSON1.get(CommonConstants.SAMPLE).toString().isEmpty()?inputJSON1.get(CommonConstants.SAMPLE).toString():"");
				 			momNotesDO.setPricing(inputJSON1.get(CommonConstants.PRICING)!=null && !inputJSON1.get(CommonConstants.PRICING).toString().isEmpty()?inputJSON1.get(CommonConstants.PRICING).toString():"");
				 			momNotesDO.setIndexValue(Long.parseLong(inputJSON1.get(CommonConstants.INDEX)!=null && !inputJSON1.get(CommonConstants.INDEX).toString().isEmpty()?inputJSON1.get(CommonConstants.INDEX).toString():""));
				 			momNotesDO.setMom(momDO);
				 			momNotesDO.setUpdatedon(new Date());
				 			momNotesDO.setCreatedon(new Date());
				 			if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			momNotesDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			momNotesDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			momNotesDO.setEmpId(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 		}
				 			if(inputJSON1.get(CommonConstants.DESCRIPTION)!=null && !inputJSON1.get(CommonConstants.DESCRIPTION).toString().isEmpty()){
				 				momNotesDO.setDescription(inputJSON1.get(CommonConstants.DESCRIPTION).toString());
				 				momNotesDOList.add(momNotesDO);
				 			}
				 			//momNotesDOList.add(momNotesDO);
				 			
				 		}
				 		momNotesService.persist(momNotesDOList);
			 		}
			 	}
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Minutes Of Meeting Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retrieveById/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveById(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<MinutesOfMeetingDO> momList = momService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = MinutesOfMeetingsUtil.getMOMList(momList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveByCustomerId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByCustomerId(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<MinutesOfMeetingDO> momList = momService.retrieveByCustomerId(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = MinutesOfMeetingsUtil.getMOMList(momList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/retrieveByEvent/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByEvent(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					int i = 0;
					List<Long> deptIds = new ArrayList<Long>();
					if(request.getSession().getAttribute("deptIds") != null){
						deptIds = (List<Long>) request.getSession().getAttribute("deptIds");
					}
					List<String> empIds = new ArrayList<String>();
					if(deptIds != null && deptIds.size() > 0){
						empIds = employeeService.retrieveByDeptIds(deptIds);
					}else if(request.getSession().getAttribute("empIds") != null){
						empIds = new ArrayList<String>();
						String empid = (request.getSession().getAttribute("empIds").toString());
						empIds.add(empid.toString());
						List<UserDO> userList = userService.retriveByEmpId(empid.toString());
						if(userList.get(0).getRole().getRoleId() != null){
							List<RolesDO> roleList = rolesService.retriveById(userList.get(0).getRole().getRoleId());
							if(roleList != null && roleList.size() > 0){
								if(roleList.get(0).getName().equalsIgnoreCase(CommonConstants.SUPERADMIN)){
									i=1;
								}
							}else{
								empIds = new ArrayList<String>();
							}
						}else{
							empIds = new ArrayList<String>();
						}
					}
					if(i == 1){
						List<MinutesOfMeetingDO> momList = momService.retrieveByEvent(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
						respJSON = MinutesOfMeetingsUtil.getMOMList(momList).toString();
					}else{
						List<MinutesOfMeetingDO> momList = momService.retrieveByEmpMom(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()), empIds);
						respJSON = MinutesOfMeetingsUtil.getMOMList(momList).toString();
						/*List<MinutesOfMeetingDO> momList = momService.retrieveByEvent(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
						respJSON = MinutesOfMeetingsUtil.getMOMList(momList).toString();*/
					}
					
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<MinutesOfMeetingDO> momList = momService.retrieve();
				respJSON = MinutesOfMeetingsUtil.getMOMList(momList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				MinutesOfMeetingDO momDO = new MinutesOfMeetingDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
				if (inputJSON != null){
					List<MinutesOfMeetingDO> momList = momService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(momList != null && momList.size() > 0){
			 			momDO = momList.get(0);
			 			momDO.setCompany(inputJSON.get(CommonConstants.COMPANY) != null && !inputJSON.get(CommonConstants.COMPANY).toString().isEmpty() ? inputJSON.get(CommonConstants.COMPANY).toString().toUpperCase() : "");
				 		momDO.setEvent(inputJSON.get(CommonConstants.EVENT) != null && !inputJSON.get(CommonConstants.EVENT).toString().isEmpty() ? inputJSON.get(CommonConstants.EVENT).toString() : "");
				 		momDO.setParticipants(inputJSON.get(CommonConstants.PARTICIPANTS) != null && !inputJSON.get(CommonConstants.PARTICIPANTS).toString().isEmpty() ? inputJSON.get(CommonConstants.PARTICIPANTS).toString() : "");
				 		momDO.setTeam(inputJSON.get(CommonConstants.TEAM) != null && !inputJSON.get(CommonConstants.TEAM).toString().isEmpty() ? inputJSON.get(CommonConstants.TEAM).toString() : "");
				 		if(inputJSON.get(CommonConstants.DATE) != null && !inputJSON.get(CommonConstants.DATE).toString().isEmpty()){
				 			momDO.setMomDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.DATE).toString()));
				 		}
				 		if(inputJSON.get(CommonConstants.TIME) != null && !inputJSON.get(CommonConstants.TIME).toString().isEmpty() ){
				 			momDO.setMomTime(inputJSON.get(CommonConstants.TIME).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.CUSTOMERID) != null && !inputJSON.get(CommonConstants.CUSTOMERID).toString().isEmpty()){
				 			List<LeadDO> leadList = leadService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.CUSTOMERID).toString()));
				 			if(leadList != null && leadList.size() > 0){
				 				momDO.setLead(leadList.get(0));
				 			}
				 		}
				 		momDO.setUpdatedon(new Date());
				 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			momDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 			momDO.setEmpId(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		momDO = momService.update(momDO);
				 		if(inputJSON.get(CommonConstants.MOM_NOTES_LIST) != null && !inputJSON.get(CommonConstants.MOM_NOTES_LIST).toString().isEmpty()){
				 			List<MinutesOfMeetingNotesDO> momNotesDOList = new ArrayList<MinutesOfMeetingNotesDO>(); 
					 		JSONArray resultJSONArray = new JSONArray();
				 			resultJSONArray.put(inputJSON.get(CommonConstants.MOM_NOTES_LIST));
					 		String rec = (String) resultJSONArray.get(0).toString();
					 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
					 		for (int i = 0; i < resultJSONArray1.size(); i++) {
					 			MinutesOfMeetingNotesDO momNotesDO = new MinutesOfMeetingNotesDO(); 
					 			JSONObject inputJSON1 = (JSONObject) resultJSONArray1.get(i);
					 			momNotesDO.setFollowups(inputJSON1.get(CommonConstants.FOLLOW_UPS)!=null && !inputJSON1.get(CommonConstants.FOLLOW_UPS).toString().isEmpty()?inputJSON1.get(CommonConstants.FOLLOW_UPS).toString():"");
					 			momNotesDO.setSample(inputJSON1.get(CommonConstants.SAMPLE)!=null && !inputJSON1.get(CommonConstants.SAMPLE).toString().isEmpty()?inputJSON1.get(CommonConstants.SAMPLE).toString():"");
					 			momNotesDO.setPricing(inputJSON1.get(CommonConstants.PRICING)!=null && !inputJSON1.get(CommonConstants.PRICING).toString().isEmpty()?inputJSON1.get(CommonConstants.PRICING).toString():"");
					 			momNotesDO.setIndexValue(Long.parseLong(inputJSON1.get(CommonConstants.INDEX)!=null && !inputJSON1.get(CommonConstants.INDEX).toString().isEmpty()?inputJSON1.get(CommonConstants.INDEX).toString():""));
					 			momNotesDO.setMom(momDO);
					 			momNotesDO.setUpdatedon(new Date());
					 			momNotesDO.setCreatedon(new Date());
					 			if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
						 			momNotesDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
						 			momNotesDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
						 			momNotesDO.setEmpId(inputJSON.get(CommonConstants.UPDATED_BY).toString());
						 		}
					 			if(inputJSON1.get(CommonConstants.DESCRIPTION)!=null && !inputJSON1.get(CommonConstants.DESCRIPTION).toString().isEmpty()){
					 				momNotesDO.setDescription(inputJSON1.get(CommonConstants.DESCRIPTION).toString());
					 				momNotesDOList.add(momNotesDO);
					 			}
					 			
					 		}
					 		momNotesService.persist(momNotesDOList);
				 		}else{
				 			momNotesService.delete(momDO.getMomId());
				 		}
			 		}
			 	}
		 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Minutes Of Meeting Updated");

			 }else{
					return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/retrieveByExportCustomer", method = RequestMethod.POST)
	public @ResponseBody String retrieveByExportCustomer(Model model, HttpServletRequest request) {
		String respJSON = null;
		JSONArray resultJSONArray = new JSONArray();
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
				if(inputJSON != null && !inputJSON.get(CommonConstants.EXPORTLIST).toString().isEmpty()){
					org.json.simple.JSONArray selectedData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.EXPORTLIST).toString());
				//	List<MinutesOfMeetingNotesDO> momNoteList = new ArrayList<MinutesOfMeetingNotesDO>();
					List<MinutesOfMeetingDO> momDetailsList = new ArrayList<MinutesOfMeetingDO>();
					for (int i=0; i < selectedData.size(); i++){
						JSONObject rowJSON = CommonWebUtil.getInputParams(selectedData.get(i).toString());
						if(rowJSON.get(CommonConstants.CUSTOMERID) != null && !rowJSON.get(CommonConstants.CUSTOMERID).toString().isEmpty()){
							List <MinutesOfMeetingDO>  momDetail  = momService.retrieveByCustomerId(Long.parseLong(rowJSON.get(CommonConstants.CUSTOMERID).toString()));
							if(momDetail != null && momDetail.size() > 0){
								for(MinutesOfMeetingDO mom : momDetail){
									List <MinutesOfMeetingNotesDO> momNoteDetails = momNotesService.retrieveForExport(mom.getMomId(), rowJSON.get(CommonConstants.PRICING).toString(), rowJSON.get(CommonConstants.FOLLOW_UPS).toString(), rowJSON.get(CommonConstants.SAMPLE).toString());
									if(momNoteDetails != null && momNoteDetails.size() > 0){
										JSONObject result = new JSONObject();
										result.put(CommonConstants.ID, String.valueOf(mom.getMomId() != null ? mom.getMomId() : ""));
										result.put(CommonConstants.COMPANY, String.valueOf(mom.getCompany()));
										result.put(CommonConstants.PARTICIPANTS, String.valueOf(mom.getParticipants() != null ? mom.getParticipants() : ""));
										result.put(CommonConstants.EVENT, String.valueOf(mom.getEvent() != null ? mom.getEvent() : ""));
										result.put(CommonConstants.TEAM, String.valueOf(mom.getTeam() != null ? mom.getTeam() : ""));
										result.put(CommonConstants.DATE, String.valueOf(mom.getMomDate() != null ? mom.getMomDate() : ""));
										result.put(CommonConstants.TIME, String.valueOf(mom.getMomTime() != null ? mom.getMomTime() : ""));
										if(mom.getLead() != null){
											result.put(CommonConstants.CUSTOMERNAME, String.valueOf(mom.getLead().getName()));
											result.put(CommonConstants.CUSTOMERID, String.valueOf(mom.getLead().getLeadId()));
											result.put(CommonConstants.TYPE, String.valueOf(mom.getLead().getType()));
										} else {
											result.put(CommonConstants.CUSTOMERNAME, "");
											result.put(CommonConstants.CUSTOMERID, "");
											result.put(CommonConstants.TYPE, "");
										}
										JSONArray resultJSONArray2 = new JSONArray();
										for(MinutesOfMeetingNotesDO momNote : momNoteDetails){
											//momNoteList.add(momNote);
											JSONObject resultNote = new JSONObject();
											resultNote.put(CommonConstants.DESCRIPTION, String.valueOf(momNote.getDescription()));
											
											
											//resultNote.put(CommonConstants.SAMPLE, String.valueOf(momNote.getSample()));
											
											if(rowJSON.get(CommonConstants.PRICING).toString().equalsIgnoreCase(CommonConstants.FALSE) && rowJSON.get(CommonConstants.FOLLOW_UPS).toString().equalsIgnoreCase(CommonConstants.FALSE) && rowJSON.get(CommonConstants.SAMPLE).toString().equalsIgnoreCase(CommonConstants.FALSE)){
												resultNote.put(CommonConstants.SAMPLE, String.valueOf(momNote.getSample()));
												resultNote.put(CommonConstants.FOLLOW_UPS, String.valueOf(momNote.getFollowups()));
												resultNote.put(CommonConstants.PRICING, String.valueOf(momNote.getPricing()));
												
											}else{
												if(rowJSON.get(CommonConstants.PRICING).toString().equalsIgnoreCase(CommonConstants.TRUE)) resultNote.put(CommonConstants.PRICING, String.valueOf(momNote.getPricing()));
												else resultNote.put(CommonConstants.PRICING, String.valueOf(""));
												
												if(rowJSON.get(CommonConstants.FOLLOW_UPS).toString().equalsIgnoreCase(CommonConstants.TRUE)) resultNote.put(CommonConstants.FOLLOW_UPS, String.valueOf(momNote.getFollowups()));
												else resultNote.put(CommonConstants.FOLLOW_UPS, String.valueOf(""));
												
												if(rowJSON.get(CommonConstants.SAMPLE).toString().equalsIgnoreCase(CommonConstants.TRUE)) resultNote.put(CommonConstants.SAMPLE, String.valueOf(momNote.getSample()));
												else resultNote.put(CommonConstants.SAMPLE, String.valueOf(""));
											}
											
											
											//resultNote.put(CommonConstants.FOLLOW_UPS, String.valueOf(momNote.getFollowups()));
											resultNote.put(CommonConstants.INDEX, String.valueOf(momNote.getIndexValue()));
											resultNote.put(CommonConstants.NOTE, String.valueOf(momNote.getDescription()));
											resultJSONArray2.put(resultNote);
										}
										result.put(CommonConstants.NOTES, resultJSONArray2);
										
										resultJSONArray.put(result);
									}else{
										momDetailsList.add(mom);
									}
								}
							}
							respJSON = MinutesOfMeetingsUtil.getMOMListForReport(resultJSONArray).toString();
						}
					}
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
}
