package com.spheresuite.erp.crm.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.IndustryDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class IndustryDAO {
	static Logger logger = Logger.getLogger(IndustryDAO.class.getName());
	@Autowired
	private GenericDAOImpl<IndustryDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(IndustryDO industryDO)  {
		boolean persistStatus = true;
		List<IndustryDO> industryList = null;
		try {
			industryList = this.sessionFactory.getCurrentSession().getNamedQuery(IndustryDO.FIND_BY_NAME)
			.setParameter(CommonConstants.NAME, industryDO.getName())
			.list();
			if(industryList != null && industryList.size() > 0){
				persistStatus = false;
			}else{
				genericObject.persist(industryDO);
			}
			
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	public List<IndustryDO> retrieve()  {
		List<IndustryDO> industryList = null;
		try {
			industryList = genericObject.retrieve(IndustryDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return industryList;
	}
	
	public List<IndustryDO> retrieveActive()  {
		List<IndustryDO> industryList = null;
		try {
			return genericObject.retrieveActive(CommonConstants.ACTIVE, 
					IndustryDO.FIND_BY_STATUS, IndustryDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return industryList;
	}
	
	@SuppressWarnings("unchecked")
	public List<IndustryDO> retrieveById(Long Id)  {
		List<IndustryDO> industryList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(IndustryDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, Id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return industryList;
	}
	
	@SuppressWarnings("unchecked")
	public List<IndustryDO> retrieveByActiveName(String name)  {
		List<IndustryDO> industryList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(IndustryDO.FIND_BY_ACTIVE_NAME)
					.setParameter(CommonConstants.NAME, name)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return industryList;
	}
	
	@SuppressWarnings("unchecked")
	public boolean update(IndustryDO industryDO)  {
		boolean updateStatus = true;
		try {
			List<IndustryDO> industryList = null;
			industryList = this.sessionFactory.getCurrentSession().getNamedQuery(IndustryDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, industryDO.getName())
					.setParameter(CommonConstants.ID, industryDO.getIndustryId())
					.list();
			if(industryList != null && industryList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(industryDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}
	
	public boolean persistList(List<IndustryDO> industryDOList){
		try {
			for (IndustryDO industryDO : industryDOList) {
				genericObject.persist(industryDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}

}
