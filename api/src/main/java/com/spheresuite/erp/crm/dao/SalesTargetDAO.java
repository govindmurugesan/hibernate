package com.spheresuite.erp.crm.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.SalesTargetDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class SalesTargetDAO {
	static Logger logger = Logger.getLogger(SalesTargetDAO.class.getName());
	
	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private GenericDAOImpl<SalesTargetDO> genericObject;
	
	public SalesTargetDO persist(SalesTargetDO salesTargetDO) {
		try {
			genericObject.persist(salesTargetDO);
		} catch (Exception eException) {
			//throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			//SessionManager.closeEntityManager(em);
		}
		return salesTargetDO;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<SalesTargetDO> retrieve(List<String> id) {
		List<SalesTargetDO> billsList = null;
		try {
			billsList = this.sessionFactory.getCurrentSession().getNamedQuery(SalesTargetDO.FIND_ALL)
										.setParameterList(CommonConstants.ID, id)
										.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return billsList;
	}
	
	public List<SalesTargetDO> retrieveAll() {
		List<SalesTargetDO> billsList = null;
		try {
			billsList = genericObject.retrieve(SalesTargetDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return billsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<SalesTargetDO> retrieveById(Long id) {
		List<SalesTargetDO> proposal = null;
		try {
			proposal =  this.sessionFactory.getCurrentSession().getNamedQuery(SalesTargetDO.FIND_BY_ID)
						.setParameter(CommonConstants.ID, id).list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return proposal;
	}
	
	public SalesTargetDO update(SalesTargetDO salesTargetDO) {
		try {
			genericObject.merge(salesTargetDO);
		} catch (Exception eException) {
			//throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			//SessionManager.closeEntityManager(em);
		}
		return salesTargetDO;
	}
	
	@SuppressWarnings("unchecked")
	public List<SalesTargetDO> retrieveByEmpId(String empId) {
		List<SalesTargetDO> proposal = null;
		try {
			proposal = this.sessionFactory.getCurrentSession().getNamedQuery(SalesTargetDO.FIND_BY_EMP_ID)
										.setParameter(CommonConstants.EMPID, empId)
										.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return proposal;
	}
	
	@SuppressWarnings("unchecked")
	public List<SalesTargetDO> retrieveByYear(Long fromDate, Long toDate) {
		List<SalesTargetDO> proposal = null;
		try {
			proposal = this.sessionFactory.getCurrentSession().getNamedQuery(SalesTargetDO.FIND_BY_YEAR)
										.setParameter(CommonConstants.FROMDATE, fromDate)
										.setParameter(CommonConstants.TODATE, toDate)
										.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return proposal;
	}
	
	@SuppressWarnings("unchecked")
	public List<SalesTargetDO> retrieveByYearForEmp(Long fromDate, Long toDate, String empId) {
		List<SalesTargetDO> proposal = null;
		try {
			proposal = this.sessionFactory.getCurrentSession().getNamedQuery(SalesTargetDO.FIND_BY_YEAR_FOR_EMP)
										.setParameter(CommonConstants.FROMDATE, fromDate)
										.setParameter(CommonConstants.TODATE, toDate)
										.setParameter(CommonConstants.EMPID, empId)
										.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return proposal;
	}

}
