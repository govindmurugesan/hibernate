package com.spheresuite.erp.crm.web.util;

import java.util.List;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.crm.service.ContactService;
import com.spheresuite.erp.crm.service.LeadService;
import com.spheresuite.erp.crm.service.OpportunitiesDocService;
import com.spheresuite.erp.crm.service.OpportunitiesService;
import com.spheresuite.erp.crm.service.OpportunityTypeService;
import com.spheresuite.erp.crm.service.SalesStageService;
import com.spheresuite.erp.domainobject.OpportunitiesDO;
import com.spheresuite.erp.domainobject.OpportunitiesDocDO;
import com.spheresuite.erp.domainobject.SalesStageDO;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
@Component
public class OpportunitiesUtil {
	
	private OpportunitiesUtil() {}
	
	@Autowired
	private  LeadService tleadService;
	private static LeadService leadService;

	@Autowired
	private  OpportunitiesService topportunitiesService;
	private static OpportunitiesService opportunitiesService;
	
	@Autowired
	private  OpportunityTypeService tprojectTypeService;
	private static OpportunityTypeService projectTypeService;
	
	@Autowired
	private ContactService tcontactService;
	private static ContactService contactService;
	
	@Autowired
	private EmployeeService temployeeService;
	
	private static EmployeeService employeeService;
	
	@Autowired
	private OpportunitiesDocService topportunitiesDocService;
	
	private static OpportunitiesDocService opportunitiesDocService;
	
	@Autowired
	private SalesStageService tsalesStageService;
	
	private static SalesStageService salesStageService;
	
	
	@PostConstruct
	public void init() {
		employeeService = temployeeService;
		leadService = tleadService;
		opportunitiesService = topportunitiesService;
		projectTypeService = tprojectTypeService;
		contactService=tcontactService;
		opportunitiesDocService=topportunitiesDocService;
		salesStageService=tsalesStageService;
	}

	
	public static JSONObject getProjectList(List<OpportunitiesDO> projectList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (OpportunitiesDO project : projectList) {
				resultJSONArray.put(getProjectDetailObject(project));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getProjectListWithOutDoc(List<OpportunitiesDO> projectList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (OpportunitiesDO project : projectList) {
				resultJSONArray.put(getProjectWithOutDocDetailObject(project));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getProjectChartList(List<SalesStageDO> salesStageList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (SalesStageDO salesStage : salesStageList) {
				resultJSONArray.put(getProjectForChartDetailObject(salesStage));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
		
	}
	
	public static JSONObject getProjectForChartDetailObject(SalesStageDO salesStage)throws JSONException {
		JSONObject result = new JSONObject();
		Long totalAmount = opportunitiesService.retrieveBySalesStageId(salesStage.getSalesStageId());
		if(totalAmount == null){
			totalAmount = 0L;
		}
		result.put(CommonConstants.NAME, String.valueOf(salesStage.getName()));
		result.put(CommonConstants.VALUE, String.valueOf(totalAmount));
		return result;
	}

	public static JSONObject getProjectDetailObject(OpportunitiesDO project)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(project.getOpportunityId()));
		
		if(project.getLead() != null){
			result.put(CommonConstants.CUSTOMERID, String.valueOf(project.getLead().getLeadId()));
			result.put(CommonConstants.CUSTOMERNAME, String.valueOf(project.getLead().getName()));
		}else{
			result.put(CommonConstants.CUSTOMERID, "");
			result.put(CommonConstants.CUSTOMERNAME, "");
		}
		if(project.getProjectname() != null){
			result.put(CommonConstants.PROJECTNAME, String.valueOf(project.getProjectname()));
		}else{
			result.put(CommonConstants.PROJECTNAME,"");
		}
		if(project.getDisplayname() != null){
			result.put(CommonConstants.DISPLAYNAME, String.valueOf(project.getDisplayname()));
		}else{
			result.put(CommonConstants.DISPLAYNAME,"");
		}
		if(project.getProjectcode() != null){
			result.put(CommonConstants.PROJECTCODE, String.valueOf(project.getProjectcode()));
		}else{
			result.put(CommonConstants.PROJECTCODE,"");
		}
		if(project.getCurrencyType() != null){
			result.put(CommonConstants.CURRENCYTYPE, String.valueOf(project.getCurrencyType().getCountryId()));
			result.put(CommonConstants.CURRENCYTYPE_NAME, String.valueOf(project.getCurrencyType().getCurrencyType()));
		}else{
			result.put(CommonConstants.CURRENCYTYPE,"");
			result.put(CommonConstants.CURRENCYTYPE_NAME, "");
		}
		if(project.getStartdate() != null){
			result.put(CommonConstants.STARTDATE, String.valueOf(project.getStartdate()));
		}else{
			result.put(CommonConstants.STARTDATE,"");
		}
		if(project.getEnddate() != null){
			result.put(CommonConstants.ENDDATE, String.valueOf(project.getEnddate()));
		}else{
			result.put(CommonConstants.ENDDATE,"");
		}
		if(project.getEmpId() != null){
			result.put(CommonConstants.UPDATED_BY, String.valueOf(project.getEmpId()));
		}else{
			result.put(CommonConstants.UPDATED_BY,"");
		}
		if(project.getProjectduration() != null){
			result.put(CommonConstants.PROJECTDURATION, String.valueOf(project.getProjectduration()));
		}else{
			result.put(CommonConstants.PROJECTDURATION,"");
		}
		if(project.getComment() != null){
			result.put(CommonConstants.COMMENT, String.valueOf(project.getComment()));
		}else{
			result.put(CommonConstants.COMMENT,"");
		}
		/*if(project.getUpdatedby() != null){
			List<UserDO> userList = userService.retriveById(Long.parseLong(project.getUpdatedby()));
			if(userList != null && userList.size() > 0){
				result.put(CommonConstants.UPDATED_BY, String.valueOf(userList.get(0).getName())); 
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
			
			List<EmployeeDO> empList = employeeService.retriveByEmpId(Long.parseLong(project.getUpdatedby()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY_NAME, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY_NAME, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY_NAME, ""); 
			}
			
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}*/
		
		if(project.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(project.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		if(project.getUpdatedon() != null){
			result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(project.getUpdatedon())));
		}else{
			result.put(CommonConstants.UPDATEDON,"");
		}
		if(project.getProjectType() != null){
			result.put(CommonConstants.PROJECT_TYPE_NAME, String.valueOf(project.getProjectType().getName()));
			result.put(CommonConstants.PROJECTTYPE, String.valueOf(project.getProjectType().getOpportunityTypeId()));
		}else{
			result.put(CommonConstants.PROJECT_TYPE_NAME, "");
			result.put(CommonConstants.PROJECTTYPE, "");
		}
		
		if(project.getContact() != null){
			result.put(CommonConstants.CONTACT_ID, String.valueOf(project.getContact().getContactId()));
			String name = "";
			if(project.getContact().getFirstname() != null){
				name = name + project.getContact().getFirstname();
			}else if(project.getContact().getMiddlename() != null){
				name = name + project.getContact().getMiddlename();
			}else if(project.getContact().getLastname() != null){
				name = name + project.getContact().getLastname();
			}
			result.put(CommonConstants.CONTACT_NAME, name);
		}else{
			result.put(CommonConstants.CONTACT_ID, "");
			result.put(CommonConstants.CONTACT_NAME, "");
		}
		
		if(project.getAmount() != null){
			result.put(CommonConstants.COST, String.valueOf(project.getAmount()));
		}else{
			result.put(CommonConstants.COST, "");
		}
		
		if(project.getProbability() != null){
			result.put(CommonConstants.PROBOBILITY, String.valueOf(project.getProbability()));
		}else{
			result.put(CommonConstants.PROBOBILITY, "");
		}
		
		if(project.getSalesStage() != null){
			result.put(CommonConstants.SALESSTAGE, String.valueOf(project.getSalesStage().getSalesStageId()));
			result.put(CommonConstants.SALESSTAGE_NAME, project.getSalesStage().getName());
		}else{
			result.put(CommonConstants.SALESSTAGE, "");
		}
		
		JSONArray resultJSONArray = new JSONArray();
		
		List<OpportunitiesDocDO> opportunitiesDocDOList = opportunitiesDocService.retrieveByOppId(project.getOpportunityId());
		if(opportunitiesDocDOList != null && opportunitiesDocDOList.size()  > 0){
			for (OpportunitiesDocDO doc : opportunitiesDocDOList) {
				resultJSONArray.put(getDocDetailObject(doc));
			}
		}
		result.put(CommonConstants.FILE, resultJSONArray);
		return result;
	}
	
	public static JSONObject getDocDetailObject(OpportunitiesDocDO doc)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.FILE, String.valueOf(doc.getPhoto()));
		if(doc.getFileName() != null){
			result.put(CommonConstants.NAME, String.valueOf(doc.getFileName()));
		}else{
			result.put(CommonConstants.NAME, "");	
		}
		
		return result;
	}
	

	public static JSONObject getProjectWithOutDocDetailObject(OpportunitiesDO project)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(project.getOpportunityId()));
		
		if(project.getLead() != null){
			result.put(CommonConstants.CUSTOMERID, String.valueOf(project.getLead().getLeadId()));
			result.put(CommonConstants.CUSTOMERNAME, String.valueOf(project.getLead().getName()));
		}else{
			result.put(CommonConstants.CUSTOMERID, "");
			result.put(CommonConstants.CUSTOMERNAME, "");
		}
		if(project.getProjectname() != null){
			result.put(CommonConstants.PROJECTNAME, String.valueOf(project.getProjectname()));
		}else{
			result.put(CommonConstants.PROJECTNAME,"");
		}
		if(project.getDisplayname() != null){
			result.put(CommonConstants.DISPLAYNAME, String.valueOf(project.getDisplayname()));
		}else{
			result.put(CommonConstants.DISPLAYNAME,"");
		}
		if(project.getProjectcode() != null){
			result.put(CommonConstants.PROJECTCODE, String.valueOf(project.getProjectcode()));
		}else{
			result.put(CommonConstants.PROJECTCODE,"");
		}
		if(project.getStartdate() != null){
			result.put(CommonConstants.STARTDATE, String.valueOf(project.getStartdate()));
		}else{
			result.put(CommonConstants.STARTDATE,"");
		}
		if(project.getEnddate() != null){
			result.put(CommonConstants.ENDDATE, String.valueOf(project.getEnddate()));
		}else{
			result.put(CommonConstants.ENDDATE,"");
		}
		if(project.getEmpId() != null){
			result.put(CommonConstants.UPDATED_BY, String.valueOf(project.getEmpId()));
		}else{
			result.put(CommonConstants.UPDATED_BY,"");
		}
		if(project.getProjectduration() != null){
			result.put(CommonConstants.PROJECTDURATION, String.valueOf(project.getProjectduration()));
		}else{
			result.put(CommonConstants.PROJECTDURATION,"");
		}
		if(project.getCurrencyType() != null){
			result.put(CommonConstants.CURRENCYTYPE, String.valueOf(project.getCurrencyType().getCountryId()));
			result.put(CommonConstants.CURRENCYTYPE_NAME, String.valueOf(project.getCurrencyType().getCurrencyType()));
		}else{
			result.put(CommonConstants.CURRENCYTYPE,"");
			result.put(CommonConstants.CURRENCYTYPE_NAME, "");
		}
		if(project.getComment() != null){
			result.put(CommonConstants.COMMENT, String.valueOf(project.getComment()));
		}else{
			result.put(CommonConstants.COMMENT,"");
		}
		/*if(project.getUpdatedby() != null){
			List<UserDO> userList = userService.retriveById(Long.parseLong(project.getUpdatedby()));
			if(userList != null && userList.size() > 0){
				result.put(CommonConstants.UPDATED_BY, String.valueOf(userList.get(0).getName())); 
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
			
			List<EmployeeDO> empList = employeeService.retriveByEmpId(Long.parseLong(project.getUpdatedby()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY_NAME, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY_NAME, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY_NAME, ""); 
			}
			
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}*/
		
		if(project.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(project.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		if(project.getUpdatedon() != null){
			result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(project.getUpdatedon())));
		}else{
			result.put(CommonConstants.UPDATEDON,"");
		}
		if(project.getProjectType() != null){
			result.put(CommonConstants.PROJECT_TYPE_NAME, String.valueOf(project.getProjectType().getName()));
			result.put(CommonConstants.PROJECTTYPE, String.valueOf(project.getProjectType().getOpportunityTypeId()));
		}else{
			result.put(CommonConstants.PROJECT_TYPE_NAME, "");
			result.put(CommonConstants.PROJECTTYPE, "");
		}
		
		if(project.getContact() != null){
			result.put(CommonConstants.CONTACT_ID, String.valueOf(project.getContact().getContactId()));
			String name = "";
			if(project.getContact().getFirstname() != null){
				name = name + project.getContact().getFirstname();
			}else if(project.getContact().getMiddlename() != null){
				name = name + project.getContact().getMiddlename();
			}else if(project.getContact().getLastname() != null){
				name = name + project.getContact().getLastname();
			}
			result.put(CommonConstants.CONTACT_NAME, name);
		}else{
			result.put(CommonConstants.CONTACT_ID, "");
			result.put(CommonConstants.CONTACT_NAME, "");
		}
		
		if(project.getAmount() != null){
			result.put(CommonConstants.COST, String.valueOf(project.getAmount()));
		}else{
			result.put(CommonConstants.COST, "");
		}
		
		if(project.getProbability() != null){
			result.put(CommonConstants.PROBOBILITY, String.valueOf(project.getProbability()));
		}else{
			result.put(CommonConstants.PROBOBILITY, "");
		}
		
		if(project.getSalesStage() != null){
			result.put(CommonConstants.SALESSTAGE, String.valueOf(project.getSalesStage().getSalesStageId()));
			result.put(CommonConstants.SALESSTAGE_NAME, project.getSalesStage().getName());
		}else{
			result.put(CommonConstants.SALESSTAGE, "");
		}
		return result;
	}
	
}
