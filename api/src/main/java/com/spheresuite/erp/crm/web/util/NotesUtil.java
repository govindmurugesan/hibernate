package com.spheresuite.erp.crm.web.util;

import java.util.List;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.crm.service.LeadService;
import com.spheresuite.erp.domainobject.NotesDO;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
@Component
public class NotesUtil {
	
	private NotesUtil() {}
	
	@Autowired
	private EmployeeService temployeeService;
	
	
	private static EmployeeService employeeService;
	
	@Autowired
	private LeadService tleadService;
	
	private static LeadService leadService;
	
	@PostConstruct
	public void init() {
		employeeService = temployeeService;
		leadService=tleadService;
	}
	
	
	
	public static JSONObject getNotesList(List<NotesDO> notesList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (NotesDO notes : notesList) {
				resultJSONArray.put(getCategoryDetailObject(notes));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getCategoryDetailObject(NotesDO notes)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(notes.getNotesId() != null ? notes.getNotesId() : ""));
		result.put(CommonConstants.NOTES, String.valueOf(notes.getNote() != null ? notes.getNote() : ""));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(notes.getUpdatedon())));
		/*if(notes.getUpdatedBy() != null){
			List<UserDO> userList = userService.retriveById(Long.parseLong(notes.getUpdatedBy()));
			if(userList != null && userList.size() > 0){
				result.put(CommonConstants.UPDATED_BY, String.valueOf(userList.get(0).getName())); 
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
			
			List<EmployeeDO> empList = employeeService.retriveByEmpId(Long.parseLong(notes.getUpdatedBy()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}*/
		if(notes.getUpdatedBy() != null){
			String empName = CommonUtil.getEmpObject(notes.getUpdatedBy());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		
		if(notes.getLead() != null){
			result.put(CommonConstants.LEAD_ID, String.valueOf(notes.getLead().getLeadId() != null ? notes.getLead().getLeadId() : ""));
			result.put(CommonConstants.LEAD_NAME, String.valueOf(notes.getLead().getName()));
		}else{
			result.put(CommonConstants.LEAD_NAME,"");
		}
		return result;
	}
}
