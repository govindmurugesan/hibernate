package com.spheresuite.erp.crm.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.PaymentTermDO;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
@Component
public class PaymentTermUtil {
	
	private PaymentTermUtil() {}
	
	public static JSONObject getPaymentTermList(List<PaymentTermDO> paymentTermList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (PaymentTermDO paymentTerm : paymentTermList) {
				resultJSONArray.put(getPaymentTermDetailObject(paymentTerm));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getPaymentTermDetailObject(PaymentTermDO paymentTerm)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(paymentTerm.getPaymentTermId()));
		result.put(CommonConstants.NAME, String.valueOf(paymentTerm.getName()));
		result.put(CommonConstants.STATUS, String.valueOf(paymentTerm.getStatus()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(paymentTerm.getUpdatedon())));
		if(paymentTerm.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(paymentTerm.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}	else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		return result;
	}
}
