package com.spheresuite.erp.crm.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.ProjectDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class ProjectDAO {
	static Logger logger = Logger.getLogger(ProjectDAO.class.getName());

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private GenericDAOImpl<ProjectDO> genericObject;
	
	public ProjectDO persist(ProjectDO projectDO) {
		try {
			genericObject.persist(projectDO);
		} catch (Exception eException) {
			//throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			//SessionManager.closeEntityManager(em);
		}
		return projectDO;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<ProjectDO> retrieve(List<String> Id) {
		List<ProjectDO> projectList = null;
		try {
			projectList = this.sessionFactory.getCurrentSession().getNamedQuery(ProjectDO.FIND_ALL)
										.setParameter(CommonConstants.ID,Id)
										.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return projectList;
	}
	
	public List<ProjectDO> retrieveAll() {
		List<ProjectDO> projectList = null;
		try {
			projectList = genericObject.retrieve(ProjectDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return projectList;
	}
	
	@SuppressWarnings("unchecked")
	public List<ProjectDO> retrieveById(Long Id) {
		List<ProjectDO> projectList = null;
		try {
			projectList =  this.sessionFactory.getCurrentSession().getNamedQuery(ProjectDO.FIND_BY_ID)
							.setParameter(CommonConstants.ID, Id).list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return projectList;
	}
	
	@SuppressWarnings("unchecked")
	public List<ProjectDO> retrieveByOpportunityId(Long Id) {
		List<ProjectDO> projectList = null;
		try {
			projectList = this.sessionFactory.getCurrentSession().getNamedQuery(ProjectDO.FIND_BY_OPP_ID)
										.setParameter(CommonConstants.ID,Id)
										.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return projectList;
	}
	
	public ProjectDO update(ProjectDO ProjectDO) {
		try {
			genericObject.merge(ProjectDO);
		} catch (Exception eException) {
		} finally {
		}
		return ProjectDO;
	}
	
	public boolean persistList(List<ProjectDO> projectDO) {
		try {
			for (ProjectDO projectDO2 : projectDO) {
				genericObject.persist(projectDO2);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}

}
