package com.spheresuite.erp.crm.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.crm.service.ContactService;
import com.spheresuite.erp.crm.service.InvoiceTermService;
import com.spheresuite.erp.crm.service.LeadService;
import com.spheresuite.erp.crm.service.OpportunitiesService;
import com.spheresuite.erp.crm.service.PaymentTermService;
import com.spheresuite.erp.crm.service.ProposalDocumentsService;
import com.spheresuite.erp.crm.service.ProposalService;
import com.spheresuite.erp.crm.web.util.ProposalUtil;
import com.spheresuite.erp.domainobject.ContactDO;
import com.spheresuite.erp.domainobject.CountryDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.InvoiceTermDO;
import com.spheresuite.erp.domainobject.LeadDO;
import com.spheresuite.erp.domainobject.OpportunitiesDO;
import com.spheresuite.erp.domainobject.PaymentTermDO;
import com.spheresuite.erp.domainobject.ProposalDO;
import com.spheresuite.erp.domainobject.ProposalDocumentsDO;
import com.spheresuite.erp.domainobject.RolesDO;
import com.spheresuite.erp.domainobject.UserDO;
import com.spheresuite.erp.service.CountryService;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.RolesService;
import com.spheresuite.erp.service.UserService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/proposals")
public class ProposalRS {

	String validation = null;
	static Logger logger = Logger.getLogger(ProposalRS.class.getName());

	@Autowired
	private ProposalService proposalService;
	
	@Autowired
	private InvoiceTermService invoiceTermService;
	
	@Autowired
	private ProposalDocumentsService proposalDocumentsService;
	
	/*@Autowired
	private ProposalDefaultService proposalDefaultService;*/
	
	@Autowired
	private EmployeeService employeeService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private RolesService rolesService;
	
	@Autowired
	private LeadService leadService;
	
	@Autowired
	private OpportunitiesService opportunitiesService;
	
	@Autowired
	private PaymentTermService paymentTermService;
	
	@Autowired
	private ContactService contactService;
	
	@Autowired
	private CountryService countryService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String persist(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				ProposalDO proposalDO = new ProposalDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		/*if(inputJSON.get(CommonConstants.PROJECTID) != null && !inputJSON.get(CommonConstants.PROJECTID).toString().isEmpty()){
			 			proposalDO.setProjectId(Long.parseLong(inputJSON.get(CommonConstants.PROJECTID).toString()));
			 		}*/

			 		/*if(inputJSON.get(CommonConstants.CUSTOMERID) != null && !inputJSON.get(CommonConstants.CUSTOMERID).toString().isEmpty()){
			 			proposalDO.setCustomerId(Long.parseLong(inputJSON.get(CommonConstants.CUSTOMERID).toString()));
			 		}*/
			 		if(inputJSON.get(CommonConstants.CUSTOMERID) != null && !inputJSON.get(CommonConstants.CUSTOMERID).toString().isEmpty()){
			 			List<LeadDO> customerList = leadService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.CUSTOMERID).toString()));
			 			if(customerList != null && customerList.size() > 0){
			 				proposalDO.setCustomer(customerList.get(0));
			 			}
			 		}
			 		if(inputJSON.get(CommonConstants.PROJECTID) != null && !inputJSON.get(CommonConstants.PROJECTID).toString().isEmpty()){
			 			List<OpportunitiesDO> oppList = opportunitiesService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.PROJECTID).toString()));
			 			if(oppList != null && oppList.size() > 0){
			 				proposalDO.setProject(oppList.get(0));
			 			}
			 		}
			 		if(inputJSON.get(CommonConstants.PAYMENTTERM) != null && !inputJSON.get(CommonConstants.PAYMENTTERM).toString().isEmpty()){
			 			List<PaymentTermDO> paymentTermList = paymentTermService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.PAYMENTTERM).toString()));
			 			if(paymentTermList != null && paymentTermList.size() > 0){
			 				proposalDO.setPaymentTerm(paymentTermList.get(0));
			 			}
			 		}
			 		/*if(!inputJSON.get(CommonConstants.PROJECTTYPE).toString().isEmpty()){
			 			proposalDO.setProjectTypeId(Long.parseLong(inputJSON.get(CommonConstants.PROJECTTYPE).toString()));
			 		}*/
			 		
			 		if(inputJSON.get(CommonConstants.QUANTITY) != null && !inputJSON.get(CommonConstants.QUANTITY).toString().isEmpty()){
			 			proposalDO.setQunatity(Long.parseLong(inputJSON.get(CommonConstants.QUANTITY).toString()));
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.PROPOSAL_NAME) != null && !inputJSON.get(CommonConstants.PROPOSAL_NAME).toString().isEmpty()){
			 			proposalDO.setProposalName(inputJSON.get(CommonConstants.PROPOSAL_NAME).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.PROPOSAL_DATE) != null && !inputJSON.get(CommonConstants.PROPOSAL_DATE).toString().isEmpty()){
			 			proposalDO.setProposalDate(CommonUtil.convertStringToSqlDateMonth(inputJSON.get(CommonConstants.PROPOSAL_DATE).toString()));
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.CURRENCY_TYPE) != null && !inputJSON.get(CommonConstants.CURRENCY_TYPE).toString().isEmpty()){
			 			List<CountryDO> countryList = countryService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.CURRENCY_TYPE).toString()));
			 			if(countryList != null && countryList.size() > 0){
			 				proposalDO.setCurrencyType(countryList.get(0));
			 			}
			 		}
			 		
			 		//proposalDO.setCurrencyType(inputJSON.get(CommonConstants.CURRENCY_TYPE) != null && !inputJSON.get(CommonConstants.CURRENCY_TYPE).toString().isEmpty()? inputJSON.get(CommonConstants.CURRENCY_TYPE).toString() : "");
			 		proposalDO.setRate(inputJSON.get(CommonConstants.RATE) != null && !inputJSON.get(CommonConstants.RATE).toString().isEmpty() ? inputJSON.get(CommonConstants.RATE).toString() : "");
			 		/*if(inputJSON.get(CommonConstants.PAYMENTTERM) != null && !inputJSON.get(CommonConstants.PAYMENTTERM).toString().isEmpty()){
			 			proposalDO.setPaymentTermId(Long.parseLong(!inputJSON.get(CommonConstants.PAYMENTTERM).toString().isEmpty() ? inputJSON.get(CommonConstants.PAYMENTTERM).toString() : ""));
			 		}*/
			 		if(inputJSON.get(CommonConstants.INVOICETERM) != null && !inputJSON.get(CommonConstants.INVOICETERM).toString().isEmpty()){
			 			proposalDO.setInvoiceTerm(inputJSON.get(CommonConstants.INVOICETERM).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.DESC) != null && !inputJSON.get(CommonConstants.DESC).toString().isEmpty()){
			 			proposalDO.setDescription(inputJSON.get(CommonConstants.DESC).toString());
			 		}
			 		
			 		/*if(!inputJSON.get(CommonConstants.SUPPLIERNAME).toString().isEmpty()){
			 			proposalDO.setSupplierName(inputJSON.get(CommonConstants.SUPPLIERNAME).toString());
			 		}*/
			 		
			 		/*if(inputJSON.get(CommonConstants.STARTDATE) != null && !inputJSON.get(CommonConstants.STARTDATE).toString().isEmpty()){
			 			proposalDO.setStartDate(inputJSON.get(CommonConstants.STARTDATE).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.ENDDATE) != null && !inputJSON.get(CommonConstants.ENDDATE).toString().isEmpty()){
			 			proposalDO.setEndDate(inputJSON.get(CommonConstants.ENDDATE).toString());
			 		}*/
			 		
			 		/*if(inputJSON.get(CommonConstants.DURATION) != null && !inputJSON.get(CommonConstants.DURATION).toString().isEmpty()){
			 			proposalDO.setDuration(Long.parseLong(inputJSON.get(CommonConstants.DURATION).toString()));
			 		}*/
			 		
			 		if(inputJSON.get(CommonConstants.PROJECTTYPE) != null && !inputJSON.get(CommonConstants.PROJECTTYPE).toString().isEmpty()){
			 			proposalDO.setProjectType(inputJSON.get(CommonConstants.PROJECTTYPE).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.EXPENSE) != null && !inputJSON.get(CommonConstants.EXPENSE).toString().isEmpty()){
			 			proposalDO.setExpenses(inputJSON.get(CommonConstants.EXPENSE).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.EXPENSE_TERM) != null && !inputJSON.get(CommonConstants.EXPENSE_TERM).toString().isEmpty()){
			 			proposalDO.setExpensesTerm(inputJSON.get(CommonConstants.EXPENSE_TERM).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.WORKLOCATION) != null && !inputJSON.get(CommonConstants.WORKLOCATION).toString().isEmpty()){
			 			proposalDO.setWorkLocation(inputJSON.get(CommonConstants.WORKLOCATION).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.WORKDESCRIPTION) != null && !inputJSON.get(CommonConstants.WORKDESCRIPTION).toString().isEmpty()){
			 			proposalDO.setWorkDesc(inputJSON.get(CommonConstants.WORKDESCRIPTION).toString());
			 		}
			 		
			 		/*if(inputJSON.get(CommonConstants.CUSTOMERCONTACT) != null && !inputJSON.get(CommonConstants.CUSTOMERCONTACT).toString().isEmpty()){
			 			proposalDO.setCustomerContact(Long.parseLong(inputJSON.get(CommonConstants.CUSTOMERCONTACT).toString()));
			 		} 
			 		
			 		if(inputJSON.get(CommonConstants.SUPPLIERCONTACT) != null && !inputJSON.get(CommonConstants.SUPPLIERCONTACT).toString().isEmpty()){
			 			proposalDO.setSupplierContact(Long.parseLong(inputJSON.get(CommonConstants.SUPPLIERCONTACT).toString()));
			 		}*/
			 		
			 		if(inputJSON.get(CommonConstants.CUSTOMERCONTACT) != null && !inputJSON.get(CommonConstants.CUSTOMERCONTACT).toString().isEmpty()){
			 			List<ContactDO> contactList = contactService.retriveById(Long.parseLong(inputJSON.get(CommonConstants.CUSTOMERCONTACT).toString()));
			 			if(contactList != null && contactList.size() > 0){
			 				proposalDO.setCustomerContact(contactList.get(0));
			 			}
			 		}
			 		if(inputJSON.get(CommonConstants.SUPPLIERCONTACT) != null && !inputJSON.get(CommonConstants.SUPPLIERCONTACT).toString().isEmpty()){
			 			List<EmployeeDO> empList = employeeService.retrieveEmpId(inputJSON.get(CommonConstants.SUPPLIERCONTACT).toString());
			 			if(empList != null && empList.size() > 0){
			 				proposalDO.setSupplierContact(empList.get(0));
			 			}
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			proposalDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			proposalDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			proposalDO.setEmployeeId(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		proposalDO.setUpdatedon(new Date());
			 		proposalDO.setCreatedon(new Date());
			 		proposalDO = proposalService.persist(proposalDO);
			 		if(inputJSON.get(CommonConstants.INVOICETERM_LIST) != null && !inputJSON.get(CommonConstants.INVOICETERM_LIST).toString().isEmpty()){
			 			List<InvoiceTermDO> invoiceTermList = new ArrayList<InvoiceTermDO>(); 
				 		JSONArray resultJSONArray = new JSONArray();
			 			resultJSONArray.put(inputJSON.get(CommonConstants.INVOICETERM_LIST));
				 		String rec = (String) resultJSONArray.get(0).toString();
				 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
				 		for (int i = 0; i < resultJSONArray1.size(); i++) {
				 			InvoiceTermDO invoiceTerm = new InvoiceTermDO(); 
				 			JSONObject inputJSON1 = (JSONObject) resultJSONArray1.get(i);
				 			invoiceTerm.setDescripetion(inputJSON1.get(CommonConstants.COMMENT).toString());
				 			invoiceTerm.setProjectPercentage(inputJSON1.get(CommonConstants.PERCENTAGE).toString());
				 			invoiceTerm.setProposal(proposalDO);
				 			invoiceTerm.setUpdatedon(new Date());
				 			invoiceTerm.setCreatedon(new Date());
				 			if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			invoiceTerm.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			invoiceTerm.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			invoiceTerm.setEmpId(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 		}
				 			invoiceTermList.add(invoiceTerm);
				 			
				 		}
				 		invoiceTermService.persist(invoiceTermList);
			 		}
			 		
			 		/*if(!inputJSON.get(CommonConstants.FIELD_LIST).toString().isEmpty()){
			 			List<ProposalDetailsDO> proposalDetailsList = new ArrayList<ProposalDetailsDO>(); 
				 		JSONArray resultJSONArray = new JSONArray();
			 			resultJSONArray.put(inputJSON.get(CommonConstants.FIELD_LIST));
				 		String rec = (String) resultJSONArray.get(0).toString();
				 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
				 		for (int i = 0; i < resultJSONArray1.size(); i++) {
				 			ProposalDetailsDO proposalDetails = new ProposalDetailsDO(); 
				 			JSONObject inputJSON1 = (JSONObject) resultJSONArray1.get(i);
				 			proposalDetails.setProposalKey(inputJSON1.get(CommonConstants.KEY).toString());
				 			proposalDetails.setProposalValue(inputJSON1.get(CommonConstants.VALUE).toString());
				 			proposalDetails.setProposalId(proposalDO.getId());
				 			proposalDetails.setUpdatedon(new Date());
				 			if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			proposalDetails.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 		}
				 			proposalDetailsList.add(proposalDetails);
				 			
				 		}
				 		proposalDetailsService.persist(proposalDetailsList);
			 		}
			 		
			 		if(!inputJSON.get(CommonConstants.DEFAULT_LIST).toString().isEmpty()){
			 			List<ProposalDefaultDO> proposalDetailsList = new ArrayList<ProposalDefaultDO>(); 
				 		JSONArray resultJSONArray = new JSONArray();
			 			resultJSONArray.put(inputJSON.get(CommonConstants.DEFAULT_LIST));
				 		String rec = (String) resultJSONArray.get(0).toString();
				 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
				 		for (int i = 0; i < resultJSONArray1.size(); i++) {
				 			ProposalDefaultDO proposalDefault = new ProposalDefaultDO(); 
				 			JSONObject inputJSON1 = (JSONObject) resultJSONArray1.get(i);
				 			proposalDefault.setProposalKey(inputJSON1.get(CommonConstants.KEY).toString());
				 			proposalDefault.setProposalValue(inputJSON1.get(CommonConstants.VALUE).toString());
				 			proposalDefault.setProposalId(proposalDO.getId());
				 			proposalDefault.setUpdatedon(new Date());
				 			if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			proposalDefault.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 		}
				 			proposalDetailsList.add(proposalDefault);
				 			
				 		}
				 		proposalDefaultService.persist(proposalDetailsList);
			 		}*/
			 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Proposal Created");
			 		
			 		/*if(!inputJSON.get(CommonConstants.FILE).toString().isEmpty()){
			 			List<ProposalDocumentsDO> proposalDocumentList = new ArrayList<ProposalDocumentsDO>(); 
				 		JSONArray resultJSONArray = new JSONArray();
			 			resultJSONArray.put(inputJSON.get(CommonConstants.FILE));
				 		String rec = (String) resultJSONArray.get(0).toString();
				 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
				 		for (int i = 0; i < resultJSONArray1.size(); i++) {
				 			ProposalDocumentsDO proposalDocuments = new ProposalDocumentsDO(); 
				 			JSONObject inputJSON1 = (JSONObject) resultJSONArray1.get(i);
				 			proposalDocuments.setProposalDocuments(inputJSON1.get(CommonConstants.VALUE).toString());
				 			proposalDocuments.setProposalId(proposalDO.getId());
				 			proposalDocuments.setUpdatedon(new Date());
				 			if(userServiceList != null && userServiceList.size() > 0){
				 				proposalDocuments.setUpdatedby(userServiceList.get(0).getName());
				 			}
				 			proposalDocumentList.add(proposalDocuments);
				 			
				 		}
				 		new ProposalDocumentsService().persist(proposalDocumentList);
			 		}*/
			 	}
				
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retriveById/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveById(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<ProposalDO> proposalList = proposalService.retriveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = ProposalUtil.getProposalList(proposalList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {

		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				int i = 0;
				List<Long> deptIds = new ArrayList<Long>();
				if(request.getSession().getAttribute("deptIds") != null){
					deptIds = (List<Long>) request.getSession().getAttribute("deptIds");
				}
				List<String> empIds = new ArrayList<String>();
				if(deptIds != null && deptIds.size() > 0){
					empIds = employeeService.retrieveByDeptIds(deptIds);
					/*List<String> newList = employeeService.retrieveByDeptIds(deptIds);
					for (String deptId : newList) { 
						empIds.add(String.valueOf(deptId)); 
					}*/
				}else if(request.getSession().getAttribute("empIds") != null){
					empIds = new ArrayList<String>();
					String empid = (request.getSession().getAttribute("empIds").toString());
					empIds.add(empid);
					List<UserDO> userList = userService.retriveByEmpId(empid.toString());
					if(userList.get(0).getRole().getRoleId() != null){
						List<RolesDO> roleList = rolesService.retriveById(userList.get(0).getRole().getRoleId());
						if(roleList != null && roleList.size() > 0){
							if(roleList.get(0).getName().equalsIgnoreCase(CommonConstants.SUPERADMIN)){
								i=1;
							}
						}else{
							empIds = new ArrayList<String>();
						}
					}else{
						empIds = new ArrayList<String>();
					}
				}
				if(i==1){
					List<ProposalDO> proposalList = proposalService.retrieveAll();
					respJSON = ProposalUtil.getProposalList(proposalList).toString();
				}else{
					/*List<ProposalDO> proposalList = proposalService.retrieveAll();
					respJSON = ProposalUtil.getProposalList(proposalList).toString();*/
					List<ProposalDO> proposalList = proposalService.retrieve(empIds);
					respJSON = ProposalUtil.getProposalList(proposalList).toString();
				}
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveByCustomerId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByCustomerId(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<ProposalDO> ProposalDetail = proposalService.retrieveCustomerId(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = ProposalUtil.getProposalList(ProposalDetail).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				ProposalDO proposalDO = new ProposalDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		
			 		List<ProposalDO> proposalList = proposalService.retriveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(proposalList != null && proposalList.size() > 0){
			 			proposalDO = proposalList.get(0);
				 		
			 			if(inputJSON.get(CommonConstants.CUSTOMERID) != null && !inputJSON.get(CommonConstants.CUSTOMERID).toString().isEmpty()){
				 			List<LeadDO> customerList = leadService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.CUSTOMERID).toString()));
				 			if(customerList != null && customerList.size() > 0){
				 				proposalDO.setCustomer(customerList.get(0));
				 			}
				 		}
				 		if(inputJSON.get(CommonConstants.PROJECTID) != null && !inputJSON.get(CommonConstants.PROJECTID).toString().isEmpty()){
				 			List<OpportunitiesDO> oppList = opportunitiesService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.PROJECTID).toString()));
				 			if(oppList != null && oppList.size() > 0){
				 				proposalDO.setProject(oppList.get(0));
				 			}
				 		}
				 		if(inputJSON.get(CommonConstants.PAYMENTTERM) != null && !inputJSON.get(CommonConstants.PAYMENTTERM).toString().isEmpty()){
				 			List<PaymentTermDO> paymentTermList = paymentTermService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.PAYMENTTERM).toString()));
				 			if(paymentTermList != null && paymentTermList.size() > 0){
				 				proposalDO.setPaymentTerm(paymentTermList.get(0));
				 			}
				 		}
				 		/*if(!inputJSON.get(CommonConstants.PROJECTTYPE).toString().isEmpty()){
				 			proposalDO.setProjectTypeId(Long.parseLong(inputJSON.get(CommonConstants.PROJECTTYPE).toString()));
				 		}*/
				 		
				 		if(inputJSON.get(CommonConstants.QUANTITY) != null && !inputJSON.get(CommonConstants.QUANTITY).toString().isEmpty()){
				 			proposalDO.setQunatity(Long.parseLong(inputJSON.get(CommonConstants.QUANTITY).toString()));
				 		}
				 		if(inputJSON.get(CommonConstants.PROPOSAL_NAME) != null && !inputJSON.get(CommonConstants.PROPOSAL_NAME).toString().isEmpty()){
				 			proposalDO.setProposalName(inputJSON.get(CommonConstants.PROPOSAL_NAME).toString());
				 		}
				 		
				 		if(inputJSON.get(CommonConstants.PROPOSAL_DATE) != null && !inputJSON.get(CommonConstants.PROPOSAL_DATE).toString().isEmpty()){
				 			proposalDO.setProposalDate(CommonUtil.convertStringToSqlDateMonth(inputJSON.get(CommonConstants.PROPOSAL_DATE).toString()));
				 		}
				 		if(inputJSON.get(CommonConstants.CURRENCY_TYPE) != null && !inputJSON.get(CommonConstants.CURRENCY_TYPE).toString().isEmpty()){
				 			List<CountryDO> countryList = countryService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.CURRENCY_TYPE).toString()));
				 			if(countryList != null && countryList.size() > 0){
				 				proposalDO.setCurrencyType(countryList.get(0));
				 			}
				 		}
				 		
				 		//proposalDO.setCurrencyType(inputJSON.get(CommonConstants.CURRENCY_TYPE) != null && !inputJSON.get(CommonConstants.CURRENCY_TYPE).toString().isEmpty()? inputJSON.get(CommonConstants.CURRENCY_TYPE).toString() : "");
				 		proposalDO.setRate(inputJSON.get(CommonConstants.RATE) != null && !inputJSON.get(CommonConstants.RATE).toString().isEmpty() ? inputJSON.get(CommonConstants.RATE).toString() : "");
				 		/*if(!inputJSON.get(CommonConstants.PAYMENTTERM).toString().isEmpty()){
				 			proposalDO.setPaymentTermId(Long.parseLong(!inputJSON.get(CommonConstants.PAYMENTTERM).toString().isEmpty() ? inputJSON.get(CommonConstants.PAYMENTTERM).toString() : ""));
				 		}*/
				 		if(inputJSON.get(CommonConstants.INVOICETERM) != null && !inputJSON.get(CommonConstants.INVOICETERM).toString().isEmpty()){
				 			proposalDO.setInvoiceTerm(inputJSON.get(CommonConstants.INVOICETERM).toString());
				 		}
				 		
				 		if(inputJSON.get(CommonConstants.DESC) != null && !inputJSON.get(CommonConstants.DESC).toString().isEmpty()){
				 			proposalDO.setDescription(inputJSON.get(CommonConstants.DESC).toString());
				 		}
				 		
				 		if(inputJSON.get(CommonConstants.SUPPLIERNAME) != null && !inputJSON.get(CommonConstants.SUPPLIERNAME).toString().isEmpty()){
				 			proposalDO.setSupplierName(inputJSON.get(CommonConstants.SUPPLIERNAME).toString());
				 		}
				 		
				 		/*if(inputJSON.get(CommonConstants.STARTDATE) != null && !inputJSON.get(CommonConstants.STARTDATE).toString().isEmpty()){
				 			proposalDO.setStartDate(inputJSON.get(CommonConstants.STARTDATE).toString());
				 		}
				 		
				 		if(inputJSON.get(CommonConstants.ENDDATE) != null && !inputJSON.get(CommonConstants.ENDDATE).toString().isEmpty()){
				 			proposalDO.setEndDate(inputJSON.get(CommonConstants.ENDDATE).toString());
				 		}*/
				 		
				 		/*if(inputJSON.get(CommonConstants.DURATION) != null && !inputJSON.get(CommonConstants.DURATION).toString().isEmpty()){
				 			proposalDO.setDuration(Long.parseLong(inputJSON.get(CommonConstants.DURATION).toString()));
				 		}*/
				 		
				 		if(inputJSON.get(CommonConstants.PROJECTTYPE) != null && !inputJSON.get(CommonConstants.PROJECTTYPE).toString().isEmpty()){
				 			proposalDO.setProjectType(inputJSON.get(CommonConstants.PROJECTTYPE).toString());
				 		}
				 		
				 		/*if(inputJSON.get(CommonConstants.EXPENSE) != null && !inputJSON.get(CommonConstants.EXPENSE).toString().isEmpty()){
				 			proposalDO.setExpenses(inputJSON.get(CommonConstants.EXPENSE).toString());
				 		}
				 		
				 		if(inputJSON.get(CommonConstants.EXPENSE_TERM) != null && !inputJSON.get(CommonConstants.EXPENSE_TERM).toString().isEmpty()){
				 			proposalDO.setExpensesTerm(inputJSON.get(CommonConstants.EXPENSE_TERM).toString());
				 		}*/
				 		
				 		if(inputJSON.get(CommonConstants.WORKLOCATION) != null && !inputJSON.get(CommonConstants.WORKLOCATION).toString().isEmpty()){
				 			proposalDO.setWorkLocation(inputJSON.get(CommonConstants.WORKLOCATION).toString());
				 		}
				 		
				 		if(inputJSON.get(CommonConstants.WORKDESCRIPTION) != null && !inputJSON.get(CommonConstants.WORKDESCRIPTION).toString().isEmpty()){
				 			proposalDO.setWorkDesc(inputJSON.get(CommonConstants.WORKDESCRIPTION).toString());
				 		}
				 		
				 		if(inputJSON.get(CommonConstants.CUSTOMERCONTACT) != null && !inputJSON.get(CommonConstants.CUSTOMERCONTACT).toString().isEmpty()){
				 			List<ContactDO> contactList = contactService.retriveById(Long.parseLong(inputJSON.get(CommonConstants.CUSTOMERCONTACT).toString()));
				 			if(contactList != null && contactList.size() > 0){
				 				proposalDO.setCustomerContact(contactList.get(0));
				 			}
				 		}
				 		if(inputJSON.get(CommonConstants.SUPPLIERCONTACT) != null && !inputJSON.get(CommonConstants.SUPPLIERCONTACT).toString().isEmpty()){
				 			List<EmployeeDO> empList = employeeService.retrieveEmpId(inputJSON.get(CommonConstants.SUPPLIERCONTACT).toString());
				 			if(empList != null && empList.size() > 0){
				 				proposalDO.setSupplierContact(empList.get(0));
				 			}
				 		}
				 		/*if(inputJSON.get(CommonConstants.CUSTOMERCONTACT) != null && !inputJSON.get(CommonConstants.CUSTOMERCONTACT).toString().isEmpty()){
				 			proposalDO.setCustomerContact(Long.parseLong(inputJSON.get(CommonConstants.CUSTOMERCONTACT).toString()));
				 		} 
				 		
				 		if(inputJSON.get(CommonConstants.SUPPLIERCONTACT) != null && !inputJSON.get(CommonConstants.SUPPLIERCONTACT).toString().isEmpty()){
				 			proposalDO.setSupplierContact(Long.parseLong(inputJSON.get(CommonConstants.SUPPLIERCONTACT).toString()));
				 		}*/
				 		
				 		
				 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			proposalDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		proposalDO.setUpdatedon(new Date());
				 		proposalDO = proposalService.update(proposalDO);
				 		proposalDocumentsService.delete(proposalDO.getProposalId());
				 		List<InvoiceTermDO> invoiceTermList = new ArrayList<InvoiceTermDO>();
				 		if(!inputJSON.get(CommonConstants.INVOICETERM_LIST).toString().isEmpty()){
					 		JSONArray resultJSONArray = new JSONArray();
				 			resultJSONArray.put(inputJSON.get(CommonConstants.INVOICETERM_LIST));
					 		String rec = (String) resultJSONArray.get(0).toString();
					 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
					 		for (int i = 0; i < resultJSONArray1.size(); i++) {
					 			InvoiceTermDO invoiceTerm = new InvoiceTermDO(); 
					 			JSONObject inputJSON1 = (JSONObject) resultJSONArray1.get(i);
					 			invoiceTerm.setDescripetion(inputJSON1.get(CommonConstants.COMMENT).toString());
					 			invoiceTerm.setProjectPercentage(inputJSON1.get(CommonConstants.PERCENTAGE).toString());
					 			invoiceTerm.setProposal(proposalDO);
					 			invoiceTerm.setUpdatedon(new Date());
					 			if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
						 			proposalDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
						 		}
					 			invoiceTermList.add(invoiceTerm);
					 			
					 		}
					 		invoiceTermService.persist(invoiceTermList);
				 		}else{
				 			invoiceTermService.delete(proposalDO.getProposalId());
				 		}
				 		
				 		/*List<ProposalDetailsDO> proposalDetailsList = new ArrayList<ProposalDetailsDO>(); 
				 		if(!inputJSON.get(CommonConstants.FIELD_LIST).toString().isEmpty()){
					 		JSONArray resultJSONArray = new JSONArray();
				 			resultJSONArray.put(inputJSON.get(CommonConstants.FIELD_LIST));
					 		String rec = (String) resultJSONArray.get(0).toString();
					 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
					 		for (int i = 0; i < resultJSONArray1.size(); i++) {
					 			ProposalDetailsDO proposalDetails = new ProposalDetailsDO(); 
					 			JSONObject inputJSON1 = (JSONObject) resultJSONArray1.get(i);
					 			proposalDetails.setProposalKey(inputJSON1.get(CommonConstants.KEY).toString());
					 			proposalDetails.setProposalValue(inputJSON1.get(CommonConstants.VALUE).toString());
					 			proposalDetails.setProposalId(proposalDO.getId());
					 			proposalDetails.setUpdatedon(new Date());
					 			if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
						 			proposalDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
						 		}
					 			proposalDetailsList.add(proposalDetails);
					 		}
					 		proposalDetailsService.persist(proposalDetailsList);
				 		}else{
				 			proposalDetailsService.delete(proposalDO.getId());
				 		}
				 		
				 		List<ProposalDefaultDO> proposalDefaultList = new ArrayList<ProposalDefaultDO>(); 
				 		if(!inputJSON.get(CommonConstants.DEFAULT_LIST).toString().isEmpty()){
					 		JSONArray resultJSONArray = new JSONArray();
				 			resultJSONArray.put(inputJSON.get(CommonConstants.DEFAULT_LIST));
					 		String rec = (String) resultJSONArray.get(0).toString();
					 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
					 		for (int i = 0; i < resultJSONArray1.size(); i++) {
					 			ProposalDefaultDO proposalDefault = new ProposalDefaultDO(); 
					 			JSONObject inputJSON1 = (JSONObject) resultJSONArray1.get(i);
					 			proposalDefault.setProposalKey(inputJSON1.get(CommonConstants.KEY).toString());
					 			proposalDefault.setProposalValue(inputJSON1.get(CommonConstants.VALUE).toString());
					 			proposalDefault.setProposalId(proposalDO.getId());
					 			proposalDefault.setUpdatedon(new Date());
					 			if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
						 			proposalDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
						 		}
					 			proposalDefaultList.add(proposalDefault);
					 		}
					 		proposalDefaultService.persist(proposalDefaultList);
				 		}else{
				 			proposalDefaultService.delete(proposalDO.getId());
				 		}*/
				 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Proposal Updated");
				 		/*if(!inputJSON.get(CommonConstants.FILE).toString().isEmpty()){
				 			List<ProposalDocumentsDO> proposalDocumentList = new ArrayList<ProposalDocumentsDO>(); 
					 		JSONArray resultJSONArray = new JSONArray();
				 			resultJSONArray.put(inputJSON.get(CommonConstants.FILE));
					 		String rec = (String) resultJSONArray.get(0).toString();
					 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
					 		for (int i = 0; i < resultJSONArray1.size(); i++) {
					 			ProposalDocumentsDO proposalDocuments = new ProposalDocumentsDO(); 
					 			JSONObject inputJSON1 = (JSONObject) resultJSONArray1.get(i);
					 			proposalDocuments.setProposalDocuments(inputJSON1.get(CommonConstants.VALUE).toString());
					 			proposalDocuments.setProposalId(proposalDO.getId());
					 			proposalDocuments.setUpdatedon(new Date());
					 			if(userServiceList != null && userServiceList.size() > 0){
					 				proposalDocuments.setUpdatedby(userServiceList.get(0).getName());
					 			}
					 			proposalDocumentList.add(proposalDocuments);
					 			
					 		}
					 		new ProposalDocumentsService().persist(proposalDocumentList);
				 		}*/
				 		
			 		}
			 	}

			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/updatepic", method = RequestMethod.POST)
	public @ResponseBody String updatepic(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				ProposalDocumentsDO proposalDocumentsDO = new ProposalDocumentsDO();
				/*JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("data").toString());*/
			 	if (request.getParameter("file") != null && request.getParameter("id") != null){
			 		proposalDocumentsDO.setProposalId(Long.parseLong(request.getParameter("id")));
			 		proposalDocumentsDO.setPhoto(request.getParameter("file"));
			 		proposalDocumentsDO.setFileName(request.getParameter("name"));
			 		proposalDocumentsService.persist(proposalDocumentsDO);
 			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
