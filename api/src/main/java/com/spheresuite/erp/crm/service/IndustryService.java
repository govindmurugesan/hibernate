package com.spheresuite.erp.crm.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.crm.dao.IndustryDAO;
import com.spheresuite.erp.domainobject.IndustryDO;
@Service
@Transactional
public class IndustryService {
	static Logger logger = Logger.getLogger(IndustryService.class.getName());
	@Autowired
	private IndustryDAO industryDAO;

	@Transactional
	public boolean persist(IndustryDO industryDO) {
		return industryDAO.persist(industryDO);
	}

	@Transactional
	public List<IndustryDO> retrieveActive() {
		return industryDAO.retrieveActive();
	}

	@Transactional
	public List<IndustryDO> retrieveById(Long Id) {
		return industryDAO.retrieveById(Id);
	}
	
	@Transactional
	public List<IndustryDO> retrieveByActiveName(String name) {
		return industryDAO.retrieveByActiveName(name);
	}

	@Transactional
	public List<IndustryDO> retrieve() {
		return industryDAO.retrieve();
	}

	@Transactional
	public boolean update(IndustryDO industryDO) {
		return industryDAO.update(industryDO);
	}
	
	@Transactional
	public boolean persistList(List<IndustryDO> industryDO) {
		return industryDAO.persistList(industryDO);
	}
}
