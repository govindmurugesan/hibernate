package com.spheresuite.erp.crm.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.NotesDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class NotesDAO {
	static Logger logger = Logger.getLogger(NotesDAO.class.getName());
	
	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private GenericDAOImpl<NotesDO> genericObject;
	
	public NotesDO persist(NotesDO noteDO) {
		try {
			genericObject.persist(noteDO);
		} catch (Exception eException) {
			//throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			//SessionManager.closeEntityManager(em);
		}
		return noteDO;
	}
	
	public NotesDO update(NotesDO noteDO) {
		try {
			genericObject.merge(noteDO);
		} catch (Exception eException) {
		} finally {
		}
		return noteDO;
	}
	
	public List<NotesDO> retrieve() {
		List<NotesDO> noteList = null;
		try {
			noteList = genericObject.retrieve(NotesDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return noteList;
	}
	
	@SuppressWarnings("unchecked")
	public List<NotesDO> retrieveById(Long Id) {
		List<NotesDO> noteList = null;
		try {
			noteList =  this.sessionFactory.getCurrentSession().getNamedQuery(NotesDO.FIND_BY_ID)
						.setParameter(CommonConstants.ID, Id).list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return noteList;
	}
	
	@SuppressWarnings("unchecked")
	public List<NotesDO> retrieveByLeadId(Long Id) {
		List<NotesDO> noteList = null;
		try {
			noteList = this.sessionFactory.getCurrentSession().getNamedQuery(NotesDO.FIND_BY_LEAD_ID)
										.setParameter(CommonConstants.ID, Id)
										.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return noteList;
	}
	
	@SuppressWarnings("unchecked")
	public List<NotesDO> retrieveByLeadIdAndNote(Long leadId,String note) {
		List<NotesDO> noteList = null;
		try {
			noteList = this.sessionFactory.getCurrentSession().getNamedQuery(NotesDO.FIND_BY_LEAD_AND_NOTE)
										.setParameter(CommonConstants.ID, leadId)
										.setParameter(CommonConstants.NOTE, note)
										.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return noteList;
	}
	
	@SuppressWarnings("unchecked")
	public List<NotesDO> retrieveByLeadRecentNote(Long Id) {
		List<NotesDO> noteList = null;
		try {
			noteList = this.sessionFactory.getCurrentSession().getNamedQuery(NotesDO.FIND_BY_LEAD_RECENT_NOTE)
										.setParameter(CommonConstants.ID, Id)
										.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return noteList;
	}
}
