package com.spheresuite.erp.crm.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.EventsDO;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;

@Component
public class EventsUtil {
	
	private EventsUtil() {}
	
	public static JSONObject getEventsList(List<EventsDO> eventList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EventsDO event : eventList) {
				resultJSONArray.put(getMOMDetailObject(event));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getMOMDetailObject(EventsDO event)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(event.getEventId() != null ? event.getEventId() : ""));
		result.put(CommonConstants.EMPID, String.valueOf(event.getEmpId() != null ? event.getEmpId() : ""));
		result.put(CommonConstants.NAME, String.valueOf(event.getName() != null ? event.getName() : ""));
		result.put(CommonConstants.COUNTRY, String.valueOf(event.getCountry() != null ? event.getCountry() : ""));
		result.put(CommonConstants.ADDRESS, String.valueOf(event.getAddress() != null ? event.getAddress() : ""));
		result.put(CommonConstants.FROMDATE, String.valueOf(event.getFromDate() != null ? event.getFromDate() : ""));
		result.put(CommonConstants.TODATE, String.valueOf(event.getToDate() != null ? event.getToDate() : ""));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(event.getUpdatedon())));
		if(event.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(event.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		return result;
	}
	
}
