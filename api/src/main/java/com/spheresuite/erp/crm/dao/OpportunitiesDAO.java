package com.spheresuite.erp.crm.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.OpportunitiesDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class OpportunitiesDAO {
	static Logger logger = Logger.getLogger(OpportunitiesDAO.class.getName());

	@Autowired
	private GenericDAOImpl<OpportunitiesDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public OpportunitiesDO persist(OpportunitiesDO projectDO) {
		try {
			genericObject.persist(projectDO);
		} catch (Exception eException) {
			//throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			//SessionManager.closeEntityManager(em);
		}
		return projectDO;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<OpportunitiesDO> retrieve(List<String> Id) {
		List<OpportunitiesDO> projectList = null;
		try {
			projectList = this.sessionFactory.getCurrentSession().getNamedQuery(OpportunitiesDO.FIND_ALL)
										.setParameterList(CommonConstants.ID, Id)
										.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return projectList;
	}
	
	public List<OpportunitiesDO> retrieveAll() {
		List<OpportunitiesDO> projectList = null;
		try {
			projectList = genericObject.retrieve(OpportunitiesDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return projectList;
	}
	
	public Long retrieveForCustomer(List<Long> ids) {
		Long projectList = null;
		try {
			projectList = (Long) this.sessionFactory.getCurrentSession().getNamedQuery(OpportunitiesDO.FIND_SUM_CUSTOMER)
										.setParameter(CommonConstants.ID, ids)
										.uniqueResult();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return projectList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<OpportunitiesDO> retrieveById(Long Id) {
		List<OpportunitiesDO> projectList = null;
		try {
			projectList =  this.sessionFactory.getCurrentSession().getNamedQuery(OpportunitiesDO.FIND_BY_ID)
								.setParameter(CommonConstants.ID, Id).list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return projectList;
	}
	
	@SuppressWarnings("unchecked")
	public List<OpportunitiesDO> retrieveByContactId(Long Id) {
		List<OpportunitiesDO> projectList = null;
		try {
			projectList = this.sessionFactory.getCurrentSession().getNamedQuery(OpportunitiesDO.FIND_BY_CONTACT)
										.setParameter(CommonConstants.ID, Id)
										.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return projectList;
	}
	
	public Long retrieveBySalesStageId(Long Id) {
		Long projectList = null;
		try {
			projectList = (Long) this.sessionFactory.getCurrentSession().getNamedQuery(OpportunitiesDO.FIND_BY_SALES_STAGE_ID)
										.setParameter(CommonConstants.ID, Id)
										.uniqueResult();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return projectList;
	}
	
	@SuppressWarnings("unchecked")
	public List<OpportunitiesDO> retrieveByCustomerId(Long Id) {
		List<OpportunitiesDO> projectList = null;
		try {
			projectList = this.sessionFactory.getCurrentSession().getNamedQuery(OpportunitiesDO.FIND_BY_CUSTOMER)
										.setParameter(CommonConstants.ID, Id)
										.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return projectList;
	}
	
	
	
	public OpportunitiesDO update(OpportunitiesDO ProjectDO) {
		try {
			genericObject.merge(ProjectDO);
		} catch (Exception eException) {
		} finally {
		}
		return ProjectDO;
	}
	
	public boolean persistList(List<OpportunitiesDO> projectDO) {
		try {
			for (OpportunitiesDO projectDO2 : projectDO) {
				genericObject.persist(projectDO2);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}

}
