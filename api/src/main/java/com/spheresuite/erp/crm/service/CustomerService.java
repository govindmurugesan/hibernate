package com.spheresuite.erp.crm.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.crm.dao.CustomerDAO;
import com.spheresuite.erp.domainobject.CustomerDO;
@Service
@Transactional
public class CustomerService {
	static Logger logger = Logger.getLogger(CustomerService.class.getName());
	@Autowired
	private CustomerDAO customerDAO;

	@Transactional
	public CustomerDO persist(CustomerDO customerDO)  {
		return customerDAO.persist(customerDO);
	}

	@Transactional
	public List<CustomerDO> retrieveById(Long Id)  {
		return customerDAO.retrieveById(Id);
	}

	@Transactional
	public List<CustomerDO> retrieveByActive()  {
		return customerDAO.retrieveByActive();
	}

	@Transactional
	public List<CustomerDO> retrieve()  {
		return customerDAO.retrieve();
	}

	@Transactional
	public CustomerDO update(CustomerDO customerDO)  {
		return customerDAO.update(customerDO);
	}

	@Transactional
	public boolean persistList(List<CustomerDO> customerDO)  {
		return customerDAO.persistList(customerDO);
	}
}
