package com.spheresuite.erp.crm.web.util;

import java.util.List;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.crm.service.ContactService;
import com.spheresuite.erp.crm.service.InvoiceTermService;
import com.spheresuite.erp.crm.service.LeadService;
import com.spheresuite.erp.crm.service.OpportunitiesService;
import com.spheresuite.erp.crm.service.OpportunityTypeService;
import com.spheresuite.erp.crm.service.PaymentTermService;
import com.spheresuite.erp.domainobject.CountryDO;
import com.spheresuite.erp.domainobject.InvoiceTermDO;
import com.spheresuite.erp.domainobject.OpportunityTypeDO;
import com.spheresuite.erp.domainobject.ProposalDO;
import com.spheresuite.erp.domainobject.ProposalDefaultDO;
import com.spheresuite.erp.domainobject.ProposalDetailsDO;
import com.spheresuite.erp.service.CountryService;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;

@Component
public class ProposalUtil {
	
	private ProposalUtil() {}
	
	@Autowired
	private EmployeeService employeeServiceTemp;
	
	@Autowired
	private LeadService leadServiceTemp;
	
	@Autowired
	private OpportunitiesService opportunitiesServiceTemp;
	
	@Autowired
	private OpportunityTypeService projectTypeServiceTemp;
	
	@Autowired
	private CountryService countryServiceTemp;
	
	@Autowired
	private PaymentTermService paymentTermServiceTemp;
	
	@Autowired
	private InvoiceTermService invoiceTermServiceTemp;
	
	@Autowired
	private ContactService contactServiceTemp;
	
	/*@Autowired
	private ProposalDetailsService proposalDetailsServiceTemp;
	
	@Autowired
	private ProposalDefaultService proposalDefaultServiceTemp;*/
	
	private static EmployeeService employeeService;
	
	private static LeadService leadService;
	
	private static OpportunitiesService opportunitiesService;
	
	private static OpportunityTypeService projectTypeService;
	
	private static CountryService countryService;
	
	private static PaymentTermService paymentTermService;
	
	private static InvoiceTermService invoiceTermService;
	
	private static ContactService contactService;
	
	/*private static ProposalDetailsService proposalDetailsService;
	
	private static ProposalDefaultService proposalDefaultService;*/
	
	@PostConstruct
	public void init() {
		employeeService = this.employeeServiceTemp;
		leadService = this.leadServiceTemp;
		opportunitiesService = this.opportunitiesServiceTemp;
		projectTypeService = this.projectTypeServiceTemp;
		countryService = this.countryServiceTemp;
		paymentTermService = this.paymentTermServiceTemp;
		invoiceTermService = this.invoiceTermServiceTemp;
		contactService = this.contactServiceTemp;
		/*proposalDetailsService = this.proposalDetailsServiceTemp;
		proposalDefaultService = this.proposalDefaultServiceTemp;*/
	}
	
	public static JSONObject getProposalList(List<ProposalDO> proposalList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (ProposalDO proposal : proposalList) {
				resultJSONArray.put(getProposalDetailObject(proposal));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	
	public static JSONObject getProposalDetailObject(ProposalDO proposal)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(proposal.getProposalId()));
		if(proposal.getCustomer() != null){
			result.put(CommonConstants.CUSTOMERID, String.valueOf(proposal.getCustomer().getLeadId()));
			result.put(CommonConstants.NAME, String.valueOf(proposal.getCustomer().getName()));
		}else{
			result.put(CommonConstants.CUSTOMERID, "");
			result.put(CommonConstants.NAME, "");
		}
		
		if(proposal.getProject() != null){
			result.put(CommonConstants.PROJECTID, String.valueOf(proposal.getProject().getOpportunityId()));
			result.put(CommonConstants.PROJECTNAME, proposal.getProject().getProjectname());
		}else{
			result.put(CommonConstants.PROJECTID, "");
			result.put(CommonConstants.PROJECTNAME,"");
		}

		if(proposal.getProjectTypeId() != null){
			result.put(CommonConstants.PROJECTTYPE, String.valueOf(proposal.getProjectTypeId()));
			List<OpportunityTypeDO> projectTypeDOs = projectTypeService.retrieveById(proposal.getProjectTypeId());
			if(projectTypeDOs != null && projectTypeDOs.size() >0){
				result.put(CommonConstants.PROJECT_TYPE_NAME, String.valueOf(projectTypeDOs.get(0).getName()));
			}else{
				result.put(CommonConstants.PROJECT_TYPE_NAME, "");
			}
		}else{
			result.put(CommonConstants.PROJECTTYPE, "");
			result.put(CommonConstants.PROJECT_TYPE_NAME, "");
		}
		result.put(CommonConstants.CURRENCYTYPE, String.valueOf(proposal.getCurrencyType() != null ? proposal.getCurrencyType() : ""));
		if(proposal.getCurrencyType() != null){
			result.put(CommonConstants.CURRENCYTYPE,proposal.getCurrencyType().getCountryId());
			result.put(CommonConstants.CURRENCYTYPE_NAME,proposal.getCurrencyType().getCurrencyType());
		}else{
			result.put(CommonConstants.CURRENCYTYPE_NAME, "");
			result.put(CommonConstants.CURRENCYTYPE, "");
		}
		result.put(CommonConstants.RATE, String.valueOf(proposal.getRate() != null ? proposal.getRate() : ""));
		result.put(CommonConstants.QUANTITY, String.valueOf(proposal.getQunatity() != null ? proposal.getQunatity() : ""));
		
		if(proposal.getPaymentTerm() != null){
			result.put(CommonConstants.PAYMENTTERM, String.valueOf(proposal.getPaymentTerm().getPaymentTermId()));
			result.put(CommonConstants.PAYMENTTERM_NAME, proposal.getPaymentTerm().getName());
		}else{
			result.put(CommonConstants.PAYMENTTERM, "");
		}
		result.put(CommonConstants.INVOICETERM, String.valueOf(proposal.getInvoiceTerm() != null ? proposal.getInvoiceTerm(): ""));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(proposal.getUpdatedon())));
		/*if(proposal.getUpdatedby() != null){
			List<UserDO> userList = userService.retriveById(Long.parseLong(proposal.getUpdatedby()));
			if(userList != null && userList.size() > 0){
				result.put(CommonConstants.UPDATED_BY, String.valueOf(userList.get(0).getName())); 
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
			
			List<EmployeeDO> empList = employeeService.retriveByEmpId(Long.parseLong(proposal.getUpdatedby()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
			
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}*/
		
		if(proposal.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(proposal.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		JSONArray resultInvoiceTerm = new JSONArray();
		List<InvoiceTermDO> invoicetermList = invoiceTermService.retrieveById(proposal.getProposalId());
		if(invoicetermList != null && invoicetermList.size() > 0){
			for (InvoiceTermDO invoiceTerm : invoicetermList) {
				resultInvoiceTerm.put(getInvoiceTermObject(invoiceTerm));
			}
			result.put(CommonConstants.INVOICETERM_LIST, resultInvoiceTerm);
		}else{
			result.put(CommonConstants.INVOICETERM_LIST, "");
		}
		
		if(proposal.getDescription() != null){
			result.put(CommonConstants.DESC, String.valueOf(proposal.getDescription()));
		}else{
			result.put(CommonConstants.DESC, "");
		}
		
		if(proposal.getSupplierName() != null){
			result.put(CommonConstants.SUPPLIERNAME, String.valueOf(proposal.getSupplierName()));
		}else{
			result.put(CommonConstants.SUPPLIERNAME, "");
		}
		
		/*if(proposal.getStartDate() != null){
			result.put(CommonConstants.STARTDATE, String.valueOf(proposal.getStartDate()));
		}else{
			result.put(CommonConstants.STARTDATE, "");
		}
		
		if(proposal.getEndDate() != null){
			result.put(CommonConstants.ENDDATE, String.valueOf(proposal.getEndDate()));
		}else{
			result.put(CommonConstants.ENDDATE, "");
		}*/
		
		if(proposal.getProposalDate() != null){
			result.put(CommonConstants.PROPOSAL_DATE, String.valueOf(proposal.getProposalDate()));
		}else{
			result.put(CommonConstants.PROPOSAL_DATE, "");
		}
		
		if(proposal.getProposalName() != null){
			result.put(CommonConstants.PROPOSAL_NAME, String.valueOf(proposal.getProposalName()));
		}else{
			result.put(CommonConstants.PROPOSAL_NAME, "");
		}
		
		/*if(proposal.getDuration() != null){
			result.put(CommonConstants.DURATION, String.valueOf(proposal.getDuration()));
		}else{
			result.put(CommonConstants.DURATION, "");
		}*/
		
		if(proposal.getProjectType() != null){
			result.put(CommonConstants.PROJECTTYPE, String.valueOf(proposal.getProjectType()));
		}else{
			result.put(CommonConstants.PROJECTTYPE, "");
		}
		
		/*if(proposal.getExpenses() != null){
			result.put(CommonConstants.EXPENSE, String.valueOf(proposal.getExpenses()));
		}else{
			result.put(CommonConstants.EXPENSE, "");
		}
		
		if(proposal.getExpensesTerm() != null){
			result.put(CommonConstants.EXPENSE_TERM, String.valueOf(proposal.getExpensesTerm()));
		}else{
			result.put(CommonConstants.EXPENSE_TERM, "");
		}*/
		
		if(proposal.getWorkLocation() != null){
			result.put(CommonConstants.WORKLOCATION, String.valueOf(proposal.getWorkLocation()));
		}else{
			result.put(CommonConstants.WORKLOCATION, "");
		}
		
		if(proposal.getWorkDesc() != null){
			result.put(CommonConstants.WORKDESCRIPTION, String.valueOf(proposal.getWorkDesc()));
		}else{
			result.put(CommonConstants.WORKDESCRIPTION, "");
		}
		
		/*if(proposal.getCustomerContact() != null){
			result.put(CommonConstants.CUSTOMERCONTACT, String.valueOf(proposal.getCustomerContact()));
		}else{
			result.put(CommonConstants.CUSTOMERCONTACT, "");
		}*/
		
		if(proposal.getCustomerContact() != null){
			result.put(CommonConstants.CUSTOMERCONTACT, String.valueOf(proposal.getCustomerContact().getContactId()));
			//List<ContactDO> contactList = contactService.retriveById(proposal.getCustomerContact());
			//
				result.put(CommonConstants.CONTACT_NAME, String.valueOf(proposal.getCustomerContact().getFirstname()!=null?proposal.getCustomerContact().getFirstname():"") 
														+ " " +String.valueOf(proposal.getCustomerContact().getMiddlename()!=null?proposal.getCustomerContact().getMiddlename():"") 
														+ " " +String.valueOf(proposal.getCustomerContact().getLastname()!=null?proposal.getCustomerContact().getLastname():""));
				result.put(CommonConstants.CONTACT_EMAIL, String.valueOf(proposal.getCustomerContact().getPrimaryemail()!=null?proposal.getCustomerContact().getPrimaryemail():""));
				result.put(CommonConstants.CONTACT_MOBILE, String.valueOf(proposal.getCustomerContact().getMobile1()!=null?proposal.getCustomerContact().getMobile1():""));
			/*}else{
				result.put(CommonConstants.CONTACT_NAME, "");
				result.put(CommonConstants.CONTACT_EMAIL, "");
				result.put(CommonConstants.CONTACT_MOBILE, "");
			}*/
		}else{
			result.put(CommonConstants.CONTACT_NAME, "");
			result.put(CommonConstants.CONTACT_EMAIL, "");
			result.put(CommonConstants.CONTACT_MOBILE, "");
		}
		
		if(proposal.getSupplierContact() != null){
			result.put(CommonConstants.SUPPLIERCONTACT, String.valueOf(proposal.getSupplierContact().getEmpId()));
			/*List<EmployeeDO> employeeList = employeeService.retriveByEmpId(proposal.getSupplierContact().toString());
			if(employeeList != null && employeeList.size() > 0){*/
				result.put(CommonConstants.EMP_NAME, String.valueOf(proposal.getSupplierContact().getFirstname()!=null?proposal.getSupplierContact().getFirstname():"") 
														+ " " +String.valueOf(proposal.getSupplierContact().getMiddlename()!=null?proposal.getSupplierContact().getMiddlename():"") 
														+ " " +String.valueOf(proposal.getSupplierContact().getLastname()!=null?proposal.getSupplierContact().getLastname():""));
				result.put(CommonConstants.EMAIL, String.valueOf(proposal.getSupplierContact().getCompanyemail()!=null?proposal.getSupplierContact().getCompanyemail():""));
				result.put(CommonConstants.MOBILE, String.valueOf(proposal.getSupplierContact().getPersonalcontact()!=null?proposal.getSupplierContact().getPersonalcontact():""));
			/*}else{
				result.put(CommonConstants.EMP_NAME, "");
				result.put(CommonConstants.EMAIL, "");
				result.put(CommonConstants.MOBILE, "");
			}*/
		}else{
			result.put(CommonConstants.EMP_NAME, "");
			result.put(CommonConstants.EMAIL, "");
			result.put(CommonConstants.MOBILE, "");
		}
		
		/*if(proposal.getSupplierContact() != null){
			result.put(CommonConstants.SUPPLIERCONTACT, String.valueOf(proposal.getSupplierContact()));
		}else{
			result.put(CommonConstants.SUPPLIERCONTACT, "");
		}*/

		/*List<ProposalDetailsDO> proposalDetailsList = proposalDetailsService.retrieveById(proposal.getId());
		JSONArray proposalDetails = new JSONArray();
		if(proposalDetailsList != null && proposalDetailsList.size() > 0){
			for (ProposalDetailsDO proposalDetail : proposalDetailsList) {
				proposalDetails.put(getProposalObject(proposalDetail));
			}
			result.put(CommonConstants.FIELD_LIST, proposalDetails);
		}else{
			result.put(CommonConstants.FIELD_LIST, "");
		}
		
		List<ProposalDefaultDO> proposalDefultList = proposalDefaultService.retrieveById(proposal.getId());
		JSONArray proposalDefault = new JSONArray();
		if(proposalDetailsList != null && proposalDetailsList.size() > 0){
			for (ProposalDefaultDO proposalDetail : proposalDefultList) {
				proposalDefault.put(getProposalDefaultObject(proposalDetail));
			}
			result.put(CommonConstants.DEFAULT_LIST, proposalDefault);
		}else{
			result.put(CommonConstants.DEFAULT_LIST, "");
		}*/
		
		
		return result;
	}
	
	public static JSONObject getProposalObject(ProposalDetailsDO proposalDetails)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.KEY, String.valueOf(proposalDetails.getProposalKey()));
		result.put(CommonConstants.VALUE, String.valueOf(proposalDetails.getProposalValue()));
		return result;
	}
	
	public static JSONObject getProposalDefaultObject(ProposalDefaultDO proposalDetails)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.KEY, String.valueOf(proposalDetails.getProposalKey()));
		result.put(CommonConstants.VALUE, String.valueOf(proposalDetails.getProposalValue()));
		return result;
	}
	
	public static JSONObject getInvoiceTermObject(InvoiceTermDO invoiceTerm)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.COMMENT, String.valueOf(invoiceTerm.getDescripetion()));
		result.put(CommonConstants.PERCENTAGE, String.valueOf(invoiceTerm.getProjectPercentage()));
		return result;
	}
}
