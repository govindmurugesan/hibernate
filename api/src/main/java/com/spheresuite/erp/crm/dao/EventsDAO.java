package com.spheresuite.erp.crm.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.EventsDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class EventsDAO {
	static Logger logger = Logger.getLogger(EventsDAO.class.getName());
	@Autowired
	private GenericDAOImpl<EventsDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public EventsDO persist(EventsDO eventsDO) {
		try {
			genericObject.persist(eventsDO);
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return eventsDO;
	}
	
	@SuppressWarnings("unchecked")
	public List<EventsDO> retrieve() {
		List<EventsDO> eventsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EventsDO.FIND_ALL)
					.list(); 
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return eventsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EventsDO> retrieveById(Long id) {
		List<EventsDO> eventsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(EventsDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list(); 
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return eventsList;
	}
	
	public EventsDO update(EventsDO eventsDO) {
		try {
			genericObject.merge(eventsDO);
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return eventsDO;
	}
	
	public boolean update(List<EventsDO> eventsDO) {
		try {
			for (EventsDO eventsDO2 : eventsDO) {
				genericObject.merge(eventsDO2);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}
	
	public boolean persistList(List<EventsDO> eventsDO) {
		try {
			for (EventsDO eventsDO2 : eventsDO) {
				genericObject.persist(eventsDO2);
			}
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
		}
		return true;
	}
}
