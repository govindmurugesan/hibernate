package com.spheresuite.erp.crm.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.SalesStageDO;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
@Component
public class SalesStageUtil {
	
	private SalesStageUtil() {}
	
	public static JSONObject getSalesStageList(List<SalesStageDO> salesStageList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (SalesStageDO salesStageDO : salesStageList) {
				resultJSONArray.put(getSalesStageDetailObject(salesStageDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getSalesStageDetailObject(SalesStageDO salesStageDO)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(salesStageDO.getSalesStageId()));
		result.put(CommonConstants.NAME, String.valueOf(salesStageDO.getName()));
		result.put(CommonConstants.STATUS, String.valueOf(salesStageDO.getStatus()));
		if(salesStageDO.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(salesStageDO.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(salesStageDO.getUpdatedon())));
		return result;
	}
}
