package com.spheresuite.erp.crm.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.crm.service.ContactService;
import com.spheresuite.erp.crm.service.LeadService;
import com.spheresuite.erp.crm.service.OpportunitiesDocService;
import com.spheresuite.erp.crm.service.OpportunitiesService;
import com.spheresuite.erp.crm.service.OpportunityTypeService;
import com.spheresuite.erp.crm.service.SalesStageService;
import com.spheresuite.erp.crm.web.util.OpportunitiesUtil;
import com.spheresuite.erp.domainobject.ContactDO;
import com.spheresuite.erp.domainobject.CountryDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.LeadDO;
import com.spheresuite.erp.domainobject.OpportunitiesDO;
import com.spheresuite.erp.domainobject.OpportunitiesDocDO;
import com.spheresuite.erp.domainobject.OpportunityTypeDO;
import com.spheresuite.erp.domainobject.RolesDO;
import com.spheresuite.erp.domainobject.SalesStageDO;
import com.spheresuite.erp.domainobject.UserDO;
import com.spheresuite.erp.service.CountryService;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.RolesService;
import com.spheresuite.erp.service.UserService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/opportunity")
public class OpportunitiesRS {

	String validation = null;
	static Logger logger = Logger.getLogger(OpportunitiesRS.class.getName());

	@Autowired
	private OpportunitiesService opportunitiesService;
	
	@Autowired
	private EmployeeService employeeService;
	
	@Autowired
	private SalesStageService salesStageService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private RolesService rolesService;
	
	@Autowired
	private OpportunitiesDocService opportunitiesDocService;
	
	@Autowired
	private LeadService leadService;
	
	@Autowired
	private OpportunityTypeService projectTypeService;
	
	@Autowired
	private ContactService contactService;
	
	@Autowired
	private CountryService countryService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		OpportunitiesDO projectDO = new OpportunitiesDO();
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		/*if(inputJSON.get(CommonConstants.CUSTOMERID) != null && !inputJSON.get(CommonConstants.CUSTOMERID).toString().isEmpty()){
			 			projectDO.setCustomerId(Long.parseLong(inputJSON.get(CommonConstants.CUSTOMERID).toString()));
			 		}*/
			 		
			 		if(inputJSON.get(CommonConstants.COST) != null && !inputJSON.get(CommonConstants.COST).toString().isEmpty()){
			 			projectDO.setAmount(Long.parseLong(inputJSON.get(CommonConstants.COST).toString()));
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.CUSTOMERID) != null && !inputJSON.get(CommonConstants.CUSTOMERID).toString().isEmpty()){
			 			List<LeadDO> leadList = leadService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.CUSTOMERID).toString()));
			 			if(leadList != null && leadList.size() > 0){
			 				projectDO.setLead(leadList.get(0));
			 			}
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.CONTACT_ID) != null && !inputJSON.get(CommonConstants.CONTACT_ID).toString().isEmpty()){
			 			List<ContactDO> contactList = contactService.retriveById(Long.parseLong(inputJSON.get(CommonConstants.CONTACT_ID).toString()));
			 			if(contactList != null && contactList.size() > 0){
			 				projectDO.setContact(contactList.get(0));
			 			}
			 		}
			 		
			 		/*if(inputJSON.get(CommonConstants.CONTACT_ID) != null && !inputJSON.get(CommonConstants.CONTACT_ID).toString().isEmpty()){
			 			projectDO.setContactId(Long.parseLong(inputJSON.get(CommonConstants.CONTACT_ID).toString()));
			 		}*/
			 		if(inputJSON.get(CommonConstants.CURRENCY_TYPE) != null && !inputJSON.get(CommonConstants.CURRENCY_TYPE).toString().isEmpty()){
			 			List<CountryDO> countryList = countryService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.CURRENCY_TYPE).toString()));
			 			if(countryList != null && countryList.size() > 0){
			 				projectDO.setCurrencyType(countryList.get(0));
			 			}
			 		}
			 		//projectDO.setCurrencyType(inputJSON.get(CommonConstants.CURRENCY_TYPE) != null && !inputJSON.get(CommonConstants.CURRENCY_TYPE).toString().isEmpty()? inputJSON.get(CommonConstants.CURRENCY_TYPE).toString() : "");
			 		if(inputJSON.get(CommonConstants.SALESSTAGE) != null && !inputJSON.get(CommonConstants.SALESSTAGE).toString().isEmpty()){
			 			List<SalesStageDO> salesStageList = salesStageService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.SALESSTAGE).toString()));
			 			if(salesStageList != null && salesStageList.size() > 0){
			 				projectDO.setSalesStage(salesStageList.get(0));
			 			}
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.PROJECTTYPE) != null && !inputJSON.get(CommonConstants.PROJECTTYPE).toString().isEmpty()){
			 			List<OpportunityTypeDO> projectTypeList = projectTypeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.PROJECTTYPE).toString()));
			 			if(projectTypeList != null && projectTypeList.size() > 0){
			 				projectDO.setProjectType(projectTypeList.get(0));
			 			}
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.PROBOBILITY) != null && !inputJSON.get(CommonConstants.PROBOBILITY).toString().isEmpty()){
			 			projectDO.setProbability(Long.parseLong(inputJSON.get(CommonConstants.PROBOBILITY).toString()));
			 		}
			 		projectDO.setProjectname(inputJSON.get(CommonConstants.PROJECTNAME) != null && !inputJSON.get(CommonConstants.PROJECTNAME).toString().isEmpty() ? inputJSON.get(CommonConstants.PROJECTNAME).toString() : "");
			 		//projectDO.setSalesStage(inputJSON.get(CommonConstants.SALESSTAGE) != null && !inputJSON.get(CommonConstants.SALESSTAGE).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.SALESSTAGE).toString()) : null);
			 		projectDO.setDisplayname(inputJSON.get(CommonConstants.DISPLAYNAME) != null && !inputJSON.get(CommonConstants.DISPLAYNAME).toString().isEmpty() ? inputJSON.get(CommonConstants.DISPLAYNAME).toString() : "");
			 		projectDO.setProjectcode(inputJSON.get(CommonConstants.PROJECTCODE) != null && !inputJSON.get(CommonConstants.PROJECTCODE).toString().isEmpty() ? inputJSON.get(CommonConstants.PROJECTCODE).toString() : "");
			 		projectDO.setStartdate(inputJSON.get(CommonConstants.STARTDATE) != null && !inputJSON.get(CommonConstants.STARTDATE).toString().isEmpty() ? CommonUtil.convertStringToSqlDateMonth(inputJSON.get(CommonConstants.STARTDATE).toString()) : null);
			 		projectDO.setEnddate(inputJSON.get(CommonConstants.ENDDATE) != null && !inputJSON.get(CommonConstants.ENDDATE).toString().isEmpty() ? CommonUtil.convertStringToSqlDateMonth(inputJSON.get(CommonConstants.ENDDATE).toString()) : null);
			 		if(inputJSON.get(CommonConstants.PROJECTDURATION) != null && !inputJSON.get(CommonConstants.PROJECTDURATION).toString().isEmpty()){
			 			projectDO.setProjectduration(inputJSON.get(CommonConstants.PROJECTDURATION).toString());
			 		}
			 		
			 		/*if(inputJSON.get(CommonConstants.PROJECTTYPE) != null && !inputJSON.get(CommonConstants.PROJECTTYPE).toString().isEmpty()){
			 			projectDO.setProjectType(Long.parseLong(inputJSON.get(CommonConstants.PROJECTTYPE).toString()));
			 		}*/
			 		projectDO.setComment(inputJSON.get(CommonConstants.COMMENT) != null && !inputJSON.get(CommonConstants.COMMENT).toString().isEmpty() ? inputJSON.get(CommonConstants.COMMENT).toString() : "");
			 		projectDO.setUpdatedon(new Date());
			 		projectDO.setCreatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			projectDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			projectDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			projectDO.setEmpId(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		
			 		
			 	}
				opportunitiesService.persist(projectDO);
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Opportunity Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponseId(projectDO.getOpportunityId().toString()).toString();
	}
	
	/*@RequestMapping(value = "/retrieveActive{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveById(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<OpportunitiesDO> ProjectList = opportunitiesService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = OpportunitiesUtil.getProjectList(ProjectList).toString();
				}
			
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}*/

	@RequestMapping(value = "/retrieveAll", method = RequestMethod.GET)
	public @ResponseBody String retrieveAll(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<SalesStageDO> salesStageList = salesStageService.retrieveActive();
				respJSON = OpportunitiesUtil.getProjectChartList(salesStageList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveopp", method = RequestMethod.GET)
	public @ResponseBody String retrieveopp(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<OpportunitiesDO> oppList = opportunitiesService.retrieveAll();
				respJSON = OpportunitiesUtil.getProjectListWithOutDoc(oppList).toString();
				
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				int i = 0;
				List<Long> deptIds = new ArrayList<Long>();
				if(request.getSession().getAttribute("deptIds") != null){
					deptIds = (List<Long>) request.getSession().getAttribute("deptIds");
				}
				List<String> empIds = new ArrayList<String>();
				if(deptIds != null && deptIds.size() > 0){
					empIds = employeeService.retrieveByDeptIds(deptIds);
					/*List<String> newList = employeeService.retrieveByDeptIds(deptIds);
					for (String deptId : newList) { 
						empIds.add(String.valueOf(deptId)); 
					}*/
				}else if(request.getSession().getAttribute("empIds") != null){
					empIds = new ArrayList<String>();
					String empid = request.getSession().getAttribute("empIds").toString();
					empIds.add(empid);
					List<UserDO> userList = userService.retriveByEmpId(empid.toString());
					if(userList.get(0).getRole().getRoleId() != null){
						List<RolesDO> roleList = rolesService.retriveById(userList.get(0).getRole().getRoleId());
						if(roleList != null && roleList.size() > 0){
							if(roleList.get(0).getName().equalsIgnoreCase(CommonConstants.SUPERADMIN)){
								i=1;
							}
						}else{
							empIds = new ArrayList<String>();
						}
					}else{
						empIds = new ArrayList<String>();
					}
				}
				if(i==1){
					List<OpportunitiesDO> categoryList = opportunitiesService.retrieveAll();
					respJSON = OpportunitiesUtil.getProjectListWithOutDoc(categoryList).toString();
				}else{
					List<OpportunitiesDO> categoryList = opportunitiesService.retrieve(empIds);
					respJSON = OpportunitiesUtil.getProjectListWithOutDoc(categoryList).toString();
					/*List<OpportunitiesDO> categoryList = opportunitiesService.retrieveAll();
					respJSON = OpportunitiesUtil.getProjectListWithOutDoc(categoryList).toString();*/
				}
				
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		OpportunitiesDO projectDO = new OpportunitiesDO();
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<OpportunitiesDO> ProjectList = opportunitiesService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		projectDO = ProjectList.get(0);

			 		if(!inputJSON.get(CommonConstants.COST).toString().isEmpty()){
			 			projectDO.setAmount(Long.parseLong(inputJSON.get(CommonConstants.COST).toString()));
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.CUSTOMERID) != null && !inputJSON.get(CommonConstants.CUSTOMERID).toString().isEmpty()){
			 			List<LeadDO> leadList = leadService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.CUSTOMERID).toString()));
			 			if(leadList != null && leadList.size() > 0){
			 				projectDO.setLead(leadList.get(0));
			 			}
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.CONTACT_ID) != null && !inputJSON.get(CommonConstants.CONTACT_ID).toString().isEmpty()){
			 			List<ContactDO> contactList = contactService.retriveById(Long.parseLong(inputJSON.get(CommonConstants.CONTACT_ID).toString()));
			 			if(contactList != null && contactList.size() > 0){
			 				projectDO.setContact(contactList.get(0));
			 			}
			 		}
			 		
			 		/*if(inputJSON.get(CommonConstants.CONTACT_ID) != null && !inputJSON.get(CommonConstants.CONTACT_ID).toString().isEmpty()){
			 			projectDO.setContactId(Long.parseLong(inputJSON.get(CommonConstants.CONTACT_ID).toString()));
			 		}*/
			 		
			 		if(inputJSON.get(CommonConstants.SALESSTAGE) != null && !inputJSON.get(CommonConstants.SALESSTAGE).toString().isEmpty()){
			 			List<SalesStageDO> salesStageList = salesStageService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.SALESSTAGE).toString()));
			 			if(salesStageList != null && salesStageList.size() > 0){
			 				projectDO.setSalesStage(salesStageList.get(0));
			 			}
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.PROJECTTYPE) != null && !inputJSON.get(CommonConstants.PROJECTTYPE).toString().isEmpty()){
			 			List<OpportunityTypeDO> projectTypeList = projectTypeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.PROJECTTYPE).toString()));
			 			if(projectTypeList != null && projectTypeList.size() > 0){
			 				projectDO.setProjectType(projectTypeList.get(0));
			 			}
			 		}
			 		if(inputJSON.get(CommonConstants.CURRENCY_TYPE) != null && !inputJSON.get(CommonConstants.CURRENCY_TYPE).toString().isEmpty()){
			 			List<CountryDO> countryList = countryService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.CURRENCY_TYPE).toString()));
			 			if(countryList != null && countryList.size() > 0){
			 				projectDO.setCurrencyType(countryList.get(0));
			 			}
			 		}
			 		//projectDO.setCurrencyType(inputJSON.get(CommonConstants.CURRENCY_TYPE) != null && !inputJSON.get(CommonConstants.CURRENCY_TYPE).toString().isEmpty()? inputJSON.get(CommonConstants.CURRENCY_TYPE).toString() : "");
			 		if(inputJSON.get(CommonConstants.PROBOBILITY) != null && !inputJSON.get(CommonConstants.PROBOBILITY).toString().isEmpty()){
			 			projectDO.setProbability(Long.parseLong(inputJSON.get(CommonConstants.PROBOBILITY).toString()));
			 		}
			 		//projectDO.setSalesStage(!inputJSON.get(CommonConstants.SALESSTAGE).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.SALESSTAGE).toString()) : null);
			 		projectDO.setProjectname(inputJSON.get(CommonConstants.PROJECTNAME) != null && !inputJSON.get(CommonConstants.PROJECTNAME).toString().isEmpty() ? inputJSON.get(CommonConstants.PROJECTNAME).toString() : "");
			 		projectDO.setDisplayname(inputJSON.get(CommonConstants.DISPLAYNAME) != null && !inputJSON.get(CommonConstants.DISPLAYNAME).toString().isEmpty() ? inputJSON.get(CommonConstants.DISPLAYNAME).toString() : "");
			 		projectDO.setProjectcode(inputJSON.get(CommonConstants.PROJECTCODE) != null && !inputJSON.get(CommonConstants.PROJECTCODE).toString().isEmpty() ? inputJSON.get(CommonConstants.PROJECTCODE).toString() : "");
			 		projectDO.setStartdate(inputJSON.get(CommonConstants.STARTDATE) != null && !inputJSON.get(CommonConstants.STARTDATE).toString().isEmpty() ? CommonUtil.convertStringToSqlDateMonth(inputJSON.get(CommonConstants.STARTDATE).toString()) : null);
			 		projectDO.setEnddate(inputJSON.get(CommonConstants.ENDDATE) != null && !inputJSON.get(CommonConstants.ENDDATE).toString().isEmpty() ? CommonUtil.convertStringToSqlDateMonth(inputJSON.get(CommonConstants.ENDDATE).toString()) : null);
			 		if(inputJSON.get(CommonConstants.PROJECTDURATION) != null && !inputJSON.get(CommonConstants.PROJECTDURATION).toString().isEmpty()){
				 		projectDO.setProjectduration(inputJSON.get(CommonConstants.PROJECTDURATION).toString());
			 		}
			 		projectDO.setComment(inputJSON.get(CommonConstants.COMMENT) != null && !inputJSON.get(CommonConstants.COMMENT).toString().isEmpty() ? inputJSON.get(CommonConstants.COMMENT).toString() : "");
			 		projectDO.setUpdatedon(new Date());
			 		/*if(inputJSON.get(CommonConstants.PROJECTTYPE) != null && !inputJSON.get(CommonConstants.PROJECTTYPE).toString().isEmpty()){
			 			projectDO.setProjectType(Long.parseLong(inputJSON.get(CommonConstants.PROJECTTYPE).toString()));
			 		}*/
			 		//projectDO.setCreatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			projectDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		opportunitiesService.update(projectDO);
			 		opportunitiesDocService.delete(projectDO.getOpportunityId());
			 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Opportunity Updated");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponseId(projectDO.getOpportunityId().toString()).toString();
	}
	
	
	@RequestMapping(value = "/retrieveById/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveById(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					//List<ProjectDO> ProjectList = projectService.retrieveByProjectId(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					List<OpportunitiesDO> ProjectDetail = opportunitiesService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = OpportunitiesUtil.getProjectList(ProjectDetail).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveByCustomerId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByCustomerId(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<OpportunitiesDO> ProjectDetail = opportunitiesService.retrieveByCustomerId(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = OpportunitiesUtil.getProjectListWithOutDoc(ProjectDetail).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/updatepic", method = RequestMethod.POST)
	public @ResponseBody String updatepic(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				OpportunitiesDocDO opportunitiesDocDO = new OpportunitiesDocDO();
				/*JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("data").toString());*/
			 	if (request.getParameter("file") != null && request.getParameter("id") != null){
			 		List<OpportunitiesDO> oppList = opportunitiesService.retrieveById(Long.parseLong(request.getParameter("id").toString()));
			 		if(oppList != null && oppList.size() > 0){
				 		opportunitiesDocDO.setOpportunity(oppList.get(0));
				 		opportunitiesDocDO.setPhoto(request.getParameter("file"));
				 		opportunitiesDocDO.setFileName(request.getParameter("name"));
				 		opportunitiesDocDO.setUpdatedon(new Date());
				 		opportunitiesDocDO.setCreatedon(new Date());
				 		if(request.getParameter(CommonConstants.UPDATED_BY) != null && !request.getParameter(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			opportunitiesDocDO.setUpdatedBy(request.getParameter(CommonConstants.UPDATED_BY).toString());
				 			opportunitiesDocDO.setCreatedBy(request.getParameter(CommonConstants.UPDATED_BY).toString());
				 		}
				 		opportunitiesDocService.persist(opportunitiesDocDO);
			 		}else{
			 			return CommonWebUtil.buildErrorResponse("").toString();
			 		}
 			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/importOpportunities", method = RequestMethod.POST)
	public @ResponseBody String importOpportunities(Model model, HttpServletRequest request) {
			
		try {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
				Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY);
				org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
				
				JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
				List<OpportunitiesDO> opportunitiesDOlist = new ArrayList<OpportunitiesDO>();
				List<OpportunityTypeDO> projectTypeList = projectTypeService.retrieveActive();
				List<SalesStageDO> salesStageList = salesStageService.retrieveActive();
				List<LeadDO> leadList = leadService.retrieveAll();
				List<EmployeeDO> employeeList = employeeService.retrieveActive();
				List<ContactDO> contactList = null;
				for (int i=0; i < fileData.size(); i++){
					OpportunitiesDO opportunitiesDO = new OpportunitiesDO();
					JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
					if(colName.get(CommonConstants.NAME) != null){
						opportunitiesDO.setProjectname(!rowJSON.get(colName.get(CommonConstants.NAME)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.NAME)).toString().toUpperCase() : "");
					}
					if(colName.get(CommonConstants.CUSTOMERNAME) != null){
						if(!rowJSON.get(colName.get(CommonConstants.CUSTOMERNAME)).toString().isEmpty()){
							if(leadList != null && leadList.size() > 0){
								for(LeadDO leadDO : leadList){
									if(leadDO.getName().toLowerCase().contains(rowJSON.get(colName.get(CommonConstants.CUSTOMERNAME)).toString().toLowerCase())){
										opportunitiesDO.setLead(leadDO);
										//customerId = leadDO.getId();
										contactList = contactService.retriveByLeadId(leadDO.getLeadId());
									}
								}
							}/* else{
								opportunitiesDO.setCustomerId(Long.parseLong(rowJSON != null ? rowJSON.get(colName.get(CommonConstants.CUSTOMERNAME)).toString() : ""));
							}*/
				 		}
					}
					
					if(colName.get(CommonConstants.CONTACT_NAME) != null){
						if(!rowJSON.get(colName.get(CommonConstants.CONTACT_NAME)).toString().isEmpty()){
							if(contactList != null && contactList.size() > 0){
								//for(StateDO stateDO : contactList){
								String Name = contactList.get(0).getFirstname() + " " +contactList.get(0).getMiddlename() + " " + contactList.get(0).getLastname();
									if(Name.toLowerCase().contains(rowJSON.get(colName.get(CommonConstants.CONTACT_NAME)).toString().toLowerCase())){
										opportunitiesDO.setContact(contactList.get(0));
									}
								//}
							} /*else{
								opportunitiesDO.setState(Long.parseLong(rowJSON != null ? rowJSON.get(colName.get(CommonConstants.STATE)).toString() : ""));
							}*/
				 		}
					}
					
					if(colName.get(CommonConstants.PROJECTTYPE) != null){
						if(!rowJSON.get(colName.get(CommonConstants.PROJECTTYPE)).toString().isEmpty()){
							if(leadList != null && leadList.size() > 0){
								for(OpportunityTypeDO projectTypeDO : projectTypeList){
									if(projectTypeDO.getName().toLowerCase().contains(rowJSON.get(colName.get(CommonConstants.PROJECTTYPE)).toString().toLowerCase())){
										opportunitiesDO.setProjectType(projectTypeDO);
									}
								}
							}/* else{
								opportunitiesDO.setCustomerId(Long.parseLong(rowJSON != null ? rowJSON.get(colName.get(CommonConstants.CUSTOMERNAME)).toString() : ""));
							}*/
				 		}
					}
					if(colName.get(CommonConstants.COST) != null){
				 		if(!rowJSON.get(colName.get(CommonConstants.COST)).toString().isEmpty()){
				 			opportunitiesDO.setAmount(Long.parseLong(rowJSON != null ? rowJSON.get(colName.get(CommonConstants.COST)).toString() : null));
				 		}
					}
					if(colName.get(CommonConstants.STARTDATE) != null){
				 		if(!rowJSON.get(colName.get(CommonConstants.STARTDATE)).toString().isEmpty()){
				 			opportunitiesDO.setStartdate(rowJSON != null ? CommonUtil.convertStringToSqlDateMonth(rowJSON.get(colName.get(CommonConstants.STARTDATE)).toString()) : null);
				 		}
					}
					if(colName.get(CommonConstants.ENDDATE) != null){
				 		if(!rowJSON.get(colName.get(CommonConstants.ENDDATE)).toString().isEmpty()){
				 			opportunitiesDO.setEnddate(rowJSON != null ? CommonUtil.convertStringToSqlDateMonth(rowJSON.get(colName.get(CommonConstants.ENDDATE)).toString()) : null);
				 		}
					}
					if(colName.get(CommonConstants.PROBOBILITY) != null){
				 		if(!rowJSON.get(colName.get(CommonConstants.PROBOBILITY)).toString().isEmpty()){
				 			opportunitiesDO.setAmount(Long.parseLong(rowJSON != null ? rowJSON.get(colName.get(CommonConstants.PROBOBILITY)).toString() : null));
				 		}
					}
					if(colName.get(CommonConstants.DISPLAYNAME) != null){
				 		if(!rowJSON.get(colName.get(CommonConstants.DISPLAYNAME)).toString().isEmpty()){
				 			opportunitiesDO.setDisplayname(rowJSON != null ? rowJSON.get(colName.get(CommonConstants.DISPLAYNAME)).toString() : "");
				 		}
					}
					if(colName.get(CommonConstants.PROJECTNAME) != null){
						opportunitiesDO.setProjectname(!rowJSON.get(colName.get(CommonConstants.PROJECTNAME)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.PROJECTNAME)).toString() : null);
					}
					if(colName.get(CommonConstants.PROJECTDURATION) != null){
						opportunitiesDO.setProjectduration(!rowJSON.get(colName.get(CommonConstants.PROJECTDURATION)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.PROJECTDURATION)).toString() : null);
					}
					if(colName.get(CommonConstants.COMMENT) != null){
						opportunitiesDO.setComment(!rowJSON.get(colName.get(CommonConstants.COMMENT)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.COMMENT)).toString() : null);
					}
					if(colName.get(CommonConstants.PROJECTCODE) != null){
						opportunitiesDO.setComment(!rowJSON.get(colName.get(CommonConstants.PROJECTCODE)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.PROJECTCODE)).toString() : null);
					}
					if(colName.get(CommonConstants.SALESSTAGE) != null){
				 		if(!rowJSON.get(colName.get(CommonConstants.SALESSTAGE)).toString().isEmpty()){
				 			if(salesStageList != null && salesStageList.size() > 0){
								for(SalesStageDO salesStageDO : salesStageList){
									if(salesStageDO.getName().toLowerCase().contains(rowJSON.get(colName.get(CommonConstants.SALESSTAGE)).toString().toLowerCase())){
										opportunitiesDO.setSalesStage(salesStageDO);
									}
								}
							} /*else{
								leadDO.setLeadType(Long.parseLong(rowJSON != null ? rowJSON.get(colName.get(CommonConstants.LEADTYPE)).toString() : ""));
							}*/
				 			//leadDO.setLeadType(Long.parseLong(rowJSON != null ? rowJSON.get(colName.get(CommonConstants.LEADTYPE)).toString() : ""));
				 		}
					}
					if(colName.get(CommonConstants.EMPNAME) != null){
				 		if(!rowJSON.get(colName.get(CommonConstants.EMPNAME)).toString().isEmpty()){
				 			if(employeeList != null && employeeList.size() > 0){
								for(EmployeeDO employeeDO : employeeList){
									String Name = employeeDO.getFirstname() + " " +employeeDO.getMiddlename() + " " + employeeDO.getLastname();
									if(Name.toLowerCase().contains(rowJSON.get(colName.get(CommonConstants.EMPNAME)).toString().toLowerCase())){
										opportunitiesDO.setEmpId(employeeDO.getEmpId());
									}
								}
							} /*else{
								leadDO.setLeadType(Long.parseLong(rowJSON != null ? rowJSON.get(colName.get(CommonConstants.LEADTYPE)).toString() : ""));
							}*/
				 			//leadDO.setLeadType(Long.parseLong(rowJSON != null ? rowJSON.get(colName.get(CommonConstants.LEADTYPE)).toString() : ""));
				 		}
					}
			 		opportunitiesDO.setUpdatedon(new Date());
			 		opportunitiesDO.setCreatedon(new Date());
			 		if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
			 			opportunitiesDO.setUpdatedby(updatedBy.toString());
			 			opportunitiesDO.setCreatedby(updatedBy.toString());
			 			//opportunitiesDO.setEmpId(Long.parseLong(updatedBy.toString()));
			 		}
			 		opportunitiesDOlist.add(opportunitiesDO);
			 		opportunitiesService.persist(opportunitiesDO);
			 		
				}
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Opportunities Created");
				//leadService.persistList(leadDOlist);
				
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
