package com.spheresuite.erp.crm.rs;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.crm.service.ContactService;
import com.spheresuite.erp.crm.service.ContactTypeService;
import com.spheresuite.erp.crm.service.IndustryService;
import com.spheresuite.erp.crm.service.LeadService;
import com.spheresuite.erp.crm.service.LeadStatusService;
import com.spheresuite.erp.crm.service.LeadTypeService;
import com.spheresuite.erp.crm.service.NotesService;
import com.spheresuite.erp.crm.service.OpportunitiesService;
import com.spheresuite.erp.crm.service.ProposalService;
import com.spheresuite.erp.crm.service.SalutationService;
import com.spheresuite.erp.crm.web.util.LeadUtil;
import com.spheresuite.erp.domainobject.ContactDO;
import com.spheresuite.erp.domainobject.ContactTypeDO;
import com.spheresuite.erp.domainobject.IndustryDO;
import com.spheresuite.erp.domainobject.LeadDO;
import com.spheresuite.erp.domainobject.LeadStatusDO;
import com.spheresuite.erp.domainobject.LeadTypeDO;
import com.spheresuite.erp.domainobject.NotesDO;
import com.spheresuite.erp.domainobject.OpportunitiesDO;
import com.spheresuite.erp.domainobject.ProposalDO;
import com.spheresuite.erp.domainobject.RequirementsDO;
import com.spheresuite.erp.domainobject.RolesDO;
import com.spheresuite.erp.domainobject.SalutationDO;
import com.spheresuite.erp.domainobject.StateDO;
import com.spheresuite.erp.domainobject.UserDO;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.RequirementsService;
import com.spheresuite.erp.service.RolesService;
import com.spheresuite.erp.service.StateService;
import com.spheresuite.erp.service.UserService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/lead")
public class LeadRS {

	String validation = null;
	static Logger logger = Logger.getLogger(LeadRS.class.getName());
	
	@Autowired
	private LeadService leadService;
	
	@Autowired
	private ContactService contactService;
	
	@Autowired
	private EmployeeService employeeService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private OpportunitiesService opportunitiesService;
	
	@Autowired
	private ProposalService proposalService;
	
	@Autowired
	private RequirementsService requirementsService;
	
	@Autowired
	private RolesService rolesService;
	
	@Autowired
	private LeadTypeService leadTypeService;
	
	@Autowired
	private LeadStatusService leadStatusService;
	
	@Autowired
	private StateService stateService;
	
	@Autowired
	private IndustryService industryService;
	
	@Autowired
	private NotesService notesService;
	
	@Autowired
	private SalutationService salutationService;
	
	@Autowired
	private ContactTypeService contactTypeService;
	
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		LeadDO leadDetails	= new LeadDO();
		try {
			if (WebManager.authenticateSession(request)) {
				LeadDO leadDO = new LeadDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		leadDO.setName(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty() ? inputJSON.get(CommonConstants.NAME).toString().toUpperCase() : "");
			 		
			 		if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
			 			List<LeadStatusDO> leadStatusList = leadStatusService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.STATUS).toString()));
			 			if(leadStatusList != null && leadStatusList.size() > 0){
			 				leadDO.setStatus(leadStatusList.get(0));
			 			}
			 			//leadDO.setStatus(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.STATUS).toString() : ""));
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.INDUSTRY) != null && !inputJSON.get(CommonConstants.INDUSTRY).toString().isEmpty()){
			 			List<IndustryDO> industryList = industryService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.INDUSTRY).toString()));
			 			if(industryList != null && industryList.size() > 0){
			 				leadDO.setIndustry(industryList.get(0));
			 			}
			 		}
			 		/*if(inputJSON.get(CommonConstants.INDUSTRY) != null && !inputJSON.get(CommonConstants.INDUSTRY).toString().isEmpty()){
			 			leadDO.setIndustryID(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.INDUSTRY).toString() : null));
			 		}*/
			 		leadDO.setSource(inputJSON.get(CommonConstants.SOURCE) != null && !inputJSON.get(CommonConstants.SOURCE).toString().isEmpty() ? inputJSON.get(CommonConstants.SOURCE).toString() : "");
			 		
			 		if(inputJSON.get(CommonConstants.MOBILE) != null && !inputJSON.get(CommonConstants.MOBILE).toString().isEmpty()){
			 			leadDO.setMobile(inputJSON.get(CommonConstants.MOBILE).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.STATE) != null && !inputJSON.get(CommonConstants.STATE).toString().isEmpty()){
			 			List<StateDO> stateList = stateService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.STATE).toString()));
			 			if(stateList != null && stateList.size() > 0){
			 				leadDO.setState(stateList.get(0));
			 			}
			 		}
			 		/*if(inputJSON.get(CommonConstants.STATE) != null && !inputJSON.get(CommonConstants.STATE).toString().isEmpty()){
			 			leadDO.setState(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.STATE).toString() : ""));
			 		}*/
			 		
			 		if(inputJSON.get(CommonConstants.CITY) != null && !inputJSON.get(CommonConstants.CITY).toString().isEmpty()){
			 			leadDO.setCity(inputJSON.get(CommonConstants.CITY).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.LEAD_DATE) != null && !inputJSON.get(CommonConstants.LEAD_DATE).toString().isEmpty()){
			 			leadDO.setLeaddate(CommonUtil.convertStringToSqlDateMonth(inputJSON.get(CommonConstants.LEAD_DATE).toString()));
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.FOLLOW_UP_DATE) != null && !inputJSON.get(CommonConstants.FOLLOW_UP_DATE).toString().isEmpty()){
			 			leadDO.setFollowupdate(CommonUtil.convertStringToDateTimePick(inputJSON.get(CommonConstants.FOLLOW_UP_DATE).toString()));
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.PHONE) != null && !inputJSON.get(CommonConstants.PHONE).toString().isEmpty()){
			 			leadDO.setPhone(inputJSON.get(CommonConstants.PHONE).toString());
			 		}
			 		
			 		leadDO.setEmail(inputJSON.get(CommonConstants.EMAIL) != null && !inputJSON.get(CommonConstants.EMAIL).toString().isEmpty() ? inputJSON.get(CommonConstants.EMAIL).toString() : "");
			 		leadDO.setAddress(inputJSON.get(CommonConstants.ADDRESS) != null && !inputJSON.get(CommonConstants.ADDRESS).toString().isEmpty() ? inputJSON.get(CommonConstants.ADDRESS).toString() : "");
			 		leadDO.setComment(inputJSON.get(CommonConstants.COMMENT) != null && !inputJSON.get(CommonConstants.COMMENT).toString().isEmpty() ? inputJSON.get(CommonConstants.COMMENT).toString() : "");
			 		
			 		if(inputJSON.get(CommonConstants.LEADTYPE) != null && !inputJSON.get(CommonConstants.LEADTYPE).toString().isEmpty()){
			 			List<LeadTypeDO> leadTypeList = leadTypeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.LEADTYPE).toString()));
			 			if(leadTypeList != null && leadTypeList.size() > 0){
			 				leadDO.setLeadType(leadTypeList.get(0));
			 			}
			 		}
			 		/*if(inputJSON.get(CommonConstants.LEADTYPE) != null && !inputJSON.get(CommonConstants.LEADTYPE).toString().isEmpty()){
			 			leadDO.setLeadType(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.LEADTYPE).toString() : ""));
			 		}*/
			 		
			 		leadDO.setType(inputJSON.get(CommonConstants.TYPE) != null && !inputJSON.get(CommonConstants.TYPE).toString().isEmpty() ? inputJSON.get(CommonConstants.TYPE).toString() : "");
			 		leadDO.setCreatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			leadDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			leadDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			leadDO.setEmpId((inputJSON.get(CommonConstants.UPDATED_BY).toString()));
			 		}
			 		leadDO.setIsDeleted("n");
			 		leadDO.setUpdatedon(new Date());
			 		
			 	}
			 	leadDetails	= leadService.persist(leadDO);
			 	if(!inputJSON.get(CommonConstants.SKIP).toString().isEmpty()){
			 		if(inputJSON.get(CommonConstants.SKIP).toString().equalsIgnoreCase("no")){
			 			if(leadDetails.getLeadId() != null){
					 		ContactDO contactDO = new ContactDO();
					 		//contactDO.setLeadtype(leadDetails.getLeadId());
					 		List<LeadDO> leadList = leadService.retrieveById(leadDetails.getLeadId());
				 			if(leadList != null && leadList.size() > 0){
				 				contactDO.setLead(leadList.get(0));
				 			}
					 		if(inputJSON.get(CommonConstants.SALUTATION) != null && !inputJSON.get(CommonConstants.SALUTATION).toString().isEmpty()){
					 			List<SalutationDO> salutationList = salutationService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.SALUTATION).toString()));
					 			if(salutationList != null && salutationList.size() > 0){
					 				contactDO.setSalutation(salutationList.get(0));
					 			}
					 		}
					 		
					 		if(inputJSON.get(CommonConstants.CONTACTTYPE) != null && !inputJSON.get(CommonConstants.CONTACTTYPE).toString().isEmpty()){
					 			List<ContactTypeDO> contactTypeList = contactTypeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.CONTACTTYPE).toString()));
					 			if(contactTypeList != null && contactTypeList.size() > 0){
					 				contactDO.setContactType(contactTypeList.get(0));
					 			}
					 		}
					 		/*if(inputJSON.get(CommonConstants.SALUTATION) != null && !inputJSON.get(CommonConstants.SALUTATION).toString().isEmpty()){
						 		contactDO.setSalutation(Long.parseLong(inputJSON.get(CommonConstants.SALUTATION).toString()));
					 		}*/
					 		contactDO.setFirstname(inputJSON.get(CommonConstants.FIRSTNAME) != null && !inputJSON.get(CommonConstants.FIRSTNAME).toString().isEmpty() ? inputJSON.get(CommonConstants.FIRSTNAME).toString() : "");
					 		contactDO.setLastname(inputJSON.get(CommonConstants.LASTNAME) != null && !inputJSON.get(CommonConstants.LASTNAME).toString().isEmpty() ? inputJSON.get(CommonConstants.LASTNAME).toString() : "");
					 		contactDO.setMiddlename(inputJSON.get(CommonConstants.MIDDLENAME) != null && !inputJSON.get(CommonConstants.MIDDLENAME).toString().isEmpty() ? inputJSON.get(CommonConstants.MIDDLENAME).toString() : "");
					 		contactDO.setDesignation((inputJSON.get(CommonConstants.DESIGNATION) != null && !inputJSON.get(CommonConstants.DESIGNATION).toString().isEmpty() ? inputJSON.get(CommonConstants.DESIGNATION).toString() : ""));
					 		/*if(inputJSON.get(CommonConstants.CONTACTTYPE) != null && !inputJSON.get(CommonConstants.CONTACTTYPE).toString().isEmpty()){
					 			contactDO.setContattype(Long.parseLong(inputJSON.get(CommonConstants.CONTACTTYPE).toString()));
					 		}*/
					 		contactDO.setPrimaryemail((inputJSON.get(CommonConstants.PRIMARYEMAIL) != null && !inputJSON.get(CommonConstants.PRIMARYEMAIL).toString().isEmpty() ? inputJSON.get(CommonConstants.PRIMARYEMAIL).toString() : ""));
					 		//contactDO.setSecondaryemail((!inputJSON.get(CommonConstants.SECONDARYEMAIL).toString().isEmpty() ? inputJSON.get(CommonConstants.SECONDARYEMAIL).toString() : null));
					 		contactDO.setAddress1(inputJSON.get(CommonConstants.ADDRESS1) != null && !inputJSON.get(CommonConstants.ADDRESS1).toString().isEmpty() ? inputJSON.get(CommonConstants.ADDRESS1).toString() : "");
					 		//contactDO.setAddress2(!inputJSON.get(CommonConstants.ADDRESS2).toString().isEmpty() ? inputJSON.get(CommonConstants.ADDRESS2).toString() : null);
					 		if(inputJSON.get(CommonConstants.MOBILE1) != null && !inputJSON.get(CommonConstants.MOBILE1).toString().isEmpty()){
						 		contactDO.setMobile1(inputJSON.get(CommonConstants.MOBILE1).toString());
					 		}
					 		/*if(!inputJSON.get(CommonConstants.MOBILE2).toString().isEmpty()){
					 			contactDO.setMobile2(Long.parseLong(inputJSON.get(CommonConstants.MOBILE2).toString()));
					 		}*/
					 		if(inputJSON.get(CommonConstants.PHONE1) != null && !inputJSON.get(CommonConstants.PHONE1).toString().isEmpty()){
						 		contactDO.setPhone1(Long.parseLong(inputJSON.get(CommonConstants.PHONE1).toString()));
					 		}
					 		/*if(!inputJSON.get(CommonConstants.PHONE2).toString().isEmpty()){
						 		contactDO.setPhone2(Long.parseLong(inputJSON.get(CommonConstants.PHONE2).toString()));
					 		}
					 		if(!inputJSON.get(CommonConstants.PHONE3).toString().isEmpty()){
						 		contactDO.setPhone3(Long.parseLong(inputJSON.get(CommonConstants.PHONE3).toString()));
					 		}
					 		if(!inputJSON.get(CommonConstants.PHONE4).toString().isEmpty()){
						 		contactDO.setPhone4(Long.parseLong(inputJSON.get(CommonConstants.PHONE4).toString()));
					 		}*/
					 		if(inputJSON.get(CommonConstants.FAX) != null && !inputJSON.get(CommonConstants.FAX).toString().isEmpty()){
						 		contactDO.setFax(Long.parseLong(inputJSON.get(CommonConstants.FAX).toString()));
					 		}
					 		contactDO.setWebsite((inputJSON.get(CommonConstants.WEBSITE) != null && !inputJSON.get(CommonConstants.WEBSITE).toString().isEmpty() ? inputJSON.get(CommonConstants.WEBSITE).toString() : ""));
					 		contactDO.setRequirements((inputJSON.get(CommonConstants.REQUIREMENT) != null && !inputJSON.get(CommonConstants.REQUIREMENT).toString().isEmpty() ? inputJSON.get(CommonConstants.REQUIREMENT).toString() : ""));
					 		contactDO.setNotes((inputJSON.get(CommonConstants.NOTE) != null && !inputJSON.get(CommonConstants.NOTE).toString().isEmpty() ? inputJSON.get(CommonConstants.NOTE).toString() : ""));
					 		contactDO.setComments((inputJSON.get(CommonConstants.COMMENT2) != null && !inputJSON.get(CommonConstants.COMMENT2).toString().isEmpty() ? inputJSON.get(CommonConstants.COMMENT2).toString() : ""));
					 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			contactDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			contactDO.setEmpId(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 		}
					 		contactDO.setIsDeleted("n");
					 		 contactService.persist(contactDO);
					 	}else{
					 		return CommonWebUtil.buildErrorResponse("").toString();
					 	}
			 		}
			 	}
			 	if(leadDO.getLeadId() != null){
			 		NotesDO notesDO = new NotesDO();
			 		if(inputJSON.get(CommonConstants.NOTES) != null && !inputJSON.get(CommonConstants.NOTES).toString().isEmpty()){
			 			notesDO.setLead(leadDO);
			 			if(!inputJSON.get(CommonConstants.NOTES).toString().isEmpty()){
			 				notesDO.setNote(inputJSON != null ? inputJSON.get(CommonConstants.NOTES).toString() : "");
				 		}
			 			notesDO.setCreatedon(new Date());
			 			if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 				notesDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 				notesDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 				notesDO.setEmpId(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		notesDO.setUpdatedon(new Date());
				 		notesService.persist(notesDO);
			 		}
			 	}
			 	CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Lead Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponseId(leadDetails.getLeadId().toString()).toString();
	}
	
	
	@RequestMapping(value = "/retrieveById/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveById(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<LeadDO> LeadList = leadService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = LeadUtil.getLeadList(LeadList).toString();
				}
				
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/retrieve/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieve(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.TYPE).toString().isEmpty()){
					int i = 0;
					List<Long> deptIds = new ArrayList<Long>();
					if(request.getSession().getAttribute("deptIds") != null){
						deptIds = (List<Long>) request.getSession().getAttribute("deptIds");
					}
					List<String> empIds = new ArrayList<String>();
					if(deptIds != null && deptIds.size() > 0){
						empIds = employeeService.retrieveByDeptIds(deptIds);
						/*List<String> newList = employeeService.retrieveByDeptIds(deptIds);
						for (String deptId : newList) { 
							empIds.add(String.valueOf(deptId)); 
						}*/
					}else if(request.getSession().getAttribute("empIds") != null){
						empIds = new ArrayList<String>();
						//Long empid = Long.parseLong(request.getSession().getAttribute("empIds").toString());
						String empid = (request.getSession().getAttribute("empIds").toString());
						empIds.add(empid.toString());
						List<UserDO> userList = userService.retriveByEmpId(empid.toString());
						if(userList.get(0).getRole().getRoleId() != null){
							List<RolesDO> roleList = rolesService.retriveById(userList.get(0).getRole().getRoleId());
							if(roleList != null && roleList.size() > 0){
								if(roleList.get(0).getName().equalsIgnoreCase(CommonConstants.SUPERADMIN)){
									i=1;
								}
							}else{
								empIds = new ArrayList<String>();
							}
						}else{
							empIds = new ArrayList<String>();
						}
						//empIds = employeeService.retrieveByDeptIds(deptIds);
					}
					if(i == 1){
						List<LeadDO> leadList = leadService.retrieveAll(inputJSON.get(CommonConstants.TYPE).toString());
						respJSON = LeadUtil.getLeadListWithOutPic(leadList).toString();
					}else{
						//List<LeadDO> leadList = leadService.retrieve(inputJSON.get(CommonConstants.TYPE).toString(), empIds);//retrieveAll(inputJSON.get(CommonConstants.TYPE).toString())/*inputJSON.get(CommonConstants.TYPE).toString(), empIds*/;
						//respJSON = LeadUtil.getLeadList(leadList).toString();
						List<LeadDO> leadList = leadService.retrieveAll(inputJSON.get(CommonConstants.TYPE).toString());
						respJSON = LeadUtil.getLeadListWithOutPic(leadList).toString();
					}
					
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveRecentlead/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveRecentlead(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && inputJSON.get(CommonConstants.ID) != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty() 
						&& inputJSON.get(CommonConstants.TYPE) != null && !inputJSON.get(CommonConstants.TYPE).toString().isEmpty()){
						List<LeadDO> leadList = leadService.retrieveLatestLead(inputJSON.get(CommonConstants.TYPE).toString(),inputJSON.get(CommonConstants.ID).toString());
						respJSON = LeadUtil.getLeadListWithOutPic(leadList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				//List<Long> deptIds = new ArrayList<Long>();
				List<LeadDO> leadList = new ArrayList<LeadDO>();
				/*if(request.getSession().getAttribute("deptIds") != null){
					deptIds = (List<Long>) request.getSession().getAttribute("deptIds");
				}
				List<Long> empIds = new ArrayList<Long>();
				if(deptIds != null && deptIds.size() > 0){
					empIds = employeeService.retrieveByDeptIds(deptIds);
				}else if(request.getSession().getAttribute("empId") != null){
					empIds = new ArrayList<Long>();
					empIds.add((Long)request.getSession().getAttribute("empId"));
				}*/
				leadList = leadService.retrieveAll();
				respJSON = LeadUtil.getLeadLimitData(leadList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveByType/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByType(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
					if(inputJSON != null && !inputJSON.get(CommonConstants.TYPE).toString().isEmpty()){
						List<LeadDO> leadList = leadService.retrieveAll(inputJSON.get(CommonConstants.TYPE).toString());
						respJSON = LeadUtil.getLeadList(leadList).toString();
				}else{
					return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
				}
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveAllForChart/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveAllForChart(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.TYPE).toString().isEmpty()){
					Long leadList = leadService.retrieveAllForChart(inputJSON.get(CommonConstants.TYPE).toString());
					respJSON = LeadUtil.getLeadList(leadList).toString();
				}else{
					return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
				}
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveProjectForChart", method = RequestMethod.GET)
	public @ResponseBody String retrieveProjectForChart(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<Long> leadList = leadService.retrieveForCustomerIds();
				if(leadList != null && leadList.size() > 0){
					Long count = opportunitiesService.retrieveForCustomer(leadList);
					if(count == null){
						count = 0L;
					}
					respJSON = LeadUtil.getLeadList(count).toString();
				}else{
					respJSON = LeadUtil.getLeadList(0L).toString();
				}
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveFreezedCustomer", method = RequestMethod.GET)
	public @ResponseBody String retrieveFreezedCustomer(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<Long> leadList = leadService.retrieveForCustomerIds();
				if(leadList != null && leadList.size() > 0){
					Long count = opportunitiesService.retrieveForCustomer(leadList);
					if(count == null){
						count = 0L;
					}
					Long freezCustomer = leadList.size() - count;
					respJSON = LeadUtil.getLeadList(freezCustomer).toString();
				}else{
					respJSON = LeadUtil.getLeadList(0L).toString();
				}
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveLead", method = RequestMethod.GET)
	public @ResponseBody String retrieveLead(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<LeadDO> leadList = leadService.retrieveAll("L");
				respJSON = LeadUtil.getLeadListForChart(leadList).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		LeadDO leadDO = new LeadDO();
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<LeadDO> LeadList = leadService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		leadDO = LeadList.get(0);
			 		leadDO.setName(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty() ? inputJSON.get(CommonConstants.NAME).toString() : "");

			 		if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
			 			List<LeadStatusDO> leadStatusList = leadStatusService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.STATUS).toString()));
			 			if(leadStatusList != null && leadStatusList.size() > 0){
			 				leadDO.setStatus(leadStatusList.get(0));
			 			}
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.INDUSTRY) != null && !inputJSON.get(CommonConstants.INDUSTRY).toString().isEmpty()){
			 			List<IndustryDO> industryList = industryService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.INDUSTRY).toString()));
			 			if(industryList != null && industryList.size() > 0){
			 				leadDO.setIndustry(industryList.get(0));
			 			}
			 		}
			 		leadDO.setSource(inputJSON.get(CommonConstants.SOURCE) != null && !inputJSON.get(CommonConstants.SOURCE).toString().isEmpty() ? inputJSON.get(CommonConstants.SOURCE).toString() : "");
			 		
			 		if(inputJSON.get(CommonConstants.MOBILE) != null && !inputJSON.get(CommonConstants.MOBILE).toString().isEmpty()){
			 			leadDO.setMobile(inputJSON.get(CommonConstants.MOBILE).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.PHONE) != null && !inputJSON.get(CommonConstants.PHONE).toString().isEmpty()){
			 			leadDO.setPhone(inputJSON.get(CommonConstants.PHONE).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.STATE) != null && !inputJSON.get(CommonConstants.STATE).toString().isEmpty()){
			 			List<StateDO> stateList = stateService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.STATE).toString()));
			 			if(stateList != null && stateList.size() > 0){
			 				leadDO.setState(stateList.get(0));
			 			}
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.CITY) != null && !inputJSON.get(CommonConstants.CITY).toString().isEmpty()){
			 			leadDO.setCity(inputJSON.get(CommonConstants.CITY).toString());
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.LEAD_DATE) != null && !inputJSON.get(CommonConstants.LEAD_DATE).toString().isEmpty()){
			 			leadDO.setLeaddate(CommonUtil.convertStringToSqlDateMonth(inputJSON.get(CommonConstants.LEAD_DATE).toString()));
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.FOLLOW_UP_DATE) != null && !inputJSON.get(CommonConstants.FOLLOW_UP_DATE).toString().isEmpty()){
			 			leadDO.setFollowupdate(CommonUtil.convertStringToDateTimePick(inputJSON.get(CommonConstants.FOLLOW_UP_DATE).toString()));
			 		}
			 		
			 		leadDO.setEmail(inputJSON.get(CommonConstants.EMAIL) != null && !inputJSON.get(CommonConstants.EMAIL).toString().isEmpty() ? inputJSON.get(CommonConstants.EMAIL).toString() : "");
			 		leadDO.setAddress(inputJSON.get(CommonConstants.ADDRESS) != null && !inputJSON.get(CommonConstants.ADDRESS).toString().isEmpty() ? inputJSON.get(CommonConstants.ADDRESS).toString() : "");
			 		leadDO.setComment(inputJSON.get(CommonConstants.COMMENT) != null && !inputJSON.get(CommonConstants.COMMENT).toString().isEmpty() ? inputJSON.get(CommonConstants.COMMENT).toString() : "");
			 		
			 		if(inputJSON.get(CommonConstants.LEADTYPE) != null && !inputJSON.get(CommonConstants.LEADTYPE).toString().isEmpty()){
			 			List<LeadTypeDO> leadTypeList = leadTypeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.LEADTYPE).toString()));
			 			if(leadTypeList != null && leadTypeList.size() > 0){
			 				leadDO.setLeadType(leadTypeList.get(0));
			 			}
			 		}
			 		/*if(!inputJSON.get(CommonConstants.LEADTYPE).toString().isEmpty()){
			 			leadDO.setLeadType(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.LEADTYPE).toString() : ""));
			 		}*/
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			leadDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		leadDO.setCreatedon(new Date());
			 		leadDO.setUpdatedon(new Date());
			 		leadService.update(leadDO);
			 		if(leadDO.getLeadId() != null){
				 		NotesDO notesDO = new NotesDO();
				 		if(inputJSON.get(CommonConstants.NOTES) != null && !inputJSON.get(CommonConstants.NOTES).toString().isEmpty()){
				 			List<NotesDO> notesList = notesService.retrieveByLeadIdAndNote(leadDO.getLeadId(), inputJSON.get(CommonConstants.NOTES).toString());
				 			if(notesList != null  && notesList.size() > 0){
				 				notesDO = notesList.get(0);
				 				notesDO.setLead(leadDO);
					 			if(inputJSON.get(CommonConstants.NOTES) != null &&  !inputJSON.get(CommonConstants.NOTES).toString().isEmpty()){
					 				notesDO.setNote(inputJSON.get(CommonConstants.NOTES).toString());
						 		}
					 			if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 				notesDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 				notesDO.setEmpId(inputJSON.get(CommonConstants.UPDATED_BY).toString());
						 		}
						 		notesDO.setUpdatedon(new Date());
						 		notesService.update(notesDO);
				 			}else{
				 				notesDO.setLead(leadDO);
					 			if(inputJSON.get(CommonConstants.NOTES) != null &&  !inputJSON.get(CommonConstants.NOTES).toString().isEmpty()){
					 				notesDO.setNote(inputJSON.get(CommonConstants.NOTES).toString());
						 		}
					 			if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 				notesDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 				notesDO.setEmpId(inputJSON.get(CommonConstants.UPDATED_BY).toString());
						 		}
						 		notesDO.setUpdatedon(new Date());
						 		notesService.persist(notesDO);
				 			}
				 			
				 		}
				 	}
			 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Lead Updated");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponseId(leadDO.getLeadId().toString()).toString();
	}
	
	@RequestMapping(value = "/convertAsCustomer", method = RequestMethod.POST)
	public @ResponseBody String convertAsCustomer(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<LeadDO> LeadList = leadService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					if(LeadList != null && LeadList.size() > 0){
						LeadDO leadDO = new LeadDO();
						leadDO = LeadList.get(0);
						if(!inputJSON.get(CommonConstants.COMMENT).toString().isEmpty()){
							leadDO.setConvertCustomerComments(inputJSON.get(CommonConstants.COMMENT).toString());
						}
						leadDO.setUpdatedon(new Date());
						leadDO.setConvertedDate(new Date());
						leadDO.setType("C");
						leadService.update(leadDO);
					}
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@SuppressWarnings("deprecation")
	@RequestMapping(value = "/retrieveBetweenDate/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String retrieveBetweenDate(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.FROMDATE).toString().isEmpty() &&
						!inputJSON.get(CommonConstants.TODATE).toString().isEmpty()){
					Date frmDate = new Date(inputJSON.get(CommonConstants.FROMDATE).toString()); 
					Date endDate = new Date(inputJSON.get(CommonConstants.TODATE).toString());  
				    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");  
				    String fromDate = formatter.format(frmDate); 
				    String toDate = formatter.format(endDate); 
					//String fromDate = CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.FROMDATE).toString()).toString();
					//String toDate = CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.TODATE).toString()).toString();
					/*fromDate = fromDate.substring(0, 10)+" 00:00:00";
					toDate = toDate.substring(0, 10)+" 23:59:59";*/
					fromDate = fromDate+" 00:00:00";
					toDate = toDate+" 23:59:59";
					Date formattedDate = CommonUtil.convertStringToDateTimeFmt(fromDate);
					Date formattedDate1 = CommonUtil.convertStringToDateTimeFmt(toDate);
					List<LeadDO> LeadList = leadService.retrieveBetweenDate(formattedDate,formattedDate1);
					respJSON = LeadUtil.getLeadListForChart(LeadList).toString();
				}else{
					List<LeadDO> leadList = leadService.retrieveAll("L");
					respJSON = LeadUtil.getLeadListForChart(leadList).toString();
				}
			
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@SuppressWarnings("deprecation")
	@RequestMapping(value = "/retrieveBetweenDateByEmp/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String retrieveBetweenDateByEmp(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
			JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
			if(inputJSON != null && !inputJSON.get(CommonConstants.FROMDATE).toString().isEmpty() &&
						!inputJSON.get(CommonConstants.TODATE).toString().isEmpty()){
					/*String fromDate = inputJSON.get(CommonConstants.FROMDATE).toString();
					String toDate = inputJSON.get(CommonConstants.TODATE).toString();*/
					/*String fromDate = CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.FROMDATE).toString()).toString();
					String toDate = CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.TODATE).toString()).toString();*/
					Date frmDate = new Date(inputJSON.get(CommonConstants.FROMDATE).toString()); 
					Date endDate = new Date(inputJSON.get(CommonConstants.TODATE).toString());  
				    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");  
				    String fromDate = formatter.format(frmDate); 
				    String toDate = formatter.format(endDate);
					String empId = "";
					if(inputJSON.get(CommonConstants.EMPID) != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
						empId = inputJSON.get(CommonConstants.EMPID).toString();
					}
					/*fromDate = fromDate.substring(0, 11)+" 00:00:00";
					toDate = toDate.substring(0, 11)+" 23:59:59";*/
					fromDate = fromDate + " 00:00:00";
					toDate = toDate + " 23:59:59";
					Date formattedDate = CommonUtil.convertStringToDateTimeFmt(fromDate);
					Date formattedDate1 = CommonUtil.convertStringToDateTimeFmt(toDate);
					List<LeadDO> LeadList = new ArrayList<LeadDO>();
					if(empId != null && !empId.isEmpty()){
						LeadList = leadService.retrieveBetweenDateByEmp(formattedDate,formattedDate1, empId);
					}else{
						LeadList = leadService.retrieveBetweenDate(formattedDate,formattedDate1);
					}
					if(LeadList != null && LeadList.size() > 0){
						respJSON = LeadUtil.getLeadListForChart(LeadList).toString();
					}
				}else if(inputJSON.get(CommonConstants.EMPID) != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
					List<LeadDO> leadList = leadService.retrieveLeadByEmp(inputJSON.get(CommonConstants.EMPID).toString(),"L");
					respJSON = LeadUtil.getLeadListForChart(leadList).toString();
				}else{
					List<LeadDO> leadList = leadService.retrieveAll("L");
					respJSON = LeadUtil.getLeadListForChart(leadList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildSuccessResponse().toString();
	}
	

	@RequestMapping(value = "/importLeads", method = RequestMethod.POST)
	public @ResponseBody String importEmp(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
				Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY);
				Object type =  inputJSON.get(CommonConstants.TYPE); 
				org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
				
				JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
				List<LeadDO> leadDOlist = new ArrayList<LeadDO>();
				for (int i=0; i < fileData.size(); i++){
					LeadDO leadDO = new LeadDO();
					JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
					if(rowJSON != null) {
						String leadName = colName.get(CommonConstants.NAME).toString();
						if(leadName != null && !leadName.isEmpty()){
							leadName = leadName.trim();
							leadDO.setName(rowJSON.get(leadName)!=null && !rowJSON.get(leadName).toString().isEmpty() ? rowJSON.get(leadName).toString().toUpperCase() : "");
						}
						if(colName.get(CommonConstants.STATUS) != null && !colName.get(CommonConstants.STATUS).toString().isEmpty()){
							if(rowJSON.get(colName.get(CommonConstants.STATUS))!=null && !rowJSON.get(colName.get(CommonConstants.STATUS)).toString().isEmpty()){
								List<LeadStatusDO> leadStatusList = leadStatusService.retrieveByActiveName(rowJSON.get(colName.get(CommonConstants.STATUS)).toString());
								if(leadStatusList != null && leadStatusList.size() > 0){
									 leadDO.setStatus(leadStatusList.get(0));
								}
					 		}
						}
						if(colName.get(CommonConstants.STATE) != null && !colName.get(CommonConstants.STATE).toString().isEmpty()){
							if(rowJSON.get(colName.get(CommonConstants.STATE))!=null && !rowJSON.get(colName.get(CommonConstants.STATE)).toString().isEmpty()){
								List<StateDO> StateList = stateService.retrieveByActiveName(rowJSON.get(colName.get(CommonConstants.STATE)).toString());
								if(StateList != null && StateList.size() > 0){
									leadDO.setState(StateList.get(0));
								}
					 		}
						}
						if(colName.get(CommonConstants.LEADTYPE) != null && !colName.get(CommonConstants.LEADTYPE).toString().isEmpty()){
							if(rowJSON.get(colName.get(CommonConstants.LEADTYPE))!=null && !rowJSON.get(colName.get(CommonConstants.LEADTYPE)).toString().isEmpty()){
								List<LeadTypeDO> leadTypeList = leadTypeService.retrieveByActvieName(rowJSON.get(colName.get(CommonConstants.LEADTYPE)).toString());
								if(leadTypeList != null && leadTypeList.size() > 0){
									leadDO.setLeadType(leadTypeList.get(0));
								}
					 		}
						}
						if(colName.get(CommonConstants.INDUSTRY) != null && !colName.get(CommonConstants.INDUSTRY).toString().isEmpty()){
							if(rowJSON.get(colName.get(CommonConstants.INDUSTRY))!=null && !rowJSON.get(colName.get(CommonConstants.INDUSTRY)).toString().isEmpty()){
								List<IndustryDO> industryList = industryService.retrieveByActiveName(rowJSON.get(colName.get(CommonConstants.INDUSTRY)).toString());
								if(industryList != null && industryList.size() > 0){
									leadDO.setIndustry(industryList.get(0));
								} else{
									IndustryDO industryDO = new IndustryDO();
							 		industryDO.setName(rowJSON.get(colName.get(CommonConstants.INDUSTRY)).toString());
							 		industryDO.setUpdatedon(new Date());
							 		industryDO.setCreatedon(new Date());
							 		if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
							 			industryDO.setUpdatedby(updatedBy.toString());
							 			industryDO.setCreatedby((updatedBy.toString()));
							 		}
							 		industryDO.setStatus(CommonConstants.ACTIVE);
							 		if(!industryService.persist(industryDO)){
										return CommonWebUtil.buildErrorResponse("Industry Already Added").toString();
									}
							 		if(industryDO.getIndustryId() != null){
							 			leadDO.setIndustry(industryDO);
							 		}
								}
					 		}
						}
						if(colName.get(CommonConstants.CITY) != null && !colName.get(CommonConstants.CITY).toString().isEmpty()){
					 		if(rowJSON.get(colName.get(CommonConstants.CITY))!=null && !rowJSON.get(colName.get(CommonConstants.CITY)).toString().isEmpty()){
					 			leadDO.setCity(rowJSON != null ? rowJSON.get(colName.get(CommonConstants.CITY)).toString() : "");
					 		}
						}
						if(colName.get(CommonConstants.FOLLOW_UP_DATE) != null && !colName.get(CommonConstants.FOLLOW_UP_DATE).toString().isEmpty()){
					 		if(rowJSON.get(colName.get(CommonConstants.FOLLOW_UP_DATE))!=null && !rowJSON.get(colName.get(CommonConstants.FOLLOW_UP_DATE)).toString().isEmpty()){
					 			leadDO.setFollowupdate(CommonUtil.convertStringToDateTimePick(rowJSON != null ? rowJSON.get(colName.get(CommonConstants.FOLLOW_UP_DATE)).toString() : ""));
					 		}
						}
						if(colName.get(CommonConstants.LEAD_DATE) != null && !colName.get(CommonConstants.LEAD_DATE).toString().isEmpty()){
					 		if(rowJSON.get(colName.get(CommonConstants.LEAD_DATE))!=null && !rowJSON.get(colName.get(CommonConstants.LEAD_DATE)).toString().isEmpty()){
					 			leadDO.setLeaddate(CommonUtil.convertStringToSqlDate(rowJSON != null ? rowJSON.get(colName.get(CommonConstants.LEAD_DATE)).toString() : ""));
					 		}
						}
						if(colName.get(CommonConstants.SOURCE) != null && !colName.get(CommonConstants.SOURCE).toString().isEmpty()){
							leadDO.setSource(rowJSON.get(colName.get(CommonConstants.SOURCE))!=null && !rowJSON.get(colName.get(CommonConstants.SOURCE)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.SOURCE)).toString() : "");
						}
						if(colName.get(CommonConstants.MOBILE) != null && !colName.get(CommonConstants.MOBILE).toString().isEmpty()){
					 		if(rowJSON.get(colName.get(CommonConstants.MOBILE))!=null && !rowJSON.get(colName.get(CommonConstants.MOBILE)).toString().isEmpty()){
					 			leadDO.setMobile(rowJSON != null  ? rowJSON.get(colName.get(CommonConstants.MOBILE)).toString() : "");
					 		}
						}
						if(colName.get(CommonConstants.PHONE) != null && !colName.get(CommonConstants.PHONE).toString().isEmpty()){
					 		if(rowJSON.get(colName.get(CommonConstants.PHONE))!=null && !rowJSON.get(colName.get(CommonConstants.PHONE)).toString().isEmpty()){
					 			leadDO.setPhone(rowJSON != null ? rowJSON.get(colName.get(CommonConstants.PHONE)).toString() : "");
					 		}
						}
						if(colName.get(CommonConstants.EMAIL) != null && !colName.get(CommonConstants.EMAIL).toString().isEmpty()){
							leadDO.setEmail(rowJSON.get(colName.get(CommonConstants.EMAIL))!=null && !rowJSON.get(colName.get(CommonConstants.EMAIL)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.EMAIL)).toString() : "");
						}
						if(colName.get(CommonConstants.ADDRESS) != null && !colName.get(CommonConstants.ADDRESS).toString().isEmpty()){
							leadDO.setAddress(rowJSON.get(colName.get(CommonConstants.ADDRESS))!=null && !rowJSON.get(colName.get(CommonConstants.ADDRESS)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.ADDRESS)).toString() : "");
						}
						if(colName.get(CommonConstants.COMMENT) != null && !colName.get(CommonConstants.COMMENT).toString().isEmpty()){
							leadDO.setComment(rowJSON.get(colName.get(CommonConstants.COMMENT))!=null && !rowJSON.get(colName.get(CommonConstants.COMMENT)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.COMMENT)).toString() : "");
						}
						/*if(colName.get(CommonConstants.LEADTYPE) != null){
					 		if(rowJSON.get(colName.get(CommonConstants.LEADTYPE)) != null && !rowJSON.get(colName.get(CommonConstants.LEADTYPE)).toString().isEmpty()){
					 			if(leadTypeList != null && leadTypeList.size() > 0){
									for(LeadTypeDO leadTypeDO : leadTypeList){
										if(leadTypeDO.getName().toLowerCase().equalsIgnoreCase(rowJSON.get(colName.get(CommonConstants.LEADTYPE)).toString().toLowerCase())){
											leadDO.setLeadType(leadTypeDO.getId());
										}
									}
								} else{
									leadDO.setLeadType(Long.parseLong(rowJSON != null ? rowJSON.get(colName.get(CommonConstants.LEADTYPE)).toString() : ""));
								}
					 			//leadDO.setLeadType(Long.parseLong(rowJSON != null ? rowJSON.get(colName.get(CommonConstants.LEADTYPE)).toString() : ""));
					 		}
						}*/
				 		leadDO.setType(!type.toString().isEmpty() ? type.toString() : "");
				 		leadDO.setCreatedon(new Date());
				 		if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
				 			leadDO.setUpdatedby(updatedBy.toString());
				 			leadDO.setEmpId((updatedBy.toString()));
				 		}
				 		leadDO.setUpdatedon(new Date());
				 		leadDO.setIsDeleted("n");
				 		leadDOlist.add(leadDO);
				 		leadService.persist(leadDO);
				 		
				 		String fname = null;
				 		String lname =  null;
				 		String mname =  null;
				 		if(leadDO.getLeadId() != null){
					 		ContactDO contactDO = new ContactDO();
					 		NotesDO notesDO = new NotesDO();
					 		if(colName.get(CommonConstants.NOTES) != null && !colName.get(CommonConstants.NOTES).toString().isEmpty()){
					 			if(rowJSON.get(colName.get(CommonConstants.NOTES)) != null && !rowJSON.get(colName.get(CommonConstants.NOTES)).toString().isEmpty()){
					 				notesDO.setNote(rowJSON.get(colName.get(CommonConstants.NOTES)).toString());
					 				notesDO.setLead(leadDO);
					 				if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
						 				notesDO.setUpdatedBy(updatedBy.toString());
						 				notesDO.setCreatedby(updatedBy.toString());
						 				notesDO.setEmpId(updatedBy.toString());
							 		}
					 				notesDO.setCreatedon(new Date());
							 		notesDO.setUpdatedon(new Date());
							 		notesService.persist(notesDO);
						 		}
					 		}
					 		if(colName.get(CommonConstants.CONTACTNAME) != null && !colName.get(CommonConstants.CONTACTNAME).toString().isEmpty()){
					 			if(rowJSON.get(colName.get(CommonConstants.CONTACTNAME))!=null && !rowJSON.get(colName.get(CommonConstants.CONTACTNAME)).toString().isEmpty()){
						 			if(rowJSON.get(colName.get(CommonConstants.CONTACTNAME)).toString().split(" ").length > 2){
						 				fname = rowJSON.get(colName.get(CommonConstants.CONTACTNAME))!=null && !rowJSON.get(colName.get(CommonConstants.CONTACTNAME)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.CONTACTNAME)).toString().split(" ")[0] : "";
							 			lname = rowJSON.get(colName.get(CommonConstants.CONTACTNAME))!=null && !rowJSON.get(colName.get(CommonConstants.CONTACTNAME)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.CONTACTNAME)).toString().split(" ")[1] : "";
							 			mname = rowJSON.get(colName.get(CommonConstants.CONTACTNAME))!=null && !rowJSON.get(colName.get(CommonConstants.CONTACTNAME)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.CONTACTNAME)).toString().split(" ")[2] : "";
							 			contactDO.setFirstname(fname);
							 			contactDO.setLastname(lname);
							 			contactDO.setMiddlename(mname);
						 			} else if(rowJSON.get(colName.get(CommonConstants.CONTACTNAME)).toString().split(" ").length > 1){
						 				fname = rowJSON.get(colName.get(CommonConstants.CONTACTNAME))!=null && !rowJSON.get(colName.get(CommonConstants.CONTACTNAME)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.CONTACTNAME)).toString().split(" ")[0] : "";
							 			lname = rowJSON.get(colName.get(CommonConstants.CONTACTNAME))!=null && !rowJSON.get(colName.get(CommonConstants.CONTACTNAME)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.CONTACTNAME)).toString().split(" ")[1] : "";
							 			contactDO.setFirstname(fname);
							 			contactDO.setLastname(lname);
						 			} else if(rowJSON.get(colName.get(CommonConstants.CONTACTNAME)).toString().split(" ").length == 1) {
						 				fname = rowJSON.get(colName.get(CommonConstants.CONTACTNAME))!=null && !rowJSON.get(colName.get(CommonConstants.CONTACTNAME)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.CONTACTNAME)).toString().split(" ")[0] : "";
						 				contactDO.setFirstname(fname);
						 			}
						 			//contactDO.setLeadtype(leadDO.getLeadId());
						 			List<LeadDO> leadList = leadService.retrieveById(leadDO.getLeadId());
						 			if(leadList != null && leadList.size() > 0){
						 				contactDO.setLead(leadList.get(0));
						 			}
						 			contactDO.setUpdatedon(new Date());
						 			contactDO.setCreatedon(new Date());
							 		contactDO.setIsDeleted("n");
					 			
						 			if(colName.get(CommonConstants.MOBILE1) != null && !colName.get(CommonConstants.MOBILE1).toString().isEmpty()){
						 				if(rowJSON.get(colName.get(CommonConstants.MOBILE1))!=null && !rowJSON.get(colName.get(CommonConstants.MOBILE1)).toString().isEmpty()){
						 					contactDO.setMobile1(rowJSON.get(colName.get(CommonConstants.MOBILE1)).toString());
						 				}
						 			}	
						 			if(colName.get(CommonConstants.PRIMARYEMAIL) != null && !colName.get(CommonConstants.PRIMARYEMAIL).toString().isEmpty()){
						 				if(rowJSON.get(colName.get(CommonConstants.PRIMARYEMAIL))!=null && !rowJSON.get(colName.get(CommonConstants.PRIMARYEMAIL)).toString().isEmpty()){
						 					contactDO.setPrimaryemail(rowJSON.get(colName.get(CommonConstants.PRIMARYEMAIL)).toString());
						 				}
						 			}
							 		if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
							 			contactDO.setUpdatedby(updatedBy.toString());
							 			contactDO.setCreatedby(updatedBy.toString());
							 			contactDO.setEmpId(updatedBy.toString());
							 		}
							 		contactService.persist(contactDO);
					 			}
					 		}
				 		}
					}
				}
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Leads Created");
				//leadService.persistList(leadDOlist);
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}	
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}

	@RequestMapping(value = "/retrieveLeadByEmp/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveLeadByEmp(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
					List<LeadDO> LeadList = leadService.retrieveLeadByEmp(inputJSON.get(CommonConstants.EMPID).toString(),inputJSON.get(CommonConstants.TYPE).toString());
					respJSON = LeadUtil.getLeadList(LeadList).toString();
				}
				
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveLeadByPermenantEmp/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveLeadByPermenantEmp(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
					List<LeadDO> LeadList = leadService.retrieveLeadByPermenantEmp((inputJSON.get(CommonConstants.EMPID).toString()),inputJSON.get(CommonConstants.TYPE).toString());
					respJSON = LeadUtil.getLeadList(LeadList).toString();
				}
				
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/transferLead", method = RequestMethod.POST)
	public @ResponseBody String transferLead(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				LeadDO leadDO = new LeadDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
				if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<LeadDO> LeadList = leadService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		leadDO = LeadList.get(0);
					String transferTo=inputJSON.get(CommonConstants.TRANSFER_TO) != null && !inputJSON.get(CommonConstants.TRANSFER_TO).toString().isEmpty() ? inputJSON.get(CommonConstants.TRANSFER_TO).toString(): null;
					String transferFrom=inputJSON.get(CommonConstants.TRANSFER_FORM) != null && !inputJSON.get(CommonConstants.TRANSFER_FORM).toString().isEmpty() ? inputJSON.get(CommonConstants.TRANSFER_FORM).toString(): null;
					String transferType=inputJSON.get(CommonConstants.TRANSFER_TYPE) != null && !inputJSON.get(CommonConstants.TRANSFER_TYPE).toString().isEmpty() ? inputJSON.get(CommonConstants.TRANSFER_TYPE).toString(): "";
					Date fromDate = null;
					Date toDate = null;
					if(transferType != null && !transferType.isEmpty()){
						leadDO.setTransferfrom(transferFrom);
						leadDO.setEmpId(String.valueOf(transferTo));
						leadDO.setTransferType(transferType);
						if(transferType.equalsIgnoreCase("T")){
							fromDate = inputJSON.get(CommonConstants.FROMDATE) != null && !inputJSON.get(CommonConstants.FROMDATE).toString().isEmpty() ? CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.FROMDATE).toString()): null;
							toDate = inputJSON.get(CommonConstants.TODATE) != null && !inputJSON.get(CommonConstants.TODATE).toString().isEmpty() ? CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.TODATE).toString()): null;
							leadDO.setTransferFromDate(fromDate);
							leadDO.setTransferToDate(toDate);
						}
						if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			leadDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		leadDO.setUpdatedon(new Date());
				 		leadService.update(leadDO);
					}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveTransferLead/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveLeadByTransferType(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.TYPE).toString().isEmpty()){
					/*List<LeadDO> LeadList = leadService.retrieveLeadByTransferType("T",inputJSON.get(CommonConstants.TYPE).toString());*/
					List<LeadDO> LeadList = leadService.retrieveLeadByAllTransferType(inputJSON.get(CommonConstants.TYPE).toString());
					respJSON = LeadUtil.getLeadList(LeadList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveTransferLeadById/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveTransferLeadById(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.TYPE).toString().isEmpty() && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
					List<LeadDO> LeadList = leadService.retrieveTransferLeadById("T",inputJSON.get(CommonConstants.TYPE).toString(),(inputJSON.get(CommonConstants.EMPID).toString()));
					respJSON = LeadUtil.getLeadList(LeadList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveCurrentFollowupLeads/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveCurrentFollowupLeads(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && inputJSON.get(CommonConstants.EMPID) != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
						List<LeadDO> LeadList = leadService.retrieveCurrentFollowupLeads((inputJSON.get(CommonConstants.EMPID).toString()));
						respJSON = LeadUtil.getLeadList(LeadList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveFollowupLeadByDate", method = RequestMethod.POST)
	public @ResponseBody String retrieveFollowupLeadByDate(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
				if(inputJSON != null && inputJSON.get(CommonConstants.EMPID) != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()
						&& inputJSON.get(CommonConstants.DATE) != null && !inputJSON.get(CommonConstants.DATE).toString().isEmpty()){
						List<LeadDO> LeadList = leadService.retrieveFollowupLeadByDate((inputJSON.get(CommonConstants.EMPID).toString()), CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.DATE).toString()));
						respJSON = LeadUtil.getLeadList(LeadList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/revokeLead", method = RequestMethod.POST)
	public @ResponseBody String revokeLead(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				LeadDO leadDO = new LeadDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
				if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<LeadDO> LeadList = leadService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		leadDO = LeadList.get(0);
			 		leadDO.setEmpId(String.valueOf(leadDO.getTransferfrom()));
			 		leadDO.setTransferfrom(null);
					leadDO.setTransferType(null);
					leadDO.setTransferFromDate(null);
					leadDO.setTransferToDate(null);
					leadDO.setUpdatedon(new Date());
					if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			leadDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		leadService.update(leadDO);
				}
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/updatepic", method = RequestMethod.POST)
	public @ResponseBody String updatepic(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				LeadDO leadDO = new LeadDO();
				/*JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("data").toString());*/
			 	if (request.getParameter("id") != null){
			 		List<LeadDO> leadList = leadService.retrieveById(Long.parseLong(request.getParameter("id")));
			 		if(request.getParameter("file") != null && !request.getParameter("file").equals("")){
			 			if(leadList != null && leadList.size() > 0){
				 			leadDO = leadList.get(0);
				 			leadDO.setPhoto(request.getParameter("file"));
				 			leadDO.setUpdatedon(new Date());
					 		leadService.update(leadDO);
				 		}
			 		}else{
			 			if(leadList != null && leadList.size() > 0){
				 			leadDO = leadList.get(0);
				 			leadDO.setPhoto(null);
				 			leadDO.setUpdatedon(new Date());
					 		leadService.update(leadDO);
				 		}
			 		}
			 		
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public @ResponseBody String delete(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				List<LeadDO> leadList = new ArrayList<LeadDO>();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && inputJSON.get(CommonConstants.IDS) != null){
			 		JSONArray resultJSONArray = new JSONArray();
			 		resultJSONArray.put(inputJSON.get(CommonConstants.IDS));
			 		String rec = (String) resultJSONArray.get(0).toString();
			 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
			 		String updatedBy = "";
		 			if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
		 				updatedBy = inputJSON.get(CommonConstants.UPDATED_BY).toString();
			 		}
			 		for (int i = 0; i < resultJSONArray1.size(); i++) {
			 			JSONObject inputJSON1 = (JSONObject) resultJSONArray1.get(i);
			 			boolean flag = true;
			 			if(!inputJSON1.get(CommonConstants.ID).toString().isEmpty()){
			 				List<ContactDO> contactList = contactService.retriveByLeadId(Long.parseLong(inputJSON1.get(CommonConstants.ID).toString()));
			 				if(contactList != null && contactList.size() > 0){
			 					flag = false;
			 				}
			 				
			 				List<OpportunitiesDO> opportunitiesList = opportunitiesService.retrieveByCustomerId(Long.parseLong(inputJSON1.get(CommonConstants.ID).toString()));
			 				if(opportunitiesList != null && opportunitiesList.size() > 0){
			 					flag = false;
			 				}
			 				
			 				List<ProposalDO> proposalList = proposalService.retrieveCustomerId(Long.parseLong(inputJSON1.get(CommonConstants.ID).toString()));
			 				if(proposalList != null && proposalList.size() > 0){
			 					flag = false;
			 				}
			 				
			 				List<RequirementsDO> requirementList = requirementsService.retrieveByCustomerId(Long.parseLong(inputJSON1.get(CommonConstants.ID).toString()));
			 				if(requirementList != null && requirementList.size() > 0){
			 					flag = false;
			 				}
			 				if(resultJSONArray1.size() == 1){
			 					if(flag){
				 					List<LeadDO> leadDoList = leadService.retrieveById(Long.parseLong(inputJSON1.get(CommonConstants.ID).toString()));
				 					if(leadDoList != null && leadDoList.size() > 0){
				 						leadDoList.get(0).setIsDeleted("y");
				 						leadDoList.get(0).setUpdatedby(updatedBy);
				 						leadList.add(leadDoList.get(0));
				 					}
				 				}else{
				 					return CommonWebUtil.buildErrorResponse("Cannot Delete").toString();
			 					}
			 				}else{
			 					if(flag){
				 					List<LeadDO> leadDoList = leadService.retrieveById(Long.parseLong(inputJSON1.get(CommonConstants.ID).toString()));
				 					if(leadDoList != null && leadDoList.size() > 0){
				 						leadDoList.get(0).setIsDeleted("y");
				 						leadDoList.get(0).setUpdatedby(updatedBy);
				 						leadList.add(leadDoList.get(0));
				 					}
				 				}
			 				}
			 				
			 			}
			 		}
			 		leadService.update(leadList);
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
