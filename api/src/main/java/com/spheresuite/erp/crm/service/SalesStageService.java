package com.spheresuite.erp.crm.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.crm.dao.SalesStageDAO;
import com.spheresuite.erp.domainobject.SalesStageDO;

@Service
@Transactional
public class SalesStageService {
	static Logger logger = Logger.getLogger(SalesStageService.class.getName());
	
	@Autowired
	private SalesStageDAO salesStageDAO;
	
	@Transactional
	public boolean persist(SalesStageDO salesStageDO) {
		return salesStageDAO.persist(salesStageDO);
	}

	@Transactional
	public List<SalesStageDO> retrieveActive() {
		return salesStageDAO.retrieveActive();
	}
	
	@Transactional
	public List<SalesStageDO> retrieveById(Long id) {
		return salesStageDAO.retrieveById(id);
	}
	
	@Transactional
	public List<SalesStageDO> retrieve() {
		return salesStageDAO.retrieve();
	}
	
	@Transactional
	public boolean update(SalesStageDO salesStageDO) {
		return salesStageDAO.update(salesStageDO);
	}
	
	@Transactional
	public boolean persistList(List<SalesStageDO> salesStageDO) {
		return salesStageDAO.persistList(salesStageDO);
	}
}
