package com.spheresuite.erp.crm.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.MinutesOfMeetingNotesDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class MinutesOfMeetingNotesDAO {
	static Logger logger = Logger.getLogger(MinutesOfMeetingNotesDAO.class.getName());
	@Autowired
	private GenericDAOImpl<MinutesOfMeetingNotesDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public List<MinutesOfMeetingNotesDO> persist(List<MinutesOfMeetingNotesDO> momList)  {
		try {
			if(momList != null && momList.size() > 0){
				org.hibernate.Query query = this.sessionFactory.getCurrentSession().createQuery("delete from MinutesOfMeetingNotesDO e where e.mom.momId=:id")
					.setParameter(CommonConstants.ID, momList.get(0).getMom().getMomId());
				query.executeUpdate();
			}
			for (MinutesOfMeetingNotesDO invoiceTermDO : momList) {
				genericObject.persist(invoiceTermDO);
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return momList;
	}
	
	public boolean delete(Long momId)  {
		try {
			org.hibernate.Query query = this.sessionFactory.getCurrentSession().createQuery("delete from MinutesOfMeetingNotesDO e where e.mom.momId=:id")
					.setParameter(CommonConstants.ID, momId);
			query.executeUpdate();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return true;
	}
	
	@SuppressWarnings("unchecked")
	public List<MinutesOfMeetingNotesDO> retrieveById(Long momId)  {
		List<MinutesOfMeetingNotesDO> proposalList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(MinutesOfMeetingNotesDO.FIND_BY_MOM_ID)
			.setParameter(CommonConstants.MOM_ID, momId)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return proposalList;
	}
	
	@SuppressWarnings("unchecked")
	public List<MinutesOfMeetingNotesDO> retrieveForExport(Long momId, String pricing, String followups, String Samples)  {
		List<MinutesOfMeetingNotesDO> proposalList = null;
		try {
			if(pricing.equalsIgnoreCase(CommonConstants.TRUE) && followups.equalsIgnoreCase(CommonConstants.FALSE) && Samples.equalsIgnoreCase(CommonConstants.FALSE)){
				return this.sessionFactory.getCurrentSession().getNamedQuery(MinutesOfMeetingNotesDO.FIND_BY_MOM_ID_PRICING)
						.setParameter(CommonConstants.MOM_ID, momId)
						.setParameter(CommonConstants.PRICING, pricing)
						.list();
			}
			
			if(pricing.equalsIgnoreCase(CommonConstants.FALSE) && followups.equalsIgnoreCase(CommonConstants.TRUE) && Samples.equalsIgnoreCase(CommonConstants.FALSE)){
				return this.sessionFactory.getCurrentSession().getNamedQuery(MinutesOfMeetingNotesDO.FIND_BY_MOM_ID_FLLOWEUPS)
						.setParameter(CommonConstants.MOM_ID, momId)
						.setParameter(CommonConstants.FOLLOW_UPS, followups)
						.list();
			}
			
			if(pricing.equalsIgnoreCase(CommonConstants.FALSE) && followups.equalsIgnoreCase(CommonConstants.FALSE) && Samples.equalsIgnoreCase(CommonConstants.TRUE)){
				return this.sessionFactory.getCurrentSession().getNamedQuery(MinutesOfMeetingNotesDO.FIND_BY_MOM_ID_SAMPLES)
						.setParameter(CommonConstants.MOM_ID, momId)
						.setParameter(CommonConstants.SAMPLE, Samples)
						.list();
			}
			
			if(pricing.equalsIgnoreCase(CommonConstants.FALSE) && followups.equalsIgnoreCase(CommonConstants.FALSE) && Samples.equalsIgnoreCase(CommonConstants.FALSE)){
				return this.sessionFactory.getCurrentSession().getNamedQuery(MinutesOfMeetingNotesDO.FIND_BY_MOM_ID)
						.setParameter(CommonConstants.MOM_ID, momId)
						.list();
			}
			
			if(pricing.equalsIgnoreCase(CommonConstants.TRUE) && followups.equalsIgnoreCase(CommonConstants.TRUE) && Samples.equalsIgnoreCase(CommonConstants.TRUE)){
				return this.sessionFactory.getCurrentSession().getNamedQuery(MinutesOfMeetingNotesDO.FIND_BY_MOM_ID_FLLOWEUPS_SAMPLES_PRICING)
						.setParameter(CommonConstants.MOM_ID, momId)
						.setParameter(CommonConstants.SAMPLE, Samples)
						.setParameter(CommonConstants.FOLLOW_UPS, followups)
						.setParameter(CommonConstants.PRICING, pricing)
						.list();
			}
			
			if(pricing.equalsIgnoreCase(CommonConstants.TRUE) && followups.equalsIgnoreCase(CommonConstants.TRUE) && Samples.equalsIgnoreCase(CommonConstants.FALSE)){
				return this.sessionFactory.getCurrentSession().getNamedQuery(MinutesOfMeetingNotesDO.FIND_BY_MOM_ID_FLLOWEUPS_PRICING)
						.setParameter(CommonConstants.MOM_ID, momId)
						.setParameter(CommonConstants.FOLLOW_UPS, followups)
						.setParameter(CommonConstants.PRICING, pricing)
						.list();
			}
			
			if(pricing.equalsIgnoreCase(CommonConstants.FALSE) && followups.equalsIgnoreCase(CommonConstants.TRUE) && Samples.equalsIgnoreCase(CommonConstants.TRUE)){
				return this.sessionFactory.getCurrentSession().getNamedQuery(MinutesOfMeetingNotesDO.FIND_BY_MOM_ID_FLLOWEUPS_SAMPLES)
						.setParameter(CommonConstants.MOM_ID, momId)
						.setParameter(CommonConstants.FOLLOW_UPS, followups)
						.setParameter(CommonConstants.SAMPLE, Samples)
						.list();
			}
			
			if(pricing.equalsIgnoreCase(CommonConstants.TRUE) && followups.equalsIgnoreCase(CommonConstants.FALSE) && Samples.equalsIgnoreCase(CommonConstants.TRUE)){
				return this.sessionFactory.getCurrentSession().getNamedQuery(MinutesOfMeetingNotesDO.FIND_BY_MOM_ID_SAMPLES_PRICING)
						.setParameter(CommonConstants.MOM_ID, momId)
						.setParameter(CommonConstants.PRICING, pricing)
						.setParameter(CommonConstants.SAMPLE, Samples)
						.list();
			}
			
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return proposalList;
	}
}
