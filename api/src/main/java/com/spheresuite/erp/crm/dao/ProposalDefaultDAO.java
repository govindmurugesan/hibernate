package com.spheresuite.erp.crm.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.ProposalDefaultDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class ProposalDefaultDAO {
	static Logger logger = Logger.getLogger(ProposalDefaultDAO.class.getName());

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private GenericDAOImpl<ProposalDefaultDO> genericObject;
	
	public List<ProposalDefaultDO> persist(List<ProposalDefaultDO> proposalDefaultList) {
		try {
			if(proposalDefaultList != null && proposalDefaultList.size() > 0){
				this.sessionFactory.getCurrentSession().createQuery("delete from ProposalDefaultDO e where e.proposalId =:proposalId")
								.setParameter(CommonConstants.PROPOSAL_ID, proposalDefaultList.get(0).getProposalId())
								.executeUpdate();
			}
			for (ProposalDefaultDO proposalDefaultDO : proposalDefaultList) {
				genericObject.persist(proposalDefaultDO);
			}
		} catch (Exception eException) {
			//throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return proposalDefaultList;
	}
	
	public boolean delete(Long proposalId) {
		try {
			this.sessionFactory.getCurrentSession().createQuery("delete from ProposalDefaultDO e where e.proposalId =:proposalId")
							.setParameter(CommonConstants.PROPOSAL_ID, proposalId)
							.executeUpdate();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return true;
	}
	
	@SuppressWarnings("unchecked")
	public List<ProposalDefaultDO> retrieveById(Long proposalId) {
		List<ProposalDefaultDO> proposalList = null;
		try {
			proposalList = this.sessionFactory.getCurrentSession().getNamedQuery(ProposalDefaultDO.FIND_BY_PROPOSAL_ID)
										.setParameter(CommonConstants.PROPOSAL_ID, proposalId)
										.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return proposalList;
	}
}
