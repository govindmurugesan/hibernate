package com.spheresuite.erp.crm.rs;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.crm.service.LeadService;
import com.spheresuite.erp.crm.service.TaskService;
import com.spheresuite.erp.crm.web.util.TaskUtil;
import com.spheresuite.erp.domainobject.LeadDO;
import com.spheresuite.erp.domainobject.TaskDO;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/task")
public class TaskRS {

	String validation = null;
	static Logger logger = Logger.getLogger(TaskRS.class.getName());
	
	@Autowired
	private TaskService taskService;
	
	@Autowired
	private LeadService leadService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				TaskDO taskDO = new TaskDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		/*if(!inputJSON.get(CommonConstants.LEAD_ID).toString().isEmpty()){
			 			taskDO.setLeadId(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.LEAD_ID).toString() : ""));
			 		}*/
			 		if(inputJSON.get(CommonConstants.LEAD_ID) != null && !inputJSON.get(CommonConstants.LEAD_ID).toString().isEmpty()){
			 			List<LeadDO> leadList = leadService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.LEAD_ID).toString()));
			 			if(leadList != null && leadList.size() > 0){
			 				taskDO.setLead(leadList.get(0));
			 			}
			 		}
			 		taskDO.setTaskDetails(inputJSON.get(CommonConstants.TASKDETAIL) != null && !inputJSON.get(CommonConstants.TASKDETAIL).toString().isEmpty() ? inputJSON.get(CommonConstants.TASKDETAIL).toString() : "");
			 		taskDO.setTaskDate(CommonUtil.convertStringToDateTimePick(inputJSON.get(CommonConstants.DATE) != null && !inputJSON.get(CommonConstants.DATE).toString().isEmpty() ? inputJSON.get(CommonConstants.DATE).toString() : ""));
			 		//taskDO.setTaskTime(inputJSON.get(CommonConstants.TIME) != null && !inputJSON.get(CommonConstants.TIME).toString().isEmpty() ? inputJSON.get(CommonConstants.TIME).toString() : "");
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			taskDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			taskDO.setEmpId((inputJSON.get(CommonConstants.UPDATED_BY).toString()));
			 			taskDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		taskDO.setCreatedon(new Date());
			 		taskDO.setStatus(CommonConstants.PENDING_STATUS);
			 		taskDO.setUpdatedon(new Date());
			 	}
				taskService.persist(taskDO);
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Task Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveByLeadId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByLeadid(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
			JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
		 	if (inputJSON != null){
		 		if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
		 			List<TaskDO> taskList = taskService.retrieveByLeadId(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
		 			respJSON = TaskUtil.getTaskList(taskList).toString();
		 		}
		 		
		 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/updateStatus", method = RequestMethod.POST)
	public @ResponseBody String updateStatus(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
			TaskDO taskDO  = new TaskDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<TaskDO> taskList = taskService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(taskList != null && taskList.size() > 0){
			 			taskDO = taskList.get(0);
			 			taskDO.setStatus(!inputJSON.get(CommonConstants.STATUS).toString().isEmpty() ? inputJSON.get(CommonConstants.STATUS).toString().charAt(0) : CommonConstants.PENDING_STATUS);
			 			//taskDO.setStatus(inputJSON.get(CommonConstants.STATUS).toString().charAt(0));
			 			if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			taskDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
					 		taskDO.setUpdatedon(new Date());
				 		taskService.update(taskDO);
				 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Task Updated");
			 		}
			 	}

			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retrievePendingTask", method = RequestMethod.GET)
	public @ResponseBody String retrievePendingTask(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
			//JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
		 			List<TaskDO> taskList = taskService.retrievePendingTask();
		 			respJSON = TaskUtil.getTaskList(taskList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
}
