package com.spheresuite.erp.crm.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.IndustryDO;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
@Component
public class IndustryUtil {
	
	private IndustryUtil() {}
	
	public static JSONObject getIndustryList(List<IndustryDO> industryList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (IndustryDO industry : industryList) {
				resultJSONArray.put(getIndustryDetailObject(industry));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getIndustryDetailObject(IndustryDO industry)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(industry.getIndustryId()));
		result.put(CommonConstants.NAME, String.valueOf(industry.getName()));
		result.put(CommonConstants.STATUS, String.valueOf(industry.getStatus()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(industry.getUpdatedon())));
		if(industry.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(industry.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		return result;
	}
}
