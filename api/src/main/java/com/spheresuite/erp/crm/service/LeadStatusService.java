package com.spheresuite.erp.crm.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.crm.dao.LeadStatusDAO;
import com.spheresuite.erp.domainobject.LeadStatusDO;
@Service
@Transactional
public class LeadStatusService {
	static Logger logger = Logger.getLogger(LeadStatusService.class.getName());
	@Autowired
	private LeadStatusDAO leadStatusDAO;

	@Transactional
	public boolean persist(LeadStatusDO leadStatusDO) {
		return leadStatusDAO.persist(leadStatusDO);
	}

	@Transactional
	public List<LeadStatusDO> retrieveActive() {
		return leadStatusDAO.retrieveActive();
	}
	
	@Transactional
	public List<LeadStatusDO> retrieveByActiveName(String name) {
		return leadStatusDAO.retrieveByActiveName(name);
	}

	@Transactional
	public List<LeadStatusDO> retrieveById(Long Id) {
		return leadStatusDAO.retrieveById(Id);
	}

	@Transactional
	public List<LeadStatusDO> retrieve() {
		return leadStatusDAO.retrieve();
	}

	@Transactional
	public boolean update(LeadStatusDO leadStatusDO) {
		return leadStatusDAO.update(leadStatusDO);
	}
	
	@Transactional
	public boolean persistList(List<LeadStatusDO> leadStatusDO) {
		return leadStatusDAO.persistList(leadStatusDO);
	}
}
