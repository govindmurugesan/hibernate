package com.spheresuite.erp.crm.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.ScheduleDO;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
@Repository
public class ScheduleDAO {
	static Logger logger = Logger.getLogger(ScheduleDAO.class.getName());

	@Autowired
	private GenericDAOImpl<ScheduleDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public ScheduleDO persist(ScheduleDO scheduleDO) {
		try {
			genericObject.persist(scheduleDO);
		} catch (Exception eException) {
			//throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			//SessionManager.closeEntityManager(em);
		}
		return scheduleDO;
	}
	
	public ScheduleDO update(ScheduleDO scheduleDO) {
		try {
			genericObject.merge(scheduleDO);
		} catch (Exception eException) {
			//throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			//SessionManager.closeEntityManager(em);
		}
		return scheduleDO;
	}
	
	@SuppressWarnings("unchecked")
	public List<ScheduleDO> retrieveById(Long Id) {
		List<ScheduleDO> scheduleList = null;
		try {
			scheduleList =  this.sessionFactory.getCurrentSession().getNamedQuery(ScheduleDO.FIND_BY_ID)
								.setParameter(CommonConstants.ID, Id).list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return scheduleList;
	}
	
	@SuppressWarnings("unchecked")
	public List<ScheduleDO> retrieveByLeadId(Long Id) {
		List<ScheduleDO> scheduleList = null;
		try {
			scheduleList = this.sessionFactory.getCurrentSession().getNamedQuery(ScheduleDO.FIND_BY_LEAD_ID)
									.setParameter(CommonConstants.ID, Id)
									.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return scheduleList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<ScheduleDO> retrieveUpcomingCalender(String empid ,java.util.Date date) {
		List<ScheduleDO> scheduleList = new ArrayList<ScheduleDO>();
		List<ScheduleDO> tempScheduleList = null;
		try {
			tempScheduleList = this.sessionFactory.getCurrentSession().getNamedQuery(ScheduleDO.FIND_UPCOMMING_CALENDER)
									/*.setParameter(CommonConstants.CURRENT_DATE, CommonUtil.convertDateToYearWithOutTime(date))*/
									.setParameter(CommonConstants.EMPID, empid)
									.list();
			int i=0;
			for (ScheduleDO scheduleDO : tempScheduleList) {
				if(CommonUtil.convertEmailDateToDate(scheduleDO.getLogDate()).compareTo(CommonUtil.convertEmailDateToDate(date)) >= 0){
					scheduleList.add(scheduleDO);
					i++;
					if(i >= 5){
						break;
					}
				}/*else{
					if(scheduleDO.getLogDate().compareTo(CommonUtil.convertDateToYearWithOutTime(date)) >= 0){
						scheduleList.add(scheduleDO);
						i++;
						if(i >= 5){
							break;
						}
					}
				}*/
			}
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return scheduleList;
	}
	
	@SuppressWarnings("unchecked")
	public List<ScheduleDO> retrieveUpcomingCalenderOfEmp(String empid ,java.util.Date date) {
		List<ScheduleDO> scheduleList = new ArrayList<ScheduleDO>();
		try {
			scheduleList = this.sessionFactory.getCurrentSession().getNamedQuery(ScheduleDO.FIND_UPCOMMING_CALENDER_OF_EMP)
									.setParameter(CommonConstants.EMPID, empid)
									.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return scheduleList;
	}
}
