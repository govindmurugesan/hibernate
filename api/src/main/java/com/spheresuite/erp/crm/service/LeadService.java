package com.spheresuite.erp.crm.service;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.crm.dao.LeadDAO;
import com.spheresuite.erp.domainobject.LeadDO;
@Service
@Transactional
public class LeadService {
	static Logger logger = Logger.getLogger(LeadService.class.getName());
	@Autowired
	private LeadDAO leadDAO;
	
	@Transactional
	public LeadDO persist(LeadDO leadDO) {
		return leadDAO.persist(leadDO);
	}

	@Transactional
	public List<LeadDO> retrieveById(Long Id) {
		return leadDAO.retrieveById(Id);
	}

	@Transactional
	public List<LeadDO> retrieveBetweenDate(Date fromDate, Date toDate) {
		return leadDAO.retrieveBetweenDate(fromDate, toDate);
	}

	@Transactional
	public List<LeadDO> retrieveBetweenDateByEmp(Date fromDate, Date toDate, String empId) {
		return leadDAO.retrieveBetweenDateByEmp(fromDate, toDate, empId);
	}

	@Transactional
	public List<LeadDO> retrieve(String type, List<String> id) {
		return leadDAO.retrieve(type, id);
	}

	@Transactional
	public boolean update(List<LeadDO> leadList) {
		return leadDAO.update(leadList);
	}

	@Transactional
	public List<LeadDO> retrieveAll(String type) {
		return leadDAO.retrieveAll(type);
	}

	@Transactional
	public Long retrieveAllForChart(String type) {
		return leadDAO.retrieveAllForChart(type);
	}

	@Transactional
	public List<LeadDO> retrieve(List<Long> id) {
		return leadDAO.retrieve(id);
	}

	@Transactional
	public List<LeadDO> retrieveAll() {
		return leadDAO.retrieveAll();
	}

	@Transactional
	public List<Long> retrieveForCustomerIds() {
		return leadDAO.retrieveForCustomerIds();
	}

	@Transactional
	public LeadDO update(LeadDO leadDO) {
		return leadDAO.update(leadDO);
	}
	
	@Transactional
	public List<LeadDO> retrieveLatestLead(String type, String empId) {
		return leadDAO.retrieveLatestLead(empId, type);
	}

	@Transactional
	public List<LeadDO> retrieveCustomerBetweenDate(Date fromDate, Date toDate) {
		return leadDAO.retrieveCustomerBetweenDate(fromDate, toDate);
	}

	@Transactional
	public List<LeadDO> retrieveCustomerBetweenDateByEmp(Date fromDate, Date toDate, String empId) {
		return leadDAO.retrieveCustomerBetweenDateByEmp(fromDate, toDate, empId);
	}
	

	@Transactional
	public boolean persistList(List<LeadDO> leadDO) {
		return leadDAO.persistList(leadDO);
	}

	@Transactional
	public List<LeadDO> retrieveLeadByEmp(String empId, String type) {
		return leadDAO.retrieveLeadByEmp(empId, type);
	}
	
	@Transactional
	public List<LeadDO> retrieveLeadByPermenantEmp(String empId, String type) {
		return leadDAO.retrieveLeadByPermenantEmp(empId, type);
	}

	@Transactional
	public List<LeadDO> retrieveLeadByTransferType(String transferType, String type) {
		return leadDAO.retrieveLeadByTransferType(transferType, type);
	}

	@Transactional
	public List<LeadDO> retrieveLeadByAllTransferType(String type) {
		return leadDAO.retrieveLeadByAllTransferType(type);
	}

	@Transactional
	public List<LeadDO> retrieveTransferLeadById(String transferType, String type, String empID) {
		return leadDAO.retrieveTransferLeadById(transferType, type, empID);
	}
	
	@Transactional
	public List<LeadDO> retrieveCurrentFollowupLeads(String empID) {
		return leadDAO.retrieveCurrentFollowupLeads(empID);
	}
	
	@Transactional
	public List<LeadDO> retrieveFollowupLeadByDate(String empID, Date date) {
		return leadDAO.retrieveFollowupLeadByDate(empID, date);
	}

	@Transactional
	public List<LeadDO> retrieveByEmp(String type, String empId) {
		return leadDAO.retrieveByEmp(type, empId);
	}
	
	@Transactional
	public List<LeadDO> retrieveByLeadName(String name) {
		return leadDAO.retrieveByLeadName(name);
	}

}
