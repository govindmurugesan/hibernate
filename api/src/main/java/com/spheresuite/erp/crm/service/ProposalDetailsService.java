package com.spheresuite.erp.crm.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.crm.dao.ProposalDetailsDAO;
import com.spheresuite.erp.domainobject.ProposalDetailsDO;

@Service
@Transactional
public class ProposalDetailsService {
	static Logger logger = Logger.getLogger(ProposalDetailsService.class.getName());
	
	@Autowired
	private ProposalDetailsDAO proposalDetailsDAO;
	
	@Transactional
	public List<ProposalDetailsDO> persist(List<ProposalDetailsDO> proposalDetailsList) {
		return proposalDetailsDAO.persist(proposalDetailsList);
	}
	
	@Transactional
	public boolean delete(Long proposalId) {
		return proposalDetailsDAO.delete(proposalId);
	}

	@Transactional
	public List<ProposalDetailsDO> retrieveById(Long proposalId) {
		return proposalDetailsDAO.retrieveById(proposalId);
	}
}
