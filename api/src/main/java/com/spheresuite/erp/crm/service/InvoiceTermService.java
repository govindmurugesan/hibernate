package com.spheresuite.erp.crm.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.crm.dao.InvoiceTermDAO;
import com.spheresuite.erp.domainobject.InvoiceTermDO;
@Service
@Transactional
public class InvoiceTermService {
	static Logger logger = Logger.getLogger(InvoiceTermService.class.getName());
	@Autowired
	private InvoiceTermDAO invoiceTermDAO;

	@Transactional
	public List<InvoiceTermDO> persist(List<InvoiceTermDO> invoiceTermList) {
		return invoiceTermDAO.persist(invoiceTermList);
	}

	@Transactional
	public List<InvoiceTermDO> retrieveById(Long proposalId) {
		return invoiceTermDAO.retrieveById(proposalId);
	}

	@Transactional
	public boolean delete(Long proposalId) {
		return invoiceTermDAO.delete(proposalId);
	}
}
