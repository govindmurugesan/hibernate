package com.spheresuite.erp.crm.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.LeadTypeDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class LeadTypeDAO {
	static Logger logger = Logger.getLogger(LeadTypeDAO.class.getName());
	@Autowired
	private GenericDAOImpl<LeadTypeDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(LeadTypeDO leadTypeDO)  {
		boolean persistStatus = true;
		List<LeadTypeDO> leadTypeList = null;
		try {
			leadTypeList = this.sessionFactory.getCurrentSession().getNamedQuery(LeadTypeDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, leadTypeDO.getName())
					.list();
					if(leadTypeList != null && leadTypeList.size() > 0){
						persistStatus = false;
					}else{
						genericObject.persist(leadTypeDO);
					}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	public List<LeadTypeDO> retrieve()  {
		List<LeadTypeDO> leadTypeList = null;
		try {
			leadTypeList = genericObject.retrieve(LeadTypeDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return leadTypeList;
	}
	
	public List<LeadTypeDO> retrieveActive()  {
		List<LeadTypeDO> leadTypeList = null;
		try {
			return genericObject.retrieveActive(CommonConstants.ACTIVE, 
					LeadTypeDO.FIND_BY_STATUS, LeadTypeDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return leadTypeList;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeadTypeDO> retrieveById(Long Id)  {
		List<LeadTypeDO> leadTypeList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeadTypeDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, Id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return leadTypeList;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeadTypeDO> retrieveByActiveName(String name)  {
		List<LeadTypeDO> leadTypeList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeadTypeDO.FIND_BY_ACTIVE_NAME)
					.setParameter(CommonConstants.NAME, name)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return leadTypeList;
	}
	
	@SuppressWarnings("unchecked")
	public boolean update(LeadTypeDO leadTypeDO)  {
		boolean updateStatus = true;
		try {
			List<LeadTypeDO> leadTypeList = null;
			leadTypeList = this.sessionFactory.getCurrentSession().getNamedQuery(LeadTypeDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, leadTypeDO.getName())
					.setParameter(CommonConstants.ID, leadTypeDO.getLeadTypeId())
					.list();
			if(leadTypeList != null && leadTypeList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(leadTypeDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}
	
	public boolean persistList(List<LeadTypeDO> leadTypeDOList){
		try {
			for (LeadTypeDO leadTypeDO : leadTypeDOList) {
				genericObject.persist(leadTypeDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}

}
