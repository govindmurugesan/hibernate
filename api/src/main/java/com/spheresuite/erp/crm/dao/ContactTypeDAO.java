package com.spheresuite.erp.crm.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.ContactTypeDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class ContactTypeDAO {
	static Logger logger = Logger.getLogger(ContactTypeDAO.class.getName());
	@Autowired
	private GenericDAOImpl<ContactTypeDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(ContactTypeDO contactTypeDO) {
		boolean persistStatus = true;
		try {
			List<ContactTypeDO> contactTypeList = null;
			contactTypeList = this.sessionFactory.getCurrentSession().getNamedQuery(ContactTypeDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, contactTypeDO.getName())
					.list();
					if(contactTypeList != null && contactTypeList.size() > 0){
						persistStatus = false;
					}else{
						genericObject.persist(contactTypeDO);
					}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	public List<ContactTypeDO> retrieve() {
		List<ContactTypeDO> contactTypeList = null;
		try {
			contactTypeList = genericObject.retrieve(ContactTypeDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return contactTypeList;
	}
	
	public List<ContactTypeDO> retrieveActive() {
		List<ContactTypeDO> contactTypeList = null;
		try {
			return genericObject.retrieveActive(CommonConstants.ACTIVE, ContactTypeDO.FIND_BY_STATUS, ContactTypeDO.class);
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return contactTypeList;
	}
	
	@SuppressWarnings("unchecked")
	public List<ContactTypeDO> retrieveById(Long id) {
		 List<ContactTypeDO> contactTypeList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(ContactTypeDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return contactTypeList;
	}
	
	@SuppressWarnings("unchecked")
	public List<ContactTypeDO> retrieveByName(String name) {
		 List<ContactTypeDO> contactTypeList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(ContactTypeDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, name)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return contactTypeList;
	}
	
	@SuppressWarnings("unchecked")
	public boolean update(ContactTypeDO contactTypeDO) {
		boolean updateStatus = true;
		try {
			List<ContactTypeDO> projectTypeList = null;
			projectTypeList = this.sessionFactory.getCurrentSession().getNamedQuery(ContactTypeDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, contactTypeDO.getName())
					.setParameter(CommonConstants.ID, contactTypeDO.getContactTypeId())
					.list();
			if(projectTypeList != null && projectTypeList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(contactTypeDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}
	
	public boolean persistList(List<ContactTypeDO> contactTypeList){
		try {
			for (ContactTypeDO contactTypeDO : contactTypeList) {
				genericObject.persist(contactTypeDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}

}
