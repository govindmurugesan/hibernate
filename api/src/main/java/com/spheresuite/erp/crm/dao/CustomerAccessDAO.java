package com.spheresuite.erp.crm.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.CustomerAccessDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class CustomerAccessDAO {
	static Logger logger = Logger.getLogger(CustomerAccessDAO.class.getName());
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private GenericDAOImpl<CustomerAccessDO> genericObject;
	
	@SuppressWarnings("unchecked")
	public boolean persist(CustomerAccessDO customerAccessDO) {
		boolean persistStatus = true;
		try {
			List<CustomerAccessDO> customerAccessDOList = null;
			customerAccessDOList = this.sessionFactory.getCurrentSession().getNamedQuery(CustomerAccessDO.FIND_BY_CUSTOMER_ID)
					.setParameter(CommonConstants.USERID, customerAccessDO.getContact().getContactId())
					.list();
					if(customerAccessDOList != null && customerAccessDOList.size() > 0){
						persistStatus = false;
					}else{
						genericObject.persist(customerAccessDO);
					}
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
		}
		return persistStatus;
	}
	
	
	public List<CustomerAccessDO> retrieve() {
		List<CustomerAccessDO> customerAccessList = null;
		try {
			customerAccessList = genericObject.retrieve(CustomerAccessDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return customerAccessList;
	}
	
	public List<CustomerAccessDO> retrieveActive() {
		List<CustomerAccessDO> customerAccessList = null;
		try {
			customerAccessList =  genericObject.retrieveActive(CommonConstants.ACTIVE, CustomerAccessDO.FIND_BY_STATUS, CustomerAccessDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return customerAccessList;
	}
	
	@SuppressWarnings("unchecked")
	public List<CustomerAccessDO> retrieveById(Long id) {
		List<CustomerAccessDO> customerAccess = null;
		try {
			customerAccess = this.sessionFactory.getCurrentSession().getNamedQuery(CustomerAccessDO.FIND_BY_ID)
										.setParameter(CommonConstants.ID, id)
										.list();
		} catch (Exception eException) {
		} finally {
		}
		return customerAccess;
	}
	
	@SuppressWarnings("unchecked")
	public List<CustomerAccessDO> retrieveByEmail(String personal, String secondary) {
		List<CustomerAccessDO> customerAccess = null;
		try {
			customerAccess = this.sessionFactory.getCurrentSession().getNamedQuery(CustomerAccessDO.FIND_BY_EMAIL_FOR_CUSTOMER)
										.setParameter(CommonConstants.PERSONAL, personal)
										.setParameter(CommonConstants.SECONDARY, secondary)
										.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return customerAccess;
	}
	
	@SuppressWarnings("unchecked")
	public List<CustomerAccessDO> retrieveByEmailId(String email) {
		List<CustomerAccessDO> customerAccess = null;
		try {
			customerAccess = this.sessionFactory.getCurrentSession().getNamedQuery(CustomerAccessDO.FIND_BY_EMAIL)
					.setParameter(CommonConstants.EMAIL, email)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return customerAccess;
	}
	
	@SuppressWarnings("unchecked")
	public List<CustomerAccessDO> retrieveByTempPassword(String tempPassword, String empId) {
		List<CustomerAccessDO> customerAccess = null;
		try {
			customerAccess = this.sessionFactory.getCurrentSession().getNamedQuery(CustomerAccessDO.FIND_BY_TEMPPASSWORD)
					.setParameter(CommonConstants.TEMP_PASSWORD, tempPassword)
					.setParameter(CommonConstants.EMPID, Long.parseLong(empId))
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return customerAccess;
	}
	
	/*public CustomerAccessDO update(CustomerAccessDO customerAccessDO) {
		try {
			genericObject.merge(customerAccessDO);
		} catch (Exception eException) {
		} finally {
		}
		return customerAccessDO;
	}*/
	
	@SuppressWarnings("unchecked")
	public boolean update(CustomerAccessDO customerAccessDO) {
		boolean updateStatus = true;
		try {
			List<CustomerAccessDO> customerAccessDOList = null;
			customerAccessDOList = this.sessionFactory.getCurrentSession().getNamedQuery(CustomerAccessDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.EMAIL, customerAccessDO.getContact().getPrimaryemail())
					.setParameter(CommonConstants.ID, customerAccessDO.getCustomerAccessId())
					.list();
			if(customerAccessDOList != null && customerAccessDOList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(customerAccessDO);
			}
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
		}
		return updateStatus;
	}
	
	public boolean updatePassword(CustomerAccessDO customerAccessDO) {
		boolean updateStatus = true;
		try {
				genericObject.merge(customerAccessDO);
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
		}
		return updateStatus;
	}
	
	@SuppressWarnings("unchecked")
	public List<CustomerAccessDO> retrieveForLogin(String email,String password) {
		List<CustomerAccessDO> customerAccess = null;
		try {
			customerAccess = this.sessionFactory.getCurrentSession().getNamedQuery(CustomerAccessDO.FIND_FOR_LOGIN)
					.setParameter(CommonConstants.EMAIL, email)
					.setParameter(CommonConstants.PASSWORD, password)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return customerAccess;
	}
	
	@SuppressWarnings("unchecked")
	public List<CustomerAccessDO> retriveByCustomerId(String id) {
		List<CustomerAccessDO> customerAccess = null;
		try {
			customerAccess = this.sessionFactory.getCurrentSession().getNamedQuery(CustomerAccessDO.FIND_BY_CUSTOMER_ID)
										.setParameter(CommonConstants.USERID, Long.parseLong(id))
										.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return customerAccess;
	}
	
	@SuppressWarnings("unchecked")
	public List<CustomerAccessDO> getSuperAdmins() {
		List<CustomerAccessDO> customerAccessList = null;
		try {
			customerAccessList = this.sessionFactory.getCurrentSession().getNamedQuery(CustomerAccessDO.FIND_SUPERADMIN)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.setParameter(CommonConstants.ADMIN, Long.parseLong(CommonConstants.ADMINID))
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return customerAccessList;
	}

}
