package com.spheresuite.erp.crm.rs;

import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.crm.service.ContactService;
import com.spheresuite.erp.crm.service.CustomerAccessService;
import com.spheresuite.erp.crm.web.util.CustomerAccessUtil;
import com.spheresuite.erp.domainobject.ContactDO;
import com.spheresuite.erp.domainobject.CustomerAccessDO;
import com.spheresuite.erp.domainobject.RolesDO;
import com.spheresuite.erp.service.RolesService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.util.JwtTokenGenerator;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.EmailProxyUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/customeraccess")
public class CustomerAccessRS {
	
	String validation = null;
	
	static Logger logger = Logger.getLogger(CustomerAccessRS.class.getName());


	@Autowired
	private CustomerAccessService customerAccessService;
	

	@Autowired
	private ContactService contactService;
	
	@Autowired
	private RolesService rolesService;
	
	

	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				CustomerAccessDO customerAccessDO = new CustomerAccessDO();
				List<ContactDO> contactList = null;
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.CONTACT_ID) != null && !inputJSON.get(CommonConstants.CONTACT_ID).toString().isEmpty()){
			 			contactList = contactService.retriveById(Long.parseLong(inputJSON.get(CommonConstants.CONTACT_ID).toString()));
			 			if(contactList != null && contactList.size() > 0){
				 			customerAccessDO.setContact(contactList.get(0));
				 		}
			 		}
			 		if(inputJSON.get(CommonConstants.ROLE_ID) != null && !inputJSON.get(CommonConstants.ROLE_ID).toString().isEmpty()){
			 			List<RolesDO> rolesList = rolesService.retriveById(Long.parseLong(inputJSON.get(CommonConstants.ROLE_ID).toString()));
						if(rolesList != null){
							customerAccessDO.setRole(rolesList.get(0));
						}
			 		}
			 		/*if(inputJSON.get(CommonConstants.EMAIL) != null && !inputJSON.get(CommonConstants.EMAIL).toString().isEmpty()){
			 			customerAccessDO.setEmail(inputJSON.get(CommonConstants.EMAIL).toString().toString());
			 		}*/
					customerAccessDO.setIsDeleted(0L);
					customerAccessDO.setUpdatedon(new Date());
					if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			customerAccessDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			customerAccessDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
					char[] temppasssword = CustomerAccessUtil.geek_Password(5);
					String pwd = temppasssword.toString();
					customerAccessDO.setTemppassowrd(Base64.getEncoder().encodeToString(pwd.getBytes("UTF-8")));
					customerAccessDO.setStatus(CommonConstants.PEDING);
					
					if(!customerAccessService.persist(customerAccessDO)){
						return CommonWebUtil.buildErrorResponse("User Already Exist").toString();
					}
					CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New User Created");
			 	if(customerAccessDO != null && customerAccessDO.getCustomerAccessId() != null){
			 		if(contactList != null && contactList.size() > 0){ 
			 			if(contactList.get(0).getPrimaryemail() != null ){
				 			String host = request.getServletContext().getInitParameter(CommonConstants.HOST);
							String fromEmail = CommonConstants.SENDMAIL;
							String toEmails = contactList.get(0).getPrimaryemail();
							String userName = null;
							
							List<ContactDO> contactList1 = contactService.retriveById(customerAccessDO.getContact().getContactId());
							if(contactList1 != null && contactList1.size() > 0){
								userName =  String.valueOf(contactList1.get(0).getFirstname()) + " " + String.valueOf(contactList1.get(0).getMiddlename()) + " " + String.valueOf(contactList1.get(0).getLastname());
							}
							String URL = CommonUtil.getUIUrl(request);
							URL = CommonConstants.HTTP+URL+"#!/changepassword/";
							String url = URL+customerAccessDO.getTemppassowrd()+"/"+contactList1.get(0).getPrimaryemail()+"/N"+"/C";
							String emailBody = "Cick Here Login : " +URL+customerAccessDO.getTemppassowrd()+"/"+contactList1.get(0).getPrimaryemail()+"/N"+"/C";
					 		boolean mailStatus = EmailProxyUtil.sendEmail(request,host,  fromEmail,  toEmails, "", "", emailBody, false, url, "inviteUser", userName); 
							if(mailStatus){
								return CommonWebUtil.buildSuccessResponse().toString();
							}else{
								return CommonWebUtil.buildErrorResponse("").toString();
							}
			 			}else{
					 		return CommonWebUtil.buildErrorResponse("Email Id not available to send mail").toString();
					 	}
			 		}else{
				 		return CommonWebUtil.buildErrorResponse("Employee not available").toString();
				 	}
					
			 	}else{
			 		return CommonWebUtil.buildErrorResponse("User Id not available to send mail").toString();
			 	}
			}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/resendInvite/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String resendInvite(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<CustomerAccessDO> customerAccessList = customerAccessService.retriveByCustomerId(inputJSON.get(CommonConstants.ID).toString());
					if(customerAccessList != null && customerAccessList.size() > 0){
						if(customerAccessList.get(0).getTemppassowrd() != null && customerAccessList.get(0).getContact().getPrimaryemail() != null){
							String host = request.getServletContext().getInitParameter(CommonConstants.HOST);
							String fromEmail = CommonConstants.SENDMAIL;
							String toEmails = customerAccessList.get(0).getContact().getPrimaryemail();
							String URL = CommonUtil.getUIUrl(request);
							URL = CommonConstants.HTTP+URL+"#!/changepassword/";
							String emailBody = "Cick Here Login : " +URL+customerAccessList.get(0).getTemppassowrd()+"/"+customerAccessList.get(0).getContact().getPrimaryemail()+"/N"+"/C";
							String url = URL+customerAccessList.get(0).getTemppassowrd()+"/"+customerAccessList.get(0).getContact().getPrimaryemail()+"/N"+"/C";

							String userName = null;
							List<ContactDO> contactList = contactService.retriveById(customerAccessList.get(0).getContact().getContactId());
							if(contactList != null && contactList.size() > 0){
								userName =  String.valueOf(contactList.get(0).getFirstname()) + " " + String.valueOf(contactList.get(0).getMiddlename()) + " " + String.valueOf(contactList.get(0).getLastname());
							}
							boolean mailStatus = EmailProxyUtil.sendEmail(request,host,  fromEmail,  toEmails, "", "", emailBody, false, url, "reInvite", userName);
							if(mailStatus){
								return CommonWebUtil.buildSuccessResponse().toString();
							}else{
								return CommonWebUtil.buildErrorResponse("").toString();
							}
						}
					}
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<CustomerAccessDO> customerAccessList = customerAccessService.retrieve();
				respJSON = CustomerAccessUtil.getCustomerAccessList(customerAccessList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<CustomerAccessDO> customerAccessList = customerAccessService.retrieveActive();
				respJSON = CustomerAccessUtil.getCustomerAccessList(customerAccessList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retriveById/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveById(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<CustomerAccessDO> customerAccessList = customerAccessService.retriveByCustomerId(inputJSON.get(CommonConstants.ID).toString());
					respJSON = CustomerAccessUtil.getCustomerAccessList(customerAccessList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		CustomerAccessDO customerAccessDO = new CustomerAccessDO();
			 		List<CustomerAccessDO>customerAccessDOList = customerAccessService.retriveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
				 	if (customerAccessDOList != null && customerAccessDOList.size() > 0){
				 		customerAccessDO = customerAccessDOList.get(0);
				 		if(inputJSON.get(CommonConstants.ROLE_ID) != null && !inputJSON.get(CommonConstants.ROLE_ID).toString().isEmpty()){
				 			List<RolesDO> rolesList = rolesService.retriveById(Long.parseLong(inputJSON.get(CommonConstants.ROLE_ID).toString()));
							if(rolesList != null){
								customerAccessDO.setRole(rolesList.get(0));
							}
				 		}
				 		/*if(inputJSON.get(CommonConstants.EMAIL) != null && !inputJSON.get(CommonConstants.EMAIL).toString().isEmpty()){
				 			customerAccessDO.setEmail(inputJSON.get(CommonConstants.EMAIL).toString().toString());
				 		}*/
						customerAccessDO.setUpdatedon(new Date());
						if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			customerAccessDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
						if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
							customerAccessDO.setStatus((char)  inputJSON.get(CommonConstants.STATUS).toString().charAt(0));
				 		}
						if(customerAccessDO.getStatus() != 'd'){
							customerAccessDO.setIsDeleted(0L);
						}else{
							customerAccessDO.setIsDeleted(1L);
						}
						//customerAccessService.update(customerAccessDO);
						if(!customerAccessService.update(customerAccessDO)){
							return CommonWebUtil.buildErrorResponse("User Already Added").toString();
						}
						CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "User Updated");
				 	}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/updatepassword", method = RequestMethod.POST)
	public @ResponseBody String updatepassword(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.EMAIL).toString().isEmpty() && !inputJSON.get(CommonConstants.PASSWORD).toString().isEmpty()){
			 		CustomerAccessDO customerAccessDO = new CustomerAccessDO();
			 		List<CustomerAccessDO>customerAccessDOList = customerAccessService.retrieveByEmailId(inputJSON.get(CommonConstants.EMAIL).toString());
				 	if (customerAccessDOList != null){
				 		customerAccessDO = customerAccessDOList.get(0);
				 		/*Base64.Encoder encoder = Base64.getEncoder(); 
						byte[] enString = encoder.encode(inputJSON.get(CommonConstants.PASSWORD).toString().getBytes());*/
						String pwd = inputJSON.get(CommonConstants.PASSWORD).toString();
						customerAccessDO.setPassword(Base64.getEncoder().encodeToString(pwd.getBytes("UTF-8")));
				 		//Ctring enString = CommonUtil.encryptText("testseed",inputJSON != null ? CommonConstants.PASSWORD.toString() : "");
						customerAccessDO.setUpdatedon(new Date());
						customerAccessService.update(customerAccessDO);
						CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Password Updated");
				 	}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/delete/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String delete(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		CustomerAccessDO customerAccessDO = new CustomerAccessDO();
			 		List<CustomerAccessDO> customerAccessList = customerAccessService.retriveByCustomerId(inputJSON.get(CommonConstants.ID).toString());
				 	if (customerAccessList != null){
				 		customerAccessDO = customerAccessList.get(0);
				 		customerAccessDO.setIsDeleted(1L);
				 		customerAccessDO.setStatus('d');
						customerAccessDO.setUpdatedon(new Date());
						customerAccessService.update(customerAccessDO);
				 	}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/cancelUser/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String cancelUser(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		CustomerAccessDO customerAccessDO = new CustomerAccessDO();
			 		List<CustomerAccessDO> customerAccessList = customerAccessService.retriveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
				 	if (customerAccessList != null && customerAccessList.size() > 0){
				 		customerAccessDO = customerAccessList.get(0);
				 		customerAccessDO.setIsDeleted(1L);
				 		customerAccessDO.setStatus('c');
						customerAccessDO.setUpdatedon(new Date());
						customerAccessService.update(customerAccessDO);
				 	}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveForLogin", method = RequestMethod.POST)
	public @ResponseBody String retrieveForLogin(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			//if (WebManager.authenticateSession(request)) {
			JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			if(inputJSON != null && !inputJSON.get(CommonConstants.EMAIL).toString().isEmpty()
					&& !inputJSON.get(CommonConstants.PASSWORD).toString().isEmpty()){
				List<CustomerAccessDO> customerAccessList = customerAccessService.retrieveByEmailId(inputJSON.get(CommonConstants.EMAIL).toString());
				if(customerAccessList.size() > 0) {
					byte[] decodedBytes = Base64.getDecoder().decode(customerAccessList.get(0).getPassword());
					String stringDecode = new String(decodedBytes, "UTF-8");
				if(stringDecode.toString().equals(inputJSON.get(CommonConstants.PASSWORD).toString())){
						String token = new JwtTokenGenerator().createCustomerJWT(customerAccessList);
						HttpSession session = request.getSession();
						session.setAttribute("token", token);
						//session.setMaxInactiveInterval(60*60);
						respJSON = CustomerAccessUtil.getCustomerForLogin(customerAccessList,token).toString();
					}
					
				}
			}
			//}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
			
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/resetpassword", method = RequestMethod.POST)
	public @ResponseBody String resetpassword(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.EMAIL).toString().isEmpty() 
			 			&& !inputJSON.get(CommonConstants.TEMP_PASSWORD).toString().isEmpty()
			 			&& !inputJSON.get(CommonConstants.NEWPASSWORD).toString().isEmpty()){
			 		List<ContactDO> contactList =  contactService.retriveByEmailId(inputJSON.get(CommonConstants.EMAIL).toString());
			 		if(contactList != null && contactList.size() > 0){
			 			CustomerAccessDO customerAccessDO = new CustomerAccessDO();
				 		List<CustomerAccessDO>customerAccessDOList = customerAccessService.retrieveByTempPassword(inputJSON.get(CommonConstants.TEMP_PASSWORD).toString(),
				 				contactList.get(0).getContactId().toString());
					 	if (customerAccessDOList != null && customerAccessDOList.size() > 0){
					 		customerAccessDO = customerAccessDOList.get(0);
					 		String pwd = inputJSON.get(CommonConstants.NEWPASSWORD).toString();
							customerAccessDO.setPassword(Base64.getEncoder().encodeToString(pwd.getBytes("UTF-8")));
							customerAccessDO.setTemppassowrd(null);
							customerAccessDO.setUpdatedon(new Date());
							customerAccessDO.setStatus(CommonConstants.ACTIVE);
							customerAccessService.update(customerAccessDO);
					 	}else{
					 		return CommonWebUtil.buildErrorResponse("User not availble").toString();
					 	}
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/sendPassword", method = RequestMethod.POST)
	public @ResponseBody String sendPassword(Model model, HttpServletRequest request) {
		try {
			JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			if(inputJSON != null && !inputJSON.get(CommonConstants.EMAIL).toString().isEmpty()){
				List<CustomerAccessDO> customerAccessList = customerAccessService.retriveByCustomerId(inputJSON.get(CommonConstants.EMAIL).toString());
				if(customerAccessList != null && customerAccessList.size() > 0){
					if(customerAccessList.get(0).getContact().getPrimaryemail() != null){
						String host = request.getServletContext().getInitParameter(CommonConstants.HOST);
						String fromEmail = request.getServletContext().getInitParameter(CommonConstants.FROMEMAIL);
						String toEmails = customerAccessList.get(0).getContact().getPrimaryemail();
						if(customerAccessList.get(0).getPassword() != null){
							CustomerAccessDO customerAccessDO = new CustomerAccessDO();
							customerAccessDO = customerAccessList.get(0);
							/*char[] temppasssword = SupplierUserUtil.geek_Password(5);
							customerAccessDO.setTemppassowrd(temppasssword.toString());*/
							char[] temppasssword = CustomerAccessUtil.geek_Password(5);
							//Base64.Encoder encoder = Base64.getEncoder(); 
							//byte[] enString = encoder.encode(temppasssword.toString().getBytes());
							//Ctring enString = CommonUtil.encryptText("testseed",temppasssword.toString());
							//customerAccessDO.setTemppassowrd(enString.toString());
							String pwd = temppasssword.toString();
							customerAccessDO.setTemppassowrd(Base64.getEncoder().encodeToString(pwd.getBytes("UTF-8")));
							customerAccessDO.setPassword(null);
							//customerAccessDO = customerAccessService.update(customerAccessDO);
							if(customerAccessService.update(customerAccessDO)){
								String URL = CommonUtil.getUIUrl(request);
								URL = CommonConstants.HTTP+URL+"#!/changepassword/";
								String emailBody = URL+customerAccessDO.getTemppassowrd()+"/"+customerAccessList.get(0).getContact().getPrimaryemail()+"/F"+"/C";
								String userName = null;
								List<ContactDO> contactList = contactService.retriveById(customerAccessList.get(0).getContact().getContactId());
								if(contactList != null && contactList.size() > 0){
									userName =  String.valueOf(contactList.get(0).getFirstname()) + " " + String.valueOf(contactList.get(0).getMiddlename()) + " " + String.valueOf(contactList.get(0).getLastname());
								}
								
								String url = URL+customerAccessDO.getTemppassowrd()+"/"+customerAccessList.get(0).getContact().getPrimaryemail()+"/F"+"/C";
						 		boolean mailStatus = EmailProxyUtil.sendEmail(request,host,  fromEmail,  toEmails, "", "", emailBody, false, url, "resetPassword", userName); 
	
								if(mailStatus){
									return CommonWebUtil.buildSuccessResponse().toString();
								}else{
									return CommonWebUtil.buildErrorResponse(CommonConstants.EMAIL_NOT_SENT).toString();
								}
							}else{
								return CommonWebUtil.buildErrorResponse("").toString();
							}
							
						}else{
							return CommonWebUtil.buildErrorResponse(CommonConstants.EMPID_NOT_REGISTER).toString();
						}
					}else{
						return CommonWebUtil.buildErrorResponse("Email Id Not found").toString();
					}
				}else{
					return CommonWebUtil.buildErrorResponse(CommonConstants.EMPID_NOT_REGISTER).toString();
				}
			}else{
				return CommonWebUtil.buildErrorResponse("").toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
	}
	
	@RequestMapping(value = "/resetpasswordWithoutAuth", method = RequestMethod.POST)
	public @ResponseBody String resetpasswordWithoutAuth(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty() && !inputJSON.get(CommonConstants.NEWPASSWORD).toString().isEmpty()){
			 		CustomerAccessDO customerAccessDO = new CustomerAccessDO();
			 		List<CustomerAccessDO>customerAccessDOList = customerAccessService.retriveByCustomerId(inputJSON.get(CommonConstants.EMPID).toString());
				 	if (customerAccessDOList != null && customerAccessDOList.size() > 0){
				 		customerAccessDO = customerAccessDOList.get(0);
				 		String pwd = inputJSON.get(CommonConstants.NEWPASSWORD).toString();
						customerAccessDO.setPassword(Base64.getEncoder().encodeToString(pwd.getBytes("UTF-8")));
						customerAccessDO.setTemppassowrd(null);
						customerAccessDO.setUpdatedon(new Date());
						customerAccessDO.setStatus(CommonConstants.ACTIVE);
						customerAccessService.updatePassword(customerAccessDO);
				 	}else{
				 		return CommonWebUtil.buildErrorResponse("").toString();
				 	}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/importUser/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String importEmp(@PathVariable String inputParams,  Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
				JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
			//	List<SupplierUserDO> userlist = new ArrayList<SupplierUserDO>();
				String updatedBy = null;
				if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY).toString();
				}
				for (int i=0; i < fileData.size(); i++){
					CustomerAccessDO customerAccessDO = new CustomerAccessDO();
					JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
					if(rowJSON.get(colName.get(CommonConstants.EMPID)) != null && !rowJSON.get(colName.get(CommonConstants.EMPID)).toString().isEmpty()){
			 			List<ContactDO> contactList = contactService.retriveById(Long.parseLong(rowJSON.get(colName.get(CommonConstants.EMPID)).toString()));
			 			if(contactList != null && contactList.size() > 0){
				 			customerAccessDO.setContact(contactList.get(0));
				 		
					 		if(rowJSON.get(colName.get(CommonConstants.ROLE_NAME)) != null && ! rowJSON.get(colName.get(CommonConstants.ROLE_NAME)).toString().isEmpty()){
					 			List<RolesDO> rolesList = rolesService.retriveByName(rowJSON.get(colName.get(CommonConstants.ROLE_NAME)).toString());
								if(rolesList != null){
									customerAccessDO.setRole(rolesList.get(0));
								}
					 		}
					 		/*if(rowJSON.get(colName.get(CommonConstants.EMAIL)) != null && !rowJSON.get(colName.get(CommonConstants.EMAIL)).toString().isEmpty()){
					 			customerAccessDO.setEmail(rowJSON.get(colName.get(CommonConstants.EMAIL)).toString());
					 		}*/
							customerAccessDO.setIsDeleted(0L);
							customerAccessDO.setUpdatedon(new Date());
							if(updatedBy != null){
					 			customerAccessDO.setUpdatedby((String) updatedBy);
					 			customerAccessDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 		}
							char[] temppasssword = CustomerAccessUtil.geek_Password(5);
							String pwd = temppasssword.toString();
							customerAccessDO.setTemppassowrd(Base64.getEncoder().encodeToString(pwd.getBytes("UTF-8")));
							customerAccessDO.setStatus(CommonConstants.PEDING);
							
							if(!customerAccessService.persist(customerAccessDO)){
								//return CommonWebUtil.buildErrorResponse("User Already Exist").toString();
							}else{
								if(customerAccessDO != null && customerAccessDO.getCustomerAccessId() != null && customerAccessDO.getContact().getPrimaryemail() != null ){
									List<ContactDO> contactList1 = contactService.retriveById(customerAccessDO.getContact().getContactId());
									if(contactList1 != null && contactList1.size() > 0){
										String host = request.getServletContext().getInitParameter(CommonConstants.HOST);
										String fromEmail = CommonConstants.SENDMAIL;
										String toEmails = customerAccessDO.getContact().getPrimaryemail();
										String userName = null;
										userName =  String.valueOf(contactList1.get(0).getFirstname()) + " " + String.valueOf(contactList1.get(0).getMiddlename()) + " " + String.valueOf(contactList1.get(0).getLastname());
										String URL = CommonUtil.getUIUrl(request);
										URL = CommonConstants.HTTP+URL+"#!/changepassword/";
										String url = URL+customerAccessDO.getTemppassowrd()+"/"+customerAccessDO.getContact().getPrimaryemail()+"/N"+"/C";
										String emailBody = "Cick Here Login : " +URL+customerAccessDO.getTemppassowrd()+"/"+customerAccessDO.getContact().getPrimaryemail()+"/N"+"/C";
								 		boolean mailStatus = EmailProxyUtil.sendEmail(request,host,  fromEmail,  toEmails, "", "", emailBody, false, url, "inviteUser", userName); 
										if(mailStatus){
											return CommonWebUtil.buildSuccessResponse().toString();
										}else{
											return CommonWebUtil.buildErrorResponse("").toString();
										}
									}
							 	}
							}
			 			}
					
					}	
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
}