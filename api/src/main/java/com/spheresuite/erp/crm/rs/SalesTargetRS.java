package com.spheresuite.erp.crm.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.crm.service.SalesTargetService;
import com.spheresuite.erp.crm.web.util.SalesTargetUtil;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.RolesDO;
import com.spheresuite.erp.domainobject.SalesTargetDO;
import com.spheresuite.erp.domainobject.UserDO;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.RolesService;
import com.spheresuite.erp.service.UserService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/salestarget")
public class SalesTargetRS {

	String validation = null;
	static Logger logger = Logger.getLogger(SalesTargetRS.class.getName());

	@Autowired
	private SalesTargetService salesTargetService;
	
	@Autowired
	private EmployeeService employeeService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private RolesService rolesService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				SalesTargetDO salesTargetDO = new SalesTargetDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.EMPID) != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			List <EmployeeDO> emplist = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
			 			if(emplist != null && emplist.size() > 0){
			 				salesTargetDO.setEmployee(emplist.get(0));
			 			}
			 		}
			 		if(inputJSON.get(CommonConstants.FROMDATE) != null && !inputJSON.get(CommonConstants.FROMDATE).toString().isEmpty()){
			 			salesTargetDO.setFromDate(Long.parseLong(inputJSON.get(CommonConstants.FROMDATE).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.TODATE) != null && !inputJSON.get(CommonConstants.TODATE).toString().isEmpty()){
			 			salesTargetDO.setToDate(Long.parseLong(inputJSON.get(CommonConstants.TODATE).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.Q1) != null && !inputJSON.get(CommonConstants.Q1).toString().isEmpty()){
			 			salesTargetDO.setQ1(Double.parseDouble(inputJSON.get(CommonConstants.Q1).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.Q2) != null && !inputJSON.get(CommonConstants.Q2).toString().isEmpty()){
			 			salesTargetDO.setQ2(Double.parseDouble(inputJSON.get(CommonConstants.Q2).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.Q3) != null && !inputJSON.get(CommonConstants.Q3).toString().isEmpty()){
			 			salesTargetDO.setQ3(Double.parseDouble(inputJSON.get(CommonConstants.Q3).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.Q4) != null && !inputJSON.get(CommonConstants.Q4).toString().isEmpty()){
			 			salesTargetDO.setQ4(Double.parseDouble(inputJSON.get(CommonConstants.Q4).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			salesTargetDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			salesTargetDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		salesTargetDO.setUpdatedon(new Date());
			 		salesTargetDO.setCreatedon(new Date());
			 		salesTargetService.persist(salesTargetDO);
			 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Sales Taraget Created");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retriveById/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveById(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<SalesTargetDO> salesTargetList = salesTargetService.retrieveByYear(Long.parseLong(inputJSON.get(CommonConstants.FROMDATE).toString()), Long.parseLong(inputJSON.get(CommonConstants.TODATE).toString()));
					respJSON = SalesTargetUtil.getSalesTargetList(salesTargetList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveByYear/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByYear(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.FROMDATE).toString().isEmpty() && !inputJSON.get(CommonConstants.TODATE).toString().isEmpty()){
					List<SalesTargetDO> salesTargetList = salesTargetService.retriveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = SalesTargetUtil.getSalesTargetList(salesTargetList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {

		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				int i = 0;
				List<Long> deptIds = new ArrayList<Long>();
				if(request.getSession().getAttribute("deptIds") != null){
					deptIds = (List<Long>) request.getSession().getAttribute("deptIds");
				}
				List<String> empIds = new ArrayList<String>();
				if(deptIds != null && deptIds.size() > 0){
					/*List<String> newList = employeeService.retrieveByDeptIds(deptIds);
					for (String deptId : newList) { 
						empIds.add(String.valueOf(deptId)); 
					}*/
					empIds = employeeService.retrieveByDeptIds(deptIds);
				}else if(request.getSession().getAttribute("empIds") != null){
					empIds = new ArrayList<String>();
					String empid = request.getSession().getAttribute("empIds").toString();
					empIds.add(empid);
					List<UserDO> userList = userService.retriveByEmpId(empid.toString());
					if(userList.get(0).getRole().getRoleId() != null){
						List<RolesDO> roleList = rolesService.retriveById(userList.get(0).getRole().getRoleId());
						if(roleList != null && roleList.size() > 0){
							if(roleList.get(0).getName().equalsIgnoreCase(CommonConstants.SUPERADMIN)){
								i=1;
							}
						}else{
							empIds = new ArrayList<String>();
						}
					}else{
						empIds = new ArrayList<String>();
					}
				}
				if(i==1){
					List<SalesTargetDO> salesTargetList = salesTargetService.retrieveAll();
					respJSON = SalesTargetUtil.getSalesTargetList(salesTargetList).toString();
				}else{
					List<SalesTargetDO> salesTargetList = salesTargetService.retrieve(empIds);
					respJSON = SalesTargetUtil.getSalesTargetList(salesTargetList).toString();
					/*List<SalesTargetDO> salesTargetList = salesTargetService.retrieveAll();
					respJSON = SalesTargetUtil.getSalesTargetList(salesTargetList).toString();*/
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				SalesTargetDO salesTargetDO = new SalesTargetDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		
			 		List<SalesTargetDO> salesTargetList = salesTargetService.retriveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(salesTargetList != null && salesTargetList.size() > 0){
			 			salesTargetDO = salesTargetList.get(0);
			 			if(inputJSON.get(CommonConstants.EMPID) != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
				 			List <EmployeeDO> emplist = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
				 			if(emplist != null && emplist.size() > 0){
				 				salesTargetDO.setEmployee(emplist.get(0));
				 			}
				 		}
				 		if(inputJSON.get(CommonConstants.FROMDATE) != null && !inputJSON.get(CommonConstants.FROMDATE).toString().isEmpty()){
				 			salesTargetDO.setFromDate(Long.parseLong(inputJSON.get(CommonConstants.FROMDATE).toString()));
				 		}
				 		if(inputJSON.get(CommonConstants.TODATE) != null && !inputJSON.get(CommonConstants.TODATE).toString().isEmpty()){
				 			salesTargetDO.setToDate(Long.parseLong(inputJSON.get(CommonConstants.TODATE).toString()));
				 		}
				 		if(inputJSON.get(CommonConstants.Q1) != null && !inputJSON.get(CommonConstants.Q1).toString().isEmpty()){
				 			salesTargetDO.setQ1(Double.parseDouble(inputJSON.get(CommonConstants.Q1).toString()));
				 		}
				 		if(inputJSON.get(CommonConstants.Q2) != null && !inputJSON.get(CommonConstants.Q2).toString().isEmpty()){
				 			salesTargetDO.setQ2(Double.parseDouble(inputJSON.get(CommonConstants.Q2).toString()));
				 		}
				 		if(inputJSON.get(CommonConstants.Q3) != null && !inputJSON.get(CommonConstants.Q3).toString().isEmpty()){
				 			salesTargetDO.setQ3(Double.parseDouble(inputJSON.get(CommonConstants.Q3).toString()));
				 		}
				 		if(inputJSON.get(CommonConstants.Q4) != null && !inputJSON.get(CommonConstants.Q4).toString().isEmpty()){
				 			salesTargetDO.setQ4(Double.parseDouble(inputJSON.get(CommonConstants.Q4).toString()));
				 		}
				 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			salesTargetDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		salesTargetDO.setUpdatedon(new Date());
				 		salesTargetService.update(salesTargetDO);
				 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Sales Target Updated");
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveBetweenDate/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String retrieveBetweenDate(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.FROMDATE).toString().isEmpty() &&
						!inputJSON.get(CommonConstants.TODATE).toString().isEmpty()){
					Long fromDate = Long.parseLong(inputJSON.get(CommonConstants.FROMDATE).toString());
					Long toDate = Long.parseLong(inputJSON.get(CommonConstants.TODATE).toString());
					String empId = null;
					if(inputJSON.get(CommonConstants.EMPID).toString() != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
						empId = inputJSON.get(CommonConstants.EMPID).toString();
					}
					if(empId != null){
						List<SalesTargetDO> salesTargetList = salesTargetService.retrieveByYearForEmp(fromDate, toDate,empId);
						respJSON = SalesTargetUtil.getSalesTargetList(salesTargetList).toString();
					}else{
						List<SalesTargetDO> salesTargetList = salesTargetService.retrieveByYear(fromDate, toDate);
						respJSON = SalesTargetUtil.getSalesTargetList(salesTargetList).toString();
					}
				}else if(!inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
					String empId = null;
					if(inputJSON.get(CommonConstants.EMPID).toString() != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
						empId = inputJSON.get(CommonConstants.EMPID).toString();
					}
					List<SalesTargetDO> salesTargetList = salesTargetService.retrieveByEmpId(empId);
					respJSON = SalesTargetUtil.getSalesTargetList(salesTargetList).toString();
				}else{
					List<SalesTargetDO> salesTargetList = salesTargetService.retrieveAll();
					respJSON = SalesTargetUtil.getSalesTargetList(salesTargetList).toString();
				}
			
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
}
