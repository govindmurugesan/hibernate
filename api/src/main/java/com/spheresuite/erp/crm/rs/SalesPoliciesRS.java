package com.spheresuite.erp.crm.rs;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.auth0.jwt.JWTVerifyException;
import com.spheresuite.erp.crm.service.SalesPoliciesService;
import com.spheresuite.erp.crm.web.util.SalesPoliciesUtil;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.RolesDO;
import com.spheresuite.erp.domainobject.SalesPoliciesDO;
import com.spheresuite.erp.domainobject.UserDO;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.RolesService;
import com.spheresuite.erp.service.UserService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/salespolicy")
public class SalesPoliciesRS {

	String validation = null;
	static Logger logger = Logger.getLogger(SalesPoliciesRS.class.getName());

	@Autowired
	private SalesPoliciesService salesPoliciesService;
	
	@Autowired
	private EmployeeService employeeService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private RolesService rolesService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && inputJSON.get(CommonConstants.VALUE) != null && !inputJSON.get(CommonConstants.VALUE).toString().isEmpty()){
		 			SalesPoliciesDO salesPoliciesDO = new SalesPoliciesDO(); 
		 			if(inputJSON.get(CommonConstants.VALUE) != null && !inputJSON.get(CommonConstants.VALUE).toString().isEmpty()){
		 				salesPoliciesDO.setName(inputJSON.get(CommonConstants.VALUE).toString());
		 			}
	 				salesPoliciesDO.setUpdatedon(new Date());
	 				if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			List <EmployeeDO> emplist = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			if(emplist != null && emplist.size() > 0){
			 				salesPoliciesDO.setEmployee(emplist.get(0));
			 			}
			 		}
		 			if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
		 				salesPoliciesDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
		 				salesPoliciesDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
		 			salesPoliciesDO.setStatus(CommonConstants.ACTIVE);
		 			salesPoliciesService.persist(salesPoliciesDO);
		 			CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Sales Policy Updated");
			 	}
				
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) throws InvalidKeyException, NoSuchAlgorithmException, IllegalStateException, SignatureException, IOException, JWTVerifyException {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				int i = 0;
				List<Long> deptIds = new ArrayList<Long>();
				if(request.getSession().getAttribute("deptIds") != null){
					deptIds = (List<Long>) request.getSession().getAttribute("deptIds");
				}
				List<String> empIds = new ArrayList<String>();
				if(deptIds != null && deptIds.size() > 0){
					/*List<String> newList = employeeService.retrieveByDeptIds(deptIds);
					for (String deptId : newList) { 
						empIds.add(String.valueOf(deptId)); 
					}*/
					empIds = employeeService.retrieveByDeptIds(deptIds);
				}else if(request.getSession().getAttribute("empIds") != null){
					empIds = new ArrayList<String>();
					String empid = request.getSession().getAttribute("empIds").toString();
					empIds.add(empid);
					List<UserDO> userList = userService.retriveByEmpId(empid.toString());
					if(userList.get(0).getRole().getRoleId() != null){
						List<RolesDO> roleList = rolesService.retriveById(userList.get(0).getRole().getRoleId());
						if(roleList != null && roleList.size() > 0){
							if(roleList.get(0).getName().equalsIgnoreCase(CommonConstants.SUPERADMIN)){
								i=1;
							}
						}else{
							empIds = new ArrayList<String>();
						}
					}else{
						empIds = new ArrayList<String>();
					}
				}
				
				if(i==1){
					List<SalesPoliciesDO> salesPoliciesList = salesPoliciesService.retrieveAll();
					respJSON = SalesPoliciesUtil.getSalesPoliciesList(salesPoliciesList).toString();
				}else{
					List<SalesPoliciesDO> salesPoliciesList = salesPoliciesService.retrieve(empIds);
					if(salesPoliciesList != null && salesPoliciesList.size() > 0){
						respJSON = SalesPoliciesUtil.getSalesPoliciesList(salesPoliciesList).toString();
					}
					/*List<SalesPoliciesDO> salesPoliciesList = salesPoliciesService.retrieveAll();
					respJSON = SalesPoliciesUtil.getSalesPoliciesList(salesPoliciesList).toString();*/
				}
				
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && inputJSON.get(CommonConstants.ID) != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 			if(!inputJSON.get(CommonConstants.VALUE).toString().isEmpty()){
			 				SalesPoliciesDO salesPoliciesDO = new SalesPoliciesDO(); 
			 				List<SalesPoliciesDO> salesPolicyList = salesPoliciesService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 				if(salesPolicyList != null && salesPolicyList.size() > 0){
			 					salesPoliciesDO = salesPolicyList.get(0);
			 				}
			 				salesPoliciesDO.setName(inputJSON.get(CommonConstants.VALUE).toString());
			 				salesPoliciesDO.setUpdatedon(new Date());
			 				if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 				salesPoliciesDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 		}
			 				salesPoliciesDO.setStatus((char) (inputJSON != null ? inputJSON.get(CommonConstants.STATUS).toString().charAt(0) : ""));
			 				salesPoliciesService.update(salesPoliciesDO);
			 				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Sales Policy Updated");
			 			}
			 		}
			 	}
				
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
