package com.spheresuite.erp.crm.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.TaskDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class TaskDAO {
	static Logger logger = Logger.getLogger(TaskDAO.class.getName());

	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private GenericDAOImpl<TaskDO> genericObject;
	
	public TaskDO persist(TaskDO taskDO) {
		try {
			genericObject.persist(taskDO);
		} catch (Exception eException) {
			//throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			//SessionManager.closeEntityManager(em);
		}
		return taskDO;
	}

	@SuppressWarnings("unchecked")
	public List<TaskDO> retrieveById(Long Id) {
		List<TaskDO> taskList = null;
		try {
			taskList =  this.sessionFactory.getCurrentSession().getNamedQuery(TaskDO.FIND_BY_ID)
							.setParameter(CommonConstants.ID, Id).list();
;
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return taskList;
	}
	
	@SuppressWarnings("unchecked")
	public List<TaskDO> retrieveByLeadId(Long Id) {
		List<TaskDO> taskList = null;
		try {
			taskList = this.sessionFactory.getCurrentSession().getNamedQuery(TaskDO.FIND_BY_LEAD_ID)
									.setParameter(CommonConstants.ID, Id)
									.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return taskList;
	}
	
	public TaskDO update(TaskDO taskDO) {
		try {
			genericObject.merge(taskDO);
		} catch (Exception eException) {
		} finally {
		}
		return taskDO;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<TaskDO> retrievePendingTask() {
		List<TaskDO> taskList = null;
		try {
			taskList = this.sessionFactory.getCurrentSession().getNamedQuery(TaskDO.FIND_PENDING_TASK)
									.setParameter(CommonConstants.STATUS, CommonConstants.PENDING_STATUS)
									.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return taskList;
	}
}
