package com.spheresuite.erp.crm.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.crm.service.OpportunitiesDocService;
import com.spheresuite.erp.crm.service.OpportunitiesService;
import com.spheresuite.erp.crm.service.ProjectService;
import com.spheresuite.erp.crm.web.util.ProjectUtil;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.GSTTaxSlabDO;
import com.spheresuite.erp.domainobject.OpportunitiesDO;
import com.spheresuite.erp.domainobject.OpportunitiesDocDO;
import com.spheresuite.erp.domainobject.ProjectDO;
import com.spheresuite.erp.domainobject.RolesDO;
import com.spheresuite.erp.domainobject.UserDO;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.GSTTaxSlabService;
import com.spheresuite.erp.service.RolesService;
import com.spheresuite.erp.service.UserService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/project")
public class ProjectRS {

	String validation = null;
	static Logger logger = Logger.getLogger(ProjectRS.class.getName());

	@Autowired
	private ProjectService projectService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private EmployeeService employeeService;
	
	@Autowired
	private RolesService rolesService;
	
	@Autowired
	private OpportunitiesDocService opportunitiesDocService;
	
	@Autowired
	private OpportunitiesService opportunitiesService;
	
	@Autowired
	private GSTTaxSlabService gstTaxSlabService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		ProjectDO projectDO = new ProjectDO();
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.COST) != null && !inputJSON.get(CommonConstants.COST).toString().isEmpty()){
			 			projectDO.setAmount(Long.parseLong(inputJSON.get(CommonConstants.COST).toString()));
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.OPPORTUNITY_ID) != null &&  !inputJSON.get(CommonConstants.OPPORTUNITY_ID).toString().isEmpty()){
			 			List<OpportunitiesDO> oppList = opportunitiesService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.OPPORTUNITY_ID).toString()));
			 			if(oppList != null && oppList.size() > 0){
			 				projectDO.setOpportunity(oppList.get(0));
			 			}
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.PROBOBILITY) != null &&  !inputJSON.get(CommonConstants.PROBOBILITY).toString().isEmpty()){
			 			projectDO.setProbability(Long.parseLong(inputJSON.get(CommonConstants.PROBOBILITY).toString()));
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.GST) != null && !inputJSON.get(CommonConstants.GST).toString().isEmpty()){
			 			List<GSTTaxSlabDO> gstList = gstTaxSlabService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.GST).toString()));
			 			if(gstList != null && gstList.size() > 0){
			 				projectDO.setGstTaxSlab(gstList.get(0));
			 			}
			 		}
			 		projectDO.setProjectname(!inputJSON.get(CommonConstants.PROJECTNAME).toString().isEmpty() ? inputJSON.get(CommonConstants.PROJECTNAME).toString() : null);
			 		projectDO.setDisplayname(!inputJSON.get(CommonConstants.DISPLAYNAME).toString().isEmpty() ? inputJSON.get(CommonConstants.DISPLAYNAME).toString() : null);
			 		projectDO.setStartdate(!inputJSON.get(CommonConstants.STARTDATE).toString().isEmpty() ? inputJSON.get(CommonConstants.STARTDATE).toString() : null);
			 		projectDO.setEnddate(!inputJSON.get(CommonConstants.ENDDATE).toString().isEmpty() ? inputJSON.get(CommonConstants.ENDDATE).toString() : null);
			 		if(!inputJSON.get(CommonConstants.PROJECTDURATION).toString().isEmpty()){
			 			projectDO.setProjectduration(inputJSON.get(CommonConstants.PROJECTDURATION).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.EMPID) != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			List <EmployeeDO> emplist = employeeService.retriveByEmpId(inputJSON.get(CommonConstants.EMPID).toString());
			 			if(emplist != null && emplist.size() > 0){
			 				projectDO.setEmployee(emplist.get(0));
			 			}
			 		}
			 		projectDO.setComment(!inputJSON.get(CommonConstants.COMMENT).toString().isEmpty() ? inputJSON.get(CommonConstants.COMMENT).toString() : null);
			 		projectDO.setUpdatedon(new Date());
			 		projectDO.setCreatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			projectDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			projectDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 	}
				projectService.persist(projectDO);
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Project Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponseId(projectDO.getProjectId().toString()).toString();
	}
	
	@RequestMapping(value = "/retrieveActive/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveById(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<ProjectDO> ProjectList = projectService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = ProjectUtil.getProjectList(ProjectList).toString();
				}
			
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveByOpportunityId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByOpportunityId(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<ProjectDO> ProjectList = projectService.retrieveByOpportunityId(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = ProjectUtil.getProjectList(ProjectList).toString();
				}
			
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				int i = 0;
				List<Long> deptIds = new ArrayList<Long>();
				if(request.getSession().getAttribute("deptIds") != null){
					deptIds = (List<Long>) request.getSession().getAttribute("deptIds");
				}
				List<String> empIds = new ArrayList<String>();
				if(deptIds != null && deptIds.size() > 0){
					empIds = employeeService.retrieveByDeptIds(deptIds);
					/*List<String> newList = employeeService.retrieveByDeptIds(deptIds);
					for (String deptId : newList) { 
						empIds.add(String.valueOf(deptId)); 
					}*/
				}else if(request.getSession().getAttribute("empIds") != null){
					empIds = new ArrayList<String>();
					String empid = (request.getSession().getAttribute("empIds").toString());
					empIds.add(empid);
					List<UserDO> userList = userService.retriveByEmpId(empid.toString());
					if(userList.get(0).getRole().getRoleId() != null){
						List<RolesDO> roleList = rolesService.retriveById(userList.get(0).getRole().getRoleId());
						if(roleList != null && roleList.size() > 0){
							if(roleList.get(0).getName().equalsIgnoreCase(CommonConstants.SUPERADMIN)){
								i=1;
							}
						}else{
							empIds = new ArrayList<String>();
						}
					}else{
						empIds = new ArrayList<String>();
					}
				}
				if(i==1){
					List<ProjectDO> categoryList = projectService.retrieveAll();
					respJSON = ProjectUtil.getProjectList(categoryList).toString();
				}else{
					/*List<ProjectDO> categoryList = projectService.retrieve(empIds);
					respJSON = ProjectUtil.getProjectList(categoryList).toString();*/
					List<ProjectDO> categoryList = projectService.retrieveAll();
					respJSON = ProjectUtil.getProjectList(categoryList).toString();
				}
				
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				ProjectDO projectDO = new ProjectDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<ProjectDO> ProjectList = projectService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		projectDO = ProjectList.get(0);
			 		if(!inputJSON.get(CommonConstants.COST).toString().isEmpty()){
			 			projectDO.setAmount(Long.parseLong(inputJSON.get(CommonConstants.COST).toString()));
			 		}
			 		if(!inputJSON.get(CommonConstants.PROBOBILITY).toString().isEmpty()){
			 			projectDO.setProbability(Long.parseLong(inputJSON.get(CommonConstants.PROBOBILITY).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.OPPORTUNITY_ID) != null &&  !inputJSON.get(CommonConstants.OPPORTUNITY_ID).toString().isEmpty()){
			 			List<OpportunitiesDO> oppList = opportunitiesService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.OPPORTUNITY_ID).toString()));
			 			if(oppList != null && oppList.size() > 0){
			 				projectDO.setOpportunity(oppList.get(0));
			 			}
			 		}
			 		if(inputJSON.get(CommonConstants.GST) != null && !inputJSON.get(CommonConstants.GST).toString().isEmpty()){
			 			List<GSTTaxSlabDO> gstList = gstTaxSlabService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.GST).toString()));
			 			if(gstList != null && gstList.size() > 0){
			 				projectDO.setGstTaxSlab(gstList.get(0));
			 			}
			 		}
			 		projectDO.setProjectname(!inputJSON.get(CommonConstants.PROJECTNAME).toString().isEmpty() ? inputJSON.get(CommonConstants.PROJECTNAME).toString() : null);
			 		projectDO.setDisplayname(!inputJSON.get(CommonConstants.DISPLAYNAME).toString().isEmpty() ? inputJSON.get(CommonConstants.DISPLAYNAME).toString() : null);
			 		projectDO.setStartdate(!inputJSON.get(CommonConstants.STARTDATE).toString().isEmpty() ? inputJSON.get(CommonConstants.STARTDATE).toString() : null);
			 		projectDO.setEnddate(!inputJSON.get(CommonConstants.ENDDATE).toString().isEmpty() ? inputJSON.get(CommonConstants.ENDDATE).toString() : "");
			 		if(!inputJSON.get(CommonConstants.PROJECTDURATION).toString().isEmpty()){
				 		projectDO.setProjectduration(inputJSON.get(CommonConstants.PROJECTDURATION).toString());
			 		}
			 		projectDO.setComment(!inputJSON.get(CommonConstants.COMMENT).toString().isEmpty() ? inputJSON.get(CommonConstants.COMMENT).toString() : "");
			 		projectDO.setUpdatedon(new Date());
			 		//projectDO.setCreatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			projectDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		projectService.update(projectDO);
			 		opportunitiesDocService.delete(projectDO.getProjectId());
			 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Project Updated");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retrieveByProjectId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByProjectId(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					//List<ProjectDO> ProjectList = projectService.retrieveByProjectId(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					List<ProjectDO> ProjectDetail = projectService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = ProjectUtil.getProjectList(ProjectDetail).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/updatepic", method = RequestMethod.POST)
	public @ResponseBody String updatepic(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				OpportunitiesDocDO opportunitiesDocDO = new OpportunitiesDocDO();
				/*JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("data").toString());*/
			 	if (request.getParameter("file") != null && request.getParameter("id") != null){
			 		List<OpportunitiesDO> oppList = opportunitiesService.retrieveById(Long.parseLong(request.getParameter("id").toString()));
			 		opportunitiesDocDO.setOpportunity(oppList.get(0));
			 		//opportunitiesDocDO.setOpportunityId(Long.parseLong(request.getParameter("id")));
			 		opportunitiesDocDO.setPhoto(request.getParameter("file"));
			 		opportunitiesDocDO.setFileName(request.getParameter("name"));
			 		opportunitiesDocService.update(opportunitiesDocDO);
 			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
