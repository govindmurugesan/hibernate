package com.spheresuite.erp.crm.web.util;

import java.util.List;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.ContactDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
@Component
public class ContactUtil {
	
	private ContactUtil() {}
	
	@Autowired
	private EmployeeService temployeeService;
	
	/*@Autowired
	private LeadService tleadService;
	
	@Autowired
	private SalutationService tsalutationService;
	
	@Autowired
	private ContactTypeService tcontactTypeService;*/
	
	
	private static EmployeeService employeeService;
	
/*	private static LeadService leadService;
	
	private static SalutationService salutationService;
	
	private static ContactTypeService contactTypeService;
*/	
	@PostConstruct
	public void init() {
		employeeService = temployeeService;
		/*leadService = tleadService;
		salutationService = tsalutationService;
		contactTypeService = tcontactTypeService;*/
	}
	
	
	
	public static JSONObject getContactList(List<ContactDO> contactList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (ContactDO contact : contactList) {
				resultJSONArray.put(getContactDetailObject(contact));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getContactDetailObject(ContactDO contact)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(contact.getContactId()));
		if(contact.getLead() !=null){
			result.put(CommonConstants.LEADTYPE, String.valueOf(contact.getLead().getLeadId()));
		}else{
			result.put(CommonConstants.LEADTYPE, "");
		}
		if(contact.getSalutation() !=null){
			result.put(CommonConstants.SALUTATION, String.valueOf(contact.getSalutation().getSalutationId()));
		}else{
			result.put(CommonConstants.SALUTATION, "");
		}
		String name="";
		if(contact.getFirstname() !=null){
			result.put(CommonConstants.FIRSTNAME, String.valueOf(contact.getFirstname()));
			name = name+" "+contact.getFirstname();
		}else{
			result.put(CommonConstants.FIRSTNAME, "");
		}
		if(contact.getMiddlename() !=null){
			result.put(CommonConstants.MIDDLENAME, String.valueOf(contact.getMiddlename()));
			name = name+" "+contact.getMiddlename();
		}else{
			result.put(CommonConstants.MIDDLENAME, "");
		}
		if(contact.getLastname() !=null){
			result.put(CommonConstants.LASTNAME, String.valueOf(contact.getLastname()));
			name = name+" "+contact.getLastname();
		}else{
			result.put(CommonConstants.LASTNAME, "");
		}
		result.put(CommonConstants.NAME, name);
		
		if(contact.getSecondaryemail() !=null){
			result.put(CommonConstants.SECONDARYEMAIL, String.valueOf(contact.getSecondaryemail()));
		}else{
			result.put(CommonConstants.SECONDARYEMAIL, "");
		}
		
		if(contact.getPrimaryemail() !=null){
			result.put(CommonConstants.PRIMARYEMAIL, String.valueOf(contact.getPrimaryemail()));
		}else{
			result.put(CommonConstants.PRIMARYEMAIL, "");
		}
		
		if(contact.getAddress1() !=null){
			result.put(CommonConstants.ADDRESS, String.valueOf(contact.getAddress1()));
		}else{
			result.put(CommonConstants.ADDRESS, "");
		}
		
		/*if(contact.getAddress2() !=null){
			result.put(CommonConstants.ADDRESS2, String.valueOf(contact.getAddress2()));
		}else{
			result.put(CommonConstants.ADDRESS2, "");
		}*/
		
		if(contact.getContactType() !=null){
			result.put(CommonConstants.CONTACTTYPE, String.valueOf(contact.getContactType().getContactTypeId()));
		}else{
			result.put(CommonConstants.CONTACTTYPE, "");
		}
		
		if(contact.getDesignation() !=null){
			result.put(CommonConstants.DESIGNATION, String.valueOf(contact.getDesignation()));
		}else{
			result.put(CommonConstants.DESIGNATION, "");
		}
		
		if(contact.getMobile1() !=null){
			result.put(CommonConstants.MOBILE, String.valueOf(contact.getMobile1()));
		}else{
			result.put(CommonConstants.MOBILE, "");
		}
		
		/*if(contact.getMobile2() !=null){
			result.put(CommonConstants.MOBILE2, String.valueOf(contact.getMobile2()));
		}else{
			result.put(CommonConstants.MOBILE2, "");
		}*/
		
		if(contact.getPhone1() !=null){
			result.put(CommonConstants.PHONE, String.valueOf(contact.getPhone1()));
		}else{
			result.put(CommonConstants.PHONE, "");
		}
		
		/*if(contact.getPhone2() !=null){
			result.put(CommonConstants.PHONE1, String.valueOf(contact.getPhone2()));
		}else{
			result.put(CommonConstants.PHONE1, "");
		}
		
		if(contact.getPhone3() !=null){
			result.put(CommonConstants.PHONE2, String.valueOf(contact.getPhone3()));
		}else{
			result.put(CommonConstants.PHONE2, "");
		}
		
		if(contact.getPhone4() !=null){
			result.put(CommonConstants.PHONE3, String.valueOf(contact.getPhone4()));
		}else{
			result.put(CommonConstants.PHONE3, "");
		}*/
		
		if(contact.getFax() !=null){
			result.put(CommonConstants.FAX, String.valueOf(contact.getFax()));
		}else{
			result.put(CommonConstants.FAX, "");
		}
		
		if(contact.getWebsite() !=null){
			result.put(CommonConstants.WEBSITE, String.valueOf(contact.getWebsite()));
		}else{
			result.put(CommonConstants.WEBSITE, "");
		}
		
		if(contact.getRequirements() !=null){
			result.put(CommonConstants.REQUIREMENT, String.valueOf(contact.getRequirements()));
		}else{
			result.put(CommonConstants.REQUIREMENT, "");
		}
		
		if(contact.getNotes() !=null){
			result.put(CommonConstants.NOTE, String.valueOf(contact.getNotes()));
		}else{
			result.put(CommonConstants.NOTE, "");
		}
		
		if(contact.getComments() !=null){
			result.put(CommonConstants.COMMENT, String.valueOf(contact.getComments()));
		}else{
			result.put(CommonConstants.COMMENT, "");
		}
		
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(contact.getUpdatedon())));
		if(contact.getUpdatedby() != null){
			
			List<EmployeeDO> empList = employeeService.retrieveEmpId(contact.getUpdatedby().toString());
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
			
			/*List<UserDO> userList = userService.retriveById(Long.parseLong(contact.getUpdatedby()));
			if(userList != null && userList.size() > 0){
				result.put(CommonConstants.UPDATED_BY, String.valueOf(userList.get(0).getName())); 
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}*/
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}		
		if(contact.getLead() !=null){
				result.put(CommonConstants.LEAD_TYPE_NAME, String.valueOf(contact.getLead().getName()));
		}else{
			result.put(CommonConstants.LEAD_TYPE_NAME, "");
		}
		
		if(contact.getSalutation() != null){
			result.put(CommonConstants.SALUTATION_NAME,contact.getSalutation().getName());
		}else{
			result.put(CommonConstants.SALUTATION_NAME, "");
		}
		
		if(contact.getContactType() != null){
			result.put(CommonConstants.CONTACTTYPE_NAME, contact.getContactType().getName());
		}else{
			result.put(CommonConstants.CONTACTTYPE_NAME, "");
		}
		
		return result;
	}
	
	public static JSONObject getMoMContactList(List<ContactDO> contactList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (ContactDO contact : contactList) {
				resultJSONArray.put(getMoMContactDetailObject(contact));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getMoMContactDetailObject(ContactDO contact)throws JSONException {
		JSONObject result = new JSONObject();
		String name="";
		if(contact.getFirstname() !=null){
			name = name+" "+contact.getFirstname();
		}else{
			result.put(CommonConstants.CONTACT_NAME, name);
		}
		if(contact.getMiddlename() !=null){
			name = name+" "+contact.getMiddlename();
		}else{
			result.put(CommonConstants.CONTACT_NAME, name);
		}
		if(contact.getLastname() !=null){
			name = name+" "+contact.getLastname();
		}else{
			result.put(CommonConstants.CONTACT_NAME, name);
		}
		result.put(CommonConstants.CONTACT_NAME, name);
		
		if(contact.getPrimaryemail() !=null){
			result.put(CommonConstants.EMAIL, String.valueOf(contact.getPrimaryemail()));
		}else{
			result.put(CommonConstants.EMAIL, "");
		}
		
		if(contact.getMobile1() !=null){
			result.put(CommonConstants.MOBILE, String.valueOf(contact.getMobile1()));
		}else{
			result.put(CommonConstants.MOBILE, "");
		}
		result.put(CommonConstants.TYPE, "old");
		return result;
	}
}
