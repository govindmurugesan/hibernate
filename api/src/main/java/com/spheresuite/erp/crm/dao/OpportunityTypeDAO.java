package com.spheresuite.erp.crm.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.OpportunityTypeDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class OpportunityTypeDAO {
	static Logger logger = Logger.getLogger(OpportunityTypeDAO.class.getName());

	@Autowired
	private GenericDAOImpl<OpportunityTypeDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(OpportunityTypeDO projectTypeDO) {
		List<OpportunityTypeDO> opportunityList = null;
		boolean persistStatus = true;
		try {
			opportunityList = this.sessionFactory.getCurrentSession().getNamedQuery(OpportunityTypeDO.FIND_BY_NAME)
			.setParameter(CommonConstants.NAME, projectTypeDO.getName())
			.list();
			if(opportunityList != null && opportunityList.size() > 0){
				persistStatus = false;
			}else{
				genericObject.persist(projectTypeDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	public List<OpportunityTypeDO> retrieve() {
		List<OpportunityTypeDO> projectTypeList = null;
		try {
			projectTypeList = genericObject.retrieve(OpportunityTypeDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return projectTypeList;
	}
	
	public List<OpportunityTypeDO> retrieveActive() {
		List<OpportunityTypeDO> projectTypeList = null;
		try {
			projectTypeList =  genericObject.retrieveActive(CommonConstants.ACTIVE, OpportunityTypeDO.FIND_BY_STATUS, OpportunityTypeDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return projectTypeList;
	}
	
	@SuppressWarnings("unchecked")
	public List<OpportunityTypeDO> retrieveById(Long Id) {
		List<OpportunityTypeDO> projectTypeList = null;
		try {
			projectTypeList =  this.sessionFactory.getCurrentSession().getNamedQuery(OpportunityTypeDO.FIND_BY_ID)
									.setParameter(CommonConstants.ID, Id).list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return projectTypeList;
	}
	
	@SuppressWarnings("unchecked")
	public boolean update(OpportunityTypeDO projectTypeDO) {
		boolean updateStatus = true;
		try {
			List<OpportunityTypeDO> opportunityTypeList = null;
			opportunityTypeList = this.sessionFactory.getCurrentSession().getNamedQuery(OpportunityTypeDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, projectTypeDO.getName())
					.setParameter(CommonConstants.ID, projectTypeDO.getOpportunityTypeId())
					.list();
			if(opportunityTypeList != null && opportunityTypeList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(projectTypeDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}
	
	public boolean persistList(List<OpportunityTypeDO> OpportunityTypeDOList){
		try {
			for (OpportunityTypeDO OpportunityTypeDO : OpportunityTypeDOList) {
				genericObject.persist(OpportunityTypeDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}

}
