package com.spheresuite.erp.crm.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.CustomerDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class CustomerDAO {
	static Logger logger = Logger.getLogger(CustomerDAO.class.getName());
	@Autowired
	private GenericDAOImpl<CustomerDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public CustomerDO persist(CustomerDO customerDO) {
		try {
			genericObject.persist(customerDO);
		} catch (Exception eException) {
		} finally {
		}
		return customerDO;
	}
	
	
	public List<CustomerDO> retrieve() {
		List<CustomerDO> projectList = null;
		try {
			projectList = genericObject.retrieve(CustomerDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return projectList;
	}
	
	@SuppressWarnings("unchecked")
	public List<CustomerDO> retrieveById(Long Id) {
		List<CustomerDO> projectList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(CustomerDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, Id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return projectList;
	}
	
	public List<CustomerDO> retrieveByActive() {
		List<CustomerDO> projectList = null;
		try {
			return genericObject.retrieveActive(CommonConstants.ACTIVE, CustomerDO.FIND_ACTIVE, CustomerDO.class);
		} catch (Exception eException) {
		} finally {
			
		}
		return projectList;
	}
	
	public CustomerDO update(CustomerDO customerDO) {
		try {
			genericObject.merge(customerDO);
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
		}
		return customerDO;
	}
	
	public boolean persistList(List<CustomerDO> customerDO) {
		try {
			for (CustomerDO customerDO2 : customerDO) {
				genericObject.persist(customerDO2);
			}
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
		}
		return true;
	}

}
