package com.spheresuite.erp.crm.service;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.crm.dao.ScheduleDAO;
import com.spheresuite.erp.domainobject.ScheduleDO;

@Service
@Transactional
public class ScheduleService {
	static Logger logger = Logger.getLogger(ScheduleService.class.getName());
	
	@Autowired
	private ScheduleDAO scheduleDAO;
	
	@Transactional
	public ScheduleDO persist(ScheduleDO scheduleDO) {
		return scheduleDAO.persist(scheduleDO);
	}
	
	@Transactional
	public ScheduleDO update(ScheduleDO scheduleDO) {
		return scheduleDAO.update(scheduleDO);
	}

	@Transactional
	public List<ScheduleDO> retrieveById(Long Id) {
		return scheduleDAO.retrieveById(Id);
	}

	@Transactional
	public List<ScheduleDO> retrieveByLeadId(Long Id) {
		return scheduleDAO.retrieveByLeadId(Id);
	}
	
	@Transactional
	public List<ScheduleDO> retrieveUpcomingCalender(String empid) {
		return scheduleDAO.retrieveUpcomingCalender(empid, new Date());
	}
	
	@Transactional
	public List<ScheduleDO> retrieveUpcomingCalenderOfEmp(String empid) {
		return scheduleDAO.retrieveUpcomingCalenderOfEmp(empid, new Date());
	}
}
