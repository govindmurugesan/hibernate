package com.spheresuite.erp.crm.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.crm.dao.TaskDAO;
import com.spheresuite.erp.domainobject.TaskDO;

@Service
@Transactional
public class TaskService {
	static Logger logger = Logger.getLogger(TaskService.class.getName());
	
	@Autowired
	private TaskDAO taskDAO;
	
	@Transactional
	public TaskDO persist(TaskDO taskDO) {
		return taskDAO.persist(taskDO);
	}

	@Transactional
	public List<TaskDO> retrieveById(Long Id) {
		return taskDAO.retrieveById(Id);
	}

	@Transactional
	public List<TaskDO> retrieveByLeadId(Long Id) {
		return taskDAO.retrieveByLeadId(Id);
	}
	
	@Transactional
	public TaskDO update(TaskDO taskDO) {
		return taskDAO.update(taskDO);
	}
	
	@Transactional
	public List<TaskDO> retrievePendingTask() {
		return taskDAO.retrievePendingTask();
	}
}
