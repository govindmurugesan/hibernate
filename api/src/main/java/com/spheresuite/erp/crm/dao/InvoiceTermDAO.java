package com.spheresuite.erp.crm.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.InvoiceTermDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class InvoiceTermDAO {
	static Logger logger = Logger.getLogger(InvoiceTermDAO.class.getName());
	@Autowired
	private GenericDAOImpl<InvoiceTermDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public List<InvoiceTermDO> persist(List<InvoiceTermDO> invoiceTermList)  {
		try {
			if(invoiceTermList != null && invoiceTermList.size() > 0){
				org.hibernate.Query query = this.sessionFactory.getCurrentSession().createQuery("delete from InvoiceTermDO e where e.proposalId=:id")
					.setParameter(CommonConstants.ID, invoiceTermList.get(0).getProposal().getProposalId());
				query.executeUpdate();
			}
			for (InvoiceTermDO invoiceTermDO : invoiceTermList) {
				genericObject.persist(invoiceTermDO);
			}
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return invoiceTermList;
	}
	
	public boolean delete(Long proposalId)  {
		try {
			org.hibernate.Query query = this.sessionFactory.getCurrentSession().createQuery("delete from InvoiceTermDO e where e.proposalId=:id")
					.setParameter(CommonConstants.ID, proposalId);
			query.executeUpdate();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return true;
	}
	
	@SuppressWarnings("unchecked")
	public List<InvoiceTermDO> retrieveById(Long proposalId)  {
		List<InvoiceTermDO> proposalList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(InvoiceTermDO.FIND_BY_PROPOSAL_ID)
			.setParameter(CommonConstants.PROPOSAL_ID, proposalId)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return proposalList;
	}
}
