package com.spheresuite.erp.crm.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.SalesPoliciesDO;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
@Component
public class SalesPoliciesUtil {
	
	private SalesPoliciesUtil() {}
	
	public static JSONObject getSalesPoliciesList(List<SalesPoliciesDO> salesPoliciesList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (SalesPoliciesDO salesPolicies : salesPoliciesList) {
				resultJSONArray.put(getSalesPoliciesDetailObject(salesPolicies));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getSalesPoliciesDetailObject(SalesPoliciesDO salesPolicies)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(salesPolicies.getSalesPoliciesId()));
		result.put(CommonConstants.NAME, String.valueOf(salesPolicies.getName()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(salesPolicies.getUpdatedon())));
		result.put(CommonConstants.STATUS, String.valueOf(salesPolicies.getStatus()));
		
		if(salesPolicies.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(salesPolicies.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		return result;
	}
}
