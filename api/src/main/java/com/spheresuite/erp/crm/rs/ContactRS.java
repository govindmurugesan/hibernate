package com.spheresuite.erp.crm.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.crm.service.ContactService;
import com.spheresuite.erp.crm.service.ContactTypeService;
import com.spheresuite.erp.crm.service.LeadService;
import com.spheresuite.erp.crm.service.OpportunitiesService;
import com.spheresuite.erp.crm.service.SalutationService;
import com.spheresuite.erp.crm.web.util.ContactUtil;
import com.spheresuite.erp.domainobject.ContactDO;
import com.spheresuite.erp.domainobject.ContactTypeDO;
import com.spheresuite.erp.domainobject.LeadDO;
import com.spheresuite.erp.domainobject.OpportunitiesDO;
import com.spheresuite.erp.domainobject.RolesDO;
import com.spheresuite.erp.domainobject.SalutationDO;
import com.spheresuite.erp.domainobject.UserDO;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.RolesService;
import com.spheresuite.erp.service.UserService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/contact")
public class ContactRS {

	String validation = null;
	static Logger logger = Logger.getLogger(ContactRS.class.getName());

	@Autowired
	private ContactService contactService;
	
	@Autowired
	private EmployeeService employeeService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private OpportunitiesService opportunitiesService;
	
	@Autowired
	private RolesService rolesService;
	
	@Autowired
	private LeadService leadService;
	
	@Autowired
	private SalutationService salutationService;
	
	@Autowired
	private ContactTypeService contactTypeService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				ContactDO contactDO = new ContactDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){

			 		/*if(!inputJSON.get(CommonConstants.LEADTYPE).toString().isEmpty() && inputJSON.get(CommonConstants.LEADTYPE) != null){
			 			contactDO.setLeadtype(Long.parseLong(inputJSON.get(CommonConstants.LEADTYPE).toString()));
			 		}*/
			 		if(inputJSON.get(CommonConstants.LEADTYPE) != null && !inputJSON.get(CommonConstants.LEADTYPE).toString().isEmpty()){
			 			List<LeadDO> leadList = leadService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.LEADTYPE).toString()));
			 			if(leadList != null && leadList.size() > 0){
			 				contactDO.setLead(leadList.get(0));
			 			}
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.SALUTATION) != null && !inputJSON.get(CommonConstants.SALUTATION).toString().isEmpty()){
			 			List<SalutationDO> salutationList = salutationService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.SALUTATION).toString()));
			 			if(salutationList != null && salutationList.size() > 0){
			 				contactDO.setSalutation(salutationList.get(0));
			 			}
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.CONTACTTYPE) != null && !inputJSON.get(CommonConstants.CONTACTTYPE).toString().isEmpty()){
			 			List<ContactTypeDO> contactTypeList = contactTypeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.CONTACTTYPE).toString()));
			 			if(contactTypeList != null && contactTypeList.size() > 0){
			 				contactDO.setContactType(contactTypeList.get(0));
			 			}
			 		}
			 		/*if(!inputJSON.get(CommonConstants.SALUTATION).toString().isEmpty()  && inputJSON.get(CommonConstants.SALUTATION) != null){
			 			contactDO.setSalutation(Long.parseLong(inputJSON.get(CommonConstants.SALUTATION).toString()));
			 		}*/
			 		contactDO.setFirstname(!inputJSON.get(CommonConstants.FIRSTNAME).toString().isEmpty()  && inputJSON.get(CommonConstants.FIRSTNAME) != null ? inputJSON.get(CommonConstants.FIRSTNAME).toString() : "");
			 		contactDO.setLastname(!inputJSON.get(CommonConstants.LASTNAME).toString().isEmpty()  && inputJSON.get(CommonConstants.LASTNAME) != null ? inputJSON.get(CommonConstants.LASTNAME).toString() : "");
			 		contactDO.setMiddlename(!inputJSON.get(CommonConstants.MIDDLENAME).toString().isEmpty()  && inputJSON.get(CommonConstants.MIDDLENAME) != null ? inputJSON.get(CommonConstants.MIDDLENAME).toString() : "");
			 		contactDO.setDesignation((!inputJSON.get(CommonConstants.DESIGNATION).toString().isEmpty()  && inputJSON.get(CommonConstants.DESIGNATION) != null ? inputJSON.get(CommonConstants.DESIGNATION).toString() : ""));
			 		/*if(!inputJSON.get(CommonConstants.CONTACTTYPE).toString().isEmpty()  && inputJSON.get(CommonConstants.CONTACTTYPE) != null){
			 			contactDO.setContattype((Long.parseLong(inputJSON.get(CommonConstants.CONTACTTYPE).toString())));
			 		}*/
			 		
			 		contactDO.setPrimaryemail((!inputJSON.get(CommonConstants.PRIMARYEMAIL).toString().isEmpty()  && inputJSON.get(CommonConstants.PRIMARYEMAIL) != null ? inputJSON.get(CommonConstants.PRIMARYEMAIL).toString() : ""));
			 		//contactDO.setSecondaryemail((!inputJSON.get(CommonConstants.SECONDARYEMAIL).toString().isEmpty() ? inputJSON.get(CommonConstants.SECONDARYEMAIL).toString() : ""));
			 		contactDO.setAddress1(!inputJSON.get(CommonConstants.ADDRESS).toString().isEmpty()  && inputJSON.get(CommonConstants.ADDRESS) != null ? inputJSON.get(CommonConstants.ADDRESS).toString() : "");
			 		/*contactDO.setAddress2(!inputJSON.get(CommonConstants.ADDRESS2).toString().isEmpty() ? inputJSON.get(CommonConstants.ADDRESS2).toString() : "");*/
			 		if(!inputJSON.get(CommonConstants.MOBILE).toString().isEmpty()  && inputJSON.get(CommonConstants.MOBILE) != null){
			 			contactDO.setMobile1(inputJSON != null ? inputJSON.get(CommonConstants.MOBILE).toString() : "");
			 		}
			 		
			 		/*if(!inputJSON.get(CommonConstants.MOBILE2).toString().isEmpty()){
			 			contactDO.setMobile2(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.MOBILE2).toString() : ""));
			 		}*/
			 		
			 		if(!inputJSON.get(CommonConstants.PHONE).toString().isEmpty()  && inputJSON.get(CommonConstants.PHONE) != null){
			 			contactDO.setPhone1(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.PHONE).toString() : ""));
			 		}
			 		
			 		/*if(!inputJSON.get(CommonConstants.PHONE1).toString().isEmpty()){
			 			contactDO.setPhone2(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.PHONE1).toString() : ""));
			 		}
			 		
			 		if(!inputJSON.get(CommonConstants.PHONE2).toString().isEmpty()){
			 			contactDO.setPhone3(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.PHONE2).toString() : ""));
			 		}
			 		
			 		if(!inputJSON.get(CommonConstants.PHONE3).toString().isEmpty()){
			 			contactDO.setPhone4(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.PHONE3).toString() : ""));
			 		}*/
			 		
			 		if(!inputJSON.get(CommonConstants.FAX).toString().isEmpty()  && inputJSON.get(CommonConstants.FAX) != null){
			 			contactDO.setFax(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.FAX).toString() : ""));
			 		}
			 		
			 		
			 		contactDO.setWebsite((!inputJSON.get(CommonConstants.WEBSITE).toString().isEmpty()  && inputJSON.get(CommonConstants.WEBSITE) != null ? inputJSON.get(CommonConstants.WEBSITE).toString() : ""));
			 		contactDO.setRequirements((!inputJSON.get(CommonConstants.REQUIREMENT).toString().isEmpty()  && inputJSON.get(CommonConstants.REQUIREMENT) != null ? inputJSON.get(CommonConstants.REQUIREMENT).toString() : ""));
			 		contactDO.setNotes((!inputJSON.get(CommonConstants.NOTE).toString().isEmpty()  && inputJSON.get(CommonConstants.NOTE) != null ? inputJSON.get(CommonConstants.NOTE).toString() : ""));
			 		contactDO.setComments((!inputJSON.get(CommonConstants.COMMENT).toString().isEmpty()  && inputJSON.get(CommonConstants.COMMENT) != null ? inputJSON.get(CommonConstants.COMMENT).toString() : ""));

			 		contactDO.setUpdatedon(new Date());
			 		contactDO.setCreatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			contactDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			contactDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			contactDO.setEmpId(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		contactDO.setIsDeleted("n");
			 	}
				contactService.persist(contactDO);
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Contact Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retriveById/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveById(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<ContactDO> contactList = contactService.retriveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = ContactUtil.getContactList(contactList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<Long> deptIds = new ArrayList<Long>();
				int i = 0;
				if(request.getSession().getAttribute("deptIds") != null){
					deptIds = (List<Long>) request.getSession().getAttribute("deptIds");
				}
				List<String> empIds = new ArrayList<String>();
				if(deptIds != null && deptIds.size() > 0){
					empIds = employeeService.retrieveByDeptIds(deptIds);
					/*List<String> newList = employeeService.retrieveByDeptIds(deptIds);
					for (String deptId : newList) { 
						empIds.add(String.valueOf(deptId)); 
					}*/
				}else if(request.getSession().getAttribute("empIds") != null){
					empIds = new ArrayList<String>();
					String empid = request.getSession().getAttribute("empIds").toString();
					empIds.add(empid);
					List<UserDO> userList = userService.retriveByEmpId(empid.toString());
					if(userList.get(0).getRole().getRoleId() != null){
						List<RolesDO> roleList = rolesService.retriveById(userList.get(0).getRole().getRoleId());
						if(roleList != null && roleList.size() > 0){
							if(roleList.get(0).getName().equalsIgnoreCase(CommonConstants.SUPERADMIN)){
								i=1;
							}
						}else{
							empIds = new ArrayList<String>();
						}
					}else{
						empIds = new ArrayList<String>();
					}
					//empIds = employeeService.retrieveByDeptIds(deptIds);
				}
				
				if(i==1){
					List<ContactDO> contactList = contactService.retrieveAll();
					respJSON = ContactUtil.getContactList(contactList).toString();
				}else{
					List<ContactDO> contactList = contactService.retrieve(empIds);
					if( contactList !=null && contactList.size() > 0){
						respJSON = ContactUtil.getContactList(contactList).toString();
					}
					//List<ContactDO> contactList = contactService.retrieveAll();
					//respJSON = ContactUtil.getContactList(contactList).toString();
				}
				
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retriveByEmailId/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String retriveByEmailId(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.EMAIL).toString().isEmpty()){
					List<ContactDO> contactList = contactService.retriveByEmailId(inputJSON.get(CommonConstants.EMAIL).toString());
					respJSON = ContactUtil.getContactList(contactList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				ContactDO contactDO = new ContactDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<ContactDO> contactList = contactService.retriveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		contactDO = contactList.get(0);
			 		/*if(!inputJSON.get(CommonConstants.LEADTYPE).toString().isEmpty() && inputJSON.get(CommonConstants.LEADTYPE) != null){
			 			contactDO.setLeadtype(Long.parseLong(inputJSON.get(CommonConstants.LEADTYPE).toString()));
			 		} else {
			 			contactDO.setLeadtype(null);
			 		}
			 		if(!inputJSON.get(CommonConstants.SALUTATION).toString().isEmpty()){
			 			contactDO.setSalutation(Long.parseLong(inputJSON.get(CommonConstants.SALUTATION).toString()));
			 		} else {
			 			contactDO.setSalutation(null);
			 		}*/
			 		if(inputJSON.get(CommonConstants.LEADTYPE) != null && !inputJSON.get(CommonConstants.LEADTYPE).toString().isEmpty()){
			 			List<LeadDO> leadList = leadService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.LEADTYPE).toString()));
			 			if(leadList != null && leadList.size() > 0){
			 				contactDO.setLead(leadList.get(0));
			 			}
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.SALUTATION) != null && !inputJSON.get(CommonConstants.SALUTATION).toString().isEmpty()){
			 			List<SalutationDO> salutationList = salutationService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.SALUTATION).toString()));
			 			if(salutationList != null && salutationList.size() > 0){
			 				contactDO.setSalutation(salutationList.get(0));
			 			}
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.CONTACTTYPE) != null && !inputJSON.get(CommonConstants.CONTACTTYPE).toString().isEmpty()){
			 			List<ContactTypeDO> contactTypeList = contactTypeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.CONTACTTYPE).toString()));
			 			if(contactTypeList != null && contactTypeList.size() > 0){
			 				contactDO.setContactType(contactTypeList.get(0));
			 			}
			 		}
			 		contactDO.setFirstname(!inputJSON.get(CommonConstants.FIRSTNAME).toString().isEmpty() ? inputJSON.get(CommonConstants.FIRSTNAME).toString() : "");
			 		contactDO.setLastname(!inputJSON.get(CommonConstants.LASTNAME).toString().isEmpty() ? inputJSON.get(CommonConstants.LASTNAME).toString() : "");
			 		contactDO.setMiddlename(!inputJSON.get(CommonConstants.MIDDLENAME).toString().isEmpty() ? inputJSON.get(CommonConstants.MIDDLENAME).toString() : "");
			 		contactDO.setDesignation((!inputJSON.get(CommonConstants.DESIGNATION).toString().isEmpty() ? inputJSON.get(CommonConstants.DESIGNATION).toString() : ""));
			 		/*if(!inputJSON.get(CommonConstants.CONTACTTYPE).toString().isEmpty()){
			 			contactDO.setContattype((Long.parseLong(inputJSON.get(CommonConstants.CONTACTTYPE).toString())));
			 		} else {
			 			contactDO.setContattype(null);
			 		}*/
			 		contactDO.setPrimaryemail((!inputJSON.get(CommonConstants.PRIMARYEMAIL).toString().isEmpty() ? inputJSON.get(CommonConstants.PRIMARYEMAIL).toString() : ""));
			 		//contactDO.setSecondaryemail((!inputJSON.get(CommonConstants.SECONDARYEMAIL).toString().isEmpty() ? inputJSON.get(CommonConstants.SECONDARYEMAIL).toString() : ""));
			 		contactDO.setAddress1(!inputJSON.get(CommonConstants.ADDRESS).toString().isEmpty() ? inputJSON.get(CommonConstants.ADDRESS).toString() : "");
			 		//contactDO.setAddress2(!inputJSON.get(CommonConstants.ADDRESS2).toString().isEmpty() ? inputJSON.get(CommonConstants.ADDRESS2).toString() : "");
			 		if(!inputJSON.get(CommonConstants.MOBILE).toString().isEmpty()){
			 			contactDO.setMobile1(inputJSON != null ? inputJSON.get(CommonConstants.MOBILE).toString() : "");
			 		} else {
			 			contactDO.setMobile1(null);
			 		}
			 		
			 		/*if(!inputJSON.get(CommonConstants.MOBILE2).toString().isEmpty()){
			 			contactDO.setMobile2(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.MOBILE2).toString() : ""));
			 		}*/
			 		
			 		if(!inputJSON.get(CommonConstants.PHONE).toString().isEmpty()){
			 			contactDO.setPhone1(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.PHONE).toString() : ""));
			 		} else {
			 			contactDO.setPhone1(null);
			 		}
			 		
			 		/*if(!inputJSON.get(CommonConstants.PHONE1).toString().isEmpty()){
			 			contactDO.setPhone2(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.PHONE1).toString() : ""));
			 		}
			 		
			 		if(!inputJSON.get(CommonConstants.PHONE2).toString().isEmpty()){
			 			contactDO.setPhone3(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.PHONE2).toString() : ""));
			 		}
			 		
			 		if(!inputJSON.get(CommonConstants.PHONE3).toString().isEmpty()){
			 			contactDO.setPhone4(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.PHONE3).toString() : ""));
			 		}*/
			 		
			 		if(!inputJSON.get(CommonConstants.FAX).toString().isEmpty()){
			 			contactDO.setFax(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.FAX).toString() : ""));
			 		} else {
			 			contactDO.setFax(null);
			 		}
			 		contactDO.setWebsite((!inputJSON.get(CommonConstants.WEBSITE).toString().isEmpty() ? inputJSON.get(CommonConstants.WEBSITE).toString() : ""));
			 		contactDO.setRequirements((!inputJSON.get(CommonConstants.REQUIREMENT).toString().isEmpty() ? inputJSON.get(CommonConstants.REQUIREMENT).toString() : ""));
			 		contactDO.setNotes((!inputJSON.get(CommonConstants.NOTE).toString().isEmpty() ? inputJSON.get(CommonConstants.NOTE).toString() : ""));
			 		contactDO.setComments((!inputJSON.get(CommonConstants.COMMENT).toString().isEmpty() ? inputJSON.get(CommonConstants.COMMENT).toString() : ""));
			 		contactDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			contactDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		contactService.update(contactDO);
			 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Contact Updated");
			 	}

			 }else{
					return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveByLeadId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByLeadId(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<ContactDO> contactList = contactService.retriveByLeadId(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = ContactUtil.getContactList(contactList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveByMoMLeadId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByMoMLeadId(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<ContactDO> contactList = contactService.retriveByLeadId(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = ContactUtil.getMoMContactList(contactList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/importContact", method = RequestMethod.POST)
	public @ResponseBody String importEmp(Model model, HttpServletRequest request) {
			
		try {
			//if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
				Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY); 
				org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
				JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
				List<ContactDO> contactDOlist = new ArrayList<ContactDO>();
				
				for (int i=0; i < fileData.size(); i++){
					ContactDO contactDO = new ContactDO();
					JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
			 		
					if(colName.get(CommonConstants.LEADTYPE) != null){
						if(!rowJSON.get(colName.get(CommonConstants.LEADTYPE)).toString().isEmpty() && rowJSON.get(colName.get(CommonConstants.LEADTYPE)) != null){
							List<LeadDO> leadList = leadService.retrieveByLeadName(rowJSON.get(colName.get(CommonConstants.LEADTYPE)).toString());
				 			if(leadList != null && leadList.size() > 0){
				 				contactDO.setLead(leadList.get(0));
				 			}
				 			//contactDO.setLead(Long.parseLong(rowJSON.get(colName.get(CommonConstants.LEADTYPE)).toString()));
				 		}
					}
					if(colName.get(CommonConstants.SALUTATION) != null){
				 		if(!rowJSON.get(colName.get(CommonConstants.SALUTATION)).toString().isEmpty()){
				 			List<SalutationDO> salutationList = salutationService.retrieveByName(rowJSON.get(colName.get(CommonConstants.SALUTATION)).toString());
				 			if(salutationList != null && salutationList.size() > 0){
				 				contactDO.setSalutation(salutationList.get(0));
				 			}
				 		}
					}
			 		if(colName.get(CommonConstants.FIRSTNAME) != null){
				 		contactDO.setFirstname(!rowJSON.get(colName.get(CommonConstants.FIRSTNAME)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.FIRSTNAME)).toString() : "");
			 		}
					if(colName.get(CommonConstants.LASTNAME) != null){
				 		contactDO.setLastname(!rowJSON.get(colName.get(CommonConstants.LASTNAME)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.LASTNAME)).toString() : "");
					}
					if(colName.get(CommonConstants.MIDDLENAME) != null){
						contactDO.setMiddlename(!rowJSON.get(colName.get(CommonConstants.MIDDLENAME)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.MIDDLENAME)).toString() : "");
					}
					if(colName.get(CommonConstants.DESIGNATION) != null){
						contactDO.setDesignation((!rowJSON.get(colName.get(CommonConstants.DESIGNATION)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.DESIGNATION)).toString() : ""));
					}
					if(colName.get(CommonConstants.CONTACTTYPE) != null){
						if(!rowJSON.get(colName.get(CommonConstants.CONTACTTYPE)).toString().isEmpty()){
							List<ContactTypeDO> contactTypeList = contactTypeService.retrieveByName(rowJSON.get(colName.get(CommonConstants.CONTACTTYPE)).toString());
				 			if(contactTypeList != null && contactTypeList.size() > 0){
				 				contactDO.setContactType(contactTypeList.get(0));
				 			}
				 		}
					}
					if(colName.get(CommonConstants.PRIMARYEMAIL) != null){
				 		contactDO.setPrimaryemail((!rowJSON.get(colName.get(CommonConstants.PRIMARYEMAIL)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.PRIMARYEMAIL)).toString() : ""));
					}
					/*if(colName.get(CommonConstants.SECONDARYEMAIL) != null){
				 		contactDO.setSecondaryemail((!rowJSON.get(colName.get(CommonConstants.SECONDARYEMAIL)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.SECONDARYEMAIL)).toString() : ""));
					}*/	
					if(colName.get(CommonConstants.ADDRESS) != null){
						contactDO.setAddress1(!rowJSON.get(colName.get(CommonConstants.ADDRESS)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.ADDRESS)).toString() : "");
					}
					/*if(colName.get(CommonConstants.ADDRESS2) != null){
						contactDO.setAddress2(!rowJSON.get(colName.get(CommonConstants.ADDRESS2)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.ADDRESS2)).toString() : "");
					}*/
					if(colName.get(CommonConstants.MOBILE) != null){
						if(!rowJSON.get(colName.get(CommonConstants.MOBILE)).toString().isEmpty()){
				 			contactDO.setMobile1(rowJSON != null ? rowJSON.get(colName.get(CommonConstants.MOBILE)).toString() : "");
				 		}
					}
					/*if(colName.get(CommonConstants.MOBILE2) != null){
				 		if(!rowJSON.get(colName.get(CommonConstants.MOBILE2)).toString().isEmpty()){
				 			contactDO.setMobile2(Long.parseLong(rowJSON != null ? rowJSON.get(colName.get(CommonConstants.MOBILE2)).toString() : ""));
				 		}
					}*/
				 		
					if(colName.get(CommonConstants.PHONE) != null){
				 		if(!rowJSON.get(colName.get(CommonConstants.PHONE)).toString().isEmpty()){
				 			contactDO.setPhone1(Long.parseLong(rowJSON != null ? rowJSON.get(colName.get(CommonConstants.PHONE)).toString() : ""));
				 		}
					}
					/*if(colName.get(CommonConstants.PHONE1) != null){
				 		if(!rowJSON.get(colName.get(CommonConstants.PHONE1)).toString().isEmpty()){
				 			contactDO.setPhone2(Long.parseLong(rowJSON != null ? rowJSON.get(colName.get(CommonConstants.PHONE1)).toString() : ""));
				 		}
					}
					if(colName.get(CommonConstants.PHONE2) != null){
				 		if(!rowJSON.get(colName.get(CommonConstants.PHONE2)).toString().isEmpty()){
				 			contactDO.setPhone3(Long.parseLong(rowJSON != null ? rowJSON.get(colName.get(CommonConstants.PHONE2)).toString() : ""));
				 		}
					}
					if(colName.get(CommonConstants.PHONE3) != null){
				 		if(!rowJSON.get(colName.get(CommonConstants.PHONE3)).toString().isEmpty()){
				 			contactDO.setPhone4(Long.parseLong(rowJSON != null ? rowJSON.get(colName.get(CommonConstants.PHONE3)).toString() : ""));
				 		}
					}*/
					if(colName.get(CommonConstants.FAX) != null){
				 		if(!rowJSON.get(colName.get(CommonConstants.FAX)).toString().isEmpty()){
				 			contactDO.setFax(Long.parseLong(rowJSON != null ? rowJSON.get(colName.get(CommonConstants.FAX)).toString() : ""));
				 		}
					}
					if(colName.get(CommonConstants.WEBSITE) != null){
				 		contactDO.setWebsite((!rowJSON.get(colName.get(CommonConstants.WEBSITE)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.WEBSITE)).toString() : ""));
					}
					if(colName.get(CommonConstants.REQUIREMENT) != null){
				 		contactDO.setRequirements((!rowJSON.get(colName.get(CommonConstants.REQUIREMENT)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.REQUIREMENT)).toString() : ""));
					}
					if(colName.get(CommonConstants.NOTE) != null){
				 		contactDO.setNotes((!rowJSON.get(colName.get(CommonConstants.NOTE)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.NOTE)).toString() : ""));
					}
					if(colName.get(CommonConstants.COMMENT) != null){
				 		contactDO.setComments((!rowJSON.get(colName.get(CommonConstants.COMMENT)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.COMMENT)).toString() : ""));
					}
			 		contactDO.setUpdatedon(new Date());
			 		contactDO.setCreatedon(new Date());
			 		if(updatedBy != null && ! updatedBy.toString().isEmpty()){
			 			contactDO.setUpdatedby(updatedBy.toString());
			 			contactDO.setCreatedby(updatedBy.toString());
			 			contactDO.setEmpId(updatedBy.toString());
			 		}
			 		contactDO.setIsDeleted("n");
					contactDOlist.add(contactDO);
				}
				
				contactService.persistList(contactDOlist);
				
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public @ResponseBody String delete(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				List<ContactDO> contactDOList = new ArrayList<ContactDO>();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && inputJSON.get(CommonConstants.IDS) != null){
			 		JSONArray resultJSONArray = new JSONArray();
			 		resultJSONArray.put(inputJSON.get(CommonConstants.IDS));
			 		String rec = (String) resultJSONArray.get(0).toString();
			 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
			 		String updatedBy = "";
		 			if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
		 				updatedBy = inputJSON.get(CommonConstants.UPDATED_BY).toString();
			 		}
			 		for (int i = 0; i < resultJSONArray1.size(); i++) {
			 			JSONObject inputJSON1 = (JSONObject) resultJSONArray1.get(i);
			 			boolean flag = true;
			 			if(!inputJSON1.get(CommonConstants.ID).toString().isEmpty()){
			 				
			 				List<OpportunitiesDO> opportunitiesList = opportunitiesService.retrieveByContactId(Long.parseLong(inputJSON1.get(CommonConstants.ID).toString()));
			 				if(opportunitiesList != null && opportunitiesList.size() > 0){
			 					flag = false;
			 				}
			 				
			 				
			 				if(resultJSONArray1.size() == 1){
			 					if(flag){
				 					List<ContactDO> contactList = contactService.retriveById(Long.parseLong(inputJSON1.get(CommonConstants.ID).toString()));
				 					if(contactList != null && contactList.size() > 0){
				 						contactList.get(0).setIsDeleted("y");
				 						contactList.get(0).setUpdatedby(updatedBy);
				 						contactDOList.add(contactList.get(0));
				 					}
				 				}else{
				 					return CommonWebUtil.buildErrorResponse("Cannot Delete").toString();
			 					}
			 				}else{
			 					if(flag){
			 						List<ContactDO> contactList = contactService.retriveById(Long.parseLong(inputJSON1.get(CommonConstants.ID).toString()));
				 					if(contactList != null && contactList.size() > 0){
				 						contactList.get(0).setIsDeleted("y");
				 						contactList.get(0).setUpdatedby(updatedBy);
				 						contactDOList.add(contactList.get(0));
				 					}
				 				}
			 				}
			 				
			 			}
			 		}
			 		contactService.update(contactDOList);
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
}
