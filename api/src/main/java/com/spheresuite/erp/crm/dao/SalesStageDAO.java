package com.spheresuite.erp.crm.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.SalesStageDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class SalesStageDAO {
	static Logger logger = Logger.getLogger(SalesStageDAO.class.getName());

	@Autowired
	private GenericDAOImpl<SalesStageDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(SalesStageDO salesStageDO) {
		boolean persistStatus = true;
		List<SalesStageDO> salesStageList = null;
		try {
			salesStageList = this.sessionFactory.getCurrentSession().getNamedQuery(SalesStageDO.FIND_BY_NAME)
			.setParameter(CommonConstants.NAME, salesStageDO.getName())
			.list();
			if(salesStageList != null && salesStageList.size() > 0){
				persistStatus = false;
			}else{
				genericObject.persist(salesStageDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	public List<SalesStageDO> retrieve() {
		List<SalesStageDO> salesStageList = null;
		try {
			salesStageList = genericObject.retrieve(SalesStageDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return salesStageList;
	}
	
	public List<SalesStageDO> retrieveActive() {
		List<SalesStageDO> salesStageList = null;
		try {
			salesStageList =  genericObject.retrieveActive(CommonConstants.ACTIVE, SalesStageDO.FIND_BY_STATUS, SalesStageDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return salesStageList;
	}
	
	@SuppressWarnings("unchecked")
	public List<SalesStageDO> retrieveById(Long id) {
		List<SalesStageDO> salesStageList = null;
		try {
			salesStageList =  this.sessionFactory.getCurrentSession().getNamedQuery(SalesStageDO.FIND_BY_ID)
								.setParameter(CommonConstants.ID, id).list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return salesStageList;
	}
	
	@SuppressWarnings("unchecked")
	public boolean update(SalesStageDO salesStageDO) {
		boolean updateStatus = true;
		try {
			List<SalesStageDO> salesStageList = null;
			salesStageList = this.sessionFactory.getCurrentSession().getNamedQuery(SalesStageDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, salesStageDO.getName())
					.setParameter(CommonConstants.ID, salesStageDO.getSalesStageId())
					.list();
			if(salesStageList != null && salesStageList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(salesStageDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}
	
	public boolean persistList(List<SalesStageDO> salesStageList){
		try {
			for (SalesStageDO salesStageDO : salesStageList) {
				genericObject.persist(salesStageDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}

}
