package com.spheresuite.erp.crm.web.util;

import java.util.List;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.crm.service.LeadService;
import com.spheresuite.erp.domainobject.LogActivityDO;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
@Component
public class LogActivityUtil {
	
	private LogActivityUtil() {}
	@Autowired
	private EmployeeService temployeeService;
	
	private static EmployeeService employeeService;
	
	@Autowired
	private LeadService tleadService;
	private static LeadService leadService;
	
	@PostConstruct
	public void init() {
		employeeService = temployeeService;
		leadService=tleadService;
	}
	
	
	
	public static JSONObject getLogList(List<LogActivityDO> logList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (LogActivityDO log : logList) {
				resultJSONArray.put(getLogDetailObject(log));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getLogDetailObject(LogActivityDO log)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(log.getLogActivityId() != null ? log.getLogActivityId() : ""));
		result.put(CommonConstants.STATUS, String.valueOf(log.getStatus() != null ? log.getStatus() : ""));
		result.put(CommonConstants.TYPE, String.valueOf(log.getType() != null ? log.getType() : ""));
		result.put(CommonConstants.TIME, String.valueOf(log.getLogTime() != null ? log.getLogTime() : ""));
		result.put(CommonConstants.DATE, String.valueOf(log.getLogDate()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(log.getUpdatedon())));
		/*if(log.getUpdatedBy() != null){
			List<UserDO> userList = userService.retriveById(Long.parseLong(log.getUpdatedBy()));
			if(userList != null && userList.size() > 0){
				result.put(CommonConstants.UPDATED_BY, String.valueOf(userList.get(0).getName())); 
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
			
			List<EmployeeDO> empList = employeeService.retriveByEmpId(Long.parseLong(log.getUpdatedBy()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
			
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}*/
		if(log.getUpdatedBy() != null){
			String empName = CommonUtil.getEmpObject(log.getUpdatedBy());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.LEAD_ID, String.valueOf(log.getLead().getLeadId() != null ? log.getLead().getLeadId() : ""));
		result.put(CommonConstants.BODY, String.valueOf(log.getLogDetails() != null ? log.getLogDetails() : ""));
		
		if(log.getLead() != null){
			//List<LeadDO> leads = leadService.retrieveById(log.getLead().getLeadId());
			//if(leads !=null && leads.size() > 0){
			result.put(CommonConstants.LEAD_NAME, String.valueOf(log.getLead().getName()));
		}else{
			result.put(CommonConstants.LEAD_NAME,"");
		}
		return result;
	}
}
