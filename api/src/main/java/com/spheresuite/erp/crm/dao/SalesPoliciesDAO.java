package com.spheresuite.erp.crm.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.SalesPoliciesDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class SalesPoliciesDAO {
	static Logger logger = Logger.getLogger(SalesPoliciesDAO.class.getName());
	
	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private GenericDAOImpl<SalesPoliciesDO> genericObject;
	
	public SalesPoliciesDO persist(SalesPoliciesDO salesPoliciesList) {
		try {
			genericObject.persist(salesPoliciesList);
		} catch (Exception eException) {
			//throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			//SessionManager.closeEntityManager(em);
		}
		return salesPoliciesList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<SalesPoliciesDO> retrieve(List<String> id) {
		List<SalesPoliciesDO> salesPoliciesList = null;
		try {
			salesPoliciesList = this.sessionFactory.getCurrentSession().getNamedQuery(SalesPoliciesDO.FIND_ALL)
										.setParameterList(CommonConstants.ID, id)
										.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return salesPoliciesList;
	}
	
	public List<SalesPoliciesDO> retrieveAll() {
		List<SalesPoliciesDO> salesPoliciesList = null;
		try {
			salesPoliciesList = genericObject.retrieve(SalesPoliciesDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return salesPoliciesList;
	}
	
	@SuppressWarnings("unchecked")
	public List<SalesPoliciesDO> retrieveById(Long Id) {
		List<SalesPoliciesDO> salesPoliciesList = null;
		try {
			salesPoliciesList =  this.sessionFactory.getCurrentSession().getNamedQuery(SalesPoliciesDO.FIND_BY_ID)
									.setParameter(CommonConstants.ID, Id).list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return salesPoliciesList;
	}
	
	public SalesPoliciesDO update(SalesPoliciesDO salesPoliciesDO) {
		try {
			genericObject.merge(salesPoliciesDO);
		} catch (Exception eException) {
		} finally {
		}
		return salesPoliciesDO;
	}

}
