package com.spheresuite.erp.crm.web.util;

import java.util.List;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.SalesTargetDO;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
@Component
public class SalesTargetUtil {
	
	private SalesTargetUtil() {}
	
	@Autowired
	private EmployeeService employeeServiceTemp;
	
	private static EmployeeService employeeService;
	
	@PostConstruct
	public void init() {
		employeeService = this.employeeServiceTemp;
	}
	
	public static JSONObject getSalesTargetList(List<SalesTargetDO> salesTargetList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (SalesTargetDO salesTarget : salesTargetList) {
				resultJSONArray.put(getSalesTargetDetailObject(salesTarget));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getSalesTargetDetailObject(SalesTargetDO salesTarget)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(salesTarget.getSalesTargetId()));
		if(salesTarget.getEmployee() != null){
			result.put(CommonConstants.EMPID, String.valueOf(salesTarget.getEmployee().getEmpId()));
			String empName = CommonUtil.getEmpObject(salesTarget.getEmployee().getEmpId());
			result.put(CommonConstants.NAME, empName);
		}else{
			result.put(CommonConstants.EMPID, "");
			result.put(CommonConstants.NAME, "");
		}
		result.put(CommonConstants.FROMDATE, String.valueOf(salesTarget.getFromDate()));
		result.put(CommonConstants.TODATE, String.valueOf(salesTarget.getToDate()));
		result.put(CommonConstants.Q1, String.valueOf(salesTarget.getQ1() != null ? salesTarget.getQ1(): "0"));
		result.put(CommonConstants.Q2, String.valueOf(salesTarget.getQ2() != null ? salesTarget.getQ2(): "0"));
		result.put(CommonConstants.Q3, String.valueOf(salesTarget.getQ3() != null ? salesTarget.getQ3(): "0"));
		result.put(CommonConstants.Q4, String.valueOf(salesTarget.getQ4() != null ? salesTarget.getQ4(): "0"));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(salesTarget.getUpdatedon())));
		
		if(salesTarget.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(salesTarget.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		return result;
	}
}
