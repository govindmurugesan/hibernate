package com.spheresuite.erp.crm.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.LeadStatusDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class LeadStatusDAO {
	static Logger logger = Logger.getLogger(LeadStatusDAO.class.getName());
	@Autowired
	private GenericDAOImpl<LeadStatusDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(LeadStatusDO leadStatusDO)  {
		boolean persistStatus = true;
		List<LeadStatusDO> leadStatusList = null;
		try {
			leadStatusList = this.sessionFactory.getCurrentSession().getNamedQuery(LeadStatusDO.FIND_BY_NAME)
			.setParameter(CommonConstants.NAME, leadStatusDO.getName())
			.list();
			if(leadStatusList != null && leadStatusList.size() > 0){
				persistStatus = false;
			}else{
				genericObject.persist(leadStatusDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	public List<LeadStatusDO> retrieve()  {
		List<LeadStatusDO> leadStatusList = null;
		try {
			leadStatusList = genericObject.retrieve(LeadStatusDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return leadStatusList;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeadStatusDO> retrieveActive()  {
		List<LeadStatusDO> leadStatusList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeadStatusDO.FIND_BY_STATUS)
			.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return leadStatusList;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeadStatusDO> retrieveById(Long Id)  {
		List<LeadStatusDO> leadStatusList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeadStatusDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, Id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return leadStatusList;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeadStatusDO> retrieveByActiveName(String name)  {
		List<LeadStatusDO> leadStatusList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(LeadStatusDO.FIND_BY_ACTIVE_NAME)
					.setParameter(CommonConstants.NAME, name)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return leadStatusList;
	}
	
	@SuppressWarnings("unchecked")
	public boolean update(LeadStatusDO leadStatusDO)  {
		boolean updateStatus = true;
		try {
			List<LeadStatusDO> projectTypeList = null;
			projectTypeList = this.sessionFactory.getCurrentSession().getNamedQuery(LeadStatusDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, leadStatusDO.getName())
					.setParameter(CommonConstants.ID, leadStatusDO.getLeadStatusId())
					.list();
			if(projectTypeList != null && projectTypeList.size() > 0){
				updateStatus = false;
			}else{
				genericObject.merge(leadStatusDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}
	
	public boolean persistList(List<LeadStatusDO> leadStatusDOList){
		try {
			for (LeadStatusDO leadStatusDO : leadStatusDOList) {
				genericObject.persist(leadStatusDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}

}
