package com.spheresuite.erp.crm.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.crm.service.LeadTypeService;
import com.spheresuite.erp.crm.web.util.LeadTypeUtil;
import com.spheresuite.erp.domainobject.LeadTypeDO;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/leadtype")
public class LeadTypeRS {

	String validation = null;
	static Logger logger = Logger.getLogger(LeadTypeRS.class.getName());

	@Autowired
	private LeadTypeService leadTypeService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				LeadTypeDO leadTypeDO = new LeadTypeDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
			 			leadTypeDO.setName(inputJSON.get(CommonConstants.NAME).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			leadTypeDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			leadTypeDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		leadTypeDO.setUpdatedon(new Date());
			 		leadTypeDO.setCreatedon(new Date());
			 		leadTypeDO.setStatus(CommonConstants.ACTIVE);
			 	}
				if(!leadTypeService.persist(leadTypeDO)){
					return CommonWebUtil.buildErrorResponse("Lead Type Already Added").toString();
				}
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Leave Type Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<LeadTypeDO> leadStatusList = leadTypeService.retrieveActive();
				respJSON = LeadTypeUtil.getLeadTypeList(leadStatusList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<LeadTypeDO> leadStatusList = leadTypeService.retrieve();
				respJSON = LeadTypeUtil.getLeadTypeList(leadStatusList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				LeadTypeDO leadTypeDO = new LeadTypeDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<LeadTypeDO> leadList = leadTypeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(leadList != null && leadList.size() > 0){
				 		leadTypeDO = leadList.get(0);
				 		if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
				 			leadTypeDO.setName(inputJSON.get(CommonConstants.NAME).toString());
				 		}
				 		leadTypeDO.setUpdatedon(new Date());
				 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			leadTypeDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
				 			leadTypeDO.setStatus((char) inputJSON.get(CommonConstants.STATUS).toString().charAt(0));
				 		}
				 		if(!leadTypeService.update(leadTypeDO)){
				 			return CommonWebUtil.buildErrorResponse("Lead Type Already Added").toString();
				 		}
				 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Leave Type Updated");
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/importLeadType", method = RequestMethod.POST)
	public @ResponseBody String importLeadType(Model model, HttpServletRequest request) {
		try {
			JSONObject inputJSON =  CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
			JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
			List<LeadTypeDO> leadList = new ArrayList<LeadTypeDO>();
			Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY);
			for (int i=0; i < fileData.size(); i++){
				JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
				LeadTypeDO leadTypeDO = new LeadTypeDO();
				
				if (colName.get(CommonConstants.NAME) != null && !colName.get(CommonConstants.NAME).toString().isEmpty()){
					leadTypeDO.setName(rowJSON.get(colName.get(CommonConstants.NAME)).toString());
		 		}
		 		if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
		 			leadTypeDO.setUpdatedby(updatedBy.toString());
		 			leadTypeDO.setCreatedby(updatedBy.toString());
		 		}
		 		leadTypeDO.setUpdatedon(new Date());
		 		leadTypeDO.setCreatedon(new Date());
		 		leadTypeDO.setStatus(CommonConstants.ACTIVE);
		 		leadList.add(leadTypeDO);
			}
			leadTypeService.persistList(leadList);
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
