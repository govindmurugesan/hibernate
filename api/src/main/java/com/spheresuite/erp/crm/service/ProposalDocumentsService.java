package com.spheresuite.erp.crm.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.crm.dao.ProposalDocumentsDAO;
import com.spheresuite.erp.domainobject.ProposalDocumentsDO;

@Service
@Transactional
public class ProposalDocumentsService {
	static Logger logger = Logger.getLogger(ProposalDocumentsService.class.getName());
	
	@Autowired
	private ProposalDocumentsDAO proposalDocumentsDAO;
	
	@Transactional
	public List<ProposalDocumentsDO> persistList(List<ProposalDocumentsDO> proposalDocumentsList) {
		return proposalDocumentsDAO.persistList(proposalDocumentsList);
	}
	
	@Transactional
	public ProposalDocumentsDO persist(ProposalDocumentsDO proposalDocumentsList) {
		return proposalDocumentsDAO.persist(proposalDocumentsList);
	}

	@Transactional
	public List<ProposalDocumentsDO> retrieveById(Long proposalId) {
		return proposalDocumentsDAO.retrieveById(proposalId);
	}
	
	@Transactional
	public boolean delete(Long id) {
		return proposalDocumentsDAO.delete(id);
	}
}
