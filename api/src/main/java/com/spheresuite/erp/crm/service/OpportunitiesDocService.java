package com.spheresuite.erp.crm.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.crm.dao.OpportunitiesDocDAO;
import com.spheresuite.erp.domainobject.OpportunitiesDocDO;
@Service
@Transactional
public class OpportunitiesDocService {
	static Logger logger = Logger.getLogger(OpportunitiesDocService.class.getName());
	@Autowired
	private OpportunitiesDocDAO opportunitiesDocDAO;

	@Transactional
	public OpportunitiesDocDO persist(OpportunitiesDocDO opportunitiesDocDO) {
		return opportunitiesDocDAO.persist(opportunitiesDocDO);
	}

	@Transactional
	public List<OpportunitiesDocDO> retrieveByOppId(Long id) {
		return opportunitiesDocDAO.retrieveByOppId(id);
	}

	@Transactional
	public boolean delete(Long id) {
		return opportunitiesDocDAO.delete(id);
	}

	@Transactional
	public List<OpportunitiesDocDO> retrieve() {
		return opportunitiesDocDAO.retrieve();
	}

	@Transactional
	public OpportunitiesDocDO update(OpportunitiesDocDO opportunitiesDocDO) {
		return opportunitiesDocDAO.update(opportunitiesDocDO);
	}
}
