package com.spheresuite.erp.crm.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.crm.dao.ProposalDAO;
import com.spheresuite.erp.domainobject.ProposalDO;

@Service
@Transactional
public class ProposalService {
	static Logger logger = Logger.getLogger(ProposalService.class.getName());
	
	@Autowired
	private ProposalDAO proposalDAO;
	
	@Transactional
	public ProposalDO persist(ProposalDO proposalDO) {
		return proposalDAO.persist(proposalDO);
	}

	@Transactional
	public List<ProposalDO> retriveById(Long id) {
		return proposalDAO.retrieveById(id);
	}
	
	@Transactional
	public List<ProposalDO> retrieve(List<String> id) {
		return proposalDAO.retrieve(id);
	}
	
	@Transactional
	public List<ProposalDO> retrieveCustomerId(Long id) {
		return proposalDAO.retrieveCustomerId(id);
	}
	
	@Transactional
	public List<ProposalDO> retrieveAll() {
		return proposalDAO.retrieveAll();
	}
	
	@Transactional
	public ProposalDO update(ProposalDO proposalDO) {
		return proposalDAO.update(proposalDO);
	}
}
