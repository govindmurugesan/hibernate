package com.spheresuite.erp.crm.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.crm.dao.EventsDAO;
import com.spheresuite.erp.domainobject.EventsDO;
@Service
@Transactional
public class EventsService {
	static Logger logger = Logger.getLogger(EventsService.class.getName());
	@Autowired
	private EventsDAO eventsDAO;

	@Transactional
	public EventsDO persist(EventsDO event) {
		return eventsDAO.persist(event);
	}

	@Transactional
	public List<EventsDO> retrieveById(Long eventId) {
		return eventsDAO.retrieveById(eventId);
	}
	
	@Transactional
	public List<EventsDO> retrieve() {
		return eventsDAO.retrieve();
	}
	
	@Transactional
	public EventsDO update(EventsDO event) {
		return eventsDAO.update(event);
	}

}
