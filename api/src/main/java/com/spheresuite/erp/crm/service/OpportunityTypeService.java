package com.spheresuite.erp.crm.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.crm.dao.OpportunityTypeDAO;
import com.spheresuite.erp.domainobject.OpportunityTypeDO;

@Service
@Transactional
public class OpportunityTypeService {
	static Logger logger = Logger.getLogger(OpportunityTypeService.class.getName());
	
	@Autowired
	private OpportunityTypeDAO opportunityTypeDAO;
	
	@Transactional
	public boolean persist(OpportunityTypeDO opportunityTypeDO) {
		return opportunityTypeDAO.persist(opportunityTypeDO);
	}

	@Transactional
	public List<OpportunityTypeDO> retrieveActive() {
		return opportunityTypeDAO.retrieveActive();
	}
	
	@Transactional
	public List<OpportunityTypeDO> retrieveById(Long Id) {
		return opportunityTypeDAO.retrieveById(Id);
	}
	
	@Transactional
	public List<OpportunityTypeDO> retrieve() {
		return opportunityTypeDAO.retrieve();
	}
	
	@Transactional
	public boolean update(OpportunityTypeDO opportunityTypeDO) {
		return opportunityTypeDAO.update(opportunityTypeDO);
	}
	
	@Transactional
	public boolean persistList(List<OpportunityTypeDO> opportunityTypeDO) {
		return opportunityTypeDAO.persistList(opportunityTypeDO);
	}
}
