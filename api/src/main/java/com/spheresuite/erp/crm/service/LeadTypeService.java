package com.spheresuite.erp.crm.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.crm.dao.LeadTypeDAO;
import com.spheresuite.erp.domainobject.LeadTypeDO;
@Service
@Transactional
public class LeadTypeService {
	static Logger logger = Logger.getLogger(LeadTypeService.class.getName());
	@Autowired
	private LeadTypeDAO leadTypeDAO;
	
	@Transactional
	public boolean persist(LeadTypeDO leadTypeDO) {
		return leadTypeDAO.persist(leadTypeDO);
	}

	@Transactional
	public List<LeadTypeDO> retrieveActive() {
		return leadTypeDAO.retrieveActive();
	}

	@Transactional
	public List<LeadTypeDO> retrieveById(Long Id) {
		return leadTypeDAO.retrieveById(Id);
	}
	
	@Transactional
	public List<LeadTypeDO> retrieveByActvieName(String name) {
		return leadTypeDAO.retrieveByActiveName(name);
	}

	@Transactional
	public List<LeadTypeDO> retrieve() {
		return leadTypeDAO.retrieve();
	}

	@Transactional
	public boolean update(LeadTypeDO leadTypeDO) {
		return leadTypeDAO.update(leadTypeDO);
	}
	
	@Transactional
	public boolean persistList(List<LeadTypeDO> leadTypeDO) {
		return leadTypeDAO.persistList(leadTypeDO);
	}
}
