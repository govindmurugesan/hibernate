package com.spheresuite.erp.crm.rs;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.crm.service.EventsService;
import com.spheresuite.erp.crm.service.MinutesOfMeetingNotesService;
import com.spheresuite.erp.crm.web.util.EventsUtil;
import com.spheresuite.erp.domainobject.EventsDO;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/events")
public class EventsRS {

	String validation = null;
	static Logger logger = Logger.getLogger(EventsRS.class.getName());

	@Autowired
	private EventsService eventsService;
	
	@Autowired
	private MinutesOfMeetingNotesService momNotesService;
	
	@Autowired
	private EmployeeService employeeService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EventsDO eventsDO = new EventsDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		eventsDO.setName(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty() ? inputJSON.get(CommonConstants.NAME).toString().toUpperCase() : "");
			 		eventsDO.setCountry(inputJSON.get(CommonConstants.COUNTRY) != null && !inputJSON.get(CommonConstants.COUNTRY).toString().isEmpty() ? inputJSON.get(CommonConstants.COUNTRY).toString().toUpperCase() : "");
			 		eventsDO.setAddress(inputJSON.get(CommonConstants.ADDRESS) != null && !inputJSON.get(CommonConstants.ADDRESS).toString().isEmpty() ? inputJSON.get(CommonConstants.ADDRESS).toString().toUpperCase() : "");
			 		if(inputJSON.get(CommonConstants.FROMDATE) != null && !inputJSON.get(CommonConstants.FROMDATE).toString().isEmpty()){
			 			eventsDO.setFromDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.FROMDATE).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.TODATE) != null && !inputJSON.get(CommonConstants.TODATE).toString().isEmpty()){
			 			eventsDO.setToDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.TODATE).toString()));
			 		}
			 		eventsDO.setUpdatedon(new Date());
			 		eventsDO.setCreatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			eventsDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			eventsDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			eventsDO.setEmpId(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		eventsDO = eventsService.persist(eventsDO);
			 	}
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Event Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retrieveById/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveById(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<EventsDO> momList = eventsService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = EventsUtil.getEventsList(momList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<EventsDO> momList = eventsService.retrieve();
				respJSON = EventsUtil.getEventsList(momList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EventsDO eventsDO = new EventsDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
				if (inputJSON != null){
					List<EventsDO> momList = eventsService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(momList != null && momList.size() > 0){
			 			eventsDO = momList.get(0);
			 			eventsDO.setName(!inputJSON.get(CommonConstants.NAME).toString().isEmpty()  && inputJSON.get(CommonConstants.NAME) != null ? inputJSON.get(CommonConstants.NAME).toString().toUpperCase() : "");
				 		eventsDO.setCountry(!inputJSON.get(CommonConstants.COUNTRY).toString().isEmpty()  && inputJSON.get(CommonConstants.COUNTRY) != null ? inputJSON.get(CommonConstants.COUNTRY).toString().toUpperCase() : "");
				 		eventsDO.setAddress(!inputJSON.get(CommonConstants.ADDRESS).toString().isEmpty()  && inputJSON.get(CommonConstants.ADDRESS) != null ? inputJSON.get(CommonConstants.ADDRESS).toString().toUpperCase() : "");
				 		if(!inputJSON.get(CommonConstants.FROMDATE).toString().isEmpty()  && inputJSON.get(CommonConstants.FROMDATE) != null ){
				 			eventsDO.setFromDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.FROMDATE).toString()));
				 		}
				 		if(!inputJSON.get(CommonConstants.TODATE).toString().isEmpty()  && inputJSON.get(CommonConstants.TODATE) != null ){
				 			eventsDO.setToDate(CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.TODATE).toString()));
				 		}
				 		eventsDO.setUpdatedon(new Date());
				 		eventsDO.setCreatedon(new Date());
				 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			eventsDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 			eventsDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 			eventsDO.setEmpId(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		eventsService.update(eventsDO);
			 		}
			 	}
		 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Event Updated");

			 }else{
					return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
