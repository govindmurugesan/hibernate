package com.spheresuite.erp.pl.web.util;

import java.util.List;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.OCNCostingDO;
import com.spheresuite.erp.domainobject.OCNDO;
import com.spheresuite.erp.domainobject.OCNDetailsDO;
import com.spheresuite.erp.pl.service.OCNCostingService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;

@Component
public class OCNDetailsUtil {
	
	private OCNDetailsUtil() {}
	
	@Autowired
	private OCNCostingService tocnCostingService;
	
	private static OCNCostingService ocnCostingService;
	
	
	@PostConstruct
	public void init() {
		ocnCostingService = tocnCostingService;
	}
	
	public static JSONObject getOCNDetailsList(List<OCNDetailsDO> ocnDetailsList){
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (OCNDetailsDO ocnDetailsDO : ocnDetailsList) {
				resultJSONArray.put(getOCNDetailObject(ocnDetailsDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getOCNFabricSumList(List<Object[]> ocnDetailsList){
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (Object[] ocnDetail : ocnDetailsList) {
				resultJSONArray.put(getOCNFabricDetailObject(ocnDetail));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getOCNFabricDetailObject(Object[] ocnDetailsDO)throws JSONException{
		JSONObject result = new JSONObject();
		OCNDO ocndo = (OCNDO) ocnDetailsDO[0];
		result.put(CommonConstants.OCN, String.valueOf(ocndo.getOcnId()));
		result.put(CommonConstants.REQUIREDMETER, String.valueOf(ocnDetailsDO[1]));
		result.put(CommonConstants.BUDGETCOST, String.valueOf(ocnDetailsDO[2]));
		result.put(CommonConstants.QUANTITY, String.valueOf(ocnDetailsDO[4]));
		result.put(CommonConstants.AMOUNT, String.valueOf(ocnDetailsDO[5]));
		return result;
	}
	
	public static JSONObject getOCNUniqueList(List<OCNDetailsDO> ocnDetailsList){
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (int i = 0; i < ocnDetailsList.size(); i++) {
				resultJSONArray.put(getOCNUnqiueDetailObject(String.valueOf(ocnDetailsList.get(i))));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getOCNUnqiueDetailObject(String ocnDetailsDO)throws JSONException{
		JSONObject result = new JSONObject();
		result.put(CommonConstants.OCN, String.valueOf(ocnDetailsDO));
		return result;
	}

	public static JSONObject getOCNDetailObject(OCNDetailsDO ocnDetailsDO)throws JSONException{
		
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(ocnDetailsDO.getOcnDetailsId()));
		if(ocnDetailsDO.getOcn() != null){
			result.put(CommonConstants.OCN, String.valueOf(ocnDetailsDO.getOcn().getOcnId()));
			result.put(CommonConstants.OCN_NUMBER, String.valueOf(ocnDetailsDO.getOcn().getOcn()));
		} else{
			result.put(CommonConstants.OCN, "");
			result.put(CommonConstants.OCN_NUMBER, "");
		}
		//result.put(CommonConstants.OCN, String.valueOf(ocnDetailsDO.getOcn()));
		result.put(CommonConstants.ARTICLE, String.valueOf(ocnDetailsDO.getArticle()));
		result.put(CommonConstants.QUANTITY, String.valueOf(ocnDetailsDO.getQty()));
		result.put(CommonConstants.AMOUNT, String.valueOf(ocnDetailsDO.getAmount()));
		result.put(CommonConstants.SIZE, String.valueOf(ocnDetailsDO.getSize()));
		
		List<OCNCostingDO> ocnCostingList = ocnCostingService.retrieveByOCNDetailsId(ocnDetailsDO.getOcnDetailsId());
		if(ocnCostingList.size() == 0){
			result.put(CommonConstants.BUDGETCON, "");
			result.put(CommonConstants.VALUELOSS, "");
			result.put(CommonConstants.GRCON, "");
			result.put(CommonConstants.CUTTINGLOSS, "");
			result.put(CommonConstants.MULOSS, "");
			result.put(CommonConstants.BUDGETCOST, "");
			result.put(CommonConstants.REQUIREDMETER, "");
			result.put(CommonConstants.FABRICTYPE, "");
			result.put(CommonConstants.FABRICTYPE_NAME, "");
			result.put(CommonConstants.OCNDETAILS_ID, "");
		}else{
			result.put(CommonConstants.BUDGETCON, ocnCostingList.get(0).getArticleCon()!=null?String.valueOf(ocnCostingList.get(0).getArticleCon()):"");
			result.put(CommonConstants.VALUELOSS, ocnCostingList.get(0).getValueLoss()!=null?String.valueOf(ocnCostingList.get(0).getValueLoss()):"");
			result.put(CommonConstants.GRCON, ocnCostingList.get(0).getGrCon()!=null?String.valueOf(ocnCostingList.get(0).getGrCon()):"");
			result.put(CommonConstants.CUTTINGLOSS, ocnCostingList.get(0).getCuttingLoss()!=null?String.valueOf(ocnCostingList.get(0).getCuttingLoss()):"");
			result.put(CommonConstants.MULOSS, ocnCostingList.get(0).getMuLoss()!=null?String.valueOf(ocnCostingList.get(0).getMuLoss()):"");
			result.put(CommonConstants.BUDGETCOST, ocnCostingList.get(0).getBudgetCost()!=null?String.valueOf(ocnCostingList.get(0).getBudgetCost()):"");
			result.put(CommonConstants.REQUIREDMETER, ocnCostingList.get(0).getReqm()!=null?String.valueOf(ocnCostingList.get(0).getReqm()):"");
			result.put(CommonConstants.FABRICTYPE, ocnCostingList.get(0).getFabricType()!=null?String.valueOf(ocnCostingList.get(0).getFabricType().getFabricTypeId()):"");
			result.put(CommonConstants.FABRICTYPE_NAME, ocnCostingList.get(0).getFabricType()!=null?String.valueOf(ocnCostingList.get(0).getFabricType().getName()):"");
			result.put(CommonConstants.OCNDETAILS_ID, ocnCostingList.get(0).getOcnDetails()!=null?String.valueOf(ocnCostingList.get(0).getOcnDetails().getOcnDetailsId()):"");
		}
		
		JSONArray resultJSONArray = new JSONArray();
		for(int i = 1; i < ocnCostingList.size(); i++){
			resultJSONArray.put(getOCNCostingDetailObject(ocnCostingList.get(i)));
		}
		result.put(CommonConstants.FABRICTYPELIST, resultJSONArray);
		result.put(CommonConstants.STATUS, String.valueOf(ocnDetailsDO.getStatus()));
		if(ocnDetailsDO.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(ocnDetailsDO.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		if(ocnDetailsDO.getUpdatedon() != null){
			result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(ocnDetailsDO.getUpdatedon())));
		}else{
			result.put(CommonConstants.UPDATEDON, "");
		}
		return result;
	}
	
	public static JSONObject getOCNCostingDetailObject(OCNCostingDO ocnCostingDO)throws JSONException{
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, ocnCostingDO.getOcnCostingId()!=null?String.valueOf(ocnCostingDO.getOcnCostingId()):"");
		result.put(CommonConstants.BUDGETCON, ocnCostingDO.getArticleCon()!=null?String.valueOf(ocnCostingDO.getArticleCon()):"");
		result.put(CommonConstants.VALUELOSS, ocnCostingDO.getValueLoss()!=null?String.valueOf(ocnCostingDO.getValueLoss()):"");
		result.put(CommonConstants.GRCON, ocnCostingDO.getGrCon()!=null?String.valueOf(ocnCostingDO.getGrCon()):"");
		result.put(CommonConstants.CUTTINGLOSS, ocnCostingDO.getCuttingLoss()!=null?String.valueOf(ocnCostingDO.getCuttingLoss()):"");
		result.put(CommonConstants.MULOSS, ocnCostingDO.getMuLoss()!=null?String.valueOf(ocnCostingDO.getMuLoss()):"");
		result.put(CommonConstants.BUDGETCOST, ocnCostingDO.getBudgetCost()!=null?String.valueOf(ocnCostingDO.getBudgetCost()):"");
		result.put(CommonConstants.REQUIREDMETER, ocnCostingDO.getReqm()!=null?String.valueOf(ocnCostingDO.getReqm()):"");
		result.put(CommonConstants.FABRICTYPE, ocnCostingDO.getFabricType()!=null?String.valueOf(ocnCostingDO.getFabricType().getFabricTypeId()):"");
		result.put(CommonConstants.FABRICTYPE_NAME, ocnCostingDO.getFabricType()!=null?String.valueOf(ocnCostingDO.getFabricType().getName()):"");
		result.put(CommonConstants.OCNDETAILS_ID, ocnCostingDO.getOcnDetails()!=null?String.valueOf(ocnCostingDO.getOcnDetails().getOcnDetailsId()):"");
		return result;
	}
}
