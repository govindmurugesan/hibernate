package com.spheresuite.erp.pl.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.OCNDetailsDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class OCNDetailsDAO {
	static Logger logger = Logger.getLogger(OCNDetailsDAO.class.getName());
	
	@Autowired
	private GenericDAOImpl<OCNDetailsDO> dao;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(OCNDetailsDO ocnDetailsDO){
		boolean persistStatus = true;
		try {
			List<OCNDetailsDO> ocnDetailsDOList = null;
			ocnDetailsDOList = this.sessionFactory.getCurrentSession().getNamedQuery(OCNDetailsDO.FIND_BY_OCN)
					.setParameter(CommonConstants.OCN, ocnDetailsDO.getOcn())
					.list();
					if(ocnDetailsDOList != null && ocnDetailsDOList.size() > 0){
						persistStatus =  false;
					}else{
						dao.persist(ocnDetailsDO);
					}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	public List<OCNDetailsDO> retrieve(){
		List<OCNDetailsDO> ocnDetailsList = null;
		try {
			ocnDetailsList = dao.retrieve(OCNDetailsDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return ocnDetailsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<OCNDetailsDO> retrieveUniqueOCN(){
		List<OCNDetailsDO> ocnDetailsList = null;
		try {
			ocnDetailsList = this.sessionFactory.getCurrentSession().getNamedQuery(OCNDetailsDO.FIND_BY_UNIQUE_OCN)
							.list();
		} catch (Exception eException) {
		} finally {
		}
		return ocnDetailsList;
	}
	
	public List<OCNDetailsDO> retrieveActive(){
		List<OCNDetailsDO> ocnDetailsList = null;
		try {
			ocnDetailsList =  dao.retrieveActive(CommonConstants.ACTIVE, OCNDetailsDO.FIND_BY_STATUS, OCNDetailsDO.class);
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return ocnDetailsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<OCNDetailsDO> retrieveOCN(Long ocn){
		List<OCNDetailsDO> ocnDetailsList = null;
		try {
			ocnDetailsList =  this.sessionFactory.getCurrentSession().getNamedQuery(OCNDetailsDO.FIND_BY_OCN)
					.setParameter(CommonConstants.OCN, ocn)
					.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return ocnDetailsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<OCNDetailsDO> retrieveById(Long id){
		 List<OCNDetailsDO> ocnDetailsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(OCNDetailsDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return ocnDetailsList;
	}
	
	public boolean update(OCNDetailsDO ocnDetailsDO) {
		boolean updateStatus = true;
		try {
			/*List<OCNDetailsDO> ocnDetailsDOList = null;
			ocnDetailsDOList = this.sessionFactory.getCurrentSession().getNamedQuery(OCNDetailsDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.OCN, ocnDetailsDO.getOcn())
					.setParameter(CommonConstants.ID, ocnDetailsDO.getOcnDetailsId())
					.list();
			if(ocnDetailsDOList != null && ocnDetailsDOList.size() > 0){
				updateStatus = false;
			}else{*/
				dao.merge(ocnDetailsDO);
			//}
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}
	
	public boolean persistList(List<OCNDetailsDO> ocnDetailsList){
		try {
			for (OCNDetailsDO ocnDetails : ocnDetailsList) {
				dao.persist(ocnDetails);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}

}
