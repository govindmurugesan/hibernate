package com.spheresuite.erp.pl.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.domainobject.OCNDO;
import com.spheresuite.erp.pl.dao.OCNDAO;

@Service
@Transactional
public class OCNService{
	
	@Autowired
	private OCNDAO ocnDAO;

	@Transactional
	public boolean persist(OCNDO ocnDO) {
		return ocnDAO.persist(ocnDO);
	}
	
	@Transactional
	public List<OCNDO> retrieve() {
		return ocnDAO.retrieve();
	}

	@Transactional
	public boolean update(OCNDO ocnDO) {
		return ocnDAO.update(ocnDO);
	}
	
	@Transactional
	public List<OCNDO> retrieveById(Long id){
		return ocnDAO.retrieveById(id);
	}
	
	@Transactional
	public boolean persistList(List<OCNDO> ocnDO) {
		return ocnDAO.persistList(ocnDO);
	}
}
