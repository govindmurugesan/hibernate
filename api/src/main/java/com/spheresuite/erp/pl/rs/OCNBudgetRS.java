package com.spheresuite.erp.pl.rs;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.CPMDO;
import com.spheresuite.erp.domainobject.OCNBudgetDO;
import com.spheresuite.erp.domainobject.OCNDO;
import com.spheresuite.erp.domainobject.OCNReportDO;
import com.spheresuite.erp.pl.service.CPMService;
import com.spheresuite.erp.pl.service.OCNBudgetService;
import com.spheresuite.erp.pl.service.OCNReportService;
import com.spheresuite.erp.pl.service.OCNService;
import com.spheresuite.erp.pl.web.util.OCNBudgetUtil;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/ocnbudget")
public class OCNBudgetRS {

	String validation = null;
	static Logger logger = Logger.getLogger(OCNBudgetRS.class.getName());
	
	@Autowired
	private OCNBudgetService ocnBudgetService;
	
	@Autowired
	private OCNService ocnDetailsService;
	
	@Autowired
	private OCNReportService ocnReportService;
	
	@Autowired
	private CPMService cpmService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				OCNBudgetDO ocnBudgetDO = new OCNBudgetDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.OCN) != null && !inputJSON.get(CommonConstants.OCN).toString().isEmpty()){
			 			List<OCNDO> ocnList = ocnDetailsService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.OCN).toString()));
			 			if(ocnList != null && ocnList.size() > 0){
			 				ocnBudgetDO.setOcnDetails(ocnList.get(0));
			 			}
			 		}
			 		if(inputJSON.get(CommonConstants.CPM) != null && !inputJSON.get(CommonConstants.CPM).toString().isEmpty()){
			 			List<CPMDO> cpmList = cpmService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.CPM).toString()));
			 			if(cpmList != null && cpmList.size() > 0){
			 				ocnBudgetDO.setCpm(cpmList.get(0));
			 			}
			 		}
			 		if(inputJSON.get(CommonConstants.GREIGE_FABRICM) != null && !inputJSON.get(CommonConstants.GREIGE_FABRICM).toString().isEmpty()){
			 			ocnBudgetDO.setGreigeFabricm(Double.parseDouble(inputJSON.get(CommonConstants.GREIGE_FABRICM).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.GREIGE_FABRIC) != null && !inputJSON.get(CommonConstants.GREIGE_FABRIC).toString().isEmpty()){
			 			ocnBudgetDO.setGreigeFabric(Double.parseDouble(inputJSON.get(CommonConstants.GREIGE_FABRIC).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.DYEING_COST) != null && !inputJSON.get(CommonConstants.DYEING_COST).toString().isEmpty()){
			 			ocnBudgetDO.setDyeingCost(Double.parseDouble(inputJSON.get(CommonConstants.DYEING_COST).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.FINISHING_COST) != null && !inputJSON.get(CommonConstants.FINISHING_COST).toString().isEmpty()){
			 			ocnBudgetDO.setFinishingCost(Double.parseDouble(inputJSON.get(CommonConstants.FINISHING_COST).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.TRIMS) != null && !inputJSON.get(CommonConstants.TRIMS).toString().isEmpty()){
			 			ocnBudgetDO.setTrims(Double.parseDouble(inputJSON.get(CommonConstants.TRIMS).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.SAM) != null && !inputJSON.get(CommonConstants.SAM).toString().isEmpty()){
			 			ocnBudgetDO.setSam(Double.parseDouble(inputJSON.get(CommonConstants.SAM).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.REJECTION) != null && !inputJSON.get(CommonConstants.REJECTION).toString().isEmpty()){
			 			ocnBudgetDO.setRejection(Double.parseDouble(inputJSON.get(CommonConstants.REJECTION).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.EXPORT_INCENTIVE) != null && !inputJSON.get(CommonConstants.EXPORT_INCENTIVE).toString().isEmpty()){
			 			ocnBudgetDO.setExportIncentive(inputJSON.get(CommonConstants.EXPORT_INCENTIVE).toString());
			 		}
			 		ocnBudgetDO.setUpdatedon(new Date());
			 		ocnBudgetDO.setCreatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			ocnBudgetDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			ocnBudgetDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		ocnBudgetDO.setStatus(CommonConstants.ACTIVESTRING);
			 	}
				if(!ocnBudgetService.persist(ocnBudgetDO)){
					return CommonWebUtil.buildErrorResponse("OCN Budget Already Added").toString();
				}
				CommonUtil.persistRecentAcvtivity(inputJSON.get(CommonConstants.UPDATED_BY).toString(), "New OCN Budget Created");
				if(ocnBudgetDO.getOcnBudgetId() != null){
					List<OCNReportDO> reportList =  ocnReportService.retrieveOCN(ocnBudgetDO.getOcnDetails().getOcnId());
					if(reportList != null && reportList.size() > 0){
						OCNReportDO ocnReportDO =  new OCNReportDO();
						ocnReportDO = reportList.get(0);
						ocnReportDO.setOcnDetails(ocnBudgetDO.getOcnDetails());
						ocnReportDO.setOcnBudget(ocnBudgetDO);
						Double totalAmt = 0D;
						if(ocnBudgetDO.getGreigeFabric() != null && ocnReportDO.getGreigeFabric() != null){
							ocnReportDO.setGreigeFabricQty(String.valueOf(ocnBudgetDO.getGreigeFabric()-ocnReportDO.getGreigeFabric()));
							if(ocnReportDO.getGreigeFabricm() != null){
								Double greigeAmt = (Double.valueOf(ocnReportDO.getGreigeFabricQty())* ocnReportDO.getGreigeFabricm())/Math.pow(10, 5);
								ocnReportDO.setGreigeFabricAmount(String.valueOf(greigeAmt));
								totalAmt += greigeAmt;
							}
						}
						if(ocnBudgetDO.getDyeingCost() != null && ocnReportDO.getDyeingCost() != null){
							ocnReportDO.setDyeingCostQty(String.valueOf(ocnBudgetDO.getDyeingCost()-ocnReportDO.getDyeingCost()));
							Double dyeingAmt = ((Double.valueOf(ocnBudgetDO.getGreigeFabric())* ocnBudgetDO.getDyeingCost())-(Double.valueOf(ocnReportDO.getGreigeFabric())* ocnReportDO.getDyeingCost()))/Math.pow(10, 5);
							ocnReportDO.setDyeingAmount(String.valueOf(dyeingAmt));
							totalAmt += dyeingAmt;
						}
						if(ocnBudgetDO.getFinishingCost() != null && ocnReportDO.getFinishingCost() != null){
							ocnReportDO.setFinishingCostQty(String.valueOf(ocnBudgetDO.getFinishingCost()-ocnReportDO.getFinishingCost()));
							Double finishingAmt = ((Double.valueOf(ocnBudgetDO.getGreigeFabric())* ocnBudgetDO.getFinishingCost())-(Double.valueOf(ocnReportDO.getGreigeFabric())* ocnReportDO.getFinishingCost()))/Math.pow(10, 5);
							ocnReportDO.setFinishingAmount(String.valueOf(finishingAmt));
							totalAmt += finishingAmt;
						}
						if(ocnBudgetDO.getSam() != null && ocnReportDO.getSam() != null){
							ocnReportDO.setSamQty(String.valueOf(ocnBudgetDO.getSam()-ocnReportDO.getSam()));
							Double samAmt = (Double.valueOf(ocnReportDO.getSamQty())*ocnBudgetDO.getGreigeFabric()*Double.valueOf(ocnBudgetDO.getCpm().getAmount()))/Math.pow(10, 5);
							ocnReportDO.setSamAmount(String.valueOf(samAmt));
							totalAmt += samAmt;
						}
						if(ocnBudgetDO.getTrims() != null && ocnReportDO.getTrims() != null){
							ocnReportDO.setTrimsQty(String.valueOf(ocnBudgetDO.getTrims()-ocnReportDO.getTrims()));
							if(ocnReportDO.getTrimsAmount() != null){
								totalAmt += Double.valueOf(ocnReportDO.getTrimsAmount());
							}
						}
						if(ocnBudgetDO.getRejection() != null && ocnReportDO.getRejection() != null){
							ocnReportDO.setRejectionQty(String.valueOf(ocnBudgetDO.getRejection()-ocnReportDO.getRejection()));
							Double rejectionAmt = (Double.valueOf(ocnReportDO.getRejectionQty())*ocnReportDO.getSam()*Double.valueOf(ocnBudgetDO.getCpm().getAmount()))/Math.pow(10, 5);
							ocnReportDO.setRejectionAmount(String.valueOf(rejectionAmt));
							totalAmt += rejectionAmt;
						}
						if(ocnBudgetDO.getExportIncentive() != null && ocnReportDO.getInvoiceAmount() != null){
							Double inv = (Double.valueOf(ocnReportDO.getInvoiceAmount())*(1+(Double.valueOf(ocnBudgetDO.getExportIncentive())/100)))/Math.pow(10, 5);
							DecimalFormat df = new DecimalFormat("#.00"); 
							String invTotal = df.format(inv);
							Double tot = ((Double.valueOf(totalAmt)*Math.pow(10, 5))/(Double.valueOf(invTotal)*Math.pow(10, 5)));
							ocnReportDO.setSalesPercentage(tot.toString());
						}
						if(totalAmt > 0){
							ocnReportDO.setAmount(totalAmt.toString());
							ocnReportDO.setStatus("p");
						}else{
							ocnReportDO.setAmount(totalAmt.toString());
							ocnReportDO.setStatus("n");
						}
						if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
							ocnReportDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
						ocnReportDO.setUpdatedon(new Date());
						ocnReportService.update(ocnReportDO);
					}
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<OCNBudgetDO> ocnBudgetList = ocnBudgetService.retrieveActive();
				respJSON = OCNBudgetUtil.getOCNBudgetList(ocnBudgetList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<OCNBudgetDO> ocnBudgetList = ocnBudgetService.retrieve();
				respJSON = OCNBudgetUtil.getOCNBudgetList(ocnBudgetList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveOCN/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveOCN(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && inputJSON.get(CommonConstants.OCN) != null && !inputJSON.get(CommonConstants.OCN).toString().isEmpty()){
					List<OCNBudgetDO> ocnBudgetList = ocnBudgetService.retrieveOCN(inputJSON.get(CommonConstants.OCN).toString());
					respJSON = OCNBudgetUtil.getOCNBudgetList(ocnBudgetList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				OCNBudgetDO ocnBudgetDO = new OCNBudgetDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<OCNBudgetDO> ocnBudgetList = ocnBudgetService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(ocnBudgetList != null && ocnBudgetList.size() > 0){
				 		ocnBudgetDO = ocnBudgetList.get(0);
				 		if(inputJSON.get(CommonConstants.OCN) != null && !inputJSON.get(CommonConstants.OCN).toString().isEmpty()){
				 			List<OCNDO> ocnList = ocnDetailsService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.OCN).toString()));
				 			if(ocnList != null && ocnList.size() > 0){
				 				ocnBudgetDO.setOcnDetails(ocnList.get(0));
				 			}
				 		}
				 		if(inputJSON.get(CommonConstants.CPM) != null && !inputJSON.get(CommonConstants.CPM).toString().isEmpty()){
				 			List<CPMDO> cpmList = cpmService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.CPM).toString()));
				 			if(cpmList != null && cpmList.size() > 0){
				 				ocnBudgetDO.setCpm(cpmList.get(0));
				 			}
				 		}
				 		if(inputJSON.get(CommonConstants.GREIGE_FABRICM) != null && !inputJSON.get(CommonConstants.GREIGE_FABRICM).toString().isEmpty()){
				 			ocnBudgetDO.setGreigeFabricm(Double.parseDouble(inputJSON.get(CommonConstants.GREIGE_FABRICM).toString()));
				 		}
				 		if(inputJSON.get(CommonConstants.GREIGE_FABRIC) != null && !inputJSON.get(CommonConstants.GREIGE_FABRIC).toString().isEmpty()){
				 			ocnBudgetDO.setGreigeFabric(Double.parseDouble(inputJSON.get(CommonConstants.GREIGE_FABRIC).toString()));
				 		}
				 		if(inputJSON.get(CommonConstants.DYEING_COST) != null && !inputJSON.get(CommonConstants.DYEING_COST).toString().isEmpty()){
				 			ocnBudgetDO.setDyeingCost(Double.parseDouble(inputJSON.get(CommonConstants.DYEING_COST).toString()));
				 		}
				 		if(inputJSON.get(CommonConstants.FINISHING_COST) != null && !inputJSON.get(CommonConstants.FINISHING_COST).toString().isEmpty()){
				 			ocnBudgetDO.setFinishingCost(Double.parseDouble(inputJSON.get(CommonConstants.FINISHING_COST).toString()));
				 		}
				 		if(inputJSON.get(CommonConstants.TRIMS) != null && !inputJSON.get(CommonConstants.TRIMS).toString().isEmpty()){
				 			ocnBudgetDO.setTrims(Double.parseDouble(inputJSON.get(CommonConstants.TRIMS).toString()));
				 		}
				 		if(inputJSON.get(CommonConstants.SAM) != null && !inputJSON.get(CommonConstants.SAM).toString().isEmpty()){
				 			ocnBudgetDO.setSam(Double.parseDouble(inputJSON.get(CommonConstants.SAM).toString()));
				 		}
				 		if(inputJSON.get(CommonConstants.REJECTION) != null && !inputJSON.get(CommonConstants.REJECTION).toString().isEmpty()){
				 			ocnBudgetDO.setRejection(Double.parseDouble(inputJSON.get(CommonConstants.REJECTION).toString()));
				 		}
				 		if(inputJSON.get(CommonConstants.EXPORT_INCENTIVE) != null && !inputJSON.get(CommonConstants.EXPORT_INCENTIVE).toString().isEmpty()){
				 			ocnBudgetDO.setExportIncentive(inputJSON.get(CommonConstants.EXPORT_INCENTIVE).toString());
				 		}
				 		ocnBudgetDO.setUpdatedon(new Date());
				 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			ocnBudgetDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
				 			ocnBudgetDO.setStatus(inputJSON.get(CommonConstants.STATUS).toString());
				 		}
				 		if(!ocnBudgetService.update(ocnBudgetDO)){
							return CommonWebUtil.buildErrorResponse("OCN Budget Already Added").toString();
						}
				 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New OCN Budget Updated");
				 		if(ocnBudgetDO.getOcnBudgetId() != null){
							List<OCNReportDO> reportList =  ocnReportService.retrieveOCN(ocnBudgetDO.getOcnDetails().getOcnId());
							if(reportList != null && reportList.size() > 0){
								OCNReportDO ocnReportDO =  new OCNReportDO();
								ocnReportDO = reportList.get(0);
								ocnReportDO.setOcnDetails(ocnBudgetDO.getOcnDetails());
								ocnReportDO.setOcnBudget(ocnBudgetDO);
								Double totalAmt = 0D;
								if(ocnBudgetDO.getGreigeFabric() != null && ocnReportDO.getGreigeFabric() != null){
									ocnReportDO.setGreigeFabricQty(String.valueOf(ocnBudgetDO.getGreigeFabric()-ocnReportDO.getGreigeFabric()));
									if(ocnReportDO.getGreigeFabricm() != null){
										Double greigeAmt = (Double.valueOf(ocnReportDO.getGreigeFabricQty())* ocnReportDO.getGreigeFabricm())/Math.pow(10, 5);
										ocnReportDO.setGreigeFabricAmount(String.valueOf(greigeAmt));
										totalAmt += greigeAmt;
									}
								}
								if(ocnBudgetDO.getDyeingCost() != null && ocnReportDO.getDyeingCost() != null){
									ocnReportDO.setDyeingCostQty(String.valueOf(ocnBudgetDO.getDyeingCost()-ocnReportDO.getDyeingCost()));
									Double dyeingAmt = ((Double.valueOf(ocnBudgetDO.getGreigeFabric())* ocnBudgetDO.getDyeingCost())-(Double.valueOf(ocnReportDO.getGreigeFabric())* ocnReportDO.getDyeingCost()))/Math.pow(10, 5);
									ocnReportDO.setDyeingAmount(String.valueOf(dyeingAmt));
									totalAmt += dyeingAmt;
								}
								if(ocnBudgetDO.getFinishingCost() != null && ocnReportDO.getFinishingCost() != null){
									ocnReportDO.setFinishingCostQty(String.valueOf(ocnBudgetDO.getFinishingCost()-ocnReportDO.getFinishingCost()));
									Double finishingAmt = ((Double.valueOf(ocnBudgetDO.getGreigeFabric())* ocnBudgetDO.getFinishingCost())-(Double.valueOf(ocnReportDO.getGreigeFabric())* ocnReportDO.getFinishingCost()))/Math.pow(10, 5);
									ocnReportDO.setFinishingAmount(String.valueOf(finishingAmt));
									totalAmt += finishingAmt;
								}
								if(ocnBudgetDO.getSam() != null && ocnReportDO.getSam() != null){
									ocnReportDO.setSamQty(String.valueOf(ocnBudgetDO.getSam()-ocnReportDO.getSam()));
									Double samAmt = (Double.valueOf(ocnReportDO.getSamQty())*ocnBudgetDO.getGreigeFabric()*Double.valueOf(ocnBudgetDO.getCpm().getAmount()))/Math.pow(10, 5);
									ocnReportDO.setSamAmount(String.valueOf(samAmt));
									totalAmt += samAmt;
								}
								if(ocnBudgetDO.getTrims() != null && ocnReportDO.getTrims() != null){
									ocnReportDO.setTrimsQty(String.valueOf(ocnBudgetDO.getTrims()-ocnReportDO.getTrims()));
									if(ocnReportDO.getTrimsAmount() != null){
										totalAmt += Double.valueOf(ocnReportDO.getTrimsAmount());
									}
								}
								if(ocnBudgetDO.getRejection() != null && ocnReportDO.getRejection() != null){
									ocnReportDO.setRejectionQty(String.valueOf(ocnBudgetDO.getRejection()-ocnReportDO.getRejection()));
									Double rejectionAmt = (Double.valueOf(ocnReportDO.getRejectionQty())*ocnReportDO.getSam()*Double.valueOf(ocnBudgetDO.getCpm().getAmount()))/Math.pow(10, 5);
									ocnReportDO.setRejectionAmount(String.valueOf(rejectionAmt));
									totalAmt += rejectionAmt;
								}
								if(ocnBudgetDO.getExportIncentive() != null && ocnReportDO.getInvoiceAmount() != null){
									Double inv = (Double.valueOf(ocnReportDO.getInvoiceAmount())*(1+(Double.valueOf(ocnBudgetDO.getExportIncentive())/100)))/Math.pow(10, 5);
									DecimalFormat df = new DecimalFormat("#.00"); 
									String invTotal = df.format(inv);
									Double tot = ((Double.valueOf(totalAmt)*Math.pow(10, 5))/(Double.valueOf(invTotal)*Math.pow(10, 5)));
									ocnReportDO.setSalesPercentage(tot.toString());
								}
								if(totalAmt > 0){
									ocnReportDO.setAmount(totalAmt.toString());
									ocnReportDO.setStatus("p");
								}else{
									ocnReportDO.setAmount(totalAmt.toString());
									ocnReportDO.setStatus("n");
								}
								if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
									ocnReportDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
						 		}
								ocnReportDO.setUpdatedon(new Date());
								ocnReportService.update(ocnReportDO);
							}
						}
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
}
