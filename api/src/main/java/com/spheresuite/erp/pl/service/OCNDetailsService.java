package com.spheresuite.erp.pl.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.domainobject.OCNDetailsDO;
import com.spheresuite.erp.pl.dao.OCNDetailsDAO;

@Service
@Transactional
public class OCNDetailsService{
	
	@Autowired
	private OCNDetailsDAO ocnDetailsDAO;

	@Transactional
	public boolean persist(OCNDetailsDO ocnDetailsDO) {
		return ocnDetailsDAO.persist(ocnDetailsDO);
	}
	
	@Transactional
	public List<OCNDetailsDO> retrieve() {
		return ocnDetailsDAO.retrieve();
	}
	
	@Transactional
	public List<OCNDetailsDO> retrieveUniqueOCN() {
		return ocnDetailsDAO.retrieveUniqueOCN();
	}

	@Transactional
	public boolean update(OCNDetailsDO ocnDetailsDO) {
		return ocnDetailsDAO.update(ocnDetailsDO);
	}
	
	@Transactional
	public List<OCNDetailsDO> retrieveActive(){
		return ocnDetailsDAO.retrieveActive();
	}
	
	@Transactional
	public List<OCNDetailsDO> retrieveOCN(Long ocn){
		return ocnDetailsDAO.retrieveOCN(ocn);
	}
	
	@Transactional
	public List<OCNDetailsDO> retrieveById(Long id){
		return ocnDetailsDAO.retrieveById(id);
	}
	
	@Transactional
	public boolean persistList(List<OCNDetailsDO> ocnDetailsDO) {
		return ocnDetailsDAO.persistList(ocnDetailsDO);
	}
}
