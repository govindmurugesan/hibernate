package com.spheresuite.erp.pl.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.OCNDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class OCNDAO {
	static Logger logger = Logger.getLogger(OCNDAO.class.getName());
	
	@Autowired
	private GenericDAOImpl<OCNDO> dao;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public boolean persist(OCNDO ocnDO){
		boolean persistStatus = true;
		try {
			dao.persist(ocnDO);
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	public List<OCNDO> retrieve(){
		List<OCNDO> ocnList = null;
		try {
			ocnList = dao.retrieve(OCNDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return ocnList;
	}
	
	@SuppressWarnings("unchecked")
	public List<OCNDO> retrieveById(Long id){
		 List<OCNDO> ocnList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(OCNDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return ocnList;
	}
	
	public boolean update(OCNDO ocnDO) {
		boolean updateStatus = true;
		try {
			dao.merge(ocnDO);
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}
	
	public boolean persistList(List<OCNDO> ocnList){
		try {
			for (OCNDO ocn : ocnList) {
				dao.persist(ocn);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}

}
