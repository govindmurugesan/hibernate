package com.spheresuite.erp.pl.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.CPMDO;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;

@Component
public class CPMUtil {
	
	private CPMUtil() {}
	
	public static JSONObject getCPMList(List<CPMDO> cpmList){
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (CPMDO cpmDO : cpmList) {
				resultJSONArray.put(getCpmDetailObject(cpmDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getCpmDetailObject(CPMDO cpmDO)throws JSONException{
		
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(cpmDO.getCpmId()));
		result.put(CommonConstants.MONTH, String.valueOf(cpmDO.getMonth()));
		result.put(CommonConstants.AMOUNT, String.valueOf(cpmDO.getAmount()));
		result.put(CommonConstants.STATUS, String.valueOf(cpmDO.getStatus()));
		if(cpmDO.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(cpmDO.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(cpmDO.getUpdatedon())));
		return result;
	}
}
