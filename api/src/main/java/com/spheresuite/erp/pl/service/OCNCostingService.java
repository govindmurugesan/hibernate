package com.spheresuite.erp.pl.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.domainobject.OCNCostingDO;
import com.spheresuite.erp.pl.dao.OCNCostingDAO;

@Service
@Transactional
public class OCNCostingService{
	
	@Autowired
	private OCNCostingDAO ocnCostingDAO;

	@Transactional
	public List<OCNCostingDO> persist(List<OCNCostingDO> ocnList) {
		return ocnCostingDAO.persist(ocnList);
	}

	@Transactional
	public List<OCNCostingDO> retrieveById(Long id) {
		return ocnCostingDAO.retrieveById(id);
	}
	
	@Transactional
	public List<OCNCostingDO> retrieveByOCNDetailsId(Long id) {
		return ocnCostingDAO.retrieveByOCNDetailsId(id);
	}
	
	@Transactional
	public List<Object[]> retrieveByOCNGreigeFabric(Long id) {
		return ocnCostingDAO.retrieveByOCNGreigeFabric(id);
	}
	
	@Transactional
	public boolean delete(Long id) {
		return ocnCostingDAO.delete(id);
	}
}
