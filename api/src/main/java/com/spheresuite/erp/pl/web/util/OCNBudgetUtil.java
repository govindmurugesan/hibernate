package com.spheresuite.erp.pl.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.OCNBudgetDO;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;

@Component
public class OCNBudgetUtil {
	
	private OCNBudgetUtil() {}
	
	public static JSONObject getOCNBudgetList(List<OCNBudgetDO> ocnBudgetList){
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (OCNBudgetDO ocnBudgetDO : ocnBudgetList) {
				resultJSONArray.put(getOCNBudgetObject(ocnBudgetDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getOCNBudgetObject(OCNBudgetDO ocnBudgetDO)throws JSONException{
		
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(ocnBudgetDO.getOcnBudgetId()));
		//result.put(CommonConstants.NAME, String.valueOf(ocnBudgetDO.getOcn()));
		if(ocnBudgetDO.getOcnDetails() != null){
			result.put(CommonConstants.OCN, String.valueOf(ocnBudgetDO.getOcnDetails().getOcnId()));
			result.put(CommonConstants.OCN_NUMBER, String.valueOf(ocnBudgetDO.getOcnDetails().getOcn()));
		} else{
			result.put(CommonConstants.OCN, "");
			result.put(CommonConstants.OCN_NUMBER, "");
		}
		if(ocnBudgetDO.getCpm() != null){
			result.put(CommonConstants.CPM, String.valueOf(ocnBudgetDO.getCpm().getCpmId()));
			result.put(CommonConstants.CPM_AMOUNT, String.valueOf(ocnBudgetDO.getCpm().getAmount()));
		} else{
			result.put(CommonConstants.CPM, "");
			result.put(CommonConstants.CPM_AMOUNT, "");
		}
		if(ocnBudgetDO.getExportIncentive() != null){
			result.put(CommonConstants.EXPORT_INCENTIVE, String.valueOf(ocnBudgetDO.getExportIncentive()));
		} else{
			result.put(CommonConstants.EXPORT_INCENTIVE, "");
		}
		if(ocnBudgetDO.getGreigeFabricm() != null){
			result.put(CommonConstants.GREIGE_FABRICM, String.valueOf(ocnBudgetDO.getGreigeFabricm()));
		} else{
			result.put(CommonConstants.GREIGE_FABRICM, "");
		}
		if(ocnBudgetDO.getGreigeFabric() != null){
			result.put(CommonConstants.GREIGE_FABRIC, String.valueOf(ocnBudgetDO.getGreigeFabric()));
		} else{
			result.put(CommonConstants.GREIGE_FABRIC, "");
		}
		if(ocnBudgetDO.getDyeingCost() != null){
			result.put(CommonConstants.DYEING_COST, String.valueOf(ocnBudgetDO.getDyeingCost()));
		} else{
			result.put(CommonConstants.DYEING_COST, "");
		}
		if(ocnBudgetDO.getFinishingCost() != null){
			result.put(CommonConstants.FINISHING_COST, String.valueOf(ocnBudgetDO.getFinishingCost()));
		} else{
			result.put(CommonConstants.FINISHING_COST, "");
		}
		if(ocnBudgetDO.getSam() != null){
			result.put(CommonConstants.SAM, String.valueOf(ocnBudgetDO.getSam()));
		} else{
			result.put(CommonConstants.SAM, "");
		}
		if(ocnBudgetDO.getTrims() != null){
			result.put(CommonConstants.TRIMS, String.valueOf(ocnBudgetDO.getTrims()));
		} else{
			result.put(CommonConstants.TRIMS, "");
		}
		if(ocnBudgetDO.getRejection() != null){
			result.put(CommonConstants.REJECTION, String.valueOf(ocnBudgetDO.getRejection()));
		} else{
			result.put(CommonConstants.REJECTION, "");
		}
		
		if(ocnBudgetDO.getStatus() != null){
			result.put(CommonConstants.STATUS, String.valueOf(ocnBudgetDO.getStatus()));
		} else{
			result.put(CommonConstants.STATUS, "");
		}
		
		if(ocnBudgetDO.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(ocnBudgetDO.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		if(ocnBudgetDO.getUpdatedon() != null){
			result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(ocnBudgetDO.getUpdatedon())));
		} else{
			result.put(CommonConstants.UPDATEDON, "");
		}
		
		return result;
	}
}
