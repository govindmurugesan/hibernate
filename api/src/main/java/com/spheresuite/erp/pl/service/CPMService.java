package com.spheresuite.erp.pl.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.domainobject.CPMDO;
import com.spheresuite.erp.pl.dao.CPMDAO;

@Service
@Transactional
public class CPMService{
	
	@Autowired
	private CPMDAO cpmDAO;

	@Transactional
	public boolean persist(CPMDO cpmDO) {
		return cpmDAO.persist(cpmDO);
	}
	
	@Transactional
	public List<CPMDO> retrieve() {
		return cpmDAO.retrieve();
	}

	@Transactional
	public boolean update(CPMDO cpmDO) {
		return cpmDAO.update(cpmDO);
	}
	
	@Transactional
	public List<CPMDO> retrieveActive(){
		return cpmDAO.retrieveActive();
	}
	
	@Transactional
	public List<CPMDO> retrieveById(Long id){
		return cpmDAO.retrieveById(id);
	}
	
	@Transactional
	public List<CPMDO> retrieveByMonth(String month){
		return cpmDAO.retrieveByMonth(month);
	}
	
	@Transactional
	public boolean persistList(List<CPMDO> cpmDO) {
		return cpmDAO.persistList(cpmDO);
	}
}
