package com.spheresuite.erp.pl.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.CPMDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class CPMDAO {
	static Logger logger = Logger.getLogger(CPMDAO.class.getName());
	
	@Autowired
	private GenericDAOImpl<CPMDO> dao;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public boolean persist(CPMDO cpmDO){
		boolean persistStatus = true;
		try {
			dao.persist(cpmDO);
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
		}
		return persistStatus;
	}
	
	public List<CPMDO> retrieve(){
		List<CPMDO> cpmList = null;
		try {
			cpmList = dao.retrieve(CPMDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return cpmList;
	}
	
	@SuppressWarnings("unchecked")
	public List<CPMDO> retrieveActive(){
		List<CPMDO> cpmList = null;
		try {
			cpmList =  this.sessionFactory.getCurrentSession().getNamedQuery(CPMDO.FIND_BY_STATUS)
						.setParameter(CommonConstants.STATUS, CommonConstants.ASTATUS)
						.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return cpmList;
	}
	
	@SuppressWarnings("unchecked")
	public List<CPMDO> retrieveByMonth(String month){
		List<CPMDO> cpmList = null;
		try {
			cpmList =  this.sessionFactory.getCurrentSession().getNamedQuery(CPMDO.FIND_BY_MONTH)
						.setParameter(CommonConstants.MONTH, month)
						.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return cpmList;
	}
	
	@SuppressWarnings("unchecked")
	public List<CPMDO> retrieveById(Long id){
		 List<CPMDO> cpmList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(CPMDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return cpmList;
	}
	
	public boolean update(CPMDO cpmDO) {
		boolean updateStatus = true;
		try {
			dao.merge(cpmDO);
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
		}
		return updateStatus;
	}
	
	public boolean persistList(List<CPMDO> cpmList){
		try {
			for (CPMDO cpm : cpmList) {
				dao.persist(cpm);
			}
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
		}
		return true;
	}

}
