package com.spheresuite.erp.pl.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.domainobject.OCNReportDO;
import com.spheresuite.erp.pl.dao.OCNReportDAO;

@Service
@Transactional
public class OCNReportService{
	
	@Autowired
	private OCNReportDAO ocnReportDAO;

	@Transactional
	public boolean persist(OCNReportDO ocnReportDO) {
		return ocnReportDAO.persist(ocnReportDO);
	}
	
	@Transactional
	public List<OCNReportDO> retrieve() {
		return ocnReportDAO.retrieve();
	}
	
	@Transactional
	public List<OCNReportDO> retrieveWithStatus() {
		return ocnReportDAO.retrieveWithStatus();
	}

	@Transactional
	public boolean update(OCNReportDO ocnReportDO) {
		return ocnReportDAO.update(ocnReportDO);
	}
	
	@Transactional
	public List<OCNReportDO> retrieveOCN(Long ocn){
		return ocnReportDAO.retrieveOCN(ocn);
	}
	
	@Transactional
	public List<OCNReportDO> retrieveByStatus(String status){
		return ocnReportDAO.retrieveByStatus(status);
	}
	
	@Transactional
	public List<OCNReportDO> retrieveById(Long id){
		return ocnReportDAO.retrieveById(id);
	}
	
	@Transactional
	public boolean persistList(List<OCNReportDO> ocnReportDO) {
		return ocnReportDAO.persistList(ocnReportDO);
	}
}
