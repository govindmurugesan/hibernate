package com.spheresuite.erp.pl.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.FabricTypeDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class FabricTypeDAO {
	static Logger logger = Logger.getLogger(FabricTypeDAO.class.getName());
	
	@Autowired
	private GenericDAOImpl<FabricTypeDO> dao;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(FabricTypeDO fabricTypeDO){
		boolean persistStatus = true;
		try {
			List<FabricTypeDO> fabricTypeDOList = null;
			fabricTypeDOList = this.sessionFactory.getCurrentSession().getNamedQuery(FabricTypeDO.FIND_BY_NAME)
					.setParameter(CommonConstants.NAME, fabricTypeDO.getName())
					.list();
					if(fabricTypeDOList != null && fabricTypeDOList.size() > 0){
						persistStatus =  false;
					}else{
						dao.persist(fabricTypeDO);
					}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	public List<FabricTypeDO> retrieve(){
		List<FabricTypeDO> fabricTypeList = null;
		try {
			fabricTypeList = dao.retrieve(FabricTypeDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return fabricTypeList;
	}
	
	public List<FabricTypeDO> retrieveActive(){
		List<FabricTypeDO> fabricTypeList = null;
		try {
			fabricTypeList =  dao.retrieveActive(CommonConstants.ACTIVE, FabricTypeDO.FIND_BY_STATUS, FabricTypeDO.class);
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return fabricTypeList;
	}
	
	@SuppressWarnings("unchecked")
	public List<FabricTypeDO> retrieveById(Long id){
		 List<FabricTypeDO> fabricTypeList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(FabricTypeDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return fabricTypeList;
	}
	
	@SuppressWarnings("unchecked")
	public boolean update(FabricTypeDO fabricTypeDO) {
		boolean updateStatus = true;
		try {
			List<FabricTypeDO> fabricTypeDOList = null;
			fabricTypeDOList = this.sessionFactory.getCurrentSession().getNamedQuery(FabricTypeDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.NAME, fabricTypeDO.getName())
					.setParameter(CommonConstants.ID, fabricTypeDO.getFabricTypeId())
					.list();
			if(fabricTypeDOList != null && fabricTypeDOList.size() > 0){
				updateStatus = false;
			}else{
				dao.merge(fabricTypeDO);
			}
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}
	
	public boolean persistList(List<FabricTypeDO> fabricTypeList){
		try {
			for (FabricTypeDO fabricType : fabricTypeList) {
				dao.persist(fabricType);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}

}
