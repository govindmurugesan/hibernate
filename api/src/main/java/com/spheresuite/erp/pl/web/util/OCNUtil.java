package com.spheresuite.erp.pl.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.OCNDO;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;

@Component
public class OCNUtil {
	
	private OCNUtil() {}
	
	public static JSONObject getOCNList(List<OCNDO> ocnList){
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (OCNDO ocnDO : ocnList) {
				resultJSONArray.put(getAddressDetailObject(ocnDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getAddressDetailObject(OCNDO ocnDO)throws JSONException{
		
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(ocnDO.getOcnId()));
		result.put(CommonConstants.OCN, String.valueOf(ocnDO.getOcn()));
		/*result.put(CommonConstants.QUANTITY, String.valueOf(ocnDO.getQty()));*/
		if(ocnDO.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(ocnDO.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(ocnDO.getUpdatedon())));
		return result;
	}
}
