package com.spheresuite.erp.pl.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.OCNReportDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class OCNReportDAO {
	static Logger logger = Logger.getLogger(OCNReportDAO.class.getName());
	
	@Autowired
	private GenericDAOImpl<OCNReportDO> dao;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public boolean persist(OCNReportDO ocnReportDO){
		boolean persistStatus = true;
		try {
				dao.persist(ocnReportDO);
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	@SuppressWarnings("unchecked")
	public List<OCNReportDO> retrieve(){
		List<OCNReportDO> ocnReportList = null;
		try {
			ocnReportList = this.sessionFactory.getCurrentSession().getNamedQuery(OCNReportDO.FIND_ALL)
							.list();
		} catch (Exception eException) {
			System.out.println(eException.getMessage());
		} finally {
		}
		return ocnReportList;
	}
	
	@SuppressWarnings("unchecked")
	public List<OCNReportDO> retrieveOCN(Long ocn){
		List<OCNReportDO> ocnReportList = null;
		try {
			ocnReportList =  this.sessionFactory.getCurrentSession().getNamedQuery(OCNReportDO.FIND_BY_OCN)
					.setParameter(CommonConstants.OCN, ocn)
					.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return ocnReportList;
	}
	
	@SuppressWarnings("unchecked")
	public List<OCNReportDO> retrieveWithStatus(){
		List<OCNReportDO> ocnReportList = null;
		try {
			ocnReportList =  this.sessionFactory.getCurrentSession().getNamedQuery(OCNReportDO.FIND_BY_STATUS_NULL)
					.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return ocnReportList;
	}
	
	@SuppressWarnings("unchecked")
	public List<OCNReportDO> retrieveByStatus(String status){
		List<OCNReportDO> ocnReportList = null;
		try {
			ocnReportList =  this.sessionFactory.getCurrentSession().getNamedQuery(OCNReportDO.FIND_BY_STATUS)
					.setParameter(CommonConstants.STATUS, status)
					.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return ocnReportList;
	}
	
	@SuppressWarnings("unchecked")
	public List<OCNReportDO> retrieveById(Long id){
		 List<OCNReportDO> ocnReportList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(OCNReportDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return ocnReportList;
	}
	
	public boolean update(OCNReportDO ocnReportDO) {
		boolean updateStatus = true;
		try {
				dao.merge(ocnReportDO);
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}
	
	public boolean persistList(List<OCNReportDO> ocnReportList){
		try {
			for (OCNReportDO ocnReport : ocnReportList) {
				dao.persist(ocnReport);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}

}
