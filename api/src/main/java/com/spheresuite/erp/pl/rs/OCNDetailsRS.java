package com.spheresuite.erp.pl.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.FabricTypeDO;
import com.spheresuite.erp.domainobject.OCNCostingDO;
import com.spheresuite.erp.domainobject.OCNDO;
import com.spheresuite.erp.domainobject.OCNDetailsDO;
import com.spheresuite.erp.pl.service.FabricTypeService;
import com.spheresuite.erp.pl.service.OCNCostingService;
import com.spheresuite.erp.pl.service.OCNDetailsService;
import com.spheresuite.erp.pl.service.OCNService;
import com.spheresuite.erp.pl.web.util.OCNDetailsUtil;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/ocndetails")
public class OCNDetailsRS {

	String validation = null;
	static Logger logger = Logger.getLogger(OCNDetailsRS.class.getName());
	
	@Autowired
	private OCNDetailsService ocnDetailsService;
	
	@Autowired
	private FabricTypeService fabricTypeService;
	
	@Autowired
	private OCNCostingService ocnCostingService;
	
	@Autowired
	private OCNService ocnService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				OCNDetailsDO ocnDetailsDO = new OCNDetailsDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.OCN) != null && !inputJSON.get(CommonConstants.OCN).toString().isEmpty()){
			 			List<OCNDO> ocnList = ocnService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.OCN).toString()));
			 			if(ocnList != null && ocnList.size() > 0){
			 				ocnDetailsDO.setOcn(ocnList.get(0));
			 			}
			 		}
			 		if(inputJSON.get(CommonConstants.ARTICLE) != null && !inputJSON.get(CommonConstants.ARTICLE).toString().isEmpty()){
			 			ocnDetailsDO.setArticle(inputJSON.get(CommonConstants.ARTICLE).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.SIZE) != null && !inputJSON.get(CommonConstants.SIZE).toString().isEmpty()){
			 			ocnDetailsDO.setSize(inputJSON.get(CommonConstants.SIZE).toString());
			 		}
			 		ocnDetailsDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			ocnDetailsDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			ocnDetailsDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		ocnDetailsDO.setStatus(CommonConstants.ACTIVESTRING);
			 	}
				if(!ocnDetailsService.persist(ocnDetailsDO)){
					return CommonWebUtil.buildErrorResponse("OCN Details Already Added").toString();
				}
				CommonUtil.persistRecentAcvtivity(inputJSON.get(CommonConstants.UPDATED_BY).toString(), "New OCN Details Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<OCNDetailsDO> ocnDetailsList = ocnDetailsService.retrieveActive();
				respJSON = OCNDetailsUtil.getOCNDetailsList(ocnDetailsList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<OCNDetailsDO> ocnDetailsList = ocnDetailsService.retrieve();
				respJSON = OCNDetailsUtil.getOCNDetailsList(ocnDetailsList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveUniqueOCN", method = RequestMethod.GET)
	public @ResponseBody String retrieveUniqueOCN(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<OCNDetailsDO> ocnDetailsList = ocnDetailsService.retrieveUniqueOCN();
				respJSON = OCNDetailsUtil.getOCNUniqueList(ocnDetailsList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveOCN/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveOCN(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && inputJSON.get(CommonConstants.OCN) != null && !inputJSON.get(CommonConstants.OCN).toString().isEmpty()){
					List<OCNDetailsDO> ocnDetailsList = ocnDetailsService.retrieveOCN(Long.parseLong(inputJSON.get(CommonConstants.OCN).toString()));
					respJSON = OCNDetailsUtil.getOCNDetailsList(ocnDetailsList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveByOCNGreigeFabric/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByOCNGreigeFabric(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && inputJSON.get(CommonConstants.OCN) != null && !inputJSON.get(CommonConstants.OCN).toString().isEmpty()){
					List<Object[]> ocnCostingList = ocnCostingService.retrieveByOCNGreigeFabric(Long.parseLong(inputJSON.get(CommonConstants.OCN).toString()));
					respJSON = OCNDetailsUtil.getOCNFabricSumList(ocnCostingList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		String ocnId =  "";
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSONObj = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
				if (inputJSONObj.get(CommonConstants.OCNDETAILS) != null && !inputJSONObj.get(CommonConstants.OCNDETAILS).toString().isEmpty()){
			 		JSONArray resultJSONArray = new JSONArray();
			 		resultJSONArray.put(inputJSONObj.get(CommonConstants.OCNDETAILS));
			 		String rec = (String) resultJSONArray.get(0).toString();
			 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
			 		List<OCNDO> ocnList = null;
			 		for (int i = 0; i < resultJSONArray1.size(); i++) {
			 			JSONObject inputJSON = (JSONObject) resultJSONArray1.get(i);
			 			OCNDetailsDO ocnDetailsDO = new OCNDetailsDO();
			 			if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					 		List<OCNDetailsDO> ocnDetailsList = ocnDetailsService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					 		if(ocnDetailsList != null && ocnDetailsList.size() > 0){
						 		ocnDetailsDO = ocnDetailsList.get(0);
						 		if(inputJSON.get(CommonConstants.OCN) != null && !inputJSON.get(CommonConstants.OCN).toString().isEmpty()){
						 			ocnList = ocnService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.OCN).toString()));
						 			if(ocnList != null && ocnList.size() > 0){
						 				ocnDetailsDO.setOcn(ocnList.get(0));
						 				ocnId = ocnList.get(0).getOcnId().toString();
						 			}
						 		}
						 		if(inputJSON.get(CommonConstants.ARTICLE) != null && !inputJSON.get(CommonConstants.ARTICLE).toString().isEmpty()){
						 			ocnDetailsDO.setArticle(inputJSON.get(CommonConstants.ARTICLE).toString());
						 		}
						 		if(inputJSON.get(CommonConstants.SIZE) != null && !inputJSON.get(CommonConstants.SIZE).toString().isEmpty()){
						 			ocnDetailsDO.setSize(inputJSON.get(CommonConstants.SIZE).toString());
						 		}
						 		if(inputJSON.get(CommonConstants.QUANTITY) != null && !inputJSON.get(CommonConstants.QUANTITY).toString().isEmpty()){
						 			ocnDetailsDO.setQty(Long.parseLong(inputJSON.get(CommonConstants.QUANTITY).toString()));
						 		}
						 		ocnDetailsDO.setUpdatedon(new Date());
						 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
						 			ocnDetailsDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
						 		}
						 		if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
						 			ocnDetailsDO.setStatus(inputJSON.get(CommonConstants.STATUS).toString());
						 		}
						 		/*if(!ocnDetailsService.update(ocnDetailsDO)){
									return CommonWebUtil.buildErrorResponse("OCN Details Already Added").toString();
								}*/
						 		ocnDetailsService.update(ocnDetailsDO);
						 		List<OCNCostingDO> ocnCostingList = new ArrayList<OCNCostingDO>(); 
						 		if(ocnDetailsDO.getOcnDetailsId() != null){
						 			OCNCostingDO ocnCostingDO = new OCNCostingDO();
						 			if(inputJSON.get(CommonConstants.FABRICTYPE) != null && !inputJSON.get(CommonConstants.FABRICTYPE).toString().isEmpty()){
							 			List<FabricTypeDO> ftList = fabricTypeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.FABRICTYPE).toString()));
							 			if(ftList != null && ftList.size() > 0){
							 				ocnCostingDO.setFabricType(ftList.get(0));
							 			}
							 		}
						 			ocnCostingDO.setOcnDetails(ocnDetailsDO);
						 			if(ocnList != null && ocnList.size() > 0){
						 				ocnCostingDO.setOcn(ocnList.get(0));
						 				ocnId = ocnList.get(0).getOcnId().toString();
						 			}
							 		if(inputJSON.get(CommonConstants.BUDGETCON) != null && !inputJSON.get(CommonConstants.BUDGETCON).toString().isEmpty()){
							 			ocnCostingDO.setArticleCon(Double.parseDouble(inputJSON.get(CommonConstants.BUDGETCON).toString()));
							 		}
							 		if(inputJSON.get(CommonConstants.VALUELOSS) != null && !inputJSON.get(CommonConstants.VALUELOSS).toString().isEmpty()){
							 			ocnCostingDO.setValueLoss(Double.parseDouble(inputJSON.get(CommonConstants.VALUELOSS).toString()));
							 		}
							 		if(inputJSON.get(CommonConstants.GRCON) != null && !inputJSON.get(CommonConstants.GRCON).toString().isEmpty()){
							 			ocnCostingDO.setGrCon(Double.parseDouble(inputJSON.get(CommonConstants.GRCON).toString()));
							 		}
							 		if(inputJSON.get(CommonConstants.CUTTINGLOSS) != null && !inputJSON.get(CommonConstants.CUTTINGLOSS).toString().isEmpty()){
							 			ocnCostingDO.setCuttingLoss(Double.parseDouble(inputJSON.get(CommonConstants.CUTTINGLOSS).toString()));
							 		}
							 		if(inputJSON.get(CommonConstants.MULOSS) != null && !inputJSON.get(CommonConstants.MULOSS).toString().isEmpty()){
							 			ocnCostingDO.setMuLoss(Double.parseDouble(inputJSON.get(CommonConstants.MULOSS).toString()));
							 		}
							 		if(inputJSON.get(CommonConstants.BUDGETCOST) != null && !inputJSON.get(CommonConstants.BUDGETCOST).toString().isEmpty()){
							 			ocnCostingDO.setBudgetCost(Double.parseDouble(inputJSON.get(CommonConstants.BUDGETCOST).toString()));
							 		}
							 		if(inputJSON.get(CommonConstants.REQUIREDMETER) != null && !inputJSON.get(CommonConstants.REQUIREDMETER).toString().isEmpty()){
							 			ocnCostingDO.setReqm(Double.parseDouble(inputJSON.get(CommonConstants.REQUIREDMETER).toString()));
							 			if(ocnCostingDO.getReqm() != null){
							 				ocnCostingDO.setQty(ocnDetailsDO.getQty()*ocnCostingDO.getReqm());
							 			}
							 			
							 			if(ocnCostingDO.getBudgetCost() != null){
							 				ocnCostingDO.setAmount(ocnCostingDO.getQty()*ocnCostingDO.getBudgetCost());
							 			}
							 			
								 		ocnCostingDO.setUpdatedon(new Date());
								 		ocnCostingDO.setCreatedon(new Date());
								 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
								 			ocnCostingDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
								 			ocnCostingDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
								 		}
								 		ocnCostingList.add(ocnCostingDO);
							 		}
							 		if(inputJSON.get(CommonConstants.FABRICTYPELIST) != null && !inputJSON.get(CommonConstants.FABRICTYPELIST).toString().isEmpty()){
							 			JSONArray resultFabricCostingJSONArray = new JSONArray();
							 			resultFabricCostingJSONArray.put(inputJSON.get(CommonConstants.FABRICTYPELIST));
								 		String rec1 = (String) resultFabricCostingJSONArray.get(0).toString();
								 		org.json.simple.JSONArray resultFabricCostingJSONArray1 = CommonWebUtil.getInputParamsArray(rec1);
								 		for (int j = 0; j < resultFabricCostingJSONArray1.size(); j++) {
								 			JSONObject inputJSON1 = (JSONObject) resultFabricCostingJSONArray1.get(j);
								 			ocnCostingDO = new OCNCostingDO();
								 			if(inputJSON1.get(CommonConstants.FABRICTYPE) != null && !inputJSON1.get(CommonConstants.FABRICTYPE).toString().isEmpty()){
									 			List<FabricTypeDO> ftList = fabricTypeService.retrieveById(Long.parseLong(inputJSON1.get(CommonConstants.FABRICTYPE).toString()));
									 			if(ftList != null && ftList.size() > 0){
									 				ocnCostingDO.setFabricType(ftList.get(0));
									 			}
									 		}
								 			ocnCostingDO.setOcnDetails(ocnDetailsDO);
								 			if(ocnList != null && ocnList.size() > 0){
								 				ocnCostingDO.setOcn(ocnList.get(0));
								 			}
									 		if(inputJSON1.get(CommonConstants.BUDGETCON) != null && !inputJSON1.get(CommonConstants.BUDGETCON).toString().isEmpty()){
									 			ocnCostingDO.setArticleCon(Double.parseDouble(inputJSON1.get(CommonConstants.BUDGETCON).toString()));
									 		}
									 		if(inputJSON1.get(CommonConstants.VALUELOSS) != null && !inputJSON1.get(CommonConstants.VALUELOSS).toString().isEmpty()){
									 			ocnCostingDO.setValueLoss(Double.parseDouble(inputJSON1.get(CommonConstants.VALUELOSS).toString()));
									 		}
									 		if(inputJSON1.get(CommonConstants.GRCON) != null && !inputJSON1.get(CommonConstants.GRCON).toString().isEmpty()){
									 			ocnCostingDO.setGrCon(Double.parseDouble(inputJSON1.get(CommonConstants.GRCON).toString()));
									 		}
									 		if(inputJSON1.get(CommonConstants.CUTTINGLOSS) != null && !inputJSON1.get(CommonConstants.CUTTINGLOSS).toString().isEmpty()){
									 			ocnCostingDO.setCuttingLoss(Double.parseDouble(inputJSON1.get(CommonConstants.CUTTINGLOSS).toString()));
									 		}
									 		if(inputJSON1.get(CommonConstants.MULOSS) != null && !inputJSON1.get(CommonConstants.MULOSS).toString().isEmpty()){
									 			ocnCostingDO.setMuLoss(Double.parseDouble(inputJSON1.get(CommonConstants.MULOSS).toString()));
									 		}
									 		if(inputJSON1.get(CommonConstants.BUDGETCOST) != null && !inputJSON1.get(CommonConstants.BUDGETCOST).toString().isEmpty()){
									 			ocnCostingDO.setBudgetCost(Double.parseDouble(inputJSON1.get(CommonConstants.BUDGETCOST).toString()));
									 		}
									 		if(inputJSON1.get(CommonConstants.REQUIREDMETER) != null && !inputJSON1.get(CommonConstants.REQUIREDMETER).toString().isEmpty()){
									 			ocnCostingDO.setReqm(Double.parseDouble(inputJSON1.get(CommonConstants.REQUIREDMETER).toString()));
									 			if(ocnCostingDO.getReqm() != null){
									 				ocnCostingDO.setQty(ocnDetailsDO.getQty()*ocnCostingDO.getReqm());
									 			}
									 			
									 			if(ocnCostingDO.getBudgetCost() != null){
									 				ocnCostingDO.setAmount(ocnCostingDO.getQty()*ocnCostingDO.getBudgetCost());
									 			}
										 		ocnCostingDO.setUpdatedon(new Date());
										 		ocnCostingDO.setCreatedon(new Date());
										 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
										 			ocnCostingDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
										 			ocnCostingDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
										 		}
										 		ocnCostingList.add(ocnCostingDO);
									 		}
								 		}
							 		}
							 		if(ocnCostingList.size() > 0){
							 			ocnCostingService.persist(ocnCostingList);
							 		}
						 		}
						 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New OCN Details Updated");
					 		}
					 	}
			 		}
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			System.out.println(e.getMessage());
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponseId(ocnId).toString();
	}
	
	/*@RequestMapping(value = "/importOCNDetails", method = RequestMethod.POST)
	public @ResponseBody String importOCNDetails(Model model, HttpServletRequest request) {
		try {
			JSONObject inputJSON =  CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
			JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
			List<OCNDetailsDO> OCNDetailsList = new ArrayList<OCNDetailsDO>();
			Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY);
			for (int i=0; i < fileData.size(); i++){
				JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
				OCNDetailsDO ocnDetailsDO = new OCNDetailsDO();
				if (colName.get(CommonConstants.OCN) != null && !colName.get(CommonConstants.OCN).toString().isEmpty()){
					ocnDetailsDO.setOcn(rowJSON.get(colName.get(CommonConstants.OCN)).toString());
		 		}
		 		if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
		 			ocnDetailsDO.setUpdatedby(updatedBy.toString());
		 			ocnDetailsDO.setCreatedby(updatedBy.toString());
		 		}
		 		ocnDetailsDO.setUpdatedon(new Date());
		 		ocnDetailsDO.setCreatedon(new Date());
		 		ocnDetailsDO.setStatus(CommonConstants.ACTIVESTRING);
		 		OCNDetailsList.add(ocnDetailsDO);
			}
			ocnDetailsService.persistList(OCNDetailsList);
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}*/
}
