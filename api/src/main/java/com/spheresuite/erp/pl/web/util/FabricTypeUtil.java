package com.spheresuite.erp.pl.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.FabricTypeDO;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;

@Component
public class FabricTypeUtil {
	
	private FabricTypeUtil() {}
	
	public static JSONObject getFabricTypeList(List<FabricTypeDO> fabricTypeList){
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (FabricTypeDO fabricTypeDO : fabricTypeList) {
				resultJSONArray.put(getAddressDetailObject(fabricTypeDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getAddressDetailObject(FabricTypeDO fabricTypeDO)throws JSONException{
		
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(fabricTypeDO.getFabricTypeId()));
		result.put(CommonConstants.NAME, String.valueOf(fabricTypeDO.getName()));
		result.put(CommonConstants.STATUS, String.valueOf(fabricTypeDO.getStatus()));
		if(fabricTypeDO.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(fabricTypeDO.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(fabricTypeDO.getUpdatedon())));
		return result;
	}
}
