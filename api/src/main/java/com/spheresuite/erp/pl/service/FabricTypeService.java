package com.spheresuite.erp.pl.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.domainobject.FabricTypeDO;
import com.spheresuite.erp.pl.dao.FabricTypeDAO;

@Service
@Transactional
public class FabricTypeService{
	
	@Autowired
	private FabricTypeDAO fabricTypeDAO;

	@Transactional
	public boolean persist(FabricTypeDO fabricTypeDO) {
		return fabricTypeDAO.persist(fabricTypeDO);
	}
	
	@Transactional
	public List<FabricTypeDO> retrieve() {
		return fabricTypeDAO.retrieve();
	}

	@Transactional
	public boolean update(FabricTypeDO fabricTypeDO) {
		return fabricTypeDAO.update(fabricTypeDO);
	}
	
	@Transactional
	public List<FabricTypeDO> retrieveActive(){
		return fabricTypeDAO.retrieveActive();
	}
	
	@Transactional
	public List<FabricTypeDO> retrieveById(Long id){
		return fabricTypeDAO.retrieveById(id);
	}
	
	@Transactional
	public boolean persistList(List<FabricTypeDO> fabricTypeDO) {
		return fabricTypeDAO.persistList(fabricTypeDO);
	}
}
