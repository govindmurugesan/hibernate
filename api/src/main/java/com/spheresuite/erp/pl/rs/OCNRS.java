package com.spheresuite.erp.pl.rs;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.OCNDO;
import com.spheresuite.erp.pl.service.OCNService;
import com.spheresuite.erp.pl.web.util.OCNUtil;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/ocn")
public class OCNRS {

	String validation = null;
	static Logger logger = Logger.getLogger(OCNRS.class.getName());
	
	@Autowired
	private OCNService ocnService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				OCNDO ocnDO = new OCNDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.OCN) != null && !inputJSON.get(CommonConstants.OCN).toString().isEmpty()){
			 			ocnDO.setOcn(inputJSON.get(CommonConstants.OCN).toString());
			 		}
			 		/*if(inputJSON.get(CommonConstants.QUANTITY) != null && !inputJSON.get(CommonConstants.QUANTITY).toString().isEmpty()){
			 			ocnDO.setQty(Long.parseLong(inputJSON.get(CommonConstants.QUANTITY).toString()));
			 		}*/
			 		ocnDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			ocnDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			ocnDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 	}
				ocnService.persist(ocnDO);
				CommonUtil.persistRecentAcvtivity(inputJSON.get(CommonConstants.UPDATED_BY).toString(), "New Fabric Type Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<OCNDO> ocnList = ocnService.retrieve();
				respJSON = OCNUtil.getOCNList(ocnList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				OCNDO ocnDO = new OCNDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<OCNDO> ocnList = ocnService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(ocnList != null && ocnList.size() > 0){
				 		ocnDO = ocnList.get(0);
				 		if(inputJSON.get(CommonConstants.OCN) != null && !inputJSON.get(CommonConstants.OCN).toString().isEmpty()){
				 			ocnDO.setOcn(inputJSON.get(CommonConstants.OCN).toString());
				 		}
				 		/*if(inputJSON.get(CommonConstants.QUANTITY) != null && !inputJSON.get(CommonConstants.QUANTITY).toString().isEmpty()){
				 			ocnDO.setQty(Long.parseLong(inputJSON.get(CommonConstants.QUANTITY).toString()));
				 		}*/
				 		ocnDO.setUpdatedon(new Date());
				 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			ocnDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		ocnService.update(ocnDO);
				 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Fabric Type Updated");
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	/*@RequestMapping(value = "/importOCN", method = RequestMethod.POST)
	public @ResponseBody String importOCN(Model model, HttpServletRequest request) {
		try {
			JSONObject inputJSON =  CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
			JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
			List<OCNDO> OCNList = new ArrayList<OCNDO>();
			Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY);
			for (int i=0; i < fileData.size(); i++){
				JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
				OCNDO ocnDO = new OCNDO();
				if (colName.get(CommonConstants.NAME) != null && !colName.get(CommonConstants.NAME).toString().isEmpty()){
					ocnDO.setName(rowJSON.get(colName.get(CommonConstants.NAME)).toString());
		 		}
		 		if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
		 			ocnDO.setUpdatedby(updatedBy.toString());
		 			ocnDO.setCreatedby(updatedBy.toString());
		 		}
		 		ocnDO.setUpdatedon(new Date());
		 		ocnDO.setCreatedon(new Date());
		 		ocnDO.setStatus(CommonConstants.ACTIVE);
		 		OCNList.add(ocnDO);
			}
			ocnService.persistList(OCNList);
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}*/
}
