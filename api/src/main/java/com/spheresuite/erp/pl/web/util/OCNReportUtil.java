package com.spheresuite.erp.pl.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.OCNReportDO;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;

@Component
public class OCNReportUtil {
	
	private OCNReportUtil() {}
	
	public static JSONObject getOCNReportList(List<OCNReportDO> ocnReportList){
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (OCNReportDO ocnReportDO : ocnReportList) {
				resultJSONArray.put(getOCNReportObject(ocnReportDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getOCNReportObject(OCNReportDO ocnReportDO)throws JSONException{
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(ocnReportDO.getOcnReportId()));
		if(ocnReportDO.getOcnDetails() != null){
			result.put(CommonConstants.OCN, String.valueOf(ocnReportDO.getOcnDetails().getOcnId()));
			result.put(CommonConstants.OCN_NUMBER, String.valueOf(ocnReportDO.getOcnDetails().getOcn()));
		} else{
			result.put(CommonConstants.OCN, "");
			result.put(CommonConstants.OCN_NUMBER, "");
		}
		if(ocnReportDO.getOcnBudget()!= null){
			if(ocnReportDO.getOcnBudget().getGreigeFabric() != null){
				result.put(CommonConstants.BUDGET_GREIGE_FABRIC, String.valueOf(ocnReportDO.getOcnBudget().getGreigeFabric()));
			} else{
				result.put(CommonConstants.BUDGET_GREIGE_FABRIC, "");
			}
			if(ocnReportDO.getOcnBudget().getDyeingCost() != null){
				result.put(CommonConstants.BUDGET_DYEING_COST, String.valueOf(ocnReportDO.getOcnBudget().getDyeingCost()));
			} else{
				result.put(CommonConstants.BUDGET_DYEING_COST, "");
			}
			if(ocnReportDO.getOcnBudget().getFinishingCost() != null){
				result.put(CommonConstants.BUDGET_FINISHING_COST, String.valueOf(ocnReportDO.getOcnBudget().getFinishingCost()));
			} else{
				result.put(CommonConstants.BUDGET_FINISHING_COST, "");
			}
			if(ocnReportDO.getOcnBudget().getSam() != null){
				result.put(CommonConstants.BUDGET_SAM, String.valueOf(ocnReportDO.getOcnBudget().getSam()));
			} else{
				result.put(CommonConstants.BUDGET_SAM, "");
			}
			if(ocnReportDO.getOcnBudget().getTrims() != null){
				result.put(CommonConstants.BUDGET_TRIMS, String.valueOf(ocnReportDO.getOcnBudget().getTrims()));
			} else{
				result.put(CommonConstants.BUDGET_TRIMS, "");
			}
			if(ocnReportDO.getOcnBudget().getRejection() != null){
				result.put(CommonConstants.BUDGET_REJECTION, String.valueOf(ocnReportDO.getOcnBudget().getRejection()));
			} else{
				result.put(CommonConstants.BUDGET_REJECTION, "");
			}
		}else{
			result.put(CommonConstants.BUDGET_REJECTION, "");
			result.put(CommonConstants.BUDGET_TRIMS, "");
			result.put(CommonConstants.BUDGET_SAM, "");
			result.put(CommonConstants.BUDGET_FINISHING_COST, "");
			result.put(CommonConstants.BUDGET_DYEING_COST, "");
			result.put(CommonConstants.BUDGET_GREIGE_FABRIC, "");
		}
		if(ocnReportDO.getGreigeFabric() != null){
			result.put(CommonConstants.GREIGE_FABRIC, String.valueOf(ocnReportDO.getGreigeFabric()));
		} else{
			result.put(CommonConstants.GREIGE_FABRIC, "");
		}
		if(ocnReportDO.getDyeingCost() != null){
			result.put(CommonConstants.DYEING_COST, String.valueOf(ocnReportDO.getDyeingCost()));
		} else{
			result.put(CommonConstants.DYEING_COST, "");
		}
		if(ocnReportDO.getFinishingCost() != null){
			result.put(CommonConstants.FINISHING_COST, String.valueOf(ocnReportDO.getFinishingCost()));
		} else{
			result.put(CommonConstants.FINISHING_COST, "");
		}
		if(ocnReportDO.getSam() != null){
			result.put(CommonConstants.SAM, String.valueOf(ocnReportDO.getSam()));
		} else{
			result.put(CommonConstants.SAM, "");
		}
		if(ocnReportDO.getTrims() != null){
			result.put(CommonConstants.TRIMS, String.valueOf(ocnReportDO.getTrims()));
		} else{
			result.put(CommonConstants.TRIMS, "");
		}
		if(ocnReportDO.getRejection() != null){
			result.put(CommonConstants.REJECTION, String.valueOf(ocnReportDO.getRejection()));
		} else{
			result.put(CommonConstants.REJECTION, "");
		}
		if(ocnReportDO.getReason() != null){
			result.put(CommonConstants.REASON, String.valueOf(ocnReportDO.getReason()));
		} else{
			result.put(CommonConstants.REASON, "");
		}
		if(ocnReportDO.getGreigeFabricQty() != null){
			result.put(CommonConstants.GREIGE_FABRIC_QTY, String.valueOf(ocnReportDO.getGreigeFabricQty()));
		} else{
			result.put(CommonConstants.GREIGE_FABRIC_QTY, "");
		}
		if(ocnReportDO.getDyeingCostQty() != null){
			result.put(CommonConstants.DYEING_COST_QTY, String.valueOf(ocnReportDO.getDyeingCostQty()));
		} else{
			result.put(CommonConstants.DYEING_COST_QTY, "");
		}
		if(ocnReportDO.getFinishingCostQty() != null){
			result.put(CommonConstants.FINISHING_COST_QTY, String.valueOf(ocnReportDO.getFinishingCostQty()));
		} else{
			result.put(CommonConstants.FINISHING_COST_QTY, "");
		}
		if(ocnReportDO.getRejectionQty() != null){
			result.put(CommonConstants.REJECTION_QTY, String.valueOf(ocnReportDO.getRejectionQty()));
		} else{
			result.put(CommonConstants.REJECTION_QTY, "");
		}
		if(ocnReportDO.getTrimsQty() != null){
			result.put(CommonConstants.TRIMS_QTY, String.valueOf(ocnReportDO.getTrimsQty()));
		} else{
			result.put(CommonConstants.TRIMS_QTY, "");
		}
		if(ocnReportDO.getSamQty() != null){
			result.put(CommonConstants.SAM_QTY, String.valueOf(ocnReportDO.getSamQty()));
		} else{
			result.put(CommonConstants.SAM_QTY, "");
		}
		if(ocnReportDO.getGreigeFabricAmount() != null){
			result.put(CommonConstants.GREIGE_FABRIC_AMOUNT, String.valueOf(ocnReportDO.getGreigeFabricAmount()));
		} else{
			result.put(CommonConstants.GREIGE_FABRIC_AMOUNT, "");
		}
		if(ocnReportDO.getDyeingAmount() != null){
			result.put(CommonConstants.DYEING_COST_AMOUNT, String.valueOf(ocnReportDO.getDyeingAmount()));
		} else{
			result.put(CommonConstants.DYEING_COST_AMOUNT, "");
		}
		if(ocnReportDO.getFinishingAmount() != null){
			result.put(CommonConstants.FINISHING_COST_AMOUNT, String.valueOf(ocnReportDO.getFinishingAmount()));
		} else{
			result.put(CommonConstants.FINISHING_COST_AMOUNT, "");
		}
		if(ocnReportDO.getRejectionAmount() != null){
			result.put(CommonConstants.REJECTION_AMOUNT, String.valueOf(ocnReportDO.getRejectionAmount()));
		} else{
			result.put(CommonConstants.REJECTION_AMOUNT, "");
		}
		if(ocnReportDO.getTrimsAmount() != null){
			result.put(CommonConstants.TRIMS_AMOUNT, String.valueOf(ocnReportDO.getTrimsAmount()));
		} else{
			result.put(CommonConstants.TRIMS_AMOUNT, "");
		}
		if(ocnReportDO.getSamAmount() != null){
			result.put(CommonConstants.SAM_AMOUNT, String.valueOf(ocnReportDO.getSamAmount()));
		} else{
			result.put(CommonConstants.SAM_AMOUNT, "");
		}
		if(ocnReportDO.getGreigeFabricm() != null){
			result.put(CommonConstants.GREIGE_FABRICM, String.valueOf(ocnReportDO.getGreigeFabricm()));
		} else{
			result.put(CommonConstants.GREIGE_FABRICM, "");
		}
		if(ocnReportDO.getAmount() != null){
			result.put(CommonConstants.AMOUNT, String.valueOf(ocnReportDO.getAmount()));
		} else{
			result.put(CommonConstants.AMOUNT, "");
		}
		if(ocnReportDO.getInvoiceAmount() != null){
			result.put(CommonConstants.INVOICE_AMOUNT, String.valueOf(ocnReportDO.getInvoiceAmount()));
		} else{
			result.put(CommonConstants.INVOICE_AMOUNT, "");
		}
		if(ocnReportDO.getSalesPercentage() != null){
			result.put(CommonConstants.SALES_PERCENTAGE, String.valueOf(ocnReportDO.getSalesPercentage()));
		} else{
			result.put(CommonConstants.SALES_PERCENTAGE, "");
		}
		if(ocnReportDO.getCustomerId() != null){
			result.put(CommonConstants.CUSTOMERID, String.valueOf(ocnReportDO.getCustomerId()));
		} else{
			result.put(CommonConstants.CUSTOMERID, "");
		}
		if(ocnReportDO.getCustomerName() != null){
			result.put(CommonConstants.CUSTOMERNAME, String.valueOf(ocnReportDO.getCustomerName()));
		} else{
			result.put(CommonConstants.CUSTOMERNAME, "");
		}
		if(ocnReportDO.getShippingDate() != null){
			result.put(CommonConstants.SHIPPINGDATE, String.valueOf(ocnReportDO.getShippingDate()));
		} else{
			result.put(CommonConstants.SHIPPINGDATE, "");
		}
		if(ocnReportDO.getStatus() != null){
			result.put(CommonConstants.STATUS, String.valueOf(ocnReportDO.getStatus()));
		} else{
			result.put(CommonConstants.STATUS, "");
		}
		
		if(ocnReportDO.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(ocnReportDO.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		if(ocnReportDO.getUpdatedon() != null){
			result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(ocnReportDO.getUpdatedon())));
		} else{
			result.put(CommonConstants.UPDATEDON, "");
		}
		
		return result;
	}
}
