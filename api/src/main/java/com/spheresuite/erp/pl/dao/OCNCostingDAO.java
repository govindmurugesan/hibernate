package com.spheresuite.erp.pl.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.OCNCostingDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class OCNCostingDAO {
	static Logger logger = Logger.getLogger(OCNCostingDAO.class.getName());
	@Autowired
	private GenericDAOImpl<OCNCostingDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public List<OCNCostingDO> persist(List<OCNCostingDO> ocnList)  {
		try {
			if(ocnList != null && ocnList.size() > 0){
				org.hibernate.Query query = this.sessionFactory.getCurrentSession().createQuery("delete from OCNCostingDO e where e.ocnDetails.ocnDetailsId=:id")
					.setParameter(CommonConstants.ID, ocnList.get(0).getOcnDetails().getOcnDetailsId());
				query.executeUpdate();
			}
			for (OCNCostingDO ocnDO : ocnList) {
				genericObject.persist(ocnDO);
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return ocnList;
	}
	
	public boolean delete(Long id)  {
		try {
			org.hibernate.Query query = this.sessionFactory.getCurrentSession().createQuery("delete from OCNCostingDO e where e.ocnDetails.ocnDetailsId=:id")
					.setParameter(CommonConstants.ID, id);
			query.executeUpdate();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return true;
	}
	
	@SuppressWarnings("unchecked")
	public List<OCNCostingDO> retrieveById(Long id)  {
		List<OCNCostingDO> proposalList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(OCNCostingDO.FIND_BY_ID)
			.setParameter(CommonConstants.ID, id)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return proposalList;
	}
	
	@SuppressWarnings("unchecked")
	public List<OCNCostingDO> retrieveByOCNDetailsId(Long id)  {
		List<OCNCostingDO> proposalList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(OCNCostingDO.FIND_BY_OCN_DETAILS_ID)
			.setParameter(CommonConstants.ID, id)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return proposalList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> retrieveByOCNGreigeFabric(Long id)  {
		List<Object[]> proposalList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(OCNCostingDO.FIND_BY_OCN_ID)
			.setParameter(CommonConstants.ID, id)
			.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return proposalList;
	}
	
}
