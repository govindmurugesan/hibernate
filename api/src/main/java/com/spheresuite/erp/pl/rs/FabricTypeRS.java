package com.spheresuite.erp.pl.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.FabricTypeDO;
import com.spheresuite.erp.pl.service.FabricTypeService;
import com.spheresuite.erp.pl.web.util.FabricTypeUtil;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/fabrictype")
public class FabricTypeRS {

	String validation = null;
	static Logger logger = Logger.getLogger(FabricTypeRS.class.getName());
	
	@Autowired
	private FabricTypeService fabricTypeService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				FabricTypeDO fabricTypeDO = new FabricTypeDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
			 			fabricTypeDO.setName(inputJSON.get(CommonConstants.NAME).toString());
			 		}
			 		fabricTypeDO.setUpdatedon(new Date());
			 		fabricTypeDO.setCreatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			fabricTypeDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			fabricTypeDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		fabricTypeDO.setStatus(CommonConstants.ACTIVE);
			 	}
				if(!fabricTypeService.persist(fabricTypeDO)){
					return CommonWebUtil.buildErrorResponse("Fabric Type Already Added").toString();
				}
				CommonUtil.persistRecentAcvtivity(inputJSON.get(CommonConstants.UPDATED_BY).toString(), "New Fabric Type Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<FabricTypeDO> fabricTypeList = fabricTypeService.retrieveActive();
				respJSON = FabricTypeUtil.getFabricTypeList(fabricTypeList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<FabricTypeDO> fabricTypeList = fabricTypeService.retrieve();
				respJSON = FabricTypeUtil.getFabricTypeList(fabricTypeList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				FabricTypeDO fabricTypeDO = new FabricTypeDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<FabricTypeDO> fabricTypeList = fabricTypeService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(fabricTypeList != null && fabricTypeList.size() > 0){
				 		fabricTypeDO = fabricTypeList.get(0);
				 		if(inputJSON.get(CommonConstants.NAME) != null && !inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
				 			fabricTypeDO.setName(inputJSON.get(CommonConstants.NAME).toString());
				 		}
				 		fabricTypeDO.setUpdatedon(new Date());
				 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			fabricTypeDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
				 			fabricTypeDO.setStatus((char) (inputJSON.get(CommonConstants.STATUS).toString().charAt(0)));
				 		}
				 		if(!fabricTypeService.update(fabricTypeDO)){
							return CommonWebUtil.buildErrorResponse("Fabric Type Already Added").toString();
						}
				 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Fabric Type Updated");
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/importFabricType", method = RequestMethod.POST)
	public @ResponseBody String importFabricType(Model model, HttpServletRequest request) {
		try {
			JSONObject inputJSON =  CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
			JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
			List<FabricTypeDO> FabricTypeList = new ArrayList<FabricTypeDO>();
			Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY);
			for (int i=0; i < fileData.size(); i++){
				JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
				FabricTypeDO fabricTypeDO = new FabricTypeDO();
				if (colName.get(CommonConstants.NAME) != null && !colName.get(CommonConstants.NAME).toString().isEmpty()){
					fabricTypeDO.setName(rowJSON.get(colName.get(CommonConstants.NAME)).toString());
		 		}
		 		if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
		 			fabricTypeDO.setUpdatedby(updatedBy.toString());
		 			fabricTypeDO.setCreatedby(updatedBy.toString());
		 		}
		 		fabricTypeDO.setUpdatedon(new Date());
		 		fabricTypeDO.setCreatedon(new Date());
		 		fabricTypeDO.setStatus(CommonConstants.ACTIVE);
		 		FabricTypeList.add(fabricTypeDO);
			}
			fabricTypeService.persistList(FabricTypeList);
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
