package com.spheresuite.erp.pl.dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.OCNBudgetDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class OCNBudgetDAO {
	static Logger logger = Logger.getLogger(OCNBudgetDAO.class.getName());
	
	@Autowired
	private GenericDAOImpl<OCNBudgetDO> dao;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public boolean persist(OCNBudgetDO ocnBudgetDO){
		boolean persistStatus = true;
		try {
			dao.persist(ocnBudgetDO);
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	public List<OCNBudgetDO> retrieve(){
		List<OCNBudgetDO> ocnBudgetList = null;
		try {
			ocnBudgetList = dao.retrieve(OCNBudgetDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return ocnBudgetList;
	}
	
	public List<OCNBudgetDO> retrieveActive(){
		List<OCNBudgetDO> ocnBudgetList = null;
		try {
			ocnBudgetList =  dao.retrieveActive(CommonConstants.ACTIVE, OCNBudgetDO.FIND_BY_STATUS, OCNBudgetDO.class);
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return ocnBudgetList;
	}
	
	@SuppressWarnings("unchecked")
	public List<OCNBudgetDO> retrieveOCN(String ocn){
		List<OCNBudgetDO> ocnBudgetList = null;
		try {
			ocnBudgetList =  this.sessionFactory.getCurrentSession().getNamedQuery(OCNBudgetDO.FIND_BY_OCN)
					.setParameter(CommonConstants.OCN, Long.parseLong(ocn))
					.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return ocnBudgetList;
	}
	
	@SuppressWarnings("unchecked")
	public List<OCNBudgetDO> retrieveById(Long id){
		 List<OCNBudgetDO> ocnBudgetList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(OCNBudgetDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, id)
					.list();
		} catch (Exception eException) {
			eException.printStackTrace();
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return ocnBudgetList;
	}
	
	public boolean update(OCNBudgetDO ocnBudgetDO) {
		boolean updateStatus = true;
		try {
			dao.merge(ocnBudgetDO);
		} catch (Exception eException) {
		} finally {
		}
		return updateStatus;
	}
	
	public boolean persistList(List<OCNBudgetDO> ocnBudgetList){
		try {
			for (OCNBudgetDO ocnBudget : ocnBudgetList) {
				dao.persist(ocnBudget);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}

}
