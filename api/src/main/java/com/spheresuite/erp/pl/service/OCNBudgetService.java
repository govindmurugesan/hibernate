package com.spheresuite.erp.pl.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.domainobject.OCNBudgetDO;
import com.spheresuite.erp.pl.dao.OCNBudgetDAO;

@Service
@Transactional
public class OCNBudgetService{
	
	@Autowired
	private OCNBudgetDAO ocnBudgetDAO;

	@Transactional
	public boolean persist(OCNBudgetDO ocnBudgetDO) {
		return ocnBudgetDAO.persist(ocnBudgetDO);
	}
	
	@Transactional
	public List<OCNBudgetDO> retrieve() {
		return ocnBudgetDAO.retrieve();
	}

	@Transactional
	public boolean update(OCNBudgetDO ocnBudgetDO) {
		return ocnBudgetDAO.update(ocnBudgetDO);
	}
	
	@Transactional
	public List<OCNBudgetDO> retrieveActive(){
		return ocnBudgetDAO.retrieveActive();
	}
	
	@Transactional
	public List<OCNBudgetDO> retrieveOCN(String ocn){
		return ocnBudgetDAO.retrieveOCN(ocn);
	}
	
	@Transactional
	public List<OCNBudgetDO> retrieveById(Long id){
		return ocnBudgetDAO.retrieveById(id);
	}
	
	@Transactional
	public boolean persistList(List<OCNBudgetDO> ocnBudgetDO) {
		return ocnBudgetDAO.persistList(ocnBudgetDO);
	}
}
