package com.spheresuite.erp.pl.rs;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.OCNBudgetDO;
import com.spheresuite.erp.domainobject.OCNDO;
import com.spheresuite.erp.domainobject.OCNReportDO;
import com.spheresuite.erp.pl.service.OCNBudgetService;
import com.spheresuite.erp.pl.service.OCNDetailsService;
import com.spheresuite.erp.pl.service.OCNReportService;
import com.spheresuite.erp.pl.service.OCNService;
import com.spheresuite.erp.pl.web.util.OCNReportUtil;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/ocnreport")
public class OCNReportRS {

	String validation = null;
	static Logger logger = Logger.getLogger(OCNReportRS.class.getName());
	
	@Autowired
	private OCNReportService ocnReportService;
	
	@Autowired
	private OCNDetailsService ocnDetailsService;
	
	@Autowired
	private OCNService ocnService;
	
	@Autowired
	private OCNBudgetService ocnBudgetService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				OCNReportDO ocnReportDO = new OCNReportDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.OCN) != null && !inputJSON.get(CommonConstants.OCN).toString().isEmpty()){
			 			List<OCNDO> ocnList = ocnService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.OCN).toString()));
			 			if(ocnList != null && ocnList.size() > 0){
			 				ocnReportDO.setOcnDetails(ocnList.get(0));
			 			}
			 		}
			 		if(inputJSON.get(CommonConstants.GREIGE_FABRIC) != null && !inputJSON.get(CommonConstants.GREIGE_FABRIC).toString().isEmpty()){
			 			ocnReportDO.setGreigeFabric(Double.parseDouble(inputJSON.get(CommonConstants.GREIGE_FABRIC).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.DYEING_COST) != null && !inputJSON.get(CommonConstants.DYEING_COST).toString().isEmpty()){
			 			ocnReportDO.setDyeingCost(Double.parseDouble(inputJSON.get(CommonConstants.DYEING_COST).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.FINISHING_COST) != null && !inputJSON.get(CommonConstants.FINISHING_COST).toString().isEmpty()){
			 			ocnReportDO.setFinishingCost(Double.parseDouble(inputJSON.get(CommonConstants.FINISHING_COST).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.TRIMS) != null && !inputJSON.get(CommonConstants.TRIMS).toString().isEmpty()){
			 			ocnReportDO.setTrims(Double.parseDouble(inputJSON.get(CommonConstants.TRIMS).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.SAM) != null && !inputJSON.get(CommonConstants.SAM).toString().isEmpty()){
			 			ocnReportDO.setSam(Double.parseDouble(inputJSON.get(CommonConstants.SAM).toString()));
			 		}
			 		if(inputJSON.get(CommonConstants.REJECTION) != null && !inputJSON.get(CommonConstants.REJECTION).toString().isEmpty()){
			 			ocnReportDO.setRejection(Double.parseDouble(inputJSON.get(CommonConstants.REJECTION).toString()));
			 		}
			 		ocnReportDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			ocnReportDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			ocnReportDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 	}
				if(!ocnReportService.persist(ocnReportDO)){
					return CommonWebUtil.buildErrorResponse("OCN Report Already Added").toString();
				}
				CommonUtil.persistRecentAcvtivity(inputJSON.get(CommonConstants.UPDATED_BY).toString(), "New OCN Report Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<OCNReportDO> ocnReportList = ocnReportService.retrieve();
				respJSON = OCNReportUtil.getOCNReportList(ocnReportList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveByStatus/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByStatus(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
					List<OCNReportDO> ocnReportList = ocnReportService.retrieveByStatus(inputJSON.get(CommonConstants.STATUS).toString());
					respJSON = OCNReportUtil.getOCNReportList(ocnReportList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveOCN/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveOCN(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && inputJSON.get(CommonConstants.OCN) != null && !inputJSON.get(CommonConstants.OCN).toString().isEmpty()){
					List<OCNReportDO> ocnReportList = ocnReportService.retrieveOCN(Long.parseLong(inputJSON.get(CommonConstants.OCN).toString()));
					respJSON = OCNReportUtil.getOCNReportList(ocnReportList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				OCNReportDO ocnReportDO = new OCNReportDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<OCNReportDO> ocnReportList = ocnReportService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(ocnReportList != null && ocnReportList.size() > 0){
				 		ocnReportDO = ocnReportList.get(0);
				 		if(inputJSON.get(CommonConstants.OCN) != null && !inputJSON.get(CommonConstants.OCN).toString().isEmpty()){
				 			List<OCNDO> ocnList = ocnService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.OCN).toString()));
				 			if(ocnList != null && ocnList.size() > 0){
				 				ocnReportDO.setOcnDetails(ocnList.get(0));
				 			}
				 		}
				 		if(inputJSON.get(CommonConstants.GREIGE_FABRIC) != null && !inputJSON.get(CommonConstants.GREIGE_FABRIC).toString().isEmpty()){
				 			ocnReportDO.setGreigeFabric(Double.parseDouble(inputJSON.get(CommonConstants.GREIGE_FABRIC).toString()));
				 		}
				 		if(inputJSON.get(CommonConstants.DYEING_COST) != null && !inputJSON.get(CommonConstants.DYEING_COST).toString().isEmpty()){
				 			ocnReportDO.setDyeingCost(Double.parseDouble(inputJSON.get(CommonConstants.DYEING_COST).toString()));
				 		}
				 		if(inputJSON.get(CommonConstants.FINISHING_COST) != null && !inputJSON.get(CommonConstants.FINISHING_COST).toString().isEmpty()){
				 			ocnReportDO.setFinishingCost(Double.parseDouble(inputJSON.get(CommonConstants.FINISHING_COST).toString()));
				 		}
				 		if(inputJSON.get(CommonConstants.TRIMS) != null && !inputJSON.get(CommonConstants.TRIMS).toString().isEmpty()){
				 			ocnReportDO.setTrims(Double.parseDouble(inputJSON.get(CommonConstants.TRIMS).toString()));
				 		}
				 		if(inputJSON.get(CommonConstants.SAM) != null && !inputJSON.get(CommonConstants.SAM).toString().isEmpty()){
				 			ocnReportDO.setSam(Double.parseDouble(inputJSON.get(CommonConstants.SAM).toString()));
				 		}
				 		if(inputJSON.get(CommonConstants.REJECTION) != null && !inputJSON.get(CommonConstants.REJECTION).toString().isEmpty()){
				 			ocnReportDO.setRejection(Double.parseDouble(inputJSON.get(CommonConstants.REJECTION).toString()));
				 		}
				 		ocnReportDO.setUpdatedon(new Date());
				 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			ocnReportDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		ocnReportService.update(ocnReportDO);
				 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New OCN Report Updated");
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/updateAll", method = RequestMethod.POST)
	public @ResponseBody String updateAll(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					List<OCNReportDO> ocnReportList = ocnReportService.retrieveWithStatus();
					if(ocnReportList != null && ocnReportList.size() > 0) {
						for(int i = 0; i<ocnReportList.size(); i++) {
							OCNReportDO ocnReportDO =  new OCNReportDO();
							ocnReportDO = ocnReportList.get(i);
							List<OCNBudgetDO> ocnBudgetList = ocnBudgetService.retrieveOCN(ocnReportDO.getOcnDetails().getOcnId().toString());
							if(ocnBudgetList != null && ocnBudgetList.size() > 0){
								ocnReportDO.setOcnDetails(ocnBudgetList.get(0).getOcnDetails());
								ocnReportDO.setOcnBudget(ocnBudgetList.get(0));
								Double totalAmt = 0D;
								if(ocnBudgetList.get(0).getGreigeFabric() != null && ocnReportDO.getGreigeFabric() != null){
									ocnReportDO.setGreigeFabricQty(String.valueOf(ocnBudgetList.get(0).getGreigeFabric()-ocnReportDO.getGreigeFabric()));
									if(ocnReportDO.getGreigeFabricm() != null){
										Double greigeAmt = (Double.valueOf(ocnReportDO.getGreigeFabricQty())* ocnReportDO.getGreigeFabricm())/Math.pow(10, 5);
										ocnReportDO.setGreigeFabricAmount(String.valueOf(greigeAmt));
										totalAmt += greigeAmt;
									}
								}
								if(ocnBudgetList.get(0).getDyeingCost() != null && ocnReportDO.getDyeingCost() != null){
									ocnReportDO.setDyeingCostQty(String.valueOf(ocnBudgetList.get(0).getDyeingCost()-ocnReportDO.getDyeingCost()));
									Double dyeingAmt = ((Double.valueOf(ocnBudgetList.get(0).getGreigeFabric())* ocnBudgetList.get(0).getDyeingCost())-(Double.valueOf(ocnReportDO.getGreigeFabric())* ocnReportDO.getDyeingCost()))/Math.pow(10, 5);
									ocnReportDO.setDyeingAmount(String.valueOf(dyeingAmt));
									totalAmt += dyeingAmt;
								}
								if(ocnBudgetList.get(0).getFinishingCost() != null && ocnReportDO.getFinishingCost() != null){
									ocnReportDO.setFinishingCostQty(String.valueOf(ocnBudgetList.get(0).getFinishingCost()-ocnReportDO.getFinishingCost()));
									Double finishingAmt = ((Double.valueOf(ocnBudgetList.get(0).getGreigeFabric())* ocnBudgetList.get(0).getFinishingCost())-(Double.valueOf(ocnReportDO.getGreigeFabric())* ocnReportDO.getFinishingCost()))/Math.pow(10, 5);
									ocnReportDO.setFinishingAmount(String.valueOf(finishingAmt));
									totalAmt += finishingAmt;
								}
								if(ocnBudgetList.get(0).getSam() != null && ocnReportDO.getSam() != null){
									ocnReportDO.setSamQty(String.valueOf(ocnBudgetList.get(0).getSam()-ocnReportDO.getSam()));
									Double samAmt = (Double.valueOf(ocnReportDO.getSamQty())*ocnBudgetList.get(0).getGreigeFabric()*Double.valueOf(ocnBudgetList.get(0).getCpm().getAmount()))/Math.pow(10, 5);
									ocnReportDO.setSamAmount(String.valueOf(samAmt));
									totalAmt += samAmt;
								}
								if(ocnBudgetList.get(0).getTrims() != null && ocnReportDO.getTrims() != null){
									ocnReportDO.setTrimsQty(String.valueOf(ocnBudgetList.get(0).getTrims()-ocnReportDO.getTrims()));
									if(ocnReportDO.getTrimsAmount() != null){
										totalAmt += Double.valueOf(ocnReportDO.getTrimsAmount());
									}
								}
								if(ocnBudgetList.get(0).getRejection() != null && ocnReportDO.getRejection() != null){
									ocnReportDO.setRejectionQty(String.valueOf(ocnBudgetList.get(0).getRejection()-ocnReportDO.getRejection()));
									Double rejectionAmt = (Double.valueOf(ocnReportDO.getRejectionQty())*ocnReportDO.getSam()*Double.valueOf(ocnBudgetList.get(0).getCpm().getAmount()))/Math.pow(10, 5);
									ocnReportDO.setRejectionAmount(String.valueOf(rejectionAmt));
									totalAmt += rejectionAmt;
								}
								if(ocnBudgetList.get(0).getExportIncentive() != null && ocnReportDO.getInvoiceAmount() != null){
									Double inv = (Double.valueOf(ocnReportDO.getInvoiceAmount())*(1+(Double.valueOf(ocnBudgetList.get(0).getExportIncentive())/100)))/Math.pow(10, 5);
									DecimalFormat df = new DecimalFormat("#.00"); 
									String invTotal = df.format(inv);
									Double tot = ((Double.valueOf(totalAmt)*Math.pow(10, 5))/(Double.valueOf(invTotal)*Math.pow(10, 5)));
									ocnReportDO.setSalesPercentage(tot.toString());
								}
								if(totalAmt > 0){
									ocnReportDO.setAmount(totalAmt.toString());
									ocnReportDO.setStatus("p");
								}else{
									ocnReportDO.setAmount(totalAmt.toString());
									ocnReportDO.setStatus("n");
								}
								if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
									ocnReportDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
						 		}
								ocnReportDO.setUpdatedon(new Date());
								ocnReportService.update(ocnReportDO);
							}
						}
					}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
}
