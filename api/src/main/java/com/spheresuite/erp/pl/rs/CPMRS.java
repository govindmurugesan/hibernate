package com.spheresuite.erp.pl.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.CPMDO;
import com.spheresuite.erp.pl.service.CPMService;
import com.spheresuite.erp.pl.web.util.CPMUtil;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/cpm")
public class CPMRS {

	String validation = null;
	static Logger logger = Logger.getLogger(CPMRS.class.getName());
	
	@Autowired
	private CPMService cpmService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				CPMDO cpmDO = new CPMDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.MONTH) != null && !inputJSON.get(CommonConstants.MONTH).toString().isEmpty()){
			 			cpmDO.setMonth(inputJSON.get(CommonConstants.MONTH).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.AMOUNT) != null && !inputJSON.get(CommonConstants.AMOUNT).toString().isEmpty()){
			 			cpmDO.setAmount(inputJSON.get(CommonConstants.AMOUNT).toString());
			 		}
			 		cpmDO.setUpdatedon(new Date());
			 		cpmDO.setCreatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			cpmDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			cpmDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		cpmDO.setStatus(CommonConstants.ASTATUS);
			 	}
				if(!cpmService.persist(cpmDO)){
					return CommonWebUtil.buildErrorResponse("Cost Per Minute Details Already Added").toString();
				}
				CommonUtil.persistRecentAcvtivity(inputJSON.get(CommonConstants.UPDATED_BY).toString(), "New Cost Per Minute Details Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<CPMDO> cpmList = cpmService.retrieveActive();
				respJSON = CPMUtil.getCPMList(cpmList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveByMonth/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByMonth(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && inputJSON.get(CommonConstants.MONTH) != null && !inputJSON.get(CommonConstants.MONTH).toString().isEmpty()){
					List<CPMDO> cpmList = cpmService.retrieveByMonth(inputJSON.get(CommonConstants.MONTH).toString());
					respJSON = CPMUtil.getCPMList(cpmList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<CPMDO> cpmList = cpmService.retrieve();
				respJSON = CPMUtil.getCPMList(cpmList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				CPMDO cpmDO = new CPMDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<CPMDO> cpmList = cpmService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(cpmList != null && cpmList.size() > 0){
				 		cpmDO = cpmList.get(0);
				 		if(inputJSON.get(CommonConstants.MONTH) != null && !inputJSON.get(CommonConstants.MONTH).toString().isEmpty()){
				 			cpmDO.setMonth(inputJSON.get(CommonConstants.MONTH).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.AMOUNT) != null && !inputJSON.get(CommonConstants.AMOUNT).toString().isEmpty()){
				 			cpmDO.setAmount(inputJSON.get(CommonConstants.AMOUNT).toString());
				 		}
				 		cpmDO.setUpdatedon(new Date());
				 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			cpmDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
				 			cpmDO.setStatus((inputJSON.get(CommonConstants.STATUS).toString()));
				 		}
				 		if(!cpmService.update(cpmDO)){
							return CommonWebUtil.buildErrorResponse("Cost Per Minute Details Already Added").toString();
						}
				 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Cost Per Minute Details Updated");
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/importCPM", method = RequestMethod.POST)
	public @ResponseBody String importCPM(Model model, HttpServletRequest request) {
		try {
			JSONObject inputJSON =  CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
			JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
			List<CPMDO> CPMList = new ArrayList<CPMDO>();
			Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY);
			for (int i=0; i < fileData.size(); i++){
				JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
				CPMDO cpmDO = new CPMDO();
				if (colName.get(CommonConstants.MONTH) != null && !colName.get(CommonConstants.MONTH).toString().isEmpty()){
					cpmDO.setMonth(rowJSON.get(colName.get(CommonConstants.MONTH)).toString());
		 		}
				if (colName.get(CommonConstants.AMOUNT) != null && !colName.get(CommonConstants.AMOUNT).toString().isEmpty()){
					cpmDO.setAmount(rowJSON.get(colName.get(CommonConstants.AMOUNT)).toString());
		 		}
		 		if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
		 			cpmDO.setUpdatedby(updatedBy.toString());
		 			cpmDO.setCreatedby(updatedBy.toString());
		 		}
		 		cpmDO.setUpdatedon(new Date());
		 		cpmDO.setCreatedon(new Date());
		 		cpmDO.setStatus(CommonConstants.ASTATUS);
		 		CPMList.add(cpmDO);
			}
			cpmService.persistList(CPMList);
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
