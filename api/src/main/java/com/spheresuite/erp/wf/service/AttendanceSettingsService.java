package com.spheresuite.erp.wf.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spheresuite.erp.domainobject.AttendanceSettingsDO;
import com.spheresuite.erp.wf.dao.AttendanceSettingsDAO;

@Service
@Transactional
public class AttendanceSettingsService {
	static Logger logger = Logger.getLogger(AttendanceSettingsService.class.getName());
	@Autowired
	private AttendanceSettingsDAO attendanceSettingsDAO;

	@Transactional
	public boolean persist(AttendanceSettingsDO attendanceSettingsDO)  {
		return attendanceSettingsDAO.persist(attendanceSettingsDO);
	}

	@Transactional
	public List<AttendanceSettingsDO> retrieveById(Long Id)  {
		return attendanceSettingsDAO.retrieveById(Id);
	}

	@Transactional
	public List<AttendanceSettingsDO> retrieveByMailType(String databaseType)  {
		return attendanceSettingsDAO.retrieveByMailType(databaseType);
	}

	@Transactional
	public List<AttendanceSettingsDO> retrieve()  {
		return attendanceSettingsDAO.retrieve();
	}
	
	@Transactional
	public boolean testConnection(AttendanceSettingsDO attendanceSettingsDO)  {
		return attendanceSettingsDAO.testConnection(attendanceSettingsDO);
	}
	
	@Transactional
	public List<AttendanceSettingsDO> retrieveActive()  {
		return attendanceSettingsDAO.retrieveActive();
	}

	@Transactional
	public boolean update(AttendanceSettingsDO attendanceSettingsDO)  {
		return attendanceSettingsDAO.update(attendanceSettingsDO);
	}
	
	@Transactional
	public boolean persistList(List<AttendanceSettingsDO> attendanceSettingsDO) {
		return attendanceSettingsDAO.persistList(attendanceSettingsDO);
	}
}
