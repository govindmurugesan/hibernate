package com.spheresuite.erp.wf.web.util;

import java.io.UnsupportedEncodingException;
import java.util.Base64;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.spheresuite.erp.domainobject.AttendanceSettingsDO;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;

@Component
public class AttendanceSettingsUtil {
	
	private AttendanceSettingsUtil() {}
	
	public static JSONObject getAttendanceSettingsList(List<AttendanceSettingsDO> attendanceSettingsDOList) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (AttendanceSettingsDO attendanceSettingsDO : attendanceSettingsDOList) {
				resultJSONArray.put(getAttendanceSettingsObject(attendanceSettingsDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getAttendanceSettingsObject(AttendanceSettingsDO attendanceSettingsDO)throws JSONException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(attendanceSettingsDO.getAttendanceSettingId()));
		result.put(CommonConstants.DATABASE_TYPE, String.valueOf(attendanceSettingsDO.getDatabaseType()));
		result.put(CommonConstants.HOST, String.valueOf(attendanceSettingsDO.getHost()));
		result.put(CommonConstants.PORT, String.valueOf(attendanceSettingsDO.getPort()));
		result.put(CommonConstants.VENDOR, String.valueOf(attendanceSettingsDO.getVendor()));
		result.put(CommonConstants.DATABASE_NAME, String.valueOf(attendanceSettingsDO.getDatabaseName()));
		result.put(CommonConstants.USERNAME, String.valueOf(attendanceSettingsDO.getUsername()));
		byte[] decodedBytes = Base64.getDecoder().decode(attendanceSettingsDO.getPassword());
		String stringDecode = null;
		try {
			stringDecode = new String(decodedBytes, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String pwd = stringDecode.toString();
		result.put(CommonConstants.PASSWORD, String.valueOf(pwd));
		result.put(CommonConstants.STATUS, String.valueOf(attendanceSettingsDO.getStatus()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(attendanceSettingsDO.getUpdatedon())));
		if(attendanceSettingsDO.getUpdatedby() != null){
			String empName = CommonUtil.getEmpObject(attendanceSettingsDO.getUpdatedby());
			result.put(CommonConstants.UPDATED_BY, empName); 
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		return result;
	}
}
