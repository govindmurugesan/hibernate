package com.spheresuite.erp.wf.dao;

import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Base64;
import java.util.List;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spheresuite.erp.dao.GenericDAOImpl;
import com.spheresuite.erp.domainobject.AttendanceSettingsDO;
import com.spheresuite.erp.util.CommonConstants;
@Repository
public class AttendanceSettingsDAO {
	static Logger logger = Logger.getLogger(AttendanceSettingsDAO.class.getName());
	
	@Autowired
	private GenericDAOImpl<AttendanceSettingsDO> genericObject;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public boolean persist(AttendanceSettingsDO attendanceSettingsDO) {
		boolean persistStatus = true;
		try {
			List<AttendanceSettingsDO> attendanceSettingsDOList = null;
			attendanceSettingsDOList = this.sessionFactory.getCurrentSession().getNamedQuery(AttendanceSettingsDO.FIND_BY_NAME)
					.setParameter(CommonConstants.DATABASE_TYPE, attendanceSettingsDO.getDatabaseType())
					.list();
					if(attendanceSettingsDOList != null && attendanceSettingsDOList.size() > 0){
						persistStatus = false;
					}else{
						genericObject.persist(attendanceSettingsDO);
					}
		} catch (Exception eException) {
		} finally {
		}
		return persistStatus;
	}
	
	
	public List<AttendanceSettingsDO> retrieve() {
		List<AttendanceSettingsDO> attendanceSettingsList = null;
		try {
			attendanceSettingsList = genericObject.retrieve(AttendanceSettingsDO.class);
		} catch (Exception eException) {
		} finally {
		}
		return attendanceSettingsList;
	}
	
	public boolean testConnection(AttendanceSettingsDO attendanceSettingsDO) {
		boolean persistStatus = true;
		Connection conn=null;
		try {
			byte[] decodedBytes = Base64.getDecoder().decode(attendanceSettingsDO.getPassword());
			String stringDecode = null;
			try {
				stringDecode = new String(decodedBytes, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			String pwd = stringDecode.toString();
			Class.forName(CommonConstants.MSSQL_DRIVER).newInstance();
			
			//mysql CommonConstants.MYSQL_URL_BASE+attendanceSettingsDO.getHost()+CommonConstants.COLON+attendanceSettingsDO.getPort()+
			//CommonConstants.SLASH+attendanceSettingsDO.getDatabaseName(),attendanceSettingsDO.getUsername(),pwd
			System.out.println(  
					CommonConstants.MYSQL_URL_BASE + attendanceSettingsDO.getHost() + CommonConstants.COLON + attendanceSettingsDO.getPort() +
        			CommonConstants.SEMI_COLON + CommonConstants.DATABASE_NAME + CommonConstants.EQUAL + attendanceSettingsDO.getDatabaseName() + "    " + attendanceSettingsDO.getUsername() + "    " + pwd);
            conn = DriverManager.getConnection(  
            		CommonConstants.MYSQL_URL_BASE + attendanceSettingsDO.getHost() + CommonConstants.COLON + attendanceSettingsDO.getPort() +
            			CommonConstants.SEMI_COLON + CommonConstants.DATABASE_NAME + CommonConstants.EQUAL + attendanceSettingsDO.getDatabaseName(), attendanceSettingsDO.getUsername(), pwd);

            if(conn!=null)
            	persistStatus = true;
           /* Statement st = conn.createStatement();
            String code = " Create or Replace VIEW userView As select * from user";

            st.executeUpdate(code);*/
            System.out.println(" Connected Succesfull");
		} catch (Exception eException) {
			persistStatus = false;
			eException.printStackTrace();
		} finally {
		}
		return persistStatus;
	}
	
	@SuppressWarnings("unchecked")
	public List<AttendanceSettingsDO> retrieveActive() {
		List<AttendanceSettingsDO> EmailSettingsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(AttendanceSettingsDO.FIND_BY_STATUS)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return EmailSettingsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<AttendanceSettingsDO> retrieveById(Long Id) {
		List<AttendanceSettingsDO> EmailSettingsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(AttendanceSettingsDO.FIND_BY_ID)
					.setParameter(CommonConstants.ID, Id)
					.list();
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return EmailSettingsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<AttendanceSettingsDO> retrieveByMailType(String mailtype) {
		List<AttendanceSettingsDO> EmailSettingsList = null;
		try {
			return this.sessionFactory.getCurrentSession().getNamedQuery(AttendanceSettingsDO.FIND_BY_DATABASETYPE)
					.setParameter(CommonConstants.MAILTYPE, mailtype)
					.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE)
					.list(); 
		} catch (Exception eException) {
		} finally {
			this.sessionFactory.getCurrentSession().flush();
			this.sessionFactory.getCurrentSession().clear();
		}
		return EmailSettingsList;
	}
	
	@SuppressWarnings("unchecked")
	public boolean update(AttendanceSettingsDO attendanceSettingsDO) {
		try {
			List<AttendanceSettingsDO> attendanceSettingsDOList = null;
			attendanceSettingsDOList = this.sessionFactory.getCurrentSession().getNamedQuery(AttendanceSettingsDO.FIND_FOR_UPDATE)
					.setParameter(CommonConstants.DATABASE_TYPE, attendanceSettingsDO.getDatabaseType())
					.setParameter(CommonConstants.ID, attendanceSettingsDO.getAttendanceSettingId())
					.list();
			if(attendanceSettingsDOList != null && attendanceSettingsDOList.size() > 0){
				return false;
			}else{
				genericObject.merge(attendanceSettingsDO);
				return true;
			}
		} catch (Exception eException) {
			return true;
		} finally {
		}
	}
	
	public boolean persistList(List<AttendanceSettingsDO> attendanceSettingsList){
		try {
			for (AttendanceSettingsDO emailSettings : attendanceSettingsList) {
				genericObject.persist(emailSettings);
			}
		} catch (Exception eException) {
		} finally {
		}
		return true;
	}
}
