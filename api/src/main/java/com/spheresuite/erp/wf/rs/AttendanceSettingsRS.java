package com.spheresuite.erp.wf.rs;

import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.AttendanceSettingsDO;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;
import com.spheresuite.erp.wf.service.AttendanceSettingsService;
import com.spheresuite.erp.wf.web.util.AttendanceSettingsUtil;

@Controller
@RequestMapping(value = "/attendanceSettings")
public class AttendanceSettingsRS {

	String validation = null;
	static Logger logger = Logger.getLogger(AttendanceSettingsRS.class.getName());

	@Autowired
	private AttendanceSettingsService attendanceSettingsService;
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				AttendanceSettingsDO attendanceSettingsDO = new AttendanceSettingsDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.DATABASE_TYPE) != null && !inputJSON.get(CommonConstants.DATABASE_TYPE).toString().isEmpty()){
			 			attendanceSettingsDO.setDatabaseType(inputJSON.get(CommonConstants.DATABASE_TYPE).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.DATABASE_NAME) != null && !inputJSON.get(CommonConstants.DATABASE_NAME).toString().isEmpty()){
			 			attendanceSettingsDO.setDatabaseName(inputJSON.get(CommonConstants.DATABASE_NAME).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.HOST) != null && !inputJSON.get(CommonConstants.HOST).toString().isEmpty()){
			 			attendanceSettingsDO.setHost(inputJSON.get(CommonConstants.HOST).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.PORT) != null && !inputJSON.get(CommonConstants.PORT).toString().isEmpty()){
			 			attendanceSettingsDO.setPort(inputJSON.get(CommonConstants.PORT).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.VENDOR) != null && !inputJSON.get(CommonConstants.VENDOR).toString().isEmpty()){
			 			attendanceSettingsDO.setVendor(inputJSON.get(CommonConstants.VENDOR).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.USERNAME) != null && !inputJSON.get(CommonConstants.USERNAME).toString().isEmpty()){
			 			attendanceSettingsDO.setUsername(inputJSON.get(CommonConstants.USERNAME).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.PASSWORD) != null && !inputJSON.get(CommonConstants.PASSWORD).toString().isEmpty()){
			 			String pwd = inputJSON.get(CommonConstants.PASSWORD).toString();
			 			attendanceSettingsDO.setPassword(Base64.getEncoder().encodeToString(pwd.getBytes("UTF-8")));
			 		}
			 		attendanceSettingsDO.setStatus(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()?inputJSON.get(CommonConstants.STATUS).toString():"");
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			attendanceSettingsDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			attendanceSettingsDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		attendanceSettingsDO.setUpdatedon(new Date());
			 		attendanceSettingsDO.setCreatedon(new Date());
			 	}
				if(!attendanceSettingsService.persist(attendanceSettingsDO)){
					return CommonWebUtil.buildErrorResponse("Attendance Settings Already Added").toString();
				}
				CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Attendance Settings Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<AttendanceSettingsDO> attendanceSettingsList = attendanceSettingsService.retrieve();
				respJSON = AttendanceSettingsUtil.getAttendanceSettingsList(attendanceSettingsList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/testConnection", method = RequestMethod.POST)
	public @ResponseBody String testConnection(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				AttendanceSettingsDO attendanceSettingsDO = new AttendanceSettingsDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		if(inputJSON.get(CommonConstants.DATABASE_TYPE) != null && !inputJSON.get(CommonConstants.DATABASE_TYPE).toString().isEmpty()){
			 			attendanceSettingsDO.setDatabaseType(inputJSON.get(CommonConstants.DATABASE_TYPE).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.DATABASE_NAME) != null && !inputJSON.get(CommonConstants.DATABASE_NAME).toString().isEmpty()){
			 			attendanceSettingsDO.setDatabaseName(inputJSON.get(CommonConstants.DATABASE_NAME).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.HOST) != null && !inputJSON.get(CommonConstants.HOST).toString().isEmpty()){
			 			attendanceSettingsDO.setHost(inputJSON.get(CommonConstants.HOST).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.PORT) != null && !inputJSON.get(CommonConstants.PORT).toString().isEmpty()){
			 			attendanceSettingsDO.setPort(inputJSON.get(CommonConstants.PORT).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.VENDOR) != null && !inputJSON.get(CommonConstants.VENDOR).toString().isEmpty()){
			 			attendanceSettingsDO.setVendor(inputJSON.get(CommonConstants.VENDOR).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.USERNAME) != null && !inputJSON.get(CommonConstants.USERNAME).toString().isEmpty()){
			 			attendanceSettingsDO.setUsername(inputJSON.get(CommonConstants.USERNAME).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.PASSWORD) != null && !inputJSON.get(CommonConstants.PASSWORD).toString().isEmpty()){
			 			String pwd = inputJSON.get(CommonConstants.PASSWORD).toString();
			 			attendanceSettingsDO.setPassword(Base64.getEncoder().encodeToString(pwd.getBytes("UTF-8")));
			 		}
			 		attendanceSettingsDO.setStatus(CommonConstants.STATUS);
			 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			attendanceSettingsDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			attendanceSettingsDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		attendanceSettingsDO.setUpdatedon(new Date());
			 		attendanceSettingsDO.setCreatedon(new Date());
			 	}
				if(attendanceSettingsService.testConnection(attendanceSettingsDO))
					respJSON = CommonWebUtil.buildSuccessResponse().toString();
				else
					respJSON = CommonWebUtil.buildErrorResponse("").toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				AttendanceSettingsDO attendanceSettingsDO = new AttendanceSettingsDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<AttendanceSettingsDO> attendanceSettingsList = attendanceSettingsService.retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(attendanceSettingsList != null && attendanceSettingsList.size() > 0){
				 		attendanceSettingsDO = attendanceSettingsList.get(0);
				 		if(inputJSON.get(CommonConstants.DATABASE_TYPE) != null && !inputJSON.get(CommonConstants.DATABASE_TYPE).toString().isEmpty()){
				 			attendanceSettingsDO.setDatabaseType(inputJSON.get(CommonConstants.DATABASE_TYPE).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.DATABASE_NAME) != null && !inputJSON.get(CommonConstants.DATABASE_NAME).toString().isEmpty()){
				 			attendanceSettingsDO.setDatabaseName(inputJSON.get(CommonConstants.DATABASE_NAME).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.HOST) != null && !inputJSON.get(CommonConstants.HOST).toString().isEmpty()){
				 			attendanceSettingsDO.setHost(inputJSON.get(CommonConstants.HOST).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.PORT) != null && !inputJSON.get(CommonConstants.PORT).toString().isEmpty()){
				 			attendanceSettingsDO.setPort(inputJSON.get(CommonConstants.PORT).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.VENDOR) != null && !inputJSON.get(CommonConstants.VENDOR).toString().isEmpty()){
				 			attendanceSettingsDO.setVendor(inputJSON.get(CommonConstants.VENDOR).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.USERNAME) != null && !inputJSON.get(CommonConstants.USERNAME).toString().isEmpty()){
				 			attendanceSettingsDO.setUsername(inputJSON.get(CommonConstants.USERNAME).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.PASSWORD) != null && !inputJSON.get(CommonConstants.PASSWORD).toString().isEmpty()){
				 			String pwd = inputJSON.get(CommonConstants.PASSWORD).toString();
				 			attendanceSettingsDO.setPassword(Base64.getEncoder().encodeToString(pwd.getBytes("UTF-8")));
				 		}
				 		attendanceSettingsDO.setUpdatedon(new Date());
				 		if(inputJSON.get(CommonConstants.UPDATED_BY) != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			attendanceSettingsDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.STATUS) != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
				 			attendanceSettingsDO.setStatus(inputJSON.get(CommonConstants.STATUS).toString());
				 		}
				 		if(!attendanceSettingsService.update(attendanceSettingsDO)){
							return CommonWebUtil.buildErrorResponse("Attendance Settings Already Added").toString();
						}
				 		CommonUtil.persistRecentAcvtivity((inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Attendance Settings Updated");
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/importAttendanceSettings", method = RequestMethod.POST)
	public @ResponseBody String importAttendanceSettings(Model model, HttpServletRequest request) {
		try {
			JSONObject inputJSON =  CommonWebUtil.getInputParams(request.getParameter(CommonConstants.INPUT_PARAMS).toString());
			org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
			JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
			List<AttendanceSettingsDO> attendanceSettingsList = new ArrayList<AttendanceSettingsDO>();
			Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY);
			for (int i=0; i < fileData.size(); i++){
				JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
				AttendanceSettingsDO attendanceSettingsDO = new AttendanceSettingsDO();
				
				if(colName.get(CommonConstants.DATABASE_TYPE) != null && !colName.get(CommonConstants.DATABASE_TYPE).toString().isEmpty()){
		 			attendanceSettingsDO.setDatabaseType(rowJSON.get(colName.get(CommonConstants.DATABASE_TYPE)).toString());
		 		}
		 		if(colName.get(CommonConstants.HOST) != null && !colName.get(CommonConstants.HOST).toString().isEmpty()){
		 			attendanceSettingsDO.setHost(rowJSON.get(colName.get(CommonConstants.HOST)).toString());
		 		}
		 		if(colName.get(CommonConstants.PORT) != null && !colName.get(CommonConstants.PORT).toString().isEmpty()){
		 			attendanceSettingsDO.setPort(rowJSON.get(colName.get(CommonConstants.PORT)).toString());
		 		}
		 		if(colName.get(CommonConstants.DATABASE_NAME) != null && !colName.get(CommonConstants.DATABASE_NAME).toString().isEmpty()){
		 			attendanceSettingsDO.setDatabaseName(rowJSON.get(colName.get(CommonConstants.DATABASE_NAME)).toString());
		 		}
		 		if(colName.get(CommonConstants.VENDOR) != null && !colName.get(CommonConstants.VENDOR).toString().isEmpty()){
		 			attendanceSettingsDO.setVendor(rowJSON.get(colName.get(CommonConstants.VENDOR)).toString());
		 		}
		 		if(colName.get(CommonConstants.USERNAME) != null && !colName.get(CommonConstants.USERNAME).toString().isEmpty()){
		 			attendanceSettingsDO.setUsername(rowJSON.get(colName.get(CommonConstants.USERNAME)).toString());
		 		}
		 		if(colName.get(CommonConstants.PASSWORD) != null && !colName.get(CommonConstants.PASSWORD).toString().isEmpty()){
		 			String pwd = rowJSON.get(colName.get(CommonConstants.PASSWORD)).toString();
		 			attendanceSettingsDO.setPassword(Base64.getEncoder().encodeToString(pwd.getBytes("UTF-8")));
		 		}
		 		if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
		 			attendanceSettingsDO.setUpdatedby(updatedBy.toString());
		 			attendanceSettingsDO.setCreatedby(updatedBy.toString());
		 		}
		 		attendanceSettingsDO.setUpdatedon(new Date());
		 		attendanceSettingsDO.setCreatedon(new Date());
		 		attendanceSettingsDO.setStatus(CommonConstants.ASTATUS);
		 		attendanceSettingsList.add(attendanceSettingsDO);
			}
			attendanceSettingsService.persistList(attendanceSettingsList);
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
