package testing;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.FetchProfile;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.UIDFolder;

import com.auth0.jwt.internal.org.apache.commons.lang3.ArrayUtils;
import com.spheresuite.erp.domainobject.LeadEmailDO;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.sun.mail.imap.IMAPFolder.FetchProfileItem;
public class MailTesting{
  private static final int BATCH_SIZE = 1;
  private static List<LeadEmailDO> leadEmailDOlist = new ArrayList<LeadEmailDO>();
   public static void main(String[] args) {
     //Set mail properties and configure accordingly
      String hostval = "192.168.5.200";
      String mailStrProt = "imap";
      String uname = "kumaravelt";
      String pwd = "KjHgF$1625";
    // Calling checkMail method to check received emails
      checkMail(hostval, mailStrProt, uname, pwd);
   }
   public static void checkMail(String hostval, String mailStrProt, String uname,String pwd) 
   {
      try {
    	  Properties props = System.getProperties();
    	  props.setProperty("mail.store.protocol", CommonConstants.EXCHANGE_IMAP);
      	  props.put("mail.imap.auth", "true");
    	  Session session = Session.getDefaultInstance(props, null);
    	  Store store = session.getStore(CommonConstants.EXCHANGE_IMAP);
    	  store.connect(hostval, uname, pwd);
    	  Folder inbox = null;
    	  inbox = store.getFolder("INBOX");
    	  int totalUnread = inbox.getMessageCount();
    	  if (totalUnread != 0)
    	  {
    	      inbox.open(Folder.READ_WRITE);
    	      int batchStart = 1;
    	      int batchEnd = (1 > BATCH_SIZE ? BATCH_SIZE
    	              : 1);
    	      int batchCount = 0;
    	      while (true)
    	      {
    	          processABatch(inbox, batchStart, batchEnd, batchCount);
    	         /* System.out.println("batchStart "+batchStart);
    	          System.out.println("batchEnd "+batchEnd);
    	          System.out.println("batchCount "+batchCount);*/
    	          batchStart = batchEnd + 1;
    	          if (batchStart > 1)
    	          {
    	              break;
    	          }
    	          batchEnd = ((batchEnd + BATCH_SIZE) < 1 ? (batchEnd + BATCH_SIZE)
    	                  : 1);
    	          batchCount++;
    	      }

    	  }
    	  inbox.close(true);
    	  store.close();
      } catch (NoSuchProviderException exp) {
         exp.printStackTrace();
      } catch (MessagingException exp) {
         exp.printStackTrace();
      } catch (Exception exp) {
         exp.printStackTrace();
      }
   }
   
   private static void processABatch(Folder inbox, int batchStart, int batchEnd, int batchCount)
	        throws MessagingException, IOException
	{
	    /*Message[] arrayMessages =  inbox.getMessages(batchStart, batchEnd);
	    ArrayUtils.reverse(arrayMessages);
        FetchProfile fp = new FetchProfile();
        fp.add(FetchProfile.Item.ENVELOPE);
        fp.add(FetchProfileItem.FLAGS);
        fp.add(FetchProfileItem.CONTENT_INFO);
        fp.add("X-mailer");
        if (inbox instanceof UIDFolder) {
        	fp.add(UIDFolder.FetchProfileItem.UID);
        }
        inbox.fetch(arrayMessages, fp);
	    for (int i = 0; i < arrayMessages.length; i++)
	    {
	        //System.out.println("i = "+i);
	    }*/
	   UIDFolder uf = (UIDFolder)inbox; // cast folder to UIDFolder interface
       /*if(!inbox.isOpen()){
       	inbox.open(Folder.READ_WRITE);
       }*/
       
       System.out.println("inbox   "+inbox.getMessageCount());
       Message[] arrayMessages = inbox.getMessages(batchStart, batchEnd);
       ArrayUtils.reverse(arrayMessages);
       FetchProfile fp = new FetchProfile();
       fp.add(FetchProfile.Item.ENVELOPE);
       fp.add(FetchProfileItem.FLAGS);
       fp.add(FetchProfileItem.CONTENT_INFO);
       fp.add("X-mailer");
       if (inbox instanceof UIDFolder) {
       	fp.add(UIDFolder.FetchProfileItem.UID);
       }
       inbox.fetch(arrayMessages, fp);
      int i=0;
      for (Message message : arrayMessages) {
       	/*LeadEmailDO leadEmailDO = new LeadEmailDO();
			Date date = null;
	        date = CommonUtil.convertEmailDate(message.getSentDate().toString());
		    String formattedDate = "";
		    if( date != null ) {
		    	formattedDate = CommonUtil.convertEmailDateToDate( date );
		    }
		   Address[] fromAddress = message.getFrom();
			leadEmailDO.setContactId(null);
			leadEmailDO.setFromAddress(fromAddress[0].toString());
	 		leadEmailDO.setSubject(message.getSubject());
	 		leadEmailDO.setSentDate(CommonUtil.convertStringToDateTimeFmt(formattedDate));
	 		leadEmailDO.setUpdatedon(new Date());
	 		leadEmailDO.setSeenMsg(message.isSet(Flags.Flag.SEEN));
			String messageContent = "";
			String attachFiles = "";
			leadEmailDO.setuId(uf.getUID(message));*/
   	   
   	  /* LeadEmailDO leadEmailDO = new LeadEmailDO();
			Date date = null;
	        date = CommonUtil.convertEmailDate(message.getReceivedDate().toString());
		    String formattedDate = "";
		    if( date != null ) {
		    	formattedDate = CommonUtil.convertEmailDateToDate( date );
		    }
		    System.out.println("date "+ CommonUtil.convertStringToDateTimeFmt(formattedDate));
		   Address[] fromAddress = message.getFrom();
			leadEmailDO.setContactId(null);
	 		leadEmailDO.setSubject(message.getSubject());
	 		leadEmailDO.setSentDate(CommonUtil.convertStringToDateTimeFmt(formattedDate));
	 		leadEmailDO.setUpdatedon(new Date());
	 		leadEmailDO.setSeenMsg(message.isSet(Flags.Flag.SEEN));
	 		leadEmailDO.setFlagMail(message.isSet(Flags.Flag.FLAGGED));
			leadEmailDO.setuId(uf.getUID(message));
			if (message.isMimeType("multipart/mixed")) {
			    Multipart mp = (Multipart)message.getContent();
			    if (mp.getCount() > 1){
			    	leadEmailDO.setHasAttachment(true);
			    } else {
			    	leadEmailDO.setHasAttachment(false);
			    }
				
			}
		 		leadEmailDOlist.add(leadEmailDO);*/
    	  if(i==0){
   		   LeadEmailDO leadEmailDO = new LeadEmailDO();
				Date date = null;
				Date date1 = new Date();
		        date = CommonUtil.convertEmailDate(message.getReceivedDate().toString());
		        Calendar now = Calendar.getInstance();
		        Calendar maildate = Calendar.getInstance();
		        now.setTime(date1);
		        maildate.setTime(date);
		        now.add(Calendar.MINUTE, -1);
		        if(now.getTime().getHours() == maildate.getTime().getHours()){
		        	if(now.getTime().getMinutes() == maildate.getTime().getMinutes()){
			        	 String formattedDate = "";
						    if( date != null ) {
						    	formattedDate = CommonUtil.convertEmailDateToDate( date );
						    }
						   Address[] fromAddress = message.getFrom();
				 			leadEmailDO.setContactId(null);
				 			leadEmailDO.setFromAddress(fromAddress[0].toString());
					 		leadEmailDO.setSubject(message.getSubject());
					 		leadEmailDO.setSentDate(CommonUtil.convertStringToDateTimeFmt(formattedDate));
					 		leadEmailDO.setUpdatedon(new Date());
							leadEmailDO.setuId(uf.getUID(message));
					 		leadEmailDOlist.add(leadEmailDO);
			        }
		        }
		 		i++;
    	  }
		}
      System.out.println(leadEmailDOlist.size());
	}
}